<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->post('/register', [
            'name'=> 'test',
            'email'=> 'test@test1.com',
            'phone_code' => '+91',
            'phone' => '1234567890',
            'password' => '12345678',
            'password_confirmation' => '12345678',
            'country' => 'US'
        ]);

        $response->assertRedirect('/');
        $this->assertAuthenticated();
        $this->assertAuthenticatedAs(\App\Models\User::where('email', 'test@test1.com')->first());
//        $response->assertStatus(200);
    }
}
