<?php

use Illuminate\Database\Seeder;

use App\Models\Country;

class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = file_get_contents(base_path('/data/countries.json'));

        $countries = json_decode($countries, true);

        foreach ($countries as $country) {
            Country::firstOrCreate([
                'iso' => $country['iso'],
                'name' => $country['country']
            ]);
        }
    }
}
