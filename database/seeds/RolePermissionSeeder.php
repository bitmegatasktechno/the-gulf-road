<?php

use Illuminate\Database\Seeder;

use App\Models\Role;
use App\Models\Permission;

class RolePermissionSeeder extends Seeder
{
    private $roles = [
        'agent',
        'buddy',
        'user',
        'admin'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->roles as $role) {
            Role::firstOrCreate([
                'name' => $role,
                'status' => 1
            ]);
        }
    }
}
