@extends('frontend.layouts.home')
@section('title','Blog Details')
@section('css')
	<link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
@endsection
@section('content')
@include('frontend.layouts.default-header')
<section class="body-light-grey pt-2 pb-0">
  <div class="container">
      <div class="row">
          <div class="col-md-12">
              <ul class="list-inline breadcrum mb-0">
                  <li><a href="{{url(app()->getLocale().'/home')}}">Home </a></li>
                  <span> / </span>
                  <li><a href="{{url(app()->getLocale().'/buy-property')}}"> Blog</a></li>
              </ul>
          </div>
      </div>
  </div>
</section>
<section class="property-image-block body-light-grey pt-0">
  <div class="container">
    <div class="row">
    <div class="col-md-12 pt-5 mt-3">
    <div class="img" style="height: 450px; overflow: hidden;">
    @if(!isset($blogs->pic) || $blogs->pic=='')
      <img class="img-fluid" src="{{asset('img/house2.png')}}" style="width: 100%; height: 100%; object-fit: cover;">
      @else
      @if(file_exists('uploads/blog/'.$blogs->pic))
      <img class="img-fluid" src="{{url('uploads/blog/'.$blogs->pic)}}" style="width: 100%; height: 100%; object-fit: cover;">
      @else
      <img class="img-fluid" src="{{asset('img/house2.png')}}" style="width: 100%; height: 100%; object-fit: cover;">
      @endif
     @endif
    </div>
    </div>
  </div>  
  </div>
</section>
<section class="body-light-grey p-0">
  <div class="container">
    <div class="row">
        <div class="col-md-12">
          <div class="banner-detail-box position-relative">
			  <h3 class="mb-3">{{$blogs->title}}</h3>
			 <div class="chat_list d-flex align-items-center p-0 mt-2">
				<div class="admin_icon mr-2">
            @if(isset($blogs->user->image_path))
				    <img src="{{$blogs->user->image_path}}" alt="">
           
            @endif    

				</div>
				<div class="admin_detail">
				   <div class="admin_name"> <label class="text-black mb-0">{{$blogs->user->name??''}}</label></div>
				   <p class="txt_msg">{{date('F d, Y', strtotime($blogs->created_at))}}</p>
				</div>
			 </div>
			<article class="mt-3">
        {!! $blogs->description !!}
			</article>
		</div>
    </div>
  </div>
</section>
@include('frontend.layouts.footer')
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
	    $(".show_hide").on("click", function () {
			var dots = document.getElementById("dots");
			if (dots.style.display === "none") {
				dots.style.display = "inline";
			} else {
				dots.style.display = "none";
			}
	        var txt = $(".content").is(':visible') ? 'Read More' : 'Read Less';
	        $(this).text(txt);
	        $('.content').toggle();
	    });
	});
</script>
@endsection