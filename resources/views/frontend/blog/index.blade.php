@extends('frontend.layouts.home')
@section('content')
@include('frontend.layouts.default-header')
<!-- contact ineer -->
<style>

p{
	margin-left: -6px;
}
.blog_image img {
    height: 100%;
    object-fit: cover;
    width: 100%;
}

.blog_image {
    height: 170px;
}
	</style>
  @php
$lang=Request::segment(1);
$loc=Request::segment(3);
$type=Request::segment(4);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
   
<section class="blogs-wrap">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h4 class="mb-5">Blogs</h4>
      </div>
      @if(count($getBlogs)>0)
	  @foreach($getBlogs as $row)

      <div class="col-md-3">
        <div class="blog-card mb-5" style="height: 329px; !important">
          <div class="blog_image">
            @if(!isset($row->pic) || $row->pic=='')
    		    <img class="img-fluid" src="{{asset('/img/no-property.png')}}">
    		    @else
    		    <img class="img-fluid" src="{{url('uploads/blog/'.$row->pic)}}">
    		    @endif
          </div>
          <div class="p-3">
            <p>{{date("M d, Y",strtotime($row->created_at))}}</p>
            <h5>{{\Str::limit(ucfirst($row->title), 25)}}</h5>
            <p>                                            
           
            </p>
            <a href="{{url(app()->getLocale().'/blog-details/'.$link.'/'.$row->_id)}}">Read More ></a>
          </div>
        </div>
      </div>
      @endforeach
      @else
                        <div class="" style="display: block;
                            max-height: 60px;
                            text-align: center;
                            background-color: #d6e9c6;
                            line-height: 4;
                            width: 100%;
                            ">
                            <p style="font-size: 16px;font-weight: 700;">No Result Found</p>
                        </div>
                    @endif
     
    </div>
  </div>
</section>
@include('frontend.layouts.footer')

@endsection


@section('scripts')
<script type="text/javascript">
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('.header').addClass('bg-header');
    } else {
       $('.header').removeClass('bg-header');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('fixed-top');
    } else {
       $('header').removeClass('fixed-top');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('#top-header-bar').removeClass('fixed-top');
    } else {
       $('#top-header-bar').addClass('fixed-top');
    }
});
  
</script>
<script type="text/javascript">
  jQuery("#testimonial").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  // autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:2,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 2,
      nav:true
    },

    1366: {
      items: 2,
      nav:true
    }
  }
});
  jQuery("#owl-example-second").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  // autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:4,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 4,
      nav:true
    },

    1366: {
      items: 4,
      nav:true
    }
  }
});
</script>
<script type="text/javascript">
  jQuery("#owl-example").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  // autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:4,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 4,
      nav:true
    },

    1366: {
      items: 4,
      nav:true
    }
  }
});
  jQuery("#carouselone").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:3,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 3,
      nav:true
    },

    1366: {
      items: 3,
      nav:true
    }
  }
});

</script>
@endsection
