@php
$lang=Request::segment(1);
$loc=Request::segment(3);
@endphp
@extends('frontend.layouts.home')
@section('title','Stripe Payment Property') 
@section('content')
@include('frontend.layouts.default-header')

@php


$sku_id = Request::get('sku_id');
$sku_base_decode = base64_decode($sku_id);
$sku_json_decode = json_decode($sku_base_decode);
$plan_amount = $sku_json_decode->plan_amount;
 

@endphp
@if($plan_amount*1 > 0.5)
<div class="payment-box">
    <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pay with Stripe</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="window.close()">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="paymentResponse" class="text-danger" ></div>
                     <form id="paymentFrmProp"  data-sku_id="{{$sku_id}}" >
                        <input type="hidden" id="plan_id" value="{{@$_GET['plan_id']}}">
                        <input type="hidden" id="plan_amount2" value="{{@$_GET['plan_amount']}}" >
                        <input type="hidden" id="plan_number" value="{{@$_GET['plan_number']}}">
                        <div class="form-group">
                            <label for="stripe_name" ><b>NAME</b></label>
                            <input readonly type="text" id="stripe_name" class="field  form-control" placeholder="Enter name" required="" autofocus="" value="{{@\Auth::user()->name}}">
                        </div>
                        <div class="form-group">
                            <label for="stripe_email"><b>EMAIL</b></label>
                            <input type="email" readonly  id="stripe_email" class=" field form-control" placeholder="Enter email" required="" value="{{@\Auth::user()->email}}">
                        </div>
                        
                        <div id="paymentElement">
                            <!--Stripe.js injects the Payment Element-->
                        </div>
                        
                        <!-- Form submit button -->
                        <br>
                         <div class="form-group">
                            <button id="submitBtn" class="btn btn-success btn-block">
                            <div class="spinner hidden" id="spinner"></div>
                            <span id="buttonText">Pay Now</span>
                        </button>
                        </div>
                    </form>

                    <div hidden id="frmProcess" class="hidden">
                        <span class="ring"></span> Processing...
                    </div>

                     <div id="payReinit" class="hidden">
                         
                    </div>
                     
                </div>
            </div>
        </div>
    </div>

 

@include('frontend.layouts.footer')
<script type="text/javascript" src="{{asset('lib/jquery.min.js')}}"></script>
<script src="https://js.stripe.com/v3/"></script>
<script STRIPE_PUBLISHABLE_KEY="{{env('STRIPE_KEY')}}" >
    
    // Get API Key
let STRIPE_PUBLISHABLE_KEY = document.currentScript.getAttribute('STRIPE_PUBLISHABLE_KEY');
var drect_url = WEBSITE_URL+"/profile/{{$loc}}"; 
var pay_redir ='sale_rent_request';
if(pay_redir =='profile_request')
{
    var drect_url = WEBSITE_URL+"/profile/{{$loc}}";    
}else if(pay_redir =='rent_request')
{
    var drect_url = WEBSITE_URL+"/listproperty/{{$loc}}/rent-sale";
}else if(pay_redir =='cospace_request')
{
    var drect_url = WEBSITE_URL+"/listproperty/{{$loc}}/coworkspace";
}else if(pay_redir =='swap_request')
{
    var drect_url = WEBSITE_URL+"/listproperty/{{$loc}}/swap";
} else if(pay_redir =='profile_request')
{
    var drect_url = WEBSITE_URL+"/stripe-success";
}  else if(pay_redir =='sale_rent_request')
{
    var drect_url = WEBSITE_URL+"/stripe-success";
} 
var drect_url = drect_url;
// Create an instance of the Stripe object and set your publishable API key
const stripe = Stripe(STRIPE_PUBLISHABLE_KEY);

let elements; // Define card elements
const paymentFrm = document.querySelector("#paymentFrmProp"); // Select payment form element

// Get payment_intent_client_secret param from URL
const clientSecretParam = new URLSearchParams(window.location.search).get(
    "payment_intent_client_secret"
);

// Check whether the payment_intent_client_secret is already exist in the URL
setProcessing(true);
if(!clientSecretParam){
    setProcessing(false);
    
    // Create an instance of the Elements UI library and attach the client secret
    //initialize();
}

// Check the PaymentIntent creation status
checkStatus();
initialize();
// Attach an event handler to payment form
paymentFrm.addEventListener("submit", handleSubmit);

// Fetch a payment intent and capture the client secret
let payment_intent_id;
async function initialize() {
    var plan_amount2 = $("#plan_amount2").val();
     var sku_id = $("#paymentFrmProp").data("sku_id");
    const { id, clientSecret } = await fetch(WEBSITE_URL+"/listproperty/stripeorder", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ request_type:'create_payment_intent', plan_amount:plan_amount2,_token:'{{ csrf_token() }}', sku_id:sku_id }),
    }).then((r) => r.json());
    
    const appearance = {
        theme: 'stripe',
        rules: {
            '.Label': {
                fontWeight: 'bold',
                textTransform: 'uppercase',
            }
        }
    };
    
    elements = stripe.elements({ clientSecret, appearance });
    
    const paymentElement = elements.create("payment");
    paymentElement.mount("#paymentElement");
    
    payment_intent_id = id;
}

// Card form submit handler
async function handleSubmit(e) {
    e.preventDefault();
    setLoading(true);
    
    let customer_name = document.getElementById("stripe_name").value;
    let customer_email = document.getElementById("stripe_email").value;

     

    let amount = document.getElementById("plan_amount2").value;
    let plan_id = document.getElementById("plan_id").value;
    let number = document.getElementById("plan_number").value;
    let sku_id = $("#paymentFrmProp").data("sku_id");
     
     
    
    const { id, customer_id } = await fetch(WEBSITE_URL+"/listproperty/stripeorder", {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ request_type:'create_customer', payment_intent_id: payment_intent_id, name: customer_name, email: customer_email,_token:'{{ csrf_token() }}', amount: amount, plan_id: plan_id, number: number,sku_id:sku_id }),
    }).then((r) => r.json());
    
    const { error } = await stripe.confirmPayment({
        elements,
        confirmParams: {
            //transaction_after_stripe();
            //"{{url('/').'/'.app()->getLocale()}}/profile/{{$loc}}"
            // Make sure to change this to your payment completion page
            return_url: drect_url+"/?popup=true&customer_id="+customer_id+"&amount="+amount+"&plan_id="+plan_id+"&number="+number,
        },
    });
    
    // This point will only be reached if there is an immediate error when
    // confirming the payment. Otherwise, your customer will be redirected to
    // your `return_url`. For some payment methods like iDEAL, your customer will
    // be redirected to an intermediate site first to authorize the payment, then
    // redirected to the `return_url`.
    if (error.type === "card_error" || error.type === "validation_error") {
        showMessage(error.message);
    } else {
        showMessage("An unexpected error occured.");
    }
    
    setLoading(false);
}

// Fetch the PaymentIntent status after payment submission
async function checkStatus() {
    const clientSecret = new URLSearchParams(window.location.search).get(
        "payment_intent_client_secret"
    );
    
    const customerID = new URLSearchParams(window.location.search).get(
        "customer_id"
    ); 
    const amount = new URLSearchParams(window.location.search).get(
        "amount"
    ); 
    const plan_id = new URLSearchParams(window.location.search).get(
        "plan_id"
    );
    const number = new URLSearchParams(window.location.search).get(
        "number"
    );
    const purchase_type = new URLSearchParams(window.location.search).get(
        "purchase_type"
    );
    

      


    if (!clientSecret) {
        return;
    }
    
    const { paymentIntent } = await stripe.retrievePaymentIntent(clientSecret);
    
    if (paymentIntent) {
         switch (paymentIntent.status) { 
            case "succeeded":
                  Swal.fire({
                            title: 'Success!',
                            text: "Transaction completed successfully ",
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                          window.location.href=WEBSITE_URL+"/profile/{{$loc}}";
                        });
                fetch(WEBSITE_URL+"/listproperty/stripeorder", {
                    method: "POST",
                    headers: { "Content-Type": "application/json" },
                    body: JSON.stringify({ request_type:'payment_insert', payment_intent: paymentIntent, customer_id: customerID,_token:'{{ csrf_token() }}',amount: amount, id: plan_id, number: number, purchase_type: purchase_type}),
                })
                .then(response => response.json())
                .then(data => {
                     
                    if (data.status=='1') {
                         
                    } else {
                        
                        showMessage(data.error);
                        //setReinit();
                    }
                })
                .catch(console.error);
                // Post the transaction info to the server-side script and redirect to the payment status page
                
                
                break;
            case "processing":
                showMessage("Your payment is processing.");
                setReinit();
                break;
            case "requires_payment_method":
                showMessage("Your payment was not successful, please try again.");
                setReinit();
                break;
            default:
                showMessage("Something went wrong.");
                setReinit();
                break;
        }
    } else {
        showMessage("Something went wrong.");
        setReinit();
    }
}


// Display message
function showMessage(messageText) {
    const messageContainer = document.querySelector("#paymentResponse");
    
    messageContainer.classList.remove("hidden");
    messageContainer.textContent = messageText;
    
    setTimeout(function () {
        messageContainer.classList.add("hidden");
        messageText.textContent = "";
    }, 5000);
}

// Show a spinner on payment submission
function setLoading(isLoading) {
    if (isLoading) {
        // Disable the button and show a spinner
        document.querySelector("#submitBtn").disabled = true;
        document.querySelector("#spinner").classList.remove("hidden");
        document.querySelector("#buttonText").classList.add("hidden");
    } else {
        // Enable the button and hide spinner
        document.querySelector("#submitBtn").disabled = false;
        document.querySelector("#spinner").classList.add("hidden");
        document.querySelector("#buttonText").classList.remove("hidden");
    }
}

// Show a spinner on payment form processing
function setProcessing(isProcessing) {
    if (isProcessing) {
        paymentFrm.classList.add("hidden");
        document.querySelector("#frmProcess").classList.remove("hidden");
    } else {
        paymentFrm.classList.remove("hidden");
        document.querySelector("#frmProcess").classList.add("hidden");
    }
}

// Show payment re-initiate button
function setReinit() {
    document.querySelector("#frmProcess").classList.add("hidden");
    document.querySelector("#payReinit").classList.remove("hidden");
}
//save data in transaction after pay
  function transaction_after_stripe()
{

     
        var ajax_url = WEBSITE_URL+"/listproperty/transaction";
        var amount=$("#plan_amount2").val();
        var id=$("#plan_id").val();
        var number=$("#plan_number").val();
        var purchase_type=$("#purchase_type").val();
        alert("amount:"+amount+",id:"+id+",number:"+number+",purchase_type:"+purchase_type);
        $.ajax({
                url : ajax_url,
                method:"post",
              data:{
                amount:amount,
                id:id,
                number:number,
                purchase_type:purchase_type
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              beforeSend:function(){
                    startLoader('.payment-box');
                },
                complete:function(){
                   stopLoader('.payment-box'); 
                },
                success : function(data){
                     
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: 'Transaction completed successfully',
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                            if(result.value){
                                $("#StripeCardModal").hide();
                                $(".subscribe").hide();
                                $(".modal-backdrop").hide();
                                $("body").removeClass('modal-open')
                                $(".modal").hide();
                                $("#myModal").hide();
                                showScreen('step1');
                            }
                        });
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                }
            });

        return "{{url('/').'/'.app()->getLocale()}}/profile/{{$loc}}";
}
</script>

@endsection
     
   @endif
 
    