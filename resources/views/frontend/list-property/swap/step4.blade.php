<section class="buy-rent step4 listPropertySteps" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3 class="mb-3">Description & Availability</h3>
                <p class="mb-5">Tell us more about your place and its surroundings. Set the availablity of your house for the users to know.</p>
                
                <div name="property_step_4" id="property_step_4" class="property_step_4">
                    <div class="row">
                        <div class="col-md-12">
                            <h5>Describe your Home</h5>
                            <div class="loc-form">
                                <textarea class="form-control mt-3 mb-1" rows="6" placeholder="“My Home is Located Near the Lake and has a beautiful view of the whole town.”" name="property_description"></textarea>
                                <span class="invalid-feedback property_description"></span>
                            </div>
                        </div>
                    </div>

                     <div class="row mt-4">
                        <div class="col-md-12">
                            <h5>Describe your Neighbourhood</h5>
                            <div class="loc-form">
                                <textarea class="form-control mt-3 mb-1" rows="6" placeholder="“My Home is Located Near the Lake and has a beautiful view of the whole town.”" name="neighbourhood_description"></textarea>
                                <span class="invalid-feedback neighbourhood_description"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-md-12">
                           <h5 class="mt-4">Set Availability</h5>
                            <p class="mb-4">Add your period of availability of your House. Your house will be open to swap during this period.</p>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="full-width" id="available_dates">
                                    <input type="hidden" name="availability" id="my_hidden_input" value="">
                                </div>

                                <span class="invalid-feedback availability"></span>
                            </div>
                        </div>
                    </div>

                    <div class="pt-3 pb-3">
                        <hr>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <a class="mt-2 dp-inline-block" href="javascript:;" onclick="showScreen('step3');">< Go back</a>
                        </div>
                        <div class="col-md-6 text-right">
                            <button class="red-btn rounded float-right submit_step_4" type="button">
                                Continue 
                                <img class="filter-white" src="{{asset('img/black_arrow.png')}}">
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="offset-md-1 col-md-6">
                <img class="img-responsive" src="{{asset('img/C.png')}}">
            </div>
        </div>
    </div>
</section>