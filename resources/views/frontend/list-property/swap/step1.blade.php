    <style type="text/css">
       .pac-container{
          margin-top: -62px;
        }
    </style>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<style type="text/css">
.label-info{
color: white;
background: #E4002B;
padding: 1px 5px 2px 5px;
}
</style>

<section class="buy-rent step1 listPropertySteps" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3 class="mb-3">List your Property for Swap</h3>
                <p class="mb-5">Tell us something about the property that you would like to swap list on our Platform. </p>
                
                <div class="second property_step_1" name="property_step_1" id="property_step_1">
                    
                    <h5>Title</h5>
                    <input type="hidden" name="loc" value="{{$loc}}">
                    <p>Give your listing a title so that people can find it.</p>
                    <div class="row">
                        <div class="col-md-11">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="“Fully Furnished House”" name="title">
                                <span class="invalid-feedback title"></span>
                            </div>
                        </div>
                    </div>
                
                    <h5>House Type</h5>
                    <p>Select the type of property that you are listing for swap.</p>
                    <div class="row boxed second mb-0">
                        <div class="col-md-11">
                            <div class="form-group">
                                @foreach(getAllHouseTypes() as $row)
                                    <input type="radio" id="{{$row->name}}" name="house_type" value="{{strtolower($row->name)}}">
                                    <label for="{{$row->name}}" class="customCursorClass">
                                        <img src="{{url('uploads/housetypes/'.$row->logo)}}"> 
                                        <br>{{ucfirst($row->name)}}
                                    </label>
                                @endforeach
                                <br>
                                 <span class="invalid-feedback house_type"></span>
                            </div>
                        </div>
                    </div>
                
                    <h5>Location</h5>
                    <p>What is the location of the property you are listing?</p>
                    <div class="row">
                        <div class="col-md-11">
                            <div class="form-group" style="position: relative;">
                                <input type="text" class="form-control" onChange="getLocation()" id="address" placeholder="Enter Location Name" name="location_name">
                                <span class="invalid-feedback location_name"></span>
                            </div>
                            <div class="row" hidden >
                               <div class="col-md-6" hidden>
                                  <input class="form-control" style="padding: 0;height: 20px;"  type="text"  name="latitude" id="lat"  hidden >
                                </div>
                                <div class="col-md-6" hidden>
                                  <input class="form-control" style="padding: 0;height: 20px;"   type="text" name="longitude" id="lng" hidden>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select class="form-control" name="country" id="country"  onchange="setCities(this)" required="true">
                                            <option value="">Select Country</option>
                                            @foreach(getAllLocations() as $country)
                                                <option value="{{ $country->name }}">{{ __(ucfirst($country->name)) }}</option>
                                            @endforeach
                                        </select>
                                        <span class="invalid-feedback country"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select class="form-control" name="city" id="countryCity" required>
                                            <option value="">Select City</option>
                                        </select>
                                        <span class="invalid-feedback city"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="map" style="display:none"></div>
                    <h5>Owner's interest</h5>
                    <p>What is the location of the property which you want to swap?</p>
                    <div class="row">
                        <div class="col-md-11">
                            <div class="form-group">
                                <input type="text" class="form-control"  placeholder="Enter Location Name" name="owner_locations" data-role="tagsinput">
                                <span class="invalid-feedback location_name"></span>
                            </div>
                        </div>
                    </div>
                    
                    <h5>Accesibility</h5>
                    <p>How accessible is the location that your house is in?</p>
                    
                    <div class="row loc-form">
                        <div class="col-md-11">
                            <div class="radio dp-inline-block mr-3" style="margin-bottom: 0;">
                             
                                <input id="access-location-radio-1" name="location_accessible" type="radio" value="urban_area">
                                <label for="access-location-radio-1" class="radio-label">Urban Area : things are easily Accessible.</label>
                            </div>
                            <div class="radio dp-inline-block mr-3" style="margin-bottom: 0;">
                                <input id="access-location-radio-2" name="location_accessible" type="radio" value="semi_area">
                                <label for="access-location-radio-2" class="radio-label">Semi Urban Area : Most of the Things are Accessible.</label>
                            </div>
                            <div class="radio dp-inline-block mr-3" style="margin-bottom: 0;">
                                <input id="access-location-radio-3" name="location_accessible" type="radio" value="rural_area">
                                <label for="access-location-radio-3" class="radio-label">Rural Area : Accessibility depends on Availability.</label>
                            </div>
                            <br>
                            <span class="invalid-feedback location_accessible"></span>
                        </div>
                    </div>
                    <div class="pt-3">
                        <hr>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            
                        </div>
                        <div class="col-md-6">
                            <button class="red-btn rounded float-right submit_step_1" type="button">
                                Continue 
                                <img class="filter-white" src="{{asset('img/black_arrow.png')}}">
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <img class="img-responsive" src="{{asset('img/buy-rent-img.jpg')}}">
            </div>
        </div>
    </div>
</section>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>  
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

@if(env('APP_ACTIVE_MAP') =='patel_map')
<style>

    #address_result
    {
        z-index: 9;
    left: unset !important;
    top: unset !important;
    width: 100% !important;
}


</style>
   <script src="https://mapapi.cloud.huawei.com/mapjs/v1/api/js?callback=initMap&key={{env('PATEL_MAP_KEY')}}"></script>
    <script>
        function initMap() {
            var mapOptions = {};
            mapOptions.center = {lat: 48.856613, lng: 2.352222};
            mapOptions.zoom = 8;
            mapOptions.language='ENG';
            var searchBoxInput = document.getElementById('address');
             var map = new HWMapJsSDK.HWMap(document.getElementById('map'), mapOptions);
            // Create the search box parameter.
            var acOptions = {
                location: {
                    lat: 48.856613,
                    lng: 2.352222
                },
                radius: 10000,
                customHandler: callback,
                language:'en'
            };
            var autocomplete;
            // Obtain the input element.
            // Create the HWAutocomplete object and set a listener.
            autocomplete = new HWMapJsSDK.HWAutocomplete(searchBoxInput, acOptions);
            autocomplete.addListener('site_changed', printSites);
            // Process the search result. 
            // index: index of a searched place.
            // data: details of a searched place. 
            // name: search keyword, which is in strong tags. 
            function callback(index, data, name) {
                
                var div
                div = document.createElement('div');
                div.innerHTML = name;
                return div;
            }
            // Listener.
            function printSites() {
                 
                
                var site = autocomplete.getSite();
                var latitude = site.location.lat;
                var longitude = site.location.lng;
                var country_name = site.address.country;
                var results = "Autocomplete Results:\n";
                var str = "Name=" + site.name + "\n"
                    + "SiteID=" + site.siteId + "\n"
                    + "Latitude=" + site.location.lat + "\n"
                    + "Longitude=" + site.location.lng + "\n"
                    + "Address=" + site.formatAddress + "\n"
                    + "Country=" + site.address.country + "\n";

                    
                results = results + str;
                 
                 


                $('#lat').val(latitude);
                $('#lng').val(longitude);
                $('#country').val(country_name);
                setCities('#country');
                console.log(latitude, longitude);
                console.log(results); 
            }
            
        }
        function getLocation()
        {

        }
    </script> 

     
    @elseif(env('APP_ACTIVE_MAP')=='google_map')
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&callback=initMap&libraries=places&v=weekly" defer ></script>
<script>

function initMap() {
    const myLatLng = {
      lat: -25.363,
      lng: 131.044
    };
    
      

  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 4,
    center: myLatLng
  });
    var input = document.getElementById('address');

 map.controls[google.maps.ControlPosition.TOP_RIGHT].push();

var autocomplete = new google.maps.places.Autocomplete(input);
autocomplete.bindTo('bounds', map);
autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);
            var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29),
          title: "Hello World!"
        });
        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindow.open(map, marker);
        });
  new google.maps.Marker({
    position: myLatLng,
    map,
    title: "Hello World!"
  });
}

function getLocation()
 {
//   alert(address);
  var address = $('#address').val();
//   alert(address);
  var geocoder = new google.maps.Geocoder();

  geocoder.geocode( { 'address': address}, function(results, status)
  {


    if (status == google.maps.GeocoderStatus.OK)
    {
      console.log(results[0],"afdcfsa");
      console.log(results[0].address_components);
      const countryObj=results[0].address_components.filter(val=>{
            if(val.types.includes('country')){
                return true
            }
            return false;
      });
     console.log(countryObj,'the country obj');


     var country_name = countryObj[0].long_name;
     console.log(country_name,'name');
      var latitude = results[0].geometry.location.lat();

      var longitude = results[0].geometry.location.lng();
      // alert(latitude +"-----"+ longitude);
     $('#lat').val(latitude);
     $('#lng').val(longitude);
     $('#countryTo').val(country_name);
     $('#country_name').val(country_name);
    console.log(latitude, longitude);
    }
  });

}
    </script>
    @endif