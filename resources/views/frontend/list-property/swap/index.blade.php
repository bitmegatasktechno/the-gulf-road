@php
$lang=Request::segment(1);
$loc=Request::segment(3);
$modules = \App\Models\Module::where('location',$loc)->get();
$link=$loc;
$current=date('Y-m-d');
$users = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','swap')->where('expiry_date','>=',$current )->count();
if($users){
    if(@$users > 0){
        $last_transaction = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','swap')->where('expiry_date','>=',$current)->latest()->first();
		if($last_transaction->count > $last_transaction->listed){
			Session::put('transaction_id',$last_transaction->_id);
			$package="NO";
		}else{
			$package="YES";
		}
    }else{
        $package="YES";
    }
}else{
    $package="YES";
}
@endphp
@extends('frontend.layouts.home')
@section('title','Swap Your Property')

@section('css')
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" />
	<script
    src="https://www.paypal.com/sdk/js?client-id={{env('SB_CLIENT_ID')}}"> // Required. Replace SB_CLIENT_ID with your sandbox client ID.
  </script>
@endsection
@section('content')
    
    @include('frontend.layouts.default-header')
    
    <form class="loc-form swap_master_form" name="swap_master_form" id="swap_master_form" enctype="multipart/form-data">
    	@csrf
		@if($package=='YES')
		@include('frontend.list-property.subscription')
		@endif
	    @include('frontend.list-property.swap.step0')
	    @include('frontend.list-property.swap.step1')
	    @include('frontend.list-property.swap.step2')
	    @include('frontend.list-property.swap.step3')
	    @include('frontend.list-property.swap.step4')
	    @include('frontend.list-property.swap.step5')
		@include('frontend.others.package')

		<div id="listPropertyErrors" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<ul class="listProperty_errors_points">
							
						</ul>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</form>

	


    
    @include('frontend.layouts.footer')

@endsection

@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

 <script type="text/javascript">
	let propertySteps = false;
	let total_beds = 0;


	function file_validation(id,max_size)
   {
       var fuData = document.getElementById(id);
       var FileUploadPath = fuData.value;
       
   
       if (FileUploadPath == ''){
           alert("Please upload Attachment");
       } 
       else {
           var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
           if (Extension == "jpg" || Extension == "jpeg" || Extension == "png" || Extension == "webp" ) {
   
                   if (fuData.files && fuData.files[0]) {
                       var size = fuData.files[0].size;
                       
                       if(size > max_size){   //1000000 = 1 mb
                           
                           Swal.fire({
			                    title: 'Warning!',
			                    text: "Maximum file size "+max_size/1000000+" MB",
			                    icon: 'info',
			                    confirmButtonText: 'Ok'
			                });
			                
                           $("#"+id).val('');
                           return false;
                            
                       }
                   }
   
           } 
           else 
           {
                
               $("#"+id).val('');
                Swal.fire({
			                    title: 'Warning!',
			                    text: "document only allows file types of  jpg , png  , jpeg , webp ",
			                    icon: 'info',
			                    confirmButtonText: 'Ok'
			                });
			                
                           $("#"+id).val('');
                           return false;
           }
       }   
   } 



	function payPlan(id,amount,number){
    
    var planid= $("#plan_id22").val();
    var planamount=$("#plan_amount22").val();
	var plannumber = $("#plan_number22").val();
	 var sku_id 	= $("#sku_id2").val();
	if(!planid)
	{
		alert('Please select any plan');
		return false;
	}
	$("#plan_id").val(planid);
    $("#plan_amount2").val(planamount);
	$("#plan_number").val(plannumber);
	$("#plan_amount").html(planamount);
	$("#sku_id").val(sku_id);
	if(planamount==0){
	$("#myModal1").show();
	}else{
   $("#myModal").show();
	}
}
    const showScreen = function(step){
		$('.listPropertySteps').hide();
		$('.'+step).show();
		$("html, body").animate({ scrollTop: 0 }, "slow");
	}

	const readURL = function(input) {

		var id = $(input).attr('id');
		 var max_size = 2000000;
           file_validation(id,max_size);

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
            	var id=input.id;
            	$('#'+id+'_src').attr('src', e.target.result);
		    }
            reader.readAsDataURL(input.files[0]);
        }
    }

	

	$(document).ready(function(){
	
		$('#available_dates').datepicker({
		  	multidate: true,
			format: 'yyyy-mm-dd',
			startDate: new Date(),
			maxViewMode:0,
			endDate: '+6m',
		});

		$("html, body").animate({ scrollTop: 0 }, "slow");

		$(".minus").click(function() {
			var $input = $(this).parent().find("input");
			if(parseInt($input.val())==0){
				return false;
			}

			var count = parseInt($input.val()) - 1;
			count = count < 1 ? 0 : count;
			if($(this).hasClass('bed_minus')){
				total_beds--;
				$('.total_beds').html(total_beds);
				$('#total_accommodation').val(total_beds);
			}
			$input.val(count);
			$input.change();
			return false;
		});

		$(".plus").click(function() {
			var $input = $(this).parent().find("input");
			if(parseInt($input.val())==10){
				return false;
			}

			if($(this).hasClass('bed_plus')){
				total_beds++;
				$('.total_beds').html(total_beds);
				$('#total_accommodation').val(total_beds);
			}
			$input.val(parseInt($input.val()) + 1);
			$input.change();
			return false;
		});

		$(document).on('click','.amenitiesBlock', function(){
			$(this).toggleClass('active');
		});

		$(document).on('click','.removeDiv', function(){
			$(this).parent().remove();
		});

		$(document).on('click','#add_more', function(){
			if($('.upload').length>=15){
				Swal.fire({
                    title: 'Warning!',
                    text: "You can upload maximum 15 pictures.",
                    icon: 'info',
                    confirmButtonText: 'Ok'
                });
                return false;
			}
			var id= $('.upload').length+1;
			$("#pictureresult").append('\
                <div class="upload col-md-4 mb-3">\
                    <input type="file" id="files'+id+'" name="files[]" class="input-file" onchange="readURL(this)" >\
                    <label for="files'+id+'" class="p-0">\
                        <img id="files'+id+'_src" src="{{asset("img/addmedia-upload.png")}}" style="height:100%;width:100%;">\
                    </label>\
                    <span class="remove removeDiv" style="cursor:pointer;position: absolute;top: -17px;right: 4px;font-size: 22px;"><i class="fa fa-times" aria-hidden="true"></i></span>\
                    <span class="invalid-feedback files'+id+'"></span>\
                </div>');
		});

		$(window).bind('beforeunload',function() {
	        if(!propertySteps){
                return "'Are you sure you want to leave the page. All data will be lost!";
            }
	    });		

		$(document).on('click','.submit_step_0', function(){
	        showScreen('step1');
	    });

		// submit on first screen event
		$(document).on('click','.submit_step_1', function(){
			$form = $('#property_step_1');
	    	$form.find('.is-invalid').removeClass('is-invalid');
	        $form.find('.invalid-feedback').text('');

	        var ajax_url = WEBSITE_URL+"/listproperty/swap/1";
	        var data = new FormData();

	        var house_type = $('input[name="house_type"]:checked').val() ? $('input[name="house_type"]:checked').val(): '';
	        var location_accessible = $('input[name="location_accessible"]:checked').val() ? $('input[name="location_accessible"]:checked').val(): '';

	        data.append('title', $form.find('input[name="title"]').val());
	        data.append('country', $form.find('select[name="country"]').val());
	        data.append('city', $form.find('select[name="city"]').val());
	        data.append('location_name', $form.find('input[name="location_name"]').val());
	        data.append('house_type', house_type);
	        data.append('location_accessible', location_accessible);
	        data.append('owner_locations',$('input[name="owner_locations"]').val());
	        
	        $.ajax({
	            url : ajax_url,
	            type:'post',
	            data : data,
	            dataType : 'json',
	            processData: false,
	            contentType: false,
	            beforeSend:function(){
	                startLoader('.swap_master_form');
	            },
	            complete:function(){
	               stopLoader('.swap_master_form'); 
	            },
	            success : function(data){
	                if(data.status){
	                    showScreen('step2');
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            },
	            error : function(data){
	                stopLoader('.swap_master_form');
	                if(data.responseJSON){
	                    var err_response = data.responseJSON;  
	                    if(err_response.errors==undefined && err_response.message) {
	                        Swal.fire({
	                            title: 'Error!',
	                            text: err_response.message,
	                            icon: 'error',
	                            confirmButtonText: 'Ok'
	                        });
	                    }
	                    $.each(err_response.errors, function(i, obj){
	                        $form.find('span.invalid-feedback.'+i+'').text(obj).show();
	                        $form.find('input[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('select[name="'+i+'"]').addClass('is-invalid');
	                    });
	                }
	            }
	        });
	    });

		// submit on second screen event
	    $(document).on('click','.submit_step_2', function(){
			$form = $('.property_step_2');
	    	$form.find('.is-invalid').removeClass('is-invalid');
	        $form.find('.invalid-feedback').text('');

	        var ajax_url = WEBSITE_URL+"/listproperty/swap/2";
	        var data = new FormData();

	        data.append('surface_area', $form.find('input[name="surface_area"]').val());
	        data.append('surface_area_unit', $form.find('select[name="surface_area_unit"]').val());
	        data.append('no_of_bedrooms', $form.find('input[name="no_of_bedrooms"]').val());
	        data.append('no_of_bathrooms', $form.find('input[name="no_of_bathrooms"]').val());
	        data.append('no_of_children_beds', $form.find('input[name="no_of_children_beds"]').val());
	        data.append('no_of_single_beds', $form.find('input[name="no_of_single_beds"]').val());
	        data.append('no_of_double_beds', $form.find('input[name="no_of_double_beds"]').val());
	        data.append('no_of_extra_beds', $form.find('input[name="no_of_extra_beds"]').val());
	        data.append('total_accommodation', $form.find('input[name="total_accommodation"]').val());
	        
	        $.ajax({
	            url : ajax_url,
	            type:'post',
	            data : data,
	            dataType : 'json',
	            processData: false,
	            contentType: false,
	            beforeSend:function(){
	                startLoader('.swap_master_form');
	            },
	            complete:function(){
	               stopLoader('.swap_master_form'); 
	            },
	            success : function(data){
	                if(data.status){
	                    showScreen('step3');
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            },
	            error : function(data){
	                stopLoader('.swap_master_form');
	                if(data.responseJSON){
	                    var err_response = data.responseJSON;  
	                    if(err_response.errors==undefined && err_response.message) {
	                        Swal.fire({
	                            title: 'Error!',
	                            text: err_response.message,
	                            icon: 'error',
	                            confirmButtonText: 'Ok'
	                        });
	                    }
	                    $.each(err_response.errors, function(i, obj){
	                        $form.find('span.invalid-feedback.'+i+'').text(obj).show();
	                        $form.find('input[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('select[name="'+i+'"]').addClass('is-invalid');
	                    });
	                }
	            }
	        });
	    });

	    // submit on 3rd screen event
	    $(document).on('click','.submit_step_3', function(){
	        var ajax_url = WEBSITE_URL+"/listproperty/swap/3";
	        var data = new FormData();

	        $('.amenitiesBlock').each(function(i){
                if($(this).is('.active')){
					
                    data.append('amenties[]', $(this).attr('data-name'));
                }
            });

	        $.ajax({
	            url : ajax_url,
	            type:'post',
	            data : data,
	            dataType : 'json',
	            processData: false,
	            contentType: false,
	            beforeSend:function(){
	                startLoader('.swap_master_form');
	            },
	            complete:function(){
	               stopLoader('.swap_master_form'); 
	            },
	            success : function(data){
	                if(data.status){
	                    showScreen('step4');
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            },
	            error : function(data){
	                stopLoader('.swap_master_form');
	                if(data.responseJSON){
	                    var err_response = data.responseJSON;  
	                    if(err_response.errors==undefined && err_response.message) {
	                        Swal.fire({
	                            title: 'Error!',
	                            text: err_response.message,
	                            icon: 'error',
	                            confirmButtonText: 'Ok'
	                        });
	                    }
	                }
	            }
	        });
	    });

	    $(document).on('click','.submit_step_4', function(){
			$form = $('.property_step_4');
	    	$form.find('.is-invalid').removeClass('is-invalid');
	        $form.find('.invalid-feedback').text('');

	        var ajax_url = WEBSITE_URL+"/listproperty/swap/4";
	        var data = new FormData();

	        data.append('property_description', $form.find('textarea[name="property_description"]').val());
	        data.append('neighbourhood_description', $form.find('textarea[name="neighbourhood_description"]').val());
	        data.append('availability', $form.find('input[name="availability"]').val());
	        
	        $.ajax({
	            url : ajax_url,
	            type:'post',
	            data : data,
	            dataType : 'json',
	            processData: false,
	            contentType: false,
	            beforeSend:function(){
	                startLoader('.swap_master_form');
	            },
	            complete:function(){
	               stopLoader('.swap_master_form'); 
	            },
	            success : function(data){
	                if(data.status){
	                    showScreen('step5');
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            },
	            error : function(data){
	                stopLoader('.swap_master_form');
	                if(data.responseJSON){
	                    var err_response = data.responseJSON;  
	                    if(err_response.errors==undefined && err_response.message) {
	                        Swal.fire({
	                            title: 'Error!',
	                            text: err_response.message,
	                            icon: 'error',
	                            confirmButtonText: 'Ok'
	                        });
	                    }
	                    $.each(err_response.errors, function(i, obj){
	                        $form.find('span.invalid-feedback.'+i+'').text(obj).show();
	                        $form.find('input[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('select[name="'+i+'"]').addClass('is-invalid');
	                    });
	                }
	            }
	        });
	    });



		$(document).on('click','.submit_step_5', function(){
			$('.buy-rent').hide();
			$(".package").show();
		});

	    $(document).on('click','.submit_step_8', function(){

	        var ajax_url = WEBSITE_URL+"/listproperty/swap/5";
	        var $form = $('#property_step_5');
	        $form.find('.is-invalid').removeClass('is-invalid');
	        $form.find('.invalid-feedback').text('');

	        var data = new FormData();
	        var error = false;
	        
	        $.each($('.listPropertySteps [type=file]'), function(index, file) {
	        	if($('input[type=file]')[0].files[0]==undefined){
	        		var id = $('input[type=file]')[0].id;
                    $('.invalid-feedback.'+(id)).text('The file is required.').show();
	        		error = true;
	        	}
	        	
				data.append('files[]', $('input[type=file]')[0].files[0] ? $('input[type=file]')[0].files[0]: '');
			});

			if(error){
				return false;
			}
			  
	        $.ajax({
	            url : ajax_url,
	            type:'post',
	            data : data,
	            enctype: 'multipart/form-data',
	            dataType : 'json',
	            processData: false,
	            contentType: false,
	            beforeSend:function(){
	                startLoader();
	            },
	            complete:function(){
	               stopLoader(); 
	            },
	            success : function(data){

	            	$(".package").hide();
	                if(data.status){
	                    submitSwap();
	                }else{
	                	stopLoader();
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            },
	            error : function(data){
	                stopLoader('.swap_master_form');
	                if(data.responseJSON){
	                    var err_response = data.responseJSON;  
	                    if(err_response.errors==undefined && err_response.message) {
	                        Swal.fire({
	                            title: 'Error!',
	                            text: err_response.message,
	                            icon: 'error',
	                            confirmButtonText: 'Ok'
	                        });
	                    }
	                    $.each(err_response.errors, function(key, value){
	                        if(key.indexOf('.') != -1) {
                                let keys = key.split('.');                                
                                $('input[name="'+keys[0]+'[]"]').eq(keys[1]).addClass('is-invalid').parent().find('.invalid-feedback').text(value).show();                         
                            }
                            else {
                                $('input[name="'+key+'[]"]').eq(0).addClass('is-invalid').parent().find('.invalid-feedback').text(value).show();
                            }
	                    });
	                }
	            }
	        });
	    });
	});

	const submitSwap = function(){
		var ajax_url = WEBSITE_URL+"/listproperty/swap/6";
        var $form = $('#swap_master_form');
        $form.find('.is-invalid').removeClass('is-invalid');
        $form.find('.invalid-feedback').text('');

        var data = new FormData($form[0]);    
              
        $('.amenitiesBlock').each(function(i){
            if($(this).is('.active')){
                data.append('amenties[]', $(this).attr('data-name'));
            }
        });
        data.append('total_accommodation', $('input[name="total_accommodation"]').val());
         
        $.ajax({
            url : ajax_url,
            type:'post',
            data : data,
            dataType : 'json',
            processData: false,
            contentType: false,
            beforeSend:function(){
                startLoader();
            },
            complete:function(){
               stopLoader();
            },
            success : function(data){
                if(data.status){
                	stopLoader();
                	propertySteps = true;
                	$('.submit_step_5').attr('disabled',true);
                	
                    Swal.fire({
                        title: 'Success!',
                        text: data.message,
                        icon: 'success',
                        confirmButtonText: 'Ok'
                    }).then((result) => {
			            if(result.value){
			                window.location =data.url;
			            }else{
			            	window.location =data.url;
			            }
			        });
                }else{
                    Swal.fire({
                        title: 'Error!',
                        text: data.message,
                        icon: 'error',
                        confirmButtonText: 'Ok'
                    });
                    stopLoader(); 
                }
            },
            error : function(data){
                stopLoader();
                if(data.responseJSON){
                    var err_response = data.responseJSON;  
                    if(err_response.errors==undefined && err_response.message) {
                        Swal.fire({
                            title: 'Error!',
                            text: err_response.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                    var html='';
                    $.each(err_response.errors, function(i, obj){
                        html+='<li>'+obj+'</li>';
                    });
                    if(html){
                    	$('.listProperty_errors_points').html(html);
                    	$('#listPropertyErrors').modal('toggle');
                    }
                }
            }
        });
	}
	</script>
<script type="text/javascript">
function selectPlan(id,amount,number,sku_id=''){
		$("#plan_amount").html(amount);
		$("#plan_id").val(id);
		$("#plan_amount2").val(amount);
		$("#plan_number").val(number);
		$("#stipe_payment_btn").val(amount);
		$("#sku_id2").val(sku_id);


	if(amount==0){
	$("#myModal1").show();
	}else{
   $("#myModal").show();
	}
}

  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('bg-header');
    } else {
       $('header').removeClass('bg-header');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('fixed-top');
    } else {
       $('header').removeClass('fixed-top');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('#top-header-bar').removeClass('fixed-top');
    } else {
       $('#top-header-bar').addClass('fixed-top');
    }
});
  
</script>
<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery('#loading').fadeOut(1000);
});

</script>
<script>
    
  paypal.Buttons({
    style:{
      color: 'white',
      layout: 'horizontal',
      tagline: false,
      shape:   'rect'
    },
    createOrder: function(data, actions) {
      // This function sets up the details of the transaction, including the amount and line item details.
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: $("#plan_amount2").val()
          }
        }]
      });
    },
    onApprove: function(data, actions) {
      // This function captures the funds from the transaction.
      return actions.order.capture().then(function(details) {
        var ajax_url = WEBSITE_URL+"/listproperty/transaction";
        var amount=$("#plan_amount2").val();
        var id=$("#plan_id").val();
        var number=$("#plan_number").val();
        $.ajax({
	            url : ajax_url,
	            method:"post",
              data:{
                amount:amount,
                id:id,
                number:number,
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
			  },
			  beforeSend:function(){
	                startLoader('.swap_master_form');
	            },
	            complete:function(){
	               stopLoader('.swap_master_form'); 
	            },
	            success : function(data){
	                if(data.status){
                        Swal.fire({
	                        title: 'Success!',
	                        text: 'Transaction completed successfully',
	                        icon: 'success',
	                        confirmButtonText: 'Ok'
	                    }).then((result) => {
				            if(result.value){
								$(".subscribe").hide();
								$("#myModal").hide();
                                showScreen('step0');
				            }
				        });
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            }
	        });
        // This function shows a transaction success message to your buyer.
        
        // alert('Transaction completed by ' + details.payer.name.given_name);
      });
    }
  }).render('#paypal-button-container10');
  //This function displays Smart Payment Buttons on your web page.

  function clodebu(){
	$("#myModal").hide();
  }

    function countinuefree(){
	var ajax_url = WEBSITE_URL+"/listproperty/transaction";
        var amount=$("#plan_amount2").val();
        var id=$("#plan_id").val();
        var number=$("#plan_number").val();
        var sku_id 	= $("#sku_id2").val();

        $.ajax({
	            url : ajax_url,
	            method:"post",
              data:{
                amount:amount,
                id:id,
                number:number,
                sku_id:sku_id,
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
			  },
			  beforeSend:function(){
	                startLoader('.swap_master_form');
	            },
	            complete:function(){
	               stopLoader('.swap_master_form'); 
	            },
	            success : function(data){
	            
	                if(data.status){
								$(".subscribe").hide();
								$("#myModal1").hide();
                                showScreen('step1');
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            }
	        });
  }
</script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
$(function() {
    var $form = $(".require-validation");
    $('form.require-validation').bind('submit', function(e) {
        var $form = $(".require-validation"),
            inputSelector = ['input[type=email]',
                            'input[type=password]',
                            'input[type=text]',
                            'input[type=file]',
                            'textarea'].join(', '),
            $inputs       = $form.find('.required').find(inputSelector),
            $errorMessage = $form.find('.inp-error'),
            valid         = true;
            $errorMessage.addClass('d-none');
        $('.has-error').removeClass('has-error');

        $inputs.each(function(i, el) {
            var $input = $(el);
            if ($input.val() === '') {
                $input.parent().addClass('has-error');
                $errorMessage.removeClass('d-none');
                e.preventDefault();
            }
        });

        if (!$form.data('cc-on-file')) {
            var StripeKey = "{{env('STRIPE_KEY')}}";

            e.preventDefault();
            Stripe.setPublishableKey(StripeKey);
            Stripe.createToken({
                number: $('.card-number').val(),
                cvc:    $('.card-cvc').val(),
                exp_month: $('.card-expiry-month').val(),
                exp_year: $('.card-expiry-year').val()
            }, stripeResponseHandler);
        }

    });
 	function stripeResponseHandler(status, response) {
         
        if (response.error) {
            $('.stripe-error').text(response.error.message);
        } else {

            var token = response['id'];
            $form.find('input[type=text]').empty(); 
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");

            var ajax_url    = WEBSITE_URL+"/listproperty/stripeorder";
            var amount      = $("#plan_amount2").val();
           
            $("#stipe_payment_btn").val(amount);
            var stipe_payment_btn = amount;
            var id          = $("#plan_id").val();
            var number      = $("#plan_number").val();
            var stripeToken       = token;
 
            $.ajax({
                type: 'post',
                url: ajax_url,
                data: {
                        amount:amount,
                        id:id,
                        number:number,
                        stipe_payment_btn:stipe_payment_btn,
                        stripeToken:token,
                      }, 
                      beforeSend:function(){
                  startLoader('.swap_master_form');
              },
              complete:function(){
                 stopLoader('.swap_master_form'); 
              },
                success: function (data) 
                {
                	 
                	console.log(data);
                      
                  if(data.status==1)
                  {

                   transaction_after_stripe();
                    

                  }
                     
                }
            });

            
        }
    } 



    

});

function stripePopup()
{
	var id  = $("#plan_id").val();
    var amount = $("#plan_amount2").val();
    var number = $("#plan_number").val();
    var sku_id 	= $("#sku_id2").val();

	var width = 800;
	var height = 700;
	var left = parseInt((screen.availWidth/2) - (width/2));
	var top = parseInt((screen.availHeight/2) - (height/2));
 
	var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,toolbar,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;
	myWindow = window.open(WEBSITE_URL+"/strip-payment-init?popup=true&plan_id="+id+"&plan_amount="+amount+"&plan_number="+number+"&sku_id="+sku_id,"subWind6Swap",windowFeatures);
	window.transaction_after_stripe = function ()
{
	    var ajax_url = WEBSITE_URL+"/listproperty/transaction";
        var amount=$("#plan_amount2").val();
        var id=$("#plan_id").val();
        var number=$("#plan_number").val();
         var sku_id 	= $("#sku_id2").val();
        $.ajax({
	            url : ajax_url,
	            method:"post",
              data:{
                amount:amount,
                id:id,
                number:number,
                 sku_id:sku_id,
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
			  },
			  beforeSend:function(){
	                startLoader('.swap_master_form');
	            },
	            complete:function(){
	               stopLoader('.swap_master_form'); 
	            },
	            success : function(data){
	            	console.log(data);
	                if(data.status){
                        Swal.fire({
	                        title: 'Success!',
	                        text: 'Transaction completed successfully',
	                        icon: 'success',
	                        confirmButtonText: 'Ok'
	                    }).then((result) => {
				            if(result.value){
								$("#StripeCardModal").hide();
								$(".subscribe").hide();
								$(".modal-backdrop").hide();
								$("body").removeClass('modal-open')
								$(".modal").hide();
								$("#myModal").hide();
                                showScreen('step1');
				            }
				        });
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            }
	        });
}
		
}

function stripePopupPremium(id,amount,sku_id='')
{
		//strip-payment-premium-init
		if(amount >0)
		{
			var id  	= id;
			var amount 	= amount;
    


	var width = 800;
	var height = 700;
	var left = parseInt((screen.availWidth/2) - (width/2));
	var top = parseInt((screen.availHeight/2) - (height/2));
 
	var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,toolbar,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;
	myWindow = window.open(WEBSITE_URL+"/strip-payment-premium-init?popup=true&plan_id="+id+"&plan_amount="+amount+"&sku_id="+sku_id,"subWind6SwapPrem",windowFeatures);
	window.premium_after_stripe = function ()
{
	    var ajax_url = WEBSITE_URL+"/list/premium?is_premium="+id+"&plan_amount="+amount+"&sku_id="+sku_id;
            $.ajax({
              url : ajax_url,
              method:"get",
              data:{ 
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}' 
              },
              beforeSend:function(){
                  startLoader('.swap_master_form');
              },
              complete:function(){
                 stopLoader('.swap_master_form'); 
              },
              success : function(data){
                  if(data.status){
                        Swal.fire({
                          title: 'Success!',
                          text: 'Transaction completed successfully',
                          icon: 'success',
                          confirmButtonText: 'Ok'
                      }).then((result) => {
                    if(result.value){
                       $(".package").hide();
                        $(".submit_step_8").trigger('click');
                    }
                });
                  }else{
                      Swal.fire({
                          title: 'Error!',
                          text: data.message,
                          icon: 'error',
                          confirmButtonText: 'Ok'
                      });
                  }
              }
          });
}
		}
	
}

/*
function transaction_after_stripe()
{
	    var ajax_url = WEBSITE_URL+"/listproperty/transaction";
        var amount=$("#plan_amount2").val();
        var id=$("#plan_id").val();
        var number=$("#plan_number").val();
        $.ajax({
	            url : ajax_url,
	            method:"post",
              data:{
                amount:amount,
                id:id,
                number:number,
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
			  },
			  beforeSend:function(){
	                startLoader('.swap_master_form');
	            },
	            complete:function(){
	               stopLoader('.swap_master_form'); 
	            },
	            success : function(data){
	            	console.log(data);
	                if(data.status){
                        Swal.fire({
	                        title: 'Success!',
	                        text: 'Transaction completed successfully',
	                        icon: 'success',
	                        confirmButtonText: 'Ok'
	                    }).then((result) => {
				            if(result.value){
								$("#StripeCardModal").hide();
								$(".subscribe").hide();
								$(".modal-backdrop").hide();
								$("body").removeClass('modal-open')
								$(".modal").hide();
								$("#myModal").hide();
                                showScreen('step0');
				            }
				        });
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            }
	        });
}*/
</script>
 
@endsection