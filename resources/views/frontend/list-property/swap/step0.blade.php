@php
				$current=date('Y-m-d');
$users = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','swap')->where('expiry_date','>=',$current )->count();
if($users){
    if(@$users > 0){
        $last_transaction = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','swap')->where('expiry_date','>=',$current)->latest()->first();
        if($last_transaction->count > $last_transaction->listed){
             
            $package="NO";
        }else{
            $package="YES";
        }
    }else{
        $package="YES";
    }
}else{
    $package="YES";
}

                    @endphp
<section class="buy-rent step0 swapandstay listPropertySteps" @if($package=='YES') style="display:none" @endif>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h4>Swap & Stay</h4>
                <p class="mb-5">Easily Swap Homes with others around the Globe. List your House and be a part of a global community.</p>
            </div>
        </div>
        <div class="row d-flex align-items-center">
            <div class="col-md-6">
                <img class="img-fluid" src="{{asset('img/Featured listing images1.png')}}">
            </div>
            <div class="col-md-6">
                <h5>List your Property for Swap.</h5>
                <p>List your property for swap with us. This is the initial step which let’s the other users see what you have to attract them for a swap with you.</p>
                <button class="red-btn rounded" onclick="showScreen('step1');" type="button">Get Started <img class="ml-2" src="{{asset('img/arrow-white.png')}}"></button>
            </div>
        </div>
        <div class="row d-flex align-items-center mt-5">
            <div class="col-md-6">
                <h5>Find your Desired Swap.</h5>
                <p>Choose from a wide range of properties available for swap. Choose the one you like, contact the owner and agree on terms for a swap.</p>
            </div>
            <div class="col-md-6">
                <img class="img-fluid" src="{{asset('img/Featured listing images1.png')}}">
            </div>
        </div>
        <div class="row d-flex align-items-center mt-5">
            <div class="col-md-6">
                <img class="img-fluid" src="{{asset('img/Featured listing images1.png')}}">
            </div>
            <div class="col-md-6">
                <h5>Swap Regularly or using Swap Credits.</h5>
                <p>You can opt for a regular swap or use Swap Credits earned to plan a trip & stay at your dream swap house.</p>
                <a href="javascript:;" onclick="showScreen('step1');" class="dp-inline-block" style="color: #E42627;">Learn More about Swap Credits</a>
            </div>
        </div>
    </div>
    <div class="modal1 skipp-page-cls">
        <div class="container">
            <div class="signup-right">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h5 class="mt-4">Different Ways to Swap</h5>
                        </div>
                        <div class="col col-md-6">
                            <div class="box-section-skip p-4">
                                <img style="height: 70px;" src="{{asset('img/regular swap.png')}}">
                                <h2>Regular Swap</h2>
                                <p class="mb-2 pb-3 text-left pl-0" style="font-family: 'Google Sans'">Regular Swap works by swapping homes with the other person. So, they come to your house & while you stay at their place.</p>
                                <a href="javascript:;" onclick="showScreen('step1');" style="color: #E4002B;">Swap FAQ’s</a>
                            </div>
                        </div>
                        <div class="col col-md-6">
                            <div class="box-section-skip p-4">
                                <img style="height: 70px;" src="{{asset('img/neighborhood.png')}}">
                                <h2>Credits Swap</h2>
                                <p class="mb-2 pb-3 text-left pl-0" style="font-family: 'Google Sans'">Earn Credits whenever someone stays at your house but you don’t at theirs. Use the credits to book stays around the globe.</p>
                                <a href="javascript:;" onclick="showScreen('step1');" style="color: #E4002B;">Learn More about Credit Swap</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>