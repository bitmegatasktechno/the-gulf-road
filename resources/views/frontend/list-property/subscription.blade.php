@php
$lang=Request::segment(1);
$loc=Request::segment(3);
$type=Request::segment(4);
if($type=='rent-sale'){
    $subscriptions = \App\Models\Subscription::where('type','rent')->orderBy('order','ASC')->get();
}
if($type=='coworkspace'){
  
    $subscriptions = \App\Models\Subscription::where('type','cospace')->orderBy('order','ASC')->get();
}
if($type=='swap'){
    $subscriptions = \App\Models\Subscription::where('type','swap')->orderBy('order','ASC')->get();
}
@endphp
@php
$current=date('Y-m-d');
if($type=='rent-sale'){
  $users = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','rent')->where('expiry_date','>=',$current )->latest()->first();
}elseif($type=='coworkspace'){
  $savedCOSpace = \App\Models\CoworkspaceProperty::where('user_id',\Auth::user()->id)->count();
  $users = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','cospace')->where('count','>',$savedCOSpace)->where('expiry_date','>=',$current )->latest()->first();
}elseif($type=='swap'){
  $users = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','swap')->where('expiry_date','>=',$current )->latest()->first();
}
if($users){
  if(@$users->count > $users->listed){
		$package="NO";
  }else{
    $package="YES";
  }
}else{
  $package="YES";
}

$link = $loc;
    $currency = 'USD';
    $currency_code = 'USD';
    if($link=='uae')
    {
        $currency = 'AED';
        $currency_code = 'AED';

    }
    elseif($link=='kuwait')
    {
         $currency = 'KD';
         $currency_code = 'KWD';
         //$currency_code = 'USD';// because in indian stripe account is not support kwd currency 
    }
    elseif($link=='ksa')
    {
         $currency = 'SAR';
         $currency_code = 'SAR';
    }
    elseif($link=='oman')
    {
         $currency = 'OR';
         $currency_code = 'OMR';
    }
    elseif($link=='qatar')
    {
        $currency = 'KD';
        $currency_code = 'QAR';
    }
    elseif($link=='kuwait')
    {
         $currency = 'QR';
         $currency_code = 'KWD';
    }
    elseif($link=='bahrain')
    {
         $currency = 'BD';
         $currency_code = 'BHD';
    }
    //$currency_code = 'INR';
@endphp



                    <style>
    ul{
      font-size:16px;
    }

   .subscriberadio .radio input[type="radio"] {
  position: absolute;
  opacity: 0;
}
.subscriberadio .radio input[type="radio"] + .radio-label:before {
  content: '';
  background: #f4f4f4;
  border-radius: 100%;
  border: 1px solid #b4b4b4;
  display: inline-block;
  width: 1.4em;
  height: 1.4em;
  position: relative;
  top: -0.2em;
  margin-right: 1em;
  vertical-align: top;
  cursor: pointer;
  text-align: center;
  -webkit-transition: all 250ms ease;
  transition: all 250ms ease;
}
.subscriberadio .radio input[type="radio"]:checked + .radio-label:before {
  background-color: #E4002B;
  box-shadow: inset 0 0 0 4px #f4f4f4;
}
.subscriberadio .radio input[type="radio"]:focus + .radio-label:before {
  outline: none;
  border-color: #E4002B;
}
.subscriberadio .radio input[type="radio"]:disabled + .radio-label:before {
  box-shadow: inset 0 0 0 4px #f4f4f4;
  border-color: #b4b4b4;
  background: #b4b4b4;
}
.subscriberadio .radio input[type="radio"] + .radio-label:empty:before {
  margin-right: 0;
}
div#ul-style ul {margin-top: 50px;}


    </style>
<section class="plans-and-pricing mt-5 bg-white subscribe listPropertySteps" @if($package=='NO') style="display:none" @endif>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h4 class="mb-4">Purchase one of the following plans to get started.</h4>
        <!-- <p class="mb-5">Enjoy unlimited property listing for 15 days then upgrade to suitable plan.</p> -->
      </div>
      <?php 
       


      $j=1; ?>
      @foreach($subscriptions as $key=>$subscription)
      <div class="col-md-4 box-design">
        <div class="price-box {{$key > 2 ? 'mt-2' : ''}}">
          <div class="pt-5 pb-4 pl-4 pr-4" id="ul-style">
          <div class="text-center">
            <img style="height: 100px;" class="img-fluid" src="{{asset('../uploads/subscription/'.@$subscription->logo)}}">
            <h5 >{{@$subscription->name}}</h5>
          </div>
          <!-- <p><img class="mr-2" src="{{asset('img/tick.png')}}">Valid for 1 months.</p>
          <p><img class="mr-2" src="{{asset('img/cross.png')}}">Property featured on Home page.</p> -->
          <?php echo @$subscription->content ?>
          <?php $i=1; ?>
          @foreach($subscription->sub_plan as $subscription_plan)

          @php

            


            $amount     = $subscription_plan['amount'];
            $plannumber = $subscription_plan['number'];
            $current_exchange = @Currency::convert()
              ->from('USD')
              ->to($currency_code)
              ->amount($amount)
              ->get();
              $current_exchange  = round($current_exchange);


              $sku_id = array('plan_id'=>$subscription->_id,'plan_amount'=>$current_exchange,'currency'=>$currency_code,'number'=>$plannumber);
              $sku_encoded = json_encode($sku_id);
              $sku_encoded_base = base64_encode($sku_encoded);

            @endphp

          
          <hr class="mt-5">
        <div class="row">
        <div class="col-md-2">
        <div class="subscriberadio">
        <div class="radio">
          <input id="radio-{{$i}}{{$j}}" name="radio" type="radio" onClick="selectPlan('{{@$subscription->_id}}',{{@$current_exchange}},{{@$subscription_plan['number']}},'{{$sku_encoded_base}}')">
          <label for="radio-{{$i}}{{$j}}" class="radio-label"></label>
        </div>
        </div>
        </div>
        <div class="col-md-10">
          <h3 class="" style="font-size: 14px;font-weight: normal;">Validity : {{@$subscription_plan['validity']}} days</h3>
          <h3 class="" style="font-size:14px;font-weight: normal;">Total Properties listed : {{@$subscription_plan['count']}}</h3>
          <h3 class="" style="font-size:14px;font-weight: normal;">Price :  {{@$currency_code}} {{@$current_exchange}}</h3>
         </div>
        </div>
        <?php $i++; ?>
          @endforeach
          <h5 class="text-center" style="font-size:16px;padding: 15px 10px;background-color: #E4002B;cursor: pointer;color: white;font-weight: 500;border-radius: 4px;width: 100%;" onClick="payPlan()">Continue with {{@$subscription->name}}</h5>
        </div>
        </div>
      </div>
      <?php $j++; ?>
      @endforeach
     
    </div>
    <input type="hidden" id="plan_id22">
    <input type="hidden" id="plan_amount22">
    <input type="hidden" id="plan_number22">
    <input type="hidden" id="sku_id2">
    <!-- Modal -->
  <div class="modal" id="myModal" role="dialog" style="display:none">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
          <h4 class="modal-title">Subscription Plans</h4>
        </div>
        <div class="modal-body">
            <input type="hidden" id="plan_id">
            <input type="hidden" id="plan_amount2">
            <input type="hidden" id="plan_number">
            
          <p><span>Plan Amount: {{$currency_code}} <span id="plan_amount"></span></span></p>
          <div id="paypal-button-container10"></div>
          <br>
          <!-- <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#StripeCardModal">Stripe - Pay Online</button> -->
          <button type="button" class="btn btn-danger btn-block"  onclick="stripePopup()">Stripe - Pay Online</button>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" onClick="$('#myModal').hide();" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
 <!--stripe popup -modal start-->
     
   


    <!--stripe popup -modal end-->
  <div class="modal" id="myModal1" role="dialog" style="display:none">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onClick="$('#myModal1').hide();">&times;</button>
          <h4 class="modal-title">Subscription Plans</h4>
        </div>
        <div class="modal-body">
            <input type="hidden" id="plan_id">
            <input type="hidden" id="plan_amount2">
            <input type="hidden" id="plan_number">
            <input type="hidden" id="sku_id">
          <p><span>Plan Amount: {{$currency_code}} 0</span></p>
          <button onClick="countinuefree()">Continue</button>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" onClick="$('#myModal1').hide();" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

     <div class="row mt-5">
                        <div class="col-md-5">
                            <a class="mt-2 dp-inline-block" href="{{url(app()->getLocale().'/listproperty/'.$loc)}}" >< Go back</a>
                        </div>
                    </div>
  </div>
</section>