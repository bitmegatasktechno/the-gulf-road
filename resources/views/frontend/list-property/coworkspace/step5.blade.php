<section class="buy-rent step5 add-coworking listPropertySteps" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>List your coworkspace.</h3>
                <p class="mb-5">Tell us about the workspace you are about to list.</p>
            </div>
            <div class="col-md-4">
                <ul class="list-inline">
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5>Title & Descreption</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Location</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Amenities</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Operating Hours & Days</h5>
                        </a>
                    </li>
                    <li class="active dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Contact Details</h5>
                            <p>Set the Opening & Closing Hours</p>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Pictures</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Pricing</h5>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-8">
                <div class="card list-coworking">
                    <div class="p-4">
                        <h4>Contact Details</h4>
                    </div>
                    <hr class="mt-0">
                    <div class="p-4 swap_div_form_5" id="swap_div_form_5">
                        <h5>Enter your contact setails</h5>
                        <p>This will let people get in touch with you.</p>
                        <div class="form-group">
                            <label>Email (Required)</label>
                            <input type="text" name="email" class="form-control" required>
                            <span class="invalid-feedback email"></span>
                        </div>
                        <div class="form-group">
                            <small class="full-width"><img class="mr-2" src="{{asset('img/info.png')}}"> All notifications will be sent to this email address.</small>
                        </div>
                        <div class="form-group">
                            <label>Contact Number (Required)</label>
                            <div class="row">
                                <div class="col-md-4" style="padding-right: 0px; ">
                                  <select name="phone_code" class="country-code-js form-control " style="height: 48px; font-size: 15px!important;" required>
                                    @foreach(getAllCodeWithName() as $row)
                                    <option value="{{ $row['dial_code'] }}">{{$row['name']}} {{$row['dial_code']}}</option>
                                    @endforeach
                                  </select>
                                </div>
                                <div class="col-md-8" style="padding-left: 0px">
                                  <div class="">
                                    <input type="number" name="contact_number" class="form-control" required>
                                  </div>
                                </div>
                              </div>
                            <span class="invalid-feedback contact_number"></span>
                        </div>
                        <div class="form-group">
                            <label>Website </label>
                            <input type="text" name="website_url" class="form-control">
                            <span class="invalid-feedback website_url"></span>
                        </div>
                        <h5>Social Media</h5>
                        <p>Add social media links of the workplace if any.</p>
                        <div class="form-group">
                            <label>Facebook </label>
                            <input type="text" name="facebook_url" class="form-control" placeholder="Please enter your social media link">
                            <span class="invalid-feedback facebook_url"></span>
                        </div>
                        <div class="form-group">
                            <label>Instagram </label>
                            <input type="text" name="instagram_url" class="form-control" placeholder="Please enter your social media link">
                            <span class="invalid-feedback instagram_url"></span>
                        </div>
                        <div class="form-group">
                            <label>Twitter</label>
                            <input type="text" name="twitter_url" class="form-control" placeholder="Please enter your social media link">
                            <span class="invalid-feedback twitter_url"></span>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a class="mt-2 dp-inline-block pt-3 pb-3 mt-5 mb-3" href="javascript:;" onclick="showScreen('step4');">< Go back</a>
                            </div>
                            <div class="col-md-6 text-right">
                                <button type="button" class="submit_step_5 red-btn rounded float-right pt-3 pb-3 mt-5 mb-3">
                                <span>Save & Continue</span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
