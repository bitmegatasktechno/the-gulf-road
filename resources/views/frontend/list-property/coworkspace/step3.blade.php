<section class="buy-rent step3 add-coworking listPropertySteps" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>List your coworkspace.</h3>
                <p class="mb-5">Tell us about the workspace You are about to list.</p>
            </div>
            <div class="col-md-4">
                <ul class="list-inline">
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5>Title & Descreption</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Location</h5>
                        </a>
                    </li>
                    <li class="active dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Amenities</h5>
                            <p>Where is your workspace located?</p>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Operating Hours & Days</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Contact Details</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Pictures</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Pricing</h5>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-8">
                <div class="card list-coworking swap_div_form_3" id="swap_div_form_3">
                    <div class="p-4">
                        <h4>Amenities</h4>
                    </div>
                    <hr class="mt-0">
                    <div class="p-4">
                        <h5>List  out the amenities that your workspace provides.</h5>
                        <p>This is an important step as it allows the users to see what amenities the space provides.</p>
                        {{--<h4>Special Amenities</h4>--}}
                        <div id="collapseOne1" class="collapse1 show" aria-labelledby="headingOne" data-parent="#accordion1">
                                   <!--  <div class="card-body">
                                        <div class="row">
                                        	@php
                                        		$food_id = \App\Models\AmenityCategory::where('name','food')->value('_id');
                                        	@endphp
                                            @foreach(getAllSpecialAmenities() as $row)
                                            	<div class="col-md-4 specialAmenitiesBlock" data-type="special" data-id="{{$row->_id}}">
	                                                <a href="javascript:;">
	                                                    <div class="blog-card mb-4 p-3">
	                                                        <span>
	                                                        	<img class="special_amenity_check" src="{{asset('img/ic_red_check.png')}}">
	                                                        </span>

	                                                        <img class="img-fluid d-block mx-auto" src="{{url('/uploads/specialamenities/'.$row->logo)}}" style="border:none">
	                                                        <strong style="text-align: center;display: inherit;margin-top: 10px;">{{\Str::limit($row->name, 15)}}</strong>
	                                                        <p style="font-size: 11px !important;text-align:center;">{{\Str::limit($row->description, 30)}}</p>
	                                                    </div>
	                                                </a>
	                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                 -->
                        <h4>All Accessibility</h4>
                        <p>Pick from the list of provided accessibility</p>
                        <div id="accordion">
                            @foreach(getAllAmentityCategory() as $key=>$category)
                            @if($category->amentities->count() > 0)
                            <div class="card">
                                <div class="card-header" id="heading{{$key}}">
                                    <h5 style="cursor: pointer;" class="mb-0" data-toggle="collapse" data-target="#collapse{{$key}}" aria-expanded="true" aria-controls="collapse{{$key}}">
	                                    {{$category->name}} <i class="fas fa-angle-down rotate-icon"></i>
                                    </h5>
                                </div>

                                <div id="collapse{{$key}}" class="collapse {{$key ? '' : 'show'}}" aria-labelledby="heading{{$key}}" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="row">
                                            @foreach($category->amentities as $row)
                                            	<div class="col-md-4 specialAmenitiesBlock" data-type="food" data-id="{{$row->_id}}">
	                                                <a href="javascript:;">
	                                                    <div class="blog-card mb-4 p-3">
	                                                        <span>
	                                                        	<img class="special_amenity_check" src="{{asset('img/ic_red_check.png')}}">
	                                                        </span>

	                                                        <img class="img-fluid d-block mx-auto" src="{{url('/uploads/specialamenities/'.$row->logo)}}" style="border:none">
	                                                        <strong style="text-align: center;display: inherit;margin-top: 10px;">{{\Str::limit($row->name, 15)}}</strong>
	                                                        <p >{{\Str::limit($row->description, 30)}}</p>
	                                                    </div>
	                                                </a>
	                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @endforeach
                        </div>
                    
                        <div class="row">
                            <div class="col-md-6">
                                <a class="mt-2 dp-inline-block pt-3 pb-3 mt-5 mb-3" href="javascript:;" onclick="showScreen('step2');">< Go back</a>
                            </div>
                            <div class="col-md-6 text-right">
                                <button type="button" class="submit_step_3 red-btn rounded float-right pt-3 pb-3 mt-5 mb-3">
                                <span>Save & Continue</span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>