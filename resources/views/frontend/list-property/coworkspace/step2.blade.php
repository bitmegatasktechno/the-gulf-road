<style>
    #map {
    height: 400px;
    width: 100%;
}

  .pac-container{
    margin-top: -62px;
  }

</style>
<section class="buy-rent step2 add-coworking listPropertySteps" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>List your coworkspace.</h3>
                <p class="mb-5">Tell us about the workspace You are about to list.</p>
            </div>
            <div class="col-md-4">
                <ul class="list-inline">
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5>Title & Description</h5>
                        </a>
                    </li>
                    <li class="active  dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Location</h5>
                            <p>Where is your workspace located</p>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Amenities</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Operating Hours & Days</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Contact Details</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Pictures</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Pricing</h5>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-8">
                <div class="card list-coworking">
                    <div class="p-4">
                        <h4>Location</h4>
                    </div>
                    <hr class="mt-0">
                    <div class="p-4 swap_div_form_2" id="swap_div_form_2">
                      <!--   <h5>Let’s find your location on the map.
</h5>
                        <p>Type your coworking space address or street address below.</p> -->
                        <div class="form-group">
                            <input class="form-control" type="text" onChange="getLocation()" id="address"  placeholder="Enter coworking space or street address." name="address"/>
                            
                        <span class="invalid-feedback address"></span>
                           <div class="row"  hidden>
                               <div class="col-md-6" hidden>
                                    
                                        <input  hidden type="text"  name="latitude" id="lat"  readonly required>
                                    
                                </div>
                                <div class="col-md-6">
                                  <input  hidden type="text" name="longitude" id="lng" readonly required>
                                </div>
                            </div>
                        </div>
                        <div id="map"></div>
                        
                        <h5 class="mt-3">Add the complete details</h5>
                        <p>Enter the complete address below.</p>

                        <div>
                            <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                     
                                        <label>Location Name</label>
                                        <input type="text" name="location_name" class="form-control"><span class="invalid-feedback location_name"></span>
                                    </div>
                                </div> 

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Unit/Floor No.</label>
                                        <input type="text" name="unit_number" class="form-control"><span class="invalid-feedback unit_number"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Address Line 1</label>
                                        <input type="text" name="address_line_1" class="form-control"><span class="invalid-feedback address_line_1"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Address Line 2</label>
                                        <input type="text" name="address_line_2" class="form-control"><span class="invalid-feedback address_line_2"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Landmark</label>
                                        <input type="text" name="landmark" class="form-control"><span class="invalid-feedback landmark"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    {{-- <div class="form-group">
                                        <label>Town/City</label>
                                        <input type="text" name="city" id="add_city" class="form-control"><span class="invalid-feedback city"></span>
                                    </div> --}}
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Country</label>
                                        <!-- <input type="text" name="country" id="add_country" class="form-control"><span class="invalid-feedback country"></span> -->
                                        <select class="form-control " name="country" data-change="native_state" onchange="setCities(this)">
                                            <option value="">Select Country</option>
                                            @foreach(getAllLocations() as $country)
                                                <option value="{{ $country->name }}">{{ __(ucfirst($country->name)) }}</option>
                                            @endforeach
                                        </select><span class="invalid-feedback country"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                         <label>City</label>
                                        <select class="form-control" name="city" id="countryCity" required>
                                            <option value="">Select City</option>
                                        </select><span class="invalid-feedback city"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Zip Code</label>
                                        <input type="text" name="zip_code" id="add_zip" class="form-control"><span class="invalid-feedback zip_code"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a class="mt-2 dp-inline-block pt-3 pb-3 mt-5 mb-3" href="javascript:;" onclick="showScreen('step1');">< Go back</a>
                            </div>
                            <div class="col-md-6 text-right">
                                <button type="button" class="submit_step_2 red-btn rounded float-right pt-3 pb-3 mt-5 mb-3">
                                <span>Save & Continue</span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<table id="address" style="display:none">
      <tr>
        <td class="label">Street address</td>
        <td class="slimField">
          <input class="field" id="street_number" disabled="true" />
        </td>
        <td class="wideField" colspan="2">
          <input class="field" id="route" disabled="true" />
        </td>
      </tr>
      <tr>
        <td class="label">City</td>
        <td class="wideField" colspan="3">
          <input class="field" id="locality" disabled="true" />
        </td>
      </tr>
      <tr>
        <td class="label">State</td>
        <td class="slimField">
          <input
            class="field"
            id="administrative_area_level_1"
            disabled="true"
          />
        </td>
        <td class="label">Zip code</td>
        <td class="wideField">
          <input class="field" id="postal_code" disabled="true" />
        </td>
      </tr>
      <tr>
        <td class="label">Country</td>
        <td class="wideField" colspan="3">
          <input class="field" id="country" disabled="true" />
        </td>
      </tr>
    </table>



@if(env('APP_ACTIVE_MAP') =='patel_map')
<style>
    #address_result
    {
        z-index: 9;
        left: unset !important;
        top:unset !important;
        width:  100% !important;
    }


</style>
   <script src="https://mapapi.cloud.huawei.com/mapjs/v1/api/js?callback=initMap&key={{env('PATEL_MAP_KEY')}}"></script>
    <script>
        function initMap() {
            var mapOptions = {};



             
            var myLatLng = {
                        lat: (-25.363),
                        lng: (131.044)
                                };
            mapOptions.center = myLatLng;
            mapOptions.zoom = 6;
            mapOptions.language='ENG';
            var searchBoxInput = document.getElementById('address');
             var map = new HWMapJsSDK.HWMap(document.getElementById('map'), mapOptions);
            // Create the search box parameter.
            var acOptions = {
                location: {
                    lat: 48.856613,
                    lng: 2.352222
                },
                radius: 10000,
                customHandler: callback,
                language:'en'
            };
            var autocomplete;
            // Obtain the input element.
            // Create the HWAutocomplete object and set a listener.
            autocomplete = new HWMapJsSDK.HWAutocomplete(searchBoxInput, acOptions);
            autocomplete.addListener('site_changed', printSites);
            // Process the search result. 
            // index: index of a searched place.
            // data: details of a searched place. 
            // name: search keyword, which is in strong tags. 
            function callback(index, data, name) {
                
                var div
                div = document.createElement('div');
                div.innerHTML = name;
                return div;
            }
            // Listener.
            function printSites() {
                 
                
                var site = autocomplete.getSite();
                var latitude = site.location.lat;
                var longitude = site.location.lng;
                 var country_name = site.address.country;
                
                var results = "Autocomplete Results:\n";
                var str = "Name=" + site.name + "\n"
                    + "SiteID=" + site.siteId + "\n"
                    + "Latitude=" + site.location.lat + "\n"
                    + "Longitude=" + site.location.lng + "\n"
                    + "Address=" + site.formatAddress + "\n";

                    
                results = results + str;

                 


            $('#lat').val(latitude);
            $('#lng').val(longitude);

            $('select[name=country]').val(country_name);
            $( "select[name=country]" ).trigger( "change" );
                  
                console.log(latitude, longitude);
                /*console.log(results);*/




                  console.log(latitude, longitude);
                document.getElementById('map').innerHTML='';
                /*start map zoom creation */
                /*console.log(results);*/
                var myLatLng = {
                  lat: latitude,
                  lng: longitude
                };

                var mapOptions = {};
                mapOptions.center = myLatLng;
                mapOptions.zoom = 5; 
                var map = new HWMapJsSDK.HWMap(document.getElementById('map'), mapOptions);
                


                mMarker = new HWMapJsSDK.HWMarker({
                              map: map,
                              position: myLatLng,
                              zIndex: 10,
                              label: {
                                text    : country_name,
                                offsetY : -30,
                                fontSize: '20px'
                              },
                              icon: {
                                opacity:1,
                                scale: 1.2,
                              }
                          });

                map.zoomIn();


                /*end map zoom creation */
            }

           

            
        }
        function getLocation()
        {

        }
    </script> 

     
    @elseif(env('APP_ACTIVE_MAP')=='google_map')
<script
      src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&callback=initMap&libraries=places&v=weekly"
      defer
    ></script>

     <script>
 

function initMap() {
  const myLatLng = {
    lat: -25.363,
    lng: 131.044
  };
    
      

  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 4,
    center: myLatLng
  });
    var input = document.getElementById('address');

 map.controls[google.maps.ControlPosition.TOP_RIGHT].push();

var autocomplete = new google.maps.places.Autocomplete(input);
autocomplete.bindTo('bounds', map);
autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);
            var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29),
          title: "Hello World!"
        });
        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindow.open(map, marker);
        });
  new google.maps.Marker({
    position: myLatLng,
    map,
    title: "Hello World!"
  });
}
function getLocation()
 {
//   alert(address);
  var address = $('#address').val();
//   alert(address);
  var geocoder = new google.maps.Geocoder();

  geocoder.geocode( { 'address': address}, function(results, status)
  {


    if (status == google.maps.GeocoderStatus.OK)
    {
      /*console.log(results[0],"afdcfsa");
      console.log(results[0].address_components);*/
      const countryObj=results[0].address_components.filter(val=>{
            if(val.types.includes('country')){
                return true
            }
            return false;
      });
    /* console.log(countryObj,'the country obj');*/


     var country_name = countryObj[0].long_name;
     /*console.log(country_name,'name');*/
      var latitude = results[0].geometry.location.lat();

      var longitude = results[0].geometry.location.lng();
      /* alert(latitude +"-----"+ longitude);*/
      $('#lat').val(latitude);
      $('#lng').val(longitude);
     /*$('#countryTo').val(country_name);*/
     /*$('#country_name').val(country_name);*/
    /*console.log(latitude, longitude);*/
    }
  });

}
</script>
@endif