<style type="text/css">
    .ui-timepicker-container.ui-timepicker-no-scrollbar.ui-timepicker-standard{
        margin-top: -64px;
    }
</style>
<section class="buy-rent step4 add-coworking listPropertySteps" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>List your coworkspace.</h3>
                <p class="mb-5">Tell us about the workspace You are about to list.</p>
            </div>
            <div class="col-md-4">
                <ul class="list-inline">
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5>Title & Descreption</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Location</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Amenities</h5>
                        </a>
                    </li>
                    <li class="active dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Operating Hours & Days</h5>
                            <p>Set the opening and closing hours</p>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Contact Details</h5>
                        </a>
                    </li>
                    
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Pictures</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Pricing</h5>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-8">
                <div class="card list-coworking">
                    <div class="p-4">
                        <h4>Operating Hours & Days</h4>
                    </div>
                    <hr class="mt-0">
                    <div class="p-4 swap_div_form_4" id="swap_div_form_4">
                        <h5>Operating hours & Working days</h5>
                        <p class="mb-4">Set the operating hours & working days of your coworkspace.</p>
                        <div class="row dp-flex align-items-center">
                            <div class="col-md-6">
                                <h3>Monday</h3>
                            </div>
                            <div class="col-md-6 text-right">
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <input class="visibly-hidden operating_radio closed_input" id="monday_closed_radio" onClick="monday_close()" type="radio"  name="monday" value="closed" data-id="operating_monday_div">
                                    <label class="radio-label" for="monday_closed_radio" id="monday_close" onClick="monday_close()">Closed</label>

                                    <input class="visibly-hidden operating_radio open_input" id="monday_open_radio" onClick="monday_open()" type="radio" name="monday" value="open" data-id="operating_monday_div" checked>
                                    <label class="radio-label" for="monday_open_radio" id="monday_open" onClick="monday_open()">Open</label>

                                </div>
                                <span class="invalid-feedback monday"></span>
                            </div>

                            <div class="row">
                                <div class="col-md-5 mt-2 mb-3 operating_monday_div">
                                <input class="form-control timepicker" type="text" name="monday_start_time" id="monday_start_time">
                                <span class="invalid-feedback monday_start_time"></span>
                            </div>
                            <div class="col-md-5 mt-2 mb-3 operating_monday_div">
                                <input class="form-control timepicker" type="text" name="monday_end_time" id="monday_end_time">
                                <span class="invalid-feedback monday_end_time"></span>
                            </div></div>
                        </div>
                        <hr>
                        <div class="row dp-flex align-items-center">
                            <div class="col-md-6">
                                <h3>Tuesday</h3>
                            </div>
                            <div class="col-md-6 text-right">
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <input class="visibly-hidden operating_radio" id="tuesday_closed_radio" onClick="tuesday_close()" type="radio" name="tuesday" value="closed" data-id="operating_tuesday_div">
                                    <label class="radio-label" for="tuesday_closed_radio" id="tuesday_close" onClick="tuesday_close()">Closed</label>

                                    <input class="visibly-hidden operating_radio" id="tuesday_open_radio" onClick="tuesday_open()" type="radio" name="tuesday" value="open" checked data-id="operating_tuesday_div">
                                    <label class="radio-label" for="tuesday_open_radio" id="tuesday_open" onClick="tuesday_open()">Open</label>
                                </div><span class="invalid-feedback tuesday"></span>
                            </div>
                            <div class="row">
                            <div class="col-md-5 mt-2 mb-3 operating_tuesday_div">
                                <input class="form-control timepicker" type="text" name="tuesday_start_time" id="tuesday_start_time">
                                <span class="invalid-feedback tuesday_start_time"></span>
                            </div>
                            <div class="col-md-5 mt-2 mb-3 operating_tuesday_div">
                                <input class="form-control timepicker" type="text" name="tuesday_end_time" id="tuesday_end_time">
                                <span class="invalid-feedback tuesday_end_time"></span>
                            </div></div>
                        </div>
                        <hr>
                        <div class="row dp-flex align-items-center">
                            <div class="col-md-6">
                                <h3>Wednesday</h3>
                            </div>
                            <div class="col-md-6 text-right">
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <input class="visibly-hidden operating_radio" id="wednesday_closed_radio" onClick="wed_close()" type="radio" name="wednesday" value="closed" data-id="operating_wednesday_div">
                                    <label class="radio-label" for="wednesday_closed_radio" id="wed_close" onClick="wed_close()">Closed</label>

                                    <input class="visibly-hidden operating_radio" id="wednesday_open_radio" onClick="wed_open()" type="radio" name="wednesday" value="open" checked data-id="operating_wednesday_div">
                                    <label class="radio-label" for="wednesday_open_radio" id="wed_open" onClick="wed_open()">Open</label>
                                </div><span class="invalid-feedback wednesday"></span>
                            </div>
                            <div class="row">
                            <div class="col-md-5 mt-2 mb-3 operating_wednesday_div">
                                <input class="form-control timepicker" type="text" name="wednesday_start_time" id="wednesday_start_time">
                                <span class="invalid-feedback wednesday_start_time"></span>
                            </div>
                            <div class="col-md-5 mt-2 mb-3 operating_wednesday_div">
                                <input class="form-control timepicker" type="text" name="wednesday_end_time" id="wednesday_end_time">
                                <span class="invalid-feedback wednesday_end_time"></span>
                            </div></div>
                        </div>
                        <hr>
                        <div class="row dp-flex align-items-center">
                            <div class="col-md-6">
                                <h3>Thursday</h3>
                            </div>

                            <div class="col-md-6 text-right">
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <input class="visibly-hidden operating_radio" id="thursday_closed_radio" type="radio" onClick="thu_close()" name="thursday" value="closed" data-id="thursday_operating_div">
                                    <label class="radio-label" for="thursday_closed_radio" id="thu_close" onClick="thu_close()">Closed</label>

                                    <input class="visibly-hidden operating_radio" id="thursday_open_radio" type="radio" onClick="thu_open()" name="thursday" value="open" checked data-id="thursday_operating_div">
                                    <label class="radio-label" for="thursday_open_radio" id="thu_open" onClick="thu_open()">Open</label>
                                </div><span class="invalid-feedback thursday"></span>
                            </div>
                            <div class="row">
                            <div class="col-md-5 mt-2 mb-3 thursday_operating_div">
                                <input class="form-control timepicker" type="text" name="thursday_start_time" id="thursday_start_time">
                                <span class="invalid-feedback thursday_start_time"></span>
                            </div>
                            <div class="col-md-5 mt-2 mb-3 thursday_operating_div">
                                <input class="form-control timepicker" type="text" name="thursday_end_time" id="thursday_end_time">
                                <span class="invalid-feedback thursday_end_time"></span>
                            </div></div>
                        </div>
                        <hr>
                        <div class="row dp-flex align-items-center">
                            <div class="col-md-6">
                                <h3>Friday</h3>
                            </div>
                            <div class="col-md-6 text-right">
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <input class="visibly-hidden operating_radio" id="friday_closed_radio" type="radio" onClick="friday_close()" name="friday" value="closed" data-id="friday_operating_div">
                                    <label class="radio-label" for="friday_closed_radio" id="friday_close" onClick="friday_close()">Closed</label>

                                    <input class="visibly-hidden operating_radio" id="friday_open_radio" type="radio" onClick="friday_open()" name="friday" value="open" checked data-id="friday_operating_div">
                                    <label class="radio-label" for="friday_open_radio" id="friday_open" onClick="friday_open()">Open</label>
                                </div><span class="invalid-feedback friday"></span>
                            </div>
                            <div class="row">
                            <div class="col-md-5 mt-2 mb-3 friday_operating_div">
                                <input class="form-control timepicker" type="text" name="friday_start_time" id="friday_start_time">
                                <span class="invalid-feedback friday_start_time"></span>
                            </div>
                            <div class="col-md-5 mt-2 mb-3 friday_operating_div">
                                <input class="form-control timepicker" type="text" name="friday_end_time" id="friday_end_time">
                                <span class="invalid-feedback friday_end_time"></span>
                            </div>
                        </div>
                        </div>
                        <hr>
                        <div class="row dp-flex align-items-center">
                            <div class="col-md-6">
                                <h3>Saturday</h3>
                            </div>
                            <div class="col-md-6 text-right">
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <input class="visibly-hidden operating_radio" id="saturday_closed_radio" type="radio" onClick="sat_close()" name="saturday" value="closed" data-id="saturday_operating_div">
                                    <label class="radio-label" for="saturday_closed_radio" id="sat_close" onClick="sat_close()">Closed</label>

                                    <input class="visibly-hidden operating_radio" id="saturday_open_radio" onClick="sat_open()" type="radio" name="saturday" value="open" checked data-id="saturday_operating_div">
                                    <label class="radio-label" for="saturday_open_radio" id="sat_open" onClick="sat_open()">Open</label>
                                </div><span class="invalid-feedback saturday"></span>
                            </div>
                            <div class="row">
                            <div class="col-md-5 mt-2 mb-3 saturday_operating_div">
                                <input class="form-control timepicker" type="text" name="saturday_start_time" id="saturday_start_time">
                                <span class="invalid-feedback saturday_start_time"></span>
                            </div>
                            <div class="col-md-5 mt-2 mb-3 saturday_operating_div">
                                <input class="form-control timepicker" type="text" name="saturday_end_time" id="saturday_end_time">
                                <span class="invalid-feedback saturday_end_time"></span>
                            </div>
                        </div>
                        </div>
                        <hr>
                        <div class="row dp-flex align-items-center">
                            <div class="col-md-6">
                                <h3>Sunday</h3>
                            </div>
                            <div class="col-md-6 text-right">
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <input class="visibly-hidden operating_radio" id="sunday_closed_radio" type="radio" onClick="sunday_close()" name="sunday" value="closed" data-id="sunday_operating_div">
                                    <label class="radio-label" for="sunday_closed_radio" id="sunday_close" onClick="sunday_close()">Closed</label>

                                    <input class="visibly-hidden operating_radio" id="sunday_open_radio" type="radio" onClick="sunday_open()" name="sunday" value="open" checked data-id="sunday_operating_div">
                                    <label class="radio-label" for="sunday_open_radio" id="sunday_open" onClick="sunday_open()">Open</label>
                                </div><span class="invalid-feedback sunday"></span>
                            </div>
                            <div class="row">
                            <div class="col-md-5 mt-2 mb-3 sunday_operating_div">
                                <input class="form-control timepicker" type="text" name="sunday_start_time" id="sunday_start_time">
                                <span class="invalid-feedback sunday_start_time"></span>
                            </div>
                            <div class="col-md-5 mt-2 mb-3 sunday_operating_div">
                                <input class="form-control timepicker" type="text" name="sunday_end_time" id="sunday_end_time">
                                <span class="invalid-feedback sunday_end_time"></span>
                            </div>
                        </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <a class="mt-2 dp-inline-block pt-3 pb-3 mt-5 mb-3" href="javascript:;" onclick="showScreen('step3');">< Go back</a>
                            </div>
                            <div class="col-md-6 text-right">
                                <button type="button" class="submit_step_4 red-btn rounded float-right pt-3 pb-3 mt-5 mb-3">
                                <span>Save & Continue</span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
