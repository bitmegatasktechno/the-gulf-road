@php

$currency = 'USD';
$currency_code = 'USD';
if($link=='uae')
{
    $currency = 'AED';
    $currency_code = 'AED';

}
elseif($link=='kuwait')
{
     $currency = 'KD';
     $currency_code = 'KWD';
}
elseif($link=='ksa')
{
     $currency = 'SAR';
     $currency_code = 'SAR';
}
elseif($link=='oman')
{
     $currency = 'OR';
     $currency_code = 'OMR';
}
elseif($link=='qatar')
{
    $currency = 'KD';
    $currency_code = 'QAR';
}
elseif($link=='kuwait')
{
     $currency = 'QR';
     $currency_code = 'KWD';
}
elseif($link=='bahrain')
{
     $currency = 'BD';
     $currency_code = 'BHD';
}

@endphp
 
<section class="buy-rent step7 add-coworking listPropertySteps" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>List your coworkspace.</h3>
                <p class="mb-5">Tell us about the workspace You are about to list.</p>
            </div>
            <div class="col-md-4">
                <ul class="list-inline">
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5>Title & Descreption</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Location</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Amenities</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Operating Hours & Days</h5>
                        </a>
                    </li>
                    <li class=" dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Contact Details</h5>
                        </a>
                    </li>
                    
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Pictures</h5>
                        </a>
                    </li>
                    <li class="activedp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Pricing</h5>
                            <p>Set Coworkspace type & prices</p>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-8">
                <div class="card list-coworking">
                    <div class="p-4">
                        <h4>Pricing</h4>
                    </div>
                    <hr class="mt-0">
                    <div class="p-4 swap_div_form_7" id="swap_div_form_7">
                        <h5>Set Pricing For Coworkspace</h5>
                        <p>Set the type of workspace available & pricing.</p>
                        
                        <div class="sale_price_list">
                            <div class="row mt-4">                                           
                                    <div class="col-md-11">
                                        <div class="form-group">
                                            <label>Currency</label>
                                            <select class="form-control rounded" name="currency_type" id="currency_type" onchange="change_currency_type(this)">
                                                    <option value="{{$currency}}">{{$currency}}</option>
                                                    <option hidden value="USD">USD</option>
                                            </select>
                                            <span class="invalid-feedback open_space_seats"></span>
                                        </div>
                                    </div>
                                </div>
	                        <div class="row mt-3">
                                <div class="col-md-10">
                                    <h5>Open Workspace</h5>
                                </div>
                                <div class="col-md-2">
                                    <div class="checkbox switcher">
                                        <a href="javascript:;">
                                        <label for="open_workspace_available">
                                            <input type="checkbox" checked="" name="open_workspace_available" id="open_workspace_available" value="checked" onchange="openWorkspaceAvailable('open_workspace_available_div');">
                                            <span><small></small></span>
                                        </label></a>
                                    </div>
                                </div>
                            </div>
	                        <div id="open_workspace_available_div">
                                <div class="row mt-4">                                           
                                    <div class="col-md-11">
                                        <div class="form-group">
                                            <label>Number of Seats</label>
                                            <select class="form-control" name="open_space_seats">
                                                <option value="">Select</option>
                                                @foreach(range(1,10) as $row)
                                                    <option value="{{$row}}">{{$row}} People</option>
                                                @endforeach
                                                <option value="10+">Above 10 People</option>
                                                
                                            </select>
                                            <span class="invalid-feedback open_space_seats"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>
                                        Price
                                    </h5>
                                </div>

                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Duration</label>
                                        <div class="listspace-price-tab-duration">
                                            1 Day
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label>Price</label>
                                            <input class="form-control" type="number" min="0" name="open_space_daily_price" placeholder="Price">
                                            <span class="invalid-feedback open_space_daily_price"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Currency</label>
                                            <input type="text" class="form-control currency_type" autocomplete=off  name="open_space_daily_currency" value="{{$currency}}" readonly>
                                            <span class="invalid-feedback open_space_daily_currency"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="listspace-price-tab-duration">
                                            1 Week
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <input class="form-control" type="number" min="0" name="open_space_weekly_price" placeholder="Price">
                                            <span class="invalid-feedback open_space_weekly_price"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <input type="text" class="form-control currency_type" autocomplete=off  name="open_space_weekly_currency" value="{{$currency}}" readonly>
                                            <span class="invalid-feedback open_space_weekly_currency"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="listspace-price-tab-duration">
                                            1 Month
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <input class="form-control" type="number" min="0" name="open_space_monthly_price" min="0" placeholder="Price">
                                            <span class="invalid-feedback open_space_monthly_price"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <input type="text" class="form-control currency_type" autocomplete=off  name="open_space_monthly_currency" value="{{$currency}}" readonly>
                                            <span class="invalid-feedback open_space_monthly_currency"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
	                    </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="sale_price_list">
                            <div class="row mt-3">
                                <div class="col-md-10">
                                    <h5>Dedicated Desk</h5>
                                </div>
                                <div class="col-md-2">
                                    <div class="checkbox switcher">
                                        <a href="javascript:;">
                                        <label for="dedicated_desk_available">
                                            <input type="checkbox" checked="" name="dedicated_desk_available" id="dedicated_desk_available" value="checked" onchange="dedicatedDeskAvailable('dedicated_desk_available_div');">
                                            <span><small></small></span>
                                        </label></a>
                                    </div>
                                </div>
                            </div>
                            <div id="dedicated_desk_available_div">
                                <div class="row  mt-4">                                           
                                    <div class="col-md-11">
                                        <div class="form-group">
                                            <label>Number of Seats</label>
                                            <select class="form-control" name="dedicated_space_seats">
                                                <option value="">Select</option>
                                                @foreach(range(1,10) as $row)
                                                    <option value="{{$row}}">{{$row}} People</option>
                                                @endforeach
                                                <option value="10+">Above 10 People</option>
                                                
                                            </select>
                                            <span class="invalid-feedback dedicated_space_seats"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h5>
                                        Price
                                    </h5>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Duration</label>
                                        <div class="listspace-price-tab-duration">
                                            1 Day
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label>Price</label>
                                            <input class="form-control" type="number" min="0" name="dedicated_space_daily_price" placeholder="Price">
                                            <span class="invalid-feedback dedicated_space_daily_price"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Currency</label>
                                            <input type="text" class="form-control currency_type"  name="dedicated_space_daily_currency" value="{{$currency}}" readonly>
                                            <span class="invalid-feedback dedicated_space_daily_currency"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="listspace-price-tab-duration">
                                            1 Week
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <input class="form-control" type="number" min="0" name="dedicated_space_weekly_price" placeholder="Price">
                                            <span class="invalid-feedback dedicated_space_weekly_price"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <input type="text" class="form-control currency_type"  name="dedicated_space_weekly_currency" value="{{$currency}}" readonly>
                                            <span class="invalid-feedback dedicated_space_weekly_currency"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="listspace-price-tab-duration">
                                            1 Month
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <input class="form-control" type="number" min="0" name="dedicated_space_monthly_price" placeholder="Price">
                                            <span class="invalid-feedback dedicated_space_monthly_price"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <input type="text" class="form-control currency_type"  name="dedicated_space_monthly_currency" value="{{$currency}}" readonly>
                                            <span class="invalid-feedback dedicated_space_monthly_currency"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="sale_price_list">
                            <div class="row mt-3">
                                <div class="col-md-10">
                                    <h5>Private Office / Meeting Room</h5>
                                </div>
                                <div class="col-md-2">
                                    <div class="checkbox switcher">
                                        <a href="javascript:;">
                                        <label for="private_office_available">
                                            <input type="checkbox" checked="" name="private_office_available" id="private_office_available" value="checked" onchange="privateOfficeAvailable('private_office_available_div');">
                                            <span><small></small></span>
                                        </label></a>
                                    </div>
                                </div>
                            </div>
                            <div id="private_office_available_div" class="mt-4">
                                <div class="row private-offices-panel-main">
                                    <div class="col-md-12">

                                        <div class="row mt-4">                              
                                            <div class="col-md-11">
                                                <div class="form-group">
                                                    <label>Number of Offices</label>
                                                    <select class="form-control" name="no_of_private_office">
                                                        <option value="">Select</option>
                                                        @foreach(range(1,10) as $row)
                                                            <option value="{{$row}}">{{$row}}</option>
                                                        @endforeach
                                                        <option value="10+">Above 10 </option>
                                                        
                                                    </select>
                                                    <span class="invalid-feedback no_of_private_office"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">                                           
                                            <div class="col-md-11">
                                                <div class="form-group">
                                                    <label>Number of Seats</label>
                                                    <select class="form-control" name="seating_capacity">
                                                        <option value="">Select</option>
                                                        @foreach(range(1,10) as $row)
                                                            <option value="{{$row}}">{{$row}} People</option>
                                                        @endforeach
                                                        <option value="10+">Above 10 People</option>
                                                        
                                                    </select>
                                                    <span class="invalid-feedback seating_capacity"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group mt-4">
                                            <h5>
                                                Price
                                            </h5>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label>Duration</label>
                                                <div class="listspace-price-tab-duration">
                                                    1 Day
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="form-group">
                                                    <label>Price</label>
                                                    <input class="form-control" min="0" type="number" name="private_space_daily_price" placeholder="Price">
                                                    <span class="invalid-feedback private_space_daily_price"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Currency</label>
                                                    <input type="text" class="form-control currency_type"  name="private_space_daily_currency" value="{{$currency}}" readonly>
                                                    <span class="invalid-feedback private_space_daily_currency"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="listspace-price-tab-duration">
                                                    1 Week
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="form-group">
                                                    <input class="form-control" type="number" min="0" name="private_space_weekly_price" placeholder="Price">
                                                    <span class="invalid-feedback private_space_weekly_price"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <input type="text" class="form-control currency_type"  name="private_space_weekly_currency" value="{{$currency}}" readonly>
                                                    <span class="invalid-feedback private_space_weekly_currency"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="listspace-price-tab-duration">
                                                    1 Month
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="form-group">
                                                    <input class="form-control" type="number" min="0" name="private_space_monthly_price" placeholder="Price">
                                                    <span class="invalid-feedback private_space_monthly_price"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <input type="text" class="form-control currency_type"  name="private_space_monthly_currency" value="{{$currency}}" readonly>
                                                    <span class="invalid-feedback private_space_monthly_currency"></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row mt-4">
                                            <div class="col-md-6">
                                                <h5>Does the office have a whiteboard ?</h5>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="checkbox switcher">
                                                    <a href="javascript:;">
                                                    <label for="white_board_available">
                                                        <input type="checkbox" name="white_board_available" id="white_board_available" value="checked">
                                                        <span><small></small></span>
                                                    </label></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <button type="button" class="red-btn rounded pt-3 pb-3 mt-5 mb-3" onclick="addPrivateOffice();">
                                <span>Add More</span></button> -->
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <a class="mt-2 dp-inline-block pt-3 pb-3 mt-5 mb-3" href="javascript:;" onclick="showScreen('step6');">< Go back</a>
                            </div>
                            <div class="col-md-6 text-right">
                                <button type="button" class="submit_step_7 red-btn rounded float-right pt-3 pb-3 mt-5 mb-3">
                                <span>Submit</span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    function change_currency_type(obj)
    {
        var currency = $(obj).val();
        $('.currency_type').val(currency);
    }
</script>
