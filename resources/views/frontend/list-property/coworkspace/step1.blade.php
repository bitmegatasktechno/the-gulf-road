<section class="buy-rent step1 add-coworking listPropertySteps" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>List your coworkspace.</h3>
                <p class="mb-4">Tell us about the workspace You are about to list.</p>
            </div>
            <div class="col-md-4">
            <input type="hidden" name="loc" value="{{$loc}}">
                <ul class="list-inline">
                    <li class="active">
                        <a href="javascript:;">
                            <h5>Title & Description</h5>
                            <p>Give your listing a title & Describe your workspace.</p>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Location</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Amenities</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Operating Hours & Days</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Contact Details</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Pictures</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Pricing</h5>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-8">
                <div class="card list-coworking">
                    <div class="p-4">
                        <h4>Title & Description</h4>
                    </div>
                    <hr class="mt-0">
                    <div class="p-4 swap_div_form_1" id="swap_div_form_1">
                        <h5>Title</h5>
                        <p>Give your workspace a name so that people can find it.</p>
                        <div>
                        	<input type="text" name="title" class="full-width rounded p-3 brdr form-control" placeholder="“The Ninja Workspace…”">
                        	<span class="invalid-feedback title"></span>
                        </div>
                        <h5 class="mt-4 mb-3">Describe your workspace.</h5>
                        <div>
                        	<textarea name="description" class="full-width rounded p-3 brdr form-control" rows="5" placeholder="“The workpsace is located in NY and i 5 minutes away from the nearest metro station…”"></textarea>
                        	<span class="invalid-feedback description"></span>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <button type="button" class="submit_step_1 red-btn rounded float-right pt-3 pb-3 mt-5 mb-3">
                                <span>Save & Continue</span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
