@php
$lang=Request::segment(1);
$loc=Request::segment(3);
if($loc == 'ksa' || $loc == 'uae' )
{
   $loc21 = strtoupper($loc);
}else 
{
    $loc21 = ucfirst($loc);
}
 $type=Request::segment(4);
$modules = \App\Models\Module::where('location', $loc21)->get();
$link=$loc;
 
$current=date('Y-m-d');
$savedCOSpace = \App\Models\CoworkspaceProperty::where('user_id',\Auth::user()->id)->count();
$users = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','cospace')->where('expiry_date','>=',$current)->where('count','>',$savedCOSpace)->count();
if($users){
	if(@$users > 0){
		$last_transaction = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','cospace')->where('expiry_date','>=',$current)->where('count','>',$savedCOSpace)->latest()->first();
		if($last_transaction->count > $last_transaction->listed){
			Session::put('transaction_id',$last_transaction->_id);
			$package="NO";
		}else{
			$package="YES";
		}
	}else{
	    $package="YES";
	}
}else{
     $package="YES";
}
@endphp
@extends('frontend.layouts.home')
@section('title','CoworkSpace Your Property')


@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.carousel.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.theme.default.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.theme.green.min.css" />

	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
	
	<style type="text/css">

		.visibly-hidden {
			position: absolute !important;
			height: 1px; width: 1px;
			overflow: hidden;
			clip: rect(1px 1px 1px 1px); 
			clip: rect(1px, 1px, 1px, 1px);
			pointer-events: none;
		}

		.radio-label{
			cursor: pointer;
			user-select: none;
			background: white;
			border: 1px solid #ccc;
			padding: 10px;
		}

		/* input[type="radio"]:checked + label{
		    border-color: #199406 !important;
		    color: white;
		    background-color: #199406 !important;
		}

		input[type="radio"]:checked + label{
		    border-color: #E4002B !important;
		    color: white;
		    background-color: #E4002B !important;
		} */
	</style>
@endsection
@section('content')
    
    @include('frontend.layouts.default-header')
    
    <form class="loc-form coworkspace_master_form" name="coworkspace_master_form" id="coworkspace_master_form" enctype="multipart/form-data">
    	@csrf
		@if($package=='YES')
		@include('frontend.list-property.subscription')
		@endif
	    @include('frontend.list-property.coworkspace.step0')
	    @include('frontend.list-property.coworkspace.step1')
	    @include('frontend.list-property.coworkspace.step2')
	    @include('frontend.list-property.coworkspace.step3')
	    @include('frontend.list-property.coworkspace.step4')
	    @include('frontend.list-property.coworkspace.step5')
	    @include('frontend.list-property.coworkspace.step6')
	    @include('frontend.list-property.coworkspace.step7')
		@include('frontend.others.package')

		<div id="listPropertyErrors" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<ul class="listProperty_errors_points" style="max-height: 450px;
    overflow-y: auto;">
							
						</ul>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</form>
    
    @include('frontend.layouts.footer')

@endsection

@section('scripts')
 <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/owl.carousel.min.js"></script>
 <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
 <script type="text/javascript">
	let propertySteps = false;
	let imageSkip = false;
	$(document).ready(function(){
                $(this).scrollTop(0);
            });
	function payPlan(id,amount,number){
    
    var planid= $("#plan_id22").val();
    var planamount=$("#plan_amount22").val();
	var plannumber = $("#plan_number22").val();
	var sku_id 	= $("#sku_id2").val();

	if(!planid)
	{
		alert('Please select any plan');
		return false;
	}
	$("#plan_id").val(planid);
    $("#plan_amount2").val(planamount);
	$("#plan_number").val(plannumber);
	$("#plan_amount").html(planamount);

	$("#sku_id").val(sku_id);
	if(planamount==0){
	$("#myModal1").show();
	}else{
   $("#myModal").show();
	}
}

    const showScreen = function(step){
		$('.listPropertySteps').hide();
		$('.'+step).show();
		$("html, body").animate({ scrollTop: 0 }, "slow");
	}
	
function file_validation(id,max_size)
   {
       var fuData = document.getElementById(id);
       var FileUploadPath = fuData.value;
       
   
       if (FileUploadPath == ''){
           alert("Please upload Attachment");
       } 
       else {
           var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
           if (Extension == "jpg" || Extension == "jpeg" || Extension == "png" || Extension == "webp" ) {
   
                   if (fuData.files && fuData.files[0]) {
                       var size = fuData.files[0].size;
                       
                       if(size > max_size){   //1000000 = 1 mb
                           
                           Swal.fire({
			                    title: 'Warning!',
			                    text: "Maximum file size "+max_size/1000000+" MB",
			                    icon: 'info',
			                    confirmButtonText: 'Ok'
			                });
			                
                           $("#"+id).val('');
                           return false;
                            
                       }
                   }
   
           } 
           else 
           {
                
               $("#"+id).val('');
                Swal.fire({
			                    title: 'Warning!',
			                    text: "document only allows file types of  jpg , png  , jpeg , webp ",
			                    icon: 'info',
			                    confirmButtonText: 'Ok'
			                });
			                
                           $("#"+id).val('');
                           return false;
           }
       }   
   }      

	function readURL(input) {

		var id = $(input).attr('id');
		 var max_size = 2000000;
           file_validation(id,max_size);

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
            	var id=input.id;
            	$('#'+id+'_src').attr('src', e.target.result);
		    }
            reader.readAsDataURL(input.files[0]);
        }
    }

    const privateOfficeAvailable = function(divid){
		if($('#'+divid).is(':visible')){
			$('#'+divid).hide();
		}else{
			$('#'+divid).show();
		}
	}

	const openWorkspaceAvailable = function(divid){
		if($('#'+divid).is(':visible')){
			$('#'+divid).hide();
		}else{
			$('#'+divid).show();
		}
	}
	const dedicatedDeskAvailable = function(divid){
		if($('#'+divid).is(':visible')){
			$('#'+divid).hide();
		}else{
			$('#'+divid).show();
		}
	}

	const addPrivateOffice = function(divid){
		var html= '<div class="col-md-12 private_office_repeat mt-3">\
		                <div class="row">\
		                    <div class="listspace-remover removeOffice">x\
		                    </div>\
		                </div>\
		                <div class="row">\
		                    <div class="col-md-12">\
		                        <div class="form-group">\
		                            <label>Name of private office</label>\
		                            <input type="text" class="form-control" placeholder="eg. small office" name="private_office_name[]">\
		                            <span class="invalid-feedback private_office_name"></span>\
		                        </div>\
		                    </div>\
		                    <div class="col-md-12">\
		                        <div class="form-group">\
		                            <label>Number of people</label>\
		                            <select class="form-control" name="country">\
		                                <option value="">Select Country</option>\
		                            </select>\
		                            <span class="invalid-feedback country"></span>\
		                        </div>\
		                    </div>\
		                </div>\
		                <div class="form-group mt-4">\
		                    <h5>\
		                        Price\
		                    </h5>\
		                </div>\
		                <div class="row">\
		                    <div class="col-md-2">\
		                        <label>Duration</label>\
		                        <div class="listspace-price-tab-duration">\
		                            1 Day\
		                        </div>\
		                    </div>\
		                    <div class="col-md-7">\
		                        <div class="form-group">\
		                            <label>Price</label>\
		                            <input class="form-control" type="text" name="private_office_daily_price" placeholder="Price">\
		                            <span class="invalid-feedback private_office_daily_price"></span>\
		                        </div>\
		                    </div>\
		                    <div class="col-md-2">\
		                        <div class="form-group">\
		                            <label>Currency</label>\
		                            <select class="form-control rounded" name="private_office_currency">\
		                                @foreach(getAllCurrencies() as $row)\
		                                    <option value="{{$row->code}}">{{$row->code}}</option>\
		                                @endforeach\
		                            </select>\
		                            <span class="invalid-feedback private_office_daily_currency"></span>\
		                        </div>\
		                    </div>\
		                </div>\
		                <div class="form-group">\
		                    <h5>\
		                        Does the office have a whiteboard\
		                    </h5>\
		                </div>\
		                <div class="row">\
		                    <div class="col-md-3">\
		                        <div class="form-group">\
		                            <label>\
		                                <input type="checkbox" name=""> Yes\
		                            </label>\
		                        </div>\
		                    </div>\
		                    <div class="col-md-3">\
		                        <div class="form-group">\
		                            <label>\
		                                <input type="checkbox" name=""> No\
		                            </label>\
		                        </div>\
		                    </div>\
		                </div>\
            		</div>';
		$('.private-offices-panel-main').append(html);
	}

	$(document).ready(function(){


		$("html, body").animate({ scrollTop: 0 }, "slow");

		$('input.timepicker').timepicker({
            timeFormat: 'HH:mm',
        });
		$(document).on('click','.removeOffice', function(){
			$(this).parent().parent().remove();
		});

		$(window).bind('beforeunload',function() {
	        if(!propertySteps){
                return "'Are you sure you want to leave the page. All data will be lost!";
            }
	    });

	    $(document).on('change','.operating_radio', function(){
	    	if($(this).val()=='open'){
	    		$('.'+$(this).data('id')).show();
	    	}else{
	    		$('.'+$(this).data('id')).hide();
	    	}
	    });

		$(document).on('click','#add_more', function(){
			if($('.upload').length>=15){
				Swal.fire({
                    title: 'Warning!',
                    text: "You can upload maximum 15 pictures.",
                    icon: 'info',
                    confirmButtonText: 'Ok'
                });
                return false;
			}
			var id= $('.upload').length+1;
			$("#pictureresult").append('\
                <div class="upload col-md-3 mb-4">\
                    <input type="file" id="files'+id+'" name="files[]" class="input-file" onchange="readURL(this)" >\
                    <label for="files'+id+'">\
                        <img id="files'+id+'_src" class="file_prev" src="{{asset("img/addmedia-upload.png")}}" style="height:120px;width:120px;">\
                    </label>\
                    <span class="remove removeDiv" style="cursor: pointer;position:absolute;right: 33px !important;font-size: 25px;top: -18px !important;"><i class="fa fa-times" aria-hidden="true"></i></span>\
                    <span class="invalid-feedback files'+id+'"></span>\
                </div>');
		});
		$(document).on('click','.removeDiv', function(){
			$(this).parent().remove();
		});

		$(document).on('change','.country_change', function(e){
            e.preventDefault();
            var country = $(this).val();
            var id = $(this).data('change');
            if(!country){
                return false;
            }
            var ajax_url = WEBSITE_URL+"/states/"+country;    
            $.ajax({
                url : ajax_url,
                type:'get',
                dataType : 'json',
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.coworkspace_master_form');
                },
                complete:function(){
                   stopLoader('.coworkspace_master_form'); 
                },
                success : function(data){
                    var html='<option value="">Select State</option>';
                        
                    if(data.states){
                        $.each(data.states, function(key, val){
                            html+='<option value="'+val+'">'+val+'</option>';
                        });
                    }
                    $('select[name="state"]').html(html);
                },
                error : function(data){
                    stopLoader('.coworkspace_master_form');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                    }
                }
            });
        });



		$(document).on('click','.submit_step_0', function(){
	        showScreen('step1');
	    });



		// submit on first screen event
		$(document).on('click','.submit_step_1', function(){
			$form = $('.swap_div_form_1');
	    	$form.find('.is-invalid').removeClass('is-invalid');
	        $form.find('.invalid-feedback').text('');

	        var ajax_url = WEBSITE_URL+"/listproperty/coworkspace/1";
	        var data = new FormData();

	        data.append('title', $form.find('input[name="title"]').val());
	        data.append('description', $form.find('textarea[name="description"]').val());
	        
	        $.ajax({
	            url : ajax_url,
	            type:'post',
	            data : data,
	            dataType : 'json',
	            processData: false,
	            contentType: false,
	            beforeSend:function(){
	                startLoader('.coworkspace_master_form');
	            },
	            complete:function(){
	               stopLoader('.coworkspace_master_form'); 
	            },
	            success : function(data){
	                if(data.status){
	                    showScreen('step2');
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            },
	            error : function(data){
	                stopLoader('.coworkspace_master_form');
	                if(data.responseJSON){
	                    var err_response = data.responseJSON;  
	                    if(err_response.errors==undefined && err_response.message) {
	                        Swal.fire({
	                            title: 'Error!',
	                            text: err_response.message,
	                            icon: 'error',
	                            confirmButtonText: 'Ok'
	                        });
	                    }
	                    $.each(err_response.errors, function(i, obj){
	                        $form.find('span.invalid-feedback.'+i+'').text(obj).show();
	                        $form.find('input[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('select[name="'+i+'"]').addClass('is-invalid');
	                    });
	                }
	            }
	        });
	    });


		// submit on second screen event
	    $(document).on('click','.submit_step_2', function(){
			$form = $('.swap_div_form_2');
	    	$form.find('.is-invalid').removeClass('is-invalid');
	        $form.find('.invalid-feedback').text('');

	        var ajax_url = WEBSITE_URL+"/listproperty/coworkspace/2";
	        var data = new FormData();

	        data.append('address', $form.find('input[name="address"]').val());
	        data.append('location_name', $form.find('input[name="location_name"]').val());
	        data.append('unit_number', $form.find('input[name="unit_number"]').val());
	        data.append('address_line_1', $form.find('input[name="address_line_1"]').val());
	        data.append('address_line_2', $form.find('input[name="address_line_2"]').val());
	        data.append('landmark', $form.find('input[name="landmark"]').val());
	        // data.append('city', $form.find('input[name="city"]').val());
	        data.append('country', $form.find('select[name="country"]').val());
	        data.append('city', $form.find('select[name="city"]').val());
	        data.append('zip_code', $form.find('input[name="zip_code"]').val());
	        
	        $.ajax({
	            url : ajax_url,
	            type:'post',
	            data : data,
	            dataType : 'json',
	            processData: false,
	            contentType: false,
	            beforeSend:function(){
	                startLoader('.coworkspace_master_form');
	            },
	            complete:function(){
	               stopLoader('.coworkspace_master_form'); 
	            },
	            success : function(data){
	                if(data.status){
	                    showScreen('step3');
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            },
	            error : function(data){
	                stopLoader('.coworkspace_master_form');
	                if(data.responseJSON){
	                    var err_response = data.responseJSON;  
	                    if(err_response.errors==undefined && err_response.message) {
	                        Swal.fire({
	                            title: 'Error!',
	                            text: err_response.message,
	                            icon: 'error',
	                            confirmButtonText: 'Ok'
	                        });
	                    }
	                    $.each(err_response.errors, function(i, obj){
	                        $form.find('span.invalid-feedback.'+i+'').text(obj).show();
	                        $form.find('input[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('select[name="'+i+'"]').addClass('is-invalid');
	                    });
	                }
	            }
	        });
	    });

	    // submit on 3rd screen event
	    $(document).on('click','.specialAmenitiesBlock', function(){
			$(this).toggleClass('active');
			$(this).find('.special_amenity_check').toggleClass('active');
		});

	    $(document).on('click','.submit_step_3', function(){
	    	$form = $('.swap_div_form_3');
	    	$form.find('.is-invalid').removeClass('is-invalid');
	        $form.find('.invalid-feedback').text('');

	        var ajax_url = WEBSITE_URL+"/listproperty/coworkspace/3";
	        var data = new FormData();

	        $('.specialAmenitiesBlock').each(function(i){
                if($(this).is('.active')){
					if($(this).data('type')=='special'){
                    	data.append('special_amenties[]', $(this).attr('data-id'));
                    }
                    if($(this).data('type')=='food'){
                    	data.append('food_amenties[]', $(this).attr('data-id'));
                    }
                    else if($(this).data('type')=='facilities'){
                    	data.append('facilities_amenties[]', $(this).attr('data-id'));
                    }
                    else if($(this).data('type')=='equipments'){
                    	data.append('equipments_amenties[]', $(this).attr('data-id'));
                    }
                    else if($(this).data('type')=='seating'){
                    	data.append('seating_amenties[]', $(this).attr('data-id'));
                    }
                    else if($(this).data('type')=='accessibility'){
                    	data.append('accessibility_amenties[]', $(this).attr('data-id'));
                    }
                }
            });

	        $.ajax({
	            url : ajax_url,
	            type:'post',
	            data : data,
	            dataType : 'json',
	            processData: false,
	            contentType: false,
	            beforeSend:function(){
	                startLoader('.coworkspace_master_form');
	            },
	            complete:function(){
	               stopLoader('.coworkspace_master_form'); 
	            },
	            success : function(data){
	                if(data.status){
	                    showScreen('step4');
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            },
	            error : function(data){
	                stopLoader('.coworkspace_master_form');
	                if(data.responseJSON){
	                    var err_response = data.responseJSON;  
	                    if(err_response.errors==undefined && err_response.message) {
	                        Swal.fire({
	                            title: 'Error!',
	                            text: err_response.message,
	                            icon: 'error',
	                            confirmButtonText: 'Ok'
	                        });
	                    }
	                }
	            }
	        });
	    });

	    // submit on 4thd screen event
	    $(document).on('click','.submit_step_4', function(){
            $form = $('.swap_div_form_4');
	    	$form.find('.is-invalid').removeClass('is-invalid');
	        $form.find('.invalid-feedback').text('');

	        var ajax_url = WEBSITE_URL+"/listproperty/coworkspace/4";
	        var data = new FormData();

	        var monday = $('input[name="monday"]:checked').val() ? $('input[name="monday"]:checked').val(): '';
	        data.append('monday', monday);
	        data.append('monday_start_time', $form.find('input[name="monday_start_time"]').val());
	        data.append('monday_end_time', $form.find('input[name="monday_end_time"]').val());

            data.append('tuesday', $('input[name="tuesday"]:checked').val());
            data.append('tuesday_start_time', $form.find('input[name="tuesday_start_time"]').val());
	        data.append('tuesday_end_time', $form.find('input[name="tuesday_end_time"]').val());

            data.append('wednesday', $('input[name="wednesday"]:checked').val());
            data.append('wednesday_start_time', $form.find('input[name="wednesday_start_time"]').val());
	        data.append('wednesday_end_time', $form.find('input[name="wednesday_end_time"]').val());

            data.append('thursday', $('input[name="thursday"]:checked').val());
            data.append('thursday_start_time', $form.find('input[name="thursday_start_time"]').val());
	        data.append('thursday_end_time', $form.find('input[name="thursday_end_time"]').val());

            data.append('friday', $('input[name="friday"]:checked').val());
            data.append('friday_start_time', $form.find('input[name="friday_start_time"]').val());
	        data.append('friday_end_time', $form.find('input[name="friday_end_time"]').val());

            data.append('saturday', $('input[name="saturday"]:checked').val());
            data.append('saturday_start_time', $form.find('input[name="saturday_start_time"]').val());
	        data.append('saturday_end_time', $form.find('input[name="saturday_end_time"]').val());

            data.append('sunday', $('input[name="sunday"]:checked').val());
	        data.append('sunday_start_time', $form.find('input[name="sunday_start_time"]').val());
	        data.append('sunday_end_time', $form.find('input[name="sunday_end_time"]').val());

	        $.ajax({
	            url : ajax_url,
	            type:'post',
	            data : data,
	            dataType : 'json',
	            processData: false,
	            contentType: false,
	            beforeSend:function(){
	                startLoader('.coworkspace_master_form');
	            },
	            complete:function(){
	               stopLoader('.coworkspace_master_form'); 
	            },
	            success : function(data){
	                if(data.status){
	                    showScreen('step5');
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            },
	            error : function(data){
	                stopLoader('.coworkspace_master_form');
	                if(data.responseJSON){
	                    var err_response = data.responseJSON;  
	                    if(err_response.errors==undefined && err_response.message) {
	                        Swal.fire({
	                            title: 'Error!',
	                            text: err_response.message,
	                            icon: 'error',
	                            confirmButtonText: 'Ok'
	                        });
	                    }
	                    $.each(err_response.errors, function(i, obj){
	                        $form.find('span.invalid-feedback.'+i+'').text(obj).css('display','block');
	                        $form.find('input[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('select[name="'+i+'"]').addClass('is-invalid');
	                    });
	                }
	            }
	        });
			
	    });

	    $(document).on('click','.submit_step_5', function(){
			$form = $('.swap_div_form_5');
	    	$form.find('.is-invalid').removeClass('is-invalid');
	        $form.find('.invalid-feedback').text('');

	        var ajax_url = WEBSITE_URL+"/listproperty/coworkspace/5";
	        var data = new FormData();
	        data.append('email', $form.find('input[name="email"]').val());
	        data.append('contact_number', $form.find('input[name="contact_number"]').val());
	        data.append('website_url', $form.find('input[name="website_url"]').val());
	        data.append('facebook_url', $form.find('input[name="facebook_url"]').val());
	        data.append('twitter_url', $form.find('input[name="twitter_url"]').val());
	        data.append('instagram_url', $form.find('input[name="instagram_url"]').val());

	        $.ajax({
	            url : ajax_url,
	            type:'post',
	            data : data,
	            dataType : 'json',
	            processData: false,
	            contentType: false,
	            beforeSend:function(){
	                startLoader('.coworkspace_master_form');
	            },
	            complete:function(){
	               stopLoader('.coworkspace_master_form'); 
	            },
	            success : function(data){
	                if(data.status){
	                    showScreen('step6');
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            },
	            error : function(data){
	                stopLoader('.coworkspace_master_form');
	                if(data.responseJSON){
	                    var err_response = data.responseJSON;  
	                    if(err_response.errors==undefined && err_response.message) {
	                        Swal.fire({
	                            title: 'Error!',
	                            text: err_response.message,
	                            icon: 'error',
	                            confirmButtonText: 'Ok'
	                        });
	                    }
	                    $.each(err_response.errors, function(i, obj){
	                        $form.find('span.invalid-feedback.'+i+'').text(obj).show();
	                        $form.find('input[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('select[name="'+i+'"]').addClass('is-invalid');
	                    });
	                }
	            }
	        });
	    });


		$(document).on('click','.submit_step_6', function(){
			$('.buy-rent').hide();
			$(".package").show();
		});

	    $(document).on('click','.submit_step_8', function(){
	    	
			$form = $('.swap_div_form_6');
	    	$form.find('.is-invalid').removeClass('is-invalid');
	        $form.find('.invalid-feedback').text('');

	        var ajax_url = WEBSITE_URL+"/listproperty/coworkspace/6";
	        var data = new FormData();
	        var error = false;
	        
	        $.each($('.listPropertySteps [type=file]'), function(index, file) {
	        	// if($('input[type=file]')[index].files[0]==undefined){
	        	// 	var id = $('input[type=file]')[index].id;
                //     $('.invalid-feedback.'+(id)).text('The file is required.').show();
	        	// 	error = true;
	        	// }
	        	
				data.append('files[]', $('input[type=file]')[index].files[0] ? $('input[type=file]')[index].files[0]: '');
			});

			if(error){
				return false;
			}

	        $.ajax({
	            url : ajax_url,
	            type:'post',
	            data : data,
	            enctype: 'multipart/form-data',
	            dataType : 'json',
	            processData: false,
	            contentType: false,
	            beforeSend:function(){
	                startLoader('.listPropertySteps');
	            },
	            complete:function(){
	               stopLoader('.listPropertySteps'); 
	            },
	            success : function(data){
	            	$(".package").hide();
	                if(data.status){
	                    showScreen('step7');
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            },
	            error : function(data){
	                stopLoader('.listPropertySteps');
	                if(data.responseJSON){
	                    var err_response = data.responseJSON;  
	                    if(err_response.errors==undefined && err_response.message) {
	                        Swal.fire({
	                            title: 'Error!',
	                            text: err_response.message,
	                            icon: 'error',
	                            confirmButtonText: 'Ok'
	                        });
	                    }
	                    $.each(err_response.errors, function(key, value){
	                        if(key.indexOf('.') != -1) {
                                let keys = key.split('.');                                
                                $('input[name="'+keys[0]+'[]"]').eq(keys[1]).addClass('is-invalid').parent().find('.invalid-feedback').text(value).show();                         
                            }
                            else {
                                $('input[name="'+key+'[]"]').eq(0).addClass('is-invalid').parent().find('.invalid-feedback').text(value).show();
                            }
	                    });
	                }
	            }
	        });
	    });

	    $(document).on('click','.submit_step_7', function(){
			$form = $('.swap_div_form_7');
	    	$form.find('.is-invalid').removeClass('is-invalid');
	        $form.find('.invalid-feedback').text('');

	        var ajax_url = WEBSITE_URL+"/listproperty/coworkspace/7";
	        var data = new FormData();
	        
	        

	        // dedicated space
	        var dedicated_desk_available = $('input[name="dedicated_desk_available"]:checked').attr('value') ? 1 : 0;
	        var private_office_available = $('input[name="private_office_available"]:checked').attr('value') ? 1 : 0;
	        var open_workspace_available = $('input[name="open_workspace_available"]:checked').attr('value') ? 1 : 0;
	        var white_board_available = $('input[name="white_board_available"]:checked').attr('value') ? 1 : 0;
	        
	        data.append('dedicated_desk_available', dedicated_desk_available);
	        data.append('private_office_available', private_office_available);
	        data.append('open_workspace_available', open_workspace_available);
	        data.append('currency_type', $('#currency_type').val()); 

	        if(open_workspace_available==1){
	        	data.append('open_space_daily_price', $form.find('input[name="open_space_daily_price"]').val());
	        	data.append('open_space_weekly_price', $form.find('input[name="open_space_weekly_price"]').val());
	        	data.append('open_space_monthly_price', $form.find('input[name="open_space_monthly_price"]').val());

	        	data.append('open_space_daily_currency', $form.find('select[name="open_space_daily_currency"]').val());
	        	data.append('open_space_weekly_currency', $form.find('select[name="open_space_weekly_currency"]').val());
	        	data.append('open_space_monthly_currency', $form.find('select[name="open_space_monthly_currency"]').val());
	        	data.append('open_space_seats', $form.find('select[name="open_space_seats"]').val());
	        }

	        if(dedicated_desk_available==1){
	        	data.append('dedicated_space_daily_price', $form.find('input[name="dedicated_space_daily_price"]').val());
		        data.append('dedicated_space_weekly_price', $form.find('input[name="dedicated_space_weekly_price"]').val());
		        data.append('dedicated_space_monthly_price', $form.find('input[name="dedicated_space_monthly_price"]').val());

		        data.append('dedicated_space_daily_currency', $form.find('select[name="dedicated_space_daily_price"]').val());
		        data.append('dedicated_space_weekly_currency', $form.find('select[name="dedicated_space_weekly_currency"]').val());
		        data.append('dedicated_space_monthly_currency', $form.find('select[name="dedicated_space_monthly_currency"]').val());
		        data.append('dedicated_space_seats', $form.find('select[name="dedicated_space_seats"]').val());
	        }

	        if(private_office_available==1){
	        	data.append('private_space_daily_price', $form.find('input[name="private_space_daily_price"]').val());
				data.append('private_space_weekly_price', $form.find('input[name="private_space_weekly_price"]').val());
				data.append('private_space_monthly_price', $form.find('input[name="private_space_monthly_price"]').val());

				data.append('private_space_daily_currency', $form.find('select[name="private_space_daily_price"]').val());
				data.append('private_space_weekly_currency', $form.find('select[name="private_space_weekly_currency"]').val());
				data.append('private_space_monthly_currency', $form.find('select[name="private_space_monthly_currency"]').val());
		        data.append('seating_capacity', $form.find('select[name="seating_capacity"]').val());
		        data.append('no_of_private_office', $form.find('select[name="no_of_private_office"]').val());
		        data.append('white_board_available', white_board_available);
	        }

	        $.ajax({
	            url : ajax_url,
	            type:'post',
	            data : data,
	            dataType : 'json',
	            processData: false,
	            contentType: false,
	            beforeSend:function(){
	                startLoader('.listPropertySteps');
	            },
	            complete:function(){
	              // 
	            },
	            success : function(data){
	                if(data.status){
	                    addProperty();
	                }else{
	                	stopLoader('.listPropertySteps'); 
	                    
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            },
	            error : function(data){
	                stopLoader('.listPropertySteps');
	                if(data.responseJSON){
	                    var err_response = data.responseJSON;  
	                    if(err_response.errors==undefined && err_response.message) {
	                        Swal.fire({
	                            title: 'Error!',
	                            text: err_response.message,
	                            icon: 'error',
	                            confirmButtonText: 'Ok'
	                        });
	                    }
	                    $.each(err_response.errors, function(i, value){
	                        $form.find('span.invalid-feedback.'+i+'').text(value).show();
	                        $form.find('input[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('select[name="'+i+'"]').addClass('is-invalid');
	                    });
	                }
	            }
	        });
	    });

	    const addProperty = function(){
	    	var ajax_url = WEBSITE_URL+"/listproperty/coworkspace/8";
	        
	        var $form = $('#coworkspace_master_form');
	        var data = new FormData($form[0]);

	        $form.find('.is-invalid').removeClass('is-invalid');
	        $form.find('.invalid-feedback').text('');
	        
	        var dedicated_desk_available = $('input[name="dedicated_desk_available"]:checked').attr('value') ? 1 : 0;
	        var private_office_available = $('input[name="private_office_available"]:checked').attr('value') ? 1 : 0;
	        var open_workspace_available = $('input[name="open_workspace_available"]:checked').attr('value') ? 1 : 0;
	        var white_board_available = $('input[name="white_board_available"]:checked').attr('value') ? 1 : 0;
	        
	        data.append('dedicated_desk_available', dedicated_desk_available);
	        data.append('private_office_available', private_office_available);
	        data.append('open_workspace_available', open_workspace_available);
	        data.append('currency_type', $('#currency_type').val()); 
	        
	        $('.specialAmenitiesBlock').each(function(i){
                if($(this).is('.active')){
					if($(this).data('type')=='special'){
                    	data.append('special_amenties[]', $(this).attr('data-id'));
                    }
                    if($(this).data('type')=='food'){
                    	data.append('food_amenties[]', $(this).attr('data-id'));
                    }
                    else if($(this).data('type')=='facilities'){
                    	data.append('facilities_amenties[]', $(this).attr('data-id'));
                    }
                    else if($(this).data('type')=='equipments'){
                    	data.append('equipments_amenties[]', $(this).attr('data-id'));
                    }
                    else if($(this).data('type')=='seating'){
                    	data.append('seating_amenties[]', $(this).attr('data-id'));
                    }
                    else if($(this).data('type')=='accessibility'){
                    	data.append('accessibility_amenties[]', $(this).attr('data-id'));
                    }
                }
            });

            if(imageSkip){
	        	data.append('image_skip', 1);
	        }else{
	        	data.append('image_skip',0);
	        }

	        $.ajax({
	            url : ajax_url,
	            type:'post',
	            data : data,
	            dataType : 'json',
	            processData: false,
	            contentType: false,
	            beforeSend:function(){
	                startLoader('.listPropertySteps');
	            },
	            complete:function(){
	               
	            },
	            success : function(data){
	                if(data.status){
	                	propertySteps = true;
	                	$('.submit_step_7').attr('disabled',true);
	                	
	                    Swal.fire({
	                        title: 'Success!',
	                        text: data.message,
	                        icon: 'success',
	                        confirmButtonText: 'Ok'
	                    }).then((result) => {
				            if(result.value){
				                window.location =data.url;
				            }else{
				            	window.location =data.url;
				            }
				        });
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                    stopLoader('.listPropertySteps'); 
	                }
	            },
	            error : function(data){
	                stopLoader('.listPropertySteps');
	                if(data.responseJSON){
	                    var err_response = data.responseJSON;  
	                    if(err_response.errors==undefined && err_response.message) {
	                        Swal.fire({
	                            title: 'Error!',
	                            text: err_response.message,
	                            icon: 'error',
	                            confirmButtonText: 'Ok'
	                        });
	                    }
	                    var html='';
	                    $.each(err_response.errors, function(i, obj){
	                        html+='<li>'+obj+'</li>';
	                    });
	                    if(html){
	                    	$('.listProperty_errors_points').html(html);
	                    	$('#listPropertyErrors').modal('toggle');
	                    }
	                }
	            }
	        });
		}

		setTimeout(function(){ 
			$('#monday_close').trigger('click');
			$('#tuesday_close').trigger('click');
			$('#wed_close').trigger('click');
			$('#thu_close').trigger('click');
			$('#friday_close').trigger('click');
			$('#sat_close').trigger('click');
			$('#sunday_close').trigger('click');
		}, 3000);

	});
function monday_close(){
	$("#monday_close").css('background-color','#E4002B');
	$("#monday_open").css('background-color','#FFFFFF');
}
function monday_open(){
	$("#monday_open").css('background-color','#199406');
	$("#monday_close").css('background-color','#FFFFFF');
}
function tuesday_close(){
	$("#tuesday_close").css('background-color','#E4002B');
	$("#tuesday_open").css('background-color','#FFFFFF');
}
function tuesday_open(){
	$("#tuesday_open").css('background-color','#199406');
	$("#tuesday_close").css('background-color','#FFFFFF');
}
function wed_close(){
	$("#wed_close").css('background-color','#E4002B');
	$("#wed_open").css('background-color','#FFFFFF');
}
function wed_open(){
	$("#wed_open").css('background-color','#199406');
	$("#wed_close").css('background-color','#FFFFFF');
}
function thu_close(){
	$("#thu_close").css('background-color','#E4002B');
	$("#thu_open").css('background-color','#FFFFFF');
}
function thu_open(){
	$("#thu_open").css('background-color','#199406');
	$("#thu_close").css('background-color','#FFFFFF');
}
function friday_close(){
	$("#friday_close").css('background-color','#E4002B');
	$("#friday_open").css('background-color','#FFFFFF');
}
function friday_open(){
	$("#friday_open").css('background-color','#199406');
	$("#friday_close").css('background-color','#FFFFFF');
}
function sat_close(){
	$("#sat_close").css('background-color','#E4002B');
	$("#sat_open").css('background-color','#FFFFFF');
}
function sat_open(){
	$("#sat_open").css('background-color','#199406');
	$("#sat_close").css('background-color','#FFFFFF');
}
function sunday_close(){
	$("#sunday_close").css('background-color','#E4002B');
	$("#sunday_open").css('background-color','#FFFFFF');
}
function sunday_open(){
	$("#sunday_open").css('background-color','#199406');
	$("#sunday_close").css('background-color','#FFFFFF');
}
</script>
<script type="text/javascript">
function selectPlan(id,amount,number,sku_id=''){
    $("#plan_amount").html(amount);
    $("#plan_id").val(id);
    $("#plan_amount2").val(amount);
    $("#plan_number").val(number);
    $("#sku_id2").val(sku_id);
	if(amount==0){
	$("#myModal1").show();
	}else{
   $("#myModal").show();
	}
}

  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('bg-header');
    } else {
       $('header').removeClass('bg-header');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('fixed-top');
    } else {
       $('header').removeClass('fixed-top');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('#top-header-bar').removeClass('fixed-top');
    } else {
       $('#top-header-bar').addClass('fixed-top');
    }
});
  
</script>
<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery('#loading').fadeOut(1000);
});

</script>
@if($package=='YES')
<script
    src="https://www.paypal.com/sdk/js?client-id={{env('SB_CLIENT_ID')}}"> // Required. Replace SB_CLIENT_ID with your sandbox client ID.
  </script>
<script>
    
	paypal.Buttons({
	  style:{
		color: 'white',
		layout: 'horizontal',
		tagline: false,
		shape:   'rect'
	  },
	  createOrder: function(data, actions) {
		// This function sets up the details of the transaction, including the amount and line item details.
		return actions.order.create({
		  purchase_units: [{
			amount: {
			  value: $("#plan_amount2").val()
			}
		  }]
		});
	  },
	  onApprove: function(data, actions) {
		// This function captures the funds from the transaction.
		return actions.order.capture().then(function(details) {
		  var ajax_url = WEBSITE_URL+"/listproperty/transaction";
		  var amount=$("#plan_amount2").val();
		  var id=$("#plan_id").val();
		  var number=$("#plan_number").val();
		  $.ajax({
				  url : ajax_url,
				  method:"post",
				data:{
				  amount:amount,
				  id:id,
				  number:number,
				},
				headers:{
				  'X-CSRF-TOKEN': '{{ csrf_token() }}'
				},
				beforeSend:function(){
	                startLoader();
	            },
	            complete:function(){
	               stopLoader(); 
	            },
				  success : function(data){
					  if(data.status){
						  Swal.fire({
							  title: 'Success!',
							  text: 'Transaction completed successfully',
							  icon: 'success',
							  confirmButtonText: 'Ok'
						  }).then((result) => {
							  if(result.value){
								  $(".subscribe").hide();
								  $("#myModal").hide();
								  showScreen('step0');
							  }
						  });
					  }else{
						  Swal.fire({
							  title: 'Error!',
							  text: data.message,
							  icon: 'error',
							  confirmButtonText: 'Ok'
						  });
					  }
				  }
			  });
		  // This function shows a transaction success message to your buyer.
		  
		  // alert('Transaction completed by ' + details.payer.name.given_name);
		});
	  }
	}).render('#paypal-button-container10');
	//This function displays Smart Payment Buttons on your web page.
  </script>
  @endif
  <script>
	function clodebu(){
	  $("#myModal").hide();
	}

	  function countinuefree(){
	var ajax_url = WEBSITE_URL+"/listproperty/transaction";
        var amount=$("#plan_amount2").val();
        var id=$("#plan_id").val();
        var number=$("#plan_number").val();
        var sku_id 	= $("#sku_id2").val();
        $.ajax({
	            url : ajax_url,
	            method:"post",
              data:{
                amount:amount,
                id:id,
                number:number,
                sku_id:sku_id,
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
			  },
			  beforeSend:function(){
	                startLoader();
	            },
	            complete:function(){
	               stopLoader(); 
	            },
	            success : function(data){
	                if(data.status){
								$(".subscribe").hide();
								$("#myModal1").hide();
                                showScreen('step1');
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            }
	        });
  }
  </script>
  <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
$(function() {
    var $form = $(".require-validation");
    $('form.require-validation').bind('submit', function(e) {
        var $form = $(".require-validation"),
            inputSelector = ['input[type=email]',
                            'input[type=password]',
                            'input[type=text]',
                            'input[type=file]',
                            'textarea'].join(', '),
            $inputs       = $form.find('.required').find(inputSelector),
            $errorMessage = $form.find('.inp-error'),
            valid         = true;
            $errorMessage.addClass('d-none');
        $('.has-error').removeClass('has-error');

        $inputs.each(function(i, el) {
            var $input = $(el);
            if ($input.val() === '') {
                $input.parent().addClass('has-error');
                $errorMessage.removeClass('d-none');
                e.preventDefault();
            }
        });

        if (!$form.data('cc-on-file')) {
            var StripeKey = "{{env('STRIPE_KEY')}}";

            e.preventDefault();
            Stripe.setPublishableKey(StripeKey);
            Stripe.createToken({
                number: $('.card-number').val(),
                cvc:    $('.card-cvc').val(),
                exp_month: $('.card-expiry-month').val(),
                exp_year: $('.card-expiry-year').val()
            }, stripeResponseHandler);
        }

    });
 	function stripeResponseHandler(status, response) {
         
        if (response.error) {
            $('.stripe-error').text(response.error.message);
        } else {

            var token = response['id'];
            $form.find('input[type=text]').empty(); 
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");

            var ajax_url    = WEBSITE_URL+"/listproperty/stripeorder";
            var amount      = $("#plan_amount2").val();
           
            $("#stipe_payment_btn").val(amount);
            var stipe_payment_btn = amount;
            var id          = $("#plan_id").val();
            var number      = $("#plan_number").val();
            var stripeToken       = token;
 
            $.ajax({
                type: 'post',
                url: ajax_url,
                data: {
                        amount:amount,
                        id:id,
                        number:number,
                        stipe_payment_btn:stipe_payment_btn,
                        stripeToken:token,
                      }, 
                success: function (data) 
                {
                	 
                	console.log(data);
                      
                  if(data.status==1)
                  {

                   transaction_after_stripe();
                    

                  }
                     
                }
            });

            
        }
    } 



    

});
function stripePopup()
{
	var id  	= $("#plan_id").val();
    var amount 	= $("#plan_amount2").val();
    var number 	= $("#plan_number").val();
    var sku_id 	= $("#sku_id2").val();


	var width = 800;
	var height = 700;
	var left = parseInt((screen.availWidth/2) - (width/2));
	var top = parseInt((screen.availHeight/2) - (height/2));
 
	var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,toolbar,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;
	myWindow = window.open(WEBSITE_URL+"/strip-payment-init?popup=true&plan_id="+id+"&plan_amount="+amount+"&plan_number="+number+"&sku_id="+sku_id,"subWind6Cospace",windowFeatures);
	window.transaction_after_stripe = function ()
{
	    var ajax_url = WEBSITE_URL+"/listproperty/transaction";
        var amount=$("#plan_amount2").val();
        var id=$("#plan_id").val();
        var number=$("#plan_number").val();
        var sku_id 	= $("#sku_id2").val();
        $.ajax({
	            url : ajax_url,
	            method:"post",
              data:{
                amount:amount,
                id:id,
                number:number,
                sku_id:sku_id,
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
			  },
			  beforeSend:function(){
	                startLoader();
	            },
	            complete:function(){
	               stopLoader(); 
	            },
	            success : function(data){
	            	console.log(data);
	                if(data.status){
                        Swal.fire({
	                        title: 'Success!',
	                        text: 'Transaction completed successfully',
	                        icon: 'success',
	                        confirmButtonText: 'Ok'
	                    }).then((result) => {
				            if(result.value){
								$("#StripeCardModal").hide();
								$(".subscribe").hide();
								$(".modal-backdrop").hide();
								$("body").removeClass('modal-open')
								$(".modal").hide();
								$("#myModal").hide();
                                showScreen('step1');
				            }
				        });
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            }
	        });
}
		
}

function stripePopupPremium(id,amount,sku_id='')
{
		//strip-payment-premium-init
		if(amount >0)
		{
			var id  	= id;
			var amount 	= amount;
    


	var width = 800;
	var height = 700;
	var left = parseInt((screen.availWidth/2) - (width/2));
	var top = parseInt((screen.availHeight/2) - (height/2));
 
	var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,toolbar,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;
	myWindow = window.open(WEBSITE_URL+"/strip-payment-premium-init?popup=true&plan_id="+id+"&plan_amount="+amount+"&sku_id="+sku_id,"subWind6CospacePrem",windowFeatures);
	window.premium_after_stripe = function ()
{
	    var ajax_url = WEBSITE_URL+"/list/premium?is_premium="+id+"&plan_amount="+amount+"&sku_id="+sku_id;
            $.ajax({
              url : ajax_url,
              method:"get",
              data:{ 
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}' 
              },
              beforeSend:function(){
                  startLoader();
              },
              complete:function(){
                 stopLoader(); 
              },
              success : function(data){
                  if(data.status){
                        Swal.fire({
                          title: 'Success!',
                          text: 'Transaction completed successfully',
                          icon: 'success',
                          confirmButtonText: 'Ok'
                      }).then((result) => {
                    if(result.value){
                        $(".package").hide();
                        $(".submit_step_8").trigger('click');
                    }
                });
                  }else{
                      Swal.fire({
                          title: 'Error!',
                          text: data.message,
                          icon: 'error',
                          confirmButtonText: 'Ok'
                      });
                  }
              }
          });
}
		}
	
}
/*
function transaction_after_stripe()
{
	    var ajax_url = WEBSITE_URL+"/listproperty/transaction";
        var amount=$("#plan_amount2").val();
        var id=$("#plan_id").val();
        var number=$("#plan_number").val();
        $.ajax({
	            url : ajax_url,
	            method:"post",
              data:{
                amount:amount,
                id:id,
                number:number,
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
			  },
			  beforeSend:function(){
	                startLoader();
	            },
	            complete:function(){
	               stopLoader(); 
	            },
	            success : function(data){
	            	console.log(data);
	                if(data.status){
                        Swal.fire({
	                        title: 'Success!',
	                        text: 'Transaction completed successfully',
	                        icon: 'success',
	                        confirmButtonText: 'Ok'
	                    }).then((result) => {
				            if(result.value){
								$("#StripeCardModal").hide();
								$(".subscribe").hide();
								$(".modal-backdrop").hide();
								$("body").removeClass('modal-open')
								$(".modal").hide();
								$("#myModal").hide();
                                showScreen('step1');
				            }
				        });
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            }
	        });
}*/
</script>
	<!-- @include('frontend.auth.modals.stripe-payment') -->
  @endsection