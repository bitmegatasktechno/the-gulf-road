<section class="buy-rent step6 add-coworking listPropertySteps" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>List your coworkspace.</h3>
                <p class="mb-5">Tell us about the workspace You are about to list.</p>
            </div>
            <div class="col-md-4">
                <ul class="list-inline">
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5>Title & Descreption</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Location</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Amenities</h5>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Operating Hours & Days</h5>
                        </a>
                    </li>
                    <li class=" dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Contact Details</h5>
                        </a>
                    </li>
                    
                    <li class="active dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Pictures</h5>
                            <p>Add pictures to get more leads</p>
                        </a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="javascript:;">
                            <h5 class="mb-0">Pricing</h5>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-8">
                <div class="card list-coworking">
                    <div class="p-4">
                        <h4>Add Pictures</h4>
                    </div>
                    <hr class="mt-0">
                    <div class="p-4 swap_div_form_6" id="swap_div_form_6">
                        <h5>Add minimum 1 Picture of your Coworkspace</h5>
                        <p>This helps the user better know about the Coworkspace and the chances of contacting you increase.</p>
                        <div class="row" id="pictureresult">
                            <div class="upload col-md-3 mb-4">
                                <input type="file" id="files1" name="files[]" class="input-file" onchange="readURL(this)" >
                                <label for="files1">
                                    <img id="files1_src" src="{{asset('img/addmedia-upload.png')}}" class="file_prev" style="height:auto;width:100%;">
                                </label>
                                <span class="invalid-feedback files1"></span>
                            </div>
                            <div class="upload col-md-3 mb-4">
                                <input type="file" id="files2" name="files[]" class="input-file" onchange="readURL(this)" >
                                <label for="files2">
                                    <img id="files2_src" src="{{asset('img/addmedia-upload.png')}}" class="file_prev" style="height:auto;width:100%;">
                                </label>
                                <span class="invalid-feedback files2"></span>
                            </div>
                            <div class="upload col-md-3 mb-4">
                                <input type="file" id="files3" name="files[]" class="input-file" onchange="readURL(this)" >
                                <label for="files3">
                                    <img id="files3_src" src="{{asset('img/addmedia-upload.png')}}" class="file_prev" style="height:auto;width:100%;">
                                </label>
                                <span class="invalid-feedback files3"></span>
                            </div>
                            <div class="upload col-md-3 mb-4">
                                <input type="file" id="files4" name="files[]" class="input-file" onchange="readURL(this)" >
                                <label for="files4">
                                    <img id="files4_src" src="{{asset('img/addmedia-upload.png')}}" class="file_prev" style="height:auto;width:100%;">
                                </label>
                                <span class="invalid-feedback files4"></span>
                            </div>
                            <div class="upload col-md-3 mb-4">
                                <input type="file" id="files5" name="files[]" class="input-file" onchange="readURL(this)" >
                                <label for="files5">
                                    <img id="files5_src" src="{{asset('img/addmedia-upload.png')}}" class="file_prev" style="height:auto;width:100%;">
                                </label>
                                <span class="invalid-feedback files5"></span>
                            </div>
                        </div>
                        <br>
                        <button type="button" id="add_more" class="red-btn rounded" value="Add More Files">Add More</button>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <a class="mt-2 dp-inline-block pt-3 pb-3 mt-5 mb-3" href="javascript:;" onclick="showScreen('step5');">< Go back</a>
                            </div>
                            <div class="col-md-6 text-right">
                                <button type="button" class="submit_step_6 red-btn rounded float-right pt-3 pb-3 mt-5 mb-3">
                                <span>Save & Continue</span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
