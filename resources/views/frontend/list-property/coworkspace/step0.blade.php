@php

$current=date('Y-m-d');
$savedCOSpace = \App\Models\CoworkspaceProperty::where('user_id',\Auth::user()->id)->count();
$users = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','cospace')->where('expiry_date','>=',$current)->where('count','>',$savedCOSpace)->count();
if($users){
    if(@$users > 0){
        $last_transaction = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','cospace')->where('expiry_date','>=',$current)->where('count','>',$savedCOSpace)->latest()->first();
        if($last_transaction->count > $last_transaction->listed){
             
            $package="NO";
        }else{
            $package="YES";
        }
    }else{
        $package="YES";
    }
}else{
     $package="YES";
}
                    @endphp
<section class="buy-rent step0 listPropertySteps" @if($package=='YES') style="display:none" @endif>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3 class="mb-3 pl-0 col-md-8" style="font-size: 32px">List your Coworking Space with us.
</h3>
                <p class="mb-3">List your coworking space with us in a few Simple steps.</p>
                <div class="mt-5">
                    <p><img class="mr-2" src="{{asset('img/tick_with_border.png')}}">Free premium listing for the early members for 15 days.</p>
                    <p><img class="mr-2" src="{{asset('img/tick_with_border.png')}}">List worksapce in easy steps.</p>
                    <p><img class="mr-2" src="{{asset('img/tick_with_border.png')}}">Easily manage your worksapce.</p>
                </div>
                <button class="submit_step_0 red-btn rounded mt-4 pt-2 pb-2" autocomplete="off" type="button" onclick="showScreen('step1');">Get Started <img class="filter-white" src="{{asset('img/black_arrow.png')}}"></button>
            </div>
            <div class="col-md-6">
                <div id="demo" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <li data-target="#demo" data-slide-to="0" class="active"></li>
                        <li data-target="#demo" data-slide-to="1"></li>
                        <li data-target="#demo" data-slide-to="2"></li>
                        <li data-target="#demo" data-slide-to="3"></li>
                    </ul>
                    <!-- The slideshow -->
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="img-fluid" src="{{asset('img/A.png')}}" alt="Los Angeles">
                        </div>
                        <div class="carousel-item">
                            <img class="img-fluid" src="{{asset('img/B.png')}}" alt="Chicago">
                        </div>
                        <div class="carousel-item">
                            <img class="img-fluid" src="{{asset('img/C.png')}}" alt="New York">
                        </div>
                        <div class="carousel-item">
                            <img class="img-fluid" src="{{asset('img/D.png')}}" alt="New York">
                        </div>
                    </div>
                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>