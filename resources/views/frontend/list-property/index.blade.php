@php
$lang=Request::segment(1);
$loc=Request::segment(3);
$modules = \App\Models\Module::where('location',$loc)->get();
 
$link=$loc;
@endphp
@php
$current=date('Y-m-d');
$users = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','user')->where('expiry_date','>=',$current )->count();
if($users){
    if(@$users > 0){
        $package="NO";
        $getPlan = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','user')->where('expiry_date','>=',$current )->first();
}else{
    $package="YES";
    
}
}else{
    $package="YES";
    }

    $checkIfBuddyOrAgent  = \App\Models\User::where('_id',\Auth::user()->id)->whereIn('role',['BUDDY','AGENT'])->first();
@endphp
@extends('frontend.layouts.home')
@section('title','List Property')
@section('content')
@include('frontend.layouts.default-header')
<style type="text/css">
.bodyFilter {
  position: unset !important;
}
.btn.disabled {
    pointer-events: auto;
}
</style>
    <section>
        <div id="register-popup" class="modal1 skipp-page-cls">
            <div class="container">
                <div class="signup-right">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <h3 class="mt-4">Hi! List Your Property for Sale, Rent, Swap or Coworkspace.</h3>
                                <p>List your property with us in Quick easy steps.</p>
                            </div>
                            <div class="col-md-6">
                                <div class="box-section-skip p-4" style="height: 357px;">
                                    <img src="{{asset('img/buyRentListing.png')}}">
                                    <h2>List Property for Sale/Rent</h2>
                                    <p class="mb-2 pb-3 text-left pl-0">List your propery for others to Sale or Rent. We get or property up and Running for sale in no time. List your Property in Few Simple steps.</p>
                                    <!-- @if($property_for_sale_rent)
                                    <a href="{{url(app()->getLocale().'/listproperty/'.$link.'/rent-sale')}}">
                                        <button class="bordered rounded">Get Started</button>
                                    </a>
                                    @else
                                    <button class="bordered rounded" disabled="" title="Please upgrade your plan.">Get Started</button>
                                    @endif -->
                                    <a href="{{url(app()->getLocale().'/listproperty/'.$link.'/rent-sale')}}">
                                     <button class="bordered rounded">Get Started</button>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="box-section-skip p-4">
                                    <img src="{{asset('img/workspace.png')}}">
                                    <h2>List your Property for Coworkspace</h2>
                                    <p class="mb-2 pb-3 text-left pl-0"> Coworking offers sharing of equipment, resources, ideas and experiences among remote professionals. This typically results in increased productivity.</p>
                                    <a href="{{url(app()->getLocale().'/listproperty/'.$link.'/coworkspace')}}">
                                        <button class="bordered rounded">Get Started</button>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="box-section-skip p-4">
                                    <img src="{{asset('img/swapListing.png')}}">
                                    <h2>List your House for Swap</h2>
                                    <p class="mb-2 pb-3 text-left pl-0">Going out Somwhere? Just list your house for swap with us and find people who are available to swap with You in Simple Easy steps.</p>
                                    <a href="{{url(app()->getLocale().'/listproperty/'.$link.'/swap')}}">
                                        <button class="bordered rounded">Get Started</button>
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="box-section-skip p-4" style="height: 332px;">
                                <img src="{{asset('img/Elevate_profile.png')}}" style="width: 54px;">
                                    <h2>Elevate Your Profile</h2>
                                    <p class="mb-2 pb-3 text-left pl-0">Elevate your profile to reach out a wider audience and get your listings be featured on our website.</p>

        @if(@$package=='YES')
        @if((isset($checkIfBuddyOrAgent->defaultLoginRole) && $checkIfBuddyOrAgent->defaultLoginRole =='AGENT')  || (isset($checkIfBuddyOrAgent->defaultLoginRole) && $checkIfBuddyOrAgent->defaultLoginRole =='BUDDY'))
        <a href="javascript:" onclick="continueWithDefaultRole('{{$checkIfBuddyOrAgent->defaultLoginRole}}')"><button class="bordered rounded">Get Started</button></a>

         
        @elseif((isset($checkIfBuddyOrAgent->defaultLoginRole) && $checkIfBuddyOrAgent->defaultLoginRole =='USER'))
        <a href="javascript:" onclick="alertChoseDefaultRole()"><button class="bordered rounded">Get Started</button></a>
        @else 
        <a href="javascript:" onclick="showBuddyAgentNoti()"><button class="bordered rounded">Get Started</button></a>
        @endif

        @else
         <a href="{{url(app()->getLocale().'/profile/'.$link)}}"> <button class="bordered rounded">Get Started</button></a>
         @endif
                                   <!--  @if(@$package=='YES')

                                    <a href="{{url(app()->getLocale().'/request/'.$link.'/subscription')}}">
                                        @else
                                        <a href="{{url(app()->getLocale().'/profile/'.$link)}}">
                                        @endif
                                        <button class="bordered rounded">Get Started</button>
                                    </a> -->
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="box-section-skip p-4" style="height: 357px;">
                                    <img src="{{asset('img/buyRentListing.png')}}">
                                    <h2>List Bulk Property for Sale, Rent, Swap, Co-space</h2>
                                    <p class="mb-2 pb-3 text-left pl-0">List your propery in bulk to Sale, Rent, Swap, Co-space</p>
                                     
                                    <a href="{{url(app()->getLocale().'/contact/'.$link)}}">
                                     <button class="bordered rounded">Get Started</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript" defer>
 
   
    function showBuddyAgentNoti()
    {
        var buddyUrl = "{{url(app()->getLocale().'/request/'.$link.'/subscription/buddy')}}";
        var agentUrl = "{{url(app()->getLocale().'/request/'.$link.'/subscription/agent')}}";
         Swal.fire({
              title: 'Before Elevate Your Profile.',
               html:
    'Click here become <b>' +
    '<a href="'+buddyUrl+'">Buddy</a> ' +
    '</b>,<b>' +
    '<a href="'+agentUrl+'">Agent</a> ' +
    '</b>',
              showCancelButton: true,
              cancelButtonText: 'CANCEL',
              reverseButtons: true,
              showCloseButton: true,
            }) 
    }
    function continueWithDefaultRole(val)
    {
         Swal.fire({
              title: 'Elevate Your Profile AS '+val,
              showCancelButton: true,
              confirmButtonText: 'OK',
              cancelButtonText: 'CANCEL',
              reverseButtons: true,
              showCloseButton: true,
            }).then((result) => {
             if (result.value) {
              var url = "{{url(app()->getLocale().'/request/'.$link.'/subscription')}}";
              window.location.replace(url);
            } 
            });
    }
    function alertChoseDefaultRole(val)
    {
         Swal.fire({
              title: 'Before Elevate Your Profile Active Profile Buddy or Agent',
              showCancelButton: true,
              confirmButtonText: 'OK',
              cancelButtonText: 'CANCEL',
              reverseButtons: true,
              showCloseButton: true,
            });
    }
</script>
        
    @include('frontend.layouts.footer')

@endsection