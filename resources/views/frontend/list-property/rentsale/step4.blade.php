<section class="buy-rent step4 listPropertySteps" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3 class="mb-3">Describe Your Property</h3>
                <p class="mb-5">We need a small description of your property. List the features that are unique to your property.</p>
                <h5>Title & Description</h5>
                <p>Give your listing a title & a short description.</p>
                <div  name="rent_sale_property_step_4" id="rent_sale_property_step_4">
                    
                    <div class="loc-form">
                        <input class="form-control" type="text" name="property_title" placeholder="“Fully furnished house”">
                        <span class="invalid-feedback property_title"></span>

                        <textarea class="form-control mt-4 mb-3" rows="6" placeholder="“My home is located near the lake and has a beautiful view of the whole town.”" name="property_description"></textarea>
                        <span class="invalid-feedback property_description"></span>
                    </div>

                    <div class="house_rules">
                        <h5>House Rules</h5>
                        <p>Is smoking allowed inside the house?</p>
                    
                        <div class="form-group">
                            <div class="radio dp-inline-block mr-4" style="margin-bottom: 0;">
                                <input id="smokingradio-1" name="smoking_allowed" type="radio" value="yes" autocomplete="off">
                                <label for="smokingradio-1" class="radio-label">Yes</label>
                            </div>
                            <div class="radio dp-inline-block" style="margin-bottom: 0;">
                                <input id="smokingradio-2" checked="" name="smoking_allowed" type="radio" value="no" autocomplete="off">
                                <label for="smokingradio-2" class="radio-label">No</label>
                            </div><br>
                            <span class="invalid-feedback smoking_allowed"></span>
                        </div>
                    
                        <p>Are parties allowed?</p>
                        <div class="form-group">
                            <div class="radio dp-inline-block mr-4" style="margin-bottom: 0;">
                                <input id="partiesradio-3" name="parties_allowed" value="yes" type="radio" autocomplete="off">
                                <label for="partiesradio-3" class="radio-label">Yes</label>
                            </div>
                            <div class="radio dp-inline-block" style="margin-bottom: 0;">
                                <input id="partiesradio-4" checked="" name="parties_allowed" value="no" type="radio" autocomplete="off">
                                <label for="partiesradio-4" class="radio-label">No</label>
                            </div><br>
                            <span class="invalid-feedback parties_allowed"></span>
                        </div>
                    
                        <p>Are pets allowed?</p>
                        
                        <div class="form-group">
                            <div class="radio dp-inline-block mr-4" style="margin-bottom: 0;">
                                <input id="petsradio-5" name="pets_allowed" type="radio" value="yes" autocomplete="off">
                                <label for="petsradio-5" class="radio-label">Yes</label>
                            </div>
                            <div class="radio dp-inline-block" style="margin-bottom: 0;">
                                <input id="petsradio-6" checked=""  name="pets_allowed" type="radio" value="no" autocomplete="off">
                                <label for="petsradio-6" class="radio-label">No</label>
                            </div><br>
                            <span class="invalid-feedback pets_allowed"></span>
                        </div>

                        <h5>Additional rules</h5>
                        <div class="loc-form">
                            <textarea class="form-control mt-4 mb-3" rows="6" placeholder="“Enter additional rules if exists”" name="additional_rules"></textarea>
                            <span class="invalid-feedback additional_rules"></span>
                        </div>
                    </div>

                    <div class="pt-3 pb-3">
                        <hr>
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <a class="mt-2 dp-inline-block" href="javascript:;" onclick="showScreen('step3');">< Go back</a>
                        </div>
                        <div class="col-md-6 text-right">
                            <button class="red-btn rounded float-right submit_step_4" type="button">
                                Continue 
                                <img class="filter-white" src="{{asset('img/black_arrow.png')}}">
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="offset-md-1 col-md-6">
                <img class="img-responsive" src="{{asset('img/C.png')}}">
            </div>
        </div>
    </div>
</section>