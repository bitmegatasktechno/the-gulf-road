<section class="buy-rent step5 listPropertySteps" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="mb-3">Add Pictures & Videos</h3>
                <p class="mb-3">Add Pictures of your House. This helps the user better know about the property and the chances of contacting you increase.</p>
                
                <div class="boxed second" name="rent_sale_property_step_5" id="rent_sale_property_step_5">
                     
                    <div class="row" id="pictureresult">
                        <div class="upload col-md-2 ">
                            <input type="file" id="files1" name="files[]" class="input-file" onchange="readURL(this)" accept='image/*'>
                            <label for="files1" class="p-0">
                                <img id="files1_src" src="{{asset('img/addmedia-upload.png')}}" style="height:100%;width:100%;">
                            </label>
                            <span class="invalid-feedback files1"></span>
                        </div>
                        <div class="upload col-md-2 ">
                            <input type="file" id="files2" name="files[]" class="input-file" onchange="readURL(this)" accept='image/*'>
                            <label for="files2" class="p-0">
                                <img id="files2_src" src="{{asset('img/addmedia-upload.png')}}" style="height:100%;width:100%;">
                            </label>
                            <span class="invalid-feedback files2"></span>
                        </div>
                        <div class="upload col-md-2 ">
                            <input type="file" id="files3" name="files[]" class="input-file" onchange="readURL(this)" accept='image/*'>
                            <label for="files3" class="p-0">
                                <img id="files3_src" src="{{asset('img/addmedia-upload.png')}}" style="height:100%;width:100%;">
                            </label>
                            <span class="invalid-feedback files3"></span>
                        </div>
                        <div class="upload col-md-2 ">
                            <input type="file" id="files4" name="files[]" class="input-file" onchange="readURL(this)" accept='image/*'>
                            <label for="files4" class="p-0">
                                <img id="files4_src" src="{{asset('img/addmedia-upload.png')}}" style="height:100%;width:100%;">
                            </label>
                            <span class="invalid-feedback files4"></span>
                        </div>
                        <div class="upload col-md-2 ">
                            <input type="file" id="files5" name="files[]" class="input-file" onchange="readURL(this)" accept='image/*'>
                            <label for="files5" class="p-0">
                                <img id="files5_src" src="{{asset('img/addmedia-upload.png')}}" style="height:100%;width:100%;">
                            </label>
                            <span class="invalid-feedback files5"></span>
                        </div>

                    </div>
                    <br>
                    <button type="button" id="add_more" class="red-btn rounded" value="Add More Files">Add More</button>
                </div>

                <div class="pt-5 pb-3">
                    <hr>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <a class="mt-2 dp-inline-block" href="javascript:;" onclick="showScreen('package');">< Go back</a>
                    </div>
                    <div class="col-md-6 text-right">
                        <a class="theme-color mr-3" href="javascript:;" onclick="showSkipScreen('step6');">Skip</a> 
                        <button class="red-btn rounded submit_step_5" type="button">
                            Continue 
                            <img class="filter-white" src="{{asset('img/black_arrow.png')}}">
                        </button>
                    </div>
                </div>
            </div>
            <!-- <div class="col-md-6">
                <img class="img-responsive" src="{{asset('img/D.png')}}">
            </div> -->
        </div>
    </div>
</section>