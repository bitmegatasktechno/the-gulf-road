@php
$lang=Request::segment(1);
$loc=Request::segment(3);
if($loc == 'ksa' || $loc == 'uae' )
{
   $loc21 = strtoupper($loc);
}else 
{
    $loc21 = ucfirst($loc);
}
$link=$loc;
 $type=Request::segment(4);
$modules = \App\Models\Module::where('location', $loc21)->get();
$link=$loc;
 
$current=date('Y-m-d');
$users = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','rent')->where('expiry_date','>=',$current )->count();
if($users){
	if(@$users > 0){
		$last_transaction = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','rent')->where('expiry_date','>=',$current )->latest()->first();
		if($last_transaction->count > $last_transaction->listed){
			Session::put('transaction_id',$last_transaction->_id);
			$package="NO";
		}else{
			$package="YES";
		}
    }else{
        $package="YES";
    }
}else{
    $package="YES";
}


@endphp
@extends('frontend.layouts.home')
@section('title','Rent Or Sale Your Property')
<script>
  var pay_redir ='rent_request';
</script>

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/datepicker.css')}}">
@endsection
@section('content')
    
    @include('frontend.layouts.default-header')


<style type="text/css">
        
.bodyFilter {
  position: unset !important;
}

</style>
 
    <input type="hidden" value="{{$link}}" id="link">
    <form class="loc-form" name="rent_sale_master_form" id="rent_sale_master_form" enctype="multipart/form-data">
		@csrf
		@if($package=='YES')
		@include('frontend.list-property.subscription') 
		@endif
	    @include('frontend.list-property.rentsale.step1')
	    @include('frontend.list-property.rentsale.step2')
	    @include('frontend.list-property.rentsale.step3')
	    @include('frontend.list-property.rentsale.step4')
	    @include('frontend.list-property.rentsale.step5')
	    @include('frontend.list-property.rentsale.step6')
		@include('frontend.others.package')

		<div id="listPropertyErrors" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<ul class="listProperty_errors_points">
							
						</ul>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</form>
    
    @include('frontend.layouts.footer')

@endsection

@section('scripts')
<script type="text/javascript" src="{{asset('js/datepicker.js')}}"></script>
<script type="text/javascript">

	function file_validation(id,max_size)
   {
       var fuData = document.getElementById(id);
       var FileUploadPath = fuData.value;
       
   
       if (FileUploadPath == ''){
           alert("Please upload Attachment");
       } 
       else {
           var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
           if (Extension == "jpg" || Extension == "jpeg" || Extension == "png" || Extension == "webp" ) {
   
                   if (fuData.files && fuData.files[0]) {
                       var size = fuData.files[0].size;
                       
                       if(size > max_size){   //1000000 = 1 mb
                           
                           Swal.fire({
			                    title: 'Warning!',
			                    text: "Maximum file size "+max_size/1000000+" MB",
			                    icon: 'info',
			                    confirmButtonText: 'Ok'
			                });
			                
                           $("#"+id).val('');
                           return false;
                            
                       }
                   }
   
           } 
           else 
           {
                
               $("#"+id).val('');
                Swal.fire({
			                    title: 'Warning!',
			                    text: "document only allows file types of  jpg , png  , jpeg , webp ",
			                    icon: 'info',
			                    confirmButtonText: 'Ok'
			                });
			                
                           $("#"+id).val('');
                           return false;
           }
       }   
   }   

var link=$("#link").val();
	let propertySteps = false;
	let imageSkip = false;
	function readURL(input) {
		var id = $(input).attr('id');
		 var max_size = 2000000;
           file_validation(id,max_size);

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
            	var id=input.id;
            	$('#'+id+'_src').attr('src', e.target.result);
		    }
            reader.readAsDataURL(input.files[0]);
            
        }
    }
    const showScreen = function(step){
    	if(step=='step6'){
    		var val = $('input[name="listing_type"]:checked').val();
    		if(val=='rent'){
    			$('.rent_price_list').show();
    			$('.sale_price_list').hide();
    			$('.step6_divided').hide();
    		}
    		else if(val=='sale'){
    			$('.rent_price_list').hide();
    			$('.sale_price_list').show();
    			$('.step6_divided').hide();
    		}else{
    			$('.rent_price_list').show();
    			$('.sale_price_list').show();
    			$('.step6_divided').show();
    		}
    	}

		$('.listPropertySteps').hide();
		$('.'+step).show();
		$("html, body").animate({ scrollTop: 0 }, "slow");
	}

	const showSkipScreen = function(step){
		imageSkip = true;
    	var val = $('input[name="listing_type"]:checked').val();
		if(val=='rent'){
			$('.rent_price_list').show();
			$('.sale_price_list').hide();
			$('.step6_divided').hide();
		}
		else if(val=='sale'){
			$('.rent_price_list').hide();
			$('.sale_price_list').show();
			$('.step6_divided').hide();
		}else{
			$('.rent_price_list').show();
			$('.sale_price_list').show();
			$('.step6_divided').show();
		}

		$('.listPropertySteps').hide();
		$('.'+step).show();
		$("html, body").animate({ scrollTop: 0 }, "slow");
	}
	const toggleInstantBooking = function(divid){
		if($('#'+divid).is(':visible')){
			$('#'+divid).hide();
		}else{
			$('#'+divid).show();
		}
	}

	const parkingAvailableRadio = function(input){
		if(input.value=='no'){
            $('.parking_available_div').hide();
        }
        else if(input.value=='yes'){
            $('.parking_available_div').show();
        }
	}

	
	$(document).ready(function()
	{



		$('.datepicker').datepicker({
			'startDate': new Date(),
			'format': 'dd/mm/yyyy',
			'autoHide':true
		});

		$(document).on('click','.amenitiesBlock', function(){
			$(this).toggleClass('active');
		});

		$(document).on('click','#add_more', function(){
			if($('.upload').length>=15){
				Swal.fire({
                    title: 'Warning!',
                    text: "You can upload maximum 15 pictures.",
                    icon: 'info',
                    confirmButtonText: 'Ok'
                });
                return false;
			}
			var id= $('.upload').length+1;
			$("#pictureresult").append('\
                <div class="upload col-md-2">\
                    <input type="file" id="files'+id+'" name="files[]" class="input-file" onchange="readURL(this)" >\
                    <label for="files'+id+'" class="p-0">\
                        <img id="files'+id+'_src" src="{{asset("img/addmedia-upload.png")}}" style="height:100%;width:100%;">\
                    </label>\
                    <span class="remove removeDiv" style="cursor:pointer;"><i class="fa fa-times" aria-hidden="true"></i></span>\
                    <span class="invalid-feedback files'+id+'"></span>\
                </div>');
		});

		$(document).on('click','.removeDiv', function(){
			$(this).parent().remove();
		});

		$(".minus").click(function() {
			var $input = $(this).parent().find("input");
			var count = parseInt($input.val()) - 1;
			count = count < 1 ? 0 : count;
			$input.val(count);
			$input.change();
			return false;
		});

		$(".plus").click(function() {
			var $input = $(this).parent().find("input");
			if(parseInt($input.val())>=10){
				return false;
			}
			$input.val(parseInt($input.val()) + 1);
			$input.change();
			return false;
		});

		$(window).bind('beforeunload',function() {
	        if(!propertySteps){
                return "'Are you sure you want to leave the page. All data will be lost!";
            }
	    });


		$(document).on('click','.submit_step_1', function(){
	        var ajax_url = WEBSITE_URL+"/listproperty/rentsale/1";
	        var $form = $('#rent_sale_property_step_1');
	        var data = new FormData();
	        var listing_type = $('input[name="listing_type"]:checked').val() ? $('input[name="listing_type"]:checked').val(): '';
	        var house_type = $('input[name="house_type"]:checked').val() ? $('input[name="house_type"]:checked').val(): '';

	        data.append('listing_type', listing_type);
	        data.append('house_type', house_type);
	        data.append('location_name', $('input[name="location_name"]').val());
	        data.append('lat', $('input[name="lat"]').val());
	        data.append('longitude', $('input[name="longitude"]').val());
	        data.append('house_number', $('input[name="house_number"]').val());
	        data.append('landmark', $('input[name="landmark"]').val());
	        data.append('country', $('select[name="country"]').val());
	        data.append('city', $('select[name="city"]').val());
	        
	        $form.find('.is-invalid').removeClass('is-invalid');
	        $form.find('.invalid-feedback').text('');

	        $.ajax({
	            url : ajax_url,
	            type:'post',
	            data : data,
	            dataType : 'json',
	            processData: false,
	            contentType: false,
	            beforeSend:function(){
	                startLoader('.listPropertySteps');
	            },
	            complete:function(){
	               stopLoader('.listPropertySteps'); 
	            },
	            success : function(data){
	                if(data.status){
	                    showScreen('step2');
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            },
	            error : function(data){
	                stopLoader('.listPropertySteps');
	                if(data.responseJSON){
	                    var err_response = data.responseJSON;  
	                    if(err_response.errors==undefined && err_response.message) {
	                        Swal.fire({
	                            title: 'Error!',
	                            text: err_response.message,
	                            icon: 'error',
	                            confirmButtonText: 'Ok'
	                        });
	                    }
	                    $.each(err_response.errors, function(i, obj){
	                        $form.find('span.invalid-feedback.'+i+'').text(obj).show();
	                        $form.find('input[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('select[name="'+i+'"]').addClass('is-invalid');
	                    });
	                }
	            }
	        });
	    });

	    $(document).on('click','.submit_step_2', function(){
	        var ajax_url = WEBSITE_URL+"/listproperty/rentsale/2";
	        var $form = $('#rent_sale_property_step_2');
	        var data = new FormData();

	        var furnishing_type = $('input[name="furnishing_type"]:checked').val() ? $('input[name="furnishing_type"]:checked').val(): '';
	        var parking_available = $('input[name="parking_available"]:checked').val() ? $('input[name="parking_available"]:checked').val(): '';

	        data.append('furnishing_type', furnishing_type);
	        data.append('parking_available', parking_available);
	        data.append('build_up_area', $('input[name="build_up_area"]').val());
	        data.append('carpet_area', $('input[name="carpet_area"]').val());
	        data.append('no_of_bedrooms', $('input[name="no_of_bedrooms"]').val());
	        data.append('no_of_bathrooms', $('input[name="no_of_bathrooms"]').val());
	        data.append('no_of_covered_parking', $('input[name="no_of_covered_parking"]').val());
	        data.append('no_of_open_parking', $('input[name="no_of_open_parking"]').val());
	        data.append('flooring_type', $('select[name="flooring_type"]').val());
	        data.append('build_up_area_unit', $('select[name="build_up_area_unit"]').val());
	        data.append('carpet_area_unit', $('select[name="carpet_area_unit"]').val());

	        $form.find('.is-invalid').removeClass('is-invalid');
	        $form.find('.invalid-feedback').text('');

	        $.ajax({
	            url : ajax_url,
	            type:'post',
	            data : data,
	            dataType : 'json',
	            processData: false,
	            contentType: false,
	            beforeSend:function(){
	                startLoader('.listPropertySteps');
	            },
	            complete:function(){
	               stopLoader('.listPropertySteps'); 
	            },
	            success : function(data){
	                if(data.status){
	                    showScreen('step3');
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            },
	            error : function(data){
	                stopLoader('.listPropertySteps');
	                if(data.responseJSON){
	                    var err_response = data.responseJSON;  
	                    if(err_response.errors==undefined && err_response.message) {
	                        Swal.fire({
	                            title: 'Error!',
	                            text: err_response.message,
	                            icon: 'error',
	                            confirmButtonText: 'Ok'
	                        });
	                    }
	                    $.each(err_response.errors, function(i, obj){
	                        $form.find('span.invalid-feedback.'+i+'').text(obj).show();
	                        $form.find('input[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('select[name="'+i+'"]').addClass('is-invalid');
	                    });
	                }
	            }
	        });
	    });

	    $(document).on('click','.submit_step_3', function(){
	        var ajax_url = WEBSITE_URL+"/listproperty/rentsale/3";
	        var data = new FormData();

	        $('.amenitiesBlock').each(function(i){
                if($(this).is('.active')){
                    data.append('amenties[]', $(this).attr('data-name'));
                }
            });

	        $.ajax({
	            url : ajax_url,
	            type:'post',
	            data : data,
	            dataType : 'json',
	            processData: false,
	            contentType: false,
	            beforeSend:function(){
	                startLoader('.listPropertySteps');
	            },
	            complete:function(){
	               stopLoader('.listPropertySteps'); 
	            },
	            success : function(data){
	                if(data.status){
	                	if($('input[name="listing_type"]:checked').val()=='rent'){
			    			$('.house_rules').show();
			    		}
			    		if($('input[name="listing_type"]:checked').val()=='sale'){
			    			$('.house_rules').hide();
			    		}else{
			    			$('.house_rules').show();
			    		}
	                    showScreen('step4');
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            },
	            error : function(data){
	                stopLoader('.listPropertySteps');
	                if(data.responseJSON){
	                    var err_response = data.responseJSON;  
	                    if(err_response.errors==undefined && err_response.message) {
	                        Swal.fire({
	                            title: 'Error!',
	                            text: err_response.message,
	                            icon: 'error',
	                            confirmButtonText: 'Ok'
	                        });
	                    }
	                }
	            }
	        });
	    });
		$(document).on('click','.submit_step_8', function(){
			$(".package").hide();
			showScreen('step5');
		});
	    $(document).on('click','.submit_step_4', function(){
	        var ajax_url = WEBSITE_URL+"/listproperty/rentsale/4";
	        var $form = $('#rent_sale_property_step_4');

	        var data = new FormData();

	        var smoking_allowed = $('input[name="smoking_allowed"]:checked').val() ? $('input[name="smoking_allowed"]:checked').val(): '';
	        var parties_allowed = $('input[name="parties_allowed"]:checked').val() ? $('input[name="parties_allowed"]:checked').val(): '';
	        var pets_allowed = $('input[name="pets_allowed"]:checked').val() ? $('input[name="pets_allowed"]:checked').val(): '';

	        data.append('smoking_allowed', smoking_allowed);
	        data.append('pets_allowed', pets_allowed);
	        data.append('parties_allowed', parties_allowed);
	        data.append('property_title', $('input[name="property_title"]').val());
	        data.append('property_description', $('textarea[name="property_description"]').val());
	        data.append('additional_rules', $('textarea[name="additional_rules"]').val());

	        $form.find('.is-invalid').removeClass('is-invalid');
	        $form.find('.invalid-feedback').text('');

	        $.ajax({
	            url : ajax_url,
	            type:'post',
	            data : data,
	            dataType : 'json',
	            processData: false,
	            contentType: false,
	            beforeSend:function(){
	                startLoader('.listPropertySteps');
	            },
	            complete:function(){
	               stopLoader('.listPropertySteps'); 
	            },
	            success : function(data){
	                if(data.status){
						$(".step4").hide();
	                    showScreen('package');
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            },
	            error : function(data){
	                stopLoader('.listPropertySteps');
	                if(data.responseJSON){
	                    var err_response = data.responseJSON;  
	                    if(err_response.errors==undefined && err_response.message) {
	                        Swal.fire({
	                            title: 'Error!',
	                            text: err_response.message,
	                            icon: 'error',
	                            confirmButtonText: 'Ok'
	                        });
	                    }
	                    $.each(err_response.errors, function(i, obj){
	                        $form.find('span.invalid-feedback.'+i+'').text(obj).show();
	                        $form.find('input[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('select[name="'+i+'"]').addClass('is-invalid');
	                    });
	                }
	            }
	        });
	    });

	    $(document).on('click','.submit_step_5', function(){
	        var ajax_url = WEBSITE_URL+"/listproperty/rentsale/5";
	        var $form = $('#rent_sale_property_step_5');
	        var data = new FormData();
	        var error = false;
	        $form.find('.is-invalid').removeClass('is-invalid');
	        $form.find('.invalid-feedback').text('');
	        
	        $.each($('.listPropertySteps [type=file]'), function(index, file) {
	        	if($('input[type=file]')[0].files[0]==undefined){
	        		var id = $('input[type=file]')[0].id;
                    $('.invalid-feedback.'+(id)).text('The file is required.').show();
	        		error = true;
	        	}
	        	
				data.append('files[]', $('input[type=file]')[0].files[0] ? $('input[type=file]')[0].files[0]: '');
			});

			if(error){
				return false;
			}

	        

	        $.ajax({
	            url : ajax_url,
	            type:'post',
	            data : data,
	            enctype: 'multipart/form-data',
	            dataType : 'json',
	            processData: false,
	            contentType: false,
	            beforeSend:function(){
	                startLoader('.listPropertySteps');
	            },
	            complete:function(){
	               stopLoader('.listPropertySteps'); 
	            },
	            success : function(data){
	                if(data.status){
	                    showScreen('step6');
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            },
	            error : function(data){
	                stopLoader('.listPropertySteps');
	                if(data.responseJSON){
	                    var err_response = data.responseJSON;  
	                    if(err_response.errors==undefined && err_response.message) {
	                        Swal.fire({
	                            title: 'Error!',
	                            text: err_response.message,
	                            icon: 'error',
	                            confirmButtonText: 'Ok'
	                        });
	                    }
	                    $.each(err_response.errors, function(key, value){
	                        if(key.indexOf('.') != -1) {
                                let keys = key.split('.');                                
                                $('input[name="'+keys[0]+'[]"]').eq(keys[1]).addClass('is-invalid').parent().find('.invalid-feedback').text(value).show();                         
                            }
                            else {
                                $('input[name="'+key+'[]"]').eq(0).addClass('is-invalid').parent().find('.invalid-feedback').text(value).show();
                            }
	                    });
	                }
	            }
	        });
	    });

	    $(document).on('click','.submit_step_6', function(){
	        var ajax_url = WEBSITE_URL+"/listproperty/rentsale/6";
	        var $form = $('#rent_sale_property_step_6');
	        var data = new FormData();

	        $form.find('.is-invalid').removeClass('is-invalid');
	        $form.find('.invalid-feedback').text('');
	        var listing_type = $('input[name="listing_type"]:checked').val();

	        data.append('listing_type', listing_type);

	        if(listing_type=='rent')
	        {
	        	var instant_book_available = $('input[name="instant_book_available"]:checked').attr('value') ? $('input[name="instant_book_available"]:checked').attr('value'): '';
				data.append('instant_book_available', instant_book_available);
				data.append('night_price', $('input[name="night_price"]').val());
				data.append('rent', $('input[name="rent"]').val());
				data.append('maintenance_charges', $('input[name="maintenance_charges"]').val());
				data.append('security_amount', $('input[name="security_amount"]').val());
				data.append('available_from', $('input[name="available_from"]').val());
	        }
	        else if(listing_type=='sale'){
	        	var discount_available = $('input[name="discount_available"]:checked').attr('value') ? $('input[name="discount_available"]:checked').attr('value'): '';
	        	data.append('discount_available', discount_available);
	        	data.append('price', $('input[name="price"]').val());
	        	data.append('discount_price', $('input[name="discount_price"]').val());
	        }else{
	        	var instant_book_available = $('input[name="instant_book_available"]:checked').attr('value') ? $('input[name="instant_book_available"]:checked').attr('value'): '';
				data.append('instant_book_available', instant_book_available);
				data.append('night_price', $('input[name="night_price"]').val());
				data.append('rent', $('input[name="rent"]').val());
				data.append('maintenance_charges', $('input[name="maintenance_charges"]').val());
				data.append('security_amount', $('input[name="security_amount"]').val());
				data.append('available_from', $('input[name="available_from"]').val());

	        	var discount_available = $('input[name="discount_available"]:checked').attr('value') ? $('input[name="discount_available"]:checked').attr('value'): '';
	        	data.append('discount_available', discount_available);
	        	data.append('price', $('input[name="price"]').val());
	        	data.append('discount_price', $('input[name="discount_price"]').val());
	        }

	        data.append('price_currency', $('select[name="price_currency"]').val());
	        data.append('discount_price_currency', $('select[name="discount_price_currency"]').val());
	        data.append('rent_currency', $('select[name="rent_currency"]').val());
	        data.append('maintenance_charges_currency', $('select[name="maintenance_charges_currency"]').val());
	        data.append('security_amount_currency', $('select[name="security_amount_currency"]').val());
	        data.append('night_price_currency', $('select[name="night_price_currency"]').val());
	        data.append('rent_frequency', $('select[name="rent_frequency"]').val());

	        $.ajax({
	            url : ajax_url,
	            type:'post',
	            data : data,
	            dataType : 'json',
	            processData: false,
	            contentType: false,
	            beforeSend:function(){
	                startLoader('.listPropertySteps');
	            },
	            complete:function(){
	               
	            },
	            success : function(data){
	                if(data.status){
	                    AddProperty();
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                    stopLoader('.listPropertySteps');
	                }
	            },
	            error : function(data){
	                stopLoader('.listPropertySteps');
	                if(data.responseJSON){
	                    var err_response = data.responseJSON;  
	                    if(err_response.errors==undefined && err_response.message) {
	                        Swal.fire({
	                            title: 'Error!',
	                            text: err_response.message,
	                            icon: 'error',
	                            confirmButtonText: 'Ok'
	                        });
	                    }
	                    $.each(err_response.errors, function(i, obj){
	                        $form.find('span.invalid-feedback.'+i+'').text(obj).show();
	                        $form.find('input[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
	                        $form.find('select[name="'+i+'"]').addClass('is-invalid');
	                    });
	                }
	            }
	        });
	    });

	    const AddProperty = function(){
	    	var ajax_url = WEBSITE_URL+"/listproperty/rentsale/7";
	        
	        var $form = $('#rent_sale_master_form');
	        var data = new FormData($form[0]);

	        $form.find('.is-invalid').removeClass('is-invalid');
	        $form.find('.invalid-feedback').text('');
	        
	        var instant_book_available = $('input[name="instant_book_available"]:checked').attr('value') ? $('input[name="instant_book_available"]:checked').attr('value'): '';
	        var discount_available = $('input[name="discount_available"]:checked').attr('value') ? $('input[name="discount_available"]:checked').attr('value'): '';
	        data.append('instant_book_available', instant_book_available);
	        data.append('discount_available', discount_available);
	        
	        $('.amenitiesBlock').each(function(i){
                if($(this).is('.active')){
                    data.append('amenties[]', $(this).attr('data-name'));
                }
            });

            if(imageSkip){
	        	data.append('image_skip', 1);
	        }else{
	        	data.append('image_skip',0);
	        }

	        $.ajax({
	            url : ajax_url,
	            type:'post',
	            data : data,
	            dataType : 'json',
	            processData: false,
	            contentType: false,
	            beforeSend:function(){
	                startLoader('.listPropertySteps');
	            },
	            complete:function(){
	               
	            },
	            success : function(data){
	                if(data.status){
	                	propertySteps = true;
	                	$('.submit_step_6').attr('disabled',true);
	                	
	                    Swal.fire({
	                        title: 'Success!',
	                        text: data.message,
	                        icon: 'success',
	                        confirmButtonText: 'Ok'
	                    }).then((result) => {
				            if(result.value){
				                window.location =data.url;
				            }else{
				            	window.location =data.url;
				            }
				        });
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                    stopLoader('.listPropertySteps'); 
	                }
	            },
	            error : function(data){
	                stopLoader('.listPropertySteps');
	                if(data.responseJSON){
	                    var err_response = data.responseJSON;  
	                    if(err_response.errors==undefined && err_response.message) {
	                        Swal.fire({
	                            title: 'Error!',
	                            text: err_response.message,
	                            icon: 'error',
	                            confirmButtonText: 'Ok'
	                        });
	                    }
	                    var html='';
	                    $.each(err_response.errors, function(i, obj){
	                        html+='<li>'+obj+'</li>';
	                    });
	                    if(html){
	                    	$('.listProperty_errors_points').html(html);
	                    	$('#listPropertyErrors').modal('toggle');
	                    }
	                }
	            }
	        });
	    }
	});
</script>

<script type="text/javascript">
function selectPlan(id,amount,number,sku_id=''){
    $("#plan_id22").val(id);
    $("#plan_amount22").val(amount);
    $("#plan_number22").val(number);
    $("#sku_id2").val(sku_id);
}


function stripePopup()
{
	var id  = $("#plan_id22").val();
    var amount = $("#plan_amount22").val();
    var number = $("#plan_number22").val();
     var sku_id 	= $("#sku_id2").val();

	var width = 800;
	var height = 700;
	var left = parseInt((screen.availWidth/2) - (width/2));
	var top = parseInt((screen.availHeight/2) - (height/2));
 
	var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,toolbar,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;
	myWindow = window.open(WEBSITE_URL+"/strip-payment-init?popup=true&plan_id="+id+"&plan_amount="+amount+"&plan_number="+number+"&sku_id="+sku_id,"subWind6RantSale",windowFeatures);
	window.transaction_after_stripe = function ()
{
	    var ajax_url = WEBSITE_URL+"/listproperty/transaction";
        var amount=$("#plan_amount2").val();
        var id=$("#plan_id").val();
        var number=$("#plan_number").val();
        var sku_id 	= $("#sku_id2").val();

        $.ajax({
	            url : ajax_url,
	            method:"post",
              data:{
                amount:amount,
                id:id,
                number:number,
                sku_id:sku_id,
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
			  },
			  beforeSend:function(){
	                startLoader();
	            },
	            complete:function(){
	               stopLoader(); 
	            },
	            success : function(data){
	            	console.log(data);
	                if(data.status){
                        Swal.fire({
	                        title: 'Success!',
	                        text: 'Transaction completed successfully',
	                        icon: 'success',
	                        confirmButtonText: 'Ok'
	                    }).then((result) => {
				            if(result.value){
								$("#StripeCardModal").hide();
								$(".subscribe").hide();
								$(".modal-backdrop").hide();
								$("body").removeClass('modal-open')
								$(".modal").hide();
								$("#myModal").hide();
                                showScreen('step1');
				            }
				        });
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            }
	        });
}
		
}


function stripePopupPremium(id,amount,sku_id='')
{
		//strip-payment-premium-init
		if(amount >0)
		{
			var id  	= id;
			var amount 	= amount;
    


	var width = 800;
	var height = 700;
	var left = parseInt((screen.availWidth/2) - (width/2));
	var top = parseInt((screen.availHeight/2) - (height/2));
 
	var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,toolbar,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;
	myWindow = window.open(WEBSITE_URL+"/strip-payment-premium-init?popup=true&plan_id="+id+"&plan_amount="+amount+"&sku_id="+sku_id,"subWind6RantSalePrem",windowFeatures);
	window.premium_after_stripe = function ()
{
	    var ajax_url = WEBSITE_URL+"/list/premium?is_premium="+id+"&plan_amount="+amount+"&sku_id="+sku_id;
            $.ajax({
              url : ajax_url,
              method:"get",
              data:{ 
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}' 
              },
              beforeSend:function(){
                  startLoader();
              },
              complete:function(){
                 stopLoader(); 
              },
              success : function(data){
                  if(data.status){
                        Swal.fire({
                          title: 'Success!',
                          text: 'Transaction completed successfully',
                          icon: 'success',
                          confirmButtonText: 'Ok'
                      }).then((result) => {
                    if(result.value){
                        $(".package").hide();
                        $(".subscribe").hide();
                        $(".modal-backdrop").hide();
                        $("body").removeClass('modal-open')
                        $(".modal").hide();
                        $("#myModal").hide();
                         $(".submit_step_8").trigger('click');
                    }
                });
                  }else{
                      Swal.fire({
                          title: 'Error!',
                          text: data.message,
                          icon: 'error',
                          confirmButtonText: 'Ok'
                      });
                  }
              }
          });
}
		}
	
}

function payPlan(id,amount,number){
    
    var planid 		= $("#plan_id22").val();
    var planamount 	= $("#plan_amount22").val();
	var plannumber 	= $("#plan_number22").val();
	 var sku_id 	= $("#sku_id2").val();

	if(!planid)
	{
		alert('Please select any plan');
		return false;
	}
	$("#plan_id").val(planid);
    $("#plan_amount2").val(planamount);
	$("#plan_number").val(plannumber);
	$("#plan_amount").html(planamount);
	$("#sku_id").val(sku_id);
	if(planamount==0){
	$("#myModal1").show();
	}else{
   $("#myModal").show();
	}
}

  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('bg-header');
    } else {
       $('header').removeClass('bg-header');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('fixed-top');
    } else {
       $('header').removeClass('fixed-top');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('#top-header-bar').removeClass('fixed-top');
    } else {
       $('#top-header-bar').addClass('fixed-top');
    }
});
  
</script>
<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery('#loading').fadeOut(1000);
});

</script>
@if($package=='YES')
<script
    src="https://www.paypal.com/sdk/js?client-id={{env('SB_CLIENT_ID')}}"> // Required. Replace SB_CLIENT_ID with your sandbox client ID.
  </script>
<script>
    
  paypal.Buttons({
    style:{
      color: 'white',
      layout: 'horizontal',
      tagline: false,
      shape:   'rect'
    },
    createOrder: function(data, actions) {
      // This function sets up the details of the transaction, including the amount and line item details.
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: $("#plan_amount2").val()
          }
        }]
      });
    },
    onApprove: function(data, actions) {
      // This function captures the funds from the transaction.
      return actions.order.capture().then(function(details) {
        var ajax_url = WEBSITE_URL+"/listproperty/transaction";
        var amount=$("#plan_amount2").val();
        var id=$("#plan_id").val();
        var number=$("#plan_number").val();
        $.ajax({
	            url : ajax_url,
	            method:"post",
              data:{
                amount:amount,
                id:id,
                number:number,
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
			  },
			  beforeSend:function(){
	                startLoader();
	            },
	            complete:function(){
	               stopLoader(); 
	            },
	            success : function(data){
	                if(data.status){
                        Swal.fire({
	                        title: 'Success!',
	                        text: 'Transaction completed successfully',
	                        icon: 'success',
	                        confirmButtonText: 'Ok'
	                    }).then((result) => {
				            if(result.value){
								$(".subscribe").hide();
								$("#myModal").hide();
                                showScreen('step1');
				            }
				        });
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            }
	        });
        // This function shows a transaction success message to your buyer.
        
        // alert('Transaction completed by ' + details.payer.name.given_name);
      });
    }
  }).render('#paypal-button-container10');
  //This function displays Smart Payment Buttons on your web page.
</script>
@endif
<script>
  function clodebu(){
	$("#myModal").hide();
  }

  function countinuefree(){
	var ajax_url = WEBSITE_URL+"/listproperty/transaction";
        var amount=$("#plan_amount2").val();
        var id=$("#plan_id").val();
        var number=$("#plan_number").val();
        var sku_id 	= $("#sku_id").val();
        $.ajax({
	            url : ajax_url,
	            method:"post",
              data:{
                amount:amount,
                id:id,
                number:number,
                sku_id:sku_id,
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
			  },
			  beforeSend:function(){
	                startLoader();
	            },
	            complete:function(){
	               stopLoader(); 
	            },
	            success : function(data){
	                if(data.status){
								$(".subscribe").hide();
								$("#myModal1").hide();
                                showScreen('step1');
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            }
	        });
  }

</script>
<!-- <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
$(function() {
    var $form = $(".require-validation");
    $('form.require-validation').bind('submit', function(e) {
        var $form = $(".require-validation"),
            inputSelector = ['input[type=email]',
                            'input[type=password]',
                            'input[type=text]',
                            'input[type=file]',
                            'textarea'].join(', '),
            $inputs       = $form.find('.required').find(inputSelector),
            $errorMessage = $form.find('.inp-error'),
            valid         = true;
            $errorMessage.addClass('d-none');
        $('.has-error').removeClass('has-error');

        $inputs.each(function(i, el) {
            var $input = $(el);
            if ($input.val() === '') {
                $input.parent().addClass('has-error');
                $errorMessage.removeClass('d-none');
                e.preventDefault();
            }
        });

        if (!$form.data('cc-on-file')) {
            var StripeKey = "{{env('STRIPE_KEY')}}";

            e.preventDefault();
            Stripe.setPublishableKey(StripeKey);
            Stripe.createToken({
                number: $('.card-number').val(),
                cvc:    $('.card-cvc').val(),
                exp_month: $('.card-expiry-month').val(),
                exp_year: $('.card-expiry-year').val()
            }, stripeResponseHandler);
        }

    });
 	function stripeResponseHandler(status, response) {
         
        if (response.error) {
            $('.stripe-error').text(response.error.message);
        } else {

            var token = response['id'];
            $form.find('input[type=text]').empty(); 
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");

            var ajax_url    = WEBSITE_URL+"/listproperty/stripeorder";
            var amount      = $("#plan_amount2").val();
           
            $("#stipe_payment_btn").val(amount);
            var stipe_payment_btn = amount;
            var id          = $("#plan_id").val();
            var number      = $("#plan_number").val();
            var stripeToken       = token;
 
            $.ajax({
                type: 'post',
                url: ajax_url,
                data: {
                        amount:amount,
                        id:id,
                        number:number,
                        stipe_payment_btn:stipe_payment_btn,
                        stripeToken:token,
                      }, 
                success: function (data) 
                {
                	 
                	console.log(data);
                      
                  if(data.status==1)
                  {

                   transaction_after_stripe();
                    

                  }
                     
                }
            });

            
        }
    } 



    

});


function transaction_after_stripe()
{
	    var ajax_url = WEBSITE_URL+"/listproperty/transaction";
        var amount=$("#plan_amount2").val();
        var id=$("#plan_id").val();
        var number=$("#plan_number").val();
        $.ajax({
	            url : ajax_url,
	            method:"post",
              data:{
                amount:amount,
                id:id,
                number:number,
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
			  },
			  beforeSend:function(){
	                startLoader();
	            },
	            complete:function(){
	               stopLoader(); 
	            },
	            success : function(data){
	            	console.log(data);
	                if(data.status){
                        Swal.fire({
	                        title: 'Success!',
	                        text: 'Transaction completed successfully',
	                        icon: 'success',
	                        confirmButtonText: 'Ok'
	                    }).then((result) => {
				            if(result.value){
								$("#StripeCardModal").hide();
								$(".subscribe").hide();
								$(".modal-backdrop").hide();
								$("body").removeClass('modal-open')
								$(".modal").hide();
								$("#myModal").hide();
                                showScreen('step1');
				            }
				        });
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            }
	        });
}
</script> -->

    <script>

    	/*function transaction_after_stripe()
{
	    var ajax_url = WEBSITE_URL+"/listproperty/transaction";
        var amount=$("#plan_amount2").val();
        var id=$("#plan_id").val();
        var number=$("#plan_number").val();
        $.ajax({
	            url : ajax_url,
	            method:"post",
              data:{
                amount:amount,
                id:id,
                number:number,
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
			  },
			  beforeSend:function(){
	                startLoader();
	            },
	            complete:function(){
	               stopLoader(); 
	            },
	            success : function(data){
	            	console.log(data);
	                if(data.status){
                        Swal.fire({
	                        title: 'Success!',
	                        text: 'Transaction completed successfully',
	                        icon: 'success',
	                        confirmButtonText: 'Ok'
	                    }).then((result) => {
				            if(result.value){
								$("#StripeCardModal").hide();
								$(".subscribe").hide();
								$(".modal-backdrop").hide();
								$("body").removeClass('modal-open')
								$(".modal").hide();
								$("#myModal").hide();
                                showScreen('step1');
				            }
				        });
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            }
	        });
}*/
history.pushState(null, null, location.href);
window.onpopstate = function () {
  history.go(1);
};
</script>

<script>
      $(document).ready(function() {
         function disablePrev() { window.history.forward() }
         window.onload = disablePrev();
         window.onpageshow = function(evt) { if (evt.persisted) disableBack() }
      });
   </script>
     <script>
history.pushState(null, null, document.URL);
window.addEventListener('popstate', function () {
    history.pushState(null, null, document.URL);
});

</script>
@endsection