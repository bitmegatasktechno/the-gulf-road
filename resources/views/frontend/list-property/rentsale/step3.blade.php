<section class="buy-rent step3 listPropertySteps" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <h3 class="mb-3">Amenities</h3>
                <p class="mb-5">Tell us about the amenities available at your Place. You can list additional items if they are not listed below.</p>
                
                <div class="row">
                    @foreach(getAllAmenities() as $row)
                        <div class="col-md-4">
                            <div class="facility-block amenitiesBlock" data-name="{{$row->_id}}">
                                <img src="{{url('uploads/amenities/'.$row->logo)}}">
                                <p class="mb-0 mt-2">{{ucfirst($row->name)}}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
                
                <div class="pt-3 pb-3">
                    <hr>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <a class="mt-2 dp-inline-block" href="javascript:;" onclick="showScreen('step2');">< Go back</a>
                    </div>
                    <div class="col-md-6 text-right">
                        
                        <button class="red-btn rounded float-right submit_step_3" type="button">
                            Continue 
                            <img class="filter-white" src="{{asset('img/black_arrow.png')}}">
                        </button>
                    </div>
                </div>
            </div>
            <div class="offset-md-1 col-md-6">
                <img class="img-responsive" src="{{asset('img/C.png')}}">
            </div>
        </div>
    </div>
</section>