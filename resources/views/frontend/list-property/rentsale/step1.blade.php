@php
				$current=date('Y-m-d');
        $users = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','rent')->where('expiry_date','>=',$current )->count();
        if($users){
          if(@$users > 0){
            $last_transaction = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','rent')->where('expiry_date','>=',$current )->latest()->first();
            if($last_transaction->count > $last_transaction->listed){
               
              $package="NO";
            }else{
              $package="YES";
            }
            }else{
                $package="YES";
            }
        }else{
            $package="YES";
        } 
                    @endphp


 
<section class="buy-rent step1 listPropertySteps" @if($package=='YES') style="display:none" @endif >
    <div class="container">
        <div class="row">
            <div class="col-md-6">
            <input type="hidden" name="loc" value="{{$loc}}">
                <h3 class="mb-3">Advertise Your Property with us.</h3>
                <p class="mb-5">To get started with your listing, tell us something about your Listing.</p>
                <h5>What would you like to list this property for?</h5>
                <p>You can list properties to either Sale , Rent or Both.</p>
                <div class="boxed second" name="rent_sale_property_step_1" id="rent_sale_property_step_1">
                    
                    <div class="form-group">
                        <input type="radio" id="rent" name="listing_type" value="rent" required>
                        <label for="rent" class="customCursorClass" style="text-align: center;">Rent</label>

                        <input type="radio" id="sale" name="listing_type" value="sale" required>
                        <label for="sale" class="customCursorClass" style="text-align: center;">Sale</label>

                        <input type="radio" id="both" name="listing_type" value="both" required>
                        <label for="both" class="customCursorClass" style="text-align: center;">Both</label>
                        <br>
                        <span class="invalid-feedback listing_type"></span>
                    </div>
                    <div id="map" style="display:none"></div>
                    <h5>House Type</h5>
                    <p>Select the type of property that you are listing for.</p>
                    <div class="form-group">

                    	@foreach(getAllHouseTypes() as $row)
                    		<input type="radio" id="{{$row->name}}" name="house_type" value="{{$row->name}}" required>
	                        <label for="{{$row->name}}" class="customCursorClass">
	                            <img src="{{url('uploads/housetypes/'.$row->logo)}}"> 
	                            <br>{{ucfirst($row->name)}}
	                        </label>
                    	@endforeach
                        <br>
                         <span class="invalid-feedback house_type"></span>
                    </div>
                
                    <h5>Location</h5>
                    <p>What is the location of the property you are listing?</p>
                    
                    
                    <div class="row">
                        <div class="col-md-11">
                            <div class="form-group">
                            <input class="full-width" onChange="getLocation()" id="address" type="textbox" name="location_name" placeholder="“Enter Location Name”" required>
                                <!-- <input type="text" class="form-control" placeholder="Enter Location Name" name="location_name"> -->
                                <span class="invalid-feedback location_name"></span>
                            </div>
                            <div class="row"  hidden >
                               <div class="col-md-6"  hidden  >
                                    
                                        <input class="form-control" style="padding: 0;height: 20px;" type="text" hidden name="lat" id="lat"  readonly  >
                                    
                                </div>
                                <div class="col-md-6">
                                  <input class="form-control" style="padding: 0;height: 20px;" type="text"  hidden  name="longitude" id="lng" readonly  >
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="House / Apartment No" name="house_number" required>
                                        <span class="invalid-feedback house_number"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Landmark (optional)" name="landmark" required>
                                        <span class="invalid-feedback landmark"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select class="form-control" name="country"  id="country" onchange="setCities(this)" required>
                                            <option value="">Select Country</option>
                                            @foreach(getAllLocations() as $country)
                                                <option value="{{ $country->name }}" >{{ __(ucfirst($country->name)) }}</option>
                                            @endforeach
                                        </select>
                                        <span class="invalid-feedback country"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select class="form-control" name="city" id="countryCity" required>
                                            <option value="">Select City</option>
                                        </select>
                                        <span class="invalid-feedback city"></span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="pt-3">
                                <hr>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <a class="mt-2 dp-inline-block" href="{{url(app()->getLocale().'/listproperty/'.$link)}}">< Go back</a>
                        </div>
                        <div class="col-md-6">
                            <button class="red-btn rounded float-right submit_step_1" type="button">
                                Continue 
                                <img class="filter-white" src="{{asset('img/black_arrow.png')}}">
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <img class="img-responsive" src="{{asset('img/buy-rent-img.jpg')}}">
            </div>
        </div>
    </div>
</section>

 @if(env('APP_ACTIVE_MAP') =='patel_map')
<style>
    #address_result
    {
        z-index: 9;
    }
</style>
   <script src="https://mapapi.cloud.huawei.com/mapjs/v1/api/js?callback=initMap&key={{env('PATEL_MAP_KEY')}}"></script>
    <script>
        function initMap() {
            var mapOptions = {};
            mapOptions.center = {lat: 48.856613, lng: 2.352222};
            mapOptions.zoom = 8;
            mapOptions.language='ENG';
            var searchBoxInput = document.getElementById('address');
             var map = new HWMapJsSDK.HWMap(document.getElementById('map'), mapOptions);
            // Create the search box parameter.
            var acOptions = {
                location: {
                    lat: 48.856613,
                    lng: 2.352222
                },
                radius: 10000,
                customHandler: callback,
                language:'en'
            };
            var autocomplete;
            // Obtain the input element.
            // Create the HWAutocomplete object and set a listener.
            autocomplete = new HWMapJsSDK.HWAutocomplete(searchBoxInput, acOptions);
            autocomplete.addListener('site_changed', printSites);
            // Process the search result. 
            // index: index of a searched place.
            // data: details of a searched place. 
            // name: search keyword, which is in strong tags. 
            function callback(index, data, name) {
                
                var div
                div = document.createElement('div');
                div.innerHTML = name;
                return div;
            }
            // Listener.
            function printSites() {
                 
                
                var site = autocomplete.getSite();
                var latitude = site.location.lat;
                var longitude = site.location.lng;
                var country_name = site.address.country;
                
                var results = "Autocomplete Results:\n";
                var str = "Name=" + site.name + "\n"
                    + "SiteID=" + site.siteId + "\n"
                    + "Latitude=" + site.location.lat + "\n"
                    + "Longitude=" + site.location.lng + "\n"
                    + "Address=" + site.formatAddress + "\n";

                    
                results = results + str;

                 


                $('#lat').val(latitude);
                $('#lng').val(longitude);
                $('#country').val(country_name);
                setCities('#country');
                console.log(latitude, longitude);
                /*console.log(results);*/
            }
            
        }
        function getLocation()
        {

        }
    </script> 

     
    @elseif(env('APP_ACTIVE_MAP')=='google_map')
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&callback=initMap&libraries=places&v=weekly" defer ></script>
<script>
  

function initMap() {
  const myLatLng = {
    lat: -25.363,
    lng: 131.044
  };
    
      

  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 4,
    center: myLatLng
  });
    var input = document.getElementById('address');

 map.controls[google.maps.ControlPosition.TOP_RIGHT].push();

var autocomplete = new google.maps.places.Autocomplete(input);
autocomplete.bindTo('bounds', map);
autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);
            var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29),
          title: "Hello World!"
        });
        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindow.open(map, marker);
        });
  new google.maps.Marker({
    position: myLatLng,
    map,
    title: "Hello World!"
  });
}

function getLocation()
 {
//   alert(address);
  var address = $('#address').val();
//   alert(address);
  var geocoder = new google.maps.Geocoder();

  geocoder.geocode( { 'address': address}, function(results, status)
  {


    if (status == google.maps.GeocoderStatus.OK)
    {
      /*console.log(results[0],"afdcfsa");
      console.log(results[0].address_components);*/
      const countryObj=results[0].address_components.filter(val=>{
            if(val.types.includes('country')){
                return true
            }
            return false;
      });
    /* console.log(countryObj,'the country obj');*/


     var country_name = countryObj[0].long_name;
     /*console.log(country_name,'name');*/
      var latitude = results[0].geometry.location.lat();

      var longitude = results[0].geometry.location.lng();
      /* alert(latitude +"-----"+ longitude);*/
      $('#lat').val(latitude);
     $('#lng').val(longitude);
     $('#country').val(country_name);
     setCities('#country');
     /*$('#countryTo').val(country_name);*/
     /*$('#country_name').val(country_name);*/
    /*console.log(latitude, longitude);*/
    }
  });

}
    </script>

    @endif