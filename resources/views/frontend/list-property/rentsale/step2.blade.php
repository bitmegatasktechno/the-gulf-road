<section class="buy-rent step2 listPropertySteps" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3 class="mb-3">Property Details</h3>
                <p class="mb-5">Share some of the details related to your property. This helps the user get to know the property a little better.</p>
                <h5>Dimensions</h5>
                <p>What are the Dimensions of your House / Apartment?</p>
                <div class="loc-form" name="rent_sale_property_step_2" id="rent_sale_property_step_2">
                   
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <input class="form-control" type="number" name="build_up_area" placeholder="Enter super build up area">
                                <span class="invalid-feedback build_up_area"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select class="form-control rounded" name="build_up_area_unit">
                                    <option value="sqft">Sq Ft</option>
                                    <option value="sqmtr">Sq Mtr</option>
                                </select>
                                <span class="invalid-feedback build_up_area_unit"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <input class="form-control" type="text" name="carpet_area" placeholder="Enter carpet area (optional)">
                                <span class="invalid-feedback carpet_area"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select class="form-control rounded" name="carpet_area_unit">
                                    <option value="sqft">Sq Ft</option>
                                    <option value="sqmtr">Sq Mtr</option>
                                </select>
                                <span class="invalid-feedback carpet_area_unit"></span>
                            </div>
                        </div>
                    </div>
                
                    <div class="row mt-4">
                        <div class="col-md-7">
                            <p class="mb-4"><img class="mr-3" src="{{asset('img/rooms.png')}}"> No of bedrooms</p>
                            <p class="mb-4"><img class="mr-3" src="{{asset('img/bathrooms.png')}}"> No of bathrooms</p>
                        </div>
                        <div class="col-md-5">
                            <div class="number mb-2">
                                <span class="minus"><small>-</small></span>
                                <input class="dp-inline-block no-border" type="text" readonly="" name="no_of_bedrooms" value="1"  />
                                <span class="plus"><small>+</small></span>
                            </div>
                            <div class="number mb-4 pt-2">
                                <span class="minus"><small>-</small></span>
                                <input class="dp-inline-block no-border" type="text" readonly="" name="no_of_bathrooms" value="1" />
                                <span class="plus"><small>+</small></span>
                            </div>
                        </div>
                        <span class="invalid-feedback no_of_bedrooms"></span>
                        <span class="invalid-feedback no_of_bathrooms"></span>
                    </div>
                    <h5 class="mt-4">Furnishing</h5>
                    <p>Tell us about the furnishing that your apartment has.</p>
                    <div class="boxed">
                        <input type="radio" id="fully_furnished" name="furnishing_type" value="fully_furnished">
                        <label for="fully_furnished" class="customCursorClass">Fully furnished</label>
                        
                        <input type="radio" id="semi_furnished" name="furnishing_type" value="semi_furnished">
                        <label for="semi_furnished" class="customCursorClass">Semi furnished</label>
                        
                        <input type="radio" id="unfurnished" name="furnishing_type" value="unfurnished">
                        <label for="unfurnished" class="customCursorClass">Unfurnished</label>
                        <br>
                        <span class="invalid-feedback furnishing_type"></span>
                    </div>
                    <h5 class="mt-4">Flooring</h5>
                    <p>Tell us about the furnishing that your apartment has.</p>
                    <div class="row">
                        <div class="col-md-11">
                            <div class="loc-form">
                                <select class="form-control rounded" name="flooring_type">
                                    <option value="">Select type</option>
                                    @foreach(getAllFlooring() as $row)
                                        <option value="{{$row->name}}">{{$row->name}}</option>
                                    @endforeach
                                </select>
                                <span class="invalid-feedback flooring_type"></span>
                            </div>
                        </div>
                    </div>
                    <h5 class="mt-4 mb-3">Is There space for parking?</h5>
                    <div class="">
                        <div class="radio dp-inline-block mr-3" style="margin-bottom: 0;">
                            <input id="parking-radio-1" onchange="parkingAvailableRadio(this)" name="parking_available" type="radio" value="yes" autocomplete="off">
                            <label for="parking-radio-1" class="radio-label">Yes</label>
                        </div>
                        <div class="radio dp-inline-block mr-3" style="margin-bottom: 0;">
                            <input id="parking-radio-2" onchange="parkingAvailableRadio(this)" checked="" name="parking_available" type="radio" value="no" autocomplete="off">
                            <label for="parking-radio-2" class="radio-label">No</label>
                        </div>
                        <br>
                        <span class="invalid-feedback parking_available"></span>
                    </div>
                    <div class="row mt-4 parking_available_div" style="display: none;">
                        <div class="col-md-7">
                            <p class="mb-4">Covered parkings</p>
                            <p class="mb-4">Open parkings</p>
                        </div>
                        <div class="col-md-5">
                            <div class="number mb-2">
                                <span class="minus">
                                    <small>-</small>
                                </span>
                                <input class="dp-inline-block no-border" readonly="" type="text" name="no_of_covered_parking" value="0" >
                                <span class="plus">
                                    <small>+</small>
                                </span>
                            </div>
                            <div class="number mb-4 pt-2">
                                <span class="minus">
                                    <small>-</small>
                                </span>
                                <input class="dp-inline-block no-border" readonly="" name="no_of_open_parking" type="text" value="0">
                                <span class="plus">
                                    <small>+</small>
                                </span>
                            </div>
                            
                        </div>
                        <span class="invalid-feedback no_of_covered_parking"></span>
                            <span class="invalid-feedback no_of_open_parking"></span>
                    </div>
                    <div class="row">
                        <div class="col-md-11">
                            <div class="pt-3">
                                <hr>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <a class="mt-2 dp-inline-block" href="javascript:;" onclick="showScreen('step1');">< Go back</a>
                        </div>
                        <div class="col-md-6">
                            <button class="red-btn rounded float-right submit_step_2" type="button">
                                Continue 
                                <img class="filter-white" src="{{asset('img/black_arrow.png')}}">
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <img class="img-responsive" src="{{asset('img/B.png')}}">
            </div>
        </div>
    </div>
</section>