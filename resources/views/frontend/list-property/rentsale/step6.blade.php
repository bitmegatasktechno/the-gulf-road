@php
$currency = 'USD';
if($link=='uae')
{
    $currency = 'AED';
}
elseif($link=='kuwait')
{
     $currency = 'KD';
}
elseif($link=='ksa')
{
     $currency = 'SAR';
}
elseif($link=='oman')
{
     $currency = 'OR';
}
elseif($link=='qatar')
{
     $currency = 'KD';
}
elseif($link=='kuwait')
{
     $currency = 'QR';
}
elseif($link=='bahrain')
{
     $currency = 'BD';
}

@endphp
<section class="buy-rent step6 listPropertySteps" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="loc-form" name="rent_sale_property_step_6" id="rent_sale_property_step_6">
                    <div class="row">
                            <div class="col-md-12">
                                <h5>Currency</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <select class="form-control rounded" name="currency_type" id="currency_type" onchange="change_currency_type(this)">
                                        <option value="{{$currency}}">{{$currency}}</option>
                                        <option hidden  value="USD">USD</option>
                                    </select>
                                    <span class="invalid-feedback currency_type"></span>
                                </div>
                            </div>
                        </div>
                    <!-- list for sale -->
                    <div class="sale_price_list" style="display: none;">
                        <h3 class="mb-3">Set Price For Sale</h3>
                        <p class="mb-3">Set the amount that you are going to list this propert for. Keep the price reasonable.</p>
                        
                        <div class="row">
                            <div class="col-md-9">
                                <h5>Price</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input class="form-control" type="number" min="0" name="price" placeholder="Enter Price Of Property">
                                    <span class="invalid-feedback price"></span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" class="form-control currency_type"  name="price_currency" value="{{$currency}}" readonly>
                                    <span class="invalid-feedback price_currency"></span>
                                </div>
                            </div>
                        </div>
                    
                        <div class="row mt-3">
                            <div class="col-md-7">
                                <h5>Give Discount for this property</h5>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox switcher">
                                    <a href="javascript:;">
                                    <label for="discount_check">
                                        <input type="checkbox" name="discount_available" id="discount_check" value="checked" onchange="toggleInstantBooking('discount_price');">
                                        <span><small></small></span>
                                    </label></a>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="discount_price" style="display: none;">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="discount_price" placeholder="Enter Price After Discount">
                                    <span class="invalid-feedback discount_price"></span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" class="form-control currency_type"  name="discount_price_currency" value="{{$currency}}" readonly>
                                    <span class="invalid-feedback discount_price_currency"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pt-5 pb-3">
                        <hr>
                    </div>
                    <!-- list for rent -->
                    <div class="rent_price_list" style="display: none;">
                       <h3 class="mb-3">Set Price For Rent</h3>
                        <p class="mb-3">Set the amount that you are going to list this propert for. Keep the price reasonable.</p>
                        
                        <div class="row">
                            <div class="col-md-9">
                                <h5>Rent Frequency</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <select class="form-control rounded" name="rent_frequency">
                                        @foreach(getAllRentFrequency() as $key=>$value)
                                            <option value="{{$key}}">{{ucfirst($value)}}</option>
                                        @endforeach
                                    </select>
                                    <span class="invalid-feedback rent_frequency"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-9">
                                <h5>Rent For This Property</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input class="form-control" type="number" min="0" name="rent" placeholder="Enter Rent Price">
                                    <span class="invalid-feedback rent"></span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" class="form-control currency_type"  name="rent_currency" value="{{$currency}}" readonly>
                                    <span class="invalid-feedback rent_currency"></span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <h5>Maintenance Charges (per month)</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input class="form-control" type="number" min="0" name="maintenance_charges" placeholder="Enter Maintenance Charges">
                                    <span class="invalid-feedback maintenance_charges"></span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" class="form-control currency_type"  name="maintenance_charges_currency" value="{{$currency}}" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-md-12">
                                <h5>Security Amount</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input class="form-control" type="number" min="0" name="security_amount" placeholder="Enter Security Amount">
                                    <span class="invalid-feedback security_amount"></span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" class="form-control currency_type"  name="security_amount_currency" value="{{$currency}}" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-md-12">
                                <h5>Available From</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input class="form-control datepicker" type="text" name="available_from" placeholder="dd/mm/yyyy">
                                    <span class="invalid-feedback available_from"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-2">
                            <div class="col-md-7">
                                <h5>List This Property For Instant Booking</h5>
                            </div>
                            <div class="col-md-3">
                                <div class="checkbox switcher">
                                    <a href="javascript:;">
                                        <label for="instant_book">
                                            <input type="checkbox" name="instant_book_available" id="instant_book" value="checked" onchange="toggleInstantBooking('instant_price');">
                                            <span><small></small></span>
                                        </label>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <small class="info-text" autocomplete="off" data-toggle="modal" data-target="#how_it_works" style="cursor: pointer;">What is instant booking?<img class="ml-2" src="{{asset('img/info.png')}}"></small>
                        
                        <div class="row" id="instant_price" style="display: none;">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="night_price" placeholder="Set Per Night Rate Of Property">
                                    <span class="invalid-feedback night_price"></span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" class="form-control currency_type"  name="night_price_currency" value="{{$currency}}" readonly>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                    
                    <!-- <div class="row add_card" style="display:none;">
                    <h5 class="mt-4 mb-3">Make Payment</h5>
                    <h5 class="mt-4 mb-3">Name</h5>
                    <input class="form-control" type="text" name="name" placeholder="Name">
                        <span class="invalid-feedback name"></span>
                        <h5 class="mt-4 mb-3">Card Number</h5>
                        <input class="form-control" type="text" name="number" placeholder="Number">
                        <span class="invalid-feedback name"></span>
                        <h5 class="mt-4 mb-3">Expiry Month</h5>
                        <select class="form-control" name="month">
                        @for($i=1;$i<=12;$i++)
                        <option value="{{$i}}">{{$i}}</option>
                        @endfor
                        </select>
                        <span class="invalid-feedback name"></span>
                        <h5 class="mt-4 mb-3">Expiry Month</h5>
                        <select class="form-control" name="year">
                        @for($j=2020;$j<=2035;$j++)
                        <option value="{{$j}}">{{$j}}</option>
                        @endfor
                        </select>
                        <span class="invalid-feedback name"></span>
                        <h5 class="mt-4 mb-3">CVV</h5>
                        <input class="form-control" type="text" name="cvv" placeholder="CVV">
                        <span class="invalid-feedback name"></span>
                    </div>
                    <div class="pt-5 pb-3 step6_divided">
                        <hr>
                    </div> -->
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <a class="mt-2 dp-inline-block" href="javascript:;" onclick="showScreen('step5');">< Go back</a>
                    </div>
                    <div class="col-md-6 text-right">
                        <button class="red-btn rounded submit_step_6" type="button">
                            Submit 
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <img class="img-responsive" src="{{asset('img/B.png')}}">
            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div class="modal fade how_it_modal" id="how_it_works" tabindex="-1" role="dialog" aria-labelledby="what_is_instant_bookingLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-0 pb-0">
        <h5 class="modal-title" id="what_is_instant_bookingLabel">What is instant booking?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p >1. Choose the dates</p>
        <p >2. If the property is available you can temporarily hold the property </p>
        <p >3. The request will be sent to the owner and you can book your inspection timings accordingly</p>
        <p >4. TheGulfroad.com team will facilitate the communications.</p>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    function change_currency_type(obj)
    {
        // alert('11111111111111111111');
        var currency = $(obj).val();
        // alert(currency);
        $('.currency_type').val(currency);
    }
    const premiumAvailableRadio = function(input){
		if(input.value=='no'){
            $('.add_card').hide();
        }
        else if(input.value=='yes'){
            $('.add_card').show();
        }
	}
</script>
