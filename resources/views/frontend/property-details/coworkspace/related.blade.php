@if(count($myProperty)>0)
@foreach($myProperty as $rentProperty)
@php
                            $totaluser = \App\Models\Review::where('is_approved',1)->where('review_type',"4")->where('type_id',$rentProperty->_id)->count();
                            $ratinggiven1=\App\Models\Review::where('is_approved',1)->where('review_type',"4")->where('type_id',$rentProperty->_id)->sum('average');
                            $totalrating=@$totaluser*5;
                            if($totalrating == 0){
                                $totalrating=1;
                            }
                            $avg=number_format(@$ratinggiven1/@$totalrating*5);
                            @endphp
                <div class="col-md-3">
                    <div class="property-card bg-white rounded">
                    @if(isset(Auth::user()->id))
                    @php
                    $fav = \App\Models\Favourate::where('propert_id',$rentProperty->_id)->where('user_id',Auth::user()->id)->first();
                    @endphp
                    @endif
                    @if(@$fav)
                    <i class="fa fa-heart fav-icon" onClick="makefav('{{$rentProperty->_id}}')"></i>
                    @else
                    <i class="fa fa-heart-o fav-icon" onClick="makefav('{{$rentProperty->_id}}')"></i>
                    @endif
                    <a href="{{url(app()->getLocale().'/property/'.$link.'/coworkspace/'.$rentProperty->_id)}}">
                    @if(!isset($rentProperty->files[0]) || $rentProperty->files[0]=='')
                    <img class="img-fluid property-img" src="{{asset('img/house2.png')}}" >
                    @else
                    @if(file_exists('uploads/coworkspce/'.$rentProperty->files[0]))
                    <img class="img-fluid property-img" src="{{url('uploads/coworkspce/'.$rentProperty->files[0])}}" >
                    @else
                    <img class="img-fluid property-img" src="{{asset('img/house2.png')}}" >
                    @endif
                    @endif
                        <div class="pl-3 pr-3 mt-3">
                        
                        <h2>{{\Str::limit(ucfirst(@$rentProperty->title), 25)}}</h2>
                        <p>
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            {{\Str::limit(ucfirst($rentProperty->address), 55)}}, {{$rentProperty->country}}</p>
                            <ul class="list-inline">
                            @if(@$avg==1)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==2)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==3)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==4)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==5)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg == 0)
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                @endif
                                <li class="white-color text" style="color:black !important;">{{@$avg}}</li>
                            </ul>
                            <h7 style="color:black;">Starting @ {{$rentProperty->open_space_monthly_currency}}{{$rentProperty->open_space_monthly_price ?? 0}} /m</h7>
                            <ul class="list-inline red-list mb-4">
                            <li class="mr-3"><img class="mr-1" src="{{asset('img/red-bed.png')}}"> {{@$rentProperty->no_of_bedrooms}}</li>
                            <li class="mr-3"><img class="mr-1" src="{{asset('img/red-tub.png')}}"> {{@$rentProperty->no_of_bathrooms}}</li>
                            <li class="mr-3"><img class="mr-1" src="{{asset('img/red-car.png')}}"> {{@$rentProperty->no_of_open_parking}}</li>
                            </ul>
                        </div>
                        </a>
                    </div>
                </div>
                @endforeach
                @else
    <div class="" style="display: block;
        max-height: 60px;
        text-align: center;
        background-color: #d6e9c6;
        line-height: 4;
        width: 100%;
        ">
        <p style="font-size: 16px;font-weight: 700;">No Result Found</p>
    </div>
@endif
