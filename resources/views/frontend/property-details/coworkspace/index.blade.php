@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
@extends('frontend.layouts.home')
@section('title','Property Details')
@section('css')
<link href="{{asset('lib/bootstrap.min.css')}}" rel="stylesheet">
<!-- Libraries CSS Files -->
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<!-- Main Stylesheet File -->
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">

<link href="{{asset('css/style.css')}}" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.carousel.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.theme.default.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.theme.green.min.css" />
	
	<link rel="stylesheet" type="text/css" href="{{asset('css/flickity.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" integrity="sha512-aEe/ZxePawj0+G2R+AaIxgrQuKT68I28qh+wgLrcAJOz3rxCP+TwrK5SPN+E5I+1IQjNtcfvb96HDagwrKRdBw==" crossorigin="anonymous" />

@endsection

@section('content')
    
	@include('frontend.layouts.default-header')
	
 
	@include('frontend.property-details.common-data.filter')


	<!-- Modal -->
<div class="modal fade how_it_modal" id="contact_information" tabindex="-1" role="dialog" aria-labelledby="contact_informationLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-0 pb-0">
        <h5 class="modal-title" id="contact_informationLabel">Owner's Contact Information</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p ><b>Name :</b> {{ucfirst($data->user->name)}}</p>
        <p ><b>Email Address :</b> {{$data->user->email}}</p>
        <p ><b>Phone Number :</b> {{$data->user->countryCode}} {{$data->user->phone}}</p>

      </div>
    </div>
  </div></div>



    <section class="body-light-grey pt-3" style="margin-top:70px;">
	    <div class="container">
	        <div class="row">
	            @include('frontend.property-details.coworkspace.left-part')
				@if(\Auth::user())
            @if(\Auth::user()->id!=$data->user_id)
            <!-- right section -->
            @include('frontend.property-details.coworkspace.right-part')
            @endif
            @else
            @include('frontend.property-details.coworkspace.right-part')
            @endif
	        </div>
	    </div>
	</section>
<section class="body-light-grey">
	<div class="container">
            <div class="row">
                <div class="col-md-12 mt-5">
                
					<h2 style="font-weight: 800;">Featured Nearby</h2>
					<span>New Co-Working offices nearby</span>
                    <p></p>
                </div>
                <div class="container">

                <div id="mydivon" class="fadeIn">
                <div class="row dynamicContent">
				@include('frontend.property-details.coworkspace.related')
                </div>
            </div>
                
                <div class="col-md-12 show-more text-center">
                    <a href="{{url(app()->getLocale().'/coworkspace/'.$link.'?country='.$link.'&location=')}}"><button class="red-bordered mt-3 mb-5">SHOW MORE <i class="fa fa-angle-right"></i></button></a>
                </div>

            </div>
            </div>
			</div>
		</section>
		<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
			<script type="text/javascript">
				var homeLoanWidgetId = (homeLoanWidgetId !=='')?(homeLoanWidgetId):'';
				var wiriteReviewWidgetId =  (wiriteReviewWidgetId !=='')?(wiriteReviewWidgetId):'';
				var bookADayWidgetId =  (bookADayWidgetId !=='')?(bookADayWidgetId):'';
				var makeInspectionInquiryWidgetId =  (makeInspectionInquiryWidgetId !=='')?(makeInspectionInquiryWidgetId):'';
				var makeReportFlagWidgetId =  (makeReportFlagWidgetId !=='')?(makeReportFlagWidgetId):'';
				 
				
				var CaptchaCallback = function() {
					wiriteReviewWidgetId = grecaptcha.render('g-wiriteReviewWidgetId', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
					bookADayWidgetId = grecaptcha.render('g-bookADayWidgetId', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
					makeInspectionInquiryWidgetId = grecaptcha.render('g-makeInspectionInquiryWidgetId', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
					makeReportFlagWidgetId = grecaptcha.render('g-makeReportFlagWidgetId', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
					 
				};

			</script>
    @include('frontend.layouts.footer')

@endsection
@section('scripts')
		<script src='https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js'></script>
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
		<script src="{{asset('js/script.js')}}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/owl.carousel.min.js"></script>
		<script type="text/javascript" src="{{asset('js/flickity.pkgd.min.js')}}"></script>
		
	<script type="text/javascript">
			 
		$('.main-carousel').flickity({
			// options
			cellAlign: 'left',
			contain: true
		});

		jQuery("#owl-example").owlCarousel({
			loop: true,
			margin: 0,
			responsiveClass: true,
			autoHeight: true,
			autoplayTimeout: 7000,
			smartSpeed: 800,
			nav: true,
			items:4,
			responsiveClass:true,
			responsive: {
			    0: {
			      items: 1,
			      nav:true
			    },

			    600: {
			      items: 2,
			      nav:true
			    },

			    1024: {
			      items: 4,
			      nav:true
			    },

			    1366: {
			      items: 4,
			      nav:true
			    }
		  	}
		});
		$(document).ready(function () {
			$(".show_hide").on("click", function () {
				var dots = document.getElementById("dots");
				if (dots.style.display === "none") {
					dots.style.display = "inline";
				} else {
					dots.style.display = "none";
				}
		        var txt = $(".content").is(':visible') ? 'Read More' : 'Read Less';
		        $(this).text(txt);
		        $('.content').toggle();
		    });
		});

		
	</script>
@endsection

<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
		<script type="text/javascript">
		var widgetId1;
		var widgetId2;
		var CaptchaCallback = function() {
		widgetId1 = grecaptcha.render('g-recaptcha1', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
		widgetId2 = grecaptcha.render('g-recaptcha2', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
		};
		 
		</script>