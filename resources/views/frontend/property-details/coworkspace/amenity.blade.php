@if(@$data->special_amenties)
<div class="card-body">
<div class="row">
                    @php
                        $classic_id = \App\Models\AmenityCategory::where('name','classic basics')->value('_id');
                    @endphp
                    @foreach(getAllSpecialAmenities() as $row)
                        @if(in_array($row->_id, @$data->special_amenties))
                            <div class="col-md-4">
                                <div class="blog-card mb-4 p-3">
                                    <img class="img-fluid d-block mx-auto" src="{{url('/uploads/specialamenities/'.$row->logo)}}" style="border: none;">
                                    <strong style="text-align: center;display: inherit;margin-top: 10px;">{{\Str::limit($row->name, 15)}}</strong>
                                    
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
            @endif
<div id="accordion" class="cospace_accordion">
    <div class="card">
        <div class="card-header" id="headingOne">
            <h5 style="cursor: pointer;" class="mb-0" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                Food
                <span class="plus-sign float-right">+</span>
                <span class="minus-sign float-right">-</span>
            </h5>
        </div>

        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body">
                <div class="row">
                    @php
                        $food_id = \App\Models\AmenityCategory::where('name','food')->value('_id');
                    @endphp
                    @foreach(getAllSpecialAmenities()->where('category_id',$food_id) as $row)
                    @if(@$data->food_amenties)
                        @if(in_array($row->_id, $data->food_amenties))
                            <div class="col-md-4">
                                <div class="blog-card mb-4 p-3">
                                    <img class="img-fluid d-block mx-auto" src="{{url('/uploads/specialamenities/'.$row->logo)}}" style="border: none;">
                                    <strong style="text-align: center;display: inherit;margin-top: 10px;">{{\Str::limit($row->name, 15)}}</strong>
                                    <p>{{\Str::limit($row->description, 30)}}</p>
                                </div>
                            </div>
                        @endif
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="headingTwo">
            <h5 style="cursor: pointer;" class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            Facilities
             <span class="plus-sign float-right">+</span>
                <span class="minus-sign float-right">-</span>
            </h5>
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
            <div class="card-body">
                <div class="row">
                    @php
                        $facility_id = \App\Models\AmenityCategory::where('name','facilities')->value('_id');
                    @endphp
                    @foreach(getAllSpecialAmenities()->where('category_id',$facility_id) as $row)
                    @if(@$data->facilities_amenties)
                        @if(in_array($row->_id, $data->facilities_amenties))
                            <div class="col-md-4">
                                <div class="blog-card mb-4 p-3">
                                    <img class="img-fluid d-block mx-auto" src="{{url('/uploads/specialamenities/'.$row->logo)}}" style="border: none;">
                                    <strong style="text-align: center;display: inherit;margin-top: 10px;">{{\Str::limit($row->name, 15)}}</strong>
                                    <p>{{\Str::limit($row->description, 30)}}</p>
                                </div>
                            </div>
                        @endif
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="headingThree">
            <h5 style="cursor: pointer;" class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Equipments
            <span class="plus-sign float-right">+</span>
                <span class="minus-sign float-right">-</span>
            </h5>
        </div>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
            <div class="card-body">
                <div class="row">
                    @php
                        $equipments_id = \App\Models\AmenityCategory::where('name','equipments')->value('_id');
                    @endphp
                    @foreach(getAllSpecialAmenities()->where('category_id',$equipments_id) as $row)
                    @if(@$data->equipments_amenties)
                        @if(in_array($row->_id, $data->equipments_amenties))
                            <div class="col-md-4">
                                <div class="blog-card mb-4 p-3">
                                    <img class="img-fluid d-block mx-auto" src="{{url('/uploads/specialamenities/'.$row->logo)}}" style="border: none;">
                                    <strong style="text-align: center;display: inherit;margin-top: 10px;">{{\Str::limit($row->name, 15)}}</strong>
                                    <p>{{\Str::limit($row->description, 30)}}</p>
                                </div>
                            </div>
                        @endif
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="headingFour">
            <h5 style="cursor: pointer;"  class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Seating
             <span class="plus-sign float-right">+</span>
                <span class="minus-sign float-right">-</span>
            </h5>
        </div>
        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
            <div class="card-body">
                <div class="row">
                    @php
                        $seating_id = \App\Models\AmenityCategory::where('name','seating')->value('_id');
                    @endphp
                    @foreach(getAllSpecialAmenities()->where('category_id',$seating_id) as $row)
                    @if(@$data->seating_amenties)
                        @if(in_array($row->_id, $data->seating_amenties))
                            <div class="col-md-4">
                                <div class="blog-card mb-4 p-3">
                                    <img class="img-fluid d-block mx-auto" src="{{url('/uploads/specialamenities/'.$row->logo)}}" style="border: none;">
                                    <strong style="text-align: center;display: inherit;margin-top: 10px;">{{\Str::limit($row->name, 15)}}</strong>
                                    <p>{{\Str::limit($row->description, 30)}}</p>
                                </div>
                            </div>
                        @endif
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="headingFive">
            <h5 style="cursor: pointer;" class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">Accessibility
             <span class="plus-sign float-right">+</span>
                <span class="minus-sign float-right">-</span>
            </h5>
        </div>
        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
            <div class="card-body">
                <div class="row">
                    @php
                        $accessibility_id = \App\Models\AmenityCategory::where('name','accessibility')->value('_id');
                    @endphp
                    @foreach(getAllSpecialAmenities()->where('category_id',$accessibility_id) as $row)
                    @if(@$data->accessibility_amenties)
                        @if(in_array($row->_id, $data->accessibility_amenties))
                            <div class="col-md-4">
                                <div class="blog-card mb-4 p-3">
                                    <img class="img-fluid d-block mx-auto" src="{{url('/uploads/specialamenities/'.$row->logo)}}" style="border: none;">
                                    <strong style="text-align: center;display: inherit;margin-top: 10px;">{{\Str::limit($row->name, 15)}}</strong>
                                    <p>{{\Str::limit($row->description, 30)}}</p>
                                </div>
                            </div>
                        @endif
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
