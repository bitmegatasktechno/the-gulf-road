<style>
#map1 {
    width: 100%;
    height: 400px;
}
#schedule_tour_start_date .input-group-addon {
    right: 8px;
    cursor: pointer;
    position: absolute;
    padding: 13px 0 0 0;
}
#schedule_tour_time .input-group-addon {
    right: 8px;
    cursor: pointer;
    padding: 13px 0 0 0;
    position: absolute;
}
</style>
@php
    $user = \App\Models\User::where('_id',$data->user_id)->first();
    if(@$user->role){
        if (in_array("AGENT", $user->role)){
            $exist=1;
        }else{
            $exist=0;
        }
    }else{
        $exist=0;
    }
@endphp
<div class="col-md-4 pl-0">
<div id="map12"></div>
    <div class="bg-white rounded p-3 mb-3 instant-booking">
        <p>Starting at</p>
        <h2>{{$data->open_space_daily_currency}} {{$data->open_space_daily_price ?? 0}} <span style="font-size: 12px;color: grey;">/Day</span></h2>
        <div class="input-daterange">
            <div class="row">
                <div class="col-md-12">
                    @if(\Auth::user())
    				<a href="javascript:;" onclick="showMainBookSpaceForm('{{$data->user_id}}');">
                    @else
                    <a href="javascript:;" onclick="showLoginForm();">
                    @endif
                    <button class="red-btn full-width rounded mt-3" autocomplete="off">Book a Day Pass</button></a>
                    @if(\Auth::user())
    				    <a href="javascript:;" onclick="showEnquiryForm('{{$data->user_id}}');">
                    @else
                        <a href="javascript:;" onclick="showLoginForm();">
                    @endif
                    <button class="send-enq-btn full-width mt-3"> Make an Enquiry</button></a>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-white rounded p-3 want-to-do">
        <div class="mt-4">
            <p><strong>Schedule a Tour</strong></p>
            <p>Schedule a tour of the place on a desired date to inspect the place.</p>
            <form name="book_property1" id="book_property1" method="post">
                @csrf
                <input type="hidden" name="property_id" id="property_id" value="{{@$data->_id}}">
                <input type="hidden" name="type" id="type" value="{{Request::segment(4)}}">
                <input type="hidden" name="userto" id="userto" value="{{@$data->user_id}}">
                <div class="input-daterange">
                <div class="row">
                    <div class="col-md-6">
                        <div class='input-group date' id='schedule_tour_start_date'>
                            <input type='text' class="form-control" readonly="" required/>
                            <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                         </div>
                    </div>
                    <div class="col-md-6">
                        <div class='input-group date' id='schedule_tour_time'>
                           <input type='text' class="form-control" required="" readonly="" />
                           <span class="input-group-addon">
                           <span class="glyphicon glyphicon-time"></span>
                           </span>
                        </div>
                    </div>
                </div>
                @if(\Auth::user())
                    <button type="submit" class="red-bordered inspection-btn full-width mb-3 mt-3">Schedule a Tour</button>
                @else
                    <a href="javascript:;" onclick="showLoginForm();"> <button type= "button" class="red-bordered inspection-btn full-width mb-3 mt-3">Schedule a Tour</button></a>
                @endif
            </form>
        </div>
    </div>
    @if(Auth::user())
        <a onclick="showReportForm('{{$data->_id}}')" style="cursor: pointer;"><p class="report-listing text-center"><img class="mr-2" src="{{asset('img/flag_report.png')}}">Report this listing</p></a>
    @endif
</div>
