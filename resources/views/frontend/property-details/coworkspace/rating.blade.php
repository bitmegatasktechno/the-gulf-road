 

<style>

/* Rating Star Widgets Style */
.rating-stars ul {
  list-style-type:none;
  padding:0;

  -moz-user-select:none;
  -webkit-user-select:none;
}
.rating-stars ul > li.star {
  display:inline-block;

}

/* Idle State of the stars */
.rating-stars ul > li.star > i.fa {
  font-size:2.5em; /* Change the size of the stars */
  color:#ccc; /* Color on idle state */
}

/* Hover state of the stars */
.rating-stars ul > li.star.hover > i.fa {
  color:pink;
}

/* Selected state of the stars */
.rating-stars ul > li.star.selected > i.fa {
  color:red;
}
.ratingsetting{
  border: 1px solid;
    border-radius: 33px;
    padding: 5px 11px;
    color: #008489;
}
.select2-container .select2-selection--single{
  padding-top: 4px;
  height: 47px !important;
}
span#select2-phone_code-nd-container{
  font-size: 16px;
}

h4.modal-title {
    font-size: 24px;
    font-weight: 600;
}

.modal-header {
    padding: 16px 20px;
    align-items: center;
}

button.close {
    vertical-align: middle;
    font-size: 28px;
}

label {
    font-size: 16px;
    font-weight: 500;
}

.modal-body.cospace {
    padding: 55px 50px !important;
}

textarea#review {
    height: 100px;
}

.cal-md-12  label {
    margin: 12px 0 10px;
}
</style>
<div class="row rtng-review">
  <div class="col-md-12">
      <h4 class="mt-4 mb-4">Ratings & Reviews
      @if(\Auth::user())
      @if(\Auth::user()->id!=$data->user_id)
           <button class="red-btn enterreview" onclick="allowReview();" style="width: 22%!important;float: right;"><span style="color:white">Write a Review </span></button>
          
      @endif
      @endif
      <h6><strong>{{@$avgtotal}} </strong>  Stars out of 5</h6>
      <ul class="list-inline">
        @if(@$avgtotal==1)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @elseif(@$avgtotal==2)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @elseif(@$avgtotal==3)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @elseif(@$avgtotal==4)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @elseif(@$avgtotal==5)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @endif
      </ul>
  </div>
</div>

@foreach($ratings as $rating)
<div class="row mt-5 dp-flex align-items-center testimonial-inner">
  <div class="col-md-1">
  @if(!isset($rating->user->image) || $rating->user->image=='')
                                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 50px; width: 50px">
                                    @else
                                    @if(file_exists('uploads/profilePics/'.$rating->user->image))
                                    <img class="circle" src="{{url('uploads/profilePics/'.$rating->user->image)}}" style="height: 50px; width: 50px">
                                    @else
                                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 50px; width: 50px">
                                    @endif
                                    @endif
  </div>
  <div class="col-md-5">
      <h6 class="mb-0">{{@$rating->user->name}}</h6>
      <ul class="list-inline mb-0">
        @if(@$rating->average==1)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @elseif(@$rating->average==2)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @elseif(@$rating->average==3)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @elseif(@$rating->average==4)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @elseif(@$rating->average==5)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @endif
      </ul>
  </div>
  <div class="col-md-6 text-right">
      <small>{{ date('d-F,Y', strtotime($rating->create)) }}</small>
  </div>
  <div class="col-md-12">
      <p class="mt-4">{{@$rating->review}}</p>
  </div>
  <div class="col-md-12">
      <hr>
  </div>
</div>
@endforeach
<!-- <div class="col-md-12 text-right">
  <a class="ratings-color" href="">View More Reviews</a>
  <hr>
</div> -->
<script>
function allowReview()
{
    $("#viewrefund").modal('show');

}
function preventReview()
{
    Swal.fire({
                  title: 'Error!',
                  text:  'You are not able to make review Now ! Before Elevate Your Profile',
                  icon:  'error',
                  confirmButtonText: 'Ok'
              })
}
</script>
 
<div class="modal fade" id="viewrefund">
<div class="modal-dialog modal-dialog-centered">
<div class="modal-content">

<div class="modal-header">
<h4 class="modal-title">Write a Reviews</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>

<!-- Modal footer -->
<div class="modal-body cospace text-center p-5">
<input type="hidden" name="user_id" id="user_id" value="@if(\Auth::user()){{\Auth::user()->id}}@endif">
<input type="hidden" name="review_type" id="review_type" value="4">
<input type="hidden" name="type_id" id="type_id" value="{{$data->_id}}">
<div class="row">
<div class="cal-md-6">
  <div style="display:none;">
  <input type="text" id="rating">
</div>
<label>Rating:</label>
</div> 
<div class="cal-md-6">

  <!-- Rating Stars Box -->
    <div class='rating-stars text-center' style="font-size: 9px;">
      <ul id='stars'>
        <li class='star' title='Poor' data-value='1'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='Fair' data-value='2'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='Good' data-value='3'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='Excellent' data-value='4'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='WOW!!!' data-value='5'>
          <i class='fa fa-star fa-fw'></i>
        </li>

      </ul>
    </div>

</div>
</div>
<span id="selectstar" class="hide" style="color: red;">Please Select All</span>
<br>
<div class="row">
<div class="cal-md-12" style="width:100%">
<!-- <label>Comment:</label> -->
</div>
</div>
<div class="row">
<div class="cal-md-12">
</div> <textarea name="review" id="review" required style="width:97%"></textarea>
<span id="selectreview" style="color:red;display:none;">This field is required</span>
</div>
<br>
<div class="cal-md-12" style="width:100%">
  <input type="checkbox" name="accept" id="accept" required checked="checked"><span style="font-size: 12px;"> I certified that this review is based on my own experience and is my genuine opinion of this coworking space.</span>
  <span id="selectaccept" style="color:red;display:none;">Please accept</span>
</div>
<br>
<div class="col-md-12">
                <div class="form-group">
                    <div class="g-recaptcha"  id="g-wiriteReviewWidgetId"></div>
                </div>
              </div>
<br>
<button type="button" class="red-btn sendreview">Submit</button>

</div>

</div>
</div>
</div>
 
@section('scripts')
@include('frontend.profile.common.js')
<script type="text/javascript">
  
$(document).on('click','.sendreview11', function(){
  var ajax_url = WEBSITE_URL+"/property/review";
  var user_id=$("#user_id").val();
  var review_type=$("#review_type").val();
  var type_id=$("#type_id").val();
  var hygine=$("#hygine").val();
  var atmosphere=$("#atmosphere").val();
  var location=$("#location").val();
  var cleanliness=$("#cleanliness").val();
  var service=$("#service").val();
  var review=$("#review").val();


  var accept=$("#accept:checked").val();
  alert(accept);
  if(!accept){
    $("#selectaccept").show();
        return false;
  }


  $.ajax({
    url:ajax_url,
    method:"POST",
    data:{
      user_id:user_id,
      review_type:review_type,
      type_id:type_id,
      atmosphere:atmosphere,
      hygine:hygine,
      location:location,
      review:review,
      cleanliness:cleanliness,
      service:service
    },
    headers:{
      'X-CSRF-TOKEN': '{{ csrf_token() }}',
    },
    success:function(data){
      // stopLoader('.page-content');
      if(data.status){
        Swal.fire({
            title: 'Success!',
            text: data.msg,
            icon: 'success',
            confirmButtonText: 'Ok'
        }).then((result) => {
          window.location.reload();
        });
      }else{
        Swal.fire({
            title: 'Success!',
            text: data.msg,
            icon: 'success',
            confirmButtonText: 'Ok'
        }).then((result) => {
          window.location.reload();
        });
      }
      // alert(data.msg);
    }
  });
});
$(".enterreview").click(function(){
$("#viewrefund").modal('show');
});

$(document).ready(function(){

/* 1. Visualizing things on Hover - See next part for action on click */
$('#stars li').on('mouseover', function(){
var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

// Now highlight all the stars that's not after the current hovered star
$(this).parent().children('li.star').each(function(e){
  if (e < onStar) {
    $(this).addClass('hover');
  }
  else {
    $(this).removeClass('hover');
  }
});

}).on('mouseout', function(){
$(this).parent().children('li.star').each(function(e){
  $(this).removeClass('hover');
});
});


/* 2. Action to perform on click */
$('#stars li').on('click', function(){
var onStar = parseInt($(this).data('value'), 10); // The star currently selected
var stars = $(this).parent().children('li.star');

for (i = 0; i < stars.length; i++) {
  $(stars[i]).removeClass('selected');
}

for (i = 0; i < onStar; i++) {
  $(stars[i]).addClass('selected');
}

// JUST RESPONSE (Not needed)
var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
var msg = "";
if (ratingValue > 1) {
    msg = ratingValue;
}
else {
    msg = ratingValue;
}
responseMessage(msg);

});


});

$(document).ready(function(){

/* 1. Visualizing things on Hover - See next part for action on click */
$('#stars1 li').on('mouseover', function(){
var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

// Now highlight all the stars that's not after the current hovered star
$(this).parent().children('li.star').each(function(e){
if (e < onStar) {
  $(this).addClass('hover');
}
else {
  $(this).removeClass('hover');
}
});

}).on('mouseout', function(){
$(this).parent().children('li.star').each(function(e){
$(this).removeClass('hover');
});
});


/* 2. Action to perform on click */
$('#stars1 li').on('click', function(){
var onStar = parseInt($(this).data('value'), 10); // The star currently selected
var stars = $(this).parent().children('li.star');

for (i = 0; i < stars.length; i++) {
$(stars[i]).removeClass('selected');
}

for (i = 0; i < onStar; i++) {
$(stars[i]).addClass('selected');
}

// JUST RESPONSE (Not needed)
var ratingValue = parseInt($('#stars1 li.selected').last().data('value'), 10);
var msg = "";
if (ratingValue > 1) {
  msg = ratingValue;
}
else {
  msg = ratingValue;
}
responseMessage1(msg);

});


});

$(document).ready(function(){

/* 1. Visualizing things on Hover - See next part for action on click */
$('#stars2 li').on('mouseover', function(){
var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

// Now highlight all the stars that's not after the current hovered star
$(this).parent().children('li.star').each(function(e){
if (e < onStar) {
  $(this).addClass('hover');
}
else {
  $(this).removeClass('hover');
}
});

}).on('mouseout', function(){
$(this).parent().children('li.star').each(function(e){
$(this).removeClass('hover');
});
});


/* 2. Action to perform on click */
$('#stars2 li').on('click', function(){
var onStar = parseInt($(this).data('value'), 10); // The star currently selected
var stars = $(this).parent().children('li.star');

for (i = 0; i < stars.length; i++) {
$(stars[i]).removeClass('selected');
}

for (i = 0; i < onStar; i++) {
$(stars[i]).addClass('selected');
}

// JUST RESPONSE (Not needed)
var ratingValue = parseInt($('#stars2 li.selected').last().data('value'), 10);
var msg = "";
if (ratingValue > 1) {
  msg = ratingValue;
}
else {
  msg = ratingValue;
}
responseMessage2(msg);

});


});

$(document).ready(function(){

/* 1. Visualizing things on Hover - See next part for action on click */
$('#stars3 li').on('mouseover', function(){
var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

// Now highlight all the stars that's not after the current hovered star
$(this).parent().children('li.star').each(function(e){
if (e < onStar) {
  $(this).addClass('hover');
}
else {
  $(this).removeClass('hover');
}
});

}).on('mouseout', function(){
$(this).parent().children('li.star').each(function(e){
$(this).removeClass('hover');
});
});


/* 2. Action to perform on click */
$('#stars3 li').on('click', function(){
var onStar = parseInt($(this).data('value'), 10); // The star currently selected
var stars = $(this).parent().children('li.star');

for (i = 0; i < stars.length; i++) {
$(stars[i]).removeClass('selected');
}

for (i = 0; i < onStar; i++) {
$(stars[i]).addClass('selected');
}

// JUST RESPONSE (Not needed)
var ratingValue = parseInt($('#stars3 li.selected').last().data('value'), 10);
var msg = "";
if (ratingValue > 1) {
  msg = ratingValue;
}
else {
  msg = ratingValue;
}
responseMessage3(msg);

});


});

$(document).ready(function(){

/* 1. Visualizing things on Hover - See next part for action on click */
$('#stars4 li').on('mouseover', function(){
var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

// Now highlight all the stars that's not after the current hovered star
$(this).parent().children('li.star').each(function(e){
if (e < onStar) {
  $(this).addClass('hover');
}
else {
  $(this).removeClass('hover');
}
});

}).on('mouseout', function(){
$(this).parent().children('li.star').each(function(e){
$(this).removeClass('hover');
});
});


/* 2. Action to perform on click */
$('#stars4 li').on('click', function(){
var onStar = parseInt($(this).data('value'), 10); // The star currently selected
var stars = $(this).parent().children('li.star');

for (i = 0; i < stars.length; i++) {
$(stars[i]).removeClass('selected');
}

for (i = 0; i < onStar; i++) {
$(stars[i]).addClass('selected');
}

// JUST RESPONSE (Not needed)
var ratingValue = parseInt($('#stars4 li.selected').last().data('value'), 10);
var msg = "";
if (ratingValue > 1) {
  msg = ratingValue;
}
else {
  msg = ratingValue;
}
responseMessage4(msg);

});


});


function responseMessage(msg) {
$("#hygine").val(msg);
}
function responseMessage1(msg) {
$("#atmosphere").val(msg);
}
function responseMessage2(msg) {
$("#location").val(msg);
}
function responseMessage3(msg) {
$("#cleanliness").val(msg);
}
function responseMessage4(msg) {
$("#service").val(msg);
}

</script>


@endsection
