<div class="col-md-8">
     @include('frontend.property-details.coworkspace.banner')

    <div class="bg-white mb-3 p-3 rounded">
        <div class="row">
        </div>
        <div class="row hd-and-details">
            <div class="col-md-8">
                <h3>{{ucfirst($data->title)}}</h3>
                <span><i class="fa fa-map-marker mr-2"></i>{{$data->address}} {{$data->country}}</span>
            </div>
            <div class="col-md-4">
            
                <small class="info-text-contact float-right" autocomplete="off" data-toggle="modal" data-target="#contact_information" style="cursor: pointer;"><img class="ml-2" src="{{asset('img/contact.png')}}"></small>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
        <div class="row des-and-details">
            <div class="col-md-12">
                <h4 class="mt-2">Description</h4>
                <p class="mb-0">
		             {{\Str::limit($data->description, 250, '')}}
            <?php
                $string = strip_tags($data->description);
                if (strlen($string) > 250) {
                    
                    $stringCut = substr($string, 250);
                    $stringp = '<span id="dots">...</span><span id="text_content" class="content" style="display:none;">'.$stringCut.'</span></p><a href="javascript:show_hide();" class="show_hide" id="show_hide" data-content="toggle-text">Read More</a>';
                    echo $stringp;
                }else{
                        //echo "</p>";
                    }
            ?>
		        
            </div>
        </div>
        <div class="row">
        	 <div class="col-md-12">
		        <h4 class="mt-3 mb-3">Workspace Pricing</h4>
		    </div>
        </div>
        @php @endphp
        @if($data->private_office_available)
        <div class="row amenities workspace-pricing" >
		    <div class="col-md-5">
		    	@if(isset($data->files) && count($data->files) && file_exists('uploads/coworkspce/'.$data->files[0]))
                    <img class="img-fluid rounded" src="{{url('uploads/coworkspce/'.$data->files[0])}}">
                    @else
		        <img class="img-fluid rounded" src="{{asset('img/buy-rent-img.jpg')}}">
		        @endif
		    </div>
		    <div class="col-md-7">
		        <h5>Private Offices</h5>
		        <p>Private offices to accomodate a team of all shapes & sizes. Offices are secure with plenty of space.</p>
		        <div class="brdr rounded">
		            <ul class="list-inline mb-0 bdrb">
		                <li class="text-center bg-light-grey"><span>{{$data->seating_capacity ?? 1}} People</span></li>
		                <li class="text-center">1 Day</li>
		                <li class="text-center">{{$data->private_space_daily_currency}} {{$data->private_space_daily_price ?? 0}}</li>
		            </ul>
		            <ul class="list-inline mb-0 bdrb">
		                <li class="text-center"><span>{{$data->seating_capacity ?? 1}} People</span></li>
		                <li class="text-center">1 Week</li>
		                <li class="text-center">{{$data->private_space_weekly_currency}} {{$data->private_space_weekly_price ?? 0}}</li>
		            </ul>
		            <ul class="list-inline mb-0">
		                <li class="text-center bg-light-grey"><span>{{$data->seating_capacity ?? 1}} People</span></li>
		                <li class="text-center">1 Month</li>
		                <li class="text-center">{{$data->private_space_monthly_currency}} {{$data->private_space_monthly_price ?? 0}}</li>
		            </ul>
		        </div>
		        <div class="mt-3">
				@if(\Auth::user())
				<a href="javascript:;" onclick="showBookSpaceForm('{{$data->user_id}}');">
                @else
                <a href="javascript:;" onclick="showLoginForm();">
                @endif
				<button class="red-btn rounded float-right">Book Space</button></a></div>
		    </div>
		</div>
		@endif
		@if($data->dedicated_desk_available)
		<div class="row amenities workspace-pricing mt-4">
		    <div class="col-md-5">
		        @if(isset($data->files) && count($data->files) && file_exists('uploads/coworkspce/'.$data->files[0]))
                    <img class="img-fluid rounded" src="{{url('uploads/coworkspce/'.$data->files[0])}}">
                @else
		        <img class="img-fluid rounded" src="{{asset('img/buy-rent-img.jpg')}}">
		        @endif
		    </div>
		    <div class="col-md-7">
		        <h5>Dedicated Desk</h5>
		        <p>Dedicated offices to accomodate a team of all shapes & sizes. Offices are secure with plenty of space.</p>
		        <div class="brdr rounded">
		            <ul class="list-inline mb-0 bdrb">
		                <li class="text-center bg-light-grey"><span>1 People</span></li>
		                <li class="text-center">1 Day</li>
		                <li class="text-center">{{$data->dedicated_space_daily_currency}} {{$data->dedicated_space_daily_price ?? 0}}</li>
		            </ul>
		            <ul class="list-inline mb-0 bdrb">
		                <li class="text-center"><span>1 People</span></li>
		                <li class="text-center">1 Week</li>
		                <li class="text-center">{{$data->dedicated_space_weekly_currency}} {{$data->dedicated_space_weekly_price ?? 0}}</li>
		            </ul>
		            <ul class="list-inline mb-0">
		                <li class="text-center bg-light-grey"><span>1 People</span></li>
		                <li class="text-center">1 Month</li>
		                <li class="text-center">{{$data->dedicated_space_monthly_currency}} {{$data->dedicated_space_monthly_price ?? 0}}</li>
		            </ul>
		        </div>
		        <div class="mt-3">
				@if(\Auth::user())
				<a href="javascript:;" onclick="showBookSpaceForm('{{$data->user_id}}');">
                @else
                <a href="javascript:;" onclick="showLoginForm();">
                @endif
				<button class="red-btn rounded float-right">Book Space</button></a></div>
		    </div>
		</div>
		@endif
		@if(isset($data->open_workspace_available) && $data->open_workspace_available)
		<div class="row amenities workspace-pricing mt-4">
		    <div class="col-md-5">
		       @if(isset($data->files) && count($data->files) && file_exists('uploads/coworkspce/'.$data->files[0]))
                    <img class="img-fluid rounded" src="{{url('uploads/coworkspce/'.$data->files[0])}}">
                @else
		        	<img class="img-fluid rounded" src="{{asset('img/buy-rent-img.jpg')}}">
		        @endif
		    </div>
		    <div class="col-md-7">
		        <h5>Open Workspace</h5>
		        <p>Open offices to accomodate a team of all shapes & sizes. Offices are secure with plenty of space.</p>
		        <div class="brdr rounded">
		            <ul class="list-inline mb-0 bdrb">
		                <li class="text-center bg-light-grey"><span>1 Person</span></li>
		                <li class="text-center">1 Day</li>
		                <li class="text-center">{{$data->open_space_daily_currency}} {{$data->open_space_daily_price ?? 0}}</li>
		            </ul>
		            <ul class="list-inline mb-0 bdrb">
		                <li class="text-center"><span>1 Person</span></li>
		                <li class="text-center">1 Week</li>
		                <li class="text-center">{{$data->open_space_weekly_currency}} {{$data->open_space_weekly_price ?? 0}}</li>
		            </ul>
		            <ul class="list-inline mb-0">
		                <li class="text-center bg-light-grey"><span>1 Person</span></li>
		                <li class="text-center">1 Month</li>
		                <li class="text-center">{{$data->open_space_monthly_currency}} {{$data->open_space_monthly_price ?? 0}}</li>
		            </ul>
		        </div>
		        <div class="mt-3">
				@if(\Auth::user())
				<a href="javascript:;" onclick="showBookSpaceForm('{{$data->user_id}}');">
                @else
                <a href="javascript:;" onclick="showLoginForm();">
                @endif
				<button class="red-btn rounded float-right">Book Space</button></a></div>
		    </div>
		</div>
		@endif
        <div class="row amenities">
            <div class="col-md-12">
                <h4 class="mt-4">Amenities</h4>
            </div>
        </div>
         @include('frontend.property-details.coworkspace.amenity')


        @include('frontend.property-details.common-data.map')
        @include('frontend.property-details.coworkspace.rating')
        <br>
        @include('frontend.property-details.common-data.map-nearby')
    </div>
</div>
</div>
</div>

<script>
	function show_hide()
            {
                 var display = $("#text_content").css("display");
                 if(display=='none')
                 {
                     $("#text_content").css("display","unset");
                    $("span#dots").text("");
                     $("a#show_hide").text("Read Less");
                    
                 }else
                 {
                       $("#text_content").css("display","none");
                      
                      $("span#dots").text("...");
                      $("a#show_hide").text("Read More");
                     

                 }
                  
            }
</script>
