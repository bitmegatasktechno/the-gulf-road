@include('frontend.property-details.common-data.banner-section')
<section class="body-light-grey p-0">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="bg-white mb-3 p-3 rounded">
                    @include('frontend.property-details.rent.details')
                    @include('frontend.property-details.common-data.amenities')
                    @include('frontend.property-details.common-data.map')
                    @include('frontend.property-details.rent.rating')
                    <div class="row house-rules dp-flex align-items-center">
                        <div class="col-md-12">
                            <h4 class="mt-4 mb-4">House Rules</h4>
                        </div>
                        <div class="col-md-6">
                            <p><img class="mr-2" src="{{asset('img/no-smoking-new.png')}}">Smoking Allowed: {{ucfirst($data->smoking_allowed)}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><img class="mr-2" src="{{asset('img/no-pets-new.png')}}">Pets Allowed: {{ucfirst($data->pets_allowed)}}</p>
                        </div>
                        <div class="col-md-6">
                            <p><img class="mr-2" src="{{asset('img/no-alcohol.png')}}">Parties Allowed: {{ucfirst($data->parties_allowed)}}</p>
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-1 col-2">
                            <img class="mr-2" src="{{asset('img/money-new.png')}}">
                        </div>
                        <div class="col-md-11 pl-0 col-10">
                            <h6 class="mb-0">In case of any damage to the property, Money will be charged fom the Security Deposit.</h6>
                        </div>
                        <div class="col-md-12">
                            <h5 class="mt-4">Additional Rules</h5>
                            <p>
                                {{$data->additional_rules ?? 'N/A'}}
                            </p>
                        </div>
                    </div>
                    @include('frontend.property-details.common-data.map-nearby')
                    
                </div>
            </div>
            @if(\Auth::user())
            @if(\Auth::user()->id!=$data->user_id)
                @include('frontend.property-details.rent.right-part')
            @endif
            @else
                @include('frontend.property-details.rent.right-part')
            @endif
        </div>
    </div>
</section>
