@php
$package="NO";
 
@endphp


<style>

/* Rating Star Widgets Style */
.rating-stars ul {
  list-style-type:none;
  padding:0;

  -moz-user-select:none;
  -webkit-user-select:none;
}
.rating-stars ul > li.star {
  display:inline-block;

}

/* Idle State of the stars */
.rating-stars ul > li.star > i.fa {
  font-size:2.5em; /* Change the size of the stars */
  color:#ccc; /* Color on idle state */
}

/* Hover state of the stars */
.rating-stars ul > li.star.hover > i.fa {
  color:pink;
}

/* Selected state of the stars */
.rating-stars ul > li.star.selected > i.fa {
  color:red;
}

</style>
<div class="row rtng-review">
  <div class="col-md-12">
      <h4 class="mt-4 mb-4">Ratings & Reviews
      @if(\Auth::user())
      @if(\Auth::user()->id!=$data->user_id)
           <button class="red-btn enterreview" onclick="allowReview();" style="width: 22%!important;float: right;"><span style="color:white">Write a Review </span></button>
           
      @endif
      @endif
    </h4>
      
      <h6><strong>{{@$avgtotal}} </strong>  Stars out of 5</h6>
      <ul class="list-inline">
        @if(@$avgtotal==1)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @elseif(@$avgtotal==2)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @elseif(@$avgtotal==3)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @elseif(@$avgtotal==4)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @elseif(@$avgtotal==5)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @endif
      </ul>
  </div>
</div>
<div class="col-md-12">
  <div class="row dp-flex align-items-center rtng-review ">
  <div class="col-md-2">
      <span>Hygine</span>
  </div>
  <div class="col-md-2 pl-0 pr-0">
      <div class="progress">
          <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="max-width: {{$avghy ? $avghy*20 : 0}}%">
          </div>
      </div>
  </div>
  <div class="col-md-2">
      {{@$avghy}}
  </div>
  <div class="col-md-2">
      <span>Cleanliness</span>
  </div>
  <div class="col-md-2 pl-0 pr-0">
      <div class="progress">
          <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="max-width: {{$avgcle ? $avgcle*20 : 0}}%">
          </div>
      </div>
  </div>
  <div class="col-md-2">
      {{@$avgcle}}
  </div>
</div>
<div class="row dp-flex align-items-center rtng-review">
  <div class="col-md-2">
      <span>Service</span>
  </div>
  <div class="col-md-2 pl-0 pr-0">
      <div class="progress">
          <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="max-width: {{$avgser ? $avgser*20 : 0}}%">
          </div>
      </div>
  </div>
  <div class="col-md-2">
      {{@$avgser}}
  </div>
  <div class="col-md-2">
      <span>Atmosphere</span>
  </div>
  <div class="col-md-2 pl-0 pr-0">
      <div class="progress">
          <div class="progress-bar" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="max-width: {{$avgatm ? $avgatm*20 : 0}}%">
          </div>
      </div>
  </div>
  <div class="col-md-2">
      {{@$avgatm}}
  </div>
</div>
<div class="row dp-flex align-items-center rtng-review">
    <div class="col-md-2">
      <span>Location</span>
  </div>
  <div class="col-md-2 pl-0 pr-0">
      <div class="progress">
          <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="max-width: {{$avgloc ? $avgloc*20 : 0}}%">
          </div>
      </div>
  </div>
  <div class="col-md-2">
      {{@$avgloc}}
  </div>
</div>


</div>
@foreach($ratings as $rating)
<div class="row mt-5 dp-flex align-items-center testimonial-inner">
  <div class="col-md-1">
  @if(!isset($rating->user->image) || $rating->user->image=='')
                                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 50px; width: 50px">
                                    @else
                                    @if(file_exists('uploads/profilePics/'.$rating->user->image))
                                    <img class="circle" src="{{url('uploads/profilePics/'.$rating->user->image)}}" style="height: 50px; width: 50px">
                                    @else
                                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 50px; width: 50px">
                                    @endif
                                    @endif
  </div>
  <div class="col-md-5">
      <h6 class="mb-0">{{@$rating->user->name}}</h6>
      <ul class="list-inline mb-0">
        @if(@$rating->average==1)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @elseif(@$rating->average==2)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @elseif(@$rating->average==3)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @elseif(@$rating->average==4)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @elseif(@$rating->average==5)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @endif
      </ul>
  </div>
  <div class="col-md-6 text-right">
      <small>{{ date('d-F,Y', strtotime($rating->create)) }}</small>
  </div>
  <div class="col-md-12">
      <p class="mt-4">{{@$rating->review}}</p>
  </div>
  <div class="col-md-12">
      <hr>
  </div>
</div>
@endforeach
<!-- <div class="col-md-12 text-right">
  <a class="ratings-color" href="">View More Reviews</a>
  <hr>
</div> -->
 <div class="modal fade" id="viewrefund">
<div class="modal-dialog modal-md">
<div class="modal-content">

<div class="modal-header" style="border-bottom:none">
<h4 class="modal-title" style="text-align: center;color: black;"></h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<h4 style="text-align: center;">Write a Reviews</h4>


<!-- Modal footer -->
<div class="modal-footer text-center" style="display: block;border-top:none">

<input type="hidden" name="review_type" id="review_type" value="2">
<input type="hidden" name="type_id" id="type_id" value="{{@$data->_id}}">
<div class="row">
  <div class="cal-md-6">
    <input type="hidden" id="hygine">
    <input type="hidden" id="atmosphere">
    <input type="hidden" id="service">
    <input type="hidden" id="location">
    <input type="hidden" id="cleanliness">
  
<label>Hygine:</label>
</div>
  <div class="cal-md-6">

    <!-- Rating Stars Box -->
      <div class='rating-stars text-center' style="font-size: 9px;">
        <ul id='stars0'>
          <li class='star' title='Poor' data-value='1'>
            <i class='fa fa-star fa-fw'></i>
          </li>
          <li class='star' title='Fair' data-value='2'>
            <i class='fa fa-star fa-fw'></i>
          </li>
          <li class='star' title='Good' data-value='3'>
            <i class='fa fa-star fa-fw'></i>
          </li>
          <li class='star' title='Excellent' data-value='4'>
            <i class='fa fa-star fa-fw'></i>
          </li>
          <li class='star' title='WOW!!!' data-value='5'>
            <i class='fa fa-star fa-fw'></i>
          </li>

        </ul>
      </div>

  </div>
</div>
<div class="row">
  <div class="cal-md-6">
<label>Atmosphere:</label>
  </div>
  <div class="cal-md-6">
    <div class='rating-stars text-center' style="font-size: 9px;">
      <ul id='stars1'>
        <li class='star' title='Poor' data-value='1'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='Fair' data-value='2'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='Good' data-value='3'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='Excellent' data-value='4'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='WOW!!!' data-value='5'>
          <i class='fa fa-star fa-fw'></i>
        </li>
      </ul>
    </div>
  </div>
</div>
<div class="row">
  <div class="cal-md-6">
<label>Location:</label>
  </div>
  <div class="cal-md-6">
    <div class='rating-stars text-center' style="font-size: 9px;">
      <ul id='stars2'>
        <li class='star' title='Poor' data-value='1'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='Fair' data-value='2'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='Good' data-value='3'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='Excellent' data-value='4'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='WOW!!!' data-value='5'>
          <i class='fa fa-star fa-fw'></i>
        </li>
      </ul>
    </div>
  </div>
</div>
<div class="row">
  <div class="cal-md-6">
<label>Cleanliness:</label>
  </div>
  <div class="cal-md-6">
    <div class='rating-stars text-center' style="font-size: 9px;">
      <ul id='stars3'>
        <li class='star' title='Poor' data-value='1'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='Fair' data-value='2'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='Good' data-value='3'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='Excellent' data-value='4'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='WOW!!!' data-value='5'>
          <i class='fa fa-star fa-fw'></i>
        </li>
      </ul>
    </div>
  </div>
</div>
<div class="row">
  <div class="cal-md-6">
<label>Service:</label>
  </div>
  <div class="cal-md-6">
    <div class='rating-stars text-center' style="font-size: 9px;">
      <ul id='stars4'>
        <li class='star' title='Poor' data-value='1'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='Fair' data-value='2'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='Good' data-value='3'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='Excellent' data-value='4'>
          <i class='fa fa-star fa-fw'></i>
        </li>
        <li class='star' title='WOW!!!' data-value='5'>
          <i class='fa fa-star fa-fw'></i>
        </li>
      </ul>
    </div>
  </div>
</div>
<span id="selectstar" style="color:red;display:none;">Please Select All</span>
<br>
<div class="row">
<div class="cal-md-12" style="width:100%">
<label>Comment:</label>
</div>
</div>
<div class="row">
<div class="cal-md-12">
</div> <textarea name="review" id="review" required style="width:97%"></textarea>
<span id="selectreview" style="color:red;display:none;">This field is required</span>
</div>
<br>
<div class="cal-md-12" style="width:100%">
  <input type="checkbox" name="accept" id="accept" required checked="checked"><span style="font-size: 12px;"> I certified that this review is based on my own experience and is my genuine opinion of this property.</span>
  <span id="selectaccept" style="color:red;display:none;">Please accept</span>
</div>
<div class="cal-md-12" style="width:100%">
 <div class="g-recaptcha"  id="g-wiriteReviewWidgetId"></div>
 
</div>
<br>
<button type="button" class="btn btn-secondary sendreviewrentbuy" style="background: red;margin-bottom: 14px;">Submit</button>

</div>

</div>
</div>
</div>
 <script>
function allowReview()
{
    $("#viewrefund").modal('show');

}
function preventReview()
{
    Swal.fire({
                  title: 'Error!',
                  text:  'You are not able to make review Now ! Before Elevate Your Profile',
                  icon:  'error',
                  confirmButtonText: 'Ok'
              })
}
</script>
