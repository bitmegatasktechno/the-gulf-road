@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
@extends('frontend.layouts.home')
@section('title','Property Details')
@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />

	<link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
     <script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
    <script type="text/javascript">
            var wiriteReviewWidgetId;
            var makeInquiryWidgetId;
            var makeInspectionInquiryWidgetId;
            var makeReportFlagWidgetId;
            var CaptchaCallback = function() {
            wiriteReviewWidgetId = grecaptcha.render('g-wiriteReviewWidgetId', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
            makeInquiryWidgetId = grecaptcha.render('g-makeInquiryWidgetId', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
            makeInspectionInquiryWidgetId = grecaptcha.render('g-makeInspectionInquiryWidgetId', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
            makeReportFlagWidgetId = grecaptcha.render('g-makeReportFlagWidgetId', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
            };
             
    </script>
@endsection
@section('content')

    @include('frontend.layouts.default-header')


<style type="text/css">
        
.bodyFilter {
  position: unset !important;
}
.error{
    color: red !important; 
}

</style>

     @include('frontend.property-details.common-data.filter')

	<!-- <section class="body-light-grey pt-2 pb-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="list-inline breadcrum mb-0">
                        <li><a href="{{url(app()->getLocale().'/home')}}">Home </a></li>
                        <span> / </span>
                        <li><a href="{{url(app()->getLocale().'/rent-property')}}"> Rent Properties</a></li>
                        <span> / </span>
                        <li><a href="javascript:;"> Property Details</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section> -->

    @include('frontend.property-details.rent.section')
<section class="body-light-grey p-0">
	    <div class="container">
            <div class="row">
                <div class="col-md-12 mt-5">
                
					<h2 style="font-weight: 600;">Nearby Homes</h2>
					<span style="font-weight: 200;">Similar homes near your location</span>
                    <p></p>
                </div>
                <div class="container">

                <div id="mydivon" class="fadeIn">
                <div class="row dynamicContent">
                    @include('frontend.property-details.common-data.list')
                </div>
            </div>
                
                <div class="col-md-12 show-more text-center">
                    <a href="{{url(app()->getLocale().'/rent-property/'.$link.'?country='.$link.'&location=')}}"><button class="red-bordered mt-3 mb-5">SHOW MORE <i class="fa fa-angle-right"></i></button></a>
                </div>

            </div>
            </div>
			</div>
	</section>
    @include('frontend.layouts.footer')

@endsection
@section('scripts')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
    <script type="text/javascript">

		$(document).ready(function () {
		    $(".show_hide").on("click", function () {
				var dots = document.getElementById("dots");
				if (dots.style.display === "none") {
					dots.style.display = "inline";
				} else {
					dots.style.display = "none";
				}
		        var txt = $(".content").is(':visible') ? 'Read More' : 'Read Less';
		        $(this).text(txt);
		        $('.content').toggle();
		    });
        });
        

	</script>
@endsection
