@extends('frontend.layouts.home')
@section('title','Cost Calculator')
@section('css')
	<link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
@endsection
@section('content')
    
	@include('frontend.layouts.default-header')
    <section class="buy-rent">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h3 class="mb-3">Cost Estimator</h3>
        <p class="mb-4">Enter your requirements and get the price estimator</p>
        <hr>
        <h4 class="mt-4">AED 15,560 Per Month</h4>
        
          <div class="row">
            <div class="col-md-11">
              <form class="loc-form">
                <img class="img-fluid" src="img/graph-line-bar.png">
                <div class="dots-block-wrap mb-4">
                  <ul class="list-inline">
                    <li><span class="small-dot first"></span>Location</li>
                    <li class=""><span class="small-dot second"></span>Year Built</li>
                    <li class=""><span class="small-dot third"></span>Area</li>
                    <li><span class="small-dot fourth"></span>Accesibility</li>
                    <li class=""><span class="small-dot fifth"></span>Others</li>
                  </ul>
                </div>
            <div class="form-group">
              <label class="">Location</label>
              <input type="email" class="form-control" placeholder="Enter Location Name" id="email">
            </div>
            <div class="row mt-4">
              <div class="col-md-6 mb-2">
                <div class="form-group">
                  <label>House Type</label>
                  <select class="form-control rounded">
                    <option>Apartment</option>
                    <option>Apartment</option>
                  </select>
                </div>              
              </div>
              <div class="col-md-6 mb-2">
                <div class="form-group">
                  <label>Year Build</label>
                  <select class="form-control rounded">
                    <option>Apartment</option>
                    <option>Apartment</option>
                  </select>
                </div> 
              </div>
              <div class="col-md-6 mb-2">
                <div class="form-group">
                  <label>Total Area</label>
                  <select class="form-control rounded">
                    <option>Apartment</option>
                    <option>Apartment</option>
                  </select>
                </div> 
              </div>
              <div class="col-md-6 mb-2">
                <div class="form-group">
                  <label>Accesibality</label>
                  <select class="form-control rounded">
                    <option>Apartment</option>
                    <option>Apartment</option>
                  </select>
                </div> 
              </div> 
              <div class="col-md-6 mb-2">
                <div class="form-group">
                  <label>Bedrooms</label>
                  <select class="form-control rounded">
                    <option>Apartment</option>
                    <option>Apartment</option>
                  </select>
                </div> 
              </div> 
              <div class="col-md-6 mb-2">
                <div class="form-group">
                  <label>Bathrooms</label>
                  <select class="form-control rounded">
                    <option>Apartment</option>
                    <option>Apartment</option>
                  </select>
                </div> 
              </div>  
            </div>
            <button class="red-btn rounded float-right full-width full-width">Continue</button>
          </form>
          </div>
          </div>
      </div>
      <div class="col-md-6">
        <img class="img-responsive" src="img/cost-calculator.png">
      </div>
    </div>
  </div>
</section>
    @include('frontend.layouts.footer')

@endsection