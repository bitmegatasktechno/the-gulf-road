@php
$loc=Request::segment(3);
@endphp
  <style>
.img{
  object-fit:cover;
}
i.fa.fa-heart {
    font-size: 18px !important;
    position: revert !important;
    /* padding-left: 309px !important; */
    color: red;
    top: 12px !important;
    right: 15px;
}

i.fa.fa-heart-o {
    font-size: 18px !important;
    position: revert !important;
    /* padding-left: 309px !important; */
    color: red;
    top: 12px !important;
    right: 15px;
}

a {
  color:black;
}

#shareButtons a {
  font-size: 18px;
  margin-right: 5px;
}
#shareButtons a i.fa.fa-facebook {
 font-size: 16px;  
}

#shareButtons {
    padding: 5px;
    background: white;
    width: 100px;
    border-radius: 24px;
    margin-left: 20em;
}

  </style>
    <section class="property-image-block body-light-grey pt-0" style="margin-top: 82px;">
      <div class="container">
  <div class="row">
    <div class="col-md-7 small-mb-3">
    <div class="img small-full" style="height: 397px;overflow: hidden;width: 98%;position: relative;">
      <div class="ptDetailButtons">
        <p id="shareButtons" style="text-align: center; display: none;"><a href="https://api.whatsapp.com/send?text=Property Share {{env('APP_URL')}}/{{Request::segment(1)}}/{{Request::segment(2)}}/{{Request::segment(3)}}/{{Request::segment(4)}}/{{Request::segment(5)}}" target="_blank" style="color:green;"><i class="fa fa-whatsapp" aria-hidden="true"></i> </a> <a  style="color:blue;" href="https://www.facebook.com/sharer/sharer.php?u={{env('APP_URL')}}/{{Request::segment(1)}}/{{Request::segment(2)}}/{{Request::segment(3)}}/{{Request::segment(4)}}/{{Request::segment(5)}}" target="_blank" ><i class="fa fa-facebook" aria-hidden="true"></i> </a>
</p>
    <a onclick="$('#shareButtons').toggle();"><span class="ptDetailSpan" ><img  src="https://img.icons8.com/windows/32/000000/share-rounded.png"/>  Share Property</span> </a>
    @if(isset(Auth::user()->id))
                @php
                    $fav = \App\Models\Favourate::where('propert_id',$data->_id)->where('user_id',Auth::user()->id)->first();
                    @endphp
                    
                    @if(@$fav)
                    <span  class="ptDetailSpan2" ><i class="fa fa-heart fav-icon" style="" onClick="makefavDetail('{{$data->_id}}')"></i> Save</span>
                    
                    @else
                    <span  class="ptDetailSpan2" ><i class="fa fa-heart-o fav-icon" style="" onClick="makefavDetail('{{$data->_id}}')"></i> Save</span>
                    
                    @endif
                    @endif
    </div>
    @if(!isset($data->files[0]) || $data->files[0]=='')
    <img class="img-fluid small-mb-3" src="{{asset('img/house2.png')}}" style="width: 100%;object-fit: cover; height: 100%;">
      @else
      @if(file_exists('uploads/properties/'.$data->files[0]))
      <img class="img-fluid small-mb-3" src="{{url('uploads/properties/'.$data->files[0])}}" style="width: 100%;object-fit: cover; height: 100%;">
      @else
      <img class="img-fluid small-mb-3" src="{{asset('img/house2.png')}}" style="width: 100%;object-fit: cover; height: 100%;">
      @endif
     @endif

      </div>
    </div>
    <div class="col-md-5 pl-0">
      <div class="row">
        <div class="col-md-6 pl-0">
        <div class="img">
        @if(!isset($data->files[1]) || $data->files[1]=='')
    <img class="img-fluid small-mb-3" src="{{asset('img/house2.png')}}" style="height: 191px;width: 100%;object-fit: cover;">
      @else
      @if(file_exists('uploads/properties/'.$data->files[1]))
      <img class="img-fluid small-mb-3" src="{{url('uploads/properties/'.$data->files[1])}}" style="height: 191px;width: 100%;object-fit: cover;">
      @else
      <img class="img-fluid small-mb-3" src="{{asset('img/house2.png')}}" style="height: 191px;width: 100%;object-fit: cover;">
      @endif
     @endif
      </div>
      <div class="img">
      @if(!isset($data->files[2]) || $data->files[2]=='')
    <img class="img-fluid mt-3" src="{{asset('img/house2.png')}}" style="height: 191px;width: 100%;object-fit: cover;">
      @else
      @if(file_exists('uploads/properties/'.$data->files[2]))
      <img class="img-fluid mt-3 small-mb-3" src="{{url('uploads/properties/'.$data->files[2])}}" style="height: 191px;width: 100%;object-fit: cover;">
      @else
      <img class="img-fluid mt-3 small-mb-3" src="{{asset('img/house2.png')}}" style="height: 191pxwidth: 100%;object-fit: cover;">
      @endif
     @endif
      </div>
    </div>
    <div class="col-md-6 pl-0">
    <div class="img">
    @if(!isset($data->files[3]) || $data->files[3]=='')
    <img class="img-fluid small-mb-3" src="{{asset('img/house2.png')}}" style="height: 191px;width: 100%;object-fit: cover;">
      @else
      @if(file_exists('uploads/properties/'.$data->files[3]))
      <img class="img-fluid small-mb-3" src="{{url('uploads/properties/'.$data->files[3])}}" style="height: 191px;width: 100%;object-fit: cover;">
      @else
      <img class="img-fluid small-mb-3" src="{{asset('img/house2.png')}}" style="height: 191px;width: 100%;object-fit: cover;">
      @endif
     @endif
      </div>
      <a href="#showmore" class="" data-toggle="modal">
        <div class="property-images" style="width: 100%;object-fit: cover;">
        <div class="img">
        @if(!isset($data->files[4]) || $data->files[4]=='')
    <img class="img-fluid mt-3 small-mb-3" src="{{asset('img/house2.png')}}" style="height: 191px;width: 100%;object-fit: cover;">
      @else
      @if(file_exists('uploads/properties/'.$data->files[4]))
      <img class="img-fluid mt-3 small-mb-3" src="{{url('uploads/properties/'.$data->files[4])}}" style="height: 191px;width: 100%;object-fit: cover;">
      @else
      <img class="img-fluid mt-3 small-mb-3" src="{{asset('img/house2.png')}}" style="height: 191px;width: 100%;object-fit: cover;">
      @endif
     @endif
        </div>
          <div class="view-more-img">
          <p class="white-color">Tap to View</p>
        </a>
        </div>
      </div>
    </div>
      </div>
    </div>
  </div>

  <div id="showmore" class="modal fade">
        <div class="modal-dialog" style="min-width: 70%;">
            <div class="modal-content">
                <div class="modal-header">
				            <h4 class="modal-title">Property Images</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    
                </div>
                <div class="modal-body">
                         	
                    <div class="row property-images-block">
                      <div class="col-md-12">

                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                          <div class="carousel-inner">
                            @php $i=0; @endphp
                            @foreach($data->files as $img)
                            @php $i++; @endphp
                            <div class="carousel-item @if($i == 1) active @endif">
                              <img class="d-block w-100" src="{{url('uploads/properties/'.$img)}}" alt="First slide">
                            </div>
                            @endforeach
                          </div>
                          <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </div>
                      
                      </div>
                    </div>
                </div>
            </div>
          </div>
</section>


