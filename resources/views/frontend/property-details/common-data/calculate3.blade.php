@extends('frontend.layouts.home')
@section('title','Cost Calculator')
@section('css')
	<link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
@endsection
@section('content')
    
	@include('frontend.layouts.default-header')
  <style>
  .inputselect-bx select {
    width: 48.1% !important;
  }

  #accordionExample h4 {
    font-size: 17px !important;
    color: black !important;
    font-weight: 800 !important;
    text-decoration: none !important;
  }

  #accordionExample h4:hover {
    font-size: 17px !important;
    color: black !important;
    font-weight: 800 !important;
    text-decoration: none !important;
  }

  #accordionExample li{
    font-size: 12px !important;
  }

  .progress {
    height: 5px !important;
  }

  .bg-danger1 {
    background-color: #286ca7!important;
  }
  .bg-danger2 {
    background-color: #a72874!important;
  }
  .bg-danger3 {
    background-color: #28a1a7!important;
  }
  .bg-danger4 {
    background-color: #2e1c67!important;
  }
  .bg-danger5 {
    background-color: #d800ff!important;
  }
  .bg-danger6 {
    background-color: #fbff00!important;
  }

  </style>
    <section class="buy-rent">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h3 class="mb-3">Cost Estimator</h3>
        <p class="mb-4">Enter your requirements and get the price estimator</p>
        <hr>

        <div class="accordion" id="accordionExample">

          @if(isset($_GET['rent']) && $_GET['rent'] && isset($_GET['security']) && $_GET['security'])


            @php

            if(isset($_GET['rent'])){
              $_GET['rent_duration'] = 1;

              $total = $_GET['rent']*$_GET['rent_duration'];

              if(!empty($_GET['electricity'])){
                $total = $total+($_GET['electricity']*$_GET['rent_duration']);
              }

              if(!empty($_GET['transportation'])){
                $total = $total+($_GET['transportation']*$_GET['rent_duration']);
              }
              if(!empty($_GET['chiller'])){
                $total = $total+($_GET['chiller']*$_GET['rent_duration']);
              }
              if(!empty($_GET['internet'])){

                $total = $total+($_GET['internet']*$_GET['rent_duration']);
              }
              if(!empty($_GET['gas'])){

                $total = $total+($_GET['gas']*$_GET['rent_duration']);
              }
              if(!empty($_GET['comission'])){

                $total = $total+($_GET['comission']*$_GET['rent_duration']);
              }
              if(!empty($_GET['other'])){

                $total = $total+($_GET['other']*$_GET['rent_duration']);
              }
              if(!empty($_GET['security'])){

                $main_total = $total+$_GET['security'];
              }
            }
        

            @endphp


          <div class="card">
            <div class="card-header" id="headingOne">
              <h2 class="mb-0">
                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  <h4 class="mt-4">AED {{$total}} @if(!empty($_GET['security'])) + ({{$_GET['security']}} Security one time) @endif Per Month</h4>
                </button>
              </h2>
            </div>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
              <div class="card-body">
                <div class="progress">
                  <div class="progress-bar bg-success" style="max-width:{{(( $_GET['rent']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>

                  @if(!empty($_GET['security']))
                  <div class="progress-bar bg-warning" style="max-width:{{(( $_GET['security'])/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['electricity']))
                  <div class="progress-bar bg-danger" style="max-width:{{(( $_GET['electricity']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['chiller']))
                  <div class="progress-bar bg-danger1" style="max-width:{{(( $_GET['chiller']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['internet']))
                  <div class="progress-bar bg-danger2" style="max-width:{{(( $_GET['internet']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['gas']))
                  <div class="progress-bar bg-danger3" style="max-width:{{(( $_GET['gas']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['comission']))
                  <div class="progress-bar bg-danger4" style="max-width:{{(( $_GET['comission']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['other']))
                  <div class="progress-bar bg-danger5" style="max-width:{{(( $_GET['other']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['transportation']))
                  <div class="progress-bar bg-danger6" style="max-width:{{(( $_GET['transportation']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                </div>
                <div class="dots-block-wrap mb-4">
                  <ul class="list-inline">
                    @if(isset($_GET['rent']) && $_GET['rent'])
                    <li><span class="small-dot first"></span>Rent (<?php echo $_GET['rent']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['security']) && $_GET['security'])
                    <li class=""><span class="small-dot second"></span>Security (<?php echo $_GET['security']; ?>)</li>
                    @endif
                    @if(isset($_GET['electricity']) && $_GET['electricity'])
                    <li class=""><span class="small-dot third"></span>Electricity  (<?php echo $_GET['electricity']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['chiller']) && $_GET['chiller'])
                    <li class=""><span class="small-dot fourth"></span>Chiller  (<?php echo $_GET['chiller']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['internet']) && $_GET['internet'])
                    <li class=""><span class="small-dot fifth"></span>Internet  (<?php echo $_GET['internet']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['gas']) && $_GET['gas'])
                    <li class=""><span class="small-dot sixth"></span>Gas  (<?php echo $_GET['gas']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['comission']) && $_GET['comission'])
                    <li><span class="small-dot seventh"></span>Comission  (<?php echo $_GET['comission']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['other']) && $_GET['other'])
                    <li class=""><span class="small-dot eight"></span>Others (<?php echo $_GET['other']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['transportation']) && $_GET['transportation'])
                    <li class=""><span class="small-dot nineth"></span>Transportation  (<?php echo $_GET['transportation']*$_GET['rent_duration']; ?>)</li>
                    @endif
                  </ul>
                </div>
              </div>
            </div>
          </div>
          @endif



          @if(isset($_GET['rent']) && $_GET['rent'] && isset($_GET['security']) && $_GET['security'])


            @php
            $_GET['rent_duration'] = 3;
            if(isset($_GET['rent'])){

              $total = $_GET['rent']*$_GET['rent_duration'];

              if(!empty($_GET['electricity'])){
                $total = $total+($_GET['electricity']*$_GET['rent_duration']);
              }

              if(!empty($_GET['transportation'])){
                $total = $total+($_GET['transportation']*$_GET['rent_duration']);
              }
              if(!empty($_GET['chiller'])){
                $total = $total+($_GET['chiller']*$_GET['rent_duration']);
              }
              if(!empty($_GET['internet'])){

                $total = $total+($_GET['internet']*$_GET['rent_duration']);
              }
              if(!empty($_GET['gas'])){

                $total = $total+($_GET['gas']*$_GET['rent_duration']);
              }
              if(!empty($_GET['comission'])){

                $total = $total+($_GET['comission']*$_GET['rent_duration']);
              }
              if(!empty($_GET['other'])){

                $total = $total+($_GET['other']*$_GET['rent_duration']);
              }
              if(!empty($_GET['security'])){

                $main_total = $total+$_GET['security'];
              }
            }

            @endphp


          <div class="card">
            <div class="card-header" id="heading2">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                  <h4 class="mt-4">AED {{$total}} + ({{$_GET['security']}} Security one time) Per Quarter</h4>
                </button>
              </h2>
            </div>

            <div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#accordionExample">
              <div class="card-body">
                <div class="progress">
                  <div class="progress-bar bg-success" style="max-width:{{(( $_GET['rent']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>

                  @if(!empty($_GET['security']))
                  <div class="progress-bar bg-warning" style="max-width:{{(( $_GET['security'])/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['electricity']))
                  <div class="progress-bar bg-danger" style="max-width:{{(( $_GET['electricity']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['chiller']))
                  <div class="progress-bar bg-danger1" style="max-width:{{(( $_GET['chiller']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['internet']))
                  <div class="progress-bar bg-danger2" style="max-width:{{(( $_GET['internet']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['gas']))
                  <div class="progress-bar bg-danger3" style="max-width:{{(( $_GET['gas']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['comission']))
                  <div class="progress-bar bg-danger4" style="max-width:{{(( $_GET['comission']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['other']))
                  <div class="progress-bar bg-danger5" style="max-width:{{(( $_GET['other']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['transportation']))
                  <div class="progress-bar bg-danger6" style="max-width:{{(( $_GET['transportation']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                </div>
                <div class="dots-block-wrap mb-4">
                  <ul class="list-inline">
                    @if(isset($_GET['rent']) && $_GET['rent'])
                    <li><span class="small-dot first"></span>Rent (<?php echo $_GET['rent']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['security']) && $_GET['security'])
                    <li class=""><span class="small-dot second"></span>Security (<?php echo $_GET['security']; ?>)</li>
                    @endif
                    @if(isset($_GET['electricity']) && $_GET['electricity'])
                    <li class=""><span class="small-dot third"></span>Electricity  (<?php echo $_GET['electricity']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['chiller']) && $_GET['chiller'])
                    <li class=""><span class="small-dot fourth"></span>Chiller  (<?php echo $_GET['chiller']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['internet']) && $_GET['internet'])
                    <li class=""><span class="small-dot fifth"></span>Internet  (<?php echo $_GET['internet']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['gas']) && $_GET['gas'])
                    <li class=""><span class="small-dot sixth"></span>Gas  (<?php echo $_GET['gas']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['comission']) && $_GET['comission'])
                    <li><span class="small-dot seventh"></span>Comission  (<?php echo $_GET['comission']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['other']) && $_GET['other'])
                    <li class=""><span class="small-dot eight"></span>Others (<?php echo $_GET['other']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['transportation']) && $_GET['transportation'])
                    <li class=""><span class="small-dot nineth"></span>Transportation  (<?php echo $_GET['transportation']*$_GET['rent_duration']; ?>)</li>
                    @endif
                  </ul>
                </div>
              </div>
            </div>
          </div>
          @endif


          @if(isset($_GET['rent']) && $_GET['rent'] && isset($_GET['security']) && $_GET['security'])


            @php
            $_GET['rent_duration'] = 6;
            if(isset($_GET['rent'])){

              $total = $_GET['rent']*$_GET['rent_duration'];

              if(!empty($_GET['electricity'])){
                $total = $total+($_GET['electricity']*$_GET['rent_duration']);
              }

              if(!empty($_GET['transportation'])){
                $total = $total+($_GET['transportation']*$_GET['rent_duration']);
              }
              if(!empty($_GET['chiller'])){
                $total = $total+($_GET['chiller']*$_GET['rent_duration']);
              }
              if(!empty($_GET['internet'])){

                $total = $total+($_GET['internet']*$_GET['rent_duration']);
              }
              if(!empty($_GET['gas'])){

                $total = $total+($_GET['gas']*$_GET['rent_duration']);
              }
              if(!empty($_GET['comission'])){

                $total = $total+($_GET['comission']*$_GET['rent_duration']);
              }
              if(!empty($_GET['other'])){

                $total = $total+($_GET['other']*$_GET['rent_duration']);
              }
              if(!empty($_GET['security'])){

                $main_total = $total+$_GET['security'];
              }
            }
            @endphp


          <div class="card">
            <div class="card-header" id="heading3">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                  <h4 class="mt-4">AED {{$total}} + ({{$_GET['security']}} Security one time) Half Yearly</h4>
                </button>
              </h2>
            </div>

            <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#accordionExample">
              <div class="card-body">
                <div class="progress">
                  <div class="progress-bar bg-success" style="max-width:{{(( $_GET['rent']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>

                  @if(!empty($_GET['security']))
                  <div class="progress-bar bg-warning" style="max-width:{{(( $_GET['security'])/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['electricity']))
                  <div class="progress-bar bg-danger" style="max-width:{{(( $_GET['electricity']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['chiller']))
                  <div class="progress-bar bg-danger1" style="max-width:{{(( $_GET['chiller']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['internet']))
                  <div class="progress-bar bg-danger2" style="max-width:{{(( $_GET['internet']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['gas']))
                  <div class="progress-bar bg-danger3" style="max-width:{{(( $_GET['gas']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['comission']))
                  <div class="progress-bar bg-danger4" style="max-width:{{(( $_GET['comission']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['other']))
                  <div class="progress-bar bg-danger5" style="max-width:{{(( $_GET['other']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['transportation']))
                  <div class="progress-bar bg-danger6" style="max-width:{{(( $_GET['transportation']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                </div>
                <div class="dots-block-wrap mb-4">
                  <ul class="list-inline">
                    @if(isset($_GET['rent']) && $_GET['rent'])
                    <li><span class="small-dot first"></span>Rent (<?php echo $_GET['rent']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['security']) && $_GET['security'])
                    <li class=""><span class="small-dot second"></span>Security (<?php echo $_GET['security']; ?>)</li>
                    @endif
                    @if(isset($_GET['electricity']) && $_GET['electricity'])
                    <li class=""><span class="small-dot third"></span>Electricity  (<?php echo $_GET['electricity']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['chiller']) && $_GET['chiller'])
                    <li class=""><span class="small-dot fourth"></span>Chiller  (<?php echo $_GET['chiller']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['internet']) && $_GET['internet'])
                    <li class=""><span class="small-dot fifth"></span>Internet  (<?php echo $_GET['internet']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['gas']) && $_GET['gas'])
                    <li class=""><span class="small-dot sixth"></span>Gas  (<?php echo $_GET['gas']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['comission']) && $_GET['comission'])
                    <li><span class="small-dot seventh"></span>Comission  (<?php echo $_GET['comission']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['other']) && $_GET['other'])
                    <li class=""><span class="small-dot eight"></span>Others (<?php echo $_GET['other']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['transportation']) && $_GET['transportation'])
                    <li class=""><span class="small-dot nineth"></span>Transportation  (<?php echo $_GET['transportation']*$_GET['rent_duration']; ?>)</li>
                    @endif
                  </ul>
                </div>
              </div>
            </div>
          </div>
          @endif


          @if(isset($_GET['rent']) && $_GET['rent'] && isset($_GET['security']) && $_GET['security'])


            @php
            $_GET['rent_duration'] = 12;
            if(isset($_GET['rent'])){

              $total = $_GET['rent']*$_GET['rent_duration'];

              if(!empty($_GET['electricity'])){
                $total = $total+($_GET['electricity']*$_GET['rent_duration']);
              }

              if(!empty($_GET['transportation'])){
                $total = $total+($_GET['transportation']*$_GET['rent_duration']);
              }
              if(!empty($_GET['chiller'])){
                $total = $total+($_GET['chiller']*$_GET['rent_duration']);
              }
              if(!empty($_GET['internet'])){

                $total = $total+($_GET['internet']*$_GET['rent_duration']);
              }
              if(!empty($_GET['gas'])){

                $total = $total+($_GET['gas']*$_GET['rent_duration']);
              }
              if(!empty($_GET['comission'])){

                $total = $total+($_GET['comission']*$_GET['rent_duration']);
              }
              if(!empty($_GET['other'])){

                $total = $total+($_GET['other']*$_GET['rent_duration']);
              }
              if(!empty($_GET['security'])){

                $main_total = $total+$_GET['security'];
              }
            }

            @endphp


          <div class="card">
            <div class="card-header" id="heading4">
              <h2 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse3">
                  <h4 class="mt-4">AED {{$total}} + ({{$_GET['security']}} Security one time) Yearly</h4>
                </button>
              </h2>
            </div>

            <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordionExample">
              <div class="card-body">
                <div class="progress">
                  <div class="progress-bar bg-success" style="max-width:{{(( $_GET['rent']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>

                  @if(!empty($_GET['security']))
                  <div class="progress-bar bg-warning" style="max-width:{{(( $_GET['security'])/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['electricity']))
                  <div class="progress-bar bg-danger" style="max-width:{{(( $_GET['electricity']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['chiller']))
                  <div class="progress-bar bg-danger1" style="max-width:{{(( $_GET['chiller']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['internet']))
                  <div class="progress-bar bg-danger2" style="max-width:{{(( $_GET['internet']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['gas']))
                  <div class="progress-bar bg-danger3" style="max-width:{{(( $_GET['gas']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['comission']))
                  <div class="progress-bar bg-danger4" style="max-width:{{(( $_GET['comission']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['other']))
                  <div class="progress-bar bg-danger5" style="max-width:{{(( $_GET['other']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                  @if(!empty($_GET['transportation']))
                  <div class="progress-bar bg-danger6" style="max-width:{{(( $_GET['transportation']*$_GET['rent_duration'] )/$main_total)*100}}%">
                  </div>
                  @endif
                </div>
                <div class="dots-block-wrap mb-4">
                  <ul class="list-inline">
                    @if(isset($_GET['rent']) && $_GET['rent'])
                    <li><span class="small-dot first"></span>Rent (<?php echo $_GET['rent']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['security']) && $_GET['security'])
                    <li class=""><span class="small-dot second"></span>Security (<?php echo $_GET['security']; ?>)</li>
                    @endif
                    @if(isset($_GET['electricity']) && $_GET['electricity'])
                    <li class=""><span class="small-dot third"></span>Electricity  (<?php echo $_GET['electricity']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['chiller']) && $_GET['chiller'])
                    <li class=""><span class="small-dot fourth"></span>Chiller  (<?php echo $_GET['chiller']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['internet']) && $_GET['internet'])
                    <li class=""><span class="small-dot fifth"></span>Internet  (<?php echo $_GET['internet']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['gas']) && $_GET['gas'])
                    <li class=""><span class="small-dot sixth"></span>Gas  (<?php echo $_GET['gas']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['comission']) && $_GET['comission'])
                    <li><span class="small-dot seventh"></span>Comission  (<?php echo $_GET['comission']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['other']) && $_GET['other'])
                    <li class=""><span class="small-dot eight"></span>Others (<?php echo $_GET['other']*$_GET['rent_duration']; ?>)</li>
                    @endif
                    @if(isset($_GET['transportation']) && $_GET['transportation'])
                    <li class=""><span class="small-dot nineth"></span>Transportation  (<?php echo $_GET['transportation']*$_GET['rent_duration']; ?>)</li>
                    @endif
                  </ul>
                </div>
              </div>
            </div>
          </div>
          @endif




        </div>

        
         <br/>

          <div class="row">
            <div class="col-md-11">
              <form class="loc-form">
              <div class="form-group inputselect-bx">
                <label class="dp-block">Enter the Rent/Buy Amount (per month)</label>
                <div class="inpt-slct">
                  <input type="number" class="" name="rent" placeholder="Enter Your Rent/Buy Amount" id="rent" required>
                  <input type="hidden" name="rent_duration" value="1">
                  <!--<select name="rent_duration" class="form-control">
                    <option value="1">Monthly</option>
                    <option value="3">Quatrly</option>
                    <option value="6">Half-Yearly</option>
                    <option value="12">Yearly</option>
                  </select> -->
                </div>
              </div>
            <div class="row mt-4">
              <div class="col-md-6 mb-2">
                <div class="form-group">
                   <input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1" checked=""  name="security_c" onchange="changeRequired(this,'security')">
                    <label for="styled-checkbox-1">Security Fees*</label>
                    <input type="number" class="form-control rounded" name="security" id="security" required>
                </div>              
              </div>
              <div class="col-md-6 mb-2">
                <div class="form-group">
                  <input class="styled-checkbox" id="styled-checkbox-2" type="checkbox" value="value1" checked=""  name="electricity_c" onchange="changeRequired(this,'electricity')">
                    <label for="styled-checkbox-2">Electricity & Water Bills</label>
                    <input type="number" class="form-control rounded" name="electricity" id="electricity"  required>
                </div> 
              </div>
              <div class="col-md-6 mb-2">
                <div class="form-group">
                  <input class="styled-checkbox" id="styled-checkbox-3" type="checkbox" value="value1" checked=""  name="transportation_c" onchange="changeRequired(this,'transportation')">
                    <label for="styled-checkbox-3">Transportation to office</label>
                    <input type="number" class="form-control rounded" name="transportation" id="transportation" required>
                </div> 
              </div>
              <div class="col-md-6 mb-2">
                <div class="form-group">
                  <input class="styled-checkbox" id="styled-checkbox-4" type="checkbox" value="value1" checked="" name="chiller_c" onchange="changeRequired(this,'chiller')">
                    <label for="styled-checkbox-4">Chiller Ac</label>
                    <input type="number" class="form-control rounded" name="chiller" id="chiller"  required>
                </div> 
              </div>
              <div class="col-md-6 mb-2">
                <div class="form-group">
                  <input class="styled-checkbox" id="styled-checkbox-6" type="checkbox" value="value1" checked=""  name="internet_c" onchange="changeRequired(this,'internet')">
                    <label for="styled-checkbox-6">Internet</label>
                    <input type="number" class="form-control rounded" name="internet" id="internet" required>
                </div> 
              </div>
              <div class="col-md-6 mb-2">
                <div class="form-group">
                  <input class="styled-checkbox" id="styled-checkbox-7" type="checkbox" value="value1" checked=""  name="gas_c" onchange="changeRequired(this,'gas')">
                    <label for="styled-checkbox-7">Gas</label>
                    <input type="number" class="form-control rounded" name="gas" id="gas"  required>
                </div> 
              </div>
              <div class="col-md-6 mb-2">
                <div class="form-group">
                  <input class="styled-checkbox" id="styled-checkbox-8" checked="" type="checkbox" value="value1"  name="comission_c" onchange="changeRequired(this,'comission')">
                    <label for="styled-checkbox-8">Comission</label>
                    <input type="number" class="form-control rounded" name="comission" id="comission" required>
                </div> 
              </div>
              <div class="col-md-6 mb-2">
                <div class="form-group">
                  <input class="styled-checkbox" id="styled-checkbox-9" checked="" type="checkbox" value="value1" name="other_c" onchange="changeRequired(this,'other')">
                    <label for="styled-checkbox-9">Other</label>
                    <input type="number" class="form-control rounded" name="other" id="other" required>
                </div> 
              </div>
            </div>
            <div class="row">

            <div class="col-md-6">
              <a href="{{url(app()->getLocale().'/property/'.$data['loc'].'/'.$data['type'].'/'.$data['id'])}}"><button type="button" class="red-btn full-width rounded" style="background: #fdc03c;border: 1px solid #fdc03c;" autocomplete="off">Back</button></a>
            </div>

            <div class="col-md-6">
              <button class="red-btn rounded float-right full-width full-width">Calculate Estimate</button>
            </div>
          </div>
          </form>
          </div>
          </div>
      </div>
      <div class="col-md-6">
        <img class="img-responsive" src="/assets/img/cost-calculator.png">
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  function changeRequired(obj,id)
  {
    if($(obj).prop("checked") == true){
      $('#'+id).attr('required',true);
    }else{
      $('#'+id).attr('required',false);
    }
  }
</script>


@include('frontend.layouts.footer')

@endsection