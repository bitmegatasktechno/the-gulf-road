<style>
    #map {
    height: 400px;
    width: 100%;
}
</style>


<!-- coworkspace swap buy rent    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
 -->
    <div class="row amenities pt-2">
            <div class="col-md-12">
              <h4 class="mt-4 mb-4">Location</h4>
              <span class="mb-2"><i class="fa fa-map-marker mr-2"></i>
                @if($listingg_type=='rent' || $listingg_type=='swap' || $listingg_type=='buy' )
                {{$data->location_name}}
                @elseif($listingg_type=='coworkspace' )
                 {{$data->address}}
                @endif
                , {{$data->country}}</span>
              <p></p>
              <div class="col-md-12">
                <button id="submit" onClick="initMap('init')" class="red-btn" style="margin-top: 2px;position: absolute;width: 183px;"><span>Show On Map</span></button>
            </div>
              <div id="map" style="display: none;"></div>
            </div>`
          </div>
          <div class="row distance-calculator pt-2">
            <div class="col-md-12 distanceCalcultor" style="display: none;" >
                
              <h4 class="mt-4 mb-1">Distance Calculator</h4>
              <p>Enter destination location to get the distance from this location</p>
              <div class="row">
                <div class="col-md-9">
                  <input class="full-width" id="address" type="textbox" name="" placeholder="“Enter Destination Location”">
                  <span id="address_error" class="error"></span>
                </div>
                <div class="col-md-3">
                  <button id="submit" onClick="getLocation()" class="red-btn" style="margin-top: 2px;
    position: absolute;
    width: 118px;"><span>Calculate</span></button>
                </div>
              </div>
            </div>
            <span id="distancecalc" style="margin-left: 17px;margin-top: 10px;padding: 5px;"></span>
            <input type="hidden" id="old_lat" value="{{@$data->getLatLong()['lat'] ?? '25.2048'}}">
            <input type="hidden" id="old_long" value="{{@$data->getLatLong()['lng'] ?? '55.2708'}}">
            <input type="hidden"  name="lat" id="lat">
            <input type="hidden" name="longitude" id="lng">
            <input type="hidden" name="countryTo" id="countryTo">
            <input type="hidden" value="{{$data->location_name}}" name="countryFrom" id="countryFrom">
    

 
    @if(env('APP_ACTIVE_MAP') =='patel_map')
    <style>
    #address_result ul li 
    {
    display: block;
      width: 100%;
  }
    #address_result
    {
        z-index: 9;
    }
</style>
        <script src="https://mapapi.cloud.huawei.com/mapjs/v1/api/js?callback=initMap&key={{env('PATEL_MAP_KEY')}}"></script>

    <script>



function initMap(val='') {

  document.getElementById('map').innerHTML='';
  const myLatLng = {
    lat: <?php echo @$data->getLatLong()['lat'] ? @$data->getLatLong()['lat'] : 25.2048 ?>,
    lng: <?php echo @$data->getLatLong()['lng'] ? @$data->getLatLong()['lng'] : 55.2708 ?>
  };
    var title="<?php echo ucfirst($data->property_title) ?>";
      if(title==''){
        var title="<?php echo ucfirst($data->title) ?>";
      }


    var mapOptions = {};
    mapOptions.center = myLatLng;
    mapOptions.zoom = 5;
    var map = new HWMapJsSDK.HWMap(document.getElementById('map'), mapOptions);

     if(val !=='')
  {
    $('#map').show();
  $('.distanceCalcultor').show();  
  var searchBoxInput = document.getElementById('address');
 var acOptions = {
                location: {
                    lat: 48.856613,
                    lng: 2.352222
                },
                radius: 10000,
                customHandler: callback
            };
            var autocomplete;
            // Obtain the input element.
            // Create the HWAutocomplete object and set a listener.
            autocomplete = new HWMapJsSDK.HWAutocomplete(searchBoxInput, acOptions);
            autocomplete.addListener('site_changed', printSites);


 
         function callback(index, data, name) {
                console.log(index, data, name);
                var div
                div = document.createElement('div');
                div.innerHTML = name;
                return div;
            }
            // Listener.
            function printSites() {
                 
                
                var site = autocomplete.getSite();
                var latitude = site.location.lat;
                var longitude = site.location.lng;
                var country_name = site.name;
                
                var results = "Autocomplete Results:\n";
                var str = "Name=" + site.name + "\n"
                    + "SiteID=" + site.siteId + "\n"
                    + "Latitude=" + site.location.lat + "\n"
                    + "Longitude=" + site.location.lng + "\n"
                    + "Address=" + site.formatAddress + "\n";

                    
                results = results + str;

                $('#lat').val(latitude);
                $('#lng').val(longitude);
                $('#countryTo').val(country_name);
                $('#country_name').val(country_name);
                console.log(latitude, longitude);
                /*console.log(results);*/
            }


            mMarker = new HWMapJsSDK.HWMarker({
                map: map,
                position: myLatLng,
                zIndex: 10,
                label: {
                    text: title.charAt(0).toLocaleUpperCase(),
                    offsetY: -30,
                    fontSize: '13px'
                },
                icon: {
                    opacity:1,
                    scale: 1,
                }
            });


            infoWindow = new HWMapJsSDK.HWInfoWindow({
                            map : map,
                            position: myLatLng,
                            content:  title ,
                            offset: [0, -40],
                        });

                         mMarker.addListener('click', () => { 
                            infoWindow.open();
                           
                             

                        });
                         
  }

     


}

function getLocation()
 {
//   alert(address);
$('#address_error').html('');
  var address = $('#address').val(); 
  if(address == "")
  {
    $('#address_error').html('Please enter address');
    return false;
  }
 
    var latitude = $('#lat').val();

      var longitude =  $('#lng').val();

  var latitudeTo = latitude;
  var longitudeTo = longitude;
  var latitudeFrom = $("#old_lat").val();
  var longitudeFrom = $("#old_long").val();
  var Tocountry = $("#address").val();
  var Fromcountry = $("#countryFrom").val();
  var ajax_url = WEBSITE_URL+"/getLongLat";
$.ajax({
          url:ajax_url,
          method:"POST",
          data:{
            latitudeTo:latitudeTo,
            longitudeTo:longitudeTo,
            latitudeFrom:latitudeFrom,
            longitudeFrom:longitudeFrom,
          },
          headers:{
            'X-CSRF-TOKEN': '{{ csrf_token() }}',
          },
          success:function(data){
              $("#distancecalc").text(Fromcountry + ' To ' + Tocountry + ' Distance is ' + data.distance.toFixed(2) + ' Km');
          }
            
        });
       
}
    </script>
    @elseif(env('APP_ACTIVE_MAP')=='google_map')
     <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&libraries=places&v=weekly" defer ></script>
    <script>



function initMap() {
  $('#map').show();
  $('.distanceCalcultor').show();
  const myLatLng = {
    lat: <?php echo @$data->getLatLong()['lat'] ? @$data->getLatLong()['lat'] : 25.2048 ?>,
    lng: <?php echo @$data->getLatLong()['lng'] ? @$data->getLatLong()['lng'] : 55.2708 ?>
  };
    var title="<?php echo ucfirst($data->property_title) ?>";
      if(title==''){
        var title="<?php echo ucfirst($data->title) ?>";
      }

  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 4,
    center: myLatLng
  });
    var input = document.getElementById('address');

 map.controls[google.maps.ControlPosition.TOP_RIGHT].push();

var autocomplete = new google.maps.places.Autocomplete(input);
autocomplete.bindTo('bounds', map);
autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);
            var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        // var marker = new google.maps.Marker({
        //   map: map,
        //   anchorPoint: new google.maps.Point(0, -29),
        //   title: "<?php echo ucfirst($data->title) ?>"
        // });
        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindow.open(map, marker);
        });
  new google.maps.Marker({
    position: myLatLng,
    map,
    title:title,
    label: {
        color: 'black',
        fontWeight: 'bold',
        // text: title
  },
  });
}

function getLocation()
 {
//   alert(address);
$('#address_error').html('');
  var address = $('#address').val();
  if(address == "")
  {
    $('#address_error').html('Please enter address');
    return false;
  }
//   alert(address);
  var geocoder = new google.maps.Geocoder();

  geocoder.geocode( { 'address': address}, function(results, status)
  {


    if (status == google.maps.GeocoderStatus.OK)
    {
      console.log(results[0],"afdcfsa");
      console.log(results[0].address_components);
      const countryObj=results[0].address_components.filter(val=>{
            if(val.types.includes('country')){
                return true
            }
            return false;
      });
     console.log(countryObj,'the country obj');


     var country_name = countryObj[0].long_name;
     console.log(country_name,'name');
      var latitude = results[0].geometry.location.lat();

      var longitude = results[0].geometry.location.lng();
      // alert(latitude +"-----"+ longitude);
     $('#lat').val(latitude);
     $('#lng').val(longitude);
     $('#countryTo').val(country_name);
     $('#country_name').val(country_name);
    console.log(latitude, longitude);
    }

  var latitudeTo = latitude;
  var longitudeTo = longitude;
  var latitudeFrom = $("#old_lat").val();
  var longitudeFrom = $("#old_long").val();
  var Tocountry = $("#address").val();
  var Fromcountry = $("#countryFrom").val();
  var ajax_url = WEBSITE_URL+"/getLongLat";
$.ajax({
          url:ajax_url,
          method:"POST",
          data:{
            latitudeTo:latitudeTo,
            longitudeTo:longitudeTo,
            latitudeFrom:latitudeFrom,
            longitudeFrom:longitudeFrom,
          },
          headers:{
            'X-CSRF-TOKEN': '{{ csrf_token() }}',
          },
          success:function(data){
              $("#distancecalc").text(Fromcountry + ' To ' + Tocountry + ' Distance is ' + data.distance.toFixed(2) + ' Km');
          }
            
        });
      });
}
    </script>
    @endif
    </div>
