


@if(count($myProperty)>0)
@foreach($myProperty as $rentProperty)
@php
    $totaluser = \App\Models\Review::where('is_approved',1)->where('review_type',"2")->where('type_id',$rentProperty->_id)->count();
    $ratinggiven1=\App\Models\Review::where('is_approved',1)->where('review_type',"2")->where('type_id',$rentProperty->_id)->sum('average');
    $totalrating=@$totaluser*5;
    if($totalrating == 0){
        $totalrating=1;
    }
    $avg=number_format(@$ratinggiven1/@$totalrating*5);
    @endphp
                <div class="col-md-3">
                    <div class="property-card bg-white rounded">
                    @if($rentProperty->is_premium==1)
                                    <button class="red-btn red-btn-new" style="position: absolute;padding: 3px;margin: 10px;border-radius: 6px;"><img src="/assets/img/Premium_Tick.png" > &nbsp; Premium</button>

                                    @endif
                    @if(isset(Auth::user()->id))
                    @php
                    $fav = \App\Models\Favourate::where('propert_id',$rentProperty->_id)->where('user_id',Auth::user()->id)->first();
                    @endphp
                    @endif
                    @if(@$fav)
                    <i class="fa fa-heart fav-icon" onClick="makefav('{{$rentProperty->_id}}')" style="position: absolute !important;"></i>
                    @else
                    <i class="fa fa-heart-o fav-icon" onClick="makefav('{{$rentProperty->_id}}')"  style="position: absolute !important;"></i>
                    @endif

                    @if($rentProperty->listing_type=='rent')
                    <a href="{{url(app()->getLocale().'/property/'.$link.'/rent/'.$rentProperty->_id)}}">

                    @elseif($rentProperty->listing_type=='sale')
                    <a href="{{url(app()->getLocale().'/property/'.$link.'/buy/'.$rentProperty->_id)}}">
                    @endif

                    @if(!isset($rentProperty->files[0]) || $rentProperty->files[0]=='')
                    <img class="img-fluid property-img" src="{{asset('img/house2.png')}}" >
                    @else
                    @if(file_exists('uploads/properties/'.$rentProperty->files[0]))
                    <img class="img-fluid property-img" src="{{url('uploads/properties/'.$rentProperty->files[0])}}" >
                    @else
                    <img class="img-fluid property-img" src="{{asset('img/house2.png')}}" >
                    @endif
                    @endif
                        <div class="pl-3 pr-3">
                        <h5>{{@$rentProperty->house_type}}  </h5>
                        <h2>{{\Str::limit(@$rentProperty->property_title, 20, '...')}}</h2>
                         <p>{{\Str::limit(ucfirst($rentProperty->location_name), 55)}}, {{$rentProperty->country}}</p>
                       <!--  <p>{{@$rentProperty->house_number}}, {{@$rentProperty->landmark}}, {{@$rentProperty->country}}</p> -->
                        <h6>{{@$rentProperty->rent}} AED / yr</h6>
                            <ul class="list-inline">
                            @if(@$avg==1)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==2)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==3)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==4)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==5)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg == 0)
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                
                                @endif
                                <li class="white-color text" style="color:black !important;">{{@$avg}}</li>
                            </ul>
                            <ul class="list-inline red-list mb-4">
                            <li class="mr-3"><img class="mr-1" src="{{asset('img/red-bed.png')}}"> {{@$rentProperty->no_of_bedrooms}}</li>
                            <li class="mr-3"><img class="mr-1" src="{{asset('img/red-tub.png')}}"> {{@$rentProperty->no_of_bathrooms}}</li>
                            <li class="mr-3"><img class="mr-1" src="{{asset('img/red-car.png')}}"> {{@$rentProperty->no_of_open_parking}}</li>
                            </ul>
                        </div>
                        </a>
                    </div>
                </div>
                @endforeach
                @else
    <div class="" style="display: block;
        max-height: 60px;
        text-align: center;
        background-color: #d6e9c6;
        line-height: 4;
        width: 100%;
        ">
        <p style="font-size: 16px;font-weight: 700;">No Result Found</p>
    </div>
@endif
