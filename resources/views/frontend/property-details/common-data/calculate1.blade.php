@php
$lang=Request::segment(1);
$loc=Request::segment(3);
$type=Request::segment(4);
$id=Request::segment(5);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
@extends('frontend.layouts.home')
@section('title','Cost Calculator')
@section('css')
	<link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
@endsection
@section('content')
    
	@include('frontend.layouts.default-header')
<!-- Bannar Section -->
<section class="buy-rent">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h3 class="mb-3">Cost Estimator</h3>
        <p class="mb-4">Enter your requirements and get the price estimator</p>
          <div class="row">
            <div class="col-md-11">
              <form class="loc-form">
            <div class="form-group">
              <label class="">Location</label>
              <input type="text" class="form-control" placeholder="Enter Location Name" id="location" value="{{@$result->location_name}}" readonly>
            </div>
            <div class="row mt-4">
              <div class="col-md-6 mb-2">
                <div class="form-group">
                  <label>House Type</label>
                  <input type="text" class="form-control" placeholder="Enter House Type" id="housetype" value="{{@$result->house_type}}" readonly>
                </div>              
              </div>
              <div class="col-md-6 mb-2">
                <div class="form-group">
                  <label>Year Build</label>
                  <input type="text" class="form-control" placeholder="Enter Year Build" id="yearbuild" value="" readonly>
                </div> 
              </div>
              <div class="col-md-6 mb-2">
                <div class="form-group">
                  <label>Total Area</label>
                  <input type="text" class="form-control" placeholder="Enter Total Area" id="area" value="{{@$result->surface_area ? @$result->surface_area : @$result->build_up_area}}" readonly>
                </div> 
              </div>
              <div class="col-md-6 mb-2">
                <div class="form-group">
                  <label>Accesibality</label>
                  <input type="text" class="form-control" placeholder="Enter Accesibality" id="accesibality" value="" readonly>
                </div> 
              </div> 
              <div class="col-md-6 mb-2">
                <div class="form-group">
                  <label>Bedrooms</label>
                  <input type="text" class="form-control" placeholder="Enter Bedrooms" id="bedrooms" value="{{@$result->no_of_bedrooms}}" readonly>
                </div> 
              </div> 
              <div class="col-md-6 mb-2">
                <div class="form-group">
                  <label>Bathrooms</label>
                  <input type="text" class="form-control" placeholder="Enter Bathrooms" id="bathrooms" value="{{@$result->no_of_bathrooms}}" readonly>
                </div> 
              </div>  
            </div>
            <a href="{{url(app()->getLocale().'/cost-calculator2/'.$link.'/'.$type.'/'.$id)}}"><button class="red-btn rounded float-right full-width full-width">Continue</button></a>
          </form>
          </div>
          </div>
      </div>
      <div class="col-md-6">
        <img class="img-responsive" src="{{asset('img/cost-calculator.png')}}">
      </div>
    </div>
  </div>
</section>
@include('frontend.layouts.footer')

@endsection