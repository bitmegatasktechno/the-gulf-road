<div class="row amenities">
    <div class="col-md-12">
        <h4 class="mt-4">Amenities</h4>
    </div>
    @php
        $amenities = $data->amenties;
        if(!is_array($amenities)){
            $amenities = [];
        }

    @endphp
    
    @foreach(getAllAmenities() as $row)
    
        @if(in_array($row['_id'], $amenities))
    
            <div class="col-md-4 col-6">
                <div class="amint-block">
                @if(file_exists('uploads/amenities/'.$row['logo']))
                <img src="{{url('uploads/amenities/'.$row['logo'])}}">
                        @endif
                    
                    <h5 class="mt-2">{{ucfirst($row['name'])}}</h5>
                </div>
            </div>
        @endif
    @endforeach
</div>
