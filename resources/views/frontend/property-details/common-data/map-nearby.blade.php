
 <style>
#near-by-map {
    height: 600px;
    width: 100%;
    display: inline-block !important;
}
.tabs-nav li {
  float: left;
  width: 25%;
  list-style: none;
}
.tabs-nav li:first-child a {
  border-right: 0;
  border-top-left-radius: 6px;
}
.tabs-nav li:last-child a {
  border-top-right-radius: 6px;
}
.tab-s a {
    background: #f5f5f5;
    border: 1px solid #cecfd5;
    color: #ff4251;
    display: block;
    font-weight: 600;
    padding: 10px 0;
    text-align: center;
    text-decoration: none;
}
.tab-s a:hover {
    color: #ffffff;
    background: #ff4251;
}
.tab-active a {
    background: #ff4251;
    border-bottom-color: transparent;
    color: #ffffff;
    cursor: default;
}
.tabs-stage {
  border: 1px solid #cecfd5;
  border-radius: 0 0 6px 6px;
  border-top: 0;
  clear: both;
  padding: 24px 30px;
  position: relative;
  top: -1px;
}
ul.tabs-nav {
    margin: 0;
    padding: 0;
}
li.tab-s a:before {
    font-family: fontawesome;
    font-size: 16px;
    margin-right: 6px;
    font-weight: 100;
}
 
@media (max-width: 767px){


.tab-s a {
    padding: 10px 2px;
    font-size: 12px;
}
.tabs-nav li span {
    display: none;
}
}
</style>

  <script>
  window.console = window.console || function(t) {};
</script>

  
  
  <script>
  if (document.location.search.match(/type=embed/gi)) {
    window.parent.postMessage("resize", "*");
  }
</script>
<section class="pt-0"  >
   <div class="container">
      <div class="row">
         <div class="col-sm-12">
            <a href="#map-nearby" class="red-btn" data-toggle="modal">
            Show Near By
            </a>
         </div>
      </div>
   </div>
   <div id="map-nearby" class="modal fade">
   <div class="modal-dialog" style="min-width: 70%;">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">Map NearBy</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         </div>
         <div class="modal-body">
            <div class="tabs">
               <ul class="tabs-nav">
                  <!-- <li class="tab-s tab-active"><a href="#tab-1" onclick="initNearByMap('PROPERTY')"><span>PROPERTY</a></li> -->
                  <li class="tab-s tab-active"><a href="#tab-2" onclick="initNearByMap('SCHOOL')"><i class="fa fa-graduation-cap"></i><span>SCHOOLS</span></a></li>
                  <li class="tab-s"><a href="#tab-3" onclick="initNearByMap('RESTAURANT')"><i class="fa fa-cutlery"></i><span>RESTAURANTS</span></a></li>
                  <li class="tab-s"><a href="#tab-4"  onclick="initNearByMap('HOSPITAL')"><i class="fa fa-plus" ></i><span>HOSPITALS</a></span></li>
                  <li class="tab-s"><a href="#tab-5" onclick="initNearByMap('PARK')" ><i class="fa fa-product-hunt"></i><span>PARKS</span></a></li>
               </ul>
               <!-- <div class="">
                  <div id="tab-1" style="display: none;">
                    
                  </div>
                  <div id="tab-2" style="display: block;">

                  </div>
                  <div id="tab-3" style="display: block;">
                     
                  </div>
                  <div id="tab-4" style="display: block;">
                     
                  </div>
                  <div id="tab-5" style="display: block;">
                     
                  </div>
               </div> -->
                <div class="row">
                  <div class="col-sm-12">
                    <div id="near-by-map" ></div>
                  </div>
                </div>
            </div>
         </div>
      </div>
   </div>
</section>

  <script src="https://cpwebassets.codepen.io/assets/common/stopExecutionOnTimeout-1b93190375e9ccc259df3a57c1abc0e64599724ae30d7ea4c6877eb615f89387.js"></script>

  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script id="rendered-js">

  $('.tabs-stage div').hide();
  $('.tabs-stage div:first').show();
  $('.tabs-nav li:first').addClass('tab-active');

  // Change tab class and display content
  $('.tabs-nav a').on('click', function (event) {
  event.preventDefault();
  $('.tabs-nav li').removeClass('tab-active');
  $(this).parent().addClass('tab-active');
  $('.tabs-stage div').hide();
  $($(this).attr('href')).show();
  });

  </script>
  @if(env('APP_ACTIVE_MAP') =='patel_map')
          <script src="https://mapapi.cloud.huawei.com/mapjs/v1/api/js?callback=initNearByMap&key={{env('PATEL_MAP_KEY')}}"></script>

  <script type="text/javascript">
    function initNearByMap(poiType)
    {
              document.getElementById('near-by-map').innerHTML='';
                const myLatLng = {
                  lat: <?php echo @$data->getLatLong()['lat'] ? @$data->getLatLong()['lat'] : 25.2048 ?>,
                  lng: <?php echo @$data->getLatLong()['lng'] ? @$data->getLatLong()['lng'] : 55.2708 ?>
                }; 

                var map;
                var mapOptions = {};
                mapOptions.center = myLatLng;
                mapOptions.zoom = 15;
                map = new HWMapJsSDK.HWMap(document.getElementById('near-by-map'), mapOptions);
                var siteService
                // Initialize the HWSiteService object.
                siteService = new HWMapJsSDK.HWSiteService();
                // Construct a request.
                var request = {
                    location: myLatLng,
                    poiType:poiType
 
                };
                // Call the search API. In the API, result indicates the search result and status indicates the search status. 
                siteService.nearbySearch(request, function (result, status) {
                    if (status == '0') {
                        for (var i = 0; i < result.sites.length; i++) {
                            // Add a marker on the map.

                            var marker = new HWMapJsSDK.HWMarker({
                                map: map,
                                position: {lat: result.sites[i].location.lat, lng: result.sites[i].location.lng},
                                label: result.sites[i].name,
                                animation: 'DROP',
                            });
                        }
                        // Set the map center.
                        map.setCenter({lat: result.sites[0].location.lat, lng: result.sites[0].location.lng})
                    }
                });
    }
    //initNearByMap('PROPERTY');
  </script>
  @elseif(env('APP_ACTIVE_MAP')=='google_map')

  @endif


