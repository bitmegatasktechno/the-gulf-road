@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
<!-- <style>
.first-two-box li:nth-child(6){
    width: 10.4% !important;
}
.error{
    color:red !important;
    font-size:13px !important;
}
    </style> -->
    <!-- <input type="hidden" value="{{$link}}" id="link">
<section class="top-searh-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 first-two-box">
                <form name="filter_listing" id="filter_listing">
                <input type="hidden" name="user_id" value="{{@$id}}">
                    <ul class="list-inline mb-0">
                        <li class="mb-2">
                            <div class="search-box">
                                <input class="full-width location_name" type="text" placeholder="“Search properties by location”" name="location" autocomplete="off" value="{{isset($_GET['location']) ? $_GET['location']: ''}}">
                                <img class="search-icon" src="{{asset('img/Search.png')}}">
                            </div>
                        </li>
                        

                        <li class="mb-2">
                            @php
                                $houseUrl = '';
                                if(isset($_GET['house_type'])){
                                    $houseUrl = $_GET['house_type'];
                                }
                            @endphp
                            <select name="house_type" class="house_type_filter">
                                <option value="">Property Type</option>
                                @foreach(getAllHouseTypes() as $row)
                                    <option value="{{$row->name}}" @if(strtolower($houseUrl)==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                @endforeach
                            </select>
                        </li>
                        <li class="mb-2">
                        @php
                            $rentUrl = '';
                            if(isset($_GET['rent_frequenecy'])){
                                $rentUrl = $_GET['rent_frequenecy'];
                            }
                        @endphp
                            <select name="rent_frequenecy" class="rent_frequenecy_filter">
                                <option value="">Rent Frequency</option>
                                @foreach(getAllRentFrequency() as $key=>$value)
                                    <option value="{{$key}}" @if($rentUrl==$key) selected @endif>{{ucfirst($value)}}</option>
                                @endforeach
                            </select>
                        </li>
                        <li class="mb-2">
                            <select name="price" class="price_filter">
                                <option value="">Price</option>
                                <option value="asc">Low to High</option>
                                <option value="desc">High to Low</option>
                            </select>
                        </li>
                        <li class="mb-2">
                            @php
                                $bedUrl = '';
                                if(isset($_GET['bedrooms'])){
                                    $bedUrl = $_GET['bedrooms'];
                                }
                            @endphp
                            <select name="bedrooms" class="bedrooms_filter">
                                <option value="">Beds</option>
                                @foreach(range(1,10) as $row)
                                    <option value="{{$row}}" @if($bedUrl==$row) selected @endif>{{$row}}</option>
                                @endforeach
                            </select>
                        </li>
                        

                        
                        

                        
                        <li class="mb-2">
                            <select name="more" class="more_filter">
                                <option value="">More Filters</option>
                                
                            </select>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</section> -->

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script> -->
    <script type="text/javascript">
    var link=$("#link").val();
        $(document).ready(function(){
var suc=$("#success").val();

if(suc=='success'){
    
  Swal.fire({
                title: 'Success!',
                text: 'Added Succesfully',
                icon: 'success',
                confirmButtonText: 'Ok'
            }).then((result) => {
              // window.location.reload();
            });
}
});
function validateHhMm(inputField) {
  var isValid = /^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/.test(inputField.value);
  if (!isValid) {
    return false;
  } 
  return true;
}
function reportadmin(){
    Swal.fire({
                title: 'Success!',
                text: 'Reported Succesfully',
                icon: 'success',
                confirmButtonText: 'Ok'
            }).then((result) => {
              // window.location.reload();
            });
}
var pathArray = window.location.pathname.split( '/' );
var segment_1 = pathArray[1];
var segment_2 = pathArray[2];
var segment_3 = pathArray[3];
var segment_4 = pathArray[4];
		function toggleDiv(divid)
		{
			varon = divid + 'on';
			varoff = divid + 'off';

			if(document.getElementById(varon).style.display == 'none')
			{
				document.getElementById(varon).style.display = 'block';
				document.getElementById(varoff).style.display = 'none';
			}
			else
			{ 
				document.getElementById(varoff).style.display = 'block';
				document.getElementById(varon).style.display = 'none';
			}
		} 

		var filter_data = $("form[name=filter_listing]").serialize();
    	var jqxhr = {abort: function () {  }};

		// save user personal informations.
		$(document).on("click", ".pagination li a", function (e){
            e.preventDefault();
            startLoader('.page-content');
            var url = $(this).attr('href');
            var page = url.split('page=')[1];      
            loadListings(url, 'filter_listing');
        });

        $(document).on("click", ".reset_filter", function (e){
            alert();
            e.preventDefault();
            $("form[name='filter_listing']")[0].reset();
            $("option:selected").prop("selected", false);
            $("form[name='filter_listing']").find('input').val('');

            var url = "{{url(app()->getLocale().'/filter-property/'.$link)}}";
            loadListings(url, 'filter_listing');
        });

        $(document).on('keyup', '.location_name', function () {
            if($(this).val().length > 3)
            {
                var url = "{{url(app()->getLocale().'/filter-property/'.$link)}}";
            	loadListings(url, 'filter_listing');
            }
            if($(this).val().length == 0)
            {
                var url = "{{url(app()->getLocale().'/filter-property/'.$link)}}";
            	loadListings(url, 'filter_listing');
            }
        });

        $(document).on('change','.house_type_filter,.bedrooms_filter,.bathrooms_filter,.price_filter,.country_filter,.rent_frequenecy_filter', function(){
          
        	var url = "{{url(app()->getLocale().'/filter-property/'.$link)}}";
        	loadListings(url, 'filter_listing');
        });

        function loadListings(url,filter_form_name){
            alert('i mhere');
            var filtering = $("form[name=filter_listing]").serialize();
            //abort previous ajax request if any
            jqxhr.abort();
            jqxhr =$.ajax({
                type : 'get',
                url : url,
                data : {filtering,
                    segment_3:segment_4},
                dataType : 'html',
                beforeSend:function(){
                    // startLoader('body');
                },
                success : function(data){
                    console.log(data);
                    // data = data.trim();
                    // $(".dynamicContent").empty().html(data);
                    if(segment_4=='rent'){
                      window.location = '{{url(app()->getLocale()."/rent-property/".$link)}}'+'?'+$("form[name=filter_listing]").serialize();
                    }else if(segment_4=='buy'){
                      window.location = '{{url(app()->getLocale()."/buy-property/".$link)}}'+'?'+$("form[name=filter_listing]").serialize();
                    }else if(segment_4=='swap'){
                        window.location = '{{url(app()->getLocale()."/swap/".$link)}}'+'?'+$("form[name=filter_listing]").serialize();
                    }else if(segment_4=='coworkspace'){
                        window.location = '{{url(app()->getLocale()."/coworkspace/".$link)}}'+'?'+$("form[name=filter_listing]").serialize();
                    }
                },
                error : function(response){
                    stopLoader('body');
                },
                complete:function(){
                    // stopLoader('body');
                }
            });
        }

        function getFilter(id,type){
    $.ajax({
        method:'get',
              url:"{{url(app()->getLocale().'/tab-property')}}",
              data:{
                id:id,
                type:type
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function(data) {
                data = data.trim();
                $(".dynamicContent").empty().html(data);
              }
    });
}

function getRating(type,id){
    if(type==6){
        $(".rating6").css('background-color','#E4002B');
       
        $(".rating5").css('background-color','white');
        $(".rating4").css('background-color','white');
        $(".rating3").css('background-color','white');
        $(".rating2").css('background-color','white');
        $(".rating1").css('background-color','white');
    }
    if(type==5){
        $(".rating5").css('background-color','#E4002B');
        $(".rating6").css('background-color','white');
        $(".rating4").css('background-color','white');
        $(".rating3").css('background-color','white');
        $(".rating2").css('background-color','white');
        $(".rating1").css('background-color','white');
    }
    if(type==4){
        $(".rating4").css('background-color','#E4002B');
        $(".rating6").css('background-color','white');
        $(".rating5").css('background-color','white');
        $(".rating3").css('background-color','white');
        $(".rating2").css('background-color','white');
        $(".rating1").css('background-color','white');
    }
    if(type==3){
        
        $(".rating3").css('background-color','#E4002B');
        $(".rating6").css('background-color','white');
        $(".rating4").css('background-color','white');
        $(".rating5").css('background-color','white');
        $(".rating2").css('background-color','white');
        $(".rating1").css('background-color','white');
    }
    if(type==2){
        $(".rating2").css('background-color','#E4002B');
        $(".rating6").css('background-color','white');
        $(".rating4").css('background-color','white');
        $(".rating3").css('background-color','white');
        $(".rating5").css('background-color','white');
        $(".rating1").css('background-color','white');
    }
    if(type==1){
        $(".rating1").css('background-color','#E4002B');
        $(".rating6").css('background-color','white');
        $(".rating4").css('background-color','white');
        $(".rating3").css('background-color','white');
        $(".rating2").css('background-color','white');
        $(".rating5").css('background-color','white');
    }
    $.ajax({
        method:'get',
              url:"{{url(app()->getLocale().'/rating-list')}}",
              data:{
                type:type,
                id:id
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function(data) {
                
                data = data.trim();
                $(".dynamicRating").empty().html(data);
              }
    });
}

$(".enterreview").click(function(){
      $("#viewrefund").modal('show');
      });

        $(document).ready(function(){

/* 1. Visualizing things on Hover - See next part for action on click */
$('#stars li').on('mouseover', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

  // Now highlight all the stars that's not after the current hovered star
  $(this).parent().children('li.star').each(function(e){
    if (e < onStar) {
      $(this).addClass('hover');
    }
    else {
      $(this).removeClass('hover');
    }
  });

}).on('mouseout', function(){
  $(this).parent().children('li.star').each(function(e){
    $(this).removeClass('hover');
  });
});


/* 2. Action to perform on click */
$('#stars li').on('click', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently selected
  var stars = $(this).parent().children('li.star');

  for (i = 0; i < stars.length; i++) {
    $(stars[i]).removeClass('selected');
  }

  for (i = 0; i < onStar; i++) {
    $(stars[i]).addClass('selected');
  }

  // JUST RESPONSE (Not needed)
  var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
  var msg = "";
  if (ratingValue > 1) {
      msg = ratingValue;
  }
  else {
      msg = ratingValue;
  }
  $("#rating").val(msg);
  responseMessage(msg);

});


$('#stars0 li').on('mouseover', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

  // Now highlight all the stars that's not after the current hovered star
  $(this).parent().children('li.star').each(function(e){
    if (e < onStar) {
      $(this).addClass('hover');
    }
    else {
      $(this).removeClass('hover');
    }
  });

}).on('mouseout', function(){
  $(this).parent().children('li.star').each(function(e){
    $(this).removeClass('hover');
  });
});


/* 2. Action to perform on click */
$('#stars0 li').on('click', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently selected
  var stars = $(this).parent().children('li.star');

  for (i = 0; i < stars.length; i++) {
    $(stars[i]).removeClass('selected');
  }

  for (i = 0; i < onStar; i++) {
    $(stars[i]).addClass('selected');
  }

  // JUST RESPONSE (Not needed)
  var ratingValue = parseInt($('#stars0 li.selected').last().data('value'), 10);
  var msg = "";
  if (ratingValue > 1) {
      msg = ratingValue;
  }
  else {
      msg = ratingValue;
  }
  $("#hygine").val(msg);
  responseMessage(msg);

});




$('#stars1 li').on('mouseover', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

  // Now highlight all the stars that's not after the current hovered star
  $(this).parent().children('li.star').each(function(e){
    if (e < onStar) {
      $(this).addClass('hover');
    }
    else {
      $(this).removeClass('hover');
    }
  });

}).on('mouseout', function(){
  $(this).parent().children('li.star').each(function(e){
    $(this).removeClass('hover');
  });
});


/* 2. Action to perform on click */
$('#stars1 li').on('click', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently selected
  var stars = $(this).parent().children('li.star');

  for (i = 0; i < stars.length; i++) {
    $(stars[i]).removeClass('selected');
  }

  for (i = 0; i < onStar; i++) {
    $(stars[i]).addClass('selected');
  }

  // JUST RESPONSE (Not needed)
  var ratingValue = parseInt($('#stars1 li.selected').last().data('value'), 10);
  var msg = "";
  if (ratingValue > 1) {
      msg = ratingValue;
  }
  else {
      msg = ratingValue;
  }
  $("#atmosphere").val(msg);
  responseMessage(msg);
  
});



$('#stars2 li').on('mouseover', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

  // Now highlight all the stars that's not after the current hovered star
  $(this).parent().children('li.star').each(function(e){
    if (e < onStar) {
      $(this).addClass('hover');
    }
    else {
      $(this).removeClass('hover');
    }
  });

}).on('mouseout', function(){
  $(this).parent().children('li.star').each(function(e){
    $(this).removeClass('hover');
  });
});


/* 2. Action to perform on click */
$('#stars2 li').on('click', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently selected
  var stars = $(this).parent().children('li.star');

  for (i = 0; i < stars.length; i++) {
    $(stars[i]).removeClass('selected');
  }

  for (i = 0; i < onStar; i++) {
    $(stars[i]).addClass('selected');
  }

  // JUST RESPONSE (Not needed)
  var ratingValue = parseInt($('#stars2 li.selected').last().data('value'), 10);
  var msg = "";
  if (ratingValue > 1) {
      msg = ratingValue;
  }
  else {
      msg = ratingValue;
  }
  $("#service").val(msg);
  responseMessage(msg);

});



$('#stars3 li').on('mouseover', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

  // Now highlight all the stars that's not after the current hovered star
  $(this).parent().children('li.star').each(function(e){
    if (e < onStar) {
      $(this).addClass('hover');
    }
    else {
      $(this).removeClass('hover');
    }
  });

}).on('mouseout', function(){
  $(this).parent().children('li.star').each(function(e){
    $(this).removeClass('hover');
  });
});


/* 2. Action to perform on click */
$('#stars3 li').on('click', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently selected
  var stars = $(this).parent().children('li.star');

  for (i = 0; i < stars.length; i++) {
    $(stars[i]).removeClass('selected');
  }

  for (i = 0; i < onStar; i++) {
    $(stars[i]).addClass('selected');
  }

  // JUST RESPONSE (Not needed)
  var ratingValue = parseInt($('#stars3 li.selected').last().data('value'), 10);
  var msg = "";
  if (ratingValue > 1) {
      msg = ratingValue;
  }
  else {
      msg = ratingValue;
  }
  $("#location").val(msg);
  responseMessage(msg);

});



$('#stars4 li').on('mouseover', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

  // Now highlight all the stars that's not after the current hovered star
  $(this).parent().children('li.star').each(function(e){
    if (e < onStar) {
      $(this).addClass('hover');
    }
    else {
      $(this).removeClass('hover');
    }
  });

}).on('mouseout', function(){
  $(this).parent().children('li.star').each(function(e){
    $(this).removeClass('hover');
  });
});


/* 2. Action to perform on click */
$('#stars4 li').on('click', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently selected
  var stars = $(this).parent().children('li.star');

  for (i = 0; i < stars.length; i++) {
    $(stars[i]).removeClass('selected');
  }

  for (i = 0; i < onStar; i++) {
    $(stars[i]).addClass('selected');
  }

  // JUST RESPONSE (Not needed)
  var ratingValue = parseInt($('#stars4 li.selected').last().data('value'), 10);
  var msg = "";
  if (ratingValue > 1) {
      msg = ratingValue;
  }
  else {
      msg = ratingValue;
  }
  $("#cleanliness").val(msg);
  responseMessage(msg);

});


});

function responseMessage(msg) {
  $("#rating").val(msg);
}

$(document).on('click','.sendreviewrentbuy', function(){
        var ajax_url = WEBSITE_URL+"/property/review";
        var review_type=$("#review_type").val();
        var type_id=$("#type_id").val();
        var hygine=$("#hygine").val();
        var atmosphere=$("#atmosphere").val();
        var service=$("#service").val();
        var location=$("#location").val();
        var cleanliness=$("#cleanliness").val();
        if(hygine=='' || atmosphere=='' || service=='' || location=='' || cleanliness==''){
          $("#selectstar").show();
              return false;
        }
        var review=$("#review").val();
        if(review==''){
          $("#selectreview").show();
              return false;
        }

        

        var accept=$("#accept:checked").val();
        if(!accept){
          $("#selectaccept").show();
              return false;
        }

         if (grecaptcha.getResponse(wiriteReviewWidgetId) ==""){
              Swal.fire({
                      title: 'Error!',
                      text: "Please Fill Captcha Code First ",
                      icon: 'error',
                      confirmButtonText: 'Ok'
                  })
            return false;
           
          } 

        $.ajax({
          url:ajax_url,
          method:"POST",
          data:{
            review_type:review_type,
            hygine:hygine,
            atmosphere:atmosphere,
            service:service,
            location:location,
            type_id:type_id,
            cleanliness:cleanliness,
            review:review,
          },
          headers:{
            'X-CSRF-TOKEN': '{{ csrf_token() }}',
          },
          beforeSend:function(){
	                startLoader();
	            },
	            complete:function(){
	               stopLoader(); 
	            },
          success:function(data){
            if(data.status){
              Swal.fire({
                  title: 'Success!',
                  text: data.msg,
                  icon: 'success',
                  confirmButtonText: 'Ok'
              }).then((result) => {
                window.location.reload();
              });
            }else{
              Swal.fire({
                  title: 'Success!',
                  text: data.msg,
                  icon: 'success',
                  confirmButtonText: 'Ok'
              }).then((result) => {
                window.location.reload();
              });
            }
          }
        });
      });

$(document).on('click','.sendreview', function(){
        var ajax_url = WEBSITE_URL+"/agent/review";
        var review_type=$("#review_type").val();
        var type_id=$("#type_id").val();
        var rating=$("#rating").val();
         
        if(rating==''){
          $("#selectstar").show();
              return false;
        }

        var review=$("#review").val();
        
        if(review==''){
          $("#selectreview").show();
              return false;
        }
        var accept=$("#accept:checked").val();
        
        if(!accept){
          $("#selectaccept").show();
              return false;
        }

         if (grecaptcha.getResponse(wiriteReviewWidgetId) ==""){
              Swal.fire({
                      title: 'Error!',
                      text: "Please Fill Captcha Code First ",
                      icon: 'error',
                      confirmButtonText: 'Ok'
                  })
            return false;
           
          } 


        $.ajax({
          url:ajax_url,
          method:"POST",
          data:{
            review_type:review_type,
            type_id:type_id,
            rating:rating,
            review:review,
          },
          headers:{
            'X-CSRF-TOKEN': '{{ csrf_token() }}',
          },
          beforeSend:function(){
	                startLoader();
	            },
	            complete:function(){
	               stopLoader();
                 $('#viewrefund').modal('hide');
	            },
          success:function(data){
            if(data.status){
              Swal.fire({
                  title: 'Success!',
                  text: data.msg,
                  icon: 'success',
                  confirmButtonText: 'Ok'
              }).then((result) => {
                window.location.reload();
              });
            }else{
              Swal.fire({
                  title: 'Success!',
                  text: data.msg,
                  icon: 'success',
                  confirmButtonText: 'Ok'
              }).then((result) => {
                window.location.reload();
              });
            }
          }
        });
      });


      $(document).on('click','.bookuser', function(){
        var ajax_url = WEBSITE_URL+"/agent/book";
        var name=$("#name").val();
        var email=$("#email").val();
        var number=$("#number").val();
        var startDate=$("#startDate").val();
        var user_id=$("#user_id").val();
        var endDate=$("#endDate").val();
        $.ajax({
          url:ajax_url,
          method:"POST",
          data:{
            name:name,
            email:email,
            number:number,
            startDate:startDate,
            endDate:endDate,
            user_id:user_id
          },
          headers:{
            'X-CSRF-TOKEN': '{{ csrf_token() }}',
          },
          beforeSend:function(){
	                startLoader();
	            },
	            complete:function(){
	               stopLoader(); 
	            },
          success:function(data){
            if(data.status){
              Swal.fire({
                  title: 'Success!',
                  text: data.msg,
                  icon: 'success',
                  confirmButtonText: 'Ok'
              }).then((result) => {
                window.location.reload();
              });
            }else{
              Swal.fire({
                  title: 'Success!',
                  text: data.msg,
                  icon: 'success',
                  confirmButtonText: 'Ok'
              }).then((result) => {
                window.location.reload();
              });
            }
          }
        });
      });
      $(function () {
      var date = new Date();
      date.setDate(date.getDate());

  $('.datepicker').datetimepicker({
      format: "L",
      ignoreReadonly:true, minDate:date
  });
  $("#startDate").datetimepicker({format: "L", ignoreReadonly:true, defaultDate:date, minDate:date});
  $("#endDate").datetimepicker({format: "L", ignoreReadonly:true, defaultDate:date, minDate:date});
  $("#book_day_date").datetimepicker({format: "L", ignoreReadonly:true, defaultDate:date, minDate:date});
  $("#book_day_time").datetimepicker({format: "LT", ignoreReadonly:true, defaultDate:date, minDate:date});
  $("#schedule_tour_time").datetimepicker({format: "LT", ignoreReadonly:true, defaultDate:date, minDate:date});
  $("#schedule_tour_start_date").datetimepicker({format: "L", ignoreReadonly:true, defaultDate:date, minDate:date});
  $("#endDate1").datetimepicker({
    format: "LT",
    icons: {up: "fa fa-chevron-up",down: "fa fa-chevron-down"}
  });
  $(".timepicker").datetimepicker({
    format: "LT",
    icons: {
      up: "fa fa-chevron-up",
      down: "fa fa-chevron-down"
    }
  });
  $(".datetimepicker").datetimepicker({
       format: "L",
       minDate: 0,  // disable past date
       showTodayButton: true,
       icons: {
         next: "fa fa-chevron-right",
         previous: "fa fa-chevron-left",
         today: 'todayText',
       }
  });
});


$(document).ready(function() {
$('form[id="contact"]').validate({
  rules: {
    startDate: 'required',
    endDate: 'required',
    message: {
      required: true
    },
  },
  messages: {
    startDate: 'This field is required',
    endDate: 'This field is required',
    message: 'This field is required',
  },
  submitHandler: function(form) {
    if (grecaptcha.getResponse(makeInquiryWidgetId) == ""){
       
      Swal.fire({
                title: 'Error!',
                text: "Please Fill Captcha Code First !",
                icon: 'error',
                confirmButtonText: 'Ok'
            })
      return false;
    } 
    var ajax_url = WEBSITE_URL+"/property/buy/send_enquiry";
        var startDate=$("#startDate").val();
        var endDate=$("#endDate").val();
        var message=$("#message").val();
        var type=$("#type").val();
        var property=$("#property").val();
        var touser=$("#touser").val();
        $.ajax({
          url:ajax_url,
          method:"POST",
          data:{
            startDate:startDate,
            endDate:endDate,
            message:message,
            type:type,
            property:property,
            touser:touser
          },
          headers:{
            'X-CSRF-TOKEN': '{{ csrf_token() }}',
          },
          beforeSend:function(){
	                startLoader();
	            },
	            complete:function(){
	               stopLoader(); 
	            },
          success:function(data){
            if(data.status){
              Swal.fire({
                  title: 'Success!',
                  text: data.message,
                  icon: 'success',
                  confirmButtonText: 'Ok'
              }).then((result) => {
                window.location.reload();
              });
            }else{
              Swal.fire({
                  title: 'Success!',
                  text: data.message,
                  icon: 'success',
                  confirmButtonText: 'Ok'
              }).then((result) => {
                window.location.reload();
              });
            }
          }
        });
  }
});

$('form[id="book_property"]').validate({
  rules: {
    startDate: 'required',
    endDate: 'required',
    capacity: {
      required: true
    },
  },
  messages: {
    startDate: 'This field is required',
    endDate: 'This field is required',
    capacity: 'This field is required',
  },
  submitHandler: function(form) {
    var type=$("#type").val();
    var touser=$("#touser").val();
    var capacity=$("#capacity").val();
    var property=$("#property").val();
    var ajax_url = WEBSITE_URL+"/book_property";
    // var endDate = $("#schedule_tour_time").data('date');
    // var startDate = $("#schedule_tour_start_date").data('date');
    var endDate = $("#endDate").val();
    var startDate = $("#startDate").val();
    $.ajax({
      url:ajax_url,
      method:"POST",
      data:{
        type: type,
        touser: touser,
        endDate: endDate,
        property: property,
        capacity: capacity,
        startDate: startDate,
      },
      headers:{'X-CSRF-TOKEN': '{{ csrf_token() }}'},
      beforeSend:function(){
        startLoader();
      },
      complete:function(){
        stopLoader(); 
      },
      success:function(data){
        if(data.status == 'success'){
          Swal.fire({
              title: 'Success!',
              text: data.message,
              icon: 'success',
              confirmButtonText: 'Ok'
          }).then((result) => {
            window.location.reload();
          });
        }else{
          Swal.fire({
              title: 'Success!',
              text: data.message,
              icon: 'success',
              confirmButtonText: 'Ok'
          }).then((result) => {
            window.location.reload();
          });
        }
      }
    });
  }
});

$('form[id="book_property1"]').validate({
  rules: {
    startDate: 'required',
    endDate: 'required',
  },
  messages: {
    startDate: 'This field is required',
    endDate: 'This field is required',
  },
  submitHandler: function(form) {
    var type=$("#type").val();
    var userto=$("#userto").val();
    var property=$("#property_id").val();
    var ajax_url = WEBSITE_URL+"/book_property";
    var endDate = $("#schedule_tour_time").data('date');
    var startDate = $("#schedule_tour_start_date").data('date');
    $.ajax({
      url:ajax_url,
      method:"POST",
      data:{
        type:type,
        userto:userto,
        endDate:endDate,
        property:property,
        startDate:startDate,
      },
      headers:{
        'X-CSRF-TOKEN': '{{ csrf_token() }}',
      },
      beforeSend:function(){
        startLoader();
      },
      complete:function(){
        stopLoader(); 
      },
      success:function(data){
        if(data.status == 'success'){
          Swal.fire({
              icon: 'success',
              title: 'Success!',
              text: 'Thank You. Your tour has been scheduled with us.',
              confirmButtonText: 'Ok'
          }).then((result) => {
            window.location.reload();
          });
        }else{
          Swal.fire({
              icon: 'success',
              title: 'Success!',
              text: 'Thank You. Your tour has been scheduled with us.',
              confirmButtonText: 'Ok'
          }).then((result) => {
            window.location.reload();
          });
        }
      }
    });
  }
});
jQuery.validator.addMethod("validate_email", function(value, element) {
    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
        return true;
    } else {
        return false;
    }
  }, "Please enter a valid Email.");
 jQuery.validator.addMethod("validate_phone", function(value, element) {
    if (/^[1-9][0-9]*$/.test(value)) {
        return true;
    } else {
        return false;
    }
  }, "Please enter a phone number.");


$('form[id="enquiry"]').validate({
  rules: {
    name: 'required',
    phone: {
      required: true,
      validate_phone:true,
      minlength : 7,
      maxlength : 13,
    },
    message: 'required',
    email: {
      required: true,
      email:true,
      validate_email:true,
    },
  },
  messages: {
    name: 'This field is required',
    phone:{
      validate_phone : 'Please enter a valid phone number.',
      minlength : 'Please enter a valid phone number.',
      maxlength : 'Please enter a valid phone number.'
    },
    message: 'This field is required',
    email:{
      email : 'Must be valid email address',
      validate_email : 'Must be valid email address',
    },
  },
  submitHandler: function(form) {

    if (grecaptcha.getResponse(makeInquiryWidgetId) == ""){
       
      Swal.fire({
                title: 'Error!',
                text: "Please Fill Captcha Code First !",
                icon: 'error',
                confirmButtonText: 'Ok'
            })
      return false;
    } 

        var ajax_url = WEBSITE_URL+"/property/buy/send_enquiry";
        var name=$("#name").val();
        var phone=$("#phone").val();
        var code = $('#phone_code').val();
        var email=$("#email").val();
        var touser=$("#touser").val();
        var message=$("#message").val();
        var type=$("#type_get_rent").val();
        var property=$("#property_id").val();
        $.ajax({
          url:ajax_url,
          method:"POST",
          data:{
            name:name,
            phone:phone,
            message:message,
            type:type,
            property:property,
            email:email,
            touser:touser,
            code:code
          },
          headers:{
            'X-CSRF-TOKEN': '{{ csrf_token() }}',
          },
          beforeSend:function(){
	                startLoader();
	            },
	            complete:function(){
	               stopLoader(); 
	            },
          success:function(data){
            if(data.status){
              Swal.fire({
                  title: 'Success!',
                  text: 'Thanks for making an enquiry. We will get back to soon.',
                  icon: 'success',
                  confirmButtonText: 'Ok'
              }).then((result) => {
                window.location.reload();
              });
            }else{
              Swal.fire({
                  title: 'Success!',
                  text: 'Thanks for making an enquiry. We will get back to soon.',
                  icon: 'success',
                  confirmButtonText: 'Ok'
              }).then((result) => {
                window.location.reload();
              });
            }
          }
        });
  }
});

$('form[id="enquiry_rent"]').validate({
  rules: {
    name: 'required',
    phone: {
      required: true,
      validate_phone:true,
      minlength : 7,
      maxlength : 13,
    },
    message: 'required',
    email: {
      required: true,
      email:true,
      validate_email:true,
    },
  },
  messages: {
    name: 'This field is required',
    phone:{
      validate_phone : 'Please enter a valid phone number.',
      minlength : 'Please enter a valid phone number.',
      maxlength : 'Please enter a valid phone number.'
    },
    message: 'This field is required',
    email:{
      email : 'Must be valid email address',
      validate_email : 'Must be valid email address',
    },
  },
  submitHandler: function(form) {

    if (grecaptcha.getResponse(makeInquiryWidgetId) == ""){
       
      Swal.fire({
                 title: 'Error!',
                text: "Please Fill Captcha Code First !",
                icon: 'error',
                confirmButtonText: 'Ok'
            })
      return false;
    } 
    var ajax_url = WEBSITE_URL+"/property/buy/send_enquiry";
    var name=$("#name").val();
    var code = $('#phone_code').val();
    var phone=$("#phone").val();
    var message=$("#message").val();
    var email=$("#email").val();
    var type=$("#type_get_rent").val();
    var property=$("#property_id").val();
    var touser=$("#touser").val();
    $.ajax({
      url:ajax_url,
      method:"POST",
      data:{
        name:name,
        phone:phone,
        message:message,
        type:type,
        property:property,
        email:email,
        touser:touser,
        code:code
      },
      headers:{
        'X-CSRF-TOKEN': '{{ csrf_token() }}',
      },
      beforeSend:function(){
              startLoader();
          },
          complete:function(){
             stopLoader(); 
          },
      success:function(data){
        if(data.status){
          Swal.fire({
              title: 'Success!',
              text: 'Thanks for making an enquiry. We will get back to soon.',
              icon: 'success',
              confirmButtonText: 'Ok'
          }).then((result) => {
            window.location.reload();
          });
        }else{
          Swal.fire({
              title: 'Success!',
              text: 'Thanks for making an enquiry. We will get back to soon.',
              icon: 'success',
              confirmButtonText: 'Ok'
          }).then((result) => {
            window.location.reload();
          });
        }
      }
    });
  }
});
});

function selecttype(type){
    $("#type_get").val(type);
}
function selecttype1(type){
    $("#type_get_rent").val(type);
    $('.want-to-do button').removeClass('active');
    $("#"+type).addClass('active');
}
function makefavDetail(id){
	var ajax_url = WEBSITE_URL+"/favourate";
	$.ajax({
    url:ajax_url,
    method:"GET",
    data:{
      id:id
    },
    headers:{
      'X-CSRF-TOKEN': '{{ csrf_token() }}',
    },
    beforeSend:function(){
            startLoader();
        },
        complete:function(){
           stopLoader(); 
        },
    success:function(data){
      if(data.success==1){
        Swal.fire({
            title: 'Success!',
            text: data.message,
            icon: 'success',
            confirmButtonText: 'Ok'
        }).then((result) => {
          window.location.reload();
        });
      }else{
        Swal.fire({
            title: 'Success!',
            text: data.message,
            icon: 'success',
            confirmButtonText: 'Ok'
        }).then((result) => {
          window.location.reload();
        });
      }
    }
  });
}
	</script>
@endsection
