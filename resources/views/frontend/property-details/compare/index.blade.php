<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Skip Page</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Bootstrap CSS File -->
    <link href="lib/bootstrap.min.css" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Main Stylesheet File -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('fonts/stylesheet.css')}}" rel="stylesheet">

    <link href="{{asset('css/resonsive.css')}}" rel="stylesheet">
    <style>
/*NEw Css*/
.compare-listing {
  text-align: left;
}
.outer-wrap-compare {
  border: 1px solid #CBCBCB;
  width: 100%;
  overflow-x: hidden;
}
.outer-wrap-compare .dark-bg{
  background: #EAEAEA;
}
.outer-wrap-compare p{
  padding: 15px;
}
.outer-wrap-compare .bb-block {
  border-bottom: 1px solid #CBCBCB;
}
.outer-wrap-compare {
  background: #FCFCFC;
}
#register-popup .outer-wrap-compare-outer h3{
   color: #000000;
  font-size: 32px;
  font-weight: bold;
  letter-spacing: -0.2px;
}
    </style>
</head>

<body>



<div id="register-popup" class="modal1 skipp-page-cls">
<div class="modal-dialog">
<!-- Modal content-->
<div class="signup-right">
<span class="close-icon"></span>
<a href="" class="skipp-section">Skip <img src="img/Arrow.png"></a>
<div class="modal-body outer-wrap-compare-outer">
    <div class="row">
        <div class="col-md-4">
            <div class="modal-left_side skipp-left-bar">
                <img src="img/address.png">
                <h2>Compare Your Swap Listing</h2>
                <p>This is the property Comparison between your & Bill’s Property</p>
            </div>
        </div>
        <div class="col-md-4 text-center">
            <div class="compare-listing">
                <img src="img/zz.png">
                <h3 class="mt-4">Hans’s Beautiful House</h3>
                <p>3 Bedroom Apartment</p>
            </div>
        </div>
        <div class="col-md-4 text-center">
            <div class="compare-listing">
                <img src="img/zz.png">
                <h3 class="mt-4">Hans’s Beautiful House</h3>
                <p>3 Bedroom Apartment</p>
            </div>
        </div>
        <div class="col-md-12 mt-5">
            <div class="outer-wrap-compare">
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg bb-block"><strong>This Place Can Accomodate</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0"><span>4.5 </span> Stars out of 5<img class="ml-2" src="img/"></p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">This Place Can Accomodate</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg bg-white"><strong>This Place Can Accomodate</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0"><span>4.5 </span> Stars out of 5<img class="ml-2" src="img/"></p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">This Place Can Accomodate</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg bb-block"><strong>This Place Can Accomodate</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0"><span>4.5 </span> Stars out of 5<img class="ml-2" src="img/"></p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">This Place Can Accomodate</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg bb-block"><strong>This Place Can Accomodate</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0"><span>4.5 </span> Stars out of 5<img class="ml-2" src="img/"></p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">This Place Can Accomodate</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg bb-block"><strong>This Place Can Accomodate</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0"><span>4.5 </span> Stars out of 5<img class="ml-2" src="img/"></p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">This Place Can Accomodate</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg bg-white"><strong>This Place Can Accomodate</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0"><span>4.5 </span> Stars out of 5<img class="ml-2" src="img/"></p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">This Place Can Accomodate</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg bb-block"><strong>This Place Can Accomodate</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0"><span>4.5 </span> Stars out of 5<img class="ml-2" src="img/"></p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">This Place Can Accomodate</p>
                    </div>
                </div>
            </div>
            <h3 class="mt-5 mb-5">Compare the House<br> Amenities</h3>
            <div class="outer-wrap-compare">
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg bb-block"><strong>This Place Can Accomodate</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0"><span>4.5 </span> Stars out of 5<img class="ml-2" src="img/"></p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">This Place Can Accomodate</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg bg-white"><strong>This Place Can Accomodate</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0"><span>4.5 </span> Stars out of 5<img class="ml-2" src="img/"></p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">This Place Can Accomodate</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg bb-block"><strong>This Place Can Accomodate</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0"><span>4.5 </span> Stars out of 5<img class="ml-2" src="img/"></p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">This Place Can Accomodate</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg bb-block"><strong>This Place Can Accomodate</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0"><span>4.5 </span> Stars out of 5<img class="ml-2" src="img/"></p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">This Place Can Accomodate</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg bb-block"><strong>This Place Can Accomodate</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0"><span>4.5 </span> Stars out of 5<img class="ml-2" src="img/"></p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">This Place Can Accomodate</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg bg-white"><strong>This Place Can Accomodate</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0"><span>4.5 </span> Stars out of 5<img class="ml-2" src="img/"></p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">This Place Can Accomodate</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg bb-block"><strong>This Place Can Accomodate</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0"><span>4.5 </span> Stars out of 5<img class="ml-2" src="img/"></p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">This Place Can Accomodate</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>
</div>



    <script src='https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js'></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>

    <script src="js/script.js"></script>
</body>

</html>