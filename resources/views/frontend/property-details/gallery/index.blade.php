@extends('frontend.layouts.home')
@section('title','Property Gallery')

@section('css')
	<link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
@endsection
@section('content')
    
    @include('frontend.layouts.default-header')
    
    <section class="top-searh-bar">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-12 first-two-box">
	                <ul class="list-inline mb-0">
	                    
	                </ul>
	            </div>
	        </div>
	    </div>
	</section>
	<section>
	    <div class="container">
	        <div class="row">
	            <div class="col-md-12 first-two-box">
	                <div 
						class="fotorama" 
						data-minwidth="100%"
						data-maxwidth="100%"
						data-height="400"
						data-maxheight="400" 
						data-allowfullscreen="native" 
						data-fit="cover" 
						data-loop="true"
						data-nav="thumbs"
						>
						@foreach($data->files as $row)
							<img src="{{url('/uploads/properties/'.$row)}}" width="144" height="96" style="width: 72px; height: 48px; left: 0px; top: 0px;">
						@endforeach
						
				  	</div>
	            </div>
	        </div>
	    </div>
	</section>
    @include('frontend.layouts.footer')

@endsection

@section('scripts')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
@endsection