@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
@extends('frontend.layouts.home')
@section('title','Property Details')
@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.carousel.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.theme.default.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.theme.green.min.css" />
	<link rel="stylesheet" type="text/css" href="{{asset('css/flickity.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('Kalendae-build/kalendae.css')}}">
	<style type="text/css">
		.kalendae .k-separator{
			height: 0px;
		}
		.kalendae .k-calendar{
			width: 185px;
			margin-bottom: 10px;
			padding-left: 4px 10px;
		}
		.kalendae .k-days span.k-range.k-in-month, .kalendae .k-days span.k-selected.k-active, .kalendae .k-header.k-active span.k-selected{
			background: #E4002B;
			border-color: #E4002B;
			color: #fff;
		}
		.kalendae{
			background: #fff;
		}

		.bodyFilter {
		  position: unset !important;
		}
		.datepicker-dropdown{
			top: 282px !important;
		}
		#shareButtons a i.fa.fa-facebook {
		    font-size: 32px;
		}


	</style>
@endsection
@section('content')
    

    
	@include('frontend.layouts.default-header')
	<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
			<script type="text/javascript">
 				var wiriteReviewWidgetId  ;
				var makeInquiryWidgetId ;
				var makeInspectionInquiryWidgetId  ;
				var makeReportFlagWidgetId  ;
				
				var CaptchaCallback = function() {
					wiriteReviewWidgetId = grecaptcha.render('g-wiriteReviewWidgetId', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
					makeInquiryWidgetId = grecaptcha.render('g-makeInquiryWidgetId', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
					makeInspectionInquiryWidgetId = grecaptcha.render('g-makeInspectionInquiryWidgetId', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
					makeReportFlagWidgetId = grecaptcha.render('g-makeReportFlagWidgetId', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
				};

			</script>
	
    
	@include('frontend.property-details.common-data.filter')

	

    <section class="body-light-grey pt-3" style="margin-top:70px;">
	    <div class="container">
	        <div class="row">
	            @include('frontend.property-details.swap.left-part')
				@if(\Auth::user())
            @if(\Auth::user()->id!=$data->user_id)
            <!-- right section -->
            @include('frontend.property-details.swap.right-part')
            @endif
            @else
            @include('frontend.property-details.swap.right-part')
            @endif
	        </div>
	    </div>
	</section>
	<div class="container">
            <div class="row">
                <div class="col-md-12 mt-5">
                
					<h2 style="font-weight: 800;">You May Also Like</h2>
					<span>Similar homes near your location</span>
                    <p></p>
                </div>
                <div class="container">

                <div id="mydivon" class="fadeIn">
                <div class="row dynamicContent">
                    @include('frontend.property-details.swap.related')
                </div>
            </div>
                
                <div class="col-md-12 show-more text-center">
                    <a href="{{url(app()->getLocale().'/swap/'.$link.'?country='.$link.'&location=')}}"><button class="red-bordered mt-3 mb-5">SHOW MORE <i class="fa fa-angle-right"></i></button></a>
                </div>

            </div>
            </div>
			</div>

    @include('frontend.layouts.footer')

@endsection
@section('scripts')
<script src='https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js'></script>
<script src="{{asset('js/script.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/owl.carousel.min.js"></script>
	<script type="text/javascript" src="{{asset('js/flickity.pkgd.min.js')}}"></script>
	<script type="text/javascript">
		$('.main-carousel').flickity({
			// options
			cellAlign: 'left',
			contain: true
		});
        </script>
@endsection