@php
$loc=Request::segment(3);
@endphp
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.carousel.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.theme.default.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.theme.green.min.css" />
	
    <link rel="stylesheet" type="text/css" href="{{asset('css/flickity.min.css')}}">
    
<style type="text/css">
  
#shareButtons a {
  font-size: 18px;
  margin-right: 5px;
}
#shareButtons a i.fa.fa-facebook {
 font-size: 16px;  
}

#shareButtons {
    padding: 5px;
    background: white;
    width: 100px;
    border-radius: 24px;
    position: absolute;
    margin-top: 360px;
    margin-left: 72px;
    z-index: 99999;
}
</style>

<div class="row property-images-block">
          <div class="col-md-12">
          <input type="hidden" name="user_id" value="{{@$id}}">

          <p id="shareButtons" style="text-align: center; display: none;"><a href="https://api.whatsapp.com/send?text=Property Share {{env('APP_URL')}}/{{Request::segment(1)}}/{{Request::segment(2)}}/{{Request::segment(3)}}/{{Request::segment(4)}}/{{Request::segment(5)}}" target="_blank" style="color:green;"><i class="fa fa-whatsapp" aria-hidden="true"></i> </a> <a  style="color:blue;" href="https://www.facebook.com/sharer/sharer.php?u={{env('APP_URL')}}/{{Request::segment(1)}}/{{Request::segment(2)}}/{{Request::segment(3)}}/{{Request::segment(4)}}/{{Request::segment(5)}}" target="_blank" ><i class="fa fa-facebook" aria-hidden="true"></i> </a>
          </p> 


          <a onclick="$('#shareButtons').toggle();"><span style="background-color: white;
    position: absolute;
    margin-top: 410px;
    margin-left: 72px;
    padding: 10px;z-index: 999;color: #131111;border-radius: 5px;"><img style="width: 15%;height: auto;" src="https://img.icons8.com/windows/32/000000/share-rounded.png"/> Share Property</span> </a>

    @if(isset(Auth::user()->id))
                @php
                    $fav = \App\Models\Favourate::where('propert_id',$data->_id)->where('user_id',Auth::user()->id)->first();
                    @endphp
                    
                    @if(@$fav)
                    <span style="background-color: white;
    position: absolute;
    margin-top: 410px;
    margin-left: 253px;
    padding: 10px;z-index: 999;width: 86px;border-radius: 5px;"><i class="fa fa-heart fav-icon" style="right: 14px;top: 10px;font-size: 19px;color: red;" onClick="makefavDetail('{{$data->_id}}')"></i> Save</span>
                    
                    @else
                    <span style="background-color: white;
    position: absolute;
    margin-top: 410px;
    margin-left: 253px;
    padding: 10px;z-index: 999;width: 86px;border-radius: 5px;"><i class="fa fa-heart-o fav-icon" style="right: 14px;top: 13px;font-size: 19px;color: red;" onClick="makefavDetail('{{$data->_id}}')"></i> Save</span>
                    
                    @endif
                    @endif
              <div class="carousel carousel-main" data-flickity='{"pageDots": false }'>
                  @if(@$data->files)
                  @foreach($data->files as $img)
                  @if(!isset($img) || $img=='')
                  <div class="carousel-cell"><img src="{{asset('img/house2.png')}}" style="height:504px"/></div>
                    @else
                    @if(file_exists('uploads/swap/'.$img))
                    <div class="carousel-cell"><img src="{{url('uploads/swap/'.$img)}}" style="height:504px"/></div>
                    @else
                    <div class="carousel-cell"><img src="{{asset('img/house2.png')}}" style="height:504px"/></div>
                    @endif
                    @endif
                    @endforeach
                    @else
                    <div class="carousel-cell"><img src="{{asset('img/house2.png')}}" style="height:504px"/></div>
                    @endif
                    </div>


            <div class="carousel carousel-nav"
              data-flickity='{ "asNavFor": ".carousel-main", "contain": true, "pageDots": false }'>
              @if(@$data->files)
              @foreach($data->files as $img)
                  @if(!isset($img) || $img=='')
                  <div class="carousel-cell"><img src="{{asset('img/house2.png')}}" style="width: 120px;height:90px;"/></div>
                    @else
                    @if(file_exists('uploads/swap/'.$img))
                    <div class="carousel-cell"><img src="{{url('uploads/swap/'.$img)}}" style="width: 120px;height:90px;"/></div>
                    @else
                    <div class="carousel-cell"><img src="{{asset('img/house2.png')}}" style="width: 120px;height:90px;"/></div>
                    @endif
                    @endif
                    @endforeach
                    @else
                    <div class="carousel-cell"><img src="{{asset('img/house2.png')}}" style="width: 120px;height:90px;"/></div>
                    @endif
            </div>
            
            <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/owl.carousel.min.js"></script>
	<script type="text/javascript" src="{{asset('js/flickity.pkgd.min.js')}}"></script>

       