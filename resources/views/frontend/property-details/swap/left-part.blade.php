<div class="col-md-8">
     @include('frontend.property-details.swap.banner')
    <div class="bg-white mb-3 p-3 rounded">
        <div class="row hd-and-details">
		    <div class="col-md-9">
		        <h3>{{ucfirst($data->title)}}</h3>
		        <p>{{$data->no_of_bedrooms ?? 0}} Bedrooms {{ucfirst($data->house_type)}}</p>
		       	<span><i class="fa fa-map-marker mr-2"></i>{{$data->location_name}}, {{$data->country}}</span>
		    </div>
		    <div class="col-md-3 text-center">
		        <div class="pl-4">
		            <img src="{{asset('img/regular-swap.png')}}">
		        </div>
		    </div>
		    <div class="col-md-12">
		        <hr>
		    </div>
		</div>
        <div class="row des-and-details">
            <div class="col-md-12">
                <h4 class="mt-2">Description</h4>
                <p class="mb-0">
		            {{\Str::limit($data->property_description, 250, '')}}
		            <?php
                $string = strip_tags($data->property_description);
                if (strlen($string) > 250) {
                    
                    $stringCut = substr($string, 250);
                    $stringp = '<span id="dots">...</span><span id="text_content" class="content" style="display:none;">'.$stringCut.'</span></p><a href="javascript:show_hide();" class="show_hide" id="show_hide" data-content="toggle-text">Read More</a>';
                    echo $stringp;
                }else{
                        //echo "</p>";
                    }
            ?>
            </div>
        </div>
         @php $locations = explode(",",$data->owner_locations); @endphp
         @if(count($locations) > 0)
         <div class="row des-and-details">
            <div class="col-md-12">
                <h4 class="mt-2">Interested Location for Swap</h4>
                <div class="row">
                    @foreach($locations as $location)
                    <div class="col-md-4">
                        <p><strong class="mr-2">·</strong>{{strtoupper($location)}}</p>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        @endif

       	<div class="row amenities pt-4">
            <div class="col-md-12">
                <h4 class="mt-2">Accomodation</h4>
                <div>
                    <div class="col-md-6">
                        <p><span style="font-size:40px"> {{$data->total_accommodation ?? 0}} </span> <span>People</span></p>
                        <p style="color: rgba(14,14,14,0.55);font-size: 16px;letter-spacing: 0px;">This place has a accomodation for {{$data->total_accommodation ?? 0}} Adults</p>
                    </div>
                </div>
		        
                <div class="row">
                    <div class="col-md-4">
                        <div class="amint-block amint-block-no-br">
                            <img src="{{asset('img/bed-black.png')}}" style="float: left;">
                            <p style="text-align: center;" class="mt-2">{{$data->no_of_bedrooms ?? 0}} Bedrooms</p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="amint-block amint-block-no-br">
                            <img src="{{asset('img/bathtub-black.png')}}" style="float: left;">
                            <p  style="text-align: center;" class="mt-2">{{$data->no_of_bathrooms ?? 0}} Bathrooms</p>
                        </div>
                    </div>

                    
                    <div class="col-md-4">
                        <div class="amint-block amint-block-no-br">
                            <img src="{{asset('img/single-bed.png')}}" style="float: left;">
                            <p  style="text-align: center;" class="mt-2">{{$data->no_of_single_beds ?? 0}} Single Beds</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                       <div class="amint-block amint-block-no-br">
                            <img src="{{asset('img/double-bed.png')}}" style="float: left;">
                            <p  style="text-align: center;" class="mt-2">{{$data->no_of_double_beds ?? 0}} Double Beds</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="amint-block amint-block-no-br">
                            <img src="{{asset('img/children-bed.png')}}" style="float: left;">
                            <p  style="text-align: center;" class="mt-2">{{$data->no_of_children_beds ?? 0}} Children Beds</p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="amint-block amint-block-no-br">
                            <img src="{{asset('img/single-bed.png')}}" style="float: left;">
                            <p  style="text-align: center;" class="mt-2">{{$data->no_of_extra_beds ?? 0}} Extra Beds</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row amenities pt-4">
            <div class="col-md-12">
                <h4 class="mt-2">Amenities</h4>
                <div class="row ">
                    @php
                        $amenities = $data->amenties;
                        if(!is_array($amenities)){
                            $amenities = [];
                        }

                    @endphp
                    @foreach($data->amenties as $row)
                    
                            <div class="col-md-4 amenities_block">
                                <div class="amint-block amint-block-no-br">
                                    <img src="{{url('uploads/amenities/'.$row['logo'])}}" style="height: 35px;width: 35px;">
                                    <p class="mt-2">{{ucfirst($row['name'])}}</p>
                                </div>
                            </div>
                        
                    @endforeach
                </div>
            </div>
        </div>

        <div class="row des-and-details pt-4">
            <div class="col-md-12">
                <h4 class="mt-2">The Neighbourhood</h4>
                <p class="mb-0">
                	{{\Str::limit($data->neighbourhood_description, 1000, '')}}
		            <?php
		                // $string = strip_tags($data->neighbourhood_description);
		                // if (strlen($string) > 250) {
		                //     $stringCut = substr($string, 250);
		                //     $stringp = '<span class="content" style="display:none;">'.$stringCut.'</span></p><a href="javascript:;" class="show_hide" data-content="toggle-text">Read More</a>';
		                //     echo $stringp;
		                // }else{
		                // 	echo "</p>";
		                // }
		            ?>		        
            </div>
        </div>

       <div class="row amenities pt-4">
            <div class="col-md-12">
                <h4 class="mt-2">Availability</h4>

                @foreach($data->availability as $date)
                    <button class="red-btn" style="border-radius: 25px;margin-top: 10px;background: #bbbbbb;
    border: 1px solid #bbbbbb;" autocomplete="off"><span style="color:white">{{date('d M Y',strtotime($date))}}</span></button>
                @endforeach

                
            </div>
        </div>

        @include('frontend.property-details.common-data.map')
        @include('frontend.property-details.swap.rating')
        <br>
        @include('frontend.property-details.common-data.map-nearby')
    </div>
</div>
</div>
</div>
<script type="text/javascript">
    function show_hide()
            {
                 var display = $("#text_content").css("display");
                 if(display=='none')
                 {
                     $("#text_content").css("display","unset");
                    $("span#dots").text("");
                     $("a#show_hide").text("Read Less");
                    
                 }else
                 {
                       $("#text_content").css("display","none");
                      
                      $("span#dots").text("...");
                      $("a#show_hide").text("Read More");
                     

                 }
                  
            }
</script>