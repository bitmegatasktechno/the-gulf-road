<style type="text/css">
        .error{
            color: red !important;
        }
    </style>
 
<div class="col-md-4 pl-0">
    <div class="bg-white rounded p-3 mb-3 instant-booking">
        
        <div class="row hd-and-details">
            <div class="col-md-2">
                
            @if(!isset($data->user->image) || $data->user->image=='')
                                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 50px; width: 50px">
                                    @else
                                    @if(file_exists('uploads/profilePics/'.$data->user->image))
                                    <img class="circle" src="{{url('uploads/profilePics/'.$data->user->image)}}" style="height: 50px; width: 50px">
                                    @else
                                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 50px; width: 50px">
                                    @endif
                                    @endif
               
            </div>
            <div class="col-md-10">

                <h3 class="mb-1">{{ucfirst($data->user->name ?? 'Unknown')}}</h3>
                <p>100% Response Rate</p>
              
            </div>
             <small class="info-text-contact " autocomplete="off" data-toggle="modal" data-target="#contact_information" style="cursor: pointer;"><img class="ml-2" src="{{asset('img/contact.png')}}"></small>

        </div>
     <!--    <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <small class="info-text-contact float-right" autocomplete="off" data-toggle="modal" data-target="#contact_information" style="cursor: pointer;">Contact Information<img class="ml-2" src="{{asset('img/info.png')}}"></small>
            </div>
        
        </div> -->
        <hr>
        <form id="contact" method="post" name="contact">
        <input type="hidden" value="swap" id="type" name="type">
        <input type="hidden" value="{{$data->_id}}" id="property" name="property">
        <input type="hidden" value="{{@$data->user_id}}" id="touser" name="touser">
        <div class="input-daterange">
            <div class="row mt-3">
                <div class="col-md-6 pr-0">
                    <label>Check in</label>
                    <input id="startDate" name="startDate"  placeholder="dd-mm-yyyy" class="form-control datepicker" required readonly />
                </div>
                <div class="col-md-6 pl-0">
                    <label>Check-out</label>
                    <input id="endDate" name="endDate" placeholder="dd-mm-yyyy" class="form-control datepicker" required readonly />
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-12">
                    <label>Message</label>
                    <textarea id="message" class="form-control" name="message" placeholder="Enter Message here.." style="height: 100px;" autocomplete="off" required></textarea>
                </div>
            </div>

            <div class="row mt-3">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="g-recaptcha"  id="g-makeInquiryWidgetId"></div>
                </div>
              </div>
         </div>
            <div class="row mt-3">
                <div class="col-md-12">
                @if(Auth::user())
                    <button type="submit" class="red-btn full-width rounded mt-3" autocomplete="off">Contact Owner</button>
                    @else
                    <a href="javascript:;" onclick="showLoginForm();"><button type="button" class="red-btn full-width rounded mt-3" autocomplete="off">Contact Owner</button></a>
                    @endif
                </div>
            </div>
        </div>
</form>
    </div>
    <div class="bg-white rounded p-3 want-to-do">
        <div class="mt-2">
            <img src="{{asset('img/compare.png')}}">
            <p><strong>Compare Your House</strong></p>
            <p>You Can Compare this Listing with your houses listed with us.</p>
           
           <a href="{{url(app()->getLocale().'/listproperty/'.$link.'/swap/compare/'.$data->_id)}}"> <button class="red-bordered inspection-btn full-width mb-3 mt-3">Compare Property</button></a>
          
        </div>
    </div>
    @if(Auth::user())
    <a onclick="showReportForm('{{$data->_id}}')" style="cursor: pointer;"><p class="report-listing text-center"><img class="mr-2" src="{{asset('img/flag_report.png')}}">Report this listing</p></a>
    @endif
</div>
<!-- Modal -->
<div class="modal fade how_it_modal" id="contact_information" tabindex="-1" role="dialog" aria-labelledby="contact_informationLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-0 pb-0">
        <h5 class="modal-title" id="contact_informationLabel">Owner's Contact Information</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p ><b>Name :</b> {{ucfirst($data->user->name)}}</p>
        <p ><b>Email Address :</b> {{$data->user->email}}</p>
        <p ><b>Phone Number :</b> {{$data->user->countryCode}} {{$data->user->phone}}</p>

      </div>
    </div>
  </div></div>