@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Compare Property</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <link rel="shortcut icon" href="{{ asset('img/ic_favi.png') }}">
    <!-- Bootstrap CSS File -->
    <link href="{{asset('lib/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Main Stylesheet File -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('fonts/stylesheet.css')}}" rel="stylesheet">
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
    <style type="text/css">
        .close-icon:after {
            font-family: FontAwesome;
            content: "\00d7";
            font-size: 70px;
            position: absolute;
        }
    </style>

</head>

<body>



<div id="register-popup" class="skipp-page-cls">
<div class="container">
<!-- Modal content-->
<div class="row">
    <div class="col-md-12">
        <div class="p-4">
        <div class="pb-4 pl-4 pr-4">
        <a href="{{url(app()->getLocale().'/property/'.$link.'/swap/'.$data->_id)}}"><span class="close-icon" id="compareCloseIcon"></span></a>
        <!--<a href="{{url(app()->getLocale().'/property/'.$link.'/swap/'.$data->_id)}}" class="skipp-section">Skip <img src="{{asset('img/Arrow.png')}}"></a> -->
        </div>
    </div>
</div>
</div>
<div class="row" style="margin-top: 75px;">
       <div class="col-md-4">
            <div class="modal-left_side skipp-left-bar">
                <img src="{{asset('img/compare.png')}}">
                <h2>Compare Your Swap Listing</h2>
                <p>This is the property Comparison between your & Bill’s Property</p>
            </div>
        </div>
        <div class="col-md-4 text-center">
            <div class="compare-listing">
            @if(!isset($data->files[0]) || $data->files[0]=='')
                                    <img src="{{asset('img/house2.png')}}" style="width: 320px;height: 271px;">
                                    @else
                                    @if(file_exists('uploads/swap/'.$data->files[0]))
                                    <img src="{{url('uploads/swap/'.$data->files[0])}}" style="width: 320px;height: 271px;">
                                    @else
                                    <img src="{{asset('img/house2.png')}}" style="width: 320px;height: 271px;">
                                    @endif
                                    @endif
            <!-- <img src="{{url('uploads/swap/'.@$data->files[0])}}" style="width: 320px;height: 271px;"> -->
                <h3 class="mt-4">{{@$data->title}}</h3>
                <p>{{@$data->no_of_bedrooms}} Bedroom Apartment</p>
            </div>
        </div>
       
        <div class="col-md-4 text-center">
        
            @if(@$compare)
            <div class="compare-listing">
            @if(!isset($compare->files[0]) || $compare->files[0]=='')
                                    <img src="{{asset('img/house2.png')}}" style="width: 320px;height: 271px;">
                                    @else
                                    @if(file_exists('uploads/swap/'.$compare->files[0]))
                                    <img src="{{url('uploads/swap/'.$compare->files[0])}}" style="width: 320px;height: 271px;">
                                    @else
                                    <img src="{{asset('img/house2.png')}}" style="width: 320px;height: 271px;">
                                    @endif
                                    @endif
            <!-- <img src="{{url('uploads/swap/'.@$compare->files[0])}}" style="width: 320px;height: 271px;"> -->
                <h3 class="mt-4">{{@$compare->title}}</h3><span style="color:red" onClick="showCompareForm()">Change</span>
                <p>{{@$compare->no_of_bedrooms}} Bedroom Apartment</p>   
            </div>
            @else
            <span style="color:red;cursor: pointer;" onClick="showCompareForm()">Add Listing to Compare</span>
            @endif
        </div>
       
        <div class="col-md-12 mt-5">
            <div class="outer-wrap-compare">
            <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg"><strong>House Rating</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0">{{@$avgtotal}} <span style="color: #7b7b7b;font-size: 15px;">Stars  out of 5</span> @if(@$avgtotal > @$avgtotal1) <img src="{{asset('img/icon-up-filled.png')}}" style="height: auto;width: 15px;"> @endif</p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">{{@$avgtotal1}} <span style="color: #7b7b7b;font-size: 15px;">Stars  out of 5</span> @if(@$avgtotal1 > @$avgtotal) <img src="{{asset('img/icon-up-filled.png')}}" style="height: auto;width: 15px;"> @endif</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg bg-white"><strong>This Place Can Accomodate</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0">{{@$data->total_accommodation}}  @if(@$data->total_accommodation > @$compare->total_accommodation) <img src="{{asset('img/icon-up-filled.png')}}" style="height: auto;width: 15px;"> @endif</p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">{{@$compare->total_accommodation}} @if(@$compare->total_accommodation > @$data->total_accommodation) <img src="{{asset('img/icon-up-filled.png')}}" style="height: auto;width: 15px;"> @endif</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg bb-block"><strong>Bedrooms</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0">{{@$data->no_of_bedrooms}}  @if(@$data->no_of_bedrooms > @$compare->no_of_bedrooms) <img src="{{asset('img/icon-up-filled.png')}}" style="height: auto;width: 15px;"> @endif</p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">{{@$compare->no_of_bedrooms}}  @if(@$compare->no_of_bedrooms > @$data->no_of_bedrooms) <img src="{{asset('img/icon-up-filled.png')}}" style="height: auto;width: 15px;"> @endif</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 bb-white"><strong>Single Beds</strong></p>
                    </div>
                   <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0">{{@$data->no_of_single_beds}} @if(@$data->no_of_single_beds > @$compare->no_of_single_beds) <img src="{{asset('img/icon-up-filled.png')}}" style="height: auto;width: 15px;"> @endif</p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">{{@$compare->no_of_single_beds}} @if(@$compare->no_of_single_beds > @$data->no_of_single_beds) <img src="{{asset('img/icon-up-filled.png')}}" style="height: auto;width: 15px;"> @endif</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg bb-block"><strong>Double Beds</strong></p>
                    </div>
                  <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0">{{@$data->no_of_double_beds}} @if(@$data->no_of_double_beds > @$compare->no_of_double_beds) <img src="{{asset('img/icon-up-filled.png')}}" style="height: auto;width: 15px;"> @endif</p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">{{@$compare->no_of_double_beds}} @if(@$compare->no_of_double_beds > @$data->no_of_double_beds) <img src="{{asset('img/icon-up-filled.png')}}" style="height: auto;width: 15px;"> @endif</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 bg-white"><strong>Surface Area</strong></p>
                    </div>
                     <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0">{{@$data->surface_area}} sqft @if(@$data->surface_area > @$compare->surface_area) <img src="{{asset('img/icon-up-filled.png')}}" style="height: auto;width: 15px;"> @endif</p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">{{@$compare->surface_area}} sqft @if(@$compare->surface_area > @$data->surface_area) <img src="{{asset('img/icon-up-filled.png')}}" style="height: auto;width: 15px;"> @endif</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg bb-block"><strong>Bathroom</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0">{{@$data->no_of_bathrooms}} @if(@$data->no_of_bathrooms > @$data->no_of_bathrooms) <img src="{{asset('img/icon-up-filled.png')}}" style="height: auto;width: 15px;"> @endif</p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">{{@$compare->no_of_bathrooms}} @if(@$compare->no_of_bathrooms > @$data->no_of_bathrooms) <img src="{{asset('img/icon-up-filled.png')}}" style="height: auto;width: 15px;"> @endif</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 bg-white"><strong>House Type</strong></p>
                    </div>
                     <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0"><span>{{@$data->house_type}}</p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">{{@$compare->house_type}}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg"><strong>Accessibility</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0">@if(@$data->location_accessible=='urban_area') {{"Urban Area"}} @endif </p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">@if(@$compare->location_accessible=='urban_area') {{"Urban Area"}} @endif</p>
                    </div>
                </div>
            </div>
            <h3 class="mt-5 mb-5">Compare The House<br> Amenities</h3>
            <div class="outer-wrap-compare mb-5">
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg"><strong>Air Conditioning</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0">
                            <?php 
                            foreach($data->amenties as $amenities){
                                 if(@$amenities['name']=='air conditioner'){
                                      $cond="Yes"; 
                                      break;
                                      }
                                       else {
                                        $cond="No"; 
                                        }
                                            }?>
                                            {{$cond}}
                                            </p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">
                        @if(@$compare)
                            <?php 
                            foreach($compare->amenties as $amenities){
                                 if(@$amenities['name']=='air conditioner'){
                                    $cond="Yes";
                                      break;
                                      }
                                       else {
                                        $cond="No";
                                        }
                                            }?>{{$cond}}@endif</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 bg-white"><strong>Wi-Fi</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0">
                            <?php 
                            foreach($data->amenties as $amenities){
                                 if($amenities['name']=='wifi'){
                                    $cond="Yes"; 
                                    break;
                                    }
                                     else {
                                      $cond="No"; 
                                      }
                                            }?>{{$cond}}
                                            </p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">@if(@$compare)<?php 
                                foreach($compare->amenties as $amenities){
                                     if(@$amenities['name']=='wifi'){
                                    $cond="Yes"; 
                                    break;
                                    }
                                     else {
                                      $cond="No"; 
                                      }
                                            }?>{{$cond}}@endif</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg bb-block"><strong>Heating</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0">
                            <?php 
                            foreach($data->amenties as $amenities){
                                 if($amenities['name']=='heating'){
                                    $cond="Yes"; 
                                    break;
                                    }
                                     else {
                                      $cond="No"; 
                                      }
                                            }?>{{$cond}}
                                            </p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">@if(@$compare)<?php 
                                foreach($compare->amenties as $amenities){
                                     if(@$amenities['name']=='heating'){
                                    $cond="Yes"; 
                                    break;
                                    }
                                     else {
                                      $cond="No"; 
                                      }
                                            }?>{{$cond}}@endif</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 bb-white"><strong>Parking</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0">
                            <?php 
                            foreach($data->amenties as $amenities){
                                 if($amenities['name']=='parking'){
                                    $cond="Yes"; 
                                    break;
                                    }
                                     else {
                                      $cond="No"; 
                                      }
                                            }?>{{$cond}}
                                            </p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">@if(@$compare)<?php 
                            foreach($compare->amenties as $amenities){
                                 if(@$amenities['name']=='parking'){
                                    $cond="Yes"; 
                                    break;
                                    }
                                     else {
                                      $cond="No"; 
                                      }
                                            }?>{{$cond}}@endif</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg bb-block"><strong>Washing Machine</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0">
                            <?php 
                            foreach($data->amenties as $amenities){
                                 if($amenities['name']=='washing machine'){
                                    $cond="Yes"; 
                                    break;
                                    }
                                     else {
                                      $cond="No"; 
                                      }
                                            }?>{{$cond}}
                                            </p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">@if(@$compare)<?php 
                            foreach($compare->amenties as $amenities){
                                 if(@$amenities['name']=='washing machine'){
                                    $cond="Yes"; 
                                    break;
                                    }
                                     else {
                                      $cond="No"; 
                                      }
                                            }?>{{$cond}}@endif</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 bg-white"><strong>Television</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0">
                            <?php 
                            foreach($data->amenties as $amenities){
                                 if($amenities['name']=='television'){
                                    $cond="Yes"; 
                                    break;
                                    }
                                     else {
                                      $cond="No"; 
                                      }
                                            }?>{{$cond}}
                                            </p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">@if(@$compare)<?php 
                            foreach($compare->amenties as $amenities){
                                 if(@$amenities['name']=='television'){
                                    $cond="Yes"; 
                                    break;
                                    }
                                     else {
                                      $cond="No"; 
                                      }
                                            }?>{{$cond}}@endif</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-0">
                        <p class="mb-0 dark-bg bb-block"><strong>Kitchen</strong></p>
                    </div>
                    <div class="col-md-4 pl-0 pr-0 bb-block">
                        <p class="mb-0">
                            <?php 
                            foreach($data->amenties as $amenities){
                                 if($amenities['name']=='kitchen'){
                                    $cond="Yes"; 
                                    break;
                                    }
                                     else {
                                      $cond="No"; 
                                      }
                                            }?>{{$cond}}
                                            </p>
                    </div>
                    <div class="col-md-4 pl-0 bb-block">
                        <p class="mb-0">@if(@$compare)<?php 
                            foreach($compare->amenties as $amenities){
                                 if(@$amenities['name']=='kitchen'){
                                    $cond="Yes"; 
                                    break;
                                    }
                                     else {
                                      $cond="No"; 
                                      }
                                            }?>{{$cond}}@endif</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

</div>
</div>
@include('frontend.auth.modals.compare')


<script src="{{asset('lib/prefixfree.min.js')}}"></script>
	<script src="{{asset('lib/jquery-3.3.1.slim.min.js')}}"></script>
	<script src="{{ asset('js/script.js') }}"></script>
	<script type="text/javascript" src="{{asset('lib/jquery.min.js')}}"></script>
	<script src="{{ asset('lib/bootstrap.bundle.min.js') }}"></script>
	<script src="{{asset('lib/sweetalert2.js')}}"></script>
	<script src="{{asset('js/waitMe.min.js')}}"></script>
	<script src="{{asset('js/site-cookie.js')}}"></script>
	<script src="{{asset('js/developer.js')}}"></script>
	<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
	<script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/owl.carousel.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="{{ asset('js/landingpage.js') }}"></script>

</body>

</html>


