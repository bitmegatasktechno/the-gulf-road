<div class="row hd-and-details">
    <div class="col-md-9">
        <h3>{{ucfirst($data->property_title)}}</h3>
        <h6>
            {{$data->no_of_bedrooms}} Bedrooms · 
            {{$data->no_of_bathrooms}} Bathrooms · 
            
            @switch($data->furnishing_type)
                @case('semi_furnished')
                    Semi Furnished
                    @break

                @case('fully_furnished')
                    Fully Furnished
                    @break

                @case('unfurnished')
                    Unfurnished
                    @break

                @default
                    
            @endswitch
        </h6>
        <span><i class="fa fa-map-marker mr-2"></i>
            {{$data->house_number}}, {{$data->location_name}} {{$data->landmark}}, {{$data->country}}
        </span>
    </div>
    <div class="col-md-3 text-center">
        
        <div class="pl-4">
            @php
                $user_name = \App\Models\User::where('_id', $data->user_id)->first();
            @endphp

            @if($user_name)
            @if(!isset($user_name->image) || $user_name->image=='')
                                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 40px; width: 40px">
                                    @else
                                    @if(file_exists('uploads/profilePics/'.$user_name->image))
                                    <img class="circle" src="{{url('uploads/profilePics/'.$user_name->image)}}" style="height: 40px; width: 40px">
                                    @else
                                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 40px; width: 40px">
                                    @endif
                                    @endif
                    
                    <span class="dp-block mt-2">
                        {{ucfirst($user_name->name)}}
                    </span>
            @else
                <img class="circle" src="https://dummyimage.com/42x42/ccc/fff" style="height: 40px; width: 40px">
                <span class="dp-block mt-2">
                        Unknown
                </span>
            @endif
           
        </div>
        <small class="info-text-contact" autocomplete="off" data-toggle="modal" data-target="#contact_information" style="cursor: pointer;"> <img class="ml-2" src="{{asset('img/contact.png')}}"></small>
    </div>
    <div class="col-md-12">
        <hr>
    </div>
</div>
<div class="row des-and-details">
    <div class="col-md-12">
        <h4 class="mt-2">Description</h4>
        <p class="mb-0">
            {{\Str::limit($data->property_description, 250, '')}}
            <?php
                $string = strip_tags($data->property_description);
                if (strlen($string) > 250) {
                    
                    $stringCut = substr($string, 250);
                    $stringp = '<span id="dots">...</span><span id="text_content" class="content" style="display:none;">'.$stringCut.'</span></p><a href="javascript:show_hide();" class="show_hide" id="show_hide" data-content="toggle-text">Read More</a>';
                    echo $stringp;
                }else{
                        //echo "</p>";
                    }
            ?>
            

        <h4 class="mt-4">Property Details</h4>
        <div class="row dp-flex align-items-center">
            <div class="col-md-4">
                <p><strong>·</strong>Build up area: {{$data->build_up_area ?? 0}} {{ucfirst($data->build_up_area_unit) ?? 'Sqft'}} </p>
            </div>
            <div class="col-md-4">
                <p><strong>·</strong>Carpet area: {{$data->carpet_area ?? 0}} {{ucfirst($data->carpet_area_unit) ?? 'Sqft'}}</p>
            </div>
            <div class="col-md-4">
                <p><strong>·</strong>Furnishing: 
                    @switch($data->furnishing_type)
                        @case('semi_furnished')
                            Semi furnished
                            @break

                        @case('fully_furnished')
                            Fully furnished
                            @break

                        @case('unfurnished')
                            Unfurnished
                            @break

                        @default
                            
                    @endswitch
                </p>
            </div>
            <div class="col-md-4">
                <p><strong>·</strong>Flooring: {{$data->flooring_type}}</p>
            </div>
            @if($data->parking_available=='yes')
            	<div class="col-md-4">
	                <p><strong>·</strong>Covered parkings: {{$data->no_of_covered_parking}}</p>
	            </div>
	            <div class="col-md-4">
	                <p><strong>·</strong>Open parkings: {{$data->no_of_open_parking}}</p>
	            </div>
            @else
            	<div class="col-md-4">
	                <p><strong>·</strong>Parkings available: No</p>
	            </div>
            @endif
            
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade how_it_modal" id="contact_information" tabindex="-1" role="dialog" aria-labelledby="contact_informationLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-0 pb-0">
        <h5 class="modal-title" id="contact_informationLabel">Owner's Contact Information</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p ><b>Name :</b> {{ucfirst($user_name->name)}}</p>
        <p ><b>Email Address :</b> {{$user_name->email}}</p>
        <p ><b>Phone Number :</b> {{$user_name->countryCode}} {{$user_name->phone}}</p>

      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    function show_hide()
            {
                 var display = $("#text_content").css("display");
                 if(display=='none')
                 {
                     $("#text_content").css("display","unset");
                    $("span#dots").text("");
                     $("a#show_hide").text("Read Less");
                    
                 }else
                 {
                       $("#text_content").css("display","none");
                      
                      $("span#dots").text("...");
                      $("a#show_hide").text("Read More");
                     

                 }
                  
            }
</script>
