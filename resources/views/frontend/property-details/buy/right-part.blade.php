<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" /><div class="col-md-4 pl-0">
    <div class="buddy-regst-form bg-white rounded p-3 mb-3 instant-booking">

        @if($data->discount_available=='checked')
            <h2>
            	<strike>{{$data->price}}</strike>
            	{{$data->discount_price}} {{$data->price_currency}}
            </h2>
        @else
            <!-- <h2>{{$data->price*12}} {{$data->price_currency}}</h2> -->
            <h2>{{number_format($data->price)}} {{$data->price_currency}}</h2>
        @endif
 
        <form id="book_property" method="post" action="">
        <input type="hidden" value="buy" id="type" name="type">
        <input type="hidden" value="{{$data->_id}}" id="property" name="property">
        <div class="input-daterange">
            <div class="row mt-3">
                <div class="col-md-6 pr-0">
                    <label>Check in</label>
                    <input id="startDate" name="startDate"  placeholder="dd-mm-yyyy" class="form-control datepicker" readonly style="margin-bottom:0px !important;"/>
                </div>
                <div class="col-md-6 pl-0">
                    <label>Check-out</label>
                    <input id="endDate" name="endDate" placeholder="dd-mm-yyyy" class="form-control datepicker" readonly style="margin-bottom:0px !important;"/>
                </div> 
            </div>
            <div class="row mt-3">
                <div class="col-md-12">
                    <label>Guest</label>
                    @php
                                $peopleUrl = '';
                                if(isset($_GET['capacity'])){
                                    $peopleUrl = $_GET['capacity'];
                                }
                            @endphp
                            <select name="capacity" id="capacity" class="people_filter">
                                <option value="">No of Guest</option>
                                @foreach(range(1, 10) as $row)
                                    <option value="{{$row}}" @if($row==$peopleUrl) selected @endif>{{$row}} Guest</option>
                                @endforeach
                                <option value="10+" @if($row==$peopleUrl) selected @endif>Above 10 Guest</option>
                            </select>
                </div>
            </div>
            <div class="row mt-3"> 
                <div class="col-md-12">
                  <a href="{{url(app()->getLocale().'/cost-calculator3/'.$link.'/buy/'.$data->_id)}}"><button type="button" class="red-btn full-width rounded mt-3" style="background: #fdc03c;border: 1px solid #fdc03c;" autocomplete="off">Cost Calculator</button></a>
                </div>

                <div class="col-md-12">
                @if(Auth::user())
                    <button type="submit" class="red-btn full-width rounded mt-3" autocomplete="off">Book Instantly</button>
                    @else
                    <a href="javascript:;" onclick="showLoginForm();"><button type="button" class="red-btn full-width rounded mt-3" autocomplete="off">Book Instantly</button></a>
                    @endif
                </div>
            </div>
        </div>
</form>
    </div>
    <div class="bg-white rounded p-3 want-to-do">
        <h6 class="mt-2">What do you want to do?</h6>
        <div class="btn-group" role="group" aria-label="Basic example">
            <button type="button" id="buy" onClick="selecttype1('buy')" class="btn brdr  {{ Request::segment(4)=='buy' ? 'active' : '' }}">Buy</button>
            <button type="button" id="rent" onClick="selecttype1('rent')" class="btn brdr  {{ Request::segment(4)=='rent' ? 'active' : '' }}">Rent</button>
            <button type="button" id="swap" onClick="selecttype1('swap')" class="btn brdr  {{ Request::segment(4)=='swap' ? 'active' : '' }}">Swap</button>
        </div>
        <form action="" id="enquiry" method="post">
        <div class="mt-4">
        <input type="hidden" name="type" id="type_get_rent" value="{{Request::segment(4)}}">
          <input type="hidden" name="property_id" id="property_id" value="{{$data->_id}}">
          <input type="hidden" name="touser" id="touser" value="{{@$data->user_id}}">
          <input class="form-control" type="text" name="name" id="name" placeholder="Full Name" required><br>
          <div class="row">
            <div class="col-md-5" style="padding-right: 0px; ">
              <select name="phone_code" id="phone_code" class="country-code-js form-control " style="height: 38px; font-size: 15px!important;" required>
                @foreach(getAllCodeWithName() as $row)
                <option value="{{ $row['dial_code'] }}">{{$row['name']}} {{$row['dial_code']}}</option>
                @endforeach
              </select>
            </div>
            <div class="col-md-7" style="padding-left: 0px">
              <div class="">
                <input class="form-control" type="number" name="phone" id="phone" placeholder="Phone" required>
              </div>
            </div>
          </div><br>
          
          <input class="form-control" type="email" name="email" id="email" placeholder="Email" required><br>
          <input class="form-control" type="text" name="message" id="message" placeholder="Message" required><br>

           <div class="row">
            <div class="col-md-12">
                <div class="form-group">
 
        
                  <!--  <div class="g-recaptcha" data-sitekey="{{env('reCAPTCHA_site_key')}}"></div> -->
                <div class="g-recaptcha"  id="g-makeInquiryWidgetId"></div>
 
 
                </div>
              </div>
         </div>
          


          @if(\Auth::user())
          <button class="send-enq-btn full-width" type="submit"><img class="mr-2" src="{{asset('img/send_enquiry.png')}}"> Make an Enquiry</button>
          @else
                <a href="javascript:;" onclick="showLoginForm();"> <button class="send-enq-btn full-width" type="button"><img class="mr-2" src="{{asset('img/send_enquiry.png')}}"> Make an Enquiry</button></a>
                @endif
        </div>
        </form>
                              </div>
                              <div class="bg-white rounded p-3 want-to-do mt-2">
        <div class="mt-4">
            <p class="text-center">Want to talk to the agent?
            @if(\Auth::user())
				        <a href="javascript:;" onclick="showEnquiryForm('{{$data->user_id}}');">
                @else
                <a href="javascript:;" onclick="showLoginForm();">
                @endif
             Contact Agent</a></p>
            <p><strong>This property is available for inspection.</strong></p>
            <p>You can go and inspect this property on the available days. Book now.</p>
            @if(\Auth::user())
            <a href="javascript:;" onclick="showEnquiryForm('{{$data->user_id}}');">
            @else
                <a href="javascript:;" onclick="showLoginForm();">
                @endif
            <button class="red-bordered inspection-btn full-width mb-3 mt-3">Book Inspection</button>
          </a>
        </div>
    </div>
    @if(Auth::user())
    <!-- href="{{url(app()->getLocale().'/report_user/'.$link.'/buy/'.$data->_id)}}" -->
    <a onclick="showReportForm('{{$data->_id}}')" style="cursor: pointer;"><p class="report-listing text-center"><img class="mr-2" src="{{asset('img/flag_report.png')}}">Report this listing</p></a>
    @endif
</div>


@section('scripts')

@include('frontend.profile.common.js')
<script type="text/javascript">
  $(document).on('click','.send_enquiry', function(e){
      e.preventDefault();
      var ajax_url = WEBSITE_URL+"/property/buy/send_enquiry";
      var name=$("#name").val();

      var email=$("#email").val();
      var phone=$("#phone").val();
      var message=$("#message").val();
      var property_id=$("#property_id").val();
      $.ajax({
        url:ajax_url,
        method:"POST",
        data:{
          name:name,
          email:email,
          phone:phone,
          message:message,
          property_id:property_id
        },
        headers:{
          'X-CSRF-TOKEN': '{{ csrf_token() }}',
        },
        success:function(data){
          stopLoader('.page-content');
          if(data.status){
            Swal.fire({
                title: 'Success!',
                text: data.message,
                icon: 'success',
                confirmButtonText: 'Ok'
            }).then((result) => {
              window.location.reload();
            });
          }else{
            Swal.fire({
                title: 'Success!',
                text: data.message,
                icon: 'success',
                confirmButtonText: 'Ok'
            }).then((result) => {
              window.location.reload();
            });
          }
          // alert(data.msg);
        }
      });
  });

  $(document).on('click','.sendreview', function(){
    var ajax_url = WEBSITE_URL+"/property/review";
    var user_id=$("#user_id").val();
    var review_type=$("#review_type").val();
    var type_id=$("#type_id").val();
    var hygine=$("#hygine").val();
    var atmosphere=$("#atmosphere").val();
    var location=$("#location").val();
    var cleanliness=$("#cleanliness").val();
    var service=$("#service").val();
    var review=$("#review").val();



  var accept=$("#accept:checked").val();
  if(!accept){
    $("#selectaccept").show();
        return false;
  }



    $.ajax({
      url:ajax_url,
      method:"POST",
      data:{
        user_id:user_id,
        review_type:review_type,
        type_id:type_id,
        atmosphere:atmosphere,
        hygine:hygine,
        location:location,
        review:review,
        cleanliness:cleanliness,
        service:service
      },
      headers:{
        'X-CSRF-TOKEN': '{{ csrf_token() }}',
      },
      success:function(data){
        stopLoader('.page-content');
        if(data.status){
          Swal.fire({
              title: 'Success!',
              text: data.msg,
              icon: 'success',
              confirmButtonText: 'Ok'
          }).then((result) => {
            window.location.reload();
          });
        }else{
          Swal.fire({
              title: 'Success!',
              text: data.msg,
              icon: 'success',
              confirmButtonText: 'Ok'
          }).then((result) => {
            window.location.reload();
          });
        }
        // alert(data.msg);
      }
    });
  });
  $(".enterreview").click(function(){
  $("#viewrefund").modal('show');
  });


  $(document).ready(function(){

  /* 1. Visualizing things on Hover - See next part for action on click */
  $('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });

  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });


  /* 2. Action to perform on click */
  $('#stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');

    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }

    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }

    // JUST RESPONSE (Not needed)
    var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
    var msg = "";
    if (ratingValue > 1) {
        msg = ratingValue;
    }
    else {
        msg = ratingValue;
    }
    responseMessage(msg);

  });


});

$(document).ready(function(){

/* 1. Visualizing things on Hover - See next part for action on click */
$('#stars1 li').on('mouseover', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

  // Now highlight all the stars that's not after the current hovered star
  $(this).parent().children('li.star').each(function(e){
    if (e < onStar) {
      $(this).addClass('hover');
    }
    else {
      $(this).removeClass('hover');
    }
  });

}).on('mouseout', function(){
  $(this).parent().children('li.star').each(function(e){
    $(this).removeClass('hover');
  });
});


/* 2. Action to perform on click */
$('#stars1 li').on('click', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently selected
  var stars = $(this).parent().children('li.star');

  for (i = 0; i < stars.length; i++) {
    $(stars[i]).removeClass('selected');
  }

  for (i = 0; i < onStar; i++) {
    $(stars[i]).addClass('selected');
  }

  // JUST RESPONSE (Not needed)
  var ratingValue = parseInt($('#stars1 li.selected').last().data('value'), 10);
  var msg = "";
  if (ratingValue > 1) {
      msg = ratingValue;
  }
  else {
      msg = ratingValue;
  }
  responseMessage1(msg);

});


});

$(document).ready(function(){

/* 1. Visualizing things on Hover - See next part for action on click */
$('#stars2 li').on('mouseover', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

  // Now highlight all the stars that's not after the current hovered star
  $(this).parent().children('li.star').each(function(e){
    if (e < onStar) {
      $(this).addClass('hover');
    }
    else {
      $(this).removeClass('hover');
    }
  });

}).on('mouseout', function(){
  $(this).parent().children('li.star').each(function(e){
    $(this).removeClass('hover');
  });
});


/* 2. Action to perform on click */
$('#stars2 li').on('click', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently selected
  var stars = $(this).parent().children('li.star');

  for (i = 0; i < stars.length; i++) {
    $(stars[i]).removeClass('selected');
  }

  for (i = 0; i < onStar; i++) {
    $(stars[i]).addClass('selected');
  }

  // JUST RESPONSE (Not needed)
  var ratingValue = parseInt($('#stars2 li.selected').last().data('value'), 10);
  var msg = "";
  if (ratingValue > 1) {
      msg = ratingValue;
  }
  else {
      msg = ratingValue;
  }
  responseMessage2(msg);

});


});

$(document).ready(function(){

/* 1. Visualizing things on Hover - See next part for action on click */
$('#stars3 li').on('mouseover', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

  // Now highlight all the stars that's not after the current hovered star
  $(this).parent().children('li.star').each(function(e){
    if (e < onStar) {
      $(this).addClass('hover');
    }
    else {
      $(this).removeClass('hover');
    }
  });

}).on('mouseout', function(){
  $(this).parent().children('li.star').each(function(e){
    $(this).removeClass('hover');
  });
});


/* 2. Action to perform on click */
$('#stars3 li').on('click', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently selected
  var stars = $(this).parent().children('li.star');

  for (i = 0; i < stars.length; i++) {
    $(stars[i]).removeClass('selected');
  }

  for (i = 0; i < onStar; i++) {
    $(stars[i]).addClass('selected');
  }

  // JUST RESPONSE (Not needed)
  var ratingValue = parseInt($('#stars3 li.selected').last().data('value'), 10);
  var msg = "";
  if (ratingValue > 1) {
      msg = ratingValue;
  }
  else {
      msg = ratingValue;
  }
  responseMessage3(msg);

});


});

$(document).ready(function(){

/* 1. Visualizing things on Hover - See next part for action on click */
$('#stars4 li').on('mouseover', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

  // Now highlight all the stars that's not after the current hovered star
  $(this).parent().children('li.star').each(function(e){
    if (e < onStar) {
      $(this).addClass('hover');
    }
    else {
      $(this).removeClass('hover');
    }
  });

}).on('mouseout', function(){
  $(this).parent().children('li.star').each(function(e){
    $(this).removeClass('hover');
  });
});


/* 2. Action to perform on click */
$('#stars4 li').on('click', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently selected
  var stars = $(this).parent().children('li.star');

  for (i = 0; i < stars.length; i++) {
    $(stars[i]).removeClass('selected');
  }

  for (i = 0; i < onStar; i++) {
    $(stars[i]).addClass('selected');
  }

  // JUST RESPONSE (Not needed)
  var ratingValue = parseInt($('#stars4 li.selected').last().data('value'), 10);
  var msg = "";
  if (ratingValue > 1) {
      msg = ratingValue;
  }
  else {
      msg = ratingValue;
  }
  responseMessage4(msg);

});


});


function responseMessage(msg) {
  $("#hygine").val(msg);
}
function responseMessage1(msg) {
  $("#atmosphere").val(msg);
}
function responseMessage2(msg) {
  $("#location").val(msg);
}
function responseMessage3(msg) {
  $("#cleanliness").val(msg);
}
function responseMessage4(msg) {
  $("#service").val(msg);
}
</script>
@endsection
