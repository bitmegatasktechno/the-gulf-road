
<!-- Bannar Section -->
@include('frontend.property-details.common-data.banner-section')

<!-- Next Section -->
<section class="body-light-grey p-0">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="bg-white mb-3 p-3 rounded">

                    @include('frontend.property-details.buy.details')

                    @include('frontend.property-details.common-data.amenities')
                     @include('frontend.property-details.common-data.map')
                    @include('frontend.property-details.buy.rating')
                    <br>
                    @include('frontend.property-details.common-data.map-nearby')

                </div>
            </div>

            @if(\Auth::user())
            @if(\Auth::user()->id!=$data->user_id)
            <!-- right section -->
            @include('frontend.property-details.buy.right-part')
            @endif
            @else
            @include('frontend.property-details.buy.right-part')
            @endif
        </div>
    </div>
</section>
