@php
$lang=Request::segment(1);
$loc=Request::segment(3);
if($loc == 'ksa' || $loc == 'uae' )
{
   $loc21 = strtoupper($loc);
}else 
{
    $loc21 = ucfirst($loc);
}
 $type=Request::segment(4);
$modules = \App\Models\Module::where('location', $loc21)->get();
if($loc!='all'){
    $link=$loc;
}else{
    $link='all';
}
@endphp




@extends('frontend.layouts.home')
@section('title','Property Details')
@section('css')
	<link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
	<style type="text/css">
		.error{
		    color: red !important;
		}
	</style>
     <script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
    <script type="text/javascript">
            var wiriteReviewWidgetId;
            var makeInquiryWidgetId;
            var makeInspectionInquiryWidgetId;
            var makeReportFlagWidgetId;
            var CaptchaCallback = function() {
            wiriteReviewWidgetId = grecaptcha.render('g-wiriteReviewWidgetId', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
            makeInquiryWidgetId = grecaptcha.render('g-makeInquiryWidgetId', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
            makeInspectionInquiryWidgetId = grecaptcha.render('g-makeInspectionInquiryWidgetId', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
            makeReportFlagWidgetId = grecaptcha.render('g-makeReportFlagWidgetId', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
            };
             
    </script>
@endsection
@section('content')
   
	@include('frontend.layouts.default-header')
	
	
    
	@include('frontend.property-details.common-data.filter')



    @include('frontend.property-details.buy.section')
    
	<section class="body-light-grey p-0">
	    <div class="container">
            <div class="row">
                <div class="col-md-12 mt-5">
                
					<h2 style="font-weight: 600;">Nearby Homes</h2>
					<span style="font-weight: 200;">Similar homes near your location</span>
                    <p></p>
                </div>
                <div class="container">

                <div id="mydivon" class="fadeIn">
                <div class="row dynamicContent">
                    @include('frontend.property-details.common-data.list')
                </div>
            </div>
                
                <div class="col-md-12 show-more text-center">
                    <a href="{{url(app()->getLocale().'/buy-property/'.$link.'?country='.$link.'&location=')}}"><button class="red-bordered mt-3 mb-5">SHOW MORE <i class="fa fa-angle-right"></i></button></a>
                </div>

            </div>
            </div>
			</div>
	</section>
    @include('frontend.layouts.footer')

@endsection
@section('scripts')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
	<script type="text/javascript">

		$(document).ready(function () {

			function show_hide()
            {
                 var display = $("#text_content").css("display");
                 if(display=='none')
                 {
                     $("#text_content").css("display","unset");
                    $("span#dots").text("");
                     $("a#show_hide").text("Read Less");
                    
                 }else
                 {
                       $("#text_content").css("display","none");
                      
                      $("span#dots").text("...");
                      $("a#show_hide").text("Read More");
                     

                 }
                  
            }


		    $(".show_hide").on("bind","click", function () {
				var dots = document.getElementById("dots");
				alert(dots);
				if (dots.style.display === "none") {
					dots.style.display = "inline";
				} else {
					dots.style.display = "none";
				}
		        var txt = $(".content").is(':visible') ? 'Read More' : 'Read Less';
		        $(this).text(txt);
		        $('.content').toggle();
		    });
		});
	</script>
	 
@endsection