@if(count($data)>0)
    @php
        $i= ($data->currentPage() - 1) * $data->perPage() + 1;
    @endphp
    @foreach($data as $row)
    @php
        $totaluser = \App\Models\Review::where('is_approved',1)->where('review_type',"7")->where('type_id',$row->_id)->count();
        $ratinggiven1=\App\Models\Review::where('is_approved',1)->where('review_type',"7")->where('type_id',$row->_id)->sum('average');
        $totalrating=@$totaluser*5;
        if($totalrating == 0){
            $totalrating=1;
        }
        $avg=number_format(@$ratinggiven1/@$totalrating*5);
    @endphp
    <div class="col-md-4">
        <a href="{{url(app()->getLocale().'/buddy/'.Request::segment(3).'/'.$row->_id)}}">
            <div class="buddy-wrapper bg-white rounded p-3 mt-4">
                <div class="row">
                    <div class="col-md-4">
                    @if(!isset($row->image) || $row->image=='')
                                <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 104px;width: 104px;">
                                @else
                                @if(file_exists('uploads/profilePics/'.$row->image))
                                <img class="circle" src="{{url('uploads/profilePics/'.$row->image)}}" style="height: 104px;width: 104px;">
                                @else
                                <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 104px;width: 104px;">
                                @endif
                                @endif
                    </div>
                    <div class="col-md-8">
                        <h4>{{ucfirst($row->name)}}</h4>
                        
                        <h6>{{ucfirst($row->country) ? ucfirst($row->country) : 'N/A'}}  ·  {{ucfirst($row->gender) ? ucfirst($row->gender) : 'N/A'}}  ·  {{$row->age}} yrs</h6>


                        <ul class="list-inline mb-2">
                            @if(@$avg==1)
                            <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                            @endif
                            @if(@$avg==2)
                            <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                            <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                            @endif
                            @if(@$avg==3)
                            <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                            <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                            <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                            @endif
                            @if(@$avg==4)
                            <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                            <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                            <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                            <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                            @endif
                            @if(@$avg==5)
                            <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                            <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                            <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                            <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                            <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                            @endif
                            @if(@$avg != 0)
                            <li class="white-color text" style="color:black !important;">{{@$avg}}</li>
                            @endif
                        </ul>

                    </div>
                    <div class="col-12">
                        <small class="dp-block">LANGUAGE KNOWN</small>
                        @if($row->buddyProfessional)
                            @php
                                $languages = $row->buddyProfessional->languages;
                                $lang = 0;
                            @endphp

                            @if(is_array($languages))
                                @foreach($languages as $subrow)
                                    @if($lang<3)
                                        <span class="mr-1">{{$subrow}}</span>
                                        @php
                                            $lang++;
                                        @endphp
                                    @else
                                        <span class="mr-1 text-center">+1</span>
                                        @break
                                    @endif
                                @endforeach
                            @else
                                <span class="mr-1">N/A</span>
                            @endif
                        @endif
                        <div><hr></div>
                    </div>
                    <div class="col-md-12">
                        <small>CONSULTATION CHARGES</small>
                        <h5>{{@$row->charge_currency??'AED'}} {{@$row->charges}}<small class="fs-16"> / {{@$row->charge_type??'Hourly'}}</small></h5>
                    </div>
                </div>
            </div>
        </a>
    </div>
    @php $i++; @endphp
    @endforeach
    <div class="col-md-12 text-center">
        <div class="pagination pagination--left">
            {{ $data->links() }}
        </div>
    </div>
@else
    <div style="display: block;max-height: 60px;text-align: center;background-color: #d6e9c6;line-height: 4;width: 100%;">
        <p style="font-size: 16px;font-weight: 700;">No Result Found</p>
    </div>
@endif
