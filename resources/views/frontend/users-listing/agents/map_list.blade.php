<style>
    #map {
    height: 650px;
    width: 100%;
}
</style>

<div class="col-md-5 property-lists">
@if(count($map_data)>0)
	@php
		$i= ($map_data->currentPage() - 1) * $map_data->perPage() + 1;
	@endphp
	@foreach($map_data as $row)
  @php
                            $totaluser = \App\Models\Review::where('is_approved',1)->where('review_type',"6")->where('type_id',$row->_id)->count();
                            $ratinggiven1=\App\Models\Review::where('is_approved',1)->where('review_type',"6")->where('type_id',$row->_id)->sum('average');
                            $totalrating=@$totaluser*5;
                            if($totalrating == 0){
                                $totalrating=1;
                            }
                            $avg=number_format(@$ratinggiven1/@$totalrating*5);
                            @endphp
        <div class="appartment-details propertycards mb-4">
        <a href="{{url(app()->getLocale().'/agent/'.$link.'/'.$row->_id)}}">
          <div class="row">
          
            <div  class="col-md-5 pr-0">
            @if(!isset($row->image) || $row->image=='')
                    <img class="img-fluid property-img" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 150px;margin-left: auto;margin-right: auto;width: auto !important;">
                    @else
                    @if(file_exists('uploads/profilePics/'.$row->image))
                    <img class="img-fluid property-img" src="{{url('uploads/profilePics/'.$row->image)}}" style="height: 150px;margin-left: auto;margin-right: auto;width: auto !important;">
                    @else
                    <img class="img-fluid property-img" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 150px;margin-left: auto;margin-right: auto;width: auto !important;">
                    @endif    
                    @endif
            </div>
            <div class="col-md-7">
              <div class="property-card-map">
            <h2>{{ucfirst($row->name)}}</h2>
            <p style="color:black;">{{ucfirst($row->country) ? ucfirst($row->country) : 'N/A'}}  ·  {{ucfirst($row->gender) ? ucfirst($row->gender) : 'N/A'}}  ·  28 yrs</p>
    

            <ul class="list-inline mb-0 dp-flex align-itmes-center">
            @if(@$avg==1)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==2)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==3)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==4)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==5)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                <li class="white-color text" style="color:black !important;">{{@$avg}}</li>
                                    </ul>

                <p class="mt-2"> <span style="color:black; font-weight: 600"> Languages: </span>
                @php 
                    if(!is_array(@$row['professionals'][0]->languages))
                        $languages = explode(',', @$row['professionals'][0]->languages);
                    else
                        $languages = @$row['professionals'][0]->languages;
                    
                @endphp
                <?php
                    foreach (@$languages as $key => $value) {
                        if($value)
                            echo ucfirst($value).",";
                    }
                ?>
                  
                </p>
            <p class="mb-1 mt-3">Properties</p>
            <div class="super-agent-blockMap">
            <span class="full-width">
                <div class="row">
                    <div class="col-md-6 text-center bdr-right">
                        <strong class="dp-block mb-0">{{$row->saleProperties->count()}}</strong>
                        <small class="dp-block mb-0">For Sale</small>
                    </div>
                    <div class="col-md-6 text-center">
                        <strong class="dp-block mb-0">{{$row->rentProperties->count()}}</strong>
                        <small class="dp-block mb-0">For Rent</small>
                    </div>
                </div>
            </span>
          </div>
           
          </div>
            </div>
          </div>
</a>
        </div>
        @php $i++; @endphp
    @endforeach
    @endif  
       
        <div class="text-center">
          <div class="pagination pagination--left">
          {{ $map_data->links() }}
        </div>
        </div>
      </div>

      <div class="col-md-7">
        <ul class="list-inline mb-2">
          <!-- <li class="listings"><a href=""><img src="{{asset('img/commute.png')}}"><p>Commute</p></a></li>
          <li class="listings"><a href=""><img src="{{asset('img/commute.png')}}"><p>School</p></a></li>
          <li class="listings"><a href=""><img src="{{asset('img/commute.png')}}"><p>Hospital</p></a></li>
          <li class="listings"><a href=""><img src="{{asset('img/commute.png')}}"><p>Shops</p></a></li> -->
        </ul>  
        <div id="map"></div>
      </div>
      @if(env('APP_ACTIVE_MAP') =='patel_map')
          
          <!--  <script src="https://mapapi.cloud.huawei.com/mapjs/v1/api/js?callback=initMaps&key={{env('PATEL_MAP_KEY')}}"></script> -->

         <script>

        var mMarker;

        // Start the script callback.
        function initMaps(){

          document.getElementById('map').innerHTML = '';


             var lat_long = <?php echo json_encode($latlongData);?>;
            var locations = lat_long;
            console.log(locations.length);
            console.log(locations);
            if(locations.length)
            {
                 
                 var mapOptions1 = {};
                 
                 //console.log(locations[0]);
                    mapOptions1.center = {lat: locations[0][1]*1, lng: locations[0][2]*1};
                   //mapOptions1.center = {lat: 25.19901, lng: 55.27984};
                
                mapOptions1.zoom = 9;

                 var map = new HWMapJsSDK.HWMap(document.getElementById('map'), mapOptions1);
               
                
                // Initialize the map.
              // map = new HWMapJsSDK.HWMap(document.getElementById('map'), mapOptions);

                // Initialize the marker. 
                for (i = 0; i < locations.length; i++) {

                         mMarker = new HWMapJsSDK.HWMarker({
                            map: map,
                            position: {
                                lat: locations[i][1]*1, lng: locations[i][2]*1
                            },
                            zIndex: 10,
                            label: {
                                text: locations[i][0],
                                offsetY: -30,
                                fontSize: '20px'
                            },
                            icon: {
                                opacity: 0.5,
                                scale: 1.2,
                            },
                            
                        });

                         
                      
                    } 
                }

                
           
}

initMaps();


     
 
     </script>
     @elseif(env('APP_ACTIVE_MAP')=='google_map')
  <script type="text/javascript">


  var lat_long = <?php echo json_encode($latlongData);?>;
 
    var locations = lat_long;

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 5,
      center: new google.maps.LatLng(25.2048493, 55.2707828),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;
    for (i = 0; i < locations.length; i++) {
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map,
        label: {
            color: 'white',
            fontWeight: 'bold',
            text: locations[i][3]
        },
        title: locations[i][0]
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }



  </script>
  @endif