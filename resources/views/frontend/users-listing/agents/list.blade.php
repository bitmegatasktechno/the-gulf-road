@if(count($data)>0)
    @php
        $i= ($data->currentPage() - 1) * $data->perPage() + 1;
    @endphp
    @foreach($data as $row)
    @php
                            $totaluser = \App\Models\Review::where('is_approved',1)->where('review_type',"6")->where('type_id',$row->_id)->count();
                            $ratinggiven1=\App\Models\Review::where('is_approved',1)->where('review_type',"6")->where('type_id',$row->_id)->sum('average');
                            $totalrating=@$totaluser*5;
                            if($totalrating == 0){
                                $totalrating=1;
                            }
                            $avg=number_format(@$ratinggiven1/@$totalrating*5);
                            @endphp
        <div class="col-md-3">
            <a href="{{url(app()->getLocale().'/agent/'.$link.'/'.$row->_id)}}">
                <div class="super-agent-block pb-4 mb-4">
                    <div class="img-block">
                    @if(!isset($row->image) || $row->image=='')
                                    <img class="dp-block" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 150px;margin-left:auto;margin-right: auto;width: auto !important;">
                                    @else
                                    @if(file_exists('uploads/profilePics/'.$row->image))
                                    <img class="dp-block" src="{{url('uploads/profilePics/'.$row->image)}}" style="height: 150px;margin-left:auto;margin-right: auto;width: auto !important;">
                                    @else
                                    <img class="dp-block" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 150px;margin-left:auto;margin-right: auto;width: auto !important;">
                                    @endif
                                    @endif
                        <div class="star-block">
                            <div class="row mt-1 mb-1">
                                <div class="col-md-7">
                                <ul class="list-inline mb-2 ml-1">
                                @if(@$avg==1)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==2)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==3)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==4)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==5)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                <li class="white-color text">{{@$avg}}</li>
                                    </ul>
                                </div>
                                <!-- <div class="col-md-5 pl-0">
                                    <ul class="list-inline mb-0 pl-3 pr-3 text-right">
                                        <li class=""><img style="height: 10px;" src="{{asset('img/mail_white_agent_ic.png')}}"></li>
                                        <li class="ml-2"><img style="height: 10px;" src="{{asset('img/call_white_agent_ic.png')}}"></li>
                                        <li class="ml-2"><img style="height: 10px;" src="{{asset('img/whatsapp_white_agent_ic.png')}}"></li>
                                    </ul>
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <div class="plr-10">
                        <h4>{{ucfirst($row->name)}}</h4>
                        
                        <h6>
                            {{ucfirst($row->country) ? ucfirst($row->country) : 'N/A'}}  ·  {{ucfirst($row->gender) ? ucfirst($row->gender) : 'N/A'}}  ·  28 yrs
                        </h6>
                        <p class="mb-1">Languages known</p>

                        @if($row->agentProfessional)
                            @php
                                $languages = $row->agentProfessional->languages;
                                $lang = 0;
                            @endphp

                            @if(is_array($languages))
                                
                                @foreach($languages as $subrow)
                                    @if($lang<2)
                                        <span class="mr-3">{{$subrow}}</span>
                                        @php
                                            $lang++;
                                        @endphp
                                    @else
                                        <span class="mr-1 text-center">+1</span>
                                        @break
                                    @endif
                                @endforeach
                            @else
                                <span>N/A</span>
                            @endif
                        @endif
                        
                        
                        <p class="mb-1 mt-3">Properties</p>
                        <span class="full-width">
                            <div class="row">

                                <div class="col-md-6 pr-0 text-center bdr-right">
                                    <strong class="dp-block mb-0">{{$row->saleProperties->count()}}</strong>
                                    <small class="dp-block mb-0">For Sale</small>
                                </div>
                                <div class="col-md-6 pr-0 text-center">
                                    <strong class="dp-block mb-0">{{$row->rentProperties->count()}}</strong>
                                    <small class="dp-block mb-0">For Rent</small>
                                </div>
                            </div>
                        </span>
                    </div>
                </div>
            </a>
        </div>      
        
        @php $i++; @endphp
    @endforeach

    <div class="col-md-12 text-center">
        <div class="pagination pagination--left">
            {{ $data->links() }}
        </div>
    </div>
@else
    <div class="" style="display: block;
        max-height: 60px;
        text-align: center;
        background-color: #d6e9c6;
        line-height: 4;
        width: 100%;
        ">
        <p style="font-size: 16px;font-weight: 700;">No Result Found</p>
    </div>
@endif
