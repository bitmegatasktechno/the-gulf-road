@php
$lang=Request::segment(1);
$loc=Request::segment(3);
    $modules = \App\Models\Module::where('location',$loc)->get();
    if($loc!='all'){
        $link=$loc;
    }else{
        $link='all';
    }
@endphp
@extends('frontend.layouts.home')
@section('title','Agents Listing')
@section('content')
@include('frontend.layouts.default-header')
<style type="text/css">
    .super-agent-block:hover{
        box-shadow: 0px 4px 12px #000000ad;
        transition: all .3s ease-in 0s;
    }
</style>
@include('frontend.users-listing.agents.filter')
    <section class="vlight-background pt-4 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="list-inline breadcrum mb-4">
                        <li>
                            <a href="{{url(app()->getLocale().'/home/'.$link)}}">Home </a>
                        </li>
                        <span> / </span>
                        <li>
                            <a href="{{url(app()->getLocale().'/agents/'.$link)}}"> Agents Listing</a>
                        </li>
                    </ul>
                </div>
                @if($map_data->count() > 0)
                <div class="col-md-6 text-right">
                    <div class="checkbox switcher">
                      <small>Show Map</small>
                      <a href="javascript:;" onmousedown="toggleDiv('mydiv');">
                          <label for="test1">
                          <input type="checkbox" id="test1" value="">
                          <span><small></small></span>      
                          </label>
                      </a>
                    </div>
                </div>
                @endif
            </div>
            <div id="mydivon" class="fadeIn">
                <div class="row dynamicContent">
                    @include('frontend.users-listing.agents.list')
                </div>
            </div>
            <div id="mydivoff" style="display:none">
                <div class="row">
                @include('frontend.users-listing.agents.map_list')
                </div>    
            </div>
        </div>
    </section>	
    @include('frontend.layouts.footer')
@endsection
@section('scripts')
<script type="text/javascript">
	function toggleDiv(divid){
		varon = divid + 'on';
		varoff = divid + 'off';
		if(document.getElementById(varon).style.display == 'none'){
			document.getElementById(varon).style.display = 'block';
			document.getElementById(varoff).style.display = 'none';
		}else{
			document.getElementById(varon).style.display = 'none';
			document.getElementById(varoff).style.display = 'block';
		}
	} 
	var jqxhr = {abort: function () {  }};
	var filter_data = $("form[name=filter_listing]").serialize();
	// save user personal informations.
	$(document).on("click", ".pagination li a", function (e){
        e.preventDefault();
        startLoader('.page-content');
        var url = $(this).attr('href');
        var page = url.split('page=')[1];      
        loadListings(url, 'filter_listing');
    });

    $(document).on("click", ".reset_filter", function (e){
        e.preventDefault();
        $("form[name='filter_listing']")[0].reset();
        $("option:selected").prop("selected", false);
        $("form[name='filter_listing']").find('input').val('');
        window.location.href = "{{url(app()->getLocale().'/agents/'.$link)}}";
    });

    $(document).on('change','.country_filter', function(){
    	var url = "{{url(app()->getLocale().'/agents/'.$link)}}";
    	loadListings(url, 'filter_listing');
    });

    function loadListings(url,filter_form_name){
        var filtering = $("form[name=filter_listing]").serialize();
        jqxhr.abort();
        jqxhr =$.ajax({
            type : 'get',
            url : url,
            data : filtering,
            dataType : 'html',
            beforeSend:function(){
                startLoader('body');
            },
            success : function(data){
                data = data.trim();
                $(".dynamicContent").empty().html(data);
            },
            error : function(response){
                stopLoader('body');
            },
            complete:function(){
                stopLoader('body');
            }
        });
    }
</script>
@endsection