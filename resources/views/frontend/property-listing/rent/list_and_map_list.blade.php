<div id="mydivon" class="fadeIn">
    <div class="row dynamicContent">
        @include('frontend.property-listing.rent.list')
    </div>
</div>
<div id="mydivoff" style="display:none">
    <div class="row " >
        @include('frontend.property-listing.rent.map_list')
    </div>    
</div>
