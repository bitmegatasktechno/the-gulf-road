@php
$lang=Request::segment(1);
$loc=Request::segment(3);
  $modules = \App\Models\Module::where('location',$loc)->get();
  if($loc!='all'){
      $link=$loc;
  }else{
      $link='all';
  }
                    @endphp
<style>
    #map {
    height: 100%;
    width: 100%;
    max-height: 650px;
}
</style>

<div class="col-md-12 text-left">
        <div class="">
           Total Rent Property found : {{ $data->total() }}
        </div>
    </div>
    <br>
    
<div class="col-md-5 property-lists">
@if(count($map_data)>0)
	@php
		$i= ($map_data->currentPage() - 1) * $map_data->perPage() + 1;
	@endphp
	@foreach($map_data as $row)
  @php
  if(isset($_GET['nearby']) && ($_GET['nearby'] == '1' || $_GET['nearby'] == 1) && $row->getDistance() > 20)
    {
        continue;
    }
                            $totaluser = \App\Models\Review::where('is_approved',1)->where('review_type',"2")->where('type_id',$row->_id)->count();
                            $ratinggiven1=\App\Models\Review::where('is_approved',1)->where('review_type',"2")->where('type_id',$row->_id)->sum('average');
                            $totalrating=@$totaluser*5;
                            if($totalrating == 0){
                                $totalrating=1;
                            }
                            $avg=number_format(@$ratinggiven1/@$totalrating*5);
                            @endphp
        <div class="appartment-details propertycards mb-4">
        <a href="{{url(app()->getLocale().'/property/'.$link.'/buy/'.$row->_id)}}">
          <div class="row">
          
            <div  class="col-md-5 pr-0">
            @if(!isset($row->files[0]) || $row->files[0]=='')
                    <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
                    @else
                    @if(file_exists('uploads/properties/'.$row->files[0]))
                    <img class="img-fluid property-img" src="{{url('uploads/properties/'.$row->files[0])}}">
                    @else
                    <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
                    @endif    
                    @endif
            </div>
            <div class="col-md-7">
              <div class="property-card-map">
            <h5>{{ucfirst($row->house_type)}}</h5>
            <h2>{{\Str::limit(ucfirst($row->property_title), 25)}}</h2>
            <p><i class="fa fa-map-marker mr-2"></i>{{\Str::limit(ucfirst($row->location_name), 20)}}, {{$row->country}}</p>
            <ul class="list-inline">
            @if(@$avg==1)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==2)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==3)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==4)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==5)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                <li class="white-color text" style="color:black !important;">{{@$avg}}</li>
            </ul>
            <h6>{{$row->rent ?? 0}} {{$row->rent_currency ? strtoupper($row->rent_currency) : 'N/A'}} / {{$row->rent_frequency ? $row->rent_frequency : 'N/A'}}</h6>
            <ul class="list-inline red-list mb-4">
            <li class="mr-3"><img class="mr-1" src="{{asset('img/red-bed.png')}}" title="Bedrooms"> {{$row->no_of_bedrooms}}</li>
                            <li class="mr-3"><img title="Bathrooms" class="mr-1" src="{{asset('img/red-tub.png')}}"> {{$row->no_of_bathrooms}}</li>
                            <li class="mr-3"><img title="Open Parkings" class="mr-1" src="{{asset('img/red-car.png')}}"> {{$row->no_of_open_parking}}</li>
            </ul>
          </div>
            </div>
          </div>
</a>
        </div>
        @php $i++; @endphp
    @endforeach
    @endif  
       
        <div class="text-center">
          <div class="pagination pagination--left">
          {{ $map_data->links() }}
        </div>
        </div>
      </div>

      <div class="col-md-7">
        <ul class="list-inline mb-2">
          <!-- <li class="listings"><a href=""><img src="{{asset('img/commute.png')}}"><p>Commute</p></a></li>
          <li class="listings"><a href=""><img src="{{asset('img/commute.png')}}"><p>School</p></a></li>
          <li class="listings"><a href=""><img src="{{asset('img/commute.png')}}"><p>Hospital</p></a></li>
          <li class="listings"><a href=""><img src="{{asset('img/commute.png')}}"><p>Shops</p></a></li> -->
        </ul>  
        <div id="map"></div>
      </div>
      
      @if(env('APP_ACTIVE_MAP') =='patel_map')
          
          <!--  <script src="https://mapapi.cloud.huawei.com/mapjs/v1/api/js?callback=initMaps&key={{env('PATEL_MAP_KEY')}}"></script> -->

         <script>

        var mMarker;

        // Start the script callback.
        function initMaps(){
             var lat_long = <?php echo json_encode($latlongData);?>;
            var locations = lat_long;
            if(locations.length)
            {
                 var mapOptions = {};
                mapOptions.center = {lat: locations[0][1], lng: locations[0][2]};
                
                mapOptions.zoom = 9;

                 var map2 = new HWMapJsSDK.HWMap(document.getElementById('map'), mapOptions);
               
                
                // Initialize the map.
              // map = new HWMapJsSDK.HWMap(document.getElementById('map'), mapOptions);

                // Initialize the marker. 
                for (i = 0; i < locations.length; i++) {

                         mMarker = new HWMapJsSDK.HWMarker({
                            map: map2,
                            position: {lat: locations[i][1], lng: locations[i][2]},
                            zIndex: 10,
                            label: {
                                text: locations[i][0],
                                offsetY: -30,
                                fontSize: '20px'
                            },
                            icon: {
                                opacity: 0.5,
                                scale: 1.2,
                            },
                            
                        });
                      
                    } 
                }
           
}

initMaps();


     
 
     </script>
     @elseif(env('APP_ACTIVE_MAP')=='google_map')

  <script type="text/javascript">


  var lat_long = <?php echo json_encode($latlongData);?>;
  console.log(lat_long);
    var locations = lat_long;

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 5,
      center: new google.maps.LatLng(25.2048493, 55.2707828),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;
    // size=16;
    //         var img=new google.maps.MarkerImage('{{asset('img/Grey_map_loc_ic.png')}}',
    //             new google.maps.Size(size, size),
    //             new google.maps.Point(0,0),
    //             new google.maps.Point(size/2, size/2)
    //        );

    for (i = 0; i < locations.length; i++) {
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map,
        label: {
        color: 'white',
        fontWeight: 'bold',
        text: locations[i][3]
  },
        title: locations[i][0]
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }



  </script>
  @endif