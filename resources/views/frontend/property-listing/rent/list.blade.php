<style>
i.fa.fa-heart{
    padding-top: 2px !important;
    padding-left: 227px !important;
}
</style>




<div class="col-md-12 text-left">
        <div class="">
           Total Rent Property found : {{ $data->total() }}
        </div>
    </div>
    <br>
@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
@if(count($data)>0)
	@php
		$i= ($data->currentPage() - 1) * $data->perPage() + 1;
	@endphp
	@foreach($data as $row)
    @php
    
                            $totaluser = \App\Models\Review::where('is_approved',1)->where('review_type',"2")->where('type_id',$row->_id)->count();
                            $ratinggiven1=\App\Models\Review::where('is_approved',1)->where('review_type',"2")->where('type_id',$row->_id)->sum('average');
                            $totalrating=@$totaluser*5;
                            if($totalrating == 0){
                                $totalrating=1;
                            }
                            $avg=number_format(@$ratinggiven1/@$totalrating*5);

                            $is_premium = \App\Models\Subscription::where('_id',$row->is_premium)->where('type','premium')->latest()->first();
                            
                            $propis_premium = 1;

                            if($is_premium && isset($is_premium->sub_plan) ){
                                $row->available_from = str_replace('/', '-',$row->available_from);

                                $lastDate = strtotime($row->available_from." +".$is_premium->sub_plan[0]['validity']."days");

                                if($lastDate < strtotime('Now') ){
                                    $propis_premium = 0;
                                }
                            }else{
                                $propis_premium = 0;
                            }

                            @endphp
		<div class="col-md-3">

                @if(isset(Auth::user()->id))
                @php
                    $fav = \App\Models\Favourate::where('propert_id',$row->_id)->where('user_id',Auth::user()->id)->first();
                    @endphp
                    @endif
                    @if(@$fav)
                    <i class="fa fa-heart fav-icon" onClick="makefav('{{$row->_id}}')" style="right: 24px;z-index: 999;"></i>
                    @else
                    <i class="fa fa-heart-o fav-icon" onClick="makefav('{{$row->_id}}')" style="right: 24px;z-index: 999;"></i>
                    @endif
            <a href="{{url(app()->getLocale().'/property/'.$link.'/rent/'.$row->_id)}}">
                <div class="property-card bg-white rounded" style="height: 428px;">
                    @if($propis_premium == 1)
                        <button class="red-btn red-btn-new" style="z-index: 1"><img src="{{url('assets/img/Premium_Tick.png')}}" > &nbsp; Premium</button>
                    @endif  
                    @if(!isset($row->files[0]) || $row->files[0]=='')
                    <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
                    @else
                    @if(file_exists('uploads/properties/'.$row->files[0]))
                    <img class="img-fluid property-img" src="{{url('uploads/properties/'.$row->files[0])}}">
                    @else
                    <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
                    @endif  
                    @endif
                    <div class="pl-3 pr-3">
                        <h5>{{ucfirst($row->house_type)}}</h5>
                        
                        <h2>{{\Str::limit(ucfirst($row->property_title), 25)}}</h2>
                        
                        <p class="mb-2">
                            <i class="fa fa-map-marker mr-2"></i> {{\Str::limit(ucfirst($row->location_name), 55)}}, {{$row->country}}
                        </p>

                        <h6 class="mb-0">
                            {{$row->rent ? $row->rent : '0'}}
                            {{$row->rent_currency ? strtoupper($row->rent_currency) : 'N/A'}} / 
                            {{$row->rent_frequency ? $row->rent_frequency : 'N/A'}}
                        </h6>
                        
                        <ul class="list-inline">
                        @if(@$avg==1)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==2)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==3)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==4)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==5)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif

                                @if(@$avg != 0)
                                <li class="white-color text" style="color:black !important;">{{@$avg}}</li>
                                @endif
                            
                        </ul>
                        <ul class="list-inline red-list mb-4">
                            <li class="mr-3"><img class="mr-1" src="{{asset('img/red-bed.png')}}" title="Bedrooms"> {{$row->no_of_bedrooms}}</li>
                            <li class="mr-3"><img title="Bathrooms" class="mr-1" src="{{asset('img/red-tub.png')}}"> {{$row->no_of_bathrooms}}</li>
                            <li class="mr-3"><img title="Open Parkings" class="mr-1" src="{{asset('img/red-car.png')}}"> {{$row->no_of_open_parking}}</li>
                        </ul>
                    </div>
                </div>
            </a>
        </div>
        @php $i++; @endphp
	@endforeach

	
    <div class="col-md-12 text-center">
        <div class="pagination pagination--left">
            {{ $data->links() }}
        </div>
    </div>
@else
    <div class="" style="display: block;
        max-height: 60px;
        text-align: center;
        background-color: #d6e9c6;
        line-height: 4;
        width: 100%;
        ">
        <p style="font-size: 16px;font-weight: 700;">No Result Found</p>
    </div>
@endif
