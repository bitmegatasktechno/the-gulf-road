<section class="top-searh-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 first-two-box with-calander">
                <form name="filter_listing " id="filter_listing">
                    <ul class="list-inline mb-0">
                        <li class="mb-2">
                            <div class="search-box">
                                <input class="full-width location_name" type="text" id="address" onChange="getLocation()" placeholder="“Search properties by location”" name="location" value="{{isset($_GET['location']) ? $_GET['location']: ''}}">
                                <input type="hidden" name="longitude" id="longitude" value="{{isset($_GET['longitude']) ? $_GET['longitude']: ''}}">
                                <input type="hidden" name="latitude" id="latitude" value="{{isset($_GET['latitude']) ? $_GET['latitude']: ''}}">
                                <img class="search-icon" src="{{asset('img/search.png')}}">
                            </div>
                        </li>
                        <li class="mb-2">
                            <div class="inputbox search-box select inputbox-label-inside" style="position: relative;">
                                <div id="reportrange" style="width: 250px;border: 1px solid rgba(0,0,0,0.08);height: 40px;padding: 8px 1px 8px 2px;">
                                    <i class="fa fa-calendar"></i>&nbsp;
                                   
                                    <span></span>
                                    
                                     <i class="fa fa-caret-down"></i>
                                </div>
                                <input type="hidden" name="start_date" id="swap_start_date" value="{{(isset($_GET['start_date']) && $_GET['start_date']!='') ? $_GET['start_date'] : \Carbon\Carbon::now()->format('Y-m-d')}}">
                                <input type="hidden" name="end_date" id="swap_end_date" value="{{(isset($_GET['end_date']) && $_GET['end_date']!='') ? $_GET['end_date'] : \Carbon\Carbon::now()->addDays(30)->format('Y-m-d')}}">
                                <!-- <input type="hidden" name="start_date" id="swap_start_date" value="{{(isset($_GET['start_date']) && $_GET['start_date']!='') ? $_GET['start_date'] : ''}}">
                                <input type="hidden" name="end_date" id="swap_end_date" value="{{(isset($_GET['end_date']) && $_GET['end_date']!='') ? $_GET['end_date'] : ''}}"> -->
                            </div>
                        </li>

 						 <li class="mb-2" style="margin-left:114px">
                            @php
                                $houseUrl = '';
                                if(isset($_GET['house_type'])){
                                    $houseUrl = $_GET['house_type'];
                                }
                            @endphp
                            <select name="house_type" class="house_type_filter">
                                <option value="">Swap Type</option>
                                @foreach(getAllHouseTypes() as $row)
                                    <option value="{{strtolower($row->name)}}" @if(strtolower($houseUrl)==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                @endforeach
                            </select>
                        </li>
                        <li class="mb-2">
                            @php
                                $peopleUrl = '';
                                if(isset($_GET['capacity'])){
                                    $peopleUrl = $_GET['capacity'];
                                }
                            @endphp
                            <select name="capacity" class="people_filter">
                                <option value="">No of Guest</option>
                                @foreach(range(1, 10) as $row)
                                    <option value="{{$row}}" @if($row==$peopleUrl) selected @endif>{{$row}} Guest</option>
                                @endforeach
                                <option value="10+" @if($row==$peopleUrl) selected @endif>Above 10 Guest</option>
                            </select>
                        </li>
                        
                        <li class="mb-2">
                            <button class="form-control list-property red" onclick="$('#filter_listing').submit();" type="button">Submit</button>
                        </li>
                        <li class="mb-2">
                            <button class="form-control list-property red reset_filter" type="button">Reset</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</section>
<div id="map2"></div>

@if(env('APP_ACTIVE_MAP') =='patel_map')
<style>
    #address_result ul li 
    {
    display: block;
      width: 100%;
  }
    #address_result
    {
        z-index: 9;
    }
</style>
    <script src="https://mapapi.cloud.huawei.com/mapjs/v1/api/js?callback=initMap&key={{env('PATEL_MAP_KEY')}}"></script>
    <script>
      function initMap() {
            var mapOptions = {};
            mapOptions.center = {lat: 48.856613, lng: 2.352222};
            mapOptions.zoom = 8;
            mapOptions.language='ENG';
            var searchBoxInput = document.getElementById('address');
             var map = new HWMapJsSDK.HWMap(document.getElementById('map2'), mapOptions);

            // Create the search box parameter.
            var acOptions = {
                location: {
                    lat: 48.856613,
                    lng: 2.352222
                },
                radius: 10000,
                customHandler: callback,
                 language:'en'
            };
            var autocomplete;
            // Obtain the input element.
            // Create the HWAutocomplete object and set a listener.
            autocomplete = new HWMapJsSDK.HWAutocomplete(searchBoxInput, acOptions);
            autocomplete.addListener('site_changed', printSites);
             initMaps();
            // Process the search result. 
            // index: index of a searched place.
            // data: details of a searched place. 
            // name: search keyword, which is in strong tags. 
            function callback(index, data, name) {
                console.log(index, data, name);
                var div
                div = document.createElement('div');
                div.innerHTML = name;
                return div;
            }
            // Listener.
            function printSites() {
                 
                
                var site = autocomplete.getSite();
                var latitude = site.location.lat;
                var longitude = site.location.lng;
                var country_name = site.name;
                
                var results = "Autocomplete Results:\n";
                var str = "Name=" + site.name + "\n"
                    + "SiteID=" + site.siteId + "\n"
                    + "Latitude=" + site.location.lat + "\n"
                    + "Longitude=" + site.location.lng + "\n"
                    + "Address=" + site.formatAddress + "\n";

                    
                results = results + str;

                $('#latitude').val(latitude);
                $('#longitude').val(longitude);
                $('#countryTo').val(country_name);
                $('#country_name').val(country_name);
                console.log(latitude, longitude);
                /*console.log(results);*/
            }
            
        }
        function getLocation()
        {

        }
    </script> 

     
    @elseif(env('APP_ACTIVE_MAP')=='google_map')
<script>

google.maps.event.addDomListener(window, 'load', initMap);
function initMap() {
var input = document.getElementById('address');
var autocomplete = new google.maps.places.Autocomplete(input);
autocomplete.bindTo('bounds', map);
autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);
}

function getLocation()
 {
//   alert(address);
  var address = $('#address').val();
  // alert(address);
  var geocoder = new google.maps.Geocoder();

  geocoder.geocode( { 'address': address}, function(results, status)
  {


    if (status == google.maps.GeocoderStatus.OK)
    {
      console.log(results[0],"afdcfsa");
      console.log(results[0].address_components);
      const countryObj=results[0].address_components.filter(val=>{
            if(val.types.includes('country')){
                return true
            }
            return false;
      });
     console.log(countryObj,'the country obj');


     var country_name = countryObj[0].long_name;
     console.log(country_name,'name');
      var latitude = results[0].geometry.location.lat();

      var longitude = results[0].geometry.location.lng();
      // alert(latitude +"-----"+ longitude);
     $('#latitude').val(latitude);
     $('#longitude').val(longitude);
     $('#countryTo').val(country_name);
     $('#country_name').val(country_name);
    console.log(latitude, longitude);
    }
  });

}
    </script>
@endif