<style>
i.fa.fa-heart{
    padding-top: 2px !important;
    padding-left: 227px !important;
}
i.fa.fa-heart {
    font-size: 24px;
    position: absolute;
    /* padding-left: 309px !important; */
    color: red;
    top: 15px;
    right: 24px !important;
    z-index: 999;
}
</style>
@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
<style>
.property-card .property-img {
    height: 151px !important;
}
</style>
@if(count($data)>0)
    @php
        $i= ($data->currentPage() - 1) * $data->perPage() + 1;
    @endphp
    @foreach($data as $row)
    @php
    
                            $totaluser = \App\Models\Review::where('is_approved',1)->where('review_type',"3")->where('type_id',$row->_id)->count();
                            $ratinggiven1=\App\Models\Review::where('is_approved',1)->where('review_type',"3")->where('type_id',$row->_id)->sum('average');
                            $totalrating=@$totaluser*5;
                            if($totalrating == 0){
                                $totalrating=1;
                            }
                            $avg=number_format(@$ratinggiven1/@$totalrating*5);


                            $is_premium = \App\Models\Subscription::where('_id',$row->is_premium)->where('type','premium')->first();
                            
                            $isprop_premium = 1;
                            if($is_premium && isset($is_premium->sub_plan) ){
                                $row->available_from = str_replace('/', '-',$row->available_from);

                                $lastDate = strtotime($row->available_from." +".$is_premium->sub_plan[0]['validity']."days");

                                if($lastDate < strtotime('Now') ){
                                    $isprop_premium = 0;
                                }
                            }else{
                                $isprop_premium = 0;
                            }


                            @endphp
        <div class="col-md-3">
        @if(isset(Auth::user()->id))
                @php
                    $fav = \App\Models\Favourate::where('propert_id',$row->_id)->where('user_id',Auth::user()->id)->first();
                    @endphp
                    @endif
                    @if(@$fav)
                    <i class="fa fa-heart fav-icon" onClick="makefav('{{$row->_id}}')"></i>
                    @else
                    <i class="fa fa-heart-o fav-icon" onClick="makefav('{{$row->_id}}')"></i>
                    @endif
            <a href="{{url(app()->getLocale().'/property/'.$link.'/swap/'.$row->_id)}}">
                <div class="property-card bg-white rounded">
                    @if($isprop_premium)
                    <button class="red-btn red-btn-new" style="z-index: 1"><img src="{{asset('img/Premium_Tick.png')}}" > &nbsp; Premium</button>
                    @endif
                    @if(!isset($row->files[0]) || $row->files[0]=='')
                    <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}" style="width: 100%;height: 150px;">
                    @else
                    @if(file_exists('uploads/swap/'.$row->files[0]))
                    <img class="img-fluid property-img" src="{{url('uploads/swap/'.$row->files[0])}}" style="width: 100%;height: 150px;">
                    @else
                    <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
                    @endif 
                    @endif

                    <div class="pl-3 pr-3">
                        <h5>{{ucfirst($row->house_type)}}</h5>

                        <h2>{{\Str::limit(ucfirst($row->title), 25)}}</h2>

                        <p class="mb-0"><i class="fa fa-map-marker mr-2"></i> {{\Str::limit($row->location_name, 55)}}, {{$row->country}}</p>
                        
                       

                        <ul class="list-inline mb-3">
                        @if(@$avg==1)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==2)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==3)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==4)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==5)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                
                                @if(@$avg == 0)
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                
                                @endif
                                <li class="white-color text" style="color:black !important;">{{@$avg}}</li>
                        </ul>

                        <ul class="list-inline red-list mb-4">
                            <li class="mr-3"><img class="mr-1" src="{{asset('img/red-bed.png')}}" title="Bedrooms"> {{$row->no_of_bedrooms ?? 0}}</li>
                            <li class="mr-3"><img title="Bathrooms" class="mr-1" src="{{asset('img/red-tub.png')}}"> {{$row->no_of_bathrooms ?? 0}}</li>
                            <li class="mr-3"><img title="Open Parkings" class="mr-1" src="{{asset('img/red-car.png')}}"> {{$row->no_of_open_parking ?? 0}}</li>
                        </ul>
                    </div>
                </div>
            </a>
        </div>   
        
        @php $i++; @endphp
    @endforeach

    <div class="col-md-12 text-center">
        <div class="pagination pagination--left">
            {{ $data->links() }}
        </div>
    </div>
@else
    <div class="no_result_div">
        <p>No Result Found</p>
    </div>
@endif
