@php
$lang=Request::segment(1);
$loc=Request::segment(3);
    $modules = \App\Models\Module::where('location',$loc)->get();
    if($loc!='all'){
        $link=$loc;
    }else{
        $link='all';
    }
@endphp
@extends('frontend.layouts.home')
@section('title','Swap Listing')
@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection
@section('content')
@include('frontend.layouts.default-header')
    <style type="text/css">
        .first-two-box li:nth-child(1) {
            width: 25%;
        }
    </style>
    @include('frontend.property-listing.swap.filter')
    <section class="vlight-background pt-4 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="list-inline breadcrum mb-4">
                        <li><a href="{{url(app()->getLocale().'/home/'.$link)}}">Home </a></li>
                        <span> / </span>
                        <li><a href="javascript:;"> Swap Listing</a></li>
                    </ul>
                </div>
                @if($data->count() > 0)
                <div class="col-md-6 text-right">
                    <div class="checkbox switcher">
                      <small>Show Map</small>
                      <a href="javascript:;" onmousedown="toggleDiv('mydiv');">
                          <label for="test1">
                          <input type="checkbox" id="test1" value="">
                          <span><small></small></span>      
                          </label>
                      </a>
                    </div>
                </div>
                @endif
            </div>
            <div id="mydivon" class="fadeIn">
                <div class="row dynamicContent">
                    @include('frontend.property-listing.swap.list')
                </div>
            </div>
            <div id="mydivoff" style="display:none">
                <div class="row">
                @include('frontend.property-listing.swap.map_list')
                </div>    
            </div>
        </div>
    </section>	
    @include('frontend.layouts.footer')

@endsection

@section('scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	<script type="text/javascript">
		
		function toggleDiv(divid){
			varon = divid + 'on';
			varoff = divid + 'off';

			if(document.getElementById(varon).style.display == 'none')
			{
				document.getElementById(varon).style.display = 'block';
				document.getElementById(varoff).style.display = 'none';
			}
			else
			{ 
				document.getElementById(varoff).style.display = 'block';
				document.getElementById(varon).style.display = 'none';
			}
		} 

		var filter_data = $("form[name=filter_listing]").serialize();
    	var jqxhr = {abort: function () {  }};
        
        $(document).ready(function(){
            
            
        $(document).on("click", ".reset_filter", function (e){
            e.preventDefault();

            window.location = "{{url(app()->getLocale().'/swap/'.$link)}}";
        });


            $('#reportrange span').html($('#swap_start_date').val() + '-' + $('#swap_end_date').val());

            $('#reportrange').daterangepicker({
                opens: 'left',
                minDate: new Date(),
                maxDate: '+8m',
            }, 
            function(start, end, label) {
                $('#swap_start_date').val(start.format('YYYY-MM-DD'));
                $('#swap_end_date').val(end.format('YYYY-MM-DD'));
                $('#reportrange span').html(start.format('DD/MM/YYYY') + '-' + end.format('DD/MM/YYYY'));

            });
    		// save user personal informations.
    		$(document).on("click", ".pagination li a", function (e){
                e.preventDefault();
                startLoader('.page-content');
                var url = $(this).attr('href');
                var page = url.split('page=')[1];      
                loadListings(url, 'filter_listing');
            });

            $(document).on("keydown", "form", function(event) { 
                return event.key != "Enter";
            });
            
            /*

            $(document).on('keyup', '.location_name', function (e) {
                
                if($(this).val().length > 3)
                {
                    var url = "{{url(app()->getLocale().'/swap/'.$link)}}";
                	loadListings(url, 'filter_listing');
                }
                if($(this).val().length == 0)
                {
                    var url = "{{url(app()->getLocale().'/swap/'.$link)}}";
                	loadListings(url, 'filter_listing');
                }
            });

            $(document).on('change','.type_filter,.people_filter,.country_filter,.house_type_filter', function(){
            	var url = "{{url(app()->getLocale().'/swap/'.$link)}}";
            	loadListings(url, 'filter_listing');
            });
            */
        });

        function loadListings(url,filter_form_name){

            var filtering = $("form[name=filter_listing]").serialize();
            //abort previous ajax request if any
            jqxhr.abort();
            jqxhr =$.ajax({
                type : 'get',
                url : url,
                data : filtering,
                dataType : 'html',
                beforeSend:function(){
                    startLoader('body');
                },
                success : function(data){
                    $("#mydivoff").hide();
                    $("#mydivon").show();
                    data = data.trim();
                    $(".dynamicContent").empty().html(data);
                },
                error : function(response){
                    stopLoader('body');
                },
                complete:function(){
                    stopLoader('body');
                }
            });
        }
	</script>
@endsection