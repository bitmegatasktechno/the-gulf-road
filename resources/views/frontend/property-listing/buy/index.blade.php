@php
$lang=Request::segment(1);
$loc=Request::segment(3);
if($loc == 'ksa' || $loc == 'uae' )
{
   $loc21 = strtoupper($loc);
}else 
{
    $loc21 = ucfirst($loc);
}
 $type=Request::segment(4);
$modules = \App\Models\Module::where('location', $loc21)->get();
if($loc!='all'){
    $link=$loc;
}else{
    $link='all';
}
@endphp
@extends('frontend.layouts.home')
@section('title','Buy Properties')
@section('content')
@include('frontend.layouts.default-header')
@include('frontend.property-listing.buy.filter')
    <section class="vlight-background pt-4 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="list-inline breadcrum mb-4">
                        <li><a href="{{url(app()->getLocale().'/home/'.$link)}}">Home </a></li>
                        <span> / </span>
                        <li><a href="{{url(app()->getLocale().'/buy-property/'.$link)}}"> Buy Properties</a></li>
                    </ul>
                </div>
                @if($data->count() > 0)
                <div class="col-md-6 text-right">
                    <div class="checkbox switcher">
                      <small>Show Map</small>
                      <a href="javascript:;" onmousedown="toggleDiv('mydiv');">
                          <label for="test1">
                            <input type="checkbox" id="test1" value="">
                            <span><small></small></span>     
                          </label>
                      </a>
                    </div>
                </div>
                @endif
            </div>
            <div id="mydivon" class="fadeIn">
                <div class="row dynamicContent">
                    @include('frontend.property-listing.buy.list')
                </div>
            </div>

            <div id="mydivoff" style="display:none">
                <div class="row dynamicMapContent">
                @include('frontend.property-listing.buy.map_list')
                </div>    
            </div>

        </div>
    </section>
@include('frontend.layouts.footer')
@endsection
@section('scripts')
	<script type="text/javascript">
		function toggleDiv(divid){
            //alert(divid);
            varon = divid + 'on';
            varoff = divid + 'off';

              if($("#test1").is(':checked'))
             {
                

                document.getElementById(varon).style.display = 'block';
                document.getElementById(varoff).style.display = 'none';
             }else
             {
                document.getElementById(varon).style.display = 'none';
                document.getElementById(varoff).style.display = 'block';
             } 

             /*if(document.getElementById(varon).style.display == 'none')
            {
                document.getElementById(varon).style.display = 'block';
                document.getElementById(varoff).style.display = 'none';

            }else{ 

                document.getElementById(varoff).style.display = 'block';
                document.getElementById(varon).style.display = 'none';

            } */
        } 
		var filter_data = $("form[name=filter_listing]").serialize();
    	var jqxhr = {abort: function () {  }};
		// save user personal informations.
		$(document).on("click", ".pagination li a", function (e){
            e.preventDefault();

            $('#showMapList').trigger('click');
            startLoader('.page-content');
            var url = $(this).attr('href');
            var page = url.split('page=')[1];      
            loadListings(url, 'filter_listing');
        });

        $(document).on("click", ".pagination li a", function (e){
            e.preventDefault();
            $('#showMapList').trigger('click');
            startLoader('.page-content');
            var url = $(this).attr('href');
            var page = url.split('page=')[1];      
            loadListings(url, 'filter_listing');
        });
        $(document).on("click", ".reset_filter", function (e){
            e.preventDefault();
            $("form[name='filter_listing']")[0].reset();
            $("option:selected").prop("selected", false);
            $("form[name='filter_listing']").find('input').val('');

            var url = "{{url(app()->getLocale().'/buy-property/'.$link)}}";
            loadListings(url, 'filter_listing');
        });
        $(document).on("click", ".submit_filter", function (e){
            e.preventDefault();
            
            var url = "{{url(app()->getLocale().'/buy-property/'.$link)}}";
            loadListings(url, 'filter_listing');
        });

        // $(document).on('keyup', '.location_name', function () {
        //     if($(this).val().length > 3){
        //         var url = "{{url(app()->getLocale().'/buy-property/'.$link)}}";
        //     	loadListings(url, 'filter_listing');
        //     }
        //     if($(this).val().length == 0){
        //         var url = "{{url(app()->getLocale().'/buy-property/'.$link)}}";
        //     	loadListings(url, 'filter_listing');
        //     }
        // });

        // $(document).on('change','.house_type_filter,.bedrooms_filter,.bathrooms_filter,.price_filter,.country_filter', function(){
        // 	var url = "{{url(app()->getLocale().'/buy-property/'.$link)}}";
        // 	loadListings(url, 'filter_listing');
        // });

        function loadListings(url,filter_form_name){
            var filtering = $("form[name=filter_listing]").serialize();
            //abort previous ajax request if any
            jqxhr.abort();
            jqxhr =$.ajax({
                type : 'get',
                url : url,
                data : filtering,
                beforeSend:function(){
                    startLoader('body');
                },
                success : function(data){
                    $(".dynamicContent").empty().html($(data).find(".dynamicContent").html());
                    $(".dynamicMapContent").empty().html($(data).find(".dynamicMapContent").html());
                    $('#map').hide();
                    if($('.property-card-map').length > 0){
                        $('#map').show();
                    }
                },
                error : function(response){
                    stopLoader('body');
                },
                complete:function(){
                    stopLoader('body');
                }
            });
        }
	</script>
@endsection