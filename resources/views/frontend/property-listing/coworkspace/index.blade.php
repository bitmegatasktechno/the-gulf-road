@php
$lang=Request::segment(1);
$loc=Request::segment(3);
$modules = \App\Models\Module::where('location',$loc)->get();
if($loc!='all'){
    $link=$loc;
}else{
    $link='all';
}
@endphp
@extends('frontend.layouts.home')
@section('title','Coworkspace Listing')
@section('content')
@include('frontend.layouts.default-header')
<style type="text/css">
    .fearure-coworking:hover {
        box-shadow: 0px 4px 12px #000000ad;
        transition: all .3s ease-in 0s;
    }
</style>

@include('frontend.property-listing.coworkspace.filter')

    <section class="vlight-background pt-4 pb-5" id="map_properties">
        <div class="container" >
            <div class="row">
                <div class="col-md-6">
                    <ul class="list-inline breadcrum mb-4">
                        <li><a href="{{url(app()->getLocale().'/home/'.$link)}}">Home </a></li>
                        <span> / </span>
                        <li><a href="javascript:;"> Coworkspace Listing</a></li>
                    </ul>
                </div>
                @if($data->count() > 0)
                    <div class="col-md-6 text-right" id="show_map_parent">
                        <div class="checkbox switcher">
                          <small>Show Map</small>
                          <a href="javascript:;" onmousedown="toggleDiv('mydiv');">
                              <label for="test1">
                                <input type="checkbox" id="test1" value="">
                                <span><small></small></span>      
                              </label>
                          </a>
                        </div>
                    </div>
                @endif
            </div>
            <div id="mydivon" class="fadeIn featured-nearby">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Featured Nearby</h4>
                        <p>New Co-Working Offices Nearby</p>
                    </div>
                </div>
                <div class="row dynamicContent">
                    @include('frontend.property-listing.coworkspace.list')
                </div>
                </div>
                <div id="mydivoff" style="display:none">
                    <div class="row dynamicContent1" >
                    @include('frontend.property-listing.coworkspace.map_list')
                    </div>    
                </div>
            </div>
    </section>	
    @include('frontend.layouts.footer')
@endsection
@section('scripts')
	<script type="text/javascript">
		function toggleDiv(divid){
			varon = divid + 'on';
			varoff = divid + 'off';
			if(document.getElementById(varon).style.display == 'none'){
				document.getElementById(varon).style.display = 'block';
				document.getElementById(varoff).style.display = 'none';
			}else{ 
				document.getElementById(varoff).style.display = 'block';
				document.getElementById(varon).style.display = 'none';
			}
		} 

		var filter_data = $("form[name=filter_listing]").serialize();
    	var jqxhr = {abort: function () {  }};

		// save user personal informations.
		$(document).on("click", ".pagination li a", function (e){
            e.preventDefault();
            startLoader('.page-content');
            var url = $(this).attr('href');
            var page = url.split('page=')[1];      
            loadListings(url, 'filter_listing');
        });

        $(document).on("click", ".pagination1 li a", function (e){
            e.preventDefault();
            startLoader('.page-content');
            var url = $(this).attr('href');
            var page = url.split('page=')[1];      
            loadListingForMaps(url, 'filter_listing');
        });

        $(document).on("click", ".reset_filter", function (e){
            e.preventDefault();
            $("form[name='filter_listing']")[0].reset();
            $("option:selected").prop("selected", false);
            $("form[name='filter_listing']").find('input').val('');

            var url = "{{url(app()->getLocale().'/coworkspace/'.$link)}}";
            loadListings(url, 'filter_listing');
        });
        $(document).on("click", ".submit_filter", function (e){
            e.preventDefault();
            var url = "{{url(app()->getLocale().'/coworkspace/'.$link)}}";
            loadListings(url, 'filter_listing');
        });

        // $(document).on('keyup', '.location_name', function () {
        //     if($(this).val().length > 3)
        //     {
        //         var url = "{{url(app()->getLocale().'/coworkspace/'.$link)}}";
        //     	loadListings(url, 'filter_listing');
        //     }
        //     if($(this).val().length == 0)
        //     {
        //         var url = "{{url(app()->getLocale().'/coworkspace/'.$link)}}";
        //     	loadListings(url, 'filter_listing');
        //     }
        // });

        // $(document).on('change','.type_filter,.people_filter,.country_filter', function(){
        // 	var url = "{{url(app()->getLocale().'/coworkspace/'.$link)}}";
        // 	loadListings(url, 'filter_listing');
        // });

        function loadListings(url,filter_form_name){
            var filtering = $("form[name=filter_listing]").serialize();
            jqxhr.abort();
            jqxhr =$.ajax({
                type : 'get',
                url : url,
                data : filtering,
                dataType : 'html',
                beforeSend:function(){
                    startLoader('body');
                },
                success : function(data){
                     
                    $("#mydivoff").hide();
                    $("#mydivon").show();
                   /* data = data.trim();*/

                    $(".dynamicContent").empty().html($(data).find(".dynamicContent").html());
                    $(".dynamicContent1").empty().html($(data).find(".dynamicContent1").html());

 
                     toggleDiv('mydiv');
                    if($('.no_result_div').is(':visible'))
                    {
                        $('#show_map_parent').hide();
                    }else{
                        $('#show_map_parent').show();
                    }

                    if($('#test1').is(":checked")){
                        $('#mydivoff').show();
                        $('#mydivon').hide();
                    }else{
                        $('#mydivon').show();
                        $('#mydivoff').hide();
                    }
                    $('html, body').animate({
                        scrollTop: $("#map_properties:visible:first").offset().top
                    }, 300);
                },
                error : function(response){
                    stopLoader('body');
                },
                complete:function(){
                    stopLoader('body');
                }
            });
        }
        function loadListingForMaps(url,filter_form_name){
            var filtering = $("form[name=filter_listing]").serialize();
            jqxhr.abort();
            jqxhr =$.ajax({
                type : 'get',
                url : url+'&view_type=map',
                data : filtering,
                dataType : 'html',
                beforeSend:function(){
                    startLoader('body');
                },
                success : function(data){
                    $("#mydivoff").hide();
                    $("#mydivon").show();
                    data = data.trim();
                    $(".dynamicContent1").empty().html(data);
                    if($('.no_result_div').is(':visible')){
                        $('#show_map_parent').hide();
                    }else{
                        $('#show_map_parent').show();
                    }
                    if($('#test1').is(":checked")){
                        $('#mydivoff').show();
                        $('#mydivon').hide();
                    }else{
                        $('#mydivon').show();
                        $('#mydivoff').hide();
                    }
                    $('html, body').animate({
                        scrollTop: $("#map_properties:visible:first").offset().top
                    }, 300);
                },
                error : function(response){
                    stopLoader('body');
                },
                complete:function(){
                    stopLoader('body');
                }
            });
        }
	</script>
@endsection 