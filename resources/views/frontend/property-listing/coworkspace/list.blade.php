<style>
/*i.fa.fa-heart{
    padding-top: 2px !important;
    padding-left: 323px !important;
}*/
.fearure-coworking .coworking-img {
    height: 234px !important;
    width: 351px;
}
</style>
@php
$lang=Request::segment(1);
$loc=Request::segment(3);
    $modules = \App\Models\Module::where('location',$loc)->get();
    if($loc!='all'){
        $link=$loc;
    }else{
        $link='all';
    }
@endphp
@if(count($data)>0)
    @php
        $i= ($data->currentPage() - 1) * $data->perPage() + 1;
    @endphp
    @foreach($data as $row)
    @php
    
                            $totaluser = \App\Models\Review::where('is_approved',1)->where('review_type',"4")->where('type_id',$row->_id)->count();
                            $ratinggiven1=\App\Models\Review::where('is_approved',1)->where('review_type',"4")->where('type_id',$row->_id)->sum('average');
                            $totalrating=@$totaluser*5;
                            if($totalrating == 0){
                                $totalrating=1;
                            }
                            $avg=number_format(@$ratinggiven1/@$totalrating*5);
                            @endphp
        <div class="col-md-4">
        @if(isset(Auth::user()->id))
                @php
                    $fav = \App\Models\Favourate::where('propert_id',$row->_id)->where('user_id',Auth::user()->id)->first();
                    @endphp
                    @endif
                    @if(@$fav)
                    <i class="fa fa-heart fav-icon" style="right: 25px;" onClick="makefav('{{$row->_id}}')"></i>
                    @else
                    <i class="fa fa-heart-o fav-icon" style="right: 25px;" onClick="makefav('{{$row->_id}}')"></i>
                    @endif
            <a href="{{url(app()->getLocale().'/property/'.$link.'/coworkspace/'.$row->_id)}}">
                <div class="fearure-coworking">
                
                @if(!isset($row->files[0]) || $row->files[0]=='')
                    <img class="img-fluid coworking-img" src="{{asset('/img/house2.png')}}">
                    @else
                    @if(file_exists('uploads/coworkspce/'.$row->files[0]))
                    <img class="img-fluid coworking-img" src="{{url('uploads/coworkspce/'.$row->files[0])}}">
                    @else
                    <img class="img-fluid coworking-img" src="{{asset('/img/house2.png')}}">
                    @endif    
                    @endif

                    <div class="p-3">
                        <h5>{{\Str::limit(ucfirst($row->title), 25)}}</h5>
                        <p class="mb-1"><i class="mr-2 fa fa-map-marker"></i> 
                            {{\Str::limit(ucfirst($row->address), 55)}}, {{$row->country}}
                        </p>
                        <ul class="list-inline mb-3">
                            @if(@$avg==1)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                            @endif
                            @if(@$avg==2)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                            @endif
                            @if(@$avg==3)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                            @endif
                            @if(@$avg==4)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                            @endif
                            @if(@$avg==5)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                            @endif
                            <li class="white-color text" style="color:black !important;">{{$avg ? $avg : ''}}</li>
                        </ul>
                        <h6>
                            Starting @ {{$row->open_space_monthly_currency}}{{$row->open_space_monthly_price ?? 0}} /m</h6>
                    </div>
                </div>
            </a>
        </div>
        @php $i++; @endphp
    @endforeach
    <div class="col-md-12 text-center">
        <div class="pagination pagination--left">
            {{ $data->links() }}
        </div>
    </div>
@else
    <div class="no_result_div">
        <p>No Result Found</p>
    </div>
@endif
