<section class="top-searh-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 first-two-box">
                <form name="filter_listing" id="filter_listing">
                    <ul class="list-inline mb-0">
                        <li class="mb-2">
                            <div class="search-box">
                                <input class="full-width location_name" type="text" id="address" onChange="getLocation()" placeholder="“Search properties by location”" name="location" value="{{isset($_GET['location']) ? $_GET['location']: ''}}">
                                <input type="hidden" name="longitude" id="longitude" value="{{isset($_GET['longitude']) ? $_GET['longitude']: ''}}">
                                <input type="hidden" name="latitude" id="latitude" value="{{isset($_GET['latitude']) ? $_GET['latitude']: ''}}">
                                <img class="search-icon" src="{{asset('img/search.png')}}">
                            </div>
                        </li>
                        <li class="mb-2">
                            @php
                                $typeUrl = '';
                                if(isset($_GET['type'])){
                                    $typeUrl = $_GET['type'];
                                }
                            @endphp
                            <select name="type" class="type_filter">
                                <option value="">Coworkspace Type</option>
                                <option value="open" @if($typeUrl=='open') selected @endif>Open </option>
                                <option value="dedicated" @if($typeUrl=='dedicated') selected @endif>Dedicated</option>
                                <option value="private" @if($typeUrl=='private') selected @endif>Private</option>
                            </select>
                        </li>
                        <!-- <li class="mb-2">
                            @php
                                $peopleUrl = '';
                                if(isset($_GET['people'])){
                                    $peopleUrl = $_GET['people'];
                                }
                            @endphp
                            <select name="people" class="people_filter">
                                <option value="">No of People</option>
                                @foreach(range(1, 100) as $row)
                                    <option value="{{$row}}" @if($row==$peopleUrl) selected @endif>{{$row}} People</option>
                                @endforeach
                            </select>
                        </li> -->
                        <li class="mb-2">
                            <button class="form-control list-property red submit_filter" type="button">Submit</button>
                        </li>
                        <li class="mb-2">
                            <button class="form-control list-property red reset_filter" type="button">Reset</button>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</section>

 <div id="map2"></div>

@if(env('APP_ACTIVE_MAP') =='patel_map')
<style>
    #address_result ul li 
    {
    display: block;
      width: 100%;
  }
    #address_result
    {
        z-index: 9;
    }
</style>
    <script src="https://mapapi.cloud.huawei.com/mapjs/v1/api/js?callback=initMap&key={{env('PATEL_MAP_KEY')}}"></script>
    <script>
      function initMap() {
            var mapOptions = {};
            mapOptions.center = {lat: 48.856613, lng: 2.352222};
            mapOptions.zoom = 8;
            mapOptions.language='ENG';
            var searchBoxInput = document.getElementById('address');
             var map = new HWMapJsSDK.HWMap(document.getElementById('map2'), mapOptions);

            // Create the search box parameter.
            var acOptions = {
                location: {
                    lat: 48.856613,
                    lng: 2.352222
                },
                radius: 10000,
                customHandler: callback,
                language:'en'
            };
            var autocomplete;
            // Obtain the input element.
            // Create the HWAutocomplete object and set a listener.
            autocomplete = new HWMapJsSDK.HWAutocomplete(searchBoxInput, acOptions);
            autocomplete.addListener('site_changed', printSites);
             initMaps();
            // Process the search result. 
            // index: index of a searched place.
            // data: details of a searched place. 
            // name: search keyword, which is in strong tags. 
            function callback(index, data, name) {
                console.log(index, data, name);
                var div
                div = document.createElement('div');
                div.innerHTML = name;
                return div;
            }
            // Listener.
            function printSites() {
                 
                
                var site = autocomplete.getSite();
                var latitude = site.location.lat;
                var longitude = site.location.lng;
                var country_name = site.name;
                
                var results = "Autocomplete Results:\n";
                var str = "Name=" + site.name + "\n"
                    + "SiteID=" + site.siteId + "\n"
                    + "Latitude=" + site.location.lat + "\n"
                    + "Longitude=" + site.location.lng + "\n"
                    + "Address=" + site.formatAddress + "\n";

                    
                results = results + str;

                $('#latitude').val(latitude);
                $('#longitude').val(longitude);
                $('#countryTo').val(country_name);
                $('#country_name').val(country_name);
                console.log(latitude, longitude);
                /*console.log(results);*/
            }
            
        }
        function getLocation()
        {

        }
    </script> 

     
    @elseif(env('APP_ACTIVE_MAP')=='google_map')
     

<script>
google.maps.event.addDomListener(window, 'load', initMap);
function initMap() {
var input = document.getElementById('address');
var autocomplete = new google.maps.places.Autocomplete(input);
autocomplete.bindTo('bounds', map);
autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);
}

function getLocation()
 {
//   alert(address);
  var address = $('#address').val();
  // alert(address);
  var geocoder = new google.maps.Geocoder();

  geocoder.geocode( { 'address': address}, function(results, status)
  {


    if (status == google.maps.GeocoderStatus.OK)
    {
      console.log(results[0],"afdcfsa");
      console.log(results[0].address_components);
      const countryObj=results[0].address_components.filter(val=>{
            if(val.types.includes('country')){
                return true
            }
            return false;
      });
     console.log(countryObj,'the country obj');


     var country_name = countryObj[0].long_name;
     console.log(country_name,'name');
      var latitude = results[0].geometry.location.lat();

      var longitude = results[0].geometry.location.lng();
      // alert(latitude +"-----"+ longitude);
     $('#latitude').val(latitude);
     $('#longitude').val(longitude);
     $('#countryTo').val(country_name);
     $('#country_name').val(country_name);
    console.log(latitude, longitude);
    }
  });

}
    </script>

@endif