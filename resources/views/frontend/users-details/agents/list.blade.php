@php
$lang=Request::segment(1);
$loc=Request::segment(3);
$modules = \App\Models\Module::where('location',$loc)->get();
if($loc!='all'){
    $link=$loc;
}else{
    $link='all';
}
@endphp
@if(count($myProperty)>0)
@foreach($myProperty as $rentProperty)
@php
$totaluser = \App\Models\Review::where('is_approved',1)->where('review_type',"2")->where('type_id',$rentProperty->_id)->count();
$ratinggiven1=\App\Models\Review::where('is_approved',1)->where('review_type',"2")->where('type_id',$rentProperty->_id)->sum('average');
$totalrating=@$totaluser*5;
if($totalrating == 0){
    $totalrating=1;
}
$avg=number_format(@$ratinggiven1/@$totalrating*5);

 
$renttype= 'rent';
 if($rentProperty->listing_type=='rent')
 {
    $renttype = 'rent';
 }else if($rentProperty->listing_type=='sale')
 {
    $renttype = 'buy';
 }else if($rentProperty->listing_type=='both')
 {
     $renttype = 'rent';
 }
@endphp
<div class="col-md-3">
    <div class="property-card bg-white rounded">

    <a href="{{url(app()->getLocale().'/property/'.$link.'/'.$renttype.'/'.$rentProperty->_id)}}">
    @if(!isset($rentProperty->files[0]) || $rentProperty->files[0]=='')
        <img class="img-fluid property-img" src="{{asset('/img/no-property.png')}}">
    @else
    @if(file_exists('uploads/properties/'.$rentProperty->files[0]))
    <img class="img-fluid property-img" src="{{url('uploads/properties/'.$rentProperty->files[0])}}">
        @else
        <img class="img-fluid property-img" src="{{asset('/img/no-property.png')}}">
        @endif
    @endif
        <div class="pl-3 pr-3">
        <h5>{{@$rentProperty->house_type}}</h5>
        <h2>{{ Str::limit($rentProperty->property_title, 25, '...') }}</h2>
        <p>{{\Str::limit(ucfirst($rentProperty->location_name), 55)}}, {{$rentProperty->country}}</p>
        <h6>{{@$rentProperty->rent}} AED / yr</h6>
            <ul class="list-inline">
            @if(@$avg==1)
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                @endif
                @if(@$avg==2)
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                @endif
                @if(@$avg==3)
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                @endif
                @if(@$avg==4)
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                @endif
                @if(@$avg==5)
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                @endif
                <li class="white-color text" style="color:black">{{@$avg}}</li>
            </ul>
            <ul class="list-inline red-list mb-4">
            <li class="mr-3"><img class="mr-1" src="{{asset('img/red-bed.png')}}"> {{@$rentProperty->no_of_bedrooms}}</li>
            <li class="mr-3"><img class="mr-1" src="{{asset('img/red-tub.png')}}"> {{@$rentProperty->no_of_bathrooms}}</li>
            <li class="mr-3"><img class="mr-1" src="{{asset('img/red-car.png')}}"> {{@$rentProperty->no_of_open_parking}}</li>
            </ul>
        </div>
        </a>
    </div>
</div>
@endforeach
@else
<div class="no_result_listing" style="display: block; width: 100%;line-height: 4;max-height: 60px;text-align: center;background-color: #d6e9c6;">
    <p style="font-size: 16px;font-weight: 700;">No Result Found</p>
</div>
@endif
