@php
$lang=Request::segment(1);
$loc=Request::segment(3);
$modules = \App\Models\Module::where('location',$loc)->get();
if($loc!='all'){
    $link=$loc;
}else{
    $link='all';
}
 
 
@endphp
@extends('frontend.layouts.home')
@section('title','Agent Details')

  <script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
    <script type="text/javascript">
            var wiriteReviewWidgetId;
            var makeBookingWidgetId;
            
            var CaptchaCallback = function() {
            wiriteReviewWidgetId = grecaptcha.render('g-wiriteReviewWidgetId', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
            makeBookingWidgetId = grecaptcha.render('g-makeBookingWidgetId', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
             
            };
             
    </script>

@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<style>
.nav-pills .nav-link.active, .nav-pills .show>.nav-link {
    color: white;
    border-radius: 0px;
    background-color: #E4002B;
}
a.nav-link {
    color: lightgrey;
}
.nav-pills .nav-link {
    background-color: darkgrey;
    border-radius: 0 !important;
}
.error {
    color: red !important;
}
.agent-round-img{
    height: 93px;
    width: 93px;
    border-radius:50%;
    overflow: hidden;
}
.agent-round-img img{
    width: 100%;
    height: 100%;
    object-fit: cover;
}
</style>
@include('frontend.layouts.default-header')
<section class="body-light-grey p-0" style="margin-top: 74px;">
    <div class="container">
        <div class="row">
            <div class="col-md-8 mt-4">
                <div class="bg-white mb-3 p-3 rounded with-shadow">
                    <div class="row hd-and-details small">
                        <div class="col-md-2">
                            <div class="agent-round-img">
                                @if(!isset($data->image) || $data->image=='')
                                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}">
                                @else
                                    @if(file_exists('uploads/profilePics/'.$data->image))
                                        <img class="circle" src="{{url('uploads/profilePics/'.$data->image)}}">
                                    @else
                                        <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}">
                                    @endif
                                @endif
                            </div>
                        </div>
                        <div class="col-md-4 pl-0">
                            <h3 class="mt-3 mb-1">{{ucfirst($data->name)}}</h3>
                            <h6 class="mb-1">{{$data->country }}  ·  {{ucfirst($data->gender)}}  ·  {{$data->age}} yrs</h6>
                            <ul class="list-inline mb-0">
                              @if(@$avgtotal==1)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @elseif(@$avgtotal==2)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @elseif(@$avgtotal==3)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @elseif(@$avgtotal==4)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @elseif(@$avgtotal==5)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if( @$avgtotal != 0)
                                    <li><span style="color:black">{{@$avgtotal}}</span></li>
                                @endif
                            </ul>
                        </div>
                        <div class="col-md-3 text-center">
                            <h4 class="mt-3">{{$data->saleProperties->count()}}</h4>
                            <span>Property for sale</span>
                        </div>
                        <div class="col-md-3 text-center">
                            <h4 class="mt-3">{{$data->rentProperties->count()}}</h4>
                            <span>Property for rent</span>
                        </div>
                        <div class="col-md-12">
                            <h6 class="mt-2">Languages Known</h6>
                            @php 
                                if(!is_array(@$data['professionals'][0]->languages))
                                    $languages = explode(',', @$data['professionals'][0]->languages);
                                else
                                    $languages = @$data['professionals'][0]->languages;
                                @endphp
                            <?php
                                foreach (@$languages as $key => $value) {
                                    if($value)
                                        echo "<small class='light-blue mr-2 mt-0'>$value</small>";
                                }
                            ?>
                        </div>
                    </div>
                    <div class="row bio">
                        <div class="col-md-12">
                            @if(isset($data->about) && !is_null($data->about) && $data->about != '')
                            <h4 class="mt-4 mb-4">Bio</h4>
                            <p>{{$data->about}}</p>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="bg-white mb-3 p-3 rounded with-shadow">
                    <div class="row rating-and-review">
                        <div class="col-md-12">
                            <h4 class="mt-4 mb-4">Ratings & Reviews
                            @if(!Auth::guest())
                                @if(Auth::user()->id!=$data->_id)
                                     <button class="red-btn enterreview"  onclick="allowReview();" style="float: right;font-size: 15px;"><span style="color:white">Write a Review </span></button></h4> 
                                     
                                @endif
                            @endif
                            <!-- <h6><strong>{{@$avgtotal}} </strong>  Stars out of 5</h6>
                            <ul class="list-inline">
                                @if(@$avgtotal==1)
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @elseif(@$avgtotal==2)
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @elseif(@$avgtotal==3)
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @elseif(@$avgtotal==4)
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @elseif(@$avgtotal==5)
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                            </ul> -->
                        </div>
                    </div>
                    <!-- <div class="row dp-flex align-items-center rtng-review ">
                        <ul class="list-inline ml-3">
                          <li>
                            <span class="ratingsetting rating6 mr-4" onClick="getRating(6,'{{$id}}')">Most Relavant</span>
                          </li>
                          <li>
                            <span class="ratingsetting rating5 mr-4" onClick="getRating(5,'{{$id}}')">5 <img src="{{asset('img/ic_rating_filled.png')}}"></span></li>
                          <li>
                            <span class="ratingsetting rating4 mr-4" onClick="getRating(4,'{{$id}}')">
                                4 
                                <img src="{{asset('img/ic_rating_filled.png')}}">
                            </span>
                          </li>
                          <li>
                            <span class="ratingsetting rating3 mr-4" onClick="getRating(3,'{{$id}}')">
                                3<img src="{{asset('img/ic_rating_filled.png')}}">
                            </span>
                          </li>
                          <li>
                            <span class="ratingsetting rating2 mr-4" onClick="getRating(2,'{{$id}}')">2 <img src="{{asset('img/ic_rating_filled.png')}}"></span></li>
                          <li><span class="ratingsetting rating1" onClick="getRating(1,'{{$id}}')">1 <img src="{{asset('img/ic_rating_filled.png')}}"></span></li>
                        </ul>
                    </div> -->
                <div class="dynamicRating">
                @include('frontend.users-details.agents.ratings')
                </div>
            </div>
            @include('frontend.users-details.agents.right-part')
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-5">
                <h2 style="font-weight: 800;">My Properties</h2>
            </div>
            <div class="container">
              <div class="row">
                  <div class="col-12 col-md-8 col-lg-6 mt-3 mb-4">
                    <ul class="nav nav-pills">
                      <li class="nav-item">
                        <a class="nav-link active" data-toggle="pill" href="#both" role="tab" aria-controls="pills-bath" aria-selected="true" onClick="getFilter('{{$id}}',1)">BOTH</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="pill" href="#rent" role="tab" aria-controls="pills-rent" aria-selected="false" onClick="getFilter('{{$id}}',2)">RENT</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="pill" href="#sell" role="tab" aria-controls="pills-sell" aria-selected="false" onClick="getFilter('{{$id}}',3)">SELL</a>
                      </li>
                    </ul>
                  </div>
              </div>
                <div id="mydivon" class="fadeIn">
                    <div class="row dynamicContent mb-4">
                        @include('frontend.users-details.agents.list')
                    </div>
                </div>
                
                <div class="col-md-12 show-more text-center">
                   <a href="{{url(app()->getLocale().'/rent-property/'.$link.'?country='.$link.'&location=')}}" id="show_more_a" style="display : {{count($myProperty) > 0 ? 'block' : 'none'}}">
                        <button class="red-bordered mt-3 mb-5">SHOW MORE 
                            <i class="fa fa-angle-right"></i>
                        </button>
                    </a>
                </div>
               
            </div>
        </div>
    </div>
    @include('frontend.users-details.agents.specialization')
</section>
@include('frontend.layouts.footer')
@endsection

