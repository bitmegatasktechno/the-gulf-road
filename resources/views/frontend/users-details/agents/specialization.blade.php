@if(@$specials!='[]')
<section class="area-of-specialization body-light-grey" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="mb-4">Area of Specialization</h3>
            </div>
            @foreach($specials as $special)
            
            <div class="col-md-4">
                <div class="spcl-box bg-white">
                    <img class="img-fluid dp-block" src="{{asset('img/Dubai_PTV.png')}}" style="width: 351px;">
                    <div class="row">
                        <div class="col-md-12 pr-0">
                            <div class="pl-3">
                            <div class="row">
                                <div class="col-md-7">
                                <h5 style="padding-top: 23px;">{{$special->title}}</h5>
                                <p class="mb-3">{{$special->content}}</p>
                                </div>
                                <div class="col-md-4" style="    background-color: #E4002B;
    padding-top: 24px;
    text-align: center;
    font-size: 25px;
    position: relative;
    left: 2px;
    color: #fff;">
    {{$special->project}} <br>Project
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endif
