@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
<style>

/* Rating Star Widgets Style */
.rating-stars ul {
  list-style-type:none;
  padding:0;

  -moz-user-select:none;
  -webkit-user-select:none;
}
.rating-stars ul > li.star {
  display:inline-block;

}

/* Idle State of the stars */
.rating-stars ul > li.star > i.fa {
  font-size:2.5em; /* Change the size of the stars */
  color:#ccc; /* Color on idle state */
}

/* Hover state of the stars */
.rating-stars ul > li.star.hover > i.fa {
  color:pink;
}

/* Selected state of the stars */
.rating-stars ul > li.star.selected > i.fa {
  color:red;
}
.ratingsetting{
  border: 1px solid;
    border-radius: 33px;
    padding: 5px 11px;
    color: #008489 !important;
}
.select2-container .select2-selection--single{
  padding-top: 4px;
  height: 47px !important;
}
span#select2-phone_code-nd-container{
  font-size: 16px;
}
</style>

      @foreach($ratings as $rating)
      <div class="row mt-5 dp-flex align-items-center testimonial-inner">
        <div class="col-md-1">
        @if(isset($rating->user->image_path))
            <img class="circle" src="{{@$rating->user->image_path}}" style="height: 40px;width: 40px;">
         @else
            <img class="invert-color circle" src="{{asset('img/profile-avtar.png')}}" style="height: 40px;width: 40px;">
         @endif  
        </div>
        <div class="col-md-5">
            <h6 class="mb-0">{{@$rating->user->name}}</h6>
            <ul class="list-inline mb-0">
              @if(@$rating->average==1)
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                @elseif(@$rating->average==2)
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                @elseif(@$rating->average==3)
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                @elseif(@$rating->average==4)
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                @elseif(@$rating->average==5)
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                @endif
            </ul>
        </div>
        <div class="col-md-6 text-right">
            <small>{{ date('d-F,Y', strtotime($rating->create)) }}</small>
        </div>
        <div class="col-md-12">
            <p class="mt-4 ml-2">{{@$rating->review}}</p>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
      </div>
      @endforeach

      <div class="modal fade" id="viewrefund">
        <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
        <div class="modal-header" >
        <h4 class="modal-title">Write a Review</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>


<!-- Modal footer -->
<div class="modal-body text-center p-5">

<input type="hidden" name="review_type" id="review_type" value="7">
<input type="hidden" name="type_id" id="type_id" value="{{@$id}}">
<div class="row">
  <div class="cal-md-6">
    <div style="display:none;">
    <input type="text" id="rating">
  </div>
<label>Rating:</label>
  </div>
  <div class="cal-md-6">
      <div class='rating-stars text-center' style="font-size: 9px;">
        <ul id='stars'>
          <li class='star' title='Poor' data-value='1'>
            <i class='fa fa-star fa-fw'></i>
          </li>
          <li class='star' title='Fair' data-value='2'>
            <i class='fa fa-star fa-fw'></i>
          </li>
          <li class='star' title='Good' data-value='3'>
            <i class='fa fa-star fa-fw'></i>
          </li>
          <li class='star' title='Excellent' data-value='4'>
            <i class='fa fa-star fa-fw'></i>
          </li>
          <li class='star' title='WOW!!!' data-value='5'>
            <i class='fa fa-star fa-fw'></i>
          </li>

        </ul>
      </div>

  </div>

</div>
  <span id="selectstar" style="color:red;display:none;">This field is required</span>

<br>
<div class="row">
<div class="cal-md-12">
<label style="text-align: left;">Comment:</label>
</div>
</div>
<div class="row">
<div class="cal-md-12">
</div> <textarea rows="4" name="review" id="review" required class="form-control"></textarea>
<span id="selectreview" style="color:red;display:none;">This field is required</span>
</div>
<br>

 <div class="row">
            <div class="col-md-12">
                <div class="form-group">
 
        
                  <!--  <div class="g-recaptcha" data-sitekey="{{env('reCAPTCHA_site_key')}}"></div> -->
                <div class="g-recaptcha"  id="g-wiriteReviewWidgetId"></div>
 
 
                </div>
              </div>
         </div>
<button class="red-btn sendreview">Submit</button>

</div>

</div>
</div>
</div>
@if(isset($awards))
  @if($awards!='[]')
    <div class="col-md-12"><h4 class="mt-4 mb-4">Awards</div>
    <ul class="list-inline usersawards">
      @foreach($awards as $award)
      @if(isset($award->awardss->logo) && !is_null($award->awardss->logo) && $award->awardss->logo!='')
        <li style="margin-bottom: 12px;">
        <span class="awardssetting mr-4" style="margin-right:38px !important">
          <img src="{{asset('../uploads/awards/'.@$award->awardss->logo)}}" style="width: 28px;">
          {{strtoupper(@$award->awardss->award)}}</span>
        </li>
      @endif
      @endforeach
    </ul>
  @endif
@endif
</div>
@section('scripts')
@include('frontend.profile.common.js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
      <script type="text/javascript">
$(document).on('click','.sendreview', function(){
        var ajax_url = WEBSITE_URL+"/agent/review";
        var review_type=$("#review_type").val();
        var type_id=$("#type_id").val();
        var rating=$("#rating").val();
        if(rating==''){
          $("#selectstar").show();
              return false;
        }
        var review=$("#review").val();
        if(review==''){
          $("#selectreview").show();
              return false;
        }


        if (grecaptcha.getResponse(wiriteReviewWidgetId) ==""){
              Swal.fire({
                      title: 'Error!',
                      text: "Please Fill Captcha Code First ",
                      icon: 'error',
                      confirmButtonText: 'Ok'
                  })
            return false;
           
          } 
          
        $.ajax({
          url:ajax_url,
          method:"POST",
          data:{
            review_type:review_type,
            type_id:type_id,
            rating:rating,
            review:review,
          },
          headers:{
            'X-CSRF-TOKEN': '{{ csrf_token() }}',
          },
          beforeSend:function(){
	                startLoader();
	            },
	            complete:function(){
	               stopLoader(); 
	            },
          success:function(data){
            stopLoader('.page-content');
            $('#viewrefund').modal('hide');
            if(data.status){
              Swal.fire({
                  title: 'Success!',
                  text: data.msg,
                  icon: 'success',
                  confirmButtonText: 'Ok'
              }).then((result) => {
                window.location.reload();
              });
            }else{
              Swal.fire({
                  title: 'Success!',
                  text: data.msg,
                  icon: 'success',
                  confirmButtonText: 'Ok'
              }).then((result) => {
                window.location.reload();
              });
            }
          }
        });
      });
      $(".enterreview").click(function(){
      $("#viewrefund").modal('show');
      });

      function allowReview()
        {
          $("#viewrefund").modal('show');
        }
        function preventReview()
        {
            Swal.fire({
                          title: 'Error!',
                          text:  'You are not able to make review Now ! Before Elevate Your Profile',
                          icon:  'error',
                          confirmButtonText: 'Ok'
                      })
        }

              $(document).ready(function(){

/* 1. Visualizing things on Hover - See next part for action on click */
$('#stars li').on('mouseover', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

  // Now highlight all the stars that's not after the current hovered star
  $(this).parent().children('li.star').each(function(e){
    if (e < onStar) {
      $(this).addClass('hover');
    }
    else {
      $(this).removeClass('hover');
    }
  });

}).on('mouseout', function(){
  $(this).parent().children('li.star').each(function(e){
    $(this).removeClass('hover');
  });
});


/* 2. Action to perform on click */
$('#stars li').on('click', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently selected
  var stars = $(this).parent().children('li.star');

  for (i = 0; i < stars.length; i++) {
    $(stars[i]).removeClass('selected');
  }

  for (i = 0; i < onStar; i++) {
    $(stars[i]).addClass('selected');
  }

  // JUST RESPONSE (Not needed)
  var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
  var msg = "";
  if (ratingValue > 1) {
      msg = ratingValue;
  }
  else {
      msg = ratingValue;
  }
  responseMessage(msg);

});



});


      function responseMessage(msg) {
      $("#rating").val(msg);
      }
      

function getRating(type,id){
    if(type==6){
        $(".rating6").css('background-color','#E4002B','color','white');
        $(".rating5").css('background-color','white');
        $(".rating4").css('background-color','white');
        $(".rating3").css('background-color','white');
        $(".rating2").css('background-color','white');
        $(".rating1").css('background-color','white');
    }
    if(type==5){
        $(".rating5").css('background-color','#E4002B');
        $(".rating6").css('background-color','white');
        $(".rating4").css('background-color','white');
        $(".rating3").css('background-color','white');
        $(".rating2").css('background-color','white');
        $(".rating1").css('background-color','white');
    }
    if(type==4){
        $(".rating4").css('background-color','#E4002B');
        $(".rating6").css('background-color','white');
        $(".rating5").css('background-color','white');
        $(".rating3").css('background-color','white');
        $(".rating2").css('background-color','white');
        $(".rating1").css('background-color','white');
    }
    if(type==3){
        $(".rating3").css('background-color','#E4002B');
        $(".rating6").css('background-color','white');
        $(".rating4").css('background-color','white');
        $(".rating5").css('background-color','white');
        $(".rating2").css('background-color','white');
        $(".rating1").css('background-color','white');
    }
    if(type==2){
        $(".rating2").css('background-color','#E4002B');
        $(".rating6").css('background-color','white');
        $(".rating4").css('background-color','white');
        $(".rating3").css('background-color','white');
        $(".rating5").css('background-color','white');
        $(".rating1").css('background-color','white');
    }
    if(type==1){
        $(".rating1").css('background-color','#E4002B');
        $(".rating6").css('background-color','white');
        $(".rating4").css('background-color','white');
        $(".rating3").css('background-color','white');
        $(".rating2").css('background-color','white');
        $(".rating5").css('background-color','white');
    }
    $.ajax({
        method:'get',
        url:"{{url(app()->getLocale().'/rating-list-buddy/'.$link)}}",
        data:{
          type:type,
          id:id
        },
        headers:{
          'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        success: function(data) {
          data = data.trim();
        }
    });
}
jQuery.validator.addMethod("validate_email", function(value, element) {
    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
        return true;
    } else {
        return false;
    }
}, "Please enter a valid Email.");
jQuery.validator.addMethod("validate_phone", function(value, element) {
    if (/^[1-9][0-9]*$/.test(value)) {
        return true;
    } else {
        return false;
    }
  }, "Please enter a phone number.");
$(document).ready(function() {
$('form[id="booking1"]').validate({
  rules: {
    name: 'required',
    number: {
        required: true,
        validate_phone:true,
        minlength : 7,
        maxlength : 13,
      },
    startDate: 'required',
    endDate: 'required',
    content: 'required',
    email: {
      email:true,
      required: true,
      validate_email:true,
    },
  },
  messages: {
    name: 'This field is required',
    number:{
        validate_phone : 'Please enter a valid phone number.',
        minlength : 'Please enter a valid phone number.',
        maxlength : 'Please enter a valid phone number.'
      },
    startDate: 'This field is required',
    endDate: 'This field is required',
    content: 'This field is required',
    email:{
        email : 'Must be valid email address',
        validate_email : 'Must be valid email address',
      },
  },
  errorPlacement: function(error, element) {
     if (element.attr("name") == "name" ){
       error.appendTo('#name_error');
     }else if(element.attr("name") == "number"){
       error.appendTo('#number_error');
     }else if(element.attr("name") == 'email'){
       error.appendTo('#email_error');
     }else if(element.attr("name") == 'content'){
       error.appendTo('#content_error');
     }else if(element.attr("name") == 'startDate'){
       error.appendTo('#date_error');
     }else if(element.attr("name") == 'endDate'){
       error.appendTo('#time_error');
     }
   },
  submitHandler: function(form) {

    if (grecaptcha.getResponse(makeInquiryWidgetId) ==""){
        Swal.fire({
                title: 'Error!',
                text: "Please Fill Captcha Code First!",
                icon: 'error',
                confirmButtonText: 'Ok'
            })
      return false;
     
    } 

    var ajax_url = WEBSITE_URL+"/property/buy/send_enquiry";
    var name=$("#name").val();
    var number=$("#number").val();
    var startDate=$("#startDate").val();
    var endDate=$("#endDate").val();
    var content=$("#content").val();
    var email=$("#email").val();
    var type=$("#type").val();
    var type_id=$("#type_id").val();
    var phone_code=$("#phone_code").val();
    $.ajax({
      url:ajax_url,
      method:"POST",
      data:{
        name:name,
        phone:number,
        startDate:startDate,
        endDate:endDate,
        message:content,
        email:email,
        type:type,
        touser:type_id,
        code:phone_code
      },
      headers:{
        'X-CSRF-TOKEN': '{{ csrf_token() }}',
      },
      beforeSend:function(){
              startLoader();
          },
          complete:function(){
             stopLoader(); 
          },
      success:function(data){
        if(data.status){
          Swal.fire({
              icon: 'success',
              title: 'Success!',
              text: data.message,
              confirmButtonText: 'Ok'
          }).then((result) => {
            window.location.reload();
          });
        }else{
          Swal.fire({
              title: 'Success!',
              text: data.message,
              icon: 'success',
              confirmButtonText: 'Ok'
          }).then((result) => {
            window.location.reload();
          });
        }
      }
    });
  }
});
});

$(function () {
  $("#startDate").datetimepicker({
    useCurrent: false,
    format: "L",
    minDate:new Date(),
    defaultDate: new Date(),
    ignoreReadonly: true,
    icons: {
      next: "fa fa-chevron-right",
      previous: "fa fa-chevron-left",
      today: 'todayText',
    }
  });
});
$("#startDate").on("dp.change", function(e) {
  var currentDateTime = new Date(e.date);
  if($('#endDate').data('DateTimePicker')){
    $("#endDate").datetimepicker('destroy');
  }
  if(!isFutureDate(e.date.format('DD/MM/YYYY'))){
    $("#endDate").datetimepicker({
      format: "LT",
      ignoreReadonly: true,
      defaultDate: new Date(),
      // minDate:currentDateTime,
      icons: {
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down"
      }
    });
  }else{
    $("#endDate").datetimepicker({
      format: "LT",
      ignoreReadonly: true,
      defaultDate: new Date(),
      // minDate:currentDateTime,
      icons: {
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down"
      }
    }); 
  }
});
function isFutureDate(dateText) {
  var arrDate = dateText.split("/");
  var today = new Date();
  useDate = new Date(arrDate[2], arrDate[1] - 1, arrDate[0]);
  if (useDate > today) {
      return true;
  } else return false;
}
function isNumber(evt) {
  evt = (evt) ? evt : window.event;
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
  }
  return true;
}
</script>
@endsection
