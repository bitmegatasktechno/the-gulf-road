<style>
.instant-booking span {
    color: #0E0E0E;
    font-size: 16px !important;
}
#startDate .input-group-addon {
    right: 8px;
    cursor: pointer;
    background: #e9ecef;
    padding: 13px 8px 12px 8px;
}
#startDate .input-group-addon {
    right: 8px;
    cursor: pointer;
    background: #e9ecef;
    padding: 13px 8px 12px 8px;
}
#endDate .input-group-addon {
    right: 8px;
    cursor: pointer;
    background: #e9ecef;
    padding: 13px 8px 12px 8px;
}
.separate-dial-code{
    width: 100%;
}
.bootstrap-datetimepicker-widget.usetwentyfour{
    background:#fff;
    display: block;
}
#startDate input::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
 font-size: 12px!important;
}

#startDate input:-ms-input-placeholder { /* Internet Explorer 10-11 */
  font-size: 12px!important;
}

#startDate input::-ms-input-placeholder { /* Microsoft Edge */
  font-size: 12px!important;
}
</style>
<div class="col-md-4 mt-5">
    <div class="bg-white rounded p-3 mb-3 instant-booking contact-agent with-shadow">
        <h2>
            <span>Contact Details</span>
        </h2>
        <p>
          <small>
            <img src="{{asset('img/agent_Call.png')}}">{{$data->countryCode}} {{$data->phone}}
          </small>
        </p>
        <p>
            <small>
                <img src="{{asset('img/agent-whatspp.png')}}">{{$data->countryCode}} {{$data->profiles->whatsapp_number}}
            </small>
        </p>
        <p>
            <small>
                <img src="{{asset('img/agent-email.png')}}">{{$data->email}}
            </small>
        </p>
        @if(\Auth::user())
            <a href="{{url(app()->getLocale().'/user_chat/'.$link.'/'.$data->_id)}}">
        @else
            <a href="javascript:;" onclick="showLoginForm();">
        @endif
            <button class="red-bordered inspection-btn full-width mb-3">
                <img class="mr-2" src="{{asset('img/chat_ic_agent.png')}}">Chat With Buddy
            </button>
        </a>
    </div>
    <div class="bg-white rounded p-3 mb-3 with-shadow instant-booking cntct-agnt">

        <h2 class="d-flex align-items-center justify-content-between">
            <span>Book Consultation</span> 
            <button type="button" class="red-btn rounded" data-toggle="modal" data-target="#myModal" style="font-size: 15px">
              Check Slots
            </button>
        </h2>
        <h1 class="theme-color">{{@$data->charge_currency??'AED'}} {{@$data->charges}} <small> / {{@$data->charge_type??'Hourly'}}</small></h1>
        <form name="booking1" id="booking1" method="post">
            <input type="hidden" name="type" id="type" value="buddy">
            <input type="hidden" name="type_id" id="type_id" value="{{$data->_id}}">
            <div class="form-group">
                    <input class="full-width mb-3" type="text" name="name" placeholder="Name" id="name">
                    <div id="name_error" ></div>
            </div>
            <div class="form-group">
                    <div class="row">
                        <div class="col-md-4" style="padding-right: 0px!important;">
                            <select class="full-width " name="country_code" style="height: 48px">
                                @foreach(getAllCodeWithName() as $row)
                                    <option value="{{ $row['dial_code'] }}"> {{$row['dial_code']}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-8" style="padding-left: 0px!important">
                            <input class="full-width " type="text" name="number" id="number" onkeypress="return isNumber(event)" placeholder="Phone number" maxlength="13">
                    <div id="number_error" class="mt-3"></div>
                        </div>
                    </div>
                </div>
            <div class="form-group">
                <input class="full-width mb-3" type="email" name="email" id="email" placeholder="Email address" >
                <div id="email_error"></div>
            </div>
            <div class="form-group">
                <textarea rows="4" placeholder="Note" class="full-width p-2 mb-3" name="content" id="content" required></textarea>
                <div id="content_error"></div>
            </div>
            <div class="row">
                <div class='col-6'>
                    <div class="form-group">
                        <div class='input-group date' id='startDate'>
                           <input type='text' class="form-control" name="startdate" readonly="" />
                           <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                           </span>
                        </div>
                    </div>
                    <div id="date_error"></div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <div class='input-group date' id='endDate'>
                           <input type='text' class="form-control" name="starttime" readonly="" />
                           <span class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                           </span>
                        </div>
                        <div id="time_error"></div>
                    </div>
                </div>
            </div>
            @if(\Auth::user())
            

 
            <div class="col-md-12">
                <div class="form-group">
 
        
                  <!--  <div class="g-recaptcha" data-sitekey="{{env('reCAPTCHA_site_key')}}"></div> -->
                <div class="g-recaptcha"  id="g-makeInquiryWidgetId"></div>
 
 
                </div>
              </div>
         
                <button type="submit" class="red-btn full-width rounded mt-3" autocomplete="off">Book Your Consultation</button>
            @else
                <a href="javascript:;" onclick="showLoginForm();"><button type="button" class="red-btn full-width rounded mt-3" autocomplete="off">Book your consultation</button></a>
            @endif
        </form>
    </div>
</div>



<!-- The Modal -->
<div class="modal fade" id="myModal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Timing and activity</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body professional_information_div">
        <div class="col-md-12 mt-2">
           

                    <div class="days-list mb-12">
                        <table class="table">
                            <thead>
                              <tr>
                                <th>Week Day</th>
                                <th>Start Time</th>
                                <th>End Time</th>
                              </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <img class="mr-2" src="{{asset('img/ic_red_check.png')}}" style="@if(@$data['professionals'][0]->availability['sunday']!=1) filter:grayscale();@endif">

                                        Sunday
                                    </td>
                                    @if(@$data['professionals'][0]->availability['sunday']==1)
                                        <td>{{@$data['professionals'][0]->timing['sunday_start_time']}}</td>
                                        <td>{{@$data['professionals'][0]->timing['sunday_end_time']}}</td>
                                    @else
                                        <td>N/A</td>
                                        <td>N/A</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>
                                        <img class="mr-2" src="{{asset('img/ic_red_check.png')}}" style="@if(@$data['professionals'][0]->availability['monday']!=1) filter:grayscale();@endif">

                                        Monday
                                    </td>
                                    @if(@$data['professionals'][0]->availability['monday']==1)
                                        <td>{{@$data['professionals'][0]->timing['monday_start_time']}}</td>
                                        <td>{{@$data['professionals'][0]->timing['monday_end_time']}}</td>
                                    @else
                                        <td>N/A</td>
                                        <td>N/A</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>
                                        <img class="mr-2" src="{{asset('img/ic_red_check.png')}}" style="@if(@$data['professionals'][0]->availability['tuesday']!=1) filter:grayscale();@endif">
                                        Tuesday</td>
                                    @if(@$data['professionals'][0]->availability['tuesday']==1)
                                        <td>{{@$data['professionals'][0]->timing['tuesday_start_time']}}</td>
                                        <td>{{@$data['professionals'][0]->timing['tuesday_end_time']}}</td>
                                    @else
                                        <td>N/A</td>
                                        <td>N/A</td>
                                    @endif
                                </tr>
                                <tr >
                                    <td>
                                        <img class="mr-2" src="{{asset('img/ic_red_check.png')}}" style="@if(@$data['professionals'][0]->availability['wednesday']!=1) filter:grayscale();@endif">
                                        Wednesday
                                    </td>
                                    @if(@$data['professionals'][0]->availability['wednesday']==1)
                                        <td>{{@$data['professionals'][0]->timing['wednesday_start_time']}}</td>
                                        <td>{{@$data['professionals'][0]->timing['wednesday_end_time']}}</td>
                                    @else
                                        <td>N/A</td>
                                        <td>N/A</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td><img class="mr-2" src="{{asset('img/ic_red_check.png')}}" style="@if(@$data['professionals'][0]->availability['thursday']!=1) filter:grayscale();@endif"> Thursday</td>
                                    @if(@$data['professionals'][0]->availability['thursday']==1)
                                        <td>{{@$data['professionals'][0]->timing['thursday_start_time']}}</td>
                                        <td>{{@$data['professionals'][0]->timing['thursday_end_time']}}</td>
                                    @else
                                        <td>N/A</td>
                                        <td>N/A</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td><img class="mr-2" src="{{asset('img/ic_red_check.png')}}" style="@if(@$data['professionals'][0]->availability['friday']!=1) filter:grayscale();@endif"> Friday</td>
                                    @if(@$data['professionals'][0]->availability['friday']==1)
                                        <td>{{@$data['professionals'][0]->timing['friday_start_time']}}</td>
                                        <td>{{@$data['professionals'][0]->timing['friday_end_time']}}</td>
                                    @else
                                        <td>N/A</td>
                                        <td>N/A</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td><img class="mr-2" src="{{asset('img/ic_red_check.png')}}" style="@if(@$data['professionals'][0]->availability['saturday']!=1) filter:grayscale();@endif"> Saturday</td>
                                    @if(@$data['professionals'][0]->availability['saturday']==1)
                                        <td>{{@$data['professionals'][0]->timing['saturday_start_time']}}</td>
                                        <td>{{@$data['professionals'][0]->timing['saturday_end_time']}}</td>
                                    @else
                                        <td>N/A</td>
                                        <td>N/A</td>
                                    @endif
                                </tr>
                            </tbody>
                          </table>
                    </div>
                </div>
      </div>
    </div>
  </div>
</div>