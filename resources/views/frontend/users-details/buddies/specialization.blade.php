@if(@$specials!='[]')
<section class="area-of-specialization body-light-grey" style="display: none;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="mb-4">Area of Specialization</h3>
                </div>
                @foreach($specials as $special)
               
                <div class="col-md-4">
                    <div class="spcl-box bg-white">
                        <img class="img-fluid dp-block" src="{{url('uploads/speacialization/'.$special->logo)}}">
                        <div class="row">
                            <div class="col-md-12 pr-0">
                                <div class="pl-3 pt-3">
                                    <h5>{{$special->title}}</h5>
                                    <p class="mb-3">{{$special->content}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    @endif
