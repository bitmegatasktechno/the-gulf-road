@php
$lang=Request::segment(1);
$loc=Request::segment(3);
    $modules = \App\Models\Module::where('location',$loc)->get();
    if($loc!='all'){
        $link=$loc;
    }else{
        $link='all';
    }
    $current=date('Y-m-d');

$package="NO";
if(@\Auth::user()->id)
{
    $savedCOSpace   = \App\Models\CoworkspaceProperty::where('user_id',\Auth::user()->id)->count();
    $users          = \App\Models\Transaction::where('user_id',\Auth::user()->id)->whereIn('purchase_type',['user','buddy','agent'])->count();
    if(@$users){
        if(@$users > 0){
            $last_transaction = \App\Models\Transaction::where('user_id',\Auth::user()->id)->whereIn('purchase_type',['user','buddy','agent'])->latest()->first();
            if(@$last_transaction){
                
                $package="YES";
            }else{
                $package="NO";
            }
        }else{
            $package="NO";
        }
    }else{
         $package="NO";
    }
}

@endphp
@extends('frontend.layouts.home')
@section('title','Buddy Details')
<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
    <script type="text/javascript">
            var wiriteReviewWidgetId;
            var makeInquiryWidgetId;
            var CaptchaCallback = function() {
            wiriteReviewWidgetId = grecaptcha.render('g-wiriteReviewWidgetId', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
            makeInquiryWidgetId = grecaptcha.render('g-makeInquiryWidgetId', {'sitekey' : "{{env('reCAPTCHA_site_key')}}"});
             
            };
             
    </script>
@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<link href="{{ asset('admin/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
@include('frontend.layouts.default-header')
<style type="text/css">
.error {
    color: red !important;
}
.buddy-round-img{
    height: 93px;
    width: 93px;
    border-radius:50%;
    overflow: hidden;
}
.buddy-round-img img{
    width: 100%;
    height: 100%;
    object-fit: cover;
}
</style>

    @if(isset($data->name) && !empty($data->name))
    <section class="body-light-grey p-0" style="margin-top: 74px;">
        <div class="container">
            <div class="row">
                <div class="col-md-8 mt-5">
                    <div class="bg-white mb-3 p-3 rounded with-shadow">
                        <div class="row hd-and-details small align-items-center">
                            <div class="col-md-2">
                                <div class="buddy-round-img">
                                    @if(!isset($data->image) || $data->image=='')
                                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}">
                                    @else
                                    @if(file_exists('uploads/profilePics/'.$data->image))
                                    <img class="circle" src="{{url('uploads/profilePics/'.$data->image)}}">
                                    @else
                                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}">
                                    @endif
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-5 pl-0">
                                <h3 class="mt-3 mb-1">{{ucfirst($data->name)}}</h3>
                                <h6 class="mb-1">{{$data->country }}  ·  {{ucfirst($data->gender)}}  ·  {{$data->age}} yrs</h6>
                                <ul class="list-inline mb-0">
                                  @if(@$avgtotal==1)
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    @elseif(@$avgtotal==2)
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    @elseif(@$avgtotal==3)
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    @elseif(@$avgtotal==4)
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    @elseif(@$avgtotal==5)
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    @endif
                                    @if( @$avgtotal != 0)
                                    <li><span style="color:black">{{@$avgtotal}}</span></li>
                                    @endif
                                </ul>
                            </div>
                            <div class="col-md-5 text-right">
                                <div class="connections">
                                    
                                    @if(count($connections))
                                    <strong>{{count($connections)}}
                                        <!-- <i class="fas fa-users ml-1"></i> -->
                                    
                                    </strong>
                                    <span class="d-block">Total Connections</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h6 class="mt-2">LANGUAGE KNOWN</h6>
                                @php 
                                    if(!is_array($data['professionals'][0]->languages))
                                        $languages = explode(',', @$data['professionals'][0]->languages);
                                    else
                                        $languages = @$data['professionals'][0]->languages;
                                    @endphp
                                <?php
                                    foreach (@$languages as $key => $value) {
                                        if($value)
                                            echo "<small class='light-blue'>$value</small>";
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="row bio">
                            <div class="col-md-12 ">
                                @if(isset($data->about) && !is_null($data->about) && $data->about != '')
                                <h4 class="mt-4 mb-4">Bio</h4>
                                <!-- <small class="">Bio</small> -->
                                <p>{{ucfirst($data->about)}}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="bg-white mb-3 p-3 rounded with-shadow">
                    <div class="row rating-and-review">
                        <div class="col-md-12">
                            <h4 class="mt-4 mb-4">Ratings & Reviews
                            @if(!Auth::guest())
                            @if(Auth::user()->id!=$data->_id)
                            @if($package=="YES")
                            <button class="red-btn enterreview"  onclick="allowReview();" style="float: right;font-size: 15px;"><span style="color:white">Write a Review </span></button></h4>
                            @elseif($package=="NO")
                            <button class="red-btn"   style="float: right;font-size: 15px;" onclick="preventReview();" ><span style="color:white" >Write a Review </span></button></h4>
                            @endif
                            <!-- <button class="red-btn enterreview" style="float: right;font-size: 15px;"><span style="color:white">Write a Review </span></button></h4> -->
                            @endif
                            @endif
                            <h6><strong>{{@$avgtotal}} </strong>  Stars out of 5</h6>
                            <ul class="list-inline">
                              @if(@$avgtotal==1)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @elseif(@$avgtotal==2)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @elseif(@$avgtotal==3)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @elseif(@$avgtotal==4)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @elseif(@$avgtotal==5)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                      <!-- <div class="row dp-flex align-items-center rtng-review ">
                        <ul class="list-inline ml-3">
                          <li><span class="ratingsetting rating6 mr-4" onClick="getRating(6,'{{$id}}')">Most Relavant</span></li>
                          <li><span class="ratingsetting rating5 mr-4" onClick="getRating(5,'{{$id}}')">5 <img src="{{asset('img/ic_rating_filled.png')}}"></span></li>
                          <li><span class="ratingsetting rating4 mr-4" onClick="getRating(4,'{{$id}}')">4 <img src="{{asset('img/ic_rating_filled.png')}}"></span></li>
                          <li><span class="ratingsetting rating3 mr-4" onClick="getRating(3,'{{$id}}')">3 <img src="{{asset('img/ic_rating_filled.png')}}"></span></li>
                          <li><span class="ratingsetting rating2 mr-4" onClick="getRating(2,'{{$id}}')">2 <img src="{{asset('img/ic_rating_filled.png')}}"></span></li>
                          <li><span class="ratingsetting rating1" onClick="getRating(1,'{{$id}}')">1 <img src="{{asset('img/ic_rating_filled.png')}}"></span></li>
                        </ul>
                      </div> -->
                    <div class="dynamicRating">@include('frontend.users-details.buddies.ratings')</div>
                    <div class="bg-white mb-3 p-3 rounded with-shadow edu-details">
                        <h4 class="mb-3 mt-2">Educational Qualification</h4>
                        <div class="row">
                            <div class="col-md-12">
                                @if(@$data['qualifications']->education!=null)
                                    @foreach(@$data['qualifications']->education as $row)
                                        <p class="mt-4 ft-16"><img class="mr-2" src="{{asset('img/ic_red_check.png')}}">{{strtoupper($row['course_name'])}} FROM {{strtoupper($row['university_name'])}}</p>
                                    @endforeach
                                @endif
                            </div>
                            <div class="col-md-12">
                                <h4 class="mb-3 mt-3">Hobbies</h4>
                                <div class="row">
                                    @if(@$data['qualifications']->hobbies!=null)
                                        @foreach(@$data['qualifications']->hobbies as $row)
                                            <div class="col-md-4">
                                                 <p><strong class="mr-2">·</strong>{{strtoupper($row)}}</p>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            @php $locations = explode(",",$data->area_of_spec); @endphp
                            @if(count($locations) > 0)
                            <div class="col-md-12">
                                <h4 class="mb-3 mt-3">Area of Specialization</h4>
                                <div class="row">
                                    @foreach($locations as $location)
                                        <div class="col-md-4">
                                             <p><strong class="mr-2">·</strong>{{strtoupper($location)}}</p>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            @endif
                            <div class="col-md-12">
                                <h4 class="mb-3 mt-3">Location</h4>
                                <p><i class="mr-2 fa fa-map-marker"></i>
                                    {{$data->full_address}}
                                </p>
                            </div>
                            <div class="col-md-12">
                            @include('frontend.users-details.buddies.map')
                        </div>
                        </div>
                    </div>
                </div>
                @include('frontend.users-details.buddies.right-part')
            </div>
        </div>
    </section>
    @include('frontend.users-details.buddies.specialization')

@else
 <section class="body-light-grey p-0" style="margin-top: 74px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mt-5">
                    <div class="bg-white mb-3 p-3 rounded with-shadow">
                        
                            
    <p class="error text-danger">Buddies is no longer available..</p>
            
            
            </div>
            </div>
            </div>
            </div>
        </section>
@endif

    @include('frontend.layouts.footer')
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
	<script type="text/javascript">
var pathArray = window.location.pathname.split( '/' );
var segment_1 = pathArray[1];
var segment_2 = pathArray[2];
var segment_3 = pathArray[3];
var segment_4 = pathArray[4];
		function toggleDiv(divid)
		{
			varon = divid + 'on';
			varoff = divid + 'off';

			if(document.getElementById(varon).style.display == 'none')
			{
				document.getElementById(varon).style.display = 'block';
				document.getElementById(varoff).style.display = 'none';
			}
			else
			{ 
				document.getElementById(varoff).style.display = 'block';
				document.getElementById(varon).style.display = 'none';
			}
		} 

		var filter_data = $("form[name=filter_listing]").serialize();
    	var jqxhr = {abort: function () {  }};

		// save user personal informations.
		$(document).on("click", ".pagination li a", function (e){
            e.preventDefault();
            startLoader('.page-content');
            var url = $(this).attr('href');
            var page = url.split('page=')[1];      
            loadListings(url, 'filter_listing');
        });

        $(document).on("click", ".reset_filter", function (e){
            e.preventDefault();
            $("form[name='filter_listing']")[0].reset();
            $("option:selected").prop("selected", false);
            $("form[name='filter_listing']").find('input').val('');
            var url = "{{url(app()->getLocale().'/filter-property/'.$link)}}";
            loadListings(url, 'filter_listing');
        });
        $(document).on('keyup', '.location_name', function () {
            if($(this).val().length > 3)
            {
                var url = "{{url(app()->getLocale().'/filter-property/'.$link)}}";
            	loadListings(url, 'filter_listing');
            }
            if($(this).val().length == 0)
            {
                var url = "{{url(app()->getLocale().'/filter-property/'.$link)}}";
            	loadListings(url, 'filter_listing');
            }
        });

        $(document).on('change','.house_type_filter,.bedrooms_filter,.bathrooms_filter,.price_filter,.country_filter,.rent_frequenecy_filter', function(){
        	var url = "{{url(app()->getLocale().'/filter-property/'.$link)}}";
        	loadListings(url, 'filter_listing');
        });

        function loadListings(url,filter_form_name){
            
            var filtering = $("form[name=filter_listing]").serialize();
            //abort previous ajax request if any
            jqxhr.abort();
            jqxhr =$.ajax({
                type : 'get',
                url : url,
                data : {filtering,
                    segment_3:segment_3},
                dataType : 'html',
                beforeSend:function(){
                    startLoader('body');
                },
                success : function(data){
                    console.log(data);
                    // data = data.trim();
                    // $(".dynamicContent").empty().html(data);
                
                        window.location = "{{url(app()->getLocale().'/buy-property')}}?"+$("form[name=filter_listing]").serialize();
                    
                },
                error : function(response){
                    stopLoader('body');
                },
                complete:function(){
                    stopLoader('body');
                }
            });
        }

        function getFilter(id,type){
    $.ajax({
        method:'get',
              url:"{{url(app()->getLocale().'/tab-property/'.$link)}}",
              data:{
                id:id,
                type:type
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function(data) {
                data = data.trim();
                $(".dynamicContent").empty().html(data);
              }
    });
}

function getRating(type,id){
    if(type==6){
                      $(".rating6").css('background-color','#E4002B');
                     
                      $(".rating5").css('background-color','white');
                      $(".rating4").css('background-color','white');
                      $(".rating3").css('background-color','white');
                      $(".rating2").css('background-color','white');
                      $(".rating1").css('background-color','white');
                  }
                  if(type==5){
                      $(".rating5").css('background-color','#E4002B');
                      $(".rating6").css('background-color','white');
                      $(".rating4").css('background-color','white');
                      $(".rating3").css('background-color','white');
                      $(".rating2").css('background-color','white');
                      $(".rating1").css('background-color','white');
                  }
                  if(type==4){
                      $(".rating4").css('background-color','#E4002B');
                      $(".rating6").css('background-color','white');
                      $(".rating5").css('background-color','white');
                      $(".rating3").css('background-color','white');
                      $(".rating2").css('background-color','white');
                      $(".rating1").css('background-color','white');
                  }
                  if(type==3){
                      
                      $(".rating3").css('background-color','#E4002B');
                      $(".rating6").css('background-color','white');
                      $(".rating4").css('background-color','white');
                      $(".rating5").css('background-color','white');
                      $(".rating2").css('background-color','white');
                      $(".rating1").css('background-color','white');
                  }
                  if(type==2){
                      $(".rating2").css('background-color','#E4002B');
                      $(".rating6").css('background-color','white');
                      $(".rating4").css('background-color','white');
                      $(".rating3").css('background-color','white');
                      $(".rating5").css('background-color','white');
                      $(".rating1").css('background-color','white');
                  }
                  if(type==1){
                      $(".rating1").css('background-color','#E4002B');
                      $(".rating6").css('background-color','white');
                      $(".rating4").css('background-color','white');
                      $(".rating3").css('background-color','white');
                      $(".rating2").css('background-color','white');
                      $(".rating5").css('background-color','white');
                  }
    $.ajax({
        method:'get',
              url:"{{url(app()->getLocale().'/rating-list-buddy/'.$link)}}",
              data:{
                type:type,
                id:id
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success: function(data) {
                
                data = data.trim();
                $(".dynamicRating").empty().html(data);
              }
    });
}

$(".enterreview").click(function(){
      $("#viewrefund").modal('show');
      });



        $(document).ready(function(){

/* 1. Visualizing things on Hover - See next part for action on click */
$('#stars li').on('mouseover', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

  // Now highlight all the stars that's not after the current hovered star
  $(this).parent().children('li.star').each(function(e){
    if (e < onStar) {
      $(this).addClass('hover');
    }
    else {
      $(this).removeClass('hover');
    }
  });

}).on('mouseout', function(){
  $(this).parent().children('li.star').each(function(e){
    $(this).removeClass('hover');
  });
});


/* 2. Action to perform on click */
$('#stars li').on('click', function(){
  var onStar = parseInt($(this).data('value'), 10); // The star currently selected
  var stars = $(this).parent().children('li.star');

  for (i = 0; i < stars.length; i++) {
    $(stars[i]).removeClass('selected');
  }

  for (i = 0; i < onStar; i++) {
    $(stars[i]).addClass('selected');
  }

  // JUST RESPONSE (Not needed)
  var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
  var msg = "";
  if (ratingValue > 1) {
      msg = ratingValue;
  }
  else {
      msg = ratingValue;
  }
  responseMessage(msg);

});



});

function responseMessage(msg) {
  $("#rating").val(msg);
}

$(document).on('click','.sendreview', function(){
    var ajax_url = WEBSITE_URL+"/agent/review";
    var review_type=$("#review_type").val();
    var type_id=$("#type_id").val();
    var rating=$("#rating").val();
    var review=$("#review").val();
    $.ajax({
      url:ajax_url,
      method:"POST",
      data:{
        review_type:review_type,
        type_id:type_id,
        rating:rating,
        review:review,
      },
      headers:{
        'X-CSRF-TOKEN': '{{ csrf_token() }}',
      },
      success:function(data){
        stopLoader('.page-content');
        $('#viewrefund').modal('hide');
        Swal.fire({
          title: 'Success!',
          text: data.msg,
          icon: 'success',
          confirmButtonText: 'Ok'
        }).then((result) => {
        window.location.reload();
        });
      }
    });
});


      $(document).on('click','.bookuser', function(){
        var ajax_url = WEBSITE_URL+"/agent/book";
        var name=$("#name").val();
        var email=$("#email").val();
        var number=$("#number").val();
        var startDate=$("#startDate").val();
        var user_id=$("#user_id").val();
        var endDate=$("#endDate").val();
        $.ajax({
          url:ajax_url,
          method:"POST",
          data:{
            name:name,
            email:email,
            number:number,
            startDate:startDate,
            endDate:endDate,
            user_id:user_id
          },
          headers:{
            'X-CSRF-TOKEN': '{{ csrf_token() }}',
          },
          success:function(data){
            stopLoader('.page-content');
            if(data.status){
              Swal.fire({
                  title: 'Success!',
                  text: data.msg,
                  icon: 'success',
                  confirmButtonText: 'Ok'
              }).then((result) => {
                window.location.reload();
              });
            }else{
              Swal.fire({
                  title: 'Success!',
                  text: data.msg,
                  icon: 'success',
                  confirmButtonText: 'Ok'
              }).then((result) => {
                window.location.reload();
              });
            }
            // alert(data.msg);
          }
        });
      });

      $(function () {
        $("#startDate").datetimepicker({
    format: "L",
    minDate:new Date(),
    showTodayButton: true,
    icons: {
      today: 'todayText',
      next: "fa fa-chevron-right",
      previous: "fa fa-chevron-left",
    }
  });
  $("#endDate").datetimepicker({
    format: "L",
    minDate:0,
    showTodayButton: true,
    icons: {
      today: 'todayText',
      next: "fa fa-chevron-right",
      previous: "fa fa-chevron-left",
    }
  });
    });
  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
  }
</script>
@endsection