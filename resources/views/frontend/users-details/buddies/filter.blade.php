<style>
.first-two-box li:nth-child(6){
    width: 10.4% !important;
}
.error{
    color:red !important;
    font-size:13px !important;
}
    </style>
<section class="top-searh-bar">
    <div class="container">
        <div class="row">
            <div class="col-md-12 first-two-box">
                <form name="filter_listing" id="filter_listing">
                <input type="hidden" name="user_id" value="{{@$id}}">
                    <ul class="list-inline mb-0">
                        <li class="mb-2">
                            <div class="search-box">
                                <input class="full-width location_name" type="text" placeholder="“Search properties by location”" name="location" autocomplete="off" value="{{isset($_GET['location']) ? $_GET['location']: ''}}">
                                <img class="search-icon" src="{{asset('img/Search.png')}}">
                            </div>
                        </li>
                        

                        <li class="mb-2">
                            @php
                                $houseUrl = '';
                                if(isset($_GET['house_type'])){
                                    $houseUrl = $_GET['house_type'];
                                }
                            @endphp
                            <select name="house_type" class="house_type_filter">
                                <option value="">Property Type</option>
                                @foreach(getAllHouseTypes() as $row)
                                    <option value="{{$row->name}}" @if(strtolower($houseUrl)==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                @endforeach
                            </select>
                        </li>
                        <li class="mb-2">
                        @php
                            $rentUrl = '';
                            if(isset($_GET['rent_frequenecy'])){
                                $rentUrl = $_GET['rent_frequenecy'];
                            }
                        @endphp
                            <select name="rent_frequenecy" class="rent_frequenecy_filter">
                                <option value="">Rent Frequency</option>
                                @foreach(getAllRentFrequency() as $key=>$value)
                                    <option value="{{$key}}" @if($rentUrl==$key) selected @endif>{{ucfirst($value)}}</option>
                                @endforeach
                            </select>
                        </li>
                        <li class="mb-2">
                            <select name="price" class="price_filter">
                                <option value="">Price</option>
                                <option value="asc">Low to High</option>
                                <option value="desc">High to Low</option>
                            </select>
                        </li>
                        <li class="mb-2">
                            @php
                                $bedUrl = '';
                                if(isset($_GET['bedrooms'])){
                                    $bedUrl = $_GET['bedrooms'];
                                }
                            @endphp
                            <select name="bedrooms" class="bedrooms_filter">
                                <option value="">Beds</option>
                                @foreach(range(1,10) as $row)
                                    <option value="{{$row}}" @if($bedUrl==$row) selected @endif>{{$row}}</option>
                                @endforeach
                            </select>
                        </li>
                        

                        
                        

                        
                        <li class="mb-2">
                            <select name="more" class="more_filter">
                                <option value="">More Filters</option>
                                
                            </select>
                        </li>
                    </ul>
                </form>
            </div>
        </div>
    </div>
</section>


