@if(count($myProperty)>0)
@foreach($myProperty as $rentProperty)
                <div class="col-md-3">
                    <div class="property-card bg-white rounded">
                    <a href="{{url(app()->getLocale().'/property/'.$link.'/rent/'.$rentProperty->_id)}}">
                    @if(!isset($rentProperty->files[0]) || $rentProperty->files[0]=='')
                        <img class="img-fluid property-img" src="{{asset('/img/no-property.png')}}">
                    @else
                    @if(file_exists('uploads/properties/'.$rentProperty->files[0]))
                    <img class="img-fluid property-img" src="{{url('uploads/properties/'.$rentProperty->files[0])}}">
                        @else
                        <img class="img-fluid property-img" src="{{asset('/img/no-property.png')}}">
                        @endif
                        
                    @endif
                        <div class="pl-3 pr-3">
                        <h5>{{@$rentProperty->house_type}}</h5>
                        <h2>{{@$rentProperty->property_title}}</h2>
                        <p>{{@$rentProperty->house_number}}, {{@$rentProperty->landmark}}, {{@$rentProperty->country}}</p>
                        <h6>{{@$rentProperty->rent}} AED / yr</h6>
                            <ul class="list-inline">
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li>413</li>
                            </ul>
                            <ul class="list-inline red-list mb-4">
                            <li class="mr-3"><img class="mr-1" src="{{asset('img/red-bed.png')}}"> {{@$rentProperty->no_of_bedrooms}}</li>
                            <li class="mr-3"><img class="mr-1" src="{{asset('img/red-tub.png')}}"> {{@$rentProperty->no_of_bathrooms}}</li>
                            <li class="mr-3"><img class="mr-1" src="{{asset('img/red-car.png')}}"> {{@$rentProperty->no_of_open_parking}}</li>
                            </ul>
                        </div>
                        </a>
                    </div>
                </div>
                @endforeach
                @else
    <div class="" style="display: block;
        max-height: 60px;
        text-align: center;
        background-color: #d6e9c6;
        line-height: 4;
        width: 100%;
        ">
        <p style="font-size: 16px;font-weight: 700;">No Result Found</p>
    </div>
@endif
