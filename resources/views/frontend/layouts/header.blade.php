<link rel="shortcut icon" href="{{ asset('img/ic_favi.png') }}">
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<style>
.goog-logo-link {
   display:none !important;
}

.goog-te-gadget {
   color: transparent !important;
}

.goog-te-gadget .goog-te-combo {
   /* color: black !important; */
   margin: 0px 0!important;
}
.goog-te-combo option {
    
    background: white!important;
    color: black!important;
}
.goog-te-combo option:hover {
    
    background: #E4072B!important;
}
</style>
    <style>
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>

<!-- Header Start -->

@php
$lang=Request::segment(1);
$loc=Request::segment(3);
if($loc == 'ksa' || $loc == 'uae' )
{
   $loc21 = strtoupper($loc);
}else 
{
    $loc21 = ucfirst($loc);
}
    $modules = \App\Models\Module::where('location',$loc21)->get();
    if($loc!='all'){
        $link=$loc;
    }else{
        $link='all';
    }
@endphp
@if(\Auth::user())
    @php
        $fav = \App\Models\Favourate::where('user_id',\Auth::user()->id)->count();
        
        if($fav > 0){
            $favi = 'red';
        }else{
            $favi = 'transparent';
        }
    @endphp
    @endif

    @if(\Auth::user())
    @php
        $not = \App\Models\Notification::where('receiver_id',\Auth::user()->id)->where('read',0)->count();
        
        if($not > 0){
            $noti = 'red';
        }else{
            $noti = 'transparent';
        }
    @endphp
@endif
<style>
    a.goog-logo-link{
        display: none;
    }
@media only screen and (min-device-width: 800px) and (max-device-width: 1280px) {

    .login-menu a {
    height: 40px;
    width: 80px;
    line-height: 36px;
}
.register-menu a {
    height: 40px;
    color: #fff;
    width: 86px;
    line-height: 38px;
}
.list-property {
    font-size: 12px;
    border: none;
    min-width: 135px;
    padding: 2px;
}
}
</style>
 
<div id="top-header-bar" class="fixed-top">
    <section class="top-header homepage">
        <div class="container">
            <div class="row">
                <div class="offset-md-1 col-md-10 text-right">
                    <ul class="list-inline mb-0 p-2">
                        <li>
                            <a href="javascript:;" onClick="showHomeLoanForm()">
                                <button class="home-loans mr-3">Home Loans</button>
                            </a>
                        </li>
                        <li>
                            <a href="{{url(app()->getLocale().'/package/'.$loc)}}">
                                <button class="pricing-special mr-2">Pricing Special Offers</button>
                            </a>
                        </li>
                        <li class="nav-item social">
                            <a class="fb-xfbml-parse-ignore nav-link" target="_blank" href="https://www.facebook.com/The-Gulf-Road-108377097722424/">
                                <i class="fa fa-facebook-official"></i>
                            </a>
                        </li>
                        <li class="nav-item social">
                            <a class="fb-xfbml-parse-ignore nav-link" target="_blank" href="https://www.linkedin.com/in/thegulfroad-tgr-01122a1b8/">
                                <i class="fa fa-linkedin-square"></i>
                            </a>
                        </li>
                      <!--   <li class="nav-item social">
                            <a class="fb-xfbml-parse-ignore nav-link" target="_blank" href="https://google.com">
                            <i class="fa fa-google"></i>
                            </a>
                        </li> -->
                        <li class="nav-item social">
                            <a class="fb-xfbml-parse-ignore nav-link" target="_blank" href="https://twitter.com/thegulfroad">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li class="nav-item social">
                            <a class="fb-xfbml-parse-ignore nav-link" target="_blank" href="https://web.whatsapp.com/">
                                <i class="fa fa-whatsapp"></i>
                            </a>
                        </li>
                        <li class="nav-item social">
                            <a  title="Download App" class="fb-xfbml-parse-ignore nav-link" download="thegulfroad.apk" target="_blank" href="{{url('uploads/app/XQC3ir4H0nDI9TaJgdPL.apk')}}">
                                <i class="fa fa-android"></i>
                            </a>
                            
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <header class="header small-device-bg homepage">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light bg-transparent p-2">
               
                <a class="navbar-brand" href="{{url(app()->getLocale().'/')}}">
                    <img src="{{asset('img/logo.svg')}}" >
                    <span class="text-white navbar-brand ">{{ucfirst(Request::segment(3))}}</span>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                            @guest
                                <a href="javascript:;" class="nav-link" onclick="showLoginForm();">
                            @endguest
                            @auth
                                <a class="nav-link" onclick="checkPhoneNumber('{{Auth::user()->phone}}','{{Auth::user()->email}}','{{Auth::user()->name}}','{{Auth::user()->country}}',this)" data-href="{{url(app()->getLocale().'/listproperty/'.$link)}}" data-profile="{{url(app()->getLocale().'/profile/'.$link)}}">
                            @endauth
                                <button class="list-property"><i class="mr-2"><img src="{{asset('img/list-property.png')}}"></i>List Your Property</button></a>
                            </li>
                            @guest
                                <li class="nav-item">
                                    <div id="google_translate_element2" style="width: 147px;"></div>
                                </li>
                                <li class="nav-item login-menu">
                                    <a href="javascript:;" onclick="showLoginForm();">Log in</a>
                                </li>
                                <li class="nav-item register-menu">
                                    <a href="javascript:;" onclick="showRegisterForm();">Register</a>
                                </li>
                            @endguest
                            @auth
                            <li class="nav-item">
                                <a class="nav-link white-color" href="{{url(app()->getLocale().'/rent-property/'.$link)}}">Explore</a>
                            </li>
                            <li class="nav-item">
                            <div id="google_translate_element2" style="width: 147px;"></div>
                            </li>
                            <li class="nav-item">
                             @if(\Auth::user())
                                @if(@$noti=='red')
                                <a class="nav-link" href="{{url(app()->getLocale().'/notifications/'.$link)}}"><img class="" src="{{asset('img/notification1.png')}}" style="height: 26px;width: 26px;"></a>
                                @else
                                <a class="nav-link" href="{{url(app()->getLocale().'/notifications/'.$link)}}"><i class="fa fa-bell-o" aria-hidden="true" style="color: white !important;font-size: 21px;margin-top: 7px;"></i></a>
                                @endif
                                @else
                                <a href="javascript:;" onclick="showLoginForm();"><i class="fa fa-bell-o" aria-hidden="true" style="color: white !important;font-size: 21px;margin-top: 7px;"></i></a>
                                @endif
                            </li>
                            <li class="nav-item">
                            @if(\Auth::user())
                                @if(@$favi=='red')
                                <a class="nav-link" href="{{url(app()->getLocale().'/myfav/'.$link)}}"><i class="fa fa-heart" style="font-size: 24px;position: absolute;color: red;top: 13px !important;right: 100px !important;padding-top: 2px !important;padding-left: 10px !important;"></i></a>
                                @else
                                <a class="nav-link" href="{{url(app()->getLocale().'/myfav/'.$link)}}"><i class="fa fa-heart-o" style="font-size: 24px;position: absolute;color: red;top: 13px !important;right: 100px !important;"></i></a>
                                @endif
                                @else
                                <a href="javascript:;" onclick="showLoginForm();"><i class="fa fa-heart-o" style="font-size: 24px;position: absolute;color: red;top: 13px !important;right: 86px !important;"></i></a>
                                @endif
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="javascipt:;" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                     
                                    @if(isset(Auth::user()->image_path))
                                        @if(Auth::user()->image_path =='/assets/img/avatar.png')
                                        <img class="circle" src="{{url(Auth::user()->image_path)}}" style="height: 40px;width: 40px;">
                                        @else
                                        <img class="circle" src="{{Auth::user()->image_path}}" style="height: 40px;width: 40px;">
                                         @endif
                                    @else
                                        <img class="invert-color circle" src="{{asset('img/profile-avtar.png')}}" style="height: 40px;width: 40px;">
                                    @endif  
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="{{url(app()->getLocale().'/profile/'.$link)}}">Profile</a>
                                   <a class="dropdown-item logout-btn-submit" href="javascript:;" >Logout</a>
                                </div>
                            </li>
                        @endauth
                    </ul>
                </div>
            </nav>
        </div>
    </header>
</div>
<script type="text/javascript">  
	function googleTranslateElementInit() {  
		new google.translate.TranslateElement( 
				{pageLanguage: 'en', includedLanguages : 'en,zh-CN,ph,fr,ar'},  
				'google_translate_element2' 
		);  
    }
    function checkPhoneNumber(phone,email,name,country,obj)
    {
        if(phone == "" || email == "" || name == "" || country == "")
        {
            Swal.fire({
                title: 'Incomplete Profile!',
                text: "Please complete your profile with personal info Mobile, Email, Name, Country before list your property.",
                icon: 'info',
                confirmButtonText: 'Ok',
                showCancelButton: true,
                focusConfirm: false,
            }).then((result) => {
                if(result.value){
                    window.location = $(obj).data('profile');
                }
            });
        }else{
            window.location = $(obj).data('href');
        }
    }


 
</script> 
 

				
			
