@php
$lang=Request::segment(1);
$loc=Request::segment(3);
if($loc == 'ksa' || $loc == 'uae' )
{
   $loc21 = strtoupper($loc);
}else 
{
    $loc21 = ucfirst($loc);
}
 $type=Request::segment(4);
$modules = \App\Models\Module::where('location', $loc21)->get();
if($loc!='all'){
    $link=$loc;
}else{
    $link='all';
}
@endphp
 
 
<footer>
    <div class="container">
        <div class="row">
            @if(@$_GET['popup'] !=='true') 
            <div class="col-md-3">
               <a href="{{url(app()->getLocale().'/')}}"> 
                <h4>
                    <img src="{{asset('img/logo.svg')}}">
                </h4>
               </a>
               <p class="white-color" style="text-align: justify;">
                    <i>The Gulf Road is a leading real estate marketplace that let the listers and finders buy, sell or rent within their budget and as per their choice.  You can choose an agent, find a co-working space, explore and be guided by the buddies of your choice.<br>When it comes to finding a home, we are committed to helping you discover a place that you will love.</i>
               </p>
                <ul class="list-inline">
                    <li class="mr-2">
                        <a target="_blank" href="https://www.facebook.com/The-Gulf-Road-108377097722424/">
                            <i class="fa fa-facebook-official"></i>
                        </a>
                    </li>
                    <li class="mr-2">
                        <a target="_blank"  href="https://www.linkedin.com/in/thegulfroad-tgr-01122a1b8/">
                            <i class="fa fa-linkedin-square"></i>
                        </a>
                    </li>
                    <li class="mr-2">
                        <a target="_blank"  href="https://www.google.com">
                            <i class="fa fa-google"></i>
                        </a>
                    </li>
                    <li class="mr-2">
                        <a  target="_blank" href="https://twitter.com/thegulfroad">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a target="_blank" href="https://web.whatsapp.com/">
                            <i class="fa fa-whatsapp"></i>
                        </a>
                    </li>
                </ul>
                <p>
                    <i class="mr-2">
                        <img  style="width: 30px;" src="{{asset('img/kw-flag.webp')}}">
                    </i><a href="tel:+971547920141">+971 54 7920141</a>
                </p>
                <p>
                    <i class="mr-2">
                        <img style="width: 30px;"  src="{{asset('img/kw-flag.webp')}}">
                    </i><a href="tel:+971553076620">+971 55 307 6620</a>
                </p>
                <p>
                    <i class="mr-2">
                        <img style="width: 30px;"  src="{{asset('img/uk-flag.webp')}}">
                    </i><a href="tel:+447436596523">+44 7436 596 523</a>
                </p>
                <p>
                    <i class="mr-2">
                    <img src="{{asset('img/mail_white_agent_ic.png')}}"></i>
                   <a href="mailto:info@thegulfroad.com">info@thegulfroad.com</a> 
                </p>
                @if(Auth::user()) 
                    <a href="{{url(app()->getLocale().'/subscribe')}}"> 
                @else
                    <a href="javascript:;" onClick="showSubscriptionForm()">
                @endif


                    <button class="subscribe full-width">
                        <i class="fa fa-bell mr-2"></i>Subscribe
                    </button>


                </a>

                <br>
                

                    
            </div>
            <div class="col-md-2">
                <h4 class="white-color mt-1">Company</h4>
                <ul class="list-inline">
                    <li class="dp-block full-width"><a target="_BLANK" href="{{url(app()->getLocale().'/faq/'.$link)}}">FAQ's</a></li>
                    <li class="dp-block full-width"><a href="{{url(app()->getLocale().'/about/'.$link)}}">About Us</a></li>
                    <li class="dp-block full-width"><a href="{{url(app()->getLocale().'/tearms/'.$link)}}">Terms & Conditions</a></li>
                    <li class="dp-block full-width"><a href="{{url(app()->getLocale().'/privacy/'.$link)}}">Privacy Policy</a></li>
                    <li class="dp-block full-width"><a href="{{url(app()->getLocale().'/contact/'.$link)}}">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-md-2">
                <h4 class="white-color mt-1">Explore</h4>
                <ul class="list-inline">
                    @foreach($modules as $module)         
                    @if($module->type=='buy')
                    <li class="dp-block full-width">
                        <a href="{{url(app()->getLocale().'/buy-property/'.$link)}}">Buy Properties</a>
                    </li>
                    @endif
                    @endforeach
                    @foreach($modules as $module)         
                    @if($module->type=='rent')
                        <li class="dp-block full-width">
                            <a href="{{url(app()->getLocale().'/rent-property/'.$link)}}">Properties For Rent</a>
                        </li>
                    @endif
                    @endforeach
                    @foreach($modules as $module)         
                    @if($module->type=='agent')
                    <li class="dp-block full-width">
                        <a href="{{url(app()->getLocale().'/agents/'.$link)}}">Agents</a>
                    </li>
                    @endif
                    @endforeach
                    @foreach($modules as $module)         
                    @if($module->type=='buddy')
                    <li class="dp-block full-width">
                        <a href="{{url(app()->getLocale().'/buddies/'.$link)}}">Buddies</a>
                    </li>
                    @endif
                    @endforeach
                    @foreach($modules as $module)         
                    @if($module->type=='swap')
                    <li class="dp-block full-width">
                        <a href="{{url(app()->getLocale().'/swap/'.$link)}}">Swap</a>
                    </li>
                    @endif
                    @endforeach
                    @foreach($modules as $module)         
                    @if($module->type=='cospace')
                    <li class="dp-block full-width">
                        <a href="{{url(app()->getLocale().'/coworkspace/'.$link)}}">Cospace</a>
                    </li>
                    @endif
                    @endforeach
                </ul>
            </div>
            <div class="col-md-2">
                <h4 class="white-color mt-1">Countries</h4>
                <ul class="list-inline">
                    @foreach(getAllLocations() as $row)
                     <li class="dp-block full-width">
                        <a href="{{url(app()->getLocale().'/home/'.strtolower($row->name))}}">{{$row->name}}</a>
                    </li>
                    @endforeach
                    <!-- <li class="dp-block full-width">
                        <a href="{{url(app()->getLocale().'/home/UAE')}}">UAE</a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="{{url(app()->getLocale().'/home/Kuwait')}}">Kuwait</a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="{{url(app()->getLocale().'/home/KSA')}}">KSA</a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="{{url(app()->getLocale().'/home/Oman')}}">Oman</a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="{{url(app()->getLocale().'/home/Qatar')}}">Qatar</a>
                    </li>
                    <li class="dp-block full-width">
                        <a href="{{url(app()->getLocale().'/home/Bahrain')}}">Bahrain</a>
                    </li> -->
                </ul>
            </div>
            @if(session()->has('message'))
                <input type="hidden" value="{{ session()->get('message') }}" id="success">
            @endif
            <div class="col-md-3">
                <h4 class="white-color mt-1">Newsletter</h4>
                <form action="{{url(app()->getLocale().'/newslatter')}}" method="get" id="newslatter_form" name="newslatter_form">
                    <div class="group-field">
                        <input class="full-width mb-3" type="text" name="name" id="name" placeholder="Name" required>
                        <div id="newslatter_name_error"></div>
                    </div>
                    <div class="group-field">
                        <input class="full-width mb-3" type="email" name="email" id="email" placeholder="Email" required>
                        <div id="newslatter_email_error"></div>
                    </div>
                    <div class="group-field">
                        <button type="sumit" class="red-btn full-width rounded">Submit</button>
                    </div>
                    
                    
                    
                </form>
                 <p class="white-color" style="text-align: justify;">
                         Once scanned, the QR Code on the phone and open Link
                         To download APP
                     </p>
               <div style="background-color: #fff;padding: 5px;height: 170px;width: 170px;" >
                    <style type="text/css">
                    svg path {
                         
                          fill: unset;

                    }
                    path:hover{
                        fill: unset;
                        stroke: unset;
                    }
                </style> 
                    <img style="height: 160px;width: 160px;" src="{{asset('img/the-gulf-road-app-qr-code.png')}}" />
                </div>

            </div>
            @endif
            <div class="col-md-12">
                <small class="copyright">© {{date('Y')}} The Gulf Road. All rights reserved.</small>
            </div>
        </div>
    </div>
</footer>