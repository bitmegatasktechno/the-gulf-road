@php
$lang=Request::segment(1);
$loc=Request::segment(3);
  $modules = \App\Models\Module::where('location',$loc)->get();
  if($loc!='all'){
      $link=$loc;
  }else{
      $link='all';
  }
                    @endphp
					
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" {{ (str_replace('_', '-', app()->getLocale()) == 'zh') || (str_replace('_', '-', app()->getLocale()) == 'ar') ? "dir=rtl" : "dir=ltr" }}>

<head>
	<meta charset="UTF-8">
	<title>@yield('title') | {{ config('app.name', 'Welcome') }}</title>
	<meta content="width=device-width, initial-scale=1.0" name="viewport">
	<meta content="" name="keywords">
	<meta content="" name="description">
	<link rel="shortcut icon" href="{{ asset('img/ic_favi.png') }}">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<!-- Bootstrap CSS File -->
	<link href="{{asset('css/style.css')}}" rel="stylesheet">
	<link href="{{ asset('lib/bootstrap.min.css') }}" rel="stylesheet">
	
  <!-- Meta Tags required for Progressive Web App -->
  <meta name=  "apple-mobile-web-app-status-bar" content="#aa7700">
  <meta name="theme-color" content="black">
 
  <!-- Libraries CSS Files -->
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
	<!-- Main Stylesheet File -->
	


	<link href="{{asset('css/developer.css')}}" rel="stylesheet">
	
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet">
	<link href="{{asset('css/waitMe.min.css')}}" rel="stylesheet">
	<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />

	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript" src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc"></script>
  @if(env('APP_ACTIVE_MAP')=='google_map')
  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&key={{env('GOOGLE_MAP_KEY')}}"></script>
  @endif
<script type="text/javascript">
    var WEBSITE_URL="{{url('/').'/'.app()->getLocale()}}";
 </script>
  <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
  
	@yield('css')
	<style>
		.goog-te-banner-frame{
			display:none !important;
		}


.progress-bar{
  background-color: #1f8489;
}

.progress {
  height: 6px;
}


		</style>
    <style>
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>
  <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/62a451a67b967b117993f8da/1g58ttsv0';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
  
  <script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallbackLoan&render=explicit" async defer></script>
    <script type="text/javascript">

      var homeLoanWidgetId = (homeLoanWidgetId !=='')?(homeLoanWidgetId):'';
      var wiriteReviewWidgetId =  (wiriteReviewWidgetId !=='')?(wiriteReviewWidgetId):'';
      var makeInquiryWidgetId =  (makeInquiryWidgetId !=='')?(makeInquiryWidgetId):'';
      var makeInspectionInquiryWidgetId =  (makeInspectionInquiryWidgetId !=='')?(makeInspectionInquiryWidgetId):'';
      var makeReportFlagWidgetId =  (makeReportFlagWidgetId !=='')?(makeReportFlagWidgetId):'';
    var CaptchaCallbackLoan = function() {
      homeLoanWidgetId = grecaptcha.render('g-homeLoanWidgetId', {'sitekey' : '{{env("reCAPTCHA_site_key")}}' });
    };
     
    </script>


 </head>

<body class="bodyFilter" >
	@yield('content')
	<input type="hidden" id="user_id" value="@if(isset(Auth::user()->id)) {{Auth::user()->id}} @endif">
	@include('frontend.auth.modals.login')
    @include('frontend.auth.modals.register')
	  @include('frontend.auth.modals.skip')
		@include('frontend.auth.modals.forgot-password')
		@include('frontend.auth.modals.subscribe')
		@include('frontend.auth.modals.enquiry')
		@include('frontend.auth.modals.homeloan')
    
    <!-- <div class="alert text-center cookiealert" role="alert" style="position: absolute; !important">
	    <div class="container-fluid">
	        We use cookies to recognize your repeat visits and your logged-in session. Cookies are small files containing information that is transferred to your computer's hard drive that can be read by your browser. They cannot be used to identify you personally, can not carry viruses, or install malware.
	        <br>
	        <button type="button" class="btn btn-default rounded mt-3 mb-3 notAcceptCookie" aria-label="Close" style="background: grey;border: 1px solid grey;color: #ffff;">Cancel</button>
	        <button type="button" class="btn red-btn rounded mt-3 mb-3 acceptcookies" aria-label="Close">Accept</button>
	        
	    </div> -->
    </div>
	
	<script src="{{asset('lib/prefixfree.min.js')}}"></script>
	<script src="{{asset('lib/jquery-3.3.1.slim.min.js')}}"></script>
	<script src="{{ asset('js/script.js') }}"></script>
	<script type="text/javascript" src="{{asset('lib/jquery.min.js')}}"></script>
	<script src="{{ asset('lib/bootstrap.bundle.min.js') }}"></script>
	<script src="{{asset('lib/sweetalert2.js')}}"></script>
	<script src="{{asset('js/waitMe.min.js')}}"></script>
	<script src="{{asset('js/site-cookie.js')}}"></script>
	<script src="{{asset('js/developer.js')}}"></script>
	<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
	<script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/owl.carousel.min.js"></script>
  <script src="{{asset('js/jquery.validate.min.js')}}"></script>

    
	<script type="text/javascript">

    function setCities(obj)
  {
    $('#countryCity').find('option').remove().end().append('<option value="">Select City</option>');
    var country = $(obj).val();
    $.ajax({
      async : false,
      url: "{{url('en/get/cities')}}", //url
      type: 'post', //request method
      data: {'country':country},
      success: function(data) {
        if(data.status){
          var cities = data.cities;
          $.each(cities, function(key, city) {   
            $('#countryCity').append($("<option></option>")
              .attr("value", city.name)
              .text(city.name)); 
      });
        }else{
         
        }
      }
    });
  }

    $(document).ready(function(){
      var check_login= "{{Session::get('check_login')}}";
      var forgot = "{{Session::forget('check_login')}}";
      if(check_login)
      {
        Swal.fire({
              title: 'Your Subscription has Expired!',
              text: "You want to renew it!",
              showCancelButton: true,
              confirmButtonText: 'Yes, renew it!',
              cancelButtonText: 'No, cancel!',
              reverseButtons: true
            }).then((result) => {
            if (result.value) {
              var url = "{{url(app()->getLocale().'/mysubscription/'.$link)}}";
              window.location.replace(url);
            }
            });
      }
    });
    $('.click-nearby').click(function(){
        var type = $(this).attr('data-id');
        if($(this).prop('checked') == true)
        {
            $('.'+type+'_nearby').val('1');
        }else{
            $('.'+type+'_nearby').val('0');
        }
    })
    function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
          return false;
      }
      return true;
    }
    function submit_enquiry_form(){
      var phone_number = $('#enquirytouser #phone').val();

      // $('.commonCls').html(' sdfsdf ');

      var checkVal = 'yes';
      $('#contact_name_error').html('');
      if($('#enquirytouser  #name').val() == ''){
          $('#contact_name_error').html('This field is required');
          checkVal = 'no';
      }
      $('#contact_phone_error').html(' ');
      if($('#enquirytouser #phone').val() == ''){
          $('#contact_phone_error').html('This field is required');
          checkVal = 'no';
      }else if(phone_number.substring(0,1) == '0'){
        $('#contact_phone_error').html('This phone number is not valid');
          checkVal = 'no';
      }
      $('#contact_email_error').html(' ');
      if($('#enquirytouser #email').val() == ''){
          $('#contact_email_error').html('This field is required');
          checkVal = 'no';
      }
      $('#content_content_error').html(' ');
      if($('#enquirytouser #content').val() == ''){
          $('#content_content_error').html('This field is required');
          checkVal = 'no';
      }
      if(checkVal == 'no'){
        return false;
      }


      var type = $("#enquirytouser #type").val();
      var phone = $("#enquirytouser #phone").val();
      var touser = $("#enquirytouser #touser").val();
      var message = $("#enquirytouser #content").val();
      var endDate = $("#enquirytouser #endDate1").val();
      var property = $("#enquirytouser #property").val();
      var startDate = $("#enquirytouser #startDate1").val();
      var ajax_url = WEBSITE_URL+"/property/buy/send_enquiry";
      var code = $("#enquirytouser #phone_code").val();
      var name = $("#enquirytouser #enquirytouser #name").val();
      var email = $("#enquirytouser #enquirytouser #email").val();
      $.ajax({
        url:ajax_url,
        method:"POST",
        data:{
          code: code,
          type: type,
          name: name,
          email: email,
          phone: phone,
          touser: touser,
          message: message,
          endDate: endDate,
          property: property,
          startDate: startDate,
        },
        headers:{'X-CSRF-TOKEN': '{{ csrf_token() }}'},
        beforeSend:function(){
            startLoader();
        },
        complete:function(){
           stopLoader(); 
        },
        success:function(data){
          hideAllModal();
          if(data.status){
            Swal.fire({
                title: 'Success!',
                text: data.message,
                icon: 'success',
                confirmButtonText: 'Ok'
            }).then((result) => {
              window.location.reload();
            });
          }else{
            hideAllModal();
            Swal.fire({
                title: 'Success!',
                text: data.message,
                icon: 'success',
                confirmButtonText: 'Ok'
            }).then((result) => {
              window.location.reload();
            });
          }
        }
      });
    }
	  $(document).ready(function(){ 
      var suc=$("#success").val();
      if(suc=='success'){
        Swal.fire({
            title: 'Success!',
            text: 'Added Successfully',
            icon: 'success',
            confirmButtonText: 'Ok'
        }).then((result) => {

        });
      }


    });
 
function  swalmessagez(suc,message)
    {
       if(suc=='success'){
            Swal.fire({
                title: 'Success!',
                text: message,
                icon: 'success',
                confirmButtonText: 'Ok'
            }).then((result) => {

            });
          }else if(suc=='error'){
             Swal.fire({
                      title: 'Error!',
                      text: message,
                      icon: 'error',
                      confirmButtonText: 'Ok'
                  })
      }
    }
                 
                

	//const WEBSITE_URL="{{url('/').'/'.app()->getLocale()}}";
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});
  jQuery.validator.addMethod("validate_email", function(value, element) {
    if (/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value)) {
        return true;
    } else {
        return false;
    }
  }, "Please enter a valid Email.");
  jQuery.validator.addMethod("validate_phone", function(value, element) {
    if (/^[1-9][0-9]*$/.test(value)) {
        return true;
    } else {
        return false;
    }
  }, "Please enter a phone number.");

$(document).ready(function(){
  $('form[id="enquirytouser"]').validate({
    rules: {
      email: {
        email:true,
        required: true,
        validate_email:true,
      },
       phone: {
        required: true,
        validate_phone:true,
        minlength : 7,
        maxlength : 13,
      }, 
    },
    messages: {
      email: {
                email : 'Must be valid email address',
                validate_email : 'Must be valid email address',
              },
      phone : {
                validate_phone : 'Please enter a valid phone number.',
                minlength : 'Please enter a valid phone number.',
                maxlength : 'Please enter a valid phone number.'
            },
    },
    errorPlacement: function(error, element) {
     if (element.attr("name") == "name"){
      error.appendTo('#contact_name_error');
     }else if(element.attr("name") == "phone"){
      error.appendTo('#contact_phone_error');
     }else if(element.attr("name") == 'email'){
      error.appendTo('#contact_email_error');
     }else if(element.attr("name") == 'content'){
      error.appendTo('#content_content_error');
     }
    },
    submitHandler: function(form) {



 if (grecaptcha.getResponse(makeInspectionInquiryWidgetId) ==""){
        Swal.fire({
                title: 'Error!',
                text: "Please Fill Captcha Code First !",
                icon: 'error',
                confirmButtonText: 'Ok'
            })
      return false;
     
    } 



      var type = $("#enquirytouser #type").val();
      var phone = $("#enquirytouser #phone").val();
      var touser = $("#enquirytouser #touser").val();
  		var message = $("#enquirytouser #content").val();
      var endDate = $("#enquirytouser #endDate1").val();
      var property = $("#enquirytouser #property").val();
      var startDate = $("#enquirytouser #startDate1").val();
      var ajax_url = WEBSITE_URL+"/property/buy/send_enquiry";
      var code = $("#enquirytouser #phone_code").val();
  		var name = $("#enquirytouser  #name").val();
  		var email = $("#enquirytouser  #email").val();
      $.ajax({
        url:ajax_url,
        method:"POST",
        data:{
    			code: code,
    			type: type,
    			name: name,
    			email: email,
    			phone: phone,
          touser: touser,
    			message: message,
          endDate: endDate,
          property: property,
          startDate: startDate,
        },
        headers:{'X-CSRF-TOKEN': '{{ csrf_token() }}'},
  		  beforeSend:function(){
            startLoader();
        },
        complete:function(){
           stopLoader(); 
        },
        success:function(data){
        	hideAllModal();
          if(data.status){
            Swal.fire({
                title: 'Success!',
                text: data.message,
                icon: 'success',
                confirmButtonText: 'Ok'
            }).then((result) => {
              window.location.reload();
            });
          }else{
          	hideAllModal();
            Swal.fire({
                title: 'Success!',
                text: data.message,
                icon: 'success',
                confirmButtonText: 'Ok'
            }).then((result) => {
              window.location.reload();
            });
          }
        }
      });
    }
  });
  $('form[id="mainbooktouser"]').validate({
    rules: {
      email: {
        email:true,
        required: true,
        validate_email:true,
      },
    },
    messages: {
      email: 'Must be valid email address',
    },
    errorPlacement: function(error, element) {
      console.log(element.attr("name"));
     if (element.attr("name") == "name"){
       error.appendTo('#name_error');
     }else if(element.attr("name") == "phone"){
       error.appendTo('#phone_error');
     }else if(element.attr("name") == 'email'){
       error.appendTo('#email_error');
     }else if(element.attr("name") == 'content'){
       error.appendTo('#content_error');
     }else if(element.attr("name") == 'startDate'){
       error.appendTo('#date_error');
     }else if(element.attr("name") == 'endDate'){
       error.appendTo('#time_error');
     }
    },
    submitHandler: function(form) {


        if (grecaptcha.getResponse(bookADayWidgetId) ==""){
        Swal.fire({
                title: 'Error!',
                text: "Please Fill Captcha Code First !",
                icon: 'error',
                confirmButtonText: 'Ok'
            })
      return false;
     
    } 
      var type=$("#mainbooktouser #type").val();
  		var name=$("#mainbooktouser  #name").val();
      var phone=$("#mainbooktouser #phone").val();
  		var email=$("#mainbooktouser  #email").val();
      var touser=$("#mainbooktouser #touser").val();
  		var message=$("#mainbooktouser #content").val();
      var property=$("#mainbooktouser #property").val();
      var endDate=$("#mainbooktouser #book_day_time").data('date');
      var startDate=$("#mainbooktouser #book_day_date").data('date');
      var ajax_url = WEBSITE_URL+"/property/buy/send_enquiry";
      var code=$("#mainbooktouser #phone_code").val();
          $.ajax({
            url:ajax_url,
            method:"POST",
            data:{
        			code:code,
        			type:type,
        			name:name,
        			email:email,
        			phone:phone,
              touser:touser,
        			message:message,
              endDate:endDate,
              property:property,
              startDate:startDate,
            },
            headers:{'X-CSRF-TOKEN': '{{ csrf_token() }}'},
  		  beforeSend:function(){
  	                startLoader();
  	            },
  	            complete:function(){
  	               stopLoader(); 
  	            },
            success:function(data){ 
            	hideAllModal();
              if(data.status){
                Swal.fire({
                    title: 'Success!',
                    text: 'Your booking is received successfully. We will get back to you soon.',
                    icon: 'success',
                    confirmButtonText: 'Ok'
                }).then((result) => {
                  window.location.reload();
                });
              }else{
              	hideAllModal();
                Swal.fire({
                    title: 'Success!',
                    text: 'Your booking is received successfully. We will get back to you soon.',
                    icon: 'success',
                    confirmButtonText: 'Ok'
                }).then((result) => {
                  window.location.reload();
                });
              }
            }
          });
    }
  });
  $('form[id="booktouser"]').validate({
    rules: {
      email: {
        email:true,
        required: true,
        validate_email:true,
      },
    },
    messages: {
      email: 'Must be valid email address',
    },
    submitHandler: function(form) {
      var ajax_url = WEBSITE_URL+"/property/buy/send_enquiry";
          var startDate=$("#booktouser #startDate1").val();
          var endDate=$("#booktouser #endDate1").val();
  		var message=$("#booktouser #content").val();
  		var phone=$("#booktouser #phone").val();
  		var code=$("#booktouser #phone-code").val();
  		var name=$("#booktouser #name").val();
  		var email=$("#booktouser #email").val();
          var type=$("#booktouser #type").val();
          var property=$("#booktouser #property").val();
          var touser=$("#booktouser #touser").val();
          $.ajax({
            url:ajax_url,
            method:"POST",
            data:{
              startDate:startDate,
              endDate:endDate,
  			message:message,
  			phone:phone,
  			code:code,
  			type:type,
  			name:name,
  			email:email,
              property:property,
              touser:touser
            },
            headers:{
              'X-CSRF-TOKEN': '{{ csrf_token() }}',
  		  },
  		  beforeSend:function(){
  	                startLoader();
  	            },
  	            complete:function(){
  	               stopLoader(); 
  	            },
            success:function(data){
            	hideAllModal();
              // stopLoader('.page-content');
              if(data.status){
                Swal.fire({
                    title: 'Success!',
                    text: data.message,
                    icon: 'success',
                    confirmButtonText: 'Ok'
                }).then((result) => {
                  window.location.reload();
                });
              }else{
              	hideAllModal();
                Swal.fire({
                    title: 'Success!',
                    text: data.msg,
                    icon: 'success',
                    confirmButtonText: 'Ok'
                }).then((result) => {
                  window.location.reload();
                });
              }
              // alert(data.msg);
            }
          });
    }
  });

  $('form[id="homeloan_form"]').validate({
    rules: {
      email: {
        email:true,
        required: true,
        validate_email:true,
      },
       phone: {
        required: true,
        validate_phone:true,
        minlength : 7,
        maxlength : 13,
      },
      phone_code: {
        required: true,
        
      },
      name: {
        required: true,
        
      }, 
      content1: {
        required: true,
        
      },
      name: {
        required: true,
        
      }, 
    },
    messages: {
      email: {
                email : 'Must be valid email address',
                validate_email : 'Must be valid email address',
              },
      phone : {
                validate_phone : 'Please enter a valid phone number.',
                minlength : 'Please enter a valid phone number.',
                maxlength : 'Please enter a valid phone number.'
            },
    },
    errorPlacement: function(error, element) {
     if (element.attr("name") == "name"){
      error.appendTo('#homeloan_name_error');
     }else if(element.attr("name") == "phone"){
      error.appendTo('#homeloan_phone_error');
     }else if(element.attr("name") == 'email'){
      error.appendTo('#homeloan_email_error');
     }else if(element.attr("name") == 'content1'){
      error.appendTo('#homeloan_content1_error');
     }else if(element.attr("name") == 'phone_code'){
      error.appendTo('#homeloan_phone_code_error');
     }
    },
    submitHandler: function(form) {

    

 if (grecaptcha.getResponse(makeInspectionInquiryWidgetId) ==""){
        Swal.fire({
                title: 'Error!',
                text: "Please Fill Captcha Code First !",
                icon: 'error',
                confirmButtonText: 'Ok'
            })
      return false;
     
    } 

      var ajax_url = '{{url(app()->getLocale())}}/homeloan';



        var name = $("#homeloan_form #name").val();
        var phone = $("#homeloan_form #phone").val();
        var phone_code = $("#homeloan_form #phone_code").val();
        var content1 = $("#homeloan_form #content1").val();
        var email = $("#homeloan_form  #email").val();
      $.ajax({
        url:ajax_url,
        method:"GET",
        data:{
             name:name,    
             phone:phone,    
             phone_code:phone_code,    
             content1:content1,    
             email:email    
        },
        headers:{'X-CSRF-TOKEN': '{{ csrf_token() }}'},
          beforeSend:function(){
            startLoader();
        },
        complete:function(){
           stopLoader(); 
        },
        success:function(data){
            hideAllModal();
          if(data.status){
            Swal.fire({
                title: 'Success!',
                text: data.message,
                icon: 'success',
                confirmButtonText: 'Ok'
            }).then((result) => {
              window.location.reload();
            });
          }else{
            hideAllModal();
            Swal.fire({
                title: 'Success!',
                text: data.message,
                icon: 'success',
                confirmButtonText: 'Ok'
            }).then((result) => {
              window.location.reload();
            });
          }
        }
      });
    }
  });

  $('form[id="reportuser"]').validate({
    rules: {
      reason: {
        required: true,
      },
      user_type: {
        required: true,
      }
    },
    submitHandler: function(form) {

       if (grecaptcha.getResponse(makeReportFlagWidgetId) ==""){
        Swal.fire({
                title: 'Error!',
                text: "Please Fill Captcha Code First !",
                icon: 'error',
                confirmButtonText: 'Ok'
            })
      return false;
     
    } 
      
      var type=$("#reportuser #type").val();
      var loc=$("#reportuser #loc").val();
      var property=$("#reportuser #property").val();
      var reason=$("#reportuser #reason").val();
      var user_type=$("#reportuser #user_type").val();
      var message=$("#reportuser #content").val();
      var ajax_url = WEBSITE_URL+"/report_user/"+loc+"/"+type+"/"+property;
      $.ajax({
            url:ajax_url,
            method:"POST",
            data:{
              reason:reason,
              user_type:user_type,
              message:message,
            },
            headers:{
              'X-CSRF-TOKEN': '{{ csrf_token() }}',
        },
        beforeSend:function(){
                    startLoader();
                },
                complete:function(){
                   stopLoader(); 
                },
            success:function(data){
              hideAllModal();
              // stopLoader('.page-content');
              if(data.status){
                Swal.fire({
                    title: '',
                    text: data.message,
                    icon: 'success',
                    confirmButtonText: 'Ok'
                }).then((result) => {
                  window.location.reload();
                });
              }else{
                hideAllModal();
                Swal.fire({
                    title: '',
                    text: data.message,
                    icon: 'success',
                    confirmButtonText: 'Ok'
                }).then((result) => {
                  window.location.reload();
                });
              }
              // alert(data.msg);
            }
          });
    }
  });


  /*subscribe form validation start*/
  $('#subscribe-form-submit').click(function(e) {
        e.preventDefault();
        $('#subscribe-form').submit();
    });
  $('#subscribe-form').validate({
    
    rules: {
      email: {
        email:true,
        required: true,
        validate_email:true,
      }
    },
    messages: {
      email: {
                email : 'Email Must be valid email address',
                validate_email : 'Email Must be valid email address',
              } 
    },
    errorPlacement: function(error, element){
       
       if(element.attr("name") == 'email'){
          error.appendTo('#subscribe-form #subscribe_email_error');
        }
    },
    submitHandler: function(form) {
       
     $( '#subscribe-form' )[0].submit();
       
      
    }
  });
  /*subscribe form validation end*/
   /*subscribe form validation start*/
  $('#newslatter_form').validate({
    rules: {
      name: {
         
        required: true,
        
      },
      email: {
        email:true,
        required: true,
        validate_email:true,
      },
    },
    messages: {
      name: {
                required : 'Name is Required',
                 
              }, 
      email: {
        required : 'Email is Required',
                email : 'Email Must be valid email address',
                validate_email : 'Email Must be valid email address',
              },

       
    },
    errorPlacement: function(error, element) {
       if(element.attr("name") == 'email'){

      error.appendTo('#newslatter_email_error');
     }else if(element.attr("name") == 'name'){

      error.appendTo('#newslatter_name_error');
     } 
    },
    submitHandler: function(form) {
       
      $( '#newslatter_form' ).submit();
       
      
    }
  });
  $('#register-form1').validate({
    rules: {
      name: {
         
        required: true,
        
      }, 
      password: {
         
        required: true,
        
      }, 
      password_confirmation: {
         
        required: true,
        
      },
       country: {
         
        required: true,
        
      },
      email: {
        email:true,
        required: true,
        validate_email:true,
      },
    },
    messages: {
      name: {
                required : 'Name is Required',
                 
              }, 
              country: {
                required : 'Country is Required',
                 
              }, 
              password: {
                required : 'Password is Required',
                 
              }, 
               password_confirmation: {
                required : 'Retype Password is Required',
                 
              }, 
      email: {
        required : 'Email is Required',
                email : 'Email Must be valid email address',
                validate_email : 'Email Must be valid email address',
              },

       
    },
    errorPlacement: function(error, element) {
       if(element.attr("name") == 'email'){

      error.appendTo('.invalid-feedback.email');
     }else if(element.attr("name") == 'name'){

      error.appendTo('.invalid-feedback.name');
     } else if(element.attr("name") == 'password'){

      error.appendTo('.invalid-feedback.password');
     } else if(element.attr("name") == 'password_confirmation'){

      error.appendTo('.invalid-feedback.password_confirmation');
     } else if(element.attr("name") == 'country'){

      error.appendTo('.invalid-feedback.country');
     }  else if(element.attr("name") == 'country'){

      error.appendTo('.invalid-feedback.country');
     } 
    },
    submitHandler: function(form) {
       
      $( '#register-form' ).submit();
       
      
    }
  });
  /*subscribe form validation end*/
});
	</script>
    @if(Session::has('error'))

           <script>
            swalmessagez('error',"{{Session::get('error')}}");
           </script>
            <div class="alert alert-danger">

                {{Session::get('error')}}

            </div>

        @endif


        @if(Session::has('success'))
          <script>
            swalmessagez('success',"{{Session::get('success')}}");
           </script>
            <div class="alert alert-success">

                {{Session::get('success')}}

            </div>

        @endif
   
	<script src="{{ asset('js/landingpage.js') }}"></script>

	<script>
function makefav(id){
	var user_id=$("#user_id").val();
	if(user_id==''){
		Swal.fire({
                  title: 'Error!',
                  text: 'Please Login First',
                  icon: 'error',
                  confirmButtonText: 'Ok'
              })
	}
	var ajax_url = WEBSITE_URL+"/favourate";
	$.ajax({
      url:ajax_url,
      method:"GET",
      data:{id:id,user_id:user_id},
      headers:{'X-CSRF-TOKEN': '{{ csrf_token() }}'},
		  beforeSend:function(){
	                startLoader();
      },
      complete:function(){
         stopLoader(); 
      },
      success:function(data){
        if(data.success==1){
          Swal.fire({
              icon: 'success',
              title: 'Success!',
              text: data.message,
              confirmButtonText: 'Ok'
          }).then((result) => {
            window.location.reload();
          });
        }else{
          Swal.fire({
              icon: 'success',
              title: 'Success!',
              text: data.message,
              confirmButtonText: 'Ok'
          }).then((result) => {
            window.location.reload();
          });
        }
      }
    });
}


</script>
 

    <script type="text/javascript">
        $(window).scroll(function(){
	        if ($(this).scrollTop() > 100) {
	        	$('header').addClass('fixed-top');
	        } else {
	        	$('header').removeClass('fixed-top');
	        }
        });
        $(window).scroll(function(){
	        if ($(this).scrollTop() > 100) {
	        	$('#top-header-bar').removeClass('fixed-top');
	        } else {
	        	$('#top-header-bar').addClass('fixed-top');
	        }
        });
    </script>
    <script type="text/javascript">
    	$(document).ready(function(){
    		$(document).on('click','.reset_buy_filter', function(){
    			$('#buy_form')[0].reset();
    		});

    		$(document).on('click','.reset_rent_filter', function(){
    			$('#rent_form')[0].reset();
    		});

    		$(document).on('click','.reset_buy_advance_filter', function(){
    			$('#buy_advance_form')[0].reset();
    		});

    		$(document).on('click','.reset_rent_advance_filter', function(){
    			$('#rent_advance_form')[0].reset();
    		});

    		$(document).on('click','.reset_swap_filter', function(){
    			$('#swap_form')[0].reset();
    		});
    		$(document).on('click','.reset_cospace_filter', function(){
    			$('#coworkspace_form')[0].reset();
    		});

    		$(document).on('click','.changeBackgroundImage', function(e){
    			var name = $(this).data('name');
    			name = name ? name : 'buy';
    			$('.bannar-section').removeClass('buy rent sale swap cospace agents buddies travel');
    			$('.bannar-section').addClass(name);
    			$('.home-filter-title').hide();
    			$('.title_'+name).show();

    		});
    	});


        jQuery("#testimonial").owlCarousel({
	        loop: false,
			rewind: true,
	        margin: 0,
	        responsiveClass: true,
	        // autoHeight: true,
	        autoplayTimeout: 7000,
	        smartSpeed: 800,
	        nav: true,
	        items:2,
	        responsiveClass:true,
	        responsive: {
	        0: {
	        items: 1,
	        nav:true
	        },

	        600: {
	        items: 2,
	        nav:true
	        },

	        1024: {
	        items: 2,
	        nav:true
	        },

	        1366: {
	        items: 2,
	        nav:true
	        }
	        }
        });
        jQuery("#owl-example-second").owlCarousel({
	        loop: false,
			rewind: true,
	        margin: 0,
	        responsiveClass: true,
	        // autoHeight: true,
	        autoplayTimeout: 7000,
	        smartSpeed: 800,
	        nav: true,
	        items:3,
	        responsiveClass:true,
	        responsive: {
	        0: {
	        items: 1,
	        nav:true
	        }, 

	        600: {
	        items: 2,
	        nav:true
	        },

	        1024: {
	        items: 4,
	        nav:true
	        },

	        1366: {
	        items: 4,
	        nav:true
	        }
	        } 
        });
		jQuery(".stories_slider").owlCarousel({
	        loop: true,
	        margin: 0,
	        responsiveClass: true,
	        // autoHeight: true,
	        autoplayTimeout: 7000,
	        smartSpeed: 800,
	        nav: true,
	        items:3, 
	        responsiveClass:true,
	        responsive: {
	        0: {
	        items: 1,
	        nav:true
	        },

	        600: {
	        items: 2,
	        nav:true
	        },

	        1024: {
	        items: 3,
	        nav:true
	        },

	        1366: {
	        items: 3,
	        nav:true
	        }
	        }
        });
    </script>
    <script type="text/javascript">
        jQuery("#owl-example").owlCarousel({
	        loop: false,
			rewind: true,
	        margin: 0,
	        responsiveClass: true,
	        // autoHeight: true,
	        autoplayTimeout: 7000,
	        smartSpeed: 800,
	        nav: true,
	        items:4,
	        responsiveClass:true,
	        responsive: {
	        0: {
	        items: 1,
	        nav:true
	        },

	        600: {
	        items: 2,
	        nav:true
	        },

	        1024: {
	        items: 4,
	        nav:true
	        },

	        1366: {
	        items: 4,
	        nav:true
	        }
	        }
        });
        jQuery("#carouselone").owlCarousel({
	        loop: false,
			rewind: true,
	        margin: 0,
	        responsiveClass: true,
	        autoHeight: true,
	        autoplayTimeout: 7000,
	        smartSpeed: 800,
	        nav: true,
	        items:3,
	        responsiveClass:true,
	        responsive: {
	        0: {
	        items: 1,
	        nav:true
	        },

	        600: {
	        items: 2,
	        nav:true
	        },

	        1024: {
	        items: 3,
	        nav:true
	        },

	        1366: {
	        items: 3,
	        nav:true
	        }
	        }
        });
        // jQuery(".owl-carousel").owlCarousel({
	       //  loop: false,
	       //  margin: 0,
	       //  responsiveClass: true,
	       //  autoplayTimeout: 7000,
	       //  smartSpeed: 800,
	       //  nav: true,
	       //  responsiveClass:true,
	       //  responsive: {
	       //  0: {
	       //  items: 1,
	       //  nav:true
	       //  },

	       //  600: {
	       //  items: 2,
	       //  nav:true
	       //  },

	       //  1024: {
	       //  items: 3,
	       //  nav:true
	       //  },

	       //  1366: {
	       //  items: 3,
	       //  nav:true
	       //  }
	       //  }
        // });
        jQuery(".owl-carousel").owlCarousel({
          nav:true,
          margin:10,
          loop:false,
          slideBy:1,
          navigation : false ,
          lazyLoad:true,
          responsive:{
              0:{
                  items:2
              },
              600:{
                  items:3
              },            
              960:{
                  items:3
              },
              1200:{
                  items:3
              }
          },
      });
    </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/intlTelInput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"></script>
<script type="text/javascript">
var telInput = $(".phone"),
  errorMsg = $("#error-msg"),
  validMsg = $("#valid-msg");
telInput.intlTelInput({

  allowExtensions: true,
  formatOnDisplay: true,
  autoFormat: true,
  autoHideDialCode: true,
  autoPlaceholder: true,
  defaultCountry: "auto",
  ipinfoToken: "yolo",
  initialCountry: "gb",
  nationalMode: false,
  numberType: "MOBILE",
  //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
  preferredCountries: ['sa', 'ae', 'qa','om','bh','kw','ma'],
  preventInvalidNumbers: true,
  separateDialCode: true,
  initialCountry: "sa",
  geoIpLookup: function(callback) {
  $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
    var countryCode = (resp && resp.country) ? resp.country : "";
    callback(countryCode);
  });
},
   utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"
});

var reset = function() {
  telInput.removeClass("error");
  errorMsg.addClass("hide");
  validMsg.addClass("hide");
};

// on blur: validate
telInput.blur(function() {
  reset();
  if ($.trim(telInput.val())) {
    if (telInput.intlTelInput("isValidNumber")) {
      validMsg.removeClass("hide");
    } else {
      telInput.addClass("error");
      errorMsg.removeClass("hide");
    }
  }
});

// on keyup / change flag: reset
telInput.on("keyup change", reset);




var telInput = $("#phoneno"),
  errorMsg = $("#error-msg"),
  validMsg = $("#valid-msg");

// initialise plugin
telInput.intlTelInput({

  allowExtensions: true,
  formatOnDisplay: true,
  autoFormat: true,
  autoHideDialCode: true,
  autoPlaceholder: true,
  defaultCountry: "auto",
  ipinfoToken: "yolo",

  nationalMode: false,
  numberType: "MOBILE",
  //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
  preferredCountries: ['sa', 'ae', 'qa','om','bh','kw','ma'],
  preventInvalidNumbers: true,
  separateDialCode: true,
  initialCountry: "auto",
  geoIpLookup: function(callback) {
  $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
    var countryCode = (resp && resp.country) ? resp.country : "";
    callback(countryCode);
  });
},
   utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"
});

var reset = function() {
  telInput.removeClass("error");
  errorMsg.addClass("hide");
  validMsg.addClass("hide");
};

// on blur: validate
telInput.blur(function() {
  reset();
  if ($.trim(telInput.val())) {
    if (telInput.intlTelInput("isValidNumber")) {
      validMsg.removeClass("hide");
    } else {
      telInput.addClass("error");
      errorMsg.removeClass("hide");
    }
  }
});

// on keyup / change flag: reset
telInput.on("keyup change", reset);





$(".styled-checkbox").change(function(){

  var flag = $(this).prop('checked');
  
  if($(this).attr('id') == 'styled-checkbox-1'){
    return;
  }

  if(flag){
    $(this).next().next().attr('required','required');
    $(this).next().next().val('');

  }else{
    $(this).next().next().removeAttr('required');
    $(this).next().next().val('');
  }

});

 

    </script>

	@yield('scripts')
  
</body>
</html>
