<link rel="shortcut icon" href="{{ asset('img/ic_favi.png') }}">
<style>
.goog-logo-link {
   display:none !important;
}

.goog-te-gadget {
   color: transparent !important;
}

.goog-te-gadget .goog-te-combo {
   color: black !important;
   margin: 0px 0!important;
   border: 1px solid black !important;
}

</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/css/intlTelInput.css" rel="stylesheet" media="screen">
<link href="{{ asset('css/countryCode.css') }}" rel="stylesheet">
<script type="text/javascript">
    var WEBSITE_URL="{{url('/').'/'.app()->getLocale()}}";
 </script>
@php
$lang=Request::segment(1);
$loc=Request::segment(3);
    $modules = \App\Models\Module::where('location',$loc)->get();
    if($loc!='all'){
        $link=$loc;
    }else{
        $link='all';
    }
@endphp

@if(\Auth::user())
@php
    $fav = \App\Models\Favourate::where('user_id',\Auth::user()->id)->count();
    
    if($fav > 0){
        $favi = 'red';
    }else{
        $favi = 'transparent';
    }
@endphp
@endif

 @if(\Auth::user())
@php
    $not = \App\Models\Notification::where('receiver_id',\Auth::user()->id)->where('read',0)->count();
    
    if($not > 0){
        $noti = 'red';
    }else{
        $noti = 'transparent';
    }
@endphp
@endif
 @if(@$_GET['popup'] !=='true') 
 
<div class="fixed-top">
    <header class="homepage inner-header">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light bg-transparent p-0">
                <a class="navbar-brand" href="{{url(app()->getLocale().'/home/'.$loc)}}">
                	<img src="{{asset('img/logo_logo.png')}}">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown ml-3 left-right-border location-block">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ucfirst(Request::segment(3)) ?? 'Properties In'}}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                @foreach(getAllLocations() as $row)
                                    <a class="dropdown-item" href="{{url(app()->getLocale().'/rent-property/'.strtolower($row->name).'/?country='.strtolower($row->name))}}">{{ucfirst($row->name)}}</a>
                                @endforeach
                            </div>
                        </li>
                    </ul>
                    <ul class="navbar-nav ml-auto">
                        @auth
                            <li class="nav-item mr-4 mt-1">
                                <a class="nav-link" onclick="checkPhoneNumber('{{Auth::user()->phone}}','{{Auth::user()->email}}','{{Auth::user()->name}}','{{Auth::user()->country}}',this)" data-href="{{url(app()->getLocale().'/listproperty/'.$link)}}" data-profile="{{url(app()->getLocale().'/profile/'.$link)}}"><button class="list-property red"><i class="mr-2"><img src="{{asset('img/list-property-white.png')}}"></i>List Your Property</button></a>

                                <!-- @if(isset(Auth::user()->phone) && !is_null(Auth::user()->phone))
                                <a class="nav-link" href="{{url(app()->getLocale().'/listproperty/'.$link)}}"><button class="list-property red"><i class="mr-2"><img src="{{asset('img/list-property-white.png')}}"></i>List Your Property</button></a>
                                @else
                                <a class="nav-link" onclick="checkPhoneNumber('{{Auth::user()->phone_number}}',this)" data-href="{{url(app()->getLocale().'/listproperty/'.$link)}}" data-profile="{{url(app()->getLocale().'/profile/'.$link)}}"><button class="list-property red"><i class="mr-2"><img src="{{asset('img/list-property-white.png')}}"></i>List Your Property</button></a>
                                @endif -->
                            </li>

                            <!-- <li class="nav-item dropdown languages">
                                <a class="nav-link dropdown-toggle" href="javascipt:;" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if(app()->getLocale()=='en')
                                        <img src="{{asset('flags/en.png')}}" style="height: 20px;width: 20px;"> {{ucfirst(str_replace('_', '-', app()->getLocale()))}}
                                    @elseif(app()->getLocale()=='ar')
                                        <img src="{{asset('flags/ar.png')}}" style="height: 20px;width: 20px;"> {{ucfirst(str_replace('_', '-', app()->getLocale()))}}
                                    @elseif(app()->getLocale()=='zh')
                                        <img src="{{asset('flags/zh.png')}}" style="height: 20px;width: 20px;"> {{ucfirst(str_replace('_', '-', app()->getLocale()))}}
                                    @else
                                        <img src="{{asset('flags/en.png')}}" style="height: 20px;width: 20px;"> {{ucfirst(str_replace('_', '-', app()->getLocale()))}}
                                    @endif
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    @foreach(getAllLocales() as $key=>$value)
                                        <a href="{{ route('lang.switch', $value['key']) }}" style="padding-left: 8px;font-weight: normal;font-size: 15px;font-family: inherit;">
                                        @if($value['name']=='English') <img src="{{asset('flags/en.png')}}" style="height: 20px;width: 20px;">
                                        @elseif($value['name']=='Arabic')<img src="{{asset('flags/ar.png')}}" style="height: 20px;width: 20px;">
                                        @elseif($value['name']=='Chinese') <img src="{{asset('flags/zh.png')}}" style="height: 20px;width: 20px;"> @endif {{$value['name']}}</a>
                                    @endforeach
                                </div>
                            </li> -->
                            <li class="nav-item" style="margin-top: 12px;">
                            <div id="google_translate_element2" style="width: 147px;"></div>
                            </li>
                            <!-- <li class="nav-item">
                                <a class="nav-link white-color" href="{{url(app()->getLocale().'/user_chat/'.$link)}}">Messages</a>
                            </li> -->
                            <li class="nav-item">
                            @if(\Auth::user())
                                @if(@$noti=='red')
                                <a class="nav-link" href="{{url(app()->getLocale().'/notifications/'.$link)}}"><img class="" src="{{asset('img/notification1.png')}}" style="height: 26px;width: 26px;"></a>
                                @else
                                <a class="nav-link" href="{{url(app()->getLocale().'/notifications/'.$link)}}"><i class="fa fa-bell-o" aria-hidden="true" style="color: black !important;font-size: 21px;margin-top: 7px;"></i></a>
                                @endif
                                @else
                                <a href="javascript:;" onclick="showLoginForm();"><i class="fa fa-bell-o" aria-hidden="true" style="color: black !important;font-size: 21px;margin-top: 7px;"></i></a>
                                @endif
                            </li>
                            <li class="nav-item">
                            @if(\Auth::user())
                                @if(@$favi=='red')
                                <a class="nav-link" href="{{url(app()->getLocale().'/myfav/'.$link)}}"><i class="fa fa-heart" style="font-size: 24px;position: absolute;color: red;top: 13px !important;right: 86px !important; padding-top: 2px !important;padding-left: 10px !important;"></i></a>
                                @else
                                <a class="nav-link" href="{{url(app()->getLocale().'/myfav/'.$link)}}"><i class="fa fa-heart-o" style="font-size: 24px;position: absolute;color: red;top: 13px !important;right: 86px !important;padding-top: 2px !important;padding-left: 10px !important;"></i></a>
                                @endif
                                @else
                                <a href="javascript:;" onclick="showLoginForm();"><i class="fa fa-heart-o" style="font-size: 24px;position: absolute;color: red;top: 13px !important;right: 86px !important;padding-top: 2px !important;padding-left: 10px !important;"></i></a>
                                @endif
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="javascipt:;" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    
                                    @if(isset(Auth::user()->image_path))
                                        @if(Auth::user()->image_path =='/assets/img/avatar.png')
                                        <img class="circle" src="{{url(Auth::user()->image_path)}}" style="height: 40px;width: 40px;">
                                        @else
                                        <img class="circle" src="{{Auth::user()->image_path}}" style="height: 40px;width: 40px;">
                                         @endif
                                    @else
                                        <img class="invert-color circle" src="{{asset('img/profile-avtar.png')}}" style="height: 40px;width: 40px;">
                                    @endif  
                                    
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    <a class="dropdown-item" href="{{url(app()->getLocale().'/profile/'.$link)}}">Profile</a>
                                   <!--  <a class="dropdown-item" href="{{ route('logout', app()->getLocale()) }}" >Logout</a> -->
                                   <a class="dropdown-item logout-btn-submit" href="javascript:;" >Logout</a>
                                </div>
                            </li>
                        @endauth

                        @guest
                            <!-- <li class="nav-item dropdown languages">
                                <a class="nav-link dropdown-toggle" href="javascipt:;" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if(app()->getLocale()=='en')
                                        <img src="{{asset('flags/en.png')}}" style="height: 20px;width: 20px;"> {{ucfirst(str_replace('_', '-', app()->getLocale()))}}
                                    @elseif(app()->getLocale()=='ar')
                                        <img src="{{asset('flags/ar.png')}}" style="height: 20px;width: 20px;"> {{ucfirst(str_replace('_', '-', app()->getLocale()))}}
                                    @elseif(app()->getLocale()=='zh')
                                        <img src="{{asset('flags/zh.png')}}" style="height: 20px;width: 20px;"> {{ucfirst(str_replace('_', '-', app()->getLocale()))}}
                                    @else
                                        <img src="{{asset('flags/en.png')}}" style="height: 20px;width: 20px;"> {{ucfirst(str_replace('_', '-', app()->getLocale()))}}
                                    @endif
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                    @foreach(getAllLocales() as $key=>$value)
                                        <a href="{{ route('lang.switch', $value['key']) }}" style="padding-left: 8px;font-weight: normal;font-size: 15px;font-family: inherit;">{{$value['name']}}</a>
                                    @endforeach
                                </div>
                            </li> -->
                            <li class="nav-item" style="margin-top: 12px;">
                            <div id="google_translate_element2" style="width: 147px;"></div>
                            </li>
                            
                            <li class="nav-item register-menu" style="margin-top: 12px;">
                                <a href="javascript:;" onclick="showLoginForm();" style="background-color: white;color: black;">Log in</a>
                            </li>
                            <li class="nav-item register-menu" style="margin-top: 12px;">
                                <a href="javascript:;" onclick="showRegisterForm();">Register</a>
                            </li>
                        @endguest
                    </ul>
                </div>
            </nav>
        </div>
    </header>
</div>
<script type="text/javascript">  
					function googleTranslateElementInit() {  
							new google.translate.TranslateElement( 
									{pageLanguage: 'en', includedLanguages : 'en,zh-CN,ph,fr,ar'},  
									'google_translate_element2' 
							);  
					}
                    function checkPhoneNumber(phone,email,name,country,obj)
                    {
                        if(phone == "" || email == "" || name == "" || country == "")
                        {
                            Swal.fire({
                                title: 'Incomplete Profile!',
                                text: "Please complete your profile with personal info Mobile, Email, Name, Country before list your property.",
                                icon: 'info',
                                confirmButtonText: 'Ok',
                                showCancelButton: true,
                                focusConfirm: false,
                            }).then((result) => {
                                if(result.value){
                                    window.location = $(obj).data('profile');
                                }
                            });
                        }else{
                            window.location = $(obj).data('href');
                        }
                    }


			</script>
            


            @endif