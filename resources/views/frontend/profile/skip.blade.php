@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Skip Page</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Bootstrap CSS File -->
    <link href="{{asset('lib/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Main Stylesheet File -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('fonts/stylesheet.css')}}" rel="stylesheet">

</head>

<body>
    <div id="register-popup" class="modal1 skipp-page-cls">
        <div class="">
            <!-- Modal content-->
            <div class="signup-right">
                <a href="{{url(app()->getLocale())}}"><span class="close-icon"></span></a>
                <a href="{{url(app()->getLocale())}}" class="skipp-section">Skip <img src="{{asset('img/Arrow.png')}}"></a>
                <div class="modal-body">
                    <div class="row">
                        <div class="col col-md-4">
                            <div class="modal-left_side skipp-left-bar">
                                <img src="{{asset('img/address.png')}}">
                                <h2>Start Connecting, Start Selling.</h2>
                                <p>Create an Agent Profile to list properties or become a Buddy and Earn Money.</p>

                            </div>
                        </div>
                        <div class="col col-md-4 text-center">
                            <div class="box-section-skip">
                                <img src="{{asset('img/Agent.png')}}">
                                <h2>Become an Agent</h2>
                                <p>Create your agent Profile & List Properties on the Platform hassel Free. Keep Track of your Properties & Projects.</p>
                                
                                <a href="{{url(app()->getLocale())}}">
                                	<button class="btn btn-large button skip-btn">Become an Agent Today 
                                		<img src="{{asset('img/arrow-white.png')}}">
                                	</button>
                                </a>
                                
                            </div>
                        </div>
                        <div class="col col-md-4 text-center">
                            <div class="box-section-skip">
                                <img src="{{asset('img/Buddy.png')}}">
                                <h2>Become a Buddy</h2>
                                <p>Create your Buddy profile and Become the Property Scout of your Area. Earn upto AED 1500 / Consultation. <a href="">Learn More.</a></p>
                                <a href="{{url(app()->getLocale())}}">
                                	<button class="btn btn-large button skip-btn">Create your Buddy Profile <img src="{{asset('img/arrow-white.png')}}"></button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</body>

</html>