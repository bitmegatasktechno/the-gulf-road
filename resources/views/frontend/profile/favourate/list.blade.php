@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
<div class="col-md-9">
	<div class="card p-4">
	    <!-- Tab panes -->
	    <div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="rentListing">
			    <div class="row mt-4">
			    	@if(count($favs)>0)
					
				        @foreach($favs as $row)

	                        <!-- <a href="{{url(app()->getLocale().'/property/'.$link.'/coworkspace/'.$row->_id)}}"> -->
		                        <div class="col-md-4">
								<i class="fa fa-heart fav-icon" style="right: 25px;z-index: 999;" onClick="makefav('{{$row->propert_id}}')"></i>
		                            <div class="property-card bg-white rounded">

                                    @if(isset($row->property->files) && file_exists('uploads/properties/'.@$row->property->files[0]))
                              
	                                    <img class="img-fluid property-img" src="{{url('uploads/properties/'.@$row->property->files[0])}}">
                                    @elseif(isset($row->swap->files) && file_exists('uploads/swap/'.@$row->swap->files[0]))
                                    
	                                    <img class="img-fluid property-img" src="{{url('uploads/swap/'.@$row->swap->files[0])}}">
	                                @elseif(isset($row->cospace->files) && file_exists('uploads/coworkspace/'.@$row->cospace->files[0]))
	                                
	                                    <img class="img-fluid property-img" src="{{url('uploads/coworkspace/'.@$row->cospace->files[0])}}">
	                                @else
	                                    <img class="img-fluid property-img" src="{{asset('img/house2.png')}}">
	                                @endif

		                                <div class="pl-3 pr-3">
		                                    <h5>
		                                       @if( isset($row->property->property_title) )
		                                        	{{\Str::limit(ucfirst(@$row->property->property_title), 25)}}
		                                        @elseif( isset($row->swap->title) )
		                                        	{{\Str::limit(ucfirst(@$row->swap->title), 25)}}
		                                        @elseif( isset($row->cospace->title) )
		                                        	{{\Str::limit(ucfirst(@$row->cospace->title), 25)}}
		                                        @endif
		                                    </h5>
		                                    
		                                    <p class="mb-2">
		                                    	<i class="fa fa-map-marker mr-2"></i> 

		                                       @if( isset($row->property->location_name) )
		                                       	{{\Str::limit(@$row->property->location_name, 25)}}
		                                       @elseif( isset($row->swap->location_name) )
												{{\Str::limit(@$row->swap->location_name, 25)}}
		                                       @elseif( isset($row->cospace->location_name) )
													{{\Str::limit(@$row->cospace->location_name, 25)}}		                                       
		                                      @endif
		                                    </p>
		                                    
		                                   
		                                   
		                                </div>
		                                <!-- <a class="edit-icon" href="{{url(app()->getLocale().'/editproperty/'.$link.'/coworkspace/'.$row->_id)}}"><img  src="{{asset('img/edit.png')}}"></a> -->

		                                <!-- <a class="delete-icon delete_cospace" href="javascript:;" data-id="{{$row->_id}}">
		                                	<img  src="{{asset('img/delete.png')}}">
		                                </a> -->
		                            </div>
		                        </div>
	                   		<!-- </a> -->
	                    @endforeach
						@else
						<div class="" style="display: block;
                            max-height: 60px;
                            text-align: center;
                            background-color: #d6e9c6;
                            line-height: 4;
                            width: 100%;
                            ">
                            <p style="font-size: 16px;font-weight: 700;">No Result Found</p>
                        </div>
						@endif
                    
			    </div>
			</div>
	    </div>
	</div>
</div>
