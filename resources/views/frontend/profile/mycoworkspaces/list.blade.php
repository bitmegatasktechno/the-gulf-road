<div class="col-md-9">
	<div class="card p-4">
	    <!-- Tab panes -->
	    <div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="rentListing">
			    <div class="row mt-4">
			    	@if(count($data)>0)
				        @foreach($data as $row)
	                        <a href="{{url(app()->getLocale().'/property/'.$link.'/coworkspace/'.$row->_id)}}">
		                        <div class="col-md-4">
		                            <div class="property-card bg-white rounded">
		                                @if(!isset($row->files[0]) || $row->files[0]=='')
		                                    <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}" style="height: 120px !important">
		                                @else
		                                    <img class="img-fluid property-img" src="{{url('uploads/coworkspce/'.$row->files[0])}}" style="height: 120px !important">
		                                @endif
		                                
		                                <div class="pl-3 pr-3">
		                                    <h5>
		                                       
		                                        {{\Str::limit(ucfirst($row->title), 25)}}
		                                        
		                                    </h5>
		                                    
		                                    <p class="mb-2">
		                                    	<i class="fa fa-map-marker mr-2"></i> {{\Str::limit($row->address, 25)}}
		                                    </p>
		                                    
		                                    <h5>Starting @ {{$row->open_space_daily_currency}} {{$row->open_space_daily_price ?? 0}} /m</h5>
		                                   <h5 class="text-danger text-right">&nbsp;{{ $row->is_approved ? '' : 'Approval Pending' }}<h5>
		                                </div>
		                                <a class="edit-icon" href="{{url(app()->getLocale().'/editproperty/'.$link.'/coworkspace/'.$row->_id)}}"><img  src="{{asset('img/edit.png')}}"></a>

		                                <a class="delete-icon delete_cospace" href="javascript:;" data-id="{{$row->_id}}">
		                                	<img  src="{{asset('img/delete.png')}}">
		                                </a>
		                            </div>
		                        </div>
	                   		</a>
	                    @endforeach
                    @else
                        <div class="" style="display: block;
                            max-height: 60px;
                            text-align: center;
                            background-color: #d6e9c6;
                            line-height: 4;
                            width: 100%;
                            ">
                            <p style="font-size: 16px;font-weight: 700;">No Result Found</p>
                        </div>
                    @endif
			    </div>
			</div>
	    </div>
	</div>
</div>
