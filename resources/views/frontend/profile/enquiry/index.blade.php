@php
$lang=Request::segment(1);
$loc=Request::segment(3);
$modules = \App\Models\Module::where('location',$loc)->get();
if($loc!='all'){
  $link=$loc;
}else{
  $link='all';
}
@endphp
@extends('frontend.layouts.home')
@section('title','Enquiries')
@section('content')

@include('frontend.layouts.default-header')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
 
    <section class="inner-body body-light-grey mt-70">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-12">
	                <ul class="list-inline breadcrum">
	                    <li><a href="{{url(app()->getLocale().'/home/'.$link)}}">Home </a></li>
	                    <span> / </span>
	                    <li><a href="{{url(app()->getLocale().'/change-password/'.$link)}}"> Enquiry</a></li>
	                </ul>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-md-3">

	                <!-- Navigation Block -->
	                <div class="inner-white-wrapper left-navigation mb-3">
@include('frontend.profile.user_profile_sidebar')
	                </div>
	            </div>
	            <!-- Right Wrapper -->
	            <div class="col-md-9 right-wrapper">
	                <div class="inner-white-wrapper personal-details mb-3 overflow-hidden">
	                    <div class="row dp-flex align-items-center">
	                        <div class="col-md-6">
	                            <h3>Enquiries</h3>
	                        </div>
	                        <div class="col-md-6 text-right">
                                    <form  id = "filter_form" method = "get" action = "{{url(app()->getLocale().'/enquiries/'.$link)}}">
                                      <div class = "row" >

                                         
                                          <div class= "col-md-7">
                                            <div class="form-group">
                                              <label for="exampleSelectInfo">Filter</label>
                                              
                                              <input type = "hidden" name = "start_date" id = "start_date">
                                              <input type = "hidden" name = "end_date" id = "end_date">
                                              <input type="text" name="daterange" value="{{date('m/d/Y',strtotime(@$start_date))}} - {{date('m/d/Y',strtotime(@$end_date))}} " class="form-control date-picker border-info" type="text" clearable="true" options="dateRangeOptions" style="width: 110%;"/>
                                             
                                            </div>
                                          </div>
                                          <div class= "col-md-5">
                                            <div class="pl-3 pr-3">
                                              <div class="form-group">
                                              <label for="exampleSelectInfo">Status</label>
                                              <select class="form-control border-info" id="filter4"  name="filter4" >
                                              <option value="e">All</option>
                                              <!-- <option value="Access Pending">Access Pending</option> -->
                                              <option value="Pending">Pending</option>
                                              <option value="In-Progress">In-Progress</option>
                                              <option value="Closed">Closed</option>
                                              </select>
                                              </div>
                                            </div>
                                          </div>
                                           
                                      </div>
                                      </form>
	                        </div>
	                        <div class="col-md-12">
	                            <hr>
	                        </div>
                   
                          
	                        <div class="col-md-12 personal_information_form">
                             <style type="text/css">
                            .table th, .table td {
                            padding:2px !important;
                            }
                            .personal_information_form table>th:nth-child(2),
                            .personal_information_form table>td:nth-child(2),
                            .personal_information_form table th:nth-child(3),
                            .personal_information_form table td:nth-child(3),
                            .personal_information_form table th:nth-child(4),
                            .personal_information_form table td:nth-child(5)
                            .personal_information_form table th:nth-child(5)
                             {
                                width: 250px !important;
                            }
                          </style>
                            <table id="example3" class="table table-responsive">
                              <thead>
                              <tr>
                                <th>SrNo</th>
                                <th>Property&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>Email&nbsp;Address</th>
                                <th>Contact&nbsp;Number</th>
                                <th >Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th  >Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>Action</th>

                              </tr>
                            </thead>
                            <tbody>
                              <?php $i=1; ?>
                              <?php if(@$getEnquiry){ ?>
                              @foreach($getEnquiry as $getEnq)
                              <div style="display:none">
                              <input type="text" id="name_{{$getEnq->_id}}" value="{{@$getEnq->name}}">
                              <input type="text" id="email_{{$getEnq->_id}}" value="{{@$getEnq->email}}">
                              <input type="text" id="phone_{{$getEnq->_id}}" value="{{@$getEnq->phone}}">
                              <input type="text" id="message_{{$getEnq->_id}}" value="{{@$getEnq->message}}">
                              @if(@$getEnq->type == 'swap')
                              <input type="text" id="property_{{$getEnq->_id}}" value="{{@$getEnq->swap->title ? @$getEnq->swap->title : 'N/A' }}">

                              @endif

                              @if(@$getEnq->type == 'coworkspace')
                              <input type="text" id="property_{{$getEnq->_id}}" value="{{@$getEnq->cospace->title ? @$getEnq->cospace->title : 'N/A' }}">

                              @endif

                              @if(@$getEnq->type == 'rent')
                              <input type="text" id="property_{{$getEnq->_id}}" value="{{@$getEnq->property->property_title ? @$getEnq->property->property_title : 'N/A' }}">

                              @endif

                              @if(@$getEnq->type == 'buy')
                              <input type="text" id="property_{{$getEnq->_id}}" value="{{@$getEnq->property->property_title ? @$getEnq->property->property_title : 'N/A' }}">

                              @endif

                              <input type="text" id="date_{{$getEnq->_id}}" value="{{date('F d, Y',strtotime(@$getEnq->created_at))}}">
                            </div>
                              <tr>
                                <td>{{$i}}</td>

                                @if(@$getEnq->type == 'swap')
                                <td><a href="{{url(app()->getLocale().'/property/'.$link.'/swap/'.$getEnq->property_id)}}" target="_blank">{{@$getEnq->swap->title ? \Str::limit(@$getEnq->swap->title, 15, '...') : 'N/A'}}</a></td>

                                @elseif(@$getEnq->type == 'coworkspace')
                                <td><a href="{{url(app()->getLocale().'/property/'.$link.'/coworkspace/'.$getEnq->property_id)}}" target="_blank">{{@$getEnq->cospace->title ? \Str::limit(@$getEnq->cospace->title, 15, '...') : 'N/A'}}</a></td>
                                

                                @elseif(@$getEnq->type == 'buy')
                                <td><a href="{{url(app()->getLocale().'/property/'.$link.'/buy/'.$getEnq->property_id)}}" target="_blank">{{@$getEnq->property->property_title ? \Str::limit(@$getEnq->property->property_title, 15, '...') : 'N/A'}}</a></td>

                                @elseif(@$getEnq->type == 'rent')
                                <td><a href="{{url(app()->getLocale().'/property/'.$link.'/rent/'.$getEnq->property_id)}}" target="_blank">{{@$getEnq->property->property_title ? \Str::limit(@$getEnq->property->property_title, 15, '...') : 'N/A'}}</a></td>
                                @else
                                <td> - </td>
                                @endif

                                <td>{{@$getEnq->name}}</td>
                                <td>{{@$getEnq->email}}</td>
                                <td>{{@$getEnq->phone_code}}&nbsp;{{@$getEnq->phone}}</td>
                                <td>{{@$getEnq->start_date ?? @$getEnq->create}}</td>
                                <td>@if(@$getEnq->is_action==0){{'Pending'}} @elseif(@$getEnq->is_action==1) {{'In-Progress'}} @else {{'Closed'}} @endif</td>


                                <td><div class="actions">
                          				<div class="btn-group">
                          					<a class="label label-primary dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="true">
                          						Actions
                          					</a>
                          					<ul class="dropdown-menu pull-right">
                                      <li>
                                          <a class="view change-status viewdata" data-id="{{$getEnq->_id}}" id="viewdata">
                                            <i class="icon-eye"></i> View
                                          </a>
                                      </li>
                          						<li>
                                        
                                         
                          							@if($getEnq->is_action==0)
                          								<a class="view change-status" href="{{url(app()->getLocale().'/enquiry_action/'.$getEnq->_id)}}">
                          									<i class="icon-eye"></i> In-Progress
                          								</a>
                                          @elseif($getEnq->is_action==1)
                            								<a class="view change-status" href="{{url(app()->getLocale().'/enquiry_action_one/'.$getEnq->_id)}}">
                            									<i class="icon-eye"></i> Closed
                            								</a>
                          							@endif
                          						</li>
                          					</ul>
                          				</div>
                          			</div></td>

                              </tr>
                              <?php $i++;  ?>

                              @endforeach
                            <?php } ?>
                            </tbody>
                            </table>
	                        </div>
	                        <div class="col-md-12">
	                            <hr>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
  <div class="modal fade" id="viewrating">
  <div class="modal-dialog modal-md">
  <div class="modal-content">

  <div class="modal-header" style="border-bottom:none">
    <h4 class="modal-title" style="text-align: center;color: black;"></h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
  </div>



  <!-- Modal footer -->
  <div class="modal-footer text-center" style="display: block;border-top:none">
    <label>Property:</label> <label id="property1"></label><br>
    <label>User Name:</label> <label id="user1"></label><br>
    <label>Date:</label> <label id="date1"></label><br>
    <label>Email:</label> <label id="email1"></label><br>
    <label>Phone:</label> <label id="phone1"></label><br>
    <label>Message:</label> <label id="message1"></label><br>

    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="background: red;margin-bottom: 14px;">Ok</button>
  </div>

  </div>
  </div>
  </div>
    @include('frontend.layouts.footer')

@endsection

@section('scripts')

@include('frontend.profile.common.js')

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

 <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
$(".viewdata").click(function(){
  var id = $(this).data('id'); 
  $("#viewrating").modal('show');
  var name=$("#name_"+id).val();
  var email=$("#email_"+id).val();
  var phone=$("#phone_"+id).val();
  var message=$("#message_"+id).val();
  var property=$("#property_"+id).val();
  var date=$("#date_"+id).val();

  $("#property1").html(property);
  $("#user1").html(name);
  $("#email1").html(email);
  $("#phone1").html(phone);
  $("#message1").html(message);
  $("#date1").html(date);
});
function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};
$(function() {
              $('#example3').DataTable();
              //$('#example3').css('width','unset');
              document.getElementById("example3").style.width='unset';
 
              //$('#example3').css('width','unset');

              var startdate = getUrlParameter('start_date');
            if(startdate){
              var enddate = getUrlParameter('end_date');
              var startdate = getUrlParameter('start_date');
            }else{
              var enddate = moment().format('MM/DD/YYYY');
              var startdate = moment().format('MM/DD/YYYY');
            }


              $('input[name="daterange"]').daterangepicker({
                opens: 'left'
              }, function(start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                      var startdate=start.format('MM/DD/YYYY');
                      var enddate=end.format('MM/DD/YYYY');
                      $('#start_date').val(startdate);
                      $('#end_date').val(enddate);
                      $('#filter_form').submit();
                      });


          });

      $('#reportrange span').html('{{@$start_date}}' + ' - ' + '{{@$end_date}}');

	// save user personal informations.
	$(document).on('click','.personal_info_submit_btn', function(e){
		e.preventDefault();
        // clear all errors
        $('#profile_form').find('.is-invalid').removeClass('is-invalid');
        $('#profile_form').find('.invalid-feedback').text('');

        var formFields = new FormData($('#profile_form')[0]);
        var ajax_url = WEBSITE_URL+"/change-password";

        $.ajax({
            url : ajax_url,
            type:'post',
            data : formFields,
            dataType : 'json',
            processData: false,
            contentType: false,
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            beforeSend:function(){
                startLoader('.page-content');
            },
            complete:function(){
                //
            },
            success : function(data){
                stopLoader('.page-content');
                if(data.status){
                	Swal.fire({
	                    title: 'Success!',
	                    text: data.message,
	                    icon: 'success',
	                    confirmButtonText: 'Ok'
	                }).then((result) => {
	                  window.location.reload();
	                });
                }else{
                	Swal.fire({
	                    title: 'Error!',
	                    text: data.message,
	                    icon: 'error',
	                    confirmButtonText: 'Ok'
	                });
                }
            },
            error : function(data){
                stopLoader('.page-content');
                if(data.responseJSON){
                    var err_response = data.responseJSON;
                    if(err_response.errors==undefined && err_response.message) {
                        Swal.fire({
                            title: 'Error!',
                            text: err_response.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }

                    $.each(err_response.errors, function(i, obj){
                        $('#profile_form').find('input[name="'+i+'"]').next().text(obj);
                        $('#profile_form').find('input[name="'+i+'"]').addClass('is-invalid');
                    });
                }
            }
        });
	});

  $("#filter4").on('change', function()
{
  if(this.value !=='')
  {
    $("#filter_form").submit();  
  }
  
  $('#example3').DataTable().columns(4).search(this.value).draw();
  var count=$("#example3_info").html();
  var slug = count.split('of ').pop();
  var res = slug.substring(0, 5);
  var matches = res.match(/(\d+)/);
    $('#total_records').text(matches[0]);
});
</script>
@endsection
