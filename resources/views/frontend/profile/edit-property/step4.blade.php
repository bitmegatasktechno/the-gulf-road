<section class="step4 listPropertySteps">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h5>Describe your property</h5>
                <p class="">We need a small description of your property. List the features that are Unique to your property.</p>
                
                
                <div  name="rent_sale_property_step_4" id="rent_sale_property_step_4">
                    
                    <div class="loc-form">
                        <h5>Title</h5>
                        <input class="form-control mt-1 mb-3" type="text" name="property_title" placeholder="“Fully Furnished House”" value="{{$data->property_title}}">
                        <span class="invalid-feedback property_title"></span>
                        
                        <h5>Description</h5>
                        <textarea class="form-control mt-1 mb-3" rows="6" placeholder="“My Home is Located Near the Lake and has a beautiful view of the whole town.”" name="property_description">{{$data->property_description}}</textarea>
                        <span class="invalid-feedback property_description"></span>
                    </div>

                    <div class="house_rules" style="display: @if($data->listing_type=='rent' || $data->listing_type=='both') '' @else none @endif">
                        <h5>House rules</h5>
                        <p>Is smoking allowed inside the house?</p>
                    
                        <div class="form-group">
                            <div class="radio dp-inline-block mr-4" style="margin-bottom: 0;">
                                <input id="smokingradio-1" name="smoking_allowed" type="radio" value="yes" autocomplete="off" @if($data->smoking_allowed=='yes') checked @endif>
                                <label for="smokingradio-1" class="radio-label">Yes</label>
                            </div>
                            <div class="radio dp-inline-block" style="margin-bottom: 0;">
                                <input id="smokingradio-2" name="smoking_allowed" type="radio" value="no" autocomplete="off" @if($data->smoking_allowed=='no') checked @endif>
                                <label for="smokingradio-2" class="radio-label">No</label>
                            </div><br>
                            <span class="invalid-feedback smoking_allowed"></span>
                        </div>
                    
                        <p>Are parties allowed?</p>
                        <div class="form-group">
                            <div class="radio dp-inline-block mr-4" style="margin-bottom: 0;">
                                <input id="partiesradio-3" name="parties_allowed" value="yes" type="radio" autocomplete="off" @if($data->parties_allowed=='yes') checked @endif>
                                <label for="partiesradio-3" class="radio-label">Yes</label>
                            </div>
                            <div class="radio dp-inline-block" style="margin-bottom: 0;">
                                <input id="partiesradio-4" name="parties_allowed" value="no" type="radio" autocomplete="off" @if($data->parties_allowed=='no') checked @endif>
                                <label for="partiesradio-4" class="radio-label">No</label>
                            </div><br>
                            <span class="invalid-feedback parties_allowed"></span>
                        </div>
                    
                        <p>Are pets allowed?</p>
                        
                        <div class="form-group">
                            <div class="radio dp-inline-block mr-4" style="margin-bottom: 0;">
                                <input id="petsradio-5" name="pets_allowed" type="radio" value="yes" autocomplete="off" @if($data->pets_allowed=='yes') checked @endif>
                                <label for="petsradio-5" class="radio-label">Yes</label>
                            </div>
                            <div class="radio dp-inline-block" style="margin-bottom: 0;">
                                <input id="petsradio-6" name="pets_allowed" type="radio" value="no" autocomplete="off" @if($data->pets_allowed=='no') checked @endif>
                                <label for="petsradio-6" class="radio-label">No</label>
                            </div><br>
                            <span class="invalid-feedback pets_allowed"></span>
                        </div>

                        <h5>Additional rules</h5>
                        <div class="loc-form">
                            <textarea class="form-control mt-4 mb-3" rows="6" placeholder="“Enter additional rules if exists”" name="additional_rules">{{$data->additional_rules}}</textarea>
                            <span class="invalid-feedback additional_rules"></span>
                        </div>
                    </div>

                    <div class="pt-3 pb-3">
                        <hr>
                    </div>
                    <div class="clearfix"></div>
        
                    <div class="row">
                        <div class="col-md-6 text-right">
                            <button class="red-btn rounded float-right submit_step_4" type="button">
                                Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>