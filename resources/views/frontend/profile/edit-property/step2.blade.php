<section class="step2 listPropertySteps">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h5>Dimensions</h5>
                <p>What are the Dimensions of your House / Apartment?</p>
                <div class="loc-form" name="rent_sale_property_step_2" id="rent_sale_property_step_2">
                   
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <input class="form-control" type="text" name="build_up_area" placeholder="Enter super build up area in sq. ft." value="{{$data->build_up_area}}">
                                <span class="invalid-feedback build_up_area"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select class="form-control rounded" name="build_up_area_unit">
                                    <option value="sqft" @if($data->build_up_area_unit=='sqft') selected @endif>Sq Ft</option>
                                    <option value="sqmtr" @if($data->build_up_area_unit=='sqmtr') selected @endif>Sq Mtr</option>
                                </select>
                                <span class="invalid-feedback build_up_area_unit"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9">
                            <div class="form-group">
                                <input class="form-control" type="text" name="carpet_area" placeholder="Enter carpet area in sq. ft." value="{{$data->carpet_area}}">
                                <span class="invalid-feedback carpet_area"></span>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <select class="form-control rounded" name="carpet_area_unit">
                                    <option value="sqft" @if($data->carpet_area_unit=='sqft') selected @endif>Sq Ft</option>
                                    <option value="sqmtr" @if($data->carpet_area_unit=='sqmtr') selected @endif>Sq Mtr</option>
                                </select>
                                <span class="invalid-feedback carpet_area_unit"></span>
                            </div>
                        </div>
                    </div>
                
                    <div class="row mt-4">
                        <div class="col-md-7">
                            <p class="mb-4"><img class="mr-3" src="{{asset('img/rooms.png')}}"> No of bedrooms</p>
                            <p class="mb-4"><img class="mr-3" src="{{asset('img/bathrooms.png')}}"> No of bathrooms</p>
                        </div>
                        <div class="col-md-5">
                            <div class="number mb-2">
                                <span class="minus"><small>-</small></span>
                                <input class="dp-inline-block no-border" type="text" readonly="" name="no_of_bedrooms" value="{{$data->no_of_bedrooms}}" min="1" max="15" />
                                <span class="plus"><small>+</small></span>
                            </div>
                            <div class="number mb-4 pt-2">
                                <span class="minus"><small>-</small></span>
                                <input class="dp-inline-block no-border" type="text" readonly="" name="no_of_bathrooms" value="{{$data->no_of_bathrooms}}" min="1" max="15"/>
                                <span class="plus"><small>+</small></span>
                            </div>
                        </div>
                        <span class="invalid-feedback no_of_bedrooms"></span>
                        <span class="invalid-feedback no_of_bathrooms"></span>
                    </div>

                    <h5 class="mt-4">Furnishing</h5>
                    <p>Tell us about the furnishing that your apartment has.</p>
                    
                    <div class="boxed">
                        <input type="radio" id="fully_furnished" name="furnishing_type" value="fully_furnished" @if($data->furnishing_type=='fully_furnished') checked @endif>
                        <label for="fully_furnished" class="customCursorClass">Fully furnished</label>
                        
                        <input type="radio" id="semi_furnished" name="furnishing_type" value="semi_furnished" @if($data->furnishing_type=='semi_furnished') checked @endif>
                        <label for="semi_furnished" class="customCursorClass">Semi furnished</label>
                        
                        <input type="radio" id="unfurnished" name="furnishing_type" value="unfurnished" @if($data->furnishing_type=='unfurnished') checked @endif>
                        <label for="unfurnished" class="customCursorClass">Unfurnished</label>
                        <br>
                        <span class="invalid-feedback furnishing_type"></span>
                    </div>

                    <h5 class="mt-4">Flooring</h5>
                    <p>Tell us about the furnishing that your apartment has.</p>
                    <div class="row">
                        <div class="col-md-11">
                            <div class="loc-form">
                                <select class="form-control rounded" name="flooring_type">
                                    <option value="">Select type</option>
                                    @foreach(getAllFlooring() as $row)
                                        <option value="{{$row->name}}" @if($data->flooring_type==$row->name) selected @endif>{{$row->name}}</option>
                                    @endforeach
                                </select>
                                <span class="invalid-feedback flooring_type"></span>
                            </div>
                        </div>
                    </div>
                    <h5 class="mt-4 mb-3">Is There space for parking?</h5>
                    <div class="">
                        <div class="radio dp-inline-block mr-3" style="margin-bottom: 0;">
                            <input id="parking-radio-1" onchange="parkingAvailableRadio(this)" name="parking_available" type="radio" value="yes" autocomplete="off" @if($data->parking_available=='yes') checked @endif>
                            <label for="parking-radio-1" class="radio-label">Yes</label>
                        </div>
                        <div class="radio dp-inline-block mr-3" style="margin-bottom: 0;">
                            <input id="parking-radio-2" onchange="parkingAvailableRadio(this)" name="parking_available" type="radio" value="no" autocomplete="off" @if($data->parking_available=='no') checked @endif>
                            <label for="parking-radio-2" class="radio-label">No</label>
                        </div>
                        <br>
                        <span class="invalid-feedback parking_available"></span>
                    </div>

                    <div class="row mt-4 parking_available_div" style="display: @if($data->parking_available=='yes') '' @else none @endif;">
                        <div class="col-md-7">
                            <p class="mb-4">Covered parkings</p>
                            <p class="mb-4">Open parkings</p>
                        </div>
                        <div class="col-md-5">
                            <div class="number mb-2">
                                <span class="minus"><small>-</small></span>
                                <input class="dp-inline-block no-border" readonly="" type="text" name="no_of_covered_parking" value="{{$data->no_of_covered_parking}}">
                                <span class="plus"><small>+</small></span>
                            </div>
                            <div class="number mb-4 pt-2">
                                <span class="minus"><small>-</small></span>
                                <input class="dp-inline-block no-border" readonly="" name="no_of_open_parking" type="text" value="{{$data->no_of_open_parking}}">
                                <span class="plus"><small>+</small></span>
                            </div>
                            
                        </div>
                        <span class="invalid-feedback no_of_covered_parking"></span>
                        <span class="invalid-feedback no_of_open_parking"></span>
                        <div class="pt-3">
                            <hr>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6 text-right">
                            <button class="red-btn rounded float-right submit_step_2" type="button">
                                Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>