<section class="step1 listPropertySteps">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h5>Property Listing Type</h5>
                <form class="boxed second" name="rent_sale_property_step_1" id="rent_sale_property_step_1">
                    <div class="form-group">

                        <input type="radio" id="rent" name="listing_type" value="rent"  @if($data->listing_type=='rent') checked @endif>
                        <label for="rent" class="customCursorClass" style="text-align: center;">Rent</label>

                        <input type="radio" id="sale" name="listing_type" value="sale"  @if($data->listing_type=='sale') checked @endif>
                        <label for="sale" class="customCursorClass" style="text-align: center;">Sale</label>

                        <input type="radio" id="both" name="listing_type" value="both"  @if($data->listing_type=='both') checked @endif>
                        <label for="both" class="customCursorClass" style="text-align: center;">Both</label>
                        <br>
                        <span class="invalid-feedback listing_type"></span>
                    </div>
                
                    <h5>House Type</h5>
                    <p>Select the type of property that you are listing for?</p>
                    <div class="form-group">
                        @foreach(getAllHouseTypes() as $row)
                            <input type="radio" id="{{$row->name}}" name="house_type" value="{{$row->name}}" @if($data->house_type==$row->name) checked @endif>
                            <label for="{{$row->name}}" class="customCursorClass">
                                <img src="{{url('uploads/housetypes/'.$row->logo)}}"> 
                                <br>{{ucfirst($row->name)}}
                            </label>
                        @endforeach
                        <br>
                         <span class="invalid-feedback house_type"></span>
                    </div>
                
                    <h5>Location</h5>
                    <p>What is the location of the property you are listing for?</p>
                    <div class="row">
                        <div class="col-md-11">
                            <div class="form-group">
                                 <div id="map" style="display:none"></div>
                                <input type="text" class="form-control" onchange="getLocation()" placeholder="Enter Location Name" name="location_name" id="address" value="{{$data->location_name}}">
                                <span class="invalid-feedback location_name"></span>
                                <div class="row"  hidden >
                                  <div class="col-sm-6"  hidden>
                                     <input type="text" hidden name="latitude" id="lat" value="{{$data->latitude}}" readonly="">
                                        
                                  </div>
                                  <div class="col-sm-6">
                                    <input  type="text"  hidden name="longitude" id="lng" value="{{$data->longitude}}"  readonly="">
                                  </div>
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="House / Apartment No" name="house_number" value="{{$data->house_number}}">
                                        <span class="invalid-feedback house_number"></span>
                                       
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Landmark (optional)" name="landmark" value="{{$data->landmark}}">
                                        <span class="invalid-feedback landmark"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select class="form-control" name="country" id="countryTo">
                                            <option value="">Select Country</option>
                                            @foreach(getAllLocations() as $country)
                                                <option value="{{ $country->name }}" @if($data->country==$country->name) selected @endif>{{ __($country->name) }}</option>
                                            @endforeach
                                        </select>
                                        <span class="invalid-feedback country"></span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="pt-3">
                                <hr>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-6 text-right">
                            <button class="red-btn rounded float-right submit_step_1" type="button">
                                Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@if(env('APP_ACTIVE_MAP') =='patel_map')
<style>
    #address_result
    {
        z-index: 9;
    }
</style>
   <script src="https://mapapi.cloud.huawei.com/mapjs/v1/api/js?callback=initMap&key={{env('PATEL_MAP_KEY')}}"></script>
    <script>
        function initMap() {
            var mapOptions = {};
            var lat_long = <?php echo json_encode($data->location);?>;
            var title = '<?php echo ($data->location_name)?>';
            var locations = lat_long;
            console.log(lat_long);
            var myLatLng = {
                        lat: (locations[1] ? locations[1] : -25.363),
                        lng: (locations[0] ? locations[0] : 131.044)
                                };
            mapOptions.center = myLatLng;

            mapOptions.zoom = 8;
            mapOptions.language='ENG';
            var searchBoxInput = document.getElementById('address');
             var map = new HWMapJsSDK.HWMap(document.getElementById('map'), mapOptions);
            // Create the search box parameter.
            var acOptions = {
                location: {
                    lat: 48.856613,
                    lng: 2.352222
                },
                radius: 10000,
                customHandler: callback,
                language:'en'

            };
            var autocomplete;
            // Obtain the input element.
            // Create the HWAutocomplete object and set a listener.
            autocomplete = new HWMapJsSDK.HWAutocomplete(searchBoxInput, acOptions);
            autocomplete.addListener('site_changed', printSites);
            // Process the search result. 
            // index: index of a searched place.
            // data: details of a searched place. 
            // name: search keyword, which is in strong tags. 
            function callback(index, data, name) {
                
                var div
                div = document.createElement('div');
                div.innerHTML = name;
                return div;
            }
            // Listener.
            function printSites() {
                 
                
                var site = autocomplete.getSite();
                var latitude = site.location.lat;
                var longitude = site.location.lng;
                var country_name = site.address.country;
                
                var results = "Autocomplete Results:\n";
                var str = "Name=" + site.name + "\n"
                    + "SiteID=" + site.siteId + "\n"
                    + "Latitude=" + site.location.lat + "\n"
                    + "Longitude=" + site.location.lng + "\n"
                    + "Address=" + site.formatAddress + "\n";

                    
                results = results + str;
                console.log(site);
                 
                 

                $('#lat').val(latitude);
                 $('#lng').val(longitude);
                 $('#countryTo').val(country_name);
                console.log(latitude, longitude);
                /*console.log(results);*/
            }


              mMarker = new HWMapJsSDK.HWMarker({
                map: map,
                position: myLatLng,
                zIndex: 10,
                label: {
                    text: title,
                    offsetY: -30,
                    fontSize: '20px'
                },
                icon: {
                    opacity:1,
                    scale: 1.2,
                }
            });
            
        }
        function getLocation()
        {

        }
    </script> 

     
    @elseif(env('APP_ACTIVE_MAP')=='google_map')

<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&callback=initMap&libraries=places&v=weekly"
      defer
    ></script>
 
<script>

function initMap() {
  var lat_long = <?php echo json_encode($data->location);?>;
            var title = '<?php echo ($data->location_name)?>';
            var locations = lat_long;
        const myLatLng = {
                        lat: (locations[1] ? locations[1] : -25.363),
                        lng: (locations[0] ? locations[0] : 131.044)
                                };
    
      

  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 4,
    center: myLatLng
  });
    var input = document.getElementById('address');

 map.controls[google.maps.ControlPosition.TOP_RIGHT].push();

var autocomplete = new google.maps.places.Autocomplete(input);
autocomplete.bindTo('bounds', map);
autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);
            var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29),
          title: "Hello World!"
        });
        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindow.open(map, marker);
        });
  new google.maps.Marker({
    position: myLatLng,
    map,
    title: "Hello World!"
  });
}

function getLocation()
 {
//   alert(address);
  var address = $('#address').val();
  // alert(address);
  var geocoder = new google.maps.Geocoder();
  $('#address').addClass('error is-invalid');
  $('.invalid-feedback.location_name').text('Geo Location Not Set Properly .Please Select.');
  $('#address').focus();
  geocoder.geocode( { 'address': address}, function(results, status)
  {

    
    if (status == google.maps.GeocoderStatus.OK)
    {
      console.log(results[0],"afdcfsa");
      console.log(results[0].address_components);
      const countryObj=results[0].address_components.filter(val=>{
            if(val.types.includes('country')){
                return true
            }
             
            return false;
      });
     console.log(countryObj,'the country obj');


     var country_name = countryObj[0].long_name;
     console.log(country_name,'name');
    var latitude = results[0].geometry.location.lat();
    var longitude = results[0].geometry.location.lng();
    $('#address').removeClass('error is-invalid');
    $('.invalid-feedback.location_name').text('');

    $('#lat').val(latitude);
    $('#lng').val(longitude);
     /*$('#countryTo').val(country_name);
     $('#country_name').val(country_name);*/
    console.log(latitude, longitude);
    }
  });

}
    </script>

<!-- 
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&callback=initMap&libraries=places&v=weekly" defer ></script>
<script type="text/javascript">
    
      function initMap() {
         var lat_long = <?php echo json_encode($data->location);?>;
            var title = '<?php echo ($data->location_name)?>';
            var locations = lat_long;
        const myLatLng = {
                        lat: (locations[1] ? locations[1] : -25.363),
                        lng: (locations[0] ? locations[0] : 131.044)
                                };
 
    
      

  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 8,
    center: myLatLng
  });
    var input = document.getElementById('address');

 map.controls[google.maps.ControlPosition.TOP_RIGHT].push();

var autocomplete = new google.maps.places.Autocomplete(input);
autocomplete.bindTo('bounds', map);
autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);
            var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29),
          title: "Hello World!"
        });
        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
             
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }
 
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);   
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindow.open(map, marker);
        });
  new google.maps.Marker({
    position: myLatLng,
    map,
    title: "Hello World!"
  });
}

function getLocation()
 {

  var address = $('#address').val();
    
 
  var geocoder = new google.maps.Geocoder();

  geocoder.geocode( { 'address': address}, function(results, status)
  {


    if (status == google.maps.GeocoderStatus.OK)
    {
      console.log(results[0],"afdcfsa");
      console.log(results[0].address_components);
      const countryObj=results[0].address_components.filter(val=>{
            if(val.types.includes('country')){
                return true
            }
            return false;
      });
 

     var country_name = countryObj[0].long_name;
      
      var latitude = results[0].geometry.location.lat();

      var longitude = results[0].geometry.location.lng();
      $('#lat').val(latitude);
     $('#lng').val(longitude);
     $('#countryTo').val(country_name);
      
    console.log(latitude, longitude);
    } 
  });

}
</script> -->
@endif