@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
@extends('frontend.layouts.home')
@section('title','Edit Property')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/datepicker.css')}}">
@endsection

@section('content')
@include('frontend.layouts.default-header')

<section class="inner-body body-light-grey mt-70">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="list-inline breadcrum">
                    <li><a href="{{url(app()->getLocale().'/home/'.$link)}}">Home </a></li>
                    <span> / </span>
                    <li><a href="{{url(app()->getLocale().'/mylistings/'.$link)}}"> My Properties</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <!-- Navigation Block -->
                <div class="inner-white-wrapper left-navigation mb-3">
@include('frontend.profile.user_profile_sidebar')
                </div>
            </div>
            <input type="hidden" name="property_id" value="{{$data->_id}}">
            <!-- Right Wrapper -->
            @include('frontend.profile.edit-property.main')
        </div>
    </div>
</section>
@include('frontend.layouts.footer')
@endsection

@section('scripts')
<script type="text/javascript" src="{{asset('js/datepicker.js')}}"></script>
<script type="text/javascript">

  
    let propertySteps = false;
    const property_id = $('input[name="property_id"]').val();
function file_validation(id,max_size)
   {
       var fuData = document.getElementById(id);
       var FileUploadPath = fuData.value;
       
   
       if (FileUploadPath == ''){
           alert("Please upload Attachment");
       } 
       else {
           var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
           if (Extension == "jpg" || Extension == "jpeg" || Extension == "png" || Extension == "webp" ) {
   
                   if (fuData.files && fuData.files[0]) {
                       var size = fuData.files[0].size;
                       
                       if(size > max_size){   //1000000 = 1 mb
                           
                           Swal.fire({
                                title: 'Warning!',
                                text: "Maximum file size "+max_size/1000000+" MB",
                                icon: 'info',
                                confirmButtonText: 'Ok'
                            });
                            
                           $("#"+id).val('');
                           return false;
                            
                       }
                   }
   
           } 
           else 
           {
                
               $("#"+id).val('');
                Swal.fire({
                                title: 'Warning!',
                                text: "document only allows file types of  jpg , png  , jpeg , webp ",
                                icon: 'info',
                                confirmButtonText: 'Ok'
                            });
                            
                           $("#"+id).val('');
                           return false;
           }
       }   
   } 

    const readURL = function(input) {

        var id = $(input).attr('id');
         var max_size = 2000000;
           file_validation(id,max_size);
           
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var id=input.id;
                $('#'+id+'_src').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            
        }
    }

    const toggleInstantBooking = function(divid){
        if($('#'+divid).is(':visible')){
            $('#'+divid).hide();
        }else{
            $('#'+divid).show();
        }
    }

    const parkingAvailableRadio = function(input){
        if(input.value=='no'){
            $('.parking_available_div').hide();
        }
        else if(input.value=='yes'){
            $('.parking_available_div').show();
        }
    }

    $(document).ready(function(){
        $("html, body").animate({ scrollTop: 0 }, "slow");

        $('.datepicker').datepicker({
            'startDate': new Date(),
            'format': 'dd/mm/yyyy',
            'autoHide':true
        });
        
        $(".minus").click(function() {
            var $input = $(this).parent().find("input");
            var count = parseInt($input.val()) - 1;
            count = count < 1 ? 0 : count;
            $input.val(count);
            $input.change();
            return false;
        });

        $(".plus").click(function() {
            var $input = $(this).parent().find("input");
            if(parseInt($input.val())>=10){
                return false;
            }
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });

        $(document).on('click','.amenitiesBlock', function(){
            $(this).toggleClass('active');
        });

        $(document).on('click','#add_more', function(){
            if($('.upload').length>=15){
                Swal.fire({
                    title: 'Warning!',
                    text: "You can upload maximum 15 pictures.",
                    icon: 'info',
                    confirmButtonText: 'Ok'
                });
                return false;
            }
            var id= $('.upload').length+1;
            $("#pictureresult").append('\
                <div class="upload col-md-3">\
                    <input type="file" id="files'+id+'" name="files[]" class="input-file" onchange="readURL(this)" >\
                    <label for="files'+id+'">\
                        <img id="files'+id+'_src" src="{{asset("img/addmedia-upload.png")}}" style="height:120px;width:120px;">\
                    </label>\
                    <span class="remove removeDiv" style="cursor:pointer;"><i class="fa fa-times" aria-hidden="true"></i></span>\
                    <span class="invalid-feedback files'+id+'"></span>\
                </div>');
        });

        $(document).on('click','.removeDiv', function(){
            $(this).parent().remove();
        });

        // save first form of property
        $(document).on('click','.submit_step_1', function(){
            var ajax_url = WEBSITE_URL+"/updateProperty/rentsale/1";

            var $form = $('#rent_sale_property_step_1');
            var data = new FormData();
            var listing_type = $('input[name="listing_type"]:checked').val() ? $('input[name="listing_type"]:checked').val(): '';
            var house_type = $('input[name="house_type"]:checked').val() ? $('input[name="house_type"]:checked').val(): '';

            data.append('house_type', house_type);
            data.append('listing_type', listing_type);
            data.append('location_name', $('input[name="location_name"]').val());
            data.append('latitude', $('input[name="latitude"]').val());
            data.append('longitude', $('input[name="longitude"]').val());
            data.append('house_number', $('input[name="house_number"]').val());
            data.append('landmark', $('input[name="landmark"]').val());
            data.append('country', $('select[name="country"]').val());
            data.append('property_id', property_id);

            $form.find('.is-invalid').removeClass('is-invalid');
            $form.find('.invalid-feedback').text('');

            $.ajax({
                url : ajax_url,
                type:'post',
                data : data,
                dataType : 'json',
                async:true,
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.listPropertySteps');
                },
                complete:function(){
                   stopLoader('.listPropertySteps'); 
                },
                success : function(data){
                    console.log(data.status);
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then(function(){
                            window.location.reload();
                        });

                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                },
                error : function(data){
                    stopLoader('.listPropertySteps');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                        $.each(err_response.errors, function(i, obj){
                            $form.find('span.invalid-feedback.'+i+'').text(obj).show();
                            $form.find('input[name="'+i+'"]').addClass('is-invalid');
                            $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
                            $form.find('select[name="'+i+'"]').addClass('is-invalid');
                        });
                    }
                }
            });
        });

        $(document).on('click','.submit_step_2', function(){
            var ajax_url = WEBSITE_URL+"/updateProperty/rentsale/2";
            var $form = $('#rent_sale_property_step_2');
            var data = new FormData();

            var furnishing_type = $('input[name="furnishing_type"]:checked').val() ? $('input[name="furnishing_type"]:checked').val(): '';
            var parking_available = $('input[name="parking_available"]:checked').val() ? $('input[name="parking_available"]:checked').val(): '';

            data.append('furnishing_type', furnishing_type);
            data.append('parking_available', parking_available);
            data.append('build_up_area', $('input[name="build_up_area"]').val());
            data.append('carpet_area', $('input[name="carpet_area"]').val());
            data.append('no_of_bedrooms', $('input[name="no_of_bedrooms"]').val());
            data.append('no_of_bathrooms', $('input[name="no_of_bathrooms"]').val());
            data.append('no_of_covered_parking', $('input[name="no_of_covered_parking"]').val());
            data.append('no_of_open_parking', $('input[name="no_of_open_parking"]').val());
            data.append('flooring_type', $('select[name="flooring_type"]').val());
            data.append('build_up_area_unit', $('select[name="build_up_area_unit"]').val());
            data.append('carpet_area_unit', $('select[name="carpet_area_unit"]').val());

            data.append('property_id', property_id);

            $form.find('.is-invalid').removeClass('is-invalid');
            $form.find('.invalid-feedback').text('');

            $.ajax({
                url : ajax_url,
                type:'post',
                data : data,
                async:true,
                dataType : 'json',
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.listPropertySteps');
                },
                complete:function(){
                   stopLoader('.listPropertySteps'); 
                },
                success : function(data){
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then(function(){
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        });
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                },
                error : function(data){
                    stopLoader('.listPropertySteps');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                        $.each(err_response.errors, function(i, obj){
                            $form.find('span.invalid-feedback.'+i+'').text(obj).show();
                            $form.find('input[name="'+i+'"]').addClass('is-invalid');
                            $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
                            $form.find('select[name="'+i+'"]').addClass('is-invalid');
                        });
                    }
                }
            });
        });

        $(document).on('click','.submit_step_3', function(){
            var ajax_url = WEBSITE_URL+"/updateProperty/rentsale/3";
            var data = new FormData();
            var count = 0;
            $('.amenitiesBlock').each(function(i){
                if($(this).is('.active')){
                    data.append('amenties[]', $(this).attr('data-name'));
                    count++;
                }
            });
            if(count==0){
                Swal.fire({
                    title: 'Warning!',
                    text: "Please select some amenties",
                    icon: 'info',
                    confirmButtonText: 'Ok'
                });
                return false;
            }
            data.append('property_id', property_id);

            $.ajax({
                url : ajax_url,
                type:'post',
                data : data,
                dataType : 'json',
                async:true,
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.listPropertySteps');
                },
                complete:function(){
                   stopLoader('.listPropertySteps'); 
                },
                success : function(data){
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then(function(){
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        });
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                },
                error : function(data){
                    stopLoader('.listPropertySteps');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                    }
                }
            });
        });

        $(document).on('click','.submit_step_4', function(){
            var ajax_url = WEBSITE_URL+"/updateProperty/rentsale/4";
            var $form = $('#rent_sale_property_step_4');

            var data = new FormData();

            var smoking_allowed = $('input[name="smoking_allowed"]:checked').val() ? $('input[name="smoking_allowed"]:checked').val(): '';
            var parties_allowed = $('input[name="parties_allowed"]:checked').val() ? $('input[name="parties_allowed"]:checked').val(): '';
            var pets_allowed = $('input[name="pets_allowed"]:checked').val() ? $('input[name="pets_allowed"]:checked').val(): '';

            data.append('smoking_allowed', smoking_allowed);
            data.append('pets_allowed', pets_allowed);
            data.append('parties_allowed', parties_allowed);
            data.append('property_title', $('input[name="property_title"]').val());
            data.append('property_description', $('textarea[name="property_description"]').val());
            data.append('additional_rules', $('textarea[name="additional_rules"]').val());
            data.append('property_id', property_id);

            $form.find('.is-invalid').removeClass('is-invalid');
            $form.find('.invalid-feedback').text('');

            $.ajax({
                url : ajax_url,
                type:'post',
                data : data,
                async:true,
                dataType : 'json',
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.listPropertySteps');
                },
                complete:function(){
                   stopLoader('.listPropertySteps'); 
                },
                success : function(data){
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then(function(){
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        });
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                },
                error : function(data){
                    stopLoader('.listPropertySteps');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                        $.each(err_response.errors, function(i, obj){
                            $form.find('span.invalid-feedback.'+i+'').text(obj).show();
                            $form.find('input[name="'+i+'"]').addClass('is-invalid');
                            $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
                            $form.find('select[name="'+i+'"]').addClass('is-invalid');
                        });
                    }
                }
            });
        });

        $(document).on('click','.submit_step_5', function(){
            var ajax_url = WEBSITE_URL+"/updateProperty/rentsale/5";
            var $form = $('#rent_sale_property_step_5');
            var data = new FormData($('#rent_sale_property_step_5')[0]);
            var error = false;
            $form.find('.is-invalid').removeClass('is-invalid');
            $form.find('.invalid-feedback').text('');
            
            $.each($('#pictureresult [type=file]'), function(index, file) {
                if($('input[type=file]')[index].files[0]==undefined){
                    var id = $('input[type=file]')[index].id;
                    $('.invalid-feedback.'+(id)).text('The file is required.').show();
                    error = true;
                }
            });

            data.append('property_id', property_id);

            if(error){
                return false;
            }
            if($('.upload').length==0){
                Swal.fire({
                    title: 'Warning!',
                    text: "Please upload minimum 5 pictures.",
                    icon: 'info',
                    confirmButtonText: 'Ok'
                });
                return false;
            }
            if($('.upload').length<5){
                Swal.fire({
                    title: 'Warning!',
                    text: "Please upload minimum 5 pictures.",
                    icon: 'info',
                    confirmButtonText: 'Ok'
                });
                return false;
            }

            $.ajax({
                url : ajax_url,
                type:'post',
                data : data,
                enctype: 'multipart/form-data',
                dataType : 'json',
                async:true,
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.listPropertySteps');
                },
                complete:function(){
                   stopLoader('.listPropertySteps'); 
                },
                success : function(data){
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then(function(){
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        });
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                },
                error : function(data){
                    stopLoader('.listPropertySteps');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                        $.each(err_response.errors, function(key, value){
                            if(key.indexOf('.') != -1) {
                                let keys = key.split('.');                                
                                $('input[name="'+keys[0]+'[]"]').eq(keys[1]).addClass('is-invalid').parent().find('.invalid-feedback').text(value).show();                         
                            }
                            else {
                                $('input[name="'+key+'[]"]').eq(0).addClass('is-invalid').parent().find('.invalid-feedback').text(value).show();
                            }
                        });
                    }
                }
            });
        });

        $(document).on('click','.submit_step_6', function(){
            var ajax_url = WEBSITE_URL+"/updateProperty/rentsale/6";
            var $form = $('#rent_sale_property_step_6');
            var data = new FormData();

            $form.find('.is-invalid').removeClass('is-invalid');
            $form.find('.invalid-feedback').text('');
            var listing_type = $('input[name="listing_type"]:checked').val();

            data.append('listing_type', listing_type);

            if(listing_type=='rent')
            {
                var instant_book_available = $('input[name="instant_book_available"]:checked').attr('value') ? $('input[name="instant_book_available"]:checked').attr('value'): '';
                data.append('instant_book_available', instant_book_available);
                data.append('night_price', $('input[name="night_price"]').val());
                data.append('rent', $('input[name="rent"]').val());
                data.append('maintenance_charges', $('input[name="maintenance_charges"]').val());
                data.append('security_amount', $('input[name="security_amount"]').val());
                data.append('available_from', $('input[name="available_from"]').val());
            }
            else if(listing_type=='sale'){
                var discount_available = $('input[name="discount_available"]:checked').attr('value') ? $('input[name="discount_available"]:checked').attr('value'): '';
                data.append('discount_available', discount_available);
                data.append('price', $('input[name="price"]').val());
                data.append('discount_price', $('input[name="discount_price"]').val());
            }else{
                var instant_book_available = $('input[name="instant_book_available"]:checked').attr('value') ? $('input[name="instant_book_available"]:checked').attr('value'): '';
                data.append('instant_book_available', instant_book_available);
                data.append('night_price', $('input[name="night_price"]').val());
                data.append('rent', $('input[name="rent"]').val());
                data.append('maintenance_charges', $('input[name="maintenance_charges"]').val());
                data.append('security_amount', $('input[name="security_amount"]').val());
                data.append('available_from', $('input[name="available_from"]').val());

                var discount_available = $('input[name="discount_available"]:checked').attr('value') ? $('input[name="discount_available"]:checked').attr('value'): '';
                data.append('discount_available', discount_available);
                data.append('price', $('input[name="price"]').val());
                data.append('discount_price', $('input[name="discount_price"]').val());
            }

            data.append('price_currency', $('select[name="price_currency"]').val());
            data.append('discount_price_currency', $('select[name="discount_price_currency"]').val());
            data.append('rent_currency', $('select[name="rent_currency"]').val());
            data.append('maintenance_charges_currency', $('select[name="maintenance_charges_currency"]').val());
            data.append('security_amount_currency', $('select[name="security_amount_currency"]').val());
            data.append('night_price_currency', $('select[name="night_price_currency"]').val());
            data.append('rent_frequency', $('select[name="rent_frequency"]').val());
            
            data.append('property_id', property_id);

            $.ajax({
                url : ajax_url,
                type:'post',
                data : data,
                dataType : 'json',
                async:true,
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.listPropertySteps');
                },
                complete:function(){
                   stopLoader('.listPropertySteps'); 
                },
                success : function(data){
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then(function(){
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        });
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                },
                error : function(data){
                    stopLoader('.listPropertySteps');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                        $.each(err_response.errors, function(i, obj){
                            $form.find('span.invalid-feedback.'+i+'').text(obj).show();
                            $form.find('input[name="'+i+'"]').addClass('is-invalid');
                            $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
                            $form.find('select[name="'+i+'"]').addClass('is-invalid');
                        });
                    }
                }
            });
        });
    });



</script>
@endsection