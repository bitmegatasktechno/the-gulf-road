<section class="step6 listPropertySteps" >
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="loc-form" name="rent_sale_property_step_6" id="rent_sale_property_step_6">
                    @if($data->listing_type=="sale" || $data->listing_type=="both")
                        <div class="sale_price_list">
                            <h5 >Set Price For Sale</h5>
                            <p>Set the amount that you are going to list this propert for. Keep the price reasonable.</p>
                            <div class="row">
                                <div class="col-md-9">
                                    <h5>Price</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input class="form-control" type="number" min="0" name="price" placeholder="Enter Price Of Property" value="{{$data->price}}">
                                        <span class="invalid-feedback price"></span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control rounded" name="price_currency">
                                            @foreach(getAllCurrencies() as $row)
                                                <option value="{{$row->code}}" @if($data->price_currency==$row->code) selected @endif>{{$row->code}}</option>
                                            @endforeach
                                        </select>
                                        <span class="invalid-feedback price_currency"></span>
                                    </div>
                                </div>
                            </div>
                        
                            <div class="row mt-3">
                                <div class="col-md-7">
                                    <h5>Give Discount for this property</h5>
                                </div>
                                <div class="col-md-3">
                                    <div class="checkbox switcher">
                                        <a href="javascript:;">
                                        <label for="discount_check">
                                            <input type="checkbox" @if($data->discount_available=='checked') checked @endif name="discount_available" id="discount_check" value="checked" onchange="toggleInstantBooking('discount_price');">
                                            <span><small></small></span>
                                        </label></a>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="discount_price" style="display: @if($data->discount_available=='checked') '' @else none @endif;">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="discount_price" placeholder="Enter Price After Discount" value="{{$data->discount_price}}">
                                        <span class="invalid-feedback discount_price"></span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control rounded" name="discount_price_currency">
                                            @foreach(getAllCurrencies() as $row)
                                                <option value="{{$row->code}}" @if($data->discount_price_currency==$row->code) selected @endif>{{$row->code}}</option>
                                            @endforeach
                                        </select>
                                        <span class="invalid-feedback discount_price_currency"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    @if($data->listing_type=="both")
                        <div class="pb-3">
                            <hr>
                        </div>
                    @endif

                    <!-- list for rent -->
                    @if($data->listing_type=="rent" || $data->listing_type=="both")
                        <div class="rent_price_list">
                           <h5 >Set Price For Rent</h5>
                            <p>Set the amount that you are going to list this propert for. Keep the price reasonable.</p>
                            
                            <div class="row">
                                <div class="col-md-9">
                                    <h5>Rent Frequency</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <select class="form-control rounded" name="rent_frequency">
                                            @foreach(getAllRentFrequency() as $key=>$value)
                                                <option value="{{$key}}"  @if($data->rent_frequency==$key) selected @endif>{{ucfirst($value)}}</option>
                                            @endforeach
                                        </select>
                                        <span class="invalid-feedback rent_frequency"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-9">
                                    <h5>Rent For This Property</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input class="form-control" type="number" min="0" name="rent" placeholder="Enter Rent Price" value="{{$data->rent}}">
                                        <span class="invalid-feedback rent"></span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control rounded" name="rent_currency">
                                            @foreach(getAllCurrencies() as $row)
                                                <option value="{{$row->code}}" @if($data->rent_currency==$row->code) selected @endif>{{$row->code}}</option>
                                            @endforeach
                                        </select>
                                        <span class="invalid-feedback rent_currency"></span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <h5>Maintenance Charges (per month)</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input class="form-control" type="number" min="0" name="maintenance_charges" placeholder="Enter Maintenance Charges" value="{{$data->maintenance_charges}}">
                                        <span class="invalid-feedback maintenance_charges"></span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control rounded" name="maintenance_charges_currency">
                                            @foreach(getAllCurrencies() as $row)
                                                <option value="{{$row->code}}" @if($data->maintenance_charges_currency==$row->code) selected @endif>{{$row->code}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <h5>Security Amount</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input class="form-control" type="number" min="0" name="security_amount" placeholder="Enter Security Amount" value="{{$data->security_amount}}">
                                        <span class="invalid-feedback security_amount"></span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control rounded" name="security_amount_currency">
                                            @foreach(getAllCurrencies() as $row)
                                                <option value="{{$row->code}}" @if($data->security_amount_currency==$row->code) selected @endif>{{$row->code}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-md-12">
                                    <h5>Available From</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input class="form-control datepicker" type="text" name="available_from" placeholder="dd/mm/yyyy" value="{{$data->available_from}}">
                                        <span class="invalid-feedback available_from"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="col-md-7">
                                    <h5>List This Property For Instant Booking</h5>
                                </div>
                                <div class="col-md-3">
                                    <div class="checkbox switcher">
                                        <a href="javascript:;">
                                            <label for="instant_book">
                                                <input type="checkbox" name="instant_book_available" id="instant_book" value="checked" onchange="toggleInstantBooking('instant_price');" @if($data->instant_book_available=='checked') checked @endif>
                                                <span><small></small></span>
                                            </label>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <small class="info-text">What is instant booking?<img class="ml-2" src="{{asset('img/info.png')}}"></small>
                            
                            <div class="row" id="instant_price" style="display: @if($data->instant_book_available=='checked') '' @else none @endif;">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="night_price" placeholder="Set Per Night Rate Of Property" value="{{$data->night_price}}">
                                        <span class="invalid-feedback night_price"></span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control rounded" name="night_price_currency">
                                            @foreach(getAllCurrencies() as $row)
                                                <option value="{{$row->code}}" @if($data->night_price_currency==$row->code) selected @endif>{{$row->code}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="clearfix"></div>
                <hr>
                <div class="row">
                    <div class="col-md-6 text-right">
                        <button class="red-btn rounded float-right submit_step_6" type="button">
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
