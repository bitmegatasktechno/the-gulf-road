<section class="step5 listPropertySteps">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h5 >Add Pictures</h5>
                <p>Add minimum 5 Pictures of your House. This helps the user better know about the property and the chances of contacting you increase.</p>
                
                <form class="boxed second" name="rent_sale_property_step_5" id="rent_sale_property_step_5" enctype="multipart/form-data">
                    
                    <div id="pictureresult" class="row">
                        @foreach($data->files as $row)
                            <div class="upload col-md-3">
                                <label for="files1">
                                    <input type="hidden" name="exist_file[]" value="{{$row}}">
                                    <img id="files1_src" class="already_exit_pic" data-name="{{$row}}" src="{{url('uploads/properties/'.$row)}}" style="height:120px;width:120px;">
                                </label>
                                <span class="remove removeDiv" style="cursor:pointer;"><i class="fa fa-times" aria-hidden="true"></i></span>
                            </div>
                        @endforeach
                    </div>
                    
                    <br>
                    <button type="button" id="add_more" class="red-btn rounded" value="Add More Files">Add More</button>
                </form>

                <div class="clearfix"></div>
                <hr>
                <div class="row">
                    <div class="col-md-6 text-right">
                        <button class="red-btn rounded float-right submit_step_5" type="button">
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>