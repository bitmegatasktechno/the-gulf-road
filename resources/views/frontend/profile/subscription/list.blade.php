<div class="col-md-9">
	<div class="card p-4">
	    <h3>My Subscription Plans</h3><hr>
	    @if($buddyAgent->count() > 0)
	    <div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="rentListing">
				<h5>Buddy Or Agent</h5>
			    <div class="row mt-4">
				        @foreach($buddyAgent as $row)
		                        <div class="col-md-4">
		                        	@if($row->expiry_date < date('Y-m-d') && $row->purchase_type != 'agent' && $row->purchase_type != 'buddy')
		                        	<button data-type="{{$row->purchase_type}}" data-id="{{$row->id}}" onclick="renewSub(this)">Renew</button>
		                        	@endif
                                <!-- <a href="{{url(app()->getLocale().'/mysubscription_property/'.$link.'/'.@$row->plan_id.'/'.@$row->purchase_type)}}"> -->
		                            <div class="property-card bg-white rounded">
	                                    <img class="img-fluid property-img" src="{{asset('../uploads/subscription/'.@$row->subscription->logo)}}" style="height: 120px !important;display: block;margin: 0 auto;width: auto;">
		                                <div class="pl-3 pr-3 mt-3">
										<h4 style="text-align: center;font-size: 19px;color: #E4002B;"><strong>{{ucfirst(@$row->purchase_type)}}</strong></h4>
		                                <h5 style="text-align: center;"><strong>{{ucfirst(@$row->subscription->name)}}</strong></h5>
		                                    <p class="mb-2">
		                                    	<strong>Amount</strong> :  ${{@$row->amount}}
		                                    </p>
                                            <p class="mb-2">
											<strong>Purchase Date</strong> : {{date('d M Y', strtotime(@$row->purchase_date))}}
		                                    </p>
                                            <p class="mb-2">
											<strong>Expiry Date</strong> : {{date('d M Y', strtotime(@$row->expiry_date))}}
		                                    </p>
		                                    <p class="mb-2">
											<strong>Validity</strong>  : {{date_diff(date_create(@$row->expiry_date),date_create(@$row->purchase_date))->days}} Days
		                                    </p>
                                            <p class="mb-2">
											<strong>Property Can be Listed</strong>  : {{@$row->count}}
		                                    </p>
                                            <p class="mb-2">
											<strong>Property Listed</strong>  : {{@$row->listed}}
		                                    </p>
		                                </div>
		                            </div>
                                   <!--  </a> -->
		                        </div>
	                    @endforeach
			    </div>
			</div>
	    </div>
	    @endif
	    @if($rent->count() > 0)
	    <div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="rentListing">
				<h5>Rent Or Buy</h5>
			    <div class="row mt-4">
				        @foreach($rent as $row)
		                        <div class="col-md-4">
		                        	@if($row->expiry_date < date('Y-m-d') && $row->purchase_type != 'agent' && $row->purchase_type != 'buddy')
		                        	<button data-type="{{$row->purchase_type}}" data-id="{{$row->id}}" onclick="renewSub(this)">Renew</button>
		                        	@endif
                                <a href="{{url(app()->getLocale().'/mysubscription_property/'.$link.'/'.@$row->plan_id.'/'.@$row->purchase_type)}}">
		                            <div class="property-card bg-white rounded">
	                                    <img class="img-fluid property-img" src="{{asset('../uploads/subscription/'.@$row->subscription->logo)}}" style="height: 120px !important;display: block;margin: 0 auto;width: auto;">
		                                <div class="pl-3 pr-3 mt-3">
										<h4 style="text-align: center;font-size: 19px;color: #E4002B;"><strong>{{ucfirst(@$row->purchase_type)}}</strong></h4>
		                                <h5 style="text-align: center;"><strong>{{ucfirst(@$row->subscription->name)}}</strong></h5>
		                                    <p class="mb-2">
		                                    	<strong>Amount</strong> :  ${{@$row->amount}}
		                                    </p>
                                            <p class="mb-2">
											<strong>Purchase Date</strong> : {{date('d M Y', strtotime(@$row->purchase_date))}}
		                                    </p>
                                            <p class="mb-2">
											<strong>Expiry Date</strong> : {{date('d M Y', strtotime(@$row->expiry_date))}}
		                                    </p>
		                                    <p class="mb-2">
											<strong>Validity</strong>  : {{date_diff(date_create(@$row->expiry_date),date_create(@$row->purchase_date))->days}} Days
		                                    </p>
                                            <p class="mb-2">
											<strong>Property Can be Listed</strong>  : {{@$row->count}}
		                                    </p>
                                            <p class="mb-2">
											<strong>Property Listed</strong>  : {{@$row->listed}}
		                                    </p>
		                                </div>
		                            </div>
                                    </a>
		                        </div>
	                    @endforeach
			    </div>
			</div>
	    </div>
	    @endif
	    @if($cospace->count() > 0)
	    <div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="rentListing">
				<h5>Coworkspace</h5>
			    <div class="row mt-4">
				        @foreach($cospace as $row)
		                        <div class="col-md-4">
		                        	@if($row->expiry_date < date('Y-m-d') && $row->purchase_type != 'agent' && $row->purchase_type != 'buddy')
		                        	<button data-type="{{$row->purchase_type}}" data-id="{{$row->id}}" onclick="renewSub(this)">Renew</button>
		                        	@endif
                                <a href="{{url(app()->getLocale().'/mysubscription_property/'.$link.'/'.@$row->plan_id.'/'.@$row->purchase_type)}}">
		                            <div class="property-card bg-white rounded">
	                                    <img class="img-fluid property-img" src="{{asset('../uploads/subscription/'.@$row->subscription->logo)}}" style="height: 120px !important;display: block;margin: 0 auto;width: auto;">
		                                <div class="pl-3 pr-3 mt-3">
										<h4 style="text-align: center;font-size: 19px;color: #E4002B;"><strong>{{ucfirst(@$row->purchase_type)}}</strong></h4>
		                                <h5 style="text-align: center;"><strong>{{ucfirst(@$row->subscription->name)}}</strong></h5>
		                                    <p class="mb-2">
		                                    	<strong>Amount</strong> :  ${{@$row->amount}}
		                                    </p>
                                            <p class="mb-2">
											<strong>Purchase Date</strong> : {{date('d M Y', strtotime(@$row->purchase_date))}}
		                                    </p>
                                            <p class="mb-2">
											<strong>Expiry Date</strong> : {{date('d M Y', strtotime(@$row->expiry_date))}}
		                                    </p>
		                                    <p class="mb-2">
											<strong>Validity</strong>  : {{date_diff(date_create(@$row->expiry_date),date_create(@$row->purchase_date))->days}} Days
		                                    </p>
                                            <p class="mb-2">
											<strong>Property Can be Listed</strong>  : {{@$row->count}}
		                                    </p>
                                            <p class="mb-2">
											<strong>Property Listed</strong>  : {{@$row->listed}}
		                                    </p>
		                                </div>
		                            </div>
                                    </a>
		                        </div>
	                    @endforeach
			    </div>
			</div>
	    </div>
	    @endif
	    @if($swap->count() > 0)
	    <div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="rentListing">
				<h5>Swap</h5>
			    <div class="row mt-4">
				        @foreach($swap as $row)
		                        <div class="col-md-4">
		                        	@if($row->expiry_date < date('Y-m-d') && $row->purchase_type != 'agent' && $row->purchase_type != 'buddy')
		                        	<button data-type="{{$row->purchase_type}}" data-id="{{$row->id}}" onclick="renewSub(this)">Renew</button>
		                        	@endif
                                <a href="{{url(app()->getLocale().'/mysubscription_property/'.$link.'/'.@$row->plan_id.'/'.@$row->purchase_type)}}">
		                            <div class="property-card bg-white rounded">
	                                    <img class="img-fluid property-img" src="{{asset('../uploads/subscription/'.@$row->subscription->logo)}}" style="height: 120px !important;display: block;margin: 0 auto;width: auto;">
		                                <div class="pl-3 pr-3 mt-3">
										<h4 style="text-align: center;font-size: 19px;color: #E4002B;"><strong>{{ucfirst(@$row->purchase_type)}}</strong></h4>
		                                <h5 style="text-align: center;"><strong>{{ucfirst(@$row->subscription->name)}}</strong></h5>
		                                    <p class="mb-2">
		                                    	<strong>Amount</strong> :  ${{@$row->amount}}
		                                    </p>
                                            <p class="mb-2">
											<strong>Purchase Date</strong> : {{date('d M Y', strtotime(@$row->purchase_date))}}
		                                    </p>
                                            <p class="mb-2">
											<strong>Expiry Date</strong> : {{date('d M Y', strtotime(@$row->expiry_date))}}
		                                    </p>
		                                    <p class="mb-2">
											<strong>Validity</strong>  : {{date_diff(date_create(@$row->expiry_date),date_create(@$row->purchase_date))->days}} Days
		                                    </p>
                                            <p class="mb-2">
											<strong>Property Can be Listed</strong>  : {{@$row->count}}
		                                    </p>
                                            <p class="mb-2">
											<strong>Property Listed</strong>  : {{@$row->listed}}
		                                    </p>
		                                </div>
		                            </div>
                                    </a>
		                        </div>
	                    @endforeach
			    </div>
			</div>
	    </div>
	    @endif
	</div>
</div>
<script type="text/javascript">
	function renewSub(obj)
	{
		var id = $(obj).data('id');
		var type = $(obj).data('type');
		sessionStorage.setItem("renew_id", id);
		Swal.fire({
              title: 'Are you sure?',
              text: "You want to renew this subscription!",
              showCancelButton: true,
              confirmButtonText: 'Yes',
              cancelButtonText: 'No',
              reverseButtons: true
            }).then((result) => {
            if (result.value) {
            	var url = "{{url(app()->getLocale().'/renew/'.$link.'/subscription')}}/"+type;
            	window.location.replace(url);
            }
            });
	}
</script>
