@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
<div class="col-md-9">
	<div class="card p-4">
    <h3>My Subscription Properties</h3>
	    <!-- Tab panes -->
	    <div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="rentListing">
			    <div class="row mt-4">
                @if(@$myProperties!='[]')
				        @foreach($myProperties as $row)
	                        
		                        <div class="col-md-4">
		                            <div class="property-card bg-white rounded">
									@if($ty=='rent')
									<a href="{{url(app()->getLocale().'/property/'.$link.'/rent/'.$row->_id)}}">
									@if(!isset($row->files[0]) || $row->files[0]=='')
                                    <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
                                    @else
                                    @if(file_exists('uploads/properties/'.$row->files[0]))
                                    <img class="img-fluid property-img" src="{{url('uploads/properties/'.$row->files[0])}}">
                                    @else
                                    <img class="img-fluid property-img" src="{{asset('img/house2.png')}}">
                                    @endif
                                    @endif
									@elseif($ty=='swap')
									<a href="{{url(app()->getLocale().'/property/'.$link.'/swap/'.$row->_id)}}">
									@if(!isset($row->files[0]) || $row->files[0]=='')
                                    <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
                                    @else
                                    @if(file_exists('uploads/swap/'.$row->files[0]))
                                    <img class="img-fluid property-img" src="{{url('uploads/swap/'.$row->files[0])}}">
                                    @else
                                    <img class="img-fluid property-img" src="{{asset('img/house2.png')}}">
                                    @endif
                                    @endif
									@elseif($ty=='cospace')
									<a href="{{url(app()->getLocale().'/property/'.$link.'/coworkspace/'.$row->_id)}}">
									@if(!isset($row->files[0]) || $row->files[0]=='')
                                    <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
                                    @else
                                    @if(file_exists('uploads/coworkspce/'.$row->files[0]))
                                    <img class="img-fluid property-img" src="{{url('uploads/coworkspce/'.$row->files[0])}}">
                                    @else
                                    <img class="img-fluid property-img" src="{{asset('img/house2.png')}}">
                                    @endif
                                    @endif
									@endif
									@if($ty=='rent')
		                                <div class="pl-3 pr-3">
		                                    <h5>{{ucfirst($row->house_type)}}</h5>
		                                    
		                                    <h5 class="mt-1 mb-1">
		                                        {{\Str::limit(ucfirst($row->property_title), 25)}}</p>
		                                    </h5>
		                                    
		                                    <p class="mb-2">
		                                    	<i class="fa fa-map-marker mr-2"></i> {{\Str::limit($row->location_name, 20)}}, {{ucfirst($row->country)}}
		                                    </p>
		                                    
		                                    <h5 class="mt-2 mb-2">
		                                    	{{$row->rent ?? 0}} {{$row->rent_currency ?? 'N/A'}} /{{$row->rent_frequency ?? 'daily'}}
		                                    </h5>
		                                    
		                                    <ul class="list-inline red-list mb-4" style="display: flex;">
		                                        <li class="mr-3"><img class="mr-1" src="{{asset('img/red-bed.png')}}" title="Bedrooms"> {{$row->no_of_bedrooms}}</li>
		                                        <li class="mr-3"><img title="Bathrooms" class="mr-1" src="{{asset('img/red-tub.png')}}"> {{$row->no_of_bathrooms}}</li>
		                                        <li class="mr-3"><img title="Open Parkings" class="mr-1" src="{{asset('img/red-car.png')}}"> {{$row->no_of_open_parking}}</li>
		                                    </ul>
		                                    <h5 class="text-danger text-right">&nbsp;{{ $row->is_approved ? '' : 'Approval Pending' }}<h5>
		                                </div>
										@elseif($ty=='swap')
										<div class="pl-3 pr-3">
		                                    <h5>{{ucfirst($row->house_type)}}</h5>
		                                    
		                                    <h5 class="mt-1 mb-1">
											{{\Str::limit(ucfirst($row->title), 20)}}</p>
		                                    </h5>
		                                    
		                                    <p class="mb-0"><i class="fa fa-map-marker mr-2"></i> {{\Str::limit($row->location_name, 20)}}, {{$row->country}}</p>
		                                    
		                                    <h5 class="mt-2 mb-2">
		                                    	{{$row->rent ?? 0}} {{$row->rent_currency ?? 'N/A'}} /{{$row->rent_frequency ?? 'daily'}}
		                                    </h5>
		                                    
											<ul class="list-inline red-list mb-4">
					                            <li class="mr-3"><img class="mr-1" src="{{asset('img/red-bed.png')}}" title="Bedrooms"> {{$row->no_of_bedrooms ?? 0}}</li>
					                            <li class="mr-3"><img title="Bathrooms" class="mr-1" src="{{asset('img/red-tub.png')}}"> {{$row->no_of_bathrooms ?? 0}}</li>
					                            <li class="mr-3"><img title="Open Parkings" class="mr-1" src="{{asset('img/red-car.png')}}"> {{$row->no_of_open_parking ?? 0}}</li>
					                        </ul>
					                        <h5 class="text-danger text-right">&nbsp;{{ $row->is_approved ? '' : 'Approval Pending' }}<h5>
		                                </div>
										@elseif($ty=='cospace')
										<div class="pl-3 pr-3">
		                                   
		                                    
		                                    <h5 class="mt-1 mb-1">
											{{\Str::limit(ucfirst($row->title), 35)}}</p>
		                                    </h5>
		                                    
		                                    <p class="mb-2">
		                                    	<i class="fa fa-map-marker mr-2"></i> {{\Str::limit(ucfirst($row->address), 30)}}, {{$row->country}}
		                                    </p>
		                                    
		                                    <h5 class="mt-2 mb-2">
											Starting @ {{$row->open_space_monthly_currency}}{{$row->open_space_monthly_price ?? 0}} /m
		                                    </h5>
		                                    <h5 class="text-danger text-right">&nbsp;{{ $row->is_approved ? '' : 'Approval Pending' }}<h5>
		                                   
		                                </div>
										@endif
		                               
		                            </div>
									</a>
		                        </div>
	                   		
	                    @endforeach
                    @else
                        <div class="" style="display: block;
                            max-height: 60px;
                            text-align: center;
                            background-color: #d6e9c6;
                            line-height: 4;
                            width: 100%;
                            ">
                            <p style="font-size: 16px;font-weight: 700;">No Result Found</p>
                        </div>
                    @endif
			    </div>
			</div>
		
	
	    </div>
	</div>
</div>
