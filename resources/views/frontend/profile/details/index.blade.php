@extends('frontend.layouts.home')
@section('title','Profile')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/datepicker.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/chosen.css')}}">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
@endsection

<style>
    
.bodyFilter {
  position: unset !important;
}

</style>

@section('content')
	@include('frontend.layouts.default-header')
	
    @if($defaultLoginRole=='USER')
        @include('frontend.profile.details.user')
    
    @elseif($defaultLoginRole=='AGENT')
        @include('frontend.profile.details.agent')

    @elseif($defaultLoginRole=='BUDDY')
        @include('frontend.profile.details.buddy')
    @else 
    	
    @endif
    
    @include('frontend.layouts.footer') 
@endsection

@section('scripts')

    @include('frontend.profile.common.js')
    <script type="text/javascript" src="{{asset('/js/chosen.jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('/js/prism.js')}}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script type="text/javascript" src="{{asset('js/datepicker.js')}}"></script>
    <script type="text/javascript">
        var todayDate = new Date();
        todayDate.setFullYear(todayDate.getFullYear() - 1);
        
        $('#dob').datepicker({
            format: 'dd/mm/yyyy',
            autoHide:true,
            endDate:todayDate,
        });

        $('#working_since').datepicker({
            endDate: new Date(),
            format: 'yyyy',
            autoHide:true
        });

        function drivingLicenceRadio(input){
            if(input.value=='no'){
                $('.driving_licence_number_div').hide();
            }
            else if(input.value=='yes'){
                $('.driving_licence_number_div').show();
            }
        }
        $(document).ready(function(){

            $('select.chosen-select').chosen();
            $('.chosen-select-deselect').chosen({ allow_single_deselect: true});

            
            $('input.timepicker').timepicker({
                timeFormat: 'HH:mm',
            });         

            
            // save user personal informations.
            $(document).on('click','.personal_info_submit_btn', function(e){
                e.preventDefault();

                $('#country_code').val($('.selected-dial-code').html());
                // clear all errors
                $form = $('#profile_form');
                $form.find('.is-invalid').removeClass('is-invalid');
                $form.find('.invalid-feedback').text('');

                var formFields = new FormData($form[0]);
                var ajax_url = WEBSITE_URL+"/update-profile";
                
                $.ajax({
                    url : ajax_url,
                    type:'post',
                    data : formFields,
                    dataType : 'json',
                    processData: false,
                    contentType: false,
                    beforeSend:function(){
                        startLoader('.inner-body');
                    },
                    complete:function(){
                       stopLoader('.inner-body'); 
                    },
                    success : function(data){
                        
                        if(data.status){
                            Swal.fire({
                                title: 'Success!',
                                text: data.message,
                                icon: 'success',
                                confirmButtonText: 'Ok'
                            }).then((result) => {
                              window.location.reload();
                            });
                        }else{
                            Swal.fire({
                                title: 'Error!',
                                text: data.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                    },
                    error : function(data){
                        stopLoader('.inner-body');
                        if(data.responseJSON){
                            var err_response = data.responseJSON;  
                            if(err_response.errors==undefined && err_response.message) {
                                Swal.fire({
                                    title: 'Error!',
                                    text: err_response.message,
                                    icon: 'error',
                                    confirmButtonText: 'Ok'
                                });
                            }                    
                            $.each(err_response.errors, function(i, obj){
                                $form.find('span.invalid-feedback.'+i).text(obj).show();
                                $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
                                $form.find('input[name="'+i+'"]').addClass('is-invalid');
                                $form.find('select[name="'+i+'"]').addClass('is-invalid');
                            });
                        }
                    }
                });
            });

            $(document).on('click','.professional_submit_btn', function(e){
                e.preventDefault();
                // clear all errors
                var $form = $('#professional_form');
                $form.find('.is-invalid').removeClass('is-invalid');
                $form.find('.invalid-feedback').text('');

                var formFields = new FormData($form[0]);
                var ajax_url = WEBSITE_URL+"/update-professional";
                
                formFields.append('monday', $('input[name="monday"]:checked').length);
                formFields.append('tuesday', $('input[name="tuesday"]:checked').length);
                formFields.append('wednesday', $('input[name="wednesday"]:checked').length);
                formFields.append('thursday', $('input[name="thursday"]:checked').length);
                formFields.append('friday', $('input[name="friday"]:checked').length);
                formFields.append('saturday', $('input[name="saturday"]:checked').length);
                formFields.append('sunday', $('input[name="sunday"]:checked').length);
        
                $.ajax({
                    url : ajax_url,
                    type:'post',
                    data : formFields,
                    dataType : 'json',
                    processData: false,
                    contentType: false,
                    beforeSend:function(){
                        startLoader('.inner-body');
                    },
                    complete:function(){
                       stopLoader('.inner-body');
                    },
                    success : function(data){
                        if(data.status){
                            Swal.fire({
                                title: 'Success!',
                                text: data.message,
                                icon: 'success',
                                confirmButtonText: 'Ok'
                            }).then((result) => {
                                window.location.reload();
                            });
                        }else{
                            Swal.fire({
                                title: 'Error!',
                                text: data.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                    },
                    error : function(data){
                        stopLoader('.registraion-process');
                        if(data.responseJSON){
                            var err_response = data.responseJSON;  
                            if(err_response.errors==undefined && err_response.message) {
                                Swal.fire({
                                    title: 'Error!',
                                    text: err_response.message,
                                    icon: 'error',
                                    confirmButtonText: 'Ok'
                                });
                            }
                            $.each(err_response.errors, function(i, obj){
                                $form.find('span.invalid-feedback.'+i).text(obj).show();
                                $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
                                $form.find('input[name="'+i+'"]').addClass('is-invalid');
                                $form.find('select[name="'+i+'"]').addClass('is-invalid');
                            });
                        }
                    }
                });
            });

            // update additional details of user
            $(document).on('click','.additional_info_submit_btn', function(e){
                e.preventDefault();
                // clear all errors
                var $form = $('#additional_information_form');
                $form.find('.is-invalid').removeClass('is-invalid');
                $form.find('.invalid-feedback').text('');

                var formFields = new FormData($form[0]);
                var ajax_url = WEBSITE_URL+"/update-additional";
                
                $.ajax({
                    url : ajax_url,
                    type:'post',
                    data : formFields,
                    dataType : 'json',
                    processData: false,
                    contentType: false,
                    beforeSend:function(){
                        startLoader('.inner-body');
                    },
                    complete:function(){
                       stopLoader('.inner-body'); 
                    },
                    success : function(data){
                        if(data.status){
                            Swal.fire({
                                title: 'Success!',
                                text: data.message,
                                icon: 'success',
                                confirmButtonText: 'Ok'
                            }).then((result) => {
                              window.location.reload();
                            });
                        }else{
                            Swal.fire({
                                title: 'Error!',
                                text: data.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                    },
                    error : function(data){
                        stopLoader('.inner-body');

                        if(data.responseJSON){
                            var err_response = data.responseJSON;  
                            if(err_response.errors==undefined && err_response.message) {
                                Swal.fire({
                                    title: 'Error!',
                                    text: err_response.message,
                                    icon: 'error',
                                    confirmButtonText: 'Ok'
                                });
                            }                    
                            $.each(err_response.errors, function(i, obj){
                                $form.find('span.invalid-feedback.'+i).text(obj);
                                $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
                                $form.find('input[name="'+i+'"]').addClass('is-invalid');
                                $form.find('select[name="'+i+'"]').addClass('is-invalid');
                            });
                        }
                    }
                });
            });

            // save user about me
            $(document).on('click','.aboutme_submit_btn', function(e){
                e.preventDefault();
                var $form = $('#aboutme_form');
                // clear all errors
                $form.find('.is-invalid').removeClass('is-invalid');
                $form.find('.invalid-feedback').text('');

                var formFields = new FormData($form[0]);
                var ajax_url = WEBSITE_URL+"/update-aboutme";
                
                $.ajax({
                    url : ajax_url,
                    type:'post',
                    data : formFields,
                    dataType : 'json',
                    processData: false,
                    contentType: false,
                    beforeSend:function(){
                        startLoader('.inner-body');
                    },
                    complete:function(){
                        stopLoader('.inner-body');
                    },
                    success : function(data){
                        
                        if(data.status){
                            Swal.fire({
                                title: 'Success!',
                                text: data.message,
                                icon: 'success',
                                confirmButtonText: 'Ok',
                                iconColor: '#333',
                            }).then((result) => {
                              window.location.reload();
                            });
                        }else{
                            Swal.fire({
                                title: 'Error!',
                                text: data.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                    },
                    error : function(data){
                        stopLoader('.inner-body');
                        if(data.responseJSON){
                            var err_response = data.responseJSON;  
                            if(err_response.errors==undefined && err_response.message) {
                                Swal.fire({
                                    title: 'Error!',
                                    text: err_response.message,
                                    icon: 'error',
                                    confirmButtonText: 'Ok'
                                });
                            }
                                                      
                            $.each(err_response.errors, function(i, obj){
                                $form.find('textarea[name="'+i+'"]').next().text(obj);
                                $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
                            });
                        }
                    }
                });
            });

            // get states for selected country
            $(document).on('change','.country_change', function(e){
                e.preventDefault();
                var country = $(this).val();
                var id = $(this).data('change');
                if(!country){
                    return false;
                }
                var ajax_url = WEBSITE_URL+"/states/"+country;    
                $.ajax({
                    url : ajax_url,
                    type:'get',
                    dataType : 'json',
                    processData: false,
                    contentType: false,
                    beforeSend:function(){
                        startLoader('.inner-body');
                    },
                    complete:function(){
                       stopLoader('.inner-body'); 
                    },
                    success : function(data){
                        var html='<option value="">Select State</option>';
                            
                        if(data.states){
                            $.each(data.states, function(key, val){
                                html+='<option value="'+val+'">'+val+'</option>';
                            });
                        }
                        $('#'+id).html(html);
                    },
                    error : function(data){
                        stopLoader('.inner-body');
                        if(data.responseJSON){
                            var err_response = data.responseJSON;  
                            if(err_response.errors==undefined && err_response.message) {
                                Swal.fire({
                                    title: 'Error!',
                                    text: err_response.message,
                                    icon: 'error',
                                    confirmButtonText: 'Ok'
                                });
                            }
                        }
                    }
                });
            });

            // save user address
            $(document).on('click','.address_submit_btn', function(e){
                e.preventDefault();
                var $form = $('#address_form');
                // clear all errors
                $form.find('.is-invalid').removeClass('is-invalid');
                $form.find('.invalid-feedback').text('');

                var formFields = new FormData($form[0]);
                var ajax_url = WEBSITE_URL+"/update-address";
                
                $.ajax({
                    url : ajax_url,
                    type:'post',
                    data : formFields,
                    dataType : 'json',
                    processData: false,
                    contentType: false,
                    beforeSend:function(){
                        startLoader('.inner-body');
                    },
                    complete:function(){
                        stopLoader('.inner-body');
                    },
                    success : function(data){
                        
                        if(data.status){
                            Swal.fire({
                                title: 'Success!',
                                text: data.message,
                                icon: 'success',
                                confirmButtonText: 'Ok'
                            }).then((result) => {
                              window.location.reload();
                            });
                        }else{
                            Swal.fire({
                                title: 'Error!',
                                text: data.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                    },
                    error : function(data){
                        stopLoader('.inner-body');
                        if(data.responseJSON){
                            var err_response = data.responseJSON;  
                            if(err_response.errors==undefined && err_response.message) {
                                Swal.fire({
                                    title: 'Error!',
                                    text: err_response.message,
                                    icon: 'error',
                                    confirmButtonText: 'Ok'
                                });
                            }
                                                      
                            $.each(err_response.errors, function(i, obj){
                                $form.find('span.invalid-feedback.'+i).text(obj);
                                
                                $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
                                $form.find('input[name="'+i+'"]').addClass('is-invalid');
                                $form.find('select[name="'+i+'"]').addClass('is-invalid');
                            });
                        }
                    }
                });
            });

            // qualification submit event
            $(document).on('click','.qualification_submit_btn', function(e){
                e.preventDefault();
                var $form = $('#qualification_form');
                // clear all errors
                $form.find('.is-invalid').removeClass('is-invalid');
                $form.find('.invalid-feedback').text('');

                var formFields = new FormData($form[0]);
                var ajax_url = WEBSITE_URL+"/update-qualification";
                
                $.ajax({
                    url : ajax_url,
                    type:'post',
                    data : formFields,
                    dataType : 'json',
                    processData: false,
                    contentType: false,
                    beforeSend:function(){
                        startLoader('.inner-body');
                    },
                    complete:function(){
                        stopLoader('.inner-body');
                    },
                    success : function(data){
                        
                        if(data.status){
                            Swal.fire({
                                title: 'Success!',
                                text: data.message,
                                icon: 'success',
                                confirmButtonText: 'Ok'
                            }).then((result) => {
                              window.location.reload();
                            });
                        }else{
                            Swal.fire({
                                title: 'Error!',
                                text: data.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                    },
                    error : function(data){
                        stopLoader('.inner-body');
                        if(data.responseJSON){
                            var err_response = data.responseJSON;  
                            if(err_response.errors==undefined && err_response.message) {
                                Swal.fire({
                                    title: 'Error!',
                                    text: err_response.message,
                                    icon: 'error',
                                    confirmButtonText: 'Ok'
                                });
                            }
                                                      
                            $.each(err_response.errors, function(key, value){
                                if(key.indexOf('.') != -1) {
                                    let keys = key.split('.');                                
                                    $('input[name="'+keys[0]+'[]"]').eq(keys[1]).addClass('is-invalid').next('span').text(value).show();                         
                                }
                                else {
                                    $('input[name="'+key+'[]"]').eq(0).addClass('is-invalid').next('span').text(value).show();
                                }
                            });
                        }
                    }
                });
            });

            $(document).on('click','.documents_submit_btn', function(e){
                e.preventDefault();
                var $form = $('#documents_form');
                // clear all errors
                $form.find('.is-invalid').removeClass('is-invalid');
                $form.find('.invalid-feedback').text('');

                var formFields = new FormData($form[0]);
                var ajax_url = WEBSITE_URL+"/update-documents";
        
                $.ajax({
                    url : ajax_url,
                    type:'post',
                    data : formFields,
                    dataType : 'json',
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    beforeSend:function(){
                        startLoader('.inner-body');
                    },
                    complete:function(){
                       stopLoader('.inner-body');
                    },
                    success : function(data){
                        if(data.status){
                            Swal.fire({
                                title: 'Success!',
                                text: data.message,
                                icon: 'success',
                                confirmButtonText: 'Ok'
                            }).then((result) => {
                              window.location.reload();
                            });
                        }else{
                            Swal.fire({
                                title: 'Error!',
                                text: data.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                    },
                    error : function(data){
                        stopLoader('.inner-body');
                        if(data.responseJSON){
                            var err_response = data.responseJSON;  
                            if(err_response.errors==undefined && err_response.message) {
                                Swal.fire({
                                    title: 'Error!',
                                    text: err_response.message,
                                    icon: 'error',
                                    confirmButtonText: 'Ok'
                                });
                            }
                            var html='';
                            $.each(err_response.errors, function(i, obj){
                                html+='<li><span class="invalid-feedback '+i+'">'+obj+'<span></li>';
                                
                            });
                            $('.errors_ul').html(html);
                            $(document).find('span.invalid-feedback').show();
                        }
                    }
                });
            });

            // delete hobby, qualification
            $(document).on('click','.delete_hobby, .delete_qualification', function(e){
                $(this).parent().parent().remove();
            });
        });
    	
        // checkbox to save native address same as current
        const setAddress = function(input){
            if(input.checked){
                $('input[name="native_location"]').val($('input[name="current_location"]').val());
                $('input[name="native_unit_number"]').val($('input[name="current_unit_number"]').val());
                $('input[name="native_address_1"]').val($('input[name="current_address_1"]').val());
                $('input[name="native_address_2"]').val($('input[name="current_address_2"]').val());
                $('input[name="native_landmark"]').val($('input[name="current_landmark"]').val());
                $('input[name="native_city"]').val($('input[name="current_city"]').val());
                $('select[name="native_country"]').val($('select[name="current_country"]').val());
                $('select[name="native_state"]').html('')
                $('#current_state option').clone().appendTo('#native_state');
                $('select[name="native_state"]').val($('select[name="current_state"]').val());
                $('input[name="native_zipcode"]').val($('input[name="current_zipcode"]').val());
            }else{
                $('input[name="native_location"]').val('');
                $('input[name="native_unit_number"]').val('');
                $('input[name="native_address_1"]').val('');
                $('input[name="native_address_2"]').val('');
                $('input[name="native_landmark"]').val('');
                $('input[name="native_city"]').val('');
                $('select[name="native_country"]').val('');
                $('select[name="native_state"]').val('');
                $('input[name="native_zipcode"]').val('');
            }
        }

        // toggle profile form
    	const toggleProfileForm = function(e){
    		if($('.personal_information_form').is(':visible')){
    			$('.personal_information_form').hide();
    			$('.personal_information_div').show();
    			$('.personal-info-btn').html('Edit Details');
    		}else{
    			

    			$('.personal_information_form').show();
    			$('.personal_information_div').hide();
    			$('.personal-info-btn').html('Show Details');
    		}
    	}

        // toggle additional information form
        const toggleAdditionalForm = function(e){
            if($('.additional_information_form').is(':visible')){
                $('.additional_information_form').hide();
                $('.additional_information_div').show();
                $('.additional-info-btn').html('Edit Details');
            }else{
                $('.additional_information_form').show();
                $('.additional_information_div').hide();
                $('.additional-info-btn').html('Show Details');
            }
        }

        const onChangeRealEstate = function(input){
            if(input.value=='yes'){
                $('.real_estate_yes_div').show();
            }else{
                $('.real_estate_yes_div').hide();
            }
        }

        // toggle professional information form
        const toggleProfessionalForm = function(e){
            if($('.professional_information_form').is(':visible')){
                $('.professional_information_form').hide();
                $('.professional_information_div').show();
                $('.professional-info-btn').html('Edit Details');
            }else{
                $('.professional_information_form').show();
                $('.professional_information_div').hide();
                $('.professional-info-btn').html('Show Details');
            }
        }

        const toggleDocumentsForm = function(e){
            if($('.documents_information_form').is(':visible')){
                $('.documents_information_form').hide();
                $('.documents_information_div').show();
                $('.documents-info-btn').html('Edit Details');
            }else{
                $('.documents_information_form').show();
                $('.documents_information_div').hide();
                $('.documents-info-btn').html('Show Details');
            }
        }

        // add qualification event
        const addQualification = function(){
            
            if($('.qualifications_div').length>10){
                return false;
            }
            $('.qualifications_append_div').append('<div class="row qualifications_div mt-2"><div class="col-md-5">\
                                            <div class="form-group"><label><strong>Course/Degree Name</strong></label>\
                                            <input class="form-control" type="text" name="course_name[]">\
                                            <span class="error invalid-feedback course_name"></span></div>\
                                        </div>\
                                        <div class="col-md-5">\
                                            <div class="form-group"><label><strong>University</strong></label>\
                                            <input class="form-control" type="text" name="university_name[]">\
                                            <span class="error invalid-feedback university_name"></span></div>\
                                        </div><div class="col-md-2">\
                                        <label>&nbsp; </label>\
                                        <button type="button" class="light-grey-btn btn-danger delete_qualification">Delete</button>\
                                    </div></div>');
        }

        // add hobby event
        const addHobby = function(){
            if($('.hobby_div').length>10){
                return false;
            }
            $('.hobby_append_div').append('<div class="row mt-2 hobby_div"><div class="col-md-6">\
                                    <div class="form-group"><label><strong>Hobby</strong></label>\
                                    <input class="form-control" type="text" name="hobby[]">\
                                    <span class="error invalid-feedback hobby"></span></div>\
                                </div><div class="col-md-2">\
                                        <label>&nbsp; </label>\
                                        <button type="button" class="light-grey-btn btn-danger delete_hobby">Delete</button>\
                                    </div></div>');
        }

        const toggleAddressForm = function(e){
            if($('.address_information_form').is(':visible')){
                $('.address_information_form').hide();
                $('.address_information_div').show();
                $('.address-info-btn').html('Edit Details');
            }else{
                $('.address_information_form').show();
                $('.address_information_div').hide();
                $('.address-info-btn').html('Show Details');
            }
        }

        const toggleQualificationForm = function(e){
            if($('.qualification_information_form').is(':visible')){
                $('.qualification_information_form').hide();
                $('.qualification_information_div').show();
                $('.qualification-info-btn').html('Edit Details');
            }else{
                $('.qualification_information_form').show();
                $('.qualification_information_div').hide();
                $('.qualification-info-btn').html('Show Details');
            }
        }

    	const togglePersonalAbout = function(){
    		if($('.personal_about_form').is(':visible')){
    			$('.personal_about_form').hide();
    			$('.personal_about_div').show();
    			$('.personal-about-btn').html('Update Bio');
    		}else{
    			var about = $('.personal_about_div').text();
    			$('.personal_about_form').find('textarea[name="about"]').val(about);
    			$('.personal_about_form').show();
    			$('.personal_about_div').hide();
    			$('.personal-about-btn').html('Show Bio');
    		}
    	}

        function readDocumentURL(input) {
            if (input.files && input.files[0]) {
                if(input.files.length>5){
                    Swal.fire({
                        title: 'Error!',
                        text: 'You can select maximum 5 docs.',
                        icon: 'error',
                        confirmButtonText: 'Ok'
                    });
                    return false;
                }
                var html = '';
                console.log(input.files)
                $(input.files ).each(function( index,val ) {
                  html+='<li>'+val.name+'<span class="invalid-feedback documents.'+index+'"><span></li>';
                });
                $('.image_prev_ul').html(html);
            }
        }
    </script>
@endsection