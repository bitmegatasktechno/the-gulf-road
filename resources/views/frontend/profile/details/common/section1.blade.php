@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp

                    @php
					$current=date('Y-m-d');
                        $users = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','user')->where('expiry_date','>=',$current )->count();
                        if($users){
                            if(@$users > 0){
                                $package="NO";
                                $getPlan = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','user')->where('expiry_date','>=',$current )->first();
                        }else{
                            $package="YES";
                            
                        }
                        }else{
                            $package="YES";
                        }


    $checkIfBuddyOrAgent  = \App\Models\User::where('_id',\Auth::user()->id)->whereIn('role',['BUDDY','AGENT'])->first();

                    @endphp
                    <style>
                    .file-upload-native, .file-upload-text{
                        top: 24px !important;
                    }
                    </style>
<div class="col-md-4">
    <div class="inner-white-wrapper mb-3">

        <div class="file-upload-wrapper" style="text-align: center;">
            <form method="post" name="profile_pic_form" id="profile_pic_form">
                @csrf
                <div class="preview img-wrapper">
                    @if(($user['image']=="") || ($user['image']==null))
                       <img src="https://dummyimage.com/400x240/ccc/fff">
                    @else

                        <img src="{{url('uploads/profilePics/'.$user['image'])}}"  >
                    @endif
                </div>
                <input type="file" id="profile_pic" onchange="uploadProfileImage();" name="profile_pic" class="file-upload-native" accept="image/*" style="margin-top: 103px;"/>
                <input type="text" disabled placeholder="Upload Image" class="file-upload-text" style="margin-top: 63px;"/>
                <div class="clearfix"></div>
                <span class="invalid-feedback"></span>
            </form>
        </div>

        <div>
            <hr>
        </div>
        <div class="row">
        	<div class="col-md-12">
        		<h6 class="mb-0 mt-2" style="color: rgba(14,14,14,0.66)">Active Profile</h6>
        	</div>
        </div>
        <div class="row">
            @php
                $roles = $user['role'];
            @endphp

            @foreach($roles as $row)
                <div class="col-md-4">
                    <label class="servicesChkBx mt-3">
                        <input type="radio" name="user_selection" @if($user['defaultLoginRole']==$row) checked=""@endif value="{{$row}}" onchange="changeDefaultRole(this)">
                        <span class="checkmark">
                        {{$row}}
                        </span>
                    </label>
                </div>
            @endforeach
        </div>
    </div>
    <!-- Navigation Block -->
    <div class="inner-white-wrapper left-navigation mb-3">
        @include('frontend.profile.user_profile_sidebar')
    </div>
     
   
         
         @if(@$package=='YES')
            <div class="inner-white-wrapper alivate-profile mb-3">
                  <div><img src="{{asset('img/Elevate_profile.png')}}"></div>
                  <h4 class="mt-3">Elevate Your<br> Profile</h4>
                  <p>Elevate your profile to reach out a wider audience and get your listings be featured on our website.</p>

                    @if((isset($checkIfBuddyOrAgent->defaultLoginRole) && $checkIfBuddyOrAgent->defaultLoginRole =='AGENT')  || (isset($checkIfBuddyOrAgent->defaultLoginRole) && $checkIfBuddyOrAgent->defaultLoginRole =='BUDDY'))
                    <a href="javascript:" onclick="continueWithDefaultRole('{{$checkIfBuddyOrAgent->defaultLoginRole}}')"><button class="red-btn rounded">Get Started</button></a>

                    <!--  <a href="{{url(app()->getLocale().'/request/'.$link.'/subscription')}}"><button class="red-btn rounded">Get Started</button></a> -->
                    @elseif((isset($checkIfBuddyOrAgent->defaultLoginRole) && $checkIfBuddyOrAgent->defaultLoginRole =='USER'))
                    <a href="javascript:" onclick="alertChoseDefaultRole()"><button class="red-btn rounded" >Get Started</button></a>
                    @else 
                    <a href="javascript:" onclick="showBuddyAgentNoti()"><button class="red-btn rounded" >Get Started</button></a>
                    @endif
                </div>
                @else
                <div class="inner-white-wrapper alivate-profile mb-3">
                  <div><img src="{{asset('img/Elevate_profile.png')}}"  ></div>
                  <h4 class="mt-3">Current Active Plan</h4>
                  <p><span><strong>Plan Name</strong></span> : {{@$getPlan->subscription->name ? @$getPlan->subscription->name : 'N/A'}}</p>
                  <p><span><strong>Amount</strong></span> : ${{@$getPlan->amount}}</p>
                  <p><span><strong>Purchase Date</strong></span> : {{date('d M Y', strtotime(@$getPlan->purchase_date))}}</p>
                  <p><span><strong>Expiry Date</strong></span> : {{date('d M Y', strtotime(@$getPlan->expiry_date))}}</p>
                  <p><span><strong>Validity Days</strong></span> : {{@$getPlan->validity}} days</p>
                </div>
                @endif
    
    
   
</div>

<script type="text/javascript" defer>
 
   
    function showBuddyAgentNoti()
    {
        var buddyUrl = "{{url(app()->getLocale().'/request/'.$link.'/subscription/buddy')}}";
        var agentUrl = "{{url(app()->getLocale().'/request/'.$link.'/subscription/agent')}}";
         Swal.fire({
              title: 'Before Elevate Your Profile.',
               html:
    'Click here become <b>' +
    '<a href="'+buddyUrl+'">Buddy</a> ' +
    '</b>,<b>' +
    '<a href="'+agentUrl+'">Agent</a> ' +
    '</b>',
              showCancelButton: true,
              cancelButtonText: 'CANCEL',
              reverseButtons: true,
              showCloseButton: true,
            }) 
    }
    function continueWithDefaultRole(val)
    {
         Swal.fire({
              title: 'Elevate Your Profile AS '+val,
              showCancelButton: true,
              confirmButtonText: 'OK',
              cancelButtonText: 'CANCEL',
              reverseButtons: true,
              showCloseButton: true,
            }).then((result) => {
             if (result.value) {
              var url = "{{url(app()->getLocale().'/request/'.$link.'/subscription')}}";
              window.location.replace(url);
            } 
            });
    }
    function alertChoseDefaultRole(val)
    {
         Swal.fire({
              title: 'Before Elevate Your Profile Active Profile Buddy or Agent',
              showCancelButton: true,
              confirmButtonText: 'OK',
              cancelButtonText: 'CANCEL',
              reverseButtons: true,
              showCloseButton: true,
            });
    }
</script>
        