@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
                    @php
					$current=date('Y-m-d');
                        $users = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','user')->where('expiry_date','>=',$current )->count();
                        $buddy = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','buddy')->where('expiry_date','>=',$current )->count();
                        $agent = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','agent')->where('expiry_date','>=',$current )->count();

                        if($users){
                            if(@$users > 0){
								$package="NO";
                        }else{
                            $package="YES";
                        }
                        }else{
                            $package="YES";
                        }

                        if($buddy){
                            if(@$buddy > 0){
                                $buddy_package="NO";
                        }else{
                            $buddy_package="YES";
                        }
                        }else{
                            $buddy_package="YES";
                        }

                        if($agent){
                            if(@$agent > 0){
                                $agent_package="NO";
                        }else{
                            $agent_package="YES";
                        }
                        }else{
                            $agent_package="YES";
                        }
                    @endphp

                     
@if($user['buddyProfile']==2 && $user['agentProfile']==2)
    
@else


    <div class="inner-white-wrapper personal-details mb-3 overflow-hidden">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <img class="" src="{{asset('img/create-profiles.png')}}">
                    <h5>Start Connecting, Start Selling.</h5>
                    <p>Create an Agent Profile to list properties or become a Buddy and Earn Money.</p>
                </div>
                
                <div class="col-md-3 pl-0">
                    @if($user['buddyProfile']==0 || !isset($user['buddyProfile']) || $user['buddyProfile']=='')
                    @if($buddy_package=='YES')
                    <a href="{{url(app()->getLocale().'/request/'.$link.'/subscription/buddy')}}">
                    @else
                        <a href="{{url(app()->getLocale().'/buddy-request/'.$link)}}">
                        @endif
                            <div class="agent-block" style="height: 121px;">
                                <img src="{{asset('img/buddy-red.png')}}">
                                <span class="dp-block" style="font-size: 13px;">Become a Buddy <img src="{{asset('img/-.png')}}"></span>
                            </div>
                        </a>
                    @elseif($user['buddyProfile']==1)
                        <div class="agent-block" style="height: 121px;">
                            <img src="{{asset('img/buddy-red.png')}}">
                            <span class="dp-block">Buddy application in progress</span>
                        </div>
                    @elseif($user['buddyProfile']==2)
                        <div class="agent-block" style="height: 121px;">
                            <img src="{{asset('img/buddy-red.png')}}">
                            <span class="dp-block" >Buddy application approved</span>
                        </div>
                    @endif
                </div>

                <div class="col-md-3 pl-0">
                    @if($user['agentProfile']==0 || !isset($user['agentProfile']) || $user['agentProfile']=='')
                    @if($agent_package=='YES')
                    <a href="{{url(app()->getLocale().'/request/'.$link.'/subscription/agent')}}">
                    @else
                        <a href="{{url(app()->getLocale().'/agent-request/'.$link)}}">
                    @endif
                            <div class="agent-block" style="height: 121px;">
                                <img src="{{asset('img/agent-red.png')}}">
                                <span class="dp-block" style="font-size: 13px;">Become an Agent <img src="{{asset('img/-.png')}}"></span>
                            </div>
                        </a>
                    @elseif($user['agentProfile']==1)
                        <div class="agent-block" style="height: 121px;">
                            <img src="{{asset('img/agent-red.png')}}">
                            <span class="dp-block">Agent application in progress</span>
                        </div>
                    @elseif($user['agentProfile']==2)
                        <div class="agent-block" style="height: 121px;">
                            <img src="{{asset('img/agent-red.png')}}">
                            <span class="dp-block">Agent application approved</span>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endif