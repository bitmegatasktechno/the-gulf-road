<div class="inner-white-wrapper personal-details overflow-hidden">
    <div class="row dp-flex align-items-center">
        <div class="col-md-6">
            <h3>Qualifications & Hobbies</h3>
        </div>
        <div class="col-md-6 text-right">
            <a href="javascript:;" class="qualification-info-btn" onclick="toggleQualificationForm();">Edit Details</a>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
        <div class="col-md-12 qualification_information_div">
            <div class="row">
                <div class="col-md-12">
                    <strong class="dp-block">Qualifications</strong>
                    @if(@$user['qualifications']->education!=null)
                        @foreach(@$user['qualifications']->education as $row)
                            <p class="mt-2 mb-2 ft-16"><img class="mr-2" src="{{asset('img/ic_red_check.png')}}">{{$row['course_name']}} from {{$row['university_name']}}</p>
                        @endforeach
                    @else
                    	N/A
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <strong class="dp-block mt-2">Hobbies</strong>
                    @if(@$user['qualifications']->hobbies!=null)
	                    @foreach(@$user['qualifications']->hobbies as $row)
	                    	<p class="mt-2 mb-2 ft-16">
	                    		<img class="mr-2" src="{{asset('img/ic_red_check.png')}}">
	                    		{{$row}}
	                    	</p>
	                    @endforeach
	                @else
                    	N/A
	                @endif
                </div>
            </div>
        </div>
        <div class="col-md-12 qualification_information_form" style="display: none;">
            <form class="new-cntct-frm" name="qualification_form" id="qualification_form">
                @csrf
                <div class="buddy-regst-form p-3">
                    
                    <div class="row">
                    	<div class="col-md-6">
                    		<button type="button" class="light-grey-btn" autocomplete="off" onclick="addQualification()">+Add Qualifications</button>
                    	</div>
                    </div>

                    <div class="row">
                        <div class=" col-md-12 qualifications_append_div">
                            
                            @if(@$user['qualifications']->education!=null)
                                @foreach(@$user['qualifications']->education as $row)
                                    
                                    <div class="row qualifications_div mt-2">
                                        <div class="col-md-5 course_name">
                                            <div class="form-group">
                                                <label><strong>Course/Degree Name</strong></label>
                                                <input class="form-control" type="text" name="course_name[]" value="{{$row['course_name']}}">
                                                <span class="error invalid-feedback course_name"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-5 university_name">
                                            <div class="form-group">
                                                <label><strong>University</strong></label>
                                                <input class="form-control" type="text" name="university_name[]" value="{{$row['university_name']}}">
                                                <span class="error invalid-feedback university_name"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <label>&nbsp; </label>
                                            <button type="button" class="light-grey-btn btn-danger delete_qualification">Delete</button>
                                        </div>
                                    </div>
                                    
                                @endforeach
                            @endif
                        </div>
                    </div>
                    

                    <div class="row mt-2">
                    	<div class="col-md-6">
                    		<button type="button" class="light-grey-btn" autocomplete="off" onclick="addHobby()">+Add Hobbies</button>
                    	</div>
                    </div>                    

                    <div class="row">
                        <div class="col-md-12 hobby_append_div">
                            
                            @if(@$user['qualifications']->hobbies!=null)
                                @foreach(@$user['qualifications']->hobbies as $row)
                                   
                                        <div class="row mt-2 hobby_div">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><strong>Hobby</strong></label>
                                                <input class="form-control" type="text" name="hobby[]" value="{{$row}}">
                                                <span class="error invalid-feedback hobby"></span></div>
                                            </div>
                                            <div class="col-md-2">
                                                <label>&nbsp; </label>
                                                <button type="button" class="light-grey-btn btn-danger delete_hobby">Delete</button>
                                            </div>
                                        </div>
                                    
                                @endforeach
                            @endif
                           
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <button type="button" class="red-btn rounded pt-3 pb-3 mt-3 mb-3 qualification_submit_btn"><span>Update</span></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>