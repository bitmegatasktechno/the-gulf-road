<div class="inner-white-wrapper personal-details overflow-hidden">
    <div class="row dp-flex align-items-center">
        <div class="col-md-6">
            <h3>{{__('Personal details')}}</h3>
        </div>
        <div class="col-md-6 text-right">
            <a href="javascript:;" class="personal-info-btn" onclick="toggleProfileForm();">
                {{__('Edit Details')}}
        </a>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
        <div class="col-md-12 personal_information_div">
            <div class="row">
                <!-- <div class="col-md-4">
                    <strong>{{__('Gender')}}</strong>
                    <h6 class="mb-4 mt-2">{{isset($user->gender) ? $user->gender : ''}}</h6>
                </div> -->

                <div class="col-md-4">
                    <strong>Full Name </strong>
                    <h6 class="full_name">{{ $user['name'] }}</h6>
                </div>

                 <!-- <div class="col-md-4">
                    <strong>Father's Name</strong>
                    <h6 class="mb-4 mt-2">{{isset($user->father_name) ? $user->father_name : ''}}</h6>
                </div> -->

                <!-- <div class="col-md-4">
                    <strong>Date Of Birth</strong>
                    <h6 class="mb-4 mt-2">{{isset($user->date_of_birth) ? $user->date_of_birth : ''}}</h6>
                </div> -->

                <div class="col-md-6">
                    <strong>Email</strong>
                    <h6 class="email">{{ $user['email'] }}</h6>
                </div>

                <!-- <div class="col-md-4">
                    <strong>Country Code</strong>
                    <h6>
                        <span class="country_code">{{ $user['countryCode'] }}</span> 
                    </h6>
                </div> -->
                <div class="col-md-4">
                    <strong>Phone</strong>
                    <h6>
                        <span class="phones">{{ $user['phone'] }}</span>
                    </h6>
                </div>
                <div class="col-md-4">
                    <strong>Country</strong>
                    <h6 class="country">{{ $user['country'] }}</h6>
                </div>

                <div class="col-md-12">
                    <strong>Address</strong>
                    <h6 class="address">
                        {{ isset($user['address']) ? $user['address']: 'N/A' }}
                    </h6>
                </div>
                
            </div>
        </div>
        <div class="col-md-12 personal_information_form" style="display: none;">
            <form class="new-cntct-frm" name="profile_form" id="profile_form">
                @csrf

                <input type="hidden" name="country_code" id="country_code" value="">
                
                <div class="loc-form p-3">
                    <div class="row">
                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="btn-grp-select parent_div">
                                    <span class="male-btnfemale-btn">
                                        <input type="radio" name="gender" id="size_1" value="male" @if(@$user->gender=='male') checked @endif>
                                        <label for="size_1">Male</label>
                                    </span>
                                    <span class="male-btnfemale-btn">
                                        <input type="radio" name="gender" id="size_2" value="female" @if(@$user->gender=='female') checked @endif>
                                        <label for="size_2">Female</label>
                                    </span>
                                    <span class="male-btnfemale-btn">
                                        <input type="radio" name="gender" id="size_3" value="other" @if(@$user->gender=='other') checked @endif>
                                        <label for="size_3">Other</label>
                                    </span>
                                    <br>
                                    <span class="invalid-feedback gender"></span>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Full Name</strong></label>
                                <input type="text" class="form-control" placeholder="Enter Full Name" name="name" value="{{ $user['name'] }}">
                                <span class="invalid-feedback name"></span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Father's Name</strong></label>
                                <input type="text" class="form-control" placeholder="Enter Father Name" name="father_name" value="{{$user->father_name}}">
                                <span class="invalid-feedback father_name"></span>
                            </div>
                        </div>
                       
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Phone Number</strong></label> <br>
                                <div class="row">
                                    <div class="col-md-4" style="padding-right: 0px!important; ">
                                        <select class="form-control rounded" name="country_code">
                                        @foreach(getAllCodeWithName() as $row)
                                            <option value="{{ $row['dial_code'] }}" {{$row['dial_code'] == @$user->country_code ?'selected' : ''}}>{{$row['dial_code']}}</option>
                                        @endforeach
                                        </select>
                                        <span class="invalid-feedback country_code"></span>
                                    </div>
                                    <div class="col=md-8">
                                        <input class="form-control " type="number" name="phone_number" placeholder="Enter Phone Number" value="{{@$user->phone}}" minlength="6" maxlength="15" required="">
                                <span class="invalid-feedback phone_number"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Country</strong></label>
                                <select class="form-control" name="country">
                                    <option value="">Select Country</option>

                                    @foreach(getAllLocations() as $row)
                                        <option value="{{$row->name}}" @if(strtolower($user['country'])==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                    @endforeach
                                </select>
                                <span class="invalid-feedback country"></span>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Date Of Birth</strong></label>
                                <input type="text" id="dob" class="form-control datepicker" placeholder="dd/mm/yyyy" name="date_of_birth" value="{{$user->date_of_birth}}" readonly="true">
                                <span class="invalid-feedback date_of_birth"></span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label><strong>Address</strong></label>
                                <textarea autofocus class="form-control" name="address" style="height: 143px; margin-top: 0px;">{{trim($user['address'])}}</textarea>
                            <span class="invalid-feedback address"></span>
                            </div>
                        </div>

                        <div class="col-md-12 text-right">
                            <button type="button" class="red-btn rounded pt-3 pb-3 mt-3 mb-3 personal_info_submit_btn">
                                <span>Update</span>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
