<div class="inner-white-wrapper personal-details mb-3 overflow-hidden professionals_edit_page">
    <div class="row dp-flex align-items-center">
        <div class="col-md-6">
            <h3>Professional details</h3>
        </div>
        <div class="col-md-6 text-right">
            <a href="javascript:;" class="professional-info-btn" onclick="toggleProfessionalForm();">Edit Details</a>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
        <div class="col-md-12 professional_information_div">
            <div class="row">
                <div class="col-md-12 mt-2">
                    <strong>Localities</strong>
                    <h6 class="mt-2 mb-2">These are the Locations you work in.</h6>
                    <div class="languages-block ">
                        @php
                            if(!is_array(@$user['professionals'][0]->localities))
                                $localities = explode(',', @$user['professionals'][0]->localities);
                            else
                                $localities = @$user['professionals'][0]->localities;
                        @endphp
                        <?php
                            foreach (@$localities as $key => $value) {
                                if($value)
                                    echo "<span class='light-blue'>$value</span>";
                            }
                        ?>
                    </div>
                </div>
                <div class="col-md-12 mt-2">
                    <strong>Languages</strong>
                    <h6 class="mb-2 mt-2">These are the Languages you Know.</h6>
                    <div class="languages-block mb-4">
                        @php
                            if(!is_array(@$user['professionals'][0]->languages))
                                $languages = explode(',', @$user['professionals'][0]->languages);
                            else
                                $languages = @$user['professionals'][0]->languages;

                        @endphp
                        <?php
                            foreach (@$languages as $key => $value) {
                                if($value)
                                    echo "<span class='light-blue'>$value</span>";
                            }
                        ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mt-2">
                    <strong> Real Estate Agent?</strong>
                    <h6 class="mb-4 mt-2">
                        {{(@$user['professionals'][0]->real_estate_agent=='yes') ? "Yes" : 'No'}}
                    </h6>

                </div>
                @if(@$user['professionals'][0]->real_estate_agent=='yes')

                    @if(@$user['professionals'][0]->deal_property_in_uae=='yes')
                        <div class="col-md-6">
                            <strong>Deal Property </strong>
                            <h6 class="mb-4 mt-2">I Deal properties in UAE.</h6>
                        </div>
                    @else
                        <div class="col-md-6">
                            <strong>Deal Property </strong>
                                <h6 class="mb-4 mt-2">I Deal properties in UAE.</h6>
                        </div>
                    @endif

                    @if($defaultLoginRole!='BUDDY')
                    <div class="col-md-6">
                        <strong>RERA Number(UAE Properties Only)</strong>
                        <h6 class="mb-4 mt-2">{{@$user['professionals'][0]->rera_number }}</h6>
                    </div>
                    @endif

                    <div class="col-md-6">
                        <strong>Permit Number</strong>
                        <h6 class="mb-4 mt-2">{{@$user['professionals'][0]->permit_number}}</h6>
                    </div>
                @endif

                <div class="col-md-12 mt-2">
                    <strong>Timing and activity</strong>

                    <div class="days-list mb-12">
                        <table class="table">
                            <thead>
                              <tr>
                                <th>Week Day</th>
                                <th>Start Time</th>
                                <th>End Time</th>
                              </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <img class="mr-2" src="{{asset('img/ic_red_check.png')}}" style="@if(@$user['professionals'][0]->availability['sunday']!=1) filter:grayscale();@endif">

                                        Sunday
                                    </td>
                                    @if(@$user['professionals'][0]->availability['sunday']==1)
                                        <td>{{@$user['professionals'][0]->timing['sunday_start_time']}}</td>
                                        <td>{{@$user['professionals'][0]->timing['sunday_end_time']}}</td>
                                    @else
                                        <td>N/A</td>
                                        <td>N/A</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>
                                        <img class="mr-2" src="{{asset('img/ic_red_check.png')}}" style="@if(@$user['professionals'][0]->availability['monday']!=1) filter:grayscale();@endif">

                                        Monday
                                    </td>
                                    @if(@$user['professionals'][0]->availability['monday']==1)
                                        <td>{{@$user['professionals'][0]->timing['monday_start_time']}}</td>
                                        <td>{{@$user['professionals'][0]->timing['monday_end_time']}}</td>
                                    @else
                                        <td>N/A</td>
                                        <td>N/A</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>
                                        <img class="mr-2" src="{{asset('img/ic_red_check.png')}}" style="@if(@$user['professionals'][0]->availability['tuesday']!=1) filter:grayscale();@endif">
                                        Tuesday</td>
                                    @if(@$user['professionals'][0]->availability['tuesday']==1)
                                        <td>{{@$user['professionals'][0]->timing['tuesday_start_time']}}</td>
                                        <td>{{@$user['professionals'][0]->timing['tuesday_end_time']}}</td>
                                    @else
                                        <td>N/A</td>
                                        <td>N/A</td>
                                    @endif
                                </tr>
                                <tr >
                                    <td>
                                        <img class="mr-2" src="{{asset('img/ic_red_check.png')}}" style="@if(@$user['professionals'][0]->availability['wednesday']!=1) filter:grayscale();@endif">
                                        Wednesday
                                    </td>
                                    @if(@$user['professionals'][0]->availability['wednesday']==1)
                                        <td>{{@$user['professionals'][0]->timing['wednesday_start_time']}}</td>
                                        <td>{{@$user['professionals'][0]->timing['wednesday_end_time']}}</td>
                                    @else
                                        <td>N/A</td>
                                        <td>N/A</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td><img class="mr-2" src="{{asset('img/ic_red_check.png')}}" style="@if(@$user['professionals'][0]->availability['thursday']!=1) filter:grayscale();@endif"> Thursday</td>
                                    @if(@$user['professionals'][0]->availability['thursday']==1)
                                        <td>{{@$user['professionals'][0]->timing['thursday_start_time']}}</td>
                                        <td>{{@$user['professionals'][0]->timing['thursday_end_time']}}</td>
                                    @else
                                        <td>N/A</td>
                                        <td>N/A</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td><img class="mr-2" src="{{asset('img/ic_red_check.png')}}" style="@if(@$user['professionals'][0]->availability['friday']!=1) filter:grayscale();@endif"> Friday</td>
                                    @if(@$user['professionals'][0]->availability['friday']==1)
                                        <td>{{@$user['professionals'][0]->timing['friday_start_time']}}</td>
                                        <td>{{@$user['professionals'][0]->timing['friday_end_time']}}</td>
                                    @else
                                        <td>N/A</td>
                                        <td>N/A</td>
                                    @endif
                                </tr>
                                <tr>
                                    <td><img class="mr-2" src="{{asset('img/ic_red_check.png')}}" style="@if(@$user['professionals'][0]->availability['saturday']!=1) filter:grayscale();@endif"> Saturday</td>
                                    @if(@$user['professionals'][0]->availability['saturday']==1)
                                        <td>{{@$user['professionals'][0]->timing['saturday_start_time']}}</td>
                                        <td>{{@$user['professionals'][0]->timing['saturday_end_time']}}</td>
                                    @else
                                        <td>N/A</td>
                                        <td>N/A</td>
                                    @endif
                                </tr>
                            </tbody>
                          </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="1buddy-regst-form col-md-12 professional_information_form" style="display: none;">
            <form method="post" name="professional_form" id="professional_form">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        <label class="mt-4">Select Languages you can Speak or Write or do Both</label>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            @php
                                if(!is_array(@$user['professionals'][0]->languages))
                                    $templanguages = explode(',', @$user['professionals'][0]->languages);
                                else
                                    $templanguages = @$user['professionals'][0]->languages;

                            @endphp

                            <select data-placeholder="Choose a language..." multiple class="chosen-select form-control" name="languages[]">
                                @foreach(getAllLanguages() as $row)
                                    <option value="{{ $row['name'] }}" @if(in_array($row['name'], $templanguages)) selected @endif>{{ __(ucfirst($row['name'])) }}</option>
                                @endforeach
                            </select>
                            <span class="invalid-feedback languages"></span>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="lic-y-n">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="mt-3">Are you a Real Estate Agent?</label>
                                </div>
                                <div class="col-md-2">
                                    <label class="servicesChkBx mt-0">
                                        <input type="radio" autocomplete="off" name="real_estate_agent" value="yes" onchange="onChangeRealEstate(this)" @if(@$user['professionals'][0]->real_estate_agent=='yes') checked @endif>
                                        <span class="checkmark">Yes</span>
                                    </label>
                                </div>
                                <div class="col-md-2">
                                    <label class="servicesChkBx mt-0">
                                        <input type="radio" autocomplete="off" name="real_estate_agent" value="no" onchange="onChangeRealEstate(this)" @if(@$user['professionals'][0]->real_estate_agent=='no') checked @endif>
                                        <span class="checkmark">No</span>
                                    </label>
                                </div>
                                <span class="invalid-feedback real_estate_agent"></span>
                            </div>
                        </div>
                    </div>
                    @php
                        $display = 'none';
                        if(@$user['professionals'][0]->real_estate_agent=='yes')
                            $display = '';
                    @endphp
                    <div class="row col-md-12 mt-1 real_estate_yes_div" style="display: {{$display}};">
                        <div class="col-md-5 ">
                            <div class="form-group">
                                <div class="radio">
                                <input id="radio-1" name="deal_property_in_uae" type="radio" @if(@$user['professionals'][0]->deal_property_in_uae=='inside') checked @endif value="inside">
                                <label for="radio-1" class="radio-label">I Deal in properties with UAE.</label>
                            </div></div>
                        </div>
                        <div class="col-md-7 ">
                            <div class="form-group">
                                <div class="radio">
                                <input id="radio-2" name="deal_property_in_uae" type="radio" @if(@$user['professionals'][0]->deal_property_in_uae=='outside') checked @endif value="outside">
                                <label for="radio-2" class="radio-label">I Deal in properties outside UAE.</label>
                            </div></div>
                        </div>
                        <span class="invalid-feedback deal_property_in_uae"></span>
                        @if($defaultLoginRole=='AGENT')
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="mt-3"><strong>RERA Number</strong><small>(UAE Properties Only)</small></label>
                                <input class="form-control" type="text" name="rera_number" placeholder="Enter RERA Number" value="{{@$user['professionals'][0]->rera_number }}">
                                <span class="invalid-feedback rera_number"></span></div>
                            </div>
                        @endif
                        <div class="col-md-6">
                           <div class="form-group">
                             <label class="mt-3"><strong>Permit Number</strong></label>
                            <input class="form-control" type="text" name="permit_number" placeholder="Enter Permit Number" value="{{@$user['professionals'][0]->permit_number }}">
                            <span class="invalid-feedback permit_number"></span></div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label class="mt-2"><strong>Set Timings & Availability</strong></label>

                        <div class="days-list mt-4">
                            <div class="timeandday">
                                <div class="dp-block no-border mb-3">
                                    <div class="row ">
                                        <div class="col-md-4">
                                            <div class="chiller_cb" style="top:0px;">
                                                <input id="monday" type="checkbox" name="monday" @if(@$user['professionals'][0]->availability['monday']==1) checked @endif>
                                                    <label for="monday">Monday</label>
                                                    <span></span>
                                            </div>
                                        </div>
                                        @if(@$user['professionals'][0]->availability['monday']==1)
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input class="form-control timepicker monday_start_time" type="text" name="monday_start_time" id="monday_start_time" value="{{@$user['professionals'][0]->timing['monday_start_time']}}">
                                                <span class="invalid-feedback monday_start_time"></span></div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input class="form-control timepicker monday_end_time" type="text" name="monday_end_time" id="monday_end_time" value="{{@$user['professionals'][0]->timing['monday_end_time']}}">
                                                <span class="invalid-feedback monday_end_time"></span></div>
                                            </div>
                                        @else
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input class="form-control timepicker monday_start_time" type="text" name="monday_start_time" id="monday_start_time" value="">
                                                <span class="invalid-feedback monday_start_time"></span></div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input class="form-control timepicker monday_end_time" type="text" name="monday_end_time" id="monday_end_time" value="">
                                                <span class="invalid-feedback monday_end_time"></span></div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="dp-block no-border mb-3">
                                    <div class="row ">
                                        <div class="col-md-4">
                                            <div class="chiller_cb" style="top:0px;">
                                                <input id="tuesday" type="checkbox" name="tuesday" @if(@$user['professionals'][0]->availability['tuesday']==1) checked @endif>
                                                <label for="tuesday">Tuesday</label>
                                                <span></span>
                                            </div>
                                        </div>
                                        @if(@$user['professionals'][0]->availability['tuesday']==1)
                                            <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker tuesday_start_time" type="text" name="tuesday_start_time" id="tuesday_start_time" value="{{@$user['professionals'][0]->timing['tuesday_start_time']}}">
                                                <span class="invalid-feedback tuesday_start_time"></span></div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                <input class="form-control timepicker tuesday_end_time" type="text" name="tuesday_end_time" id="tuesday_end_time" value="{{@$user['professionals'][0]->timing['tuesday_end_time']}}">
                                                <span class="invalid-feedback tuesday_end_time"></span></div>
                                            </div>
                                        @else
                                            <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker tuesday_start_time" type="text" name="tuesday_start_time" id="tuesday_start_time" >
                                                <span class="invalid-feedback tuesday_start_time"></span></div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                <input class="form-control timepicker tuesday_end_time" type="text" name="tuesday_end_time" id="tuesday_end_time" >
                                                <span class="invalid-feedback tuesday_end_time"></span></div>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="dp-block no-border mb-3">
                                    <div class="row ">
                                        <div class="col-md-4">
                                            <div class="chiller_cb" style="top:0px;">
                                                <input id="wednesday" type="checkbox" name="wednesday" @if(@$user['professionals'][0]->availability['wednesday']==1) checked @endif>
                                                <label for="wednesday">Wednesday</label>
                                                <span></span>
                                            </div>
                                        </div>
                                        @if(@$user['professionals'][0]->availability['wednesday']==1)
                                            <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker wednesday_start_time" type="text" name="wednesday_start_time" id="wednesday_start_time" value="{{@$user['professionals'][0]->timing['wednesday_start_time']}}">
                                                <span class="invalid-feedback wednesday_start_time"></span></div>
                                            </div>
                                            <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker wednesday_end_time" type="text" name="wednesday_end_time" id="wednesday_end_time" value="{{@$user['professionals'][0]->timing['wednesday_end_time']}}">
                                                <span class="invalid-feedback wednesday_end_time"></span></div>
                                            </div>
                                        @else
                                            <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker wednesday_start_time" type="text" name="wednesday_start_time" id="wednesday_start_time" >
                                                <span class="invalid-feedback wednesday_start_time"></span></div>
                                            </div>
                                            <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker wednesday_end_time" type="text" name="wednesday_end_time" id="wednesday_end_time">
                                                <span class="invalid-feedback wednesday_end_time"></span></div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <input type="hidden" name="defaultLoginRole" value="{{$defaultLoginRole}}">

                                <div class="clearfix"></div>
                                <div class="dp-block no-border mb-3">
                                    <div class="row ">
                                        <div class="col-md-4">
                                            <div class="chiller_cb" style="top:0px;">
                                                <input id="thursday" type="checkbox" name="thursday" @if(@$user['professionals'][0]->availability['thursday']==1) checked @endif>
                                                <label for="thursday">Thursday</label>
                                                <span></span>
                                            </div>
                                        </div>
                                        @if(@$user['professionals'][0]->availability['thursday']==1)
                                            <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker thursday_start_time" type="text" name="thursday_start_time" id="thursday_start_time" value="{{@$user['professionals'][0]->timing['thursday_start_time']}}">
                                                <span class="invalid-feedback thursday_start_time"></span></div>
                                            </div>
                                            <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker thursday_end_time" type="text" name="thursday_end_time" id="thursday_end_time" value="{{@$user['professionals'][0]->timing['thursday_end_time']}}">
                                                <span class="invalid-feedback thursday_end_time"></span></div>
                                            </div>
                                        @else
                                            <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker thursday_start_time" type="text" name="thursday_start_time" id="thursday_start_time">
                                                <span class="invalid-feedback thursday_start_time"></span></div>
                                            </div>
                                            <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker thursday_end_time" type="text" name="thursday_end_time" id="thursday_end_time" >
                                                <span class="invalid-feedback thursday_end_time"></span></div>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="dp-block no-border mb-3">
                                    <div class="row ">
                                        <div class="col-md-4">
                                            <div class="chiller_cb" style="top:0px;">
                                                <input id="friday" type="checkbox" name="friday" @if(@$user['professionals'][0]->availability['friday']==1) checked @endif>
                                                <label for="friday">Friday</label>
                                                <span></span>
                                            </div>
                                        </div>
                                        @if(@$user['professionals'][0]->availability['friday']==1)
                                            <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker friday_start_time" type="text" name="friday_start_time" id="friday_start_time" value="{{@$user['professionals'][0]->timing['friday_start_time']}}">
                                                <span class="invalid-feedback friday_start_time"></span></div>
                                            </div>
                                            <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker friday_end_time" type="text" name="friday_end_time" id="friday_end_time" value="{{@$user['professionals'][0]->timing['friday_end_time']}}">
                                                <span class="invalid-feedback friday_end_time"></span></div>
                                            </div>
                                        @else
                                            <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker friday_start_time" type="text" name="friday_start_time" id="friday_start_time" value="">
                                                <span class="invalid-feedback friday_start_time"></span></div>
                                            </div>
                                            <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker friday_end_time" type="text" name="friday_end_time" id="friday_end_time" value="">
                                                <span class="invalid-feedback friday_end_time"></span></div>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="dp-block no-border mb-3">
                                    <div class="row ">
                                        <div class="col-md-4">
                                            <div class="chiller_cb" style="top:0px;">
                                                <input id="saturday" type="checkbox" name="saturday" @if(@$user['professionals'][0]->availability['saturday']==1) checked @endif>
                                                <label for="saturday">Saturday</label>
                                                <span></span>
                                            </div>
                                        </div>
                                        @if(@$user['professionals'][0]->availability['saturday']==1)
                                            <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker saturday_start_time" type="text" name="saturday_start_time" id="saturday_start_time" value="{{@$user['professionals'][0]->timing['saturday_start_time']}}">
                                                <span class="invalid-feedback saturday_start_time"></span></div>
                                            </div>
                                            <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker saturday_end_time" type="text" name="saturday_end_time" id="saturday_end_time" value="{{@$user['professionals'][0]->timing['saturday_end_time']}}">
                                                <span class="invalid-feedback saturday_end_time"></span></div>
                                            </div>
                                        @else
                                             <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker saturday_start_time" type="text" name="saturday_start_time" id="saturday_start_time">
                                                <span class="invalid-feedback saturday_start_time"></span></div>
                                            </div>
                                            <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker saturday_end_time" type="text" name="saturday_end_time" id="saturday_end_time" >
                                                <span class="invalid-feedback saturday_end_time"></span></div>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="dp-block no-border mb-3">
                                    <div class="row ">
                                        <div class="col-md-4">
                                            <div class="chiller_cb" style="top: 0px">
                                            <input id="sunday" type="checkbox" name="sunday" @if(@$user['professionals'][0]->availability['sunday']==1) checked @endif>
                                                <label for="sunday">Sunday</label>
                                                <span></span>
                                            </div>
                                        </div>
                                        @if(@$user['professionals'][0]->availability['sunday']==1)
                                           <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker sunday_start_time" type="text" name="sunday_start_time" id="sunday_start_time" value="{{@$user['professionals'][0]->timing['sunday_start_time']}}">
                                                <span class="invalid-feedback sunday_start_time"></span></div>
                                            </div>
                                            <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker sunday_end_time" type="text" name="sunday_end_time" id="sunday_end_time" value="{{@$user['professionals'][0]->timing['sunday_end_time']}}">
                                                <span class="invalid-feedback sunday_end_time"></span></div>
                                            </div>
                                        @else
                                            <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker sunday_start_time" type="text" name="sunday_start_time" id="sunday_start_time" value="">
                                                <span class="invalid-feedback sunday_start_time"></span></div>
                                            </div>
                                            <div class="col-md-4"><div class="form-group">
                                                <input class="form-control timepicker sunday_end_time" type="text" name="sunday_end_time" id="sunday_end_time" value="">
                                                <span class="invalid-feedback sunday_end_time"></span></div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label class="mt-4">Select Localities where you are able to provide Services</label>
                    </div>
                    <div class="col-md-12">
                        @php
                            if(!is_array(@$user['professionals'][0]->localities))
                                $templocalities = explode(',', @$user['professionals'][0]->localities);
                            else
                                $templocalities = @$user['professionals'][0]->localities;
                        @endphp
                        
                        <select data-placeholder="Choose a Locality..." multiple class="chosen-select form-control" name="localities[]">
                            @foreach(getAllLocations() as $row)
                                <option value="{{ $row['name'] }}" @if(in_array($row['name'], $templocalities)) selected @endif>{{ __($row['name']) }}</option>
                                
                            @endforeach
                        </select>
                         <span class="invalid-feedback localities"></span>
                    </div>
                </div>
                <div class="col-md-12 text-right mt-5">
                    <button type="button" class="professional_submit_btn red-btn rounded pt-3 pb-3 mt-3 mb-3"><span>Update</span></button>
                </div>
            </form>
        </div>
    </div>
</div>
