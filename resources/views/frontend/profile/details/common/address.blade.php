 <div class="inner-white-wrapper personal-details mb-3 overflow-hidden">
    <div class="row dp-flex align-items-center">
        <div class="col-md-6">
            <h3>Addresses Details</h3>
        </div>
        <div class="col-md-6 text-right">
            <a href="javascript:;" class="address-info-btn" onclick="toggleAddressForm();">Edit Details</a>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
        <div class="col-md-12 address_information_div">
            <strong>Current Address</strong>
            <div class="row">
            
                <div class="col-md-4">
                    <strong>Location Name</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['addresses']->current_location) ? $user['addresses']->current_location : ''}}</h6>
                </div>

                <div class="col-md-4">
                    <strong>Unit/Floor No.</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['addresses']->current_unit_number) ? $user['addresses']->current_unit_number : ''}}</h6>
                </div>
                <div class="col-md-4">
                    <strong>Address Line 1</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['addresses']->current_address_1) ? $user['addresses']->current_address_1 : ''}}</h6>
                </div>
                <div class="col-md-4">
                    <strong>Address Line 2</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['addresses']->current_address_2) ? $user['addresses']->current_address_2 : ''}}</h6>
                </div>
                <div class="col-md-4">
                    <strong>Landmark</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['addresses']->current_landmark) ? $user['addresses']->current_landmark : ''}}</h6>
                </div>
                <div class="col-md-4">
                    <strong>Town/City</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['addresses']->current_city) ? $user['addresses']->current_city : ''}}</h6>
                </div>

                <div class="col-md-4">
                    <strong>Country</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['addresses']->current_country) ? $user['addresses']->current_country : ''}}</h6>
                </div>

                <div class="col-md-4">
                    <strong>State</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['addresses']->current_state) ? $user['addresses']->current_state : ''}}</h6>
                </div>
               
                <div class="col-md-4">
                    <strong>Zip Code</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['addresses']->current_zipcode) ? $user['addresses']->current_zipcode : ''}}</h6>
                </div>
            </div>
            <hr>
            <strong>Native Address</strong>
            <div class="row">
                <div class="col-md-4">
                    <strong>Location Name</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['addresses']->native_location) ? $user['addresses']->native_location : ''}}</h6>
                </div>

                <div class="col-md-4">
                    <strong>Unit/Floor No.</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['addresses']->native_unit_number) ? $user['addresses']->native_unit_number : ''}}</h6>
                </div>
                <div class="col-md-4">
                    <strong>Address Line 1</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['addresses']->native_address_1) ? $user['addresses']->native_address_1 : ''}}</h6>
                </div>
                <div class="col-md-4">
                    <strong>Address Line 2</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['addresses']->native_address_2) ? $user['addresses']->native_address_2 : ''}}</h6>
                </div>
                <div class="col-md-4">
                    <strong>Landmark</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['addresses']->native_landmark) ? $user['addresses']->native_landmark : ''}}</h6>
                </div>
                <div class="col-md-4">
                    <strong>Town/City</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['addresses']->native_city) ? $user['addresses']->native_city : ''}}</h6>
                </div>

                <div class="col-md-4">
                    <strong>Country</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['addresses']->native_country) ? $user['addresses']->native_country : ''}}</h6>
                </div>

                <div class="col-md-4">
                    <strong>State</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['addresses']->native_state) ? $user['addresses']->native_state : ''}}</h6>
                </div>
               
                <div class="col-md-4">
                    <strong>Zip Code</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['addresses']->native_zipcode) ? $user['addresses']->native_zipcode : ''}}</h6>
                </div>
            </div>
        </div>
 
        <div class="loc-form col-md-12 address_information_form" style="display: none;">
           
            <p class="dark-color">Current Address</p>
            <form method="post" name="address_form" id="address_form">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><strong>Location Name</strong></label>
                            <input class="form-control" type="text" onChange="getLocation1()" id="address1" name="current_location" value="{{@$user['addresses']->current_location}}">
                            <span class="invalid-feedback current_location"></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                         <div class="form-group">
                        <label><strong>Unit/Floor No.</strong></label>
                        <input class="form-control" type="text" name="current_unit_number" value="{{@$user['addresses']->current_unit_number}}">
                        <span class="invalid-feedback current_unit_number"></span></div>
                    </div>
                    <div class="col-md-6">
                         <div class="form-group">
                        <label><strong>Address Line 1</strong></label>
                        <input class="form-control" type="text" name="current_address_1" value="{{@$user['addresses']->current_address_1}}">
                        <span class="invalid-feedback current_address_1"></span></div>
                    </div>
                    <div class="col-md-6">
                         <div class="form-group">
                        <label><strong>Address Line 2</strong></label>
                        <input class="form-control" type="text" name="current_address_2" value="{{@$user['addresses']->current_address_2}}">
                        <span class="invalid-feedback current_address_2"></span></div>
                    </div>
                    <div class="col-md-6">
                         <div class="form-group">
                        <label><strong>Landmark</strong></label>
                        <input class="form-control" type="text" name="current_landmark" value="{{@$user['addresses']->current_landmark}}">
                        <span class="invalid-feedback current_landmark"></span></div>
                    </div>
                    <div class="col-md-6">
                         <div class="form-group">
                        <label><strong>Town/City</strong></label>
                        <input class="form-control" type="text" name="current_city" value="{{@$user['addresses']->current_city}}">
                        <span class="invalid-feedback current_city"></span></div>
                    </div>
                    <div class="col-md-4">
                         <div class="form-group">
                            <label><strong>Country</strong></label>
                            <select class="form-control rounded country_change" name="current_country" id="current_country" data-change="current_state">
                                <option value="">Select Country</option>
                               @foreach($countries as $row)
                                    <option value="{{ $row->name }}" @if(@$user['addresses']->current_country==$row->name) selected @endif>{{ __($row->name) }}</option>
                                @endforeach
                            </select>
                            <span class="invalid-feedback current_country"></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label><strong>State</strong></label>
                            <select class="form-control rounded " name="current_state" id="current_state">
                                <option value="">Select State</option>
                                @if(@$user['addresses']->current_state)
                                    <option value="{{@$user['addresses']->current_state}}" selected="true">{{@$user['addresses']->current_state}}</option>
                                @endif
                            </select>
                            <span class="invalid-feedback current_state"></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                         <div class="form-group">
                            <label><strong>Zip Code</strong></label>
                            <input class="form-control" type="text" name="current_zipcode" value="{{@$user['addresses']->current_zipcode}}">
                            <span class="invalid-feedback current_zipcode"></span>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12 mb-4">
                        <p class="mt-4 dark-color">Native Country Address</p>
                        <div class="checkbox-block">
                            <div class="form-group mb-2 mt-3">
                                <input type="checkbox" id="java1" name="same_current_address" onchange="setAddress(this)" @if(@$user['addresses']->same_current_address=='on') checked @endif>
                                <label class="" for="java1">Same as Current Address</label>
                                <small class="dp-block ml-4 pl-2">Select this option if your Native Country Address is Same as Current Address</small>
                            </div>
                        </div>
                    </div>
                </div>

<div id="map" style="display:none"></div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group" style="position: relative;">
                        <input type="hidden" name="latitude" class="form-control" id="lat" value="{{@$user->latitude}}" >
                        <input type="hidden" name="longitude" class="form-control" id="lng" value="{{@$user->longitude}}">

                            <label><strong>Location Name</strong></label>
                            <input class="form-control" type="text" name="native_location" value="{{@$user['addresses']->native_location}}">
                            <span class="invalid-feedback native_location"></span>
                        </div>
                    </div>
                    <div class="col-md-6">
                         <div class="form-group">
                        <label><strong>Unit/Floor No.</strong></label>
                        <input class="form-control" type="text" name="native_unit_number" value="{{@$user['addresses']->native_unit_number}}">
                        <span class="invalid-feedback native_unit_number"></span></div>
                    </div>
                    <div class="col-md-6">
                         <div class="form-group">
                        <label><strong>Address Line 1</strong></label>
                        <input class="form-control" type="text" name="native_address_1" value="{{@$user['addresses']->native_address_1}}">
                        <span class="invalid-feedback native_address_1"></span></div>
                    </div>
                    <div class="col-md-6">
                         <div class="form-group">
                        <label><strong>Address Line 2</strong></label>
                        <input class="form-control" type="text" name="native_address_2" value="{{@$user['addresses']->native_address_2}}">
                        <span class="invalid-feedback native_address_2"></span></div>
                    </div>
                    <div class="col-md-6">
                         <div class="form-group">
                        <label><strong>Landmark</strong></label>
                        <input class="form-control" type="text" name="native_landmark" value="{{@$user['addresses']->native_landmark}}">
                        <span class="invalid-feedback native_landmark"></span></div>
                    </div>
                    <div class="col-md-6">
                         <div class="form-group">
                        <label><strong>Town/City</strong></label>
                        <input class="form-control" type="text" name="native_city" value="{{@$user['addresses']->native_city}}">
                        <span class="invalid-feedback native_city"></span></div>
                    </div>
                    <div class="col-md-4">
                         <div class="form-group">
                            <label><strong>Country</strong></label>
                            <select class="form-control rounded country_change" name="native_country" id="native_country" data-change="native_state">
                                <option value="">Select Country</option>
                               @foreach($countries as $row)
                                    <option value="{{ $row->name }}" @if(@$user['addresses']->native_country==$row->name) selected @endif>{{ __($row->name) }}</option>
                                @endforeach
                            </select>
                            <span class="invalid-feedback native_country"></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label><strong>State</strong></label>
                            <select class="form-control rounded " name="native_state" id="native_state">
                                <option value="">Select State</option>
                                @if(@$user['addresses']->native_state)
                                    <option value="{{@$user['addresses']->native_state}}" selected="true">{{@$user['addresses']->native_state}}</option>
                                @endif
                            </select>
                            <span class="invalid-feedback native_state"></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                         <div class="form-group">
                            <label><strong>Zip Code</strong></label>
                            <input class="form-control" type="text" name="native_zipcode" value="{{@$user['addresses']->native_zipcode}}">
                            <span class="invalid-feedback native_zipcode"></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 text-right">
                    <button type="button" class="red-btn rounded pt-3 pb-3 mt-3 mb-3 address_submit_btn"><span>Update</span></button>
                </div>
            </form>
        </div>
    </div>
</div>

@if(env('APP_ACTIVE_MAP') =='patel_map')
<style>
    #address1_result
    {
        z-index: 20;
        left: unset !important;
        top:unset !important;
        width:  100% !important;
        
    }


</style>
   <script src="https://mapapi.cloud.huawei.com/mapjs/v1/api/js?callback=initMap&key={{env('PATEL_MAP_KEY')}}"></script>
    <script>
        function initMap() {
            var mapOptions = {};



             
            var myLatLng = {
                        lat: (-25.363),
                        lng: (131.044)
                                };
            mapOptions.center = myLatLng;
            mapOptions.zoom = 6;
            mapOptions.language='ENG';
            var searchBoxInput = document.getElementById('address1');
             var map = new HWMapJsSDK.HWMap(document.getElementById('map'), mapOptions);
            // Create the search box parameter.
            var acOptions = {
                location: {
                    lat: 48.856613,
                    lng: 2.352222
                },
                radius: 10000,
                customHandler: callback,
                language:'en'
            };
            var autocomplete;
            // Obtain the input element.
            // Create the HWAutocomplete object and set a listener.
            autocomplete = new HWMapJsSDK.HWAutocomplete(searchBoxInput, acOptions);
            autocomplete.addListener('site_changed', printSites);
            // Process the search result. 
            // index: index of a searched place.
            // data: details of a searched place. 
            // name: search keyword, which is in strong tags. 
            function callback(index, data, name) {
                
                var div
                div = document.createElement('div');
                div.innerHTML = name;
                return div;
            }
            // Listener.
            function printSites() {
                 
                
                var site = autocomplete.getSite();
                var latitude = site.location.lat;
                var longitude = site.location.lng;
                var country_name = site.address.country;
                
                var results = "Autocomplete Results:\n";
                var str = "Name=" + site.name + "\n"
                    + "SiteID=" + site.siteId + "\n"
                    + "Latitude=" + site.location.lat + "\n"
                    + "Longitude=" + site.location.lng + "\n"
                    + "Address=" + site.formatAddress + "\n";

                    
                results = results + str;

                 


                $('#lat').val(latitude);
                $('#lng').val(longitude);
                 $('#current_country').val(country_name);
                  $( "#current_country" ).trigger( "change" );
 


                /*end map zoom creation */
            }

           

            
        }
        function getLocation1()
        {

        }
    </script> 

     
    @elseif(env('APP_ACTIVE_MAP')=='google_map')

<script
      src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&callback=initMap&libraries=places&v=weekly"
      defer
    ></script>
<script>

function initMap() {
  const myLatLng = {
    lat: -25.363,
    lng: 131.044
  };
    
      

  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 4,
    center: myLatLng
  });
    var input = document.getElementById('address1');

 map.controls[google.maps.ControlPosition.TOP_RIGHT].push();

var autocomplete = new google.maps.places.Autocomplete(input);
autocomplete.bindTo('bounds', map);
autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);
            var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29),
          title: "Hello World!"
        });
        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindow.open(map, marker);
        });
  new google.maps.Marker({
    position: myLatLng,
    map,
    title: "Hello World!"
  });
}


function getLocation1()
 {
//   alert(address);
  var address = $('#address1').val();
//   alert(address);
  var geocoder = new google.maps.Geocoder();
  $('#address1').addClass('error is-invalid');
  $('.invalid-feedback.current_location').text('Geo Location Not Set Properly .Please Select.');
  $('#address1').focus();
  geocoder.geocode( { 'address': address}, function(results, status)
  {


    if (status == google.maps.GeocoderStatus.OK)
    {
      console.log(results[0],"afdcfsa");
      console.log(results[0].address_components);
      const countryObj=results[0].address_components.filter(val=>{
            if(val.types.includes('country')){
                return true
            }
            return false;
      });
     console.log(countryObj,'the country obj');


     var country_name = countryObj[0].long_name;
     console.log(country_name,'name');
      var latitude = results[0].geometry.location.lat();

      var longitude = results[0].geometry.location.lng();
      $('#address1').removeClass('error is-invalid');
    $('.invalid-feedback.current_location').text('');
      // alert(latitude +"-----"+ longitude);
     $('#lat').val(latitude);
     $('#lng').val(longitude);
     //$('#countryTo').val(country_name);
     $('#country_name').val(country_name);
     $('#current_country').val(country_name);
     $( "#current_country" ).trigger( "change" );
    console.log(latitude, longitude);
    }
  });

}
    </script>
    @endif