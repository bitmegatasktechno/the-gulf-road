<div class="inner-white-wrapper personal-details mb-3 overflow-hidden">
    <div class="row dp-flex align-items-center">
        <div class="col-md-6">
            <h3>Additional Details</h3>
        </div>
        <div class="col-md-6 text-right">
            <a href="javascript:;" class="additional-info-btn" onclick="toggleAdditionalForm();">Edit Details</a>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
        <div class="col-md-12 additional_information_div">
            <div class="row">
               
                <div class="col-md-4">
                    <strong>Whatsapp Number</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['profiles']->whatsapp_number) ? $user['profiles']->whatsapp_number : ''}}</h6>
                </div>
                <div class="col-md-4">
                    <strong>Marital Status</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['profiles']->marriage_status) ? $user['profiles']->marriage_status : ''}}</h6>
                </div>

                <div class="col-md-4">
                    <strong>Preffered Language</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['profiles']->preffered_language) ? $user['profiles']->preffered_language : ''}}</h6>
                </div>

                <div class="col-md-4">
                    <strong>Preffered Currency</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['profiles']->preffered_currency) ? $user['profiles']->preffered_currency : ''}}</h6>
                </div>

                <div class="col-md-4">
                    <strong>Working In Region Since?</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['profiles']->working_since) ? $user['profiles']->working_since : ''}}</h6>
                </div>

                <div class="col-md-4">
                    <strong>Residence ID Number</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['profiles']->residence_id_number) ? $user['profiles']->residence_id_number : ''}}</h6>
                </div>

                <div class="col-md-4">
                    <strong>Passport Number</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['profiles']->passport_number) ? $user['profiles']->passport_number : ''}}</h6>
                </div>

                <div class="col-md-4">
                    <strong>Driving Licence</strong>
                    <h6 class="mb-4 mt-2">{{isset($user['profiles']->driving_licence) ? $user['profiles']->driving_licence : ''}}</h6>
                </div>
                @if(isset($user['profiles']->driving_licence) && $user['profiles']->driving_licence=='yes')
                    <div class="col-md-4">
                        <strong>Licence Number</strong>
                        <h6 class="mb-4 mt-2">{{isset($user['profiles']->licence_number) ? $user['profiles']->licence_number : ''}}</h6>
                    </div>
                @endif
            </div>
        </div>
        <div class="col-md-12 additional_information_form" style="display: none;">
            
            <form class="new-cntct-frm" name="additional_information_form" id="additional_information_form">
                @csrf
                <div class="1buddy-regst-form p-3">
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group" id="dob">
                                <label><strong>Whatsapp Number</strong></label>
                                <input type="text" class="form-control" placeholder="Enter Whatsapp Number" name="whatsapp_number" value="{{$user['profiles']->whatsapp_number}}">
                                <span class="invalid-feedback whatsapp_number"></span>
                            </div>
                        </div>

                        

                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Residence ID Number</strong></label>
                                <input type="text" class="form-control" placeholder="Enter Residence ID Number" name="residence_id_number" value="{{$user['profiles']->residence_id_number}}">
                                <span class="invalid-feedback residence_id_number"></span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Passport Number</strong></label>
                                <input type="text" class="form-control" placeholder="Enter Passport Number" name="passport_number" value="{{$user['profiles']->passport_number}}">
                                <span class="invalid-feedback passport_number"></span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Marital Status</strong></label>
                                <select class="form-control" name="marriage_status">
                                    <option value="">Select Marital Status</option>
                                    <option value="married" @if(@$user['profiles']->marriage_status=='married') selected @endif>Married</option>
                                    <option value="single" @if(@$user['profiles']->marriage_status=='single') selected @endif>Single</option>
                                    <option value="divorced" @if(@$user['profiles']->marriage_status=='divorced') selected @endif>Divorced</option>
                                    <option value="widowed" @if(@$user['profiles']->marriage_status=='widowed') selected @endif>Widowed</option>
                                </select>
                                <span class="invalid-feedback marriage_status"></span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Preffered Language</strong></label>
                                <select class="form-control rounded" name="preffered_language">
                                    <option value="">Select Language</option>
                                    @foreach(getAllLanguages() as $row)
                                        <option value="{{ $row['name'] }}"  @if(@$user['profiles']->preffered_language==$row['name']) selected @endif>{{ __(ucfirst($row['name'])) }}</option>
                                    @endforeach
                                </select>
                                <span class="invalid-feedback preffered_language"></span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Preffered Currency</strong></label>
                                <select class="form-control rounded" name="preffered_currency">
                                    @foreach(getAllCurrencies() as $row)
                                        <option value="{{ $row->name }}"  @if(@$user['profiles']->preffered_currency==$row->name) selected @endif>{{ucfirst($row->name)}}</option>
                                    @endforeach
                                </select>
                                <span class="invalid-feedback preffered_currency"></span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label><strong>Working In Region Since?</strong></label>
                                <input class=" form-control" id="working_since" type="text" name="working_since" placeholder="yyyy" value="{{@$user['profiles']->working_since}}">
                                <span class="invalid-feedback working_since"></span>
                            </div>
                        </div>                        
                        <div class="col-md-12">
                            <div class="lic-y-n">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="mt-3">Driving Licence?</label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="servicesChkBx mt-0">
                                            <input type="radio" autocomplete="off" name="driving_licence" value="yes" onchange="drivingLicenceRadio(this)" @if($user['profiles']->driving_licence=='yes') checked @endif>
                                            <span class="checkmark">Yes</span>
                                        </label>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="servicesChkBx mt-0">
                                            <input type="radio" autocomplete="off" name="driving_licence" value="no" onchange="drivingLicenceRadio(this)" @if($user['profiles']->driving_licence=='no') checked @endif>
                                            <span class="checkmark">No</span>
                                        </label>
                                    </div>
                                    <span class="invalid-feedback driving_licence"></span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6 driving_licence_number_div" style="display: @if($user['profiles']->driving_licence=='yes') block @else none @endif">
                            <div class="form-group" id="dob">
                                <label><strong>Licence Number</strong></label>
                                <input type="text" class="form-control" placeholder="Enter Licence Number" name="licence_number" value="{{$user['profiles']->licence_number}}">
                                <span class="invalid-feedback licence_number"></span>
                            </div>
                        </div>

                        <div class="col-md-12 text-right">
                            <button type="button" class="red-btn rounded pt-3 pb-3 mt-3 mb-3 additional_info_submit_btn">
                                <span>Update</span>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
