<div class="inner-white-wrapper info-wrapper mb-3">
    <div class="row dp-flex align-items-center">
        <div class="col-md-6">
            <h2>Hi, I'm {{ $user['name'] }}</h2>
        </div>
        <div class="col-md-6 text-right">
            <a href="javascript:;" class="personal-about-btn" onclick="togglePersonalAbout();">{{__('Update Bio')}}</a>
        </div>
        <div class="col-md-12">
            <small class="dp-block mb-3">{{__('User Since')}} {{ date("Y", strtotime($user['created_at']))}}</small>
            <img src="{{asset('img/quote2.png')}}">
            <h5 class="mt-2 mb-0 personal_about_div" style="font-size:14px; display: inline-block;">{{ isset($user['about']) ? $user['about'] : ''}}</h5>
        </div>
        
        <div class="col-md-12 personal_about_form" style="display: none;">
            <form class="new-cntct-frm" name="aboutme_form" id="aboutme_form">
                @csrf
                <div class="loc-form p-3">
                    <div class="row">
                        <div class="col-md-12 parent_div">
                            <label>{{__('About Me')}}</label>
                            <textarea autofocus class="form-control" name="about" style="height: 143px; margin-top: 0px;"></textarea>
                            <span class="invalid-feedback"></span>
                        </div>

                        <div class="col-md-12 text-right">
                            <button type="button" class="red-btn rounded pt-3 pb-3 mt-3 mb-3 aboutme_submit_btn">
                                <span>{{__('Update')}}</span>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>