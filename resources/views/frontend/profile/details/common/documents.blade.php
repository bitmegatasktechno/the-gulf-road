<div class="inner-white-wrapper personal-details mb-3 overflow-hidden">
    <div class="row dp-flex align-items-center">
        <div class="col-md-6">
            <h3>Upload documents</h3>
        </div>
        <div class="col-md-6 text-right">
            <a href="javascript:;" class="documents-info-btn" onclick="toggleDocumentsForm();">Edit Details</a>
        </div>
        <div class="col-md-12">
            <hr>
        </div>
        <div class="col-md-12 documents_information_div">
            <div class="row">
                @if(@$user['documents'][0]->documents!=null)
                    @foreach(@$user['documents'][0]->documents as $row)
                        @php
                            $path_info = pathinfo($row);
                        @endphp
                        <div class="col-md-12">
                            <span class="p-2 rounded body-light-grey dp-inline-block mt-3 img-upload">
                                @if($path_info['extension']=='pdf')
                                    <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                @else
                                    <i class="fa fa-file-image-o" aria-hidden="true"></i>
                                @endif
                                <a href="{{url('uploads/requests/docs/'.@$user['documents'][0]->user_id.'/'.$row)}}" target="_blank">{{$row}}</a>
                            </span>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>

        <div class="col-md-12 documents_information_form" style="display: none;">
            <div class="buddy-regst-form upload-doc p-4">
                <p class="">Upload civil ID/ emirates ID/ passport or any national identification document.(max 5). </p>
                <div class="errors_ul">
                            
                </div>
                <div class="col-md-5">
                    <form method="post" name="documents_form" id="documents_form" class="buddy_request_Form uploader mt-5" enctype="multipart/form-data">
                        @csrf
                        
                        <input id="file-upload" type="file" multiple="" name="documents[]" onchange="readDocumentURL(this)">
                        <span class="invalid-feedback documents"></span>
                        
                        <label for="file-upload" id="file-drag">
                            <img id="file-image" src="#" alt="Preview" class="hidden">
                            <div id="start">
                                <div class="col-md-12">
                                    <img src="{{asset('img/ic_share@2x.png')}}">
                                </div>
                                <div id="notimage" class="hidden">Please select an image</div>
                                <span id="file-upload-btn" class="">Upload document</span>
                            </div>
                        </label>
                        <div class="image_prev">
                            <ul class="image_prev_ul">
                                
                            </ul>
                        </div>
                    </form>
                </div>
                <div class="col-md-12 text-right">
                    <button type="button" class="documents_submit_btn red-btn rounded pt-3 pb-3 mt-5 mb-3"><span>Update</span></button>
                </div>
            </div>
        </div>
    </div>
</div>
