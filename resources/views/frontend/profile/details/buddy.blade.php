@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
<section class="inner-body body-light-grey mt-70">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="list-inline breadcrum">
                    <li><a href="{{url(app()->getLocale().'/home/'.$link)}}">Home </a></li>
                    <span> / </span>
                    <li><a href="{{url(app()->getLocale().'/profile/'.$link)}}"> Profile</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            @include('frontend.profile.details.common.section1')

            <!-- Right Wrapper -->
            <div class="col-md-8 right-wrapper">


                 <!-- about me -->
                @include('frontend.profile.details.common.aboutme')

                <!-- Personal details -->
                @include('frontend.profile.details.common.personal')

                <!-- additional details -->
                @include('frontend.profile.details.common.additional')

                <!-- address details -->
                @include('frontend.profile.details.common.address')

                <!-- professionals -->
                @include('frontend.profile.details.common.professionals')

                <!-- documents -->
                @include('frontend.profile.details.common.documents')

                <!-- qualification details -->
                @include('frontend.profile.details.common.qualifications')

                <!-- promo -->
                @include('frontend.profile.details.common.promo')
            </div>
        </div>
    </div>
</section>
