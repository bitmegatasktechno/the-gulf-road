<div class="col-md-9">
	<div class="card p-4">
	    <ul class="nav nav-tabs" role="tablist">
			<li class="nav-item mr-3">
			    <a class="nav-link active" href="#rentListing" role="tab" data-toggle="tab">Rent</a>
			</li>
			<li class="nav-item">
			    <a class="nav-link" href="#saleListing" role="tab" data-toggle="tab">Sale</a>
			</li>
			<li class="nav-item">
			    <a class="nav-link" href="#rentsaleListing" role="tab" data-toggle="tab">Rent & Sale</a>
			</li>
	    </ul>
	    <!-- Tab panes -->
	    <div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="rentListing">
			    <div class="row mt-4">
			    	@if(count($data->where('listing_type','rent'))>0)
				        @foreach($data->where('listing_type','rent') as $row)
	                        <a href="{{url(app()->getLocale().'/property/'.$link.'/rent/'.$row->_id)}}">
		                        <div class="col-md-4">
		                            <div class="property-card bg-white rounded">
		                                @if(!isset($row->files[0]) || $row->files[0]=='')
		                                    <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}" style="height: 120px !important">
		                                @else
		                                    <img class="img-fluid property-img" src="{{url('uploads/properties/'.$row->files[0])}}" style="height: 120px !important">
		                                @endif
		                                
		                                <div class="pl-3 pr-3">
		                                    <h5>{{ucfirst($row->house_type)}}</h5>
		                                    
		                                    <h5 class="mt-1 mb-1">
		                                        {{\Str::limit(ucfirst($row->property_title), 25)}}</p>
		                                    </h5>
		                                    
		                                    <p class="mb-2">
		                                    	<i class="fa fa-map-marker mr-2"></i> {{\Str::limit($row->location_name, 18)}}, {{ucfirst($row->country)}}
		                                    </p>
		                                    
		                                    <h5 class="mt-2 mb-2">
		                                    	{{$row->rent ?? 0}} {{$row->rent_currency ?? 'N/A'}} / {{$row->rent_frequency ?? 'daily'}}
		                                    </h5>
		                                    
		                                    <ul class="list-inline red-list mb-4" style="display: flex;">
		                                        <li class="mr-3"><img class="mr-1" src="{{asset('img/red-bed.png')}}" title="Bedrooms"> {{$row->no_of_bedrooms}}</li>
		                                        <li class="mr-3"><img title="Bathrooms" class="mr-1" src="{{asset('img/red-tub.png')}}"> {{$row->no_of_bathrooms}}</li>
		                                        <li class="mr-3"><img title="Open Parkings" class="mr-1" src="{{asset('img/red-car.png')}}"> {{$row->no_of_open_parking}}</li>
		                                    </ul>
		                                    <h5 class="text-danger text-right">&nbsp;{{ $row->is_approved ? '' : 'Approval Pending' }}<h5>
		                                     
		                                    <h5><h5>
		                                </div>
		                                <a class="edit-icon" href="{{url(app()->getLocale().'/editproperty/'.$link.'/rent-sale/'.$row->_id)}}"><img  src="{{asset('img/edit.png')}}"></a>

		                                <a class="delete-icon delete_property" href="javascript:;" data-id="{{$row->_id}}">
		                                	<img  src="{{asset('img/delete.png')}}">
		                                </a>
		                            </div>
		                        </div>
	                   		</a>
	                    @endforeach
                    @else
                        <div class="" style="display: block;
                            max-height: 60px;
                            text-align: center;
                            background-color: #d6e9c6;
                            line-height: 4;
                            width: 100%;
                            ">
                            <p style="font-size: 16px;font-weight: 700;">No Result Found</p>
                        </div>
                    @endif
			    </div>
			</div>
			<div role="tabpanel" class="tab-pane fade" id="saleListing">
			    <div class="row mt-4">
			    	@if(count($data->where('listing_type','sale'))>0)
				        @foreach($data->where('listing_type','sale') as $row)
				        	<a href="{{url(app()->getLocale().'/property/'.$link.'/buy/'.$row->_id)}}">
		                        <div class="col-md-4">
		                            <div class="property-card bg-white rounded">
		                                @if(!isset($row->files[0]) || $row->files[0]=='')
		                                    <img class="img-fluid property-img" src="{{asset('/img/no-property.png')}}" style="height: 120px !important">
		                                @else
		                                    <img class="img-fluid property-img" src="{{url('uploads/properties/'.$row->files[0])}}" style="height: 120px !important">
		                                @endif
		                                
		                                <div class="pl-3 pr-3">
		                                    <h5>{{ucfirst($row->house_type)}}</h5>
		                                    
		                                    <h5 class="mt-1 mb-1">
		                                        {{\Str::limit(ucfirst($row->property_title), 25)}}</p>
		                                    </h5>
		                                    
		                                    <p class="mb-2">
		                                    	<i class="fa fa-map-marker mr-2"></i> {{\Str::limit($row->location_name, 20)}}, {{ucfirst($row->country)}}
		                                    </p>

		                                    <h5 class="mt-2 mb-2">
		                                    	{{$row->price ? number_format($row->price) : 0}} {{$row->price_currency ?? 'N/A'}}
		                                    </h5>
		                                    
		                                    <ul class="list-inline red-list mb-4" style="display: flex;">
		                                        <li class="mr-3"><img class="mr-1" src="{{asset('img/red-bed.png')}}" title="Bedrooms"> {{$row->no_of_bedrooms}}</li>
		                                        <li class="mr-3"><img title="Bathrooms" class="mr-1" src="{{asset('img/red-tub.png')}}"> {{$row->no_of_bathrooms}}</li>
		                                        <li class="mr-3"><img title="Open Parkings" class="mr-1" src="{{asset('img/red-car.png')}}"> {{$row->no_of_open_parking}}</li>
		                                    </ul>
		                                    <h5 class="text-danger text-right">&nbsp;{{ $row->is_approved ? '' : 'Approval Pending' }}<h5>
		                                </div>
		                                <a class="edit-icon" href="{{url(app()->getLocale().'/editproperty/'.$link.'/rent-sale/'.$row->_id)}}"><img  src="{{asset('img/edit.png')}}"></a>

		                                <a class="delete-icon delete_property" href="javascript:;" data-id="{{$row->_id}}">
		                                	<img  src="{{asset('img/delete.png')}}">
		                                </a>
		                                
		                            </div>
		                        </div>
	                    	</a>
	                    @endforeach
                    @else
                        <div class="" style="display: block;
                            max-height: 60px;
                            text-align: center;
                            background-color: #d6e9c6;
                            line-height: 4;
                            width: 100%;
                            ">
                            <p style="font-size: 16px;font-weight: 700;">No Result Found</p>
                        </div>
                    @endif
			    </div>
			</div>
			<div role="tabpanel" class="tab-pane fade" id="rentsaleListing">
			    <div class="row mt-4">
			    	@if(count($data->where('listing_type','both'))>0)
				        @foreach($data as $row)
				        	<a href="{{url(app()->getLocale().'/property/'.$link.'/buy/'.$row->_id)}}">
		                        <div class="col-md-4">
		                            <div class="property-card bg-white rounded">
		                                @if(!isset($row->files[0]) || $row->files[0]=='')
		                                    <img class="img-fluid property-img" src="{{asset('/img/no-property.png')}}" style="height: 120px !important">
		                                @else
		                                    <img class="img-fluid property-img" src="{{url('uploads/properties/'.$row->files[0])}}" style="height: 120px !important">
		                                @endif
		                                
		                                <div class="pl-3 pr-3">
		                                    <h5>{{ucfirst($row->house_type)}}</h5>
		                                    
		                                    <h5 class="mt-1 mb-1">
		                                        {{\Str::limit(ucfirst($row->property_title), 25)}}</p>
		                                    </h5>
		                                    
		                                    <p class="mb-2">
		                                    	<i class="fa fa-map-marker mr-2"></i> {{\Str::limit($row->location_name, 20)}}, {{ucfirst($row->country)}}
		                                    </p>
		                                    <h5 class="mt-2 mb-2">
		                                    	Sale: {{$row->price ?? 0}} {{$row->price_currency ?? 'USD'}}
		                                    </h5>
		                                    <h5 class="mt-2 mb-2">
		                                    	Rent: {{$row->rent ?? 0}} {{$row->rent_currency ?? 'N/A'}} /{{$row->rent_frequency ?? 'daily'}}
		                                    </h5>
		                                    
		                                    <ul class="list-inline red-list mb-4" style="display: flex;">
		                                        <li class="mr-3"><img class="mr-1" src="{{asset('img/red-bed.png')}}" title="Bedrooms"> {{$row->no_of_bedrooms}}</li>
		                                        <li class="mr-3"><img title="Bathrooms" class="mr-1" src="{{asset('img/red-tub.png')}}"> {{$row->no_of_bathrooms}}</li>
		                                        <li class="mr-3"><img title="Open Parkings" class="mr-1" src="{{asset('img/red-car.png')}}"> {{$row->no_of_open_parking}}</li>
		                                    </ul>
		                                    <h5 class="text-danger text-right">&nbsp;{{ $row->is_approved ? '' : 'Approval Pending' }}<h5>
		                                </div>
		                                <a class="edit-icon" href="{{url(app()->getLocale().'/editproperty/'.$link.'/rent-sale/'.$row->_id)}}"><img  src="{{asset('img/edit.png')}}"></a>

		                                <a class="delete-icon delete_property" href="javascript:;" data-id="{{$row->_id}}">
		                                	<img  src="{{asset('img/delete.png')}}">
		                                </a>
		                                
		                            </div>
		                        </div>
	                    	</a>
	                    @endforeach
                    @else
                        <div class="" style="display: block;
                            max-height: 60px;
                            text-align: center;
                            background-color: #d6e9c6;
                            line-height: 4;
                            width: 100%;
                            ">
                            <p style="font-size: 16px;font-weight: 700;">No Result Found</p>
                        </div>
                    @endif
			    </div>
			</div>
	    </div>
	</div>
</div>
