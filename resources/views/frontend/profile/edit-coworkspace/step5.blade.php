<section class="step5 col-md-12 listPropertySteps">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h5>Enter Your Contact Details</h5>
                <p>This will let people get in touch with you.</p>
                
                <form class="loc-form" name="rent_sale_property_step_5" id="rent_sale_property_step_5">
                    <div class="form-group">
                        <label>Email (Required)</label>
                        <input type="text" name="email" class="form-control" value="{{$data->email}}">
                        <span class="invalid-feedback email"></span>
                    </div>
                    <div class="form-group">
                        <small class="full-width"><img class="mr-2" src="{{asset('img/info.png')}}"> All Notifications will be sent to this email address.</small>
                    </div>
                    <div class="form-group">
                        <label>Contact Number (Required)</label>
                        <input type="text" name="contact_number" class="form-control" value="{{$data->contact_number}}">
                        <span class="invalid-feedback contact_number"></span>
                    </div>
                    <div class="form-group">
                        <label>Website </label>
                        <input type="text" name="website_url" class="form-control" value="{{$data->website_url}}">
                        <span class="invalid-feedback website_url"></span>
                    </div>
                    <h5>Enter Your Contact Details</h5>
                    <p>This will let people get in touch with you.</p>
                    <div class="form-group">
                        <label>Facebook </label>
                        <input type="text" name="facebook_url" class="form-control" placeholder="facebook.com/coworking" value="{{$data->facebook_url}}">
                        <span class="invalid-feedback facebook_url"></span>
                    </div>
                    <div class="form-group">
                        <label>Instagram </label>
                        <input type="text" name="instagram_url" class="form-control" placeholder="instagram.com/coworking" value="{{$data->instagram_url}}">
                        <span class="invalid-feedback instagram_url"></span>
                    </div>
                    <div class="form-group">
                        <label>Twitter</label>
                        <input type="text" name="twitter_url" class="form-control" placeholder="twitter.com/coworking" value="{{$data->twitter_url}}">
                        <span class="invalid-feedback twitter_url"></span>
                    </div>
                </form>

                <div class="pt-3 pb-3">
                    <hr>
                </div>
                <div class="clearfix"></div>
    
                <div class="row float-right">
                    <div class="col-md-6 text-right">
                        <button class="red-btn rounded float-right submit_step_5" type="button">
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>