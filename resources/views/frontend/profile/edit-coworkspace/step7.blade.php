<section class="step6 col-md-12 listPropertySteps" >
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form class="loc-form" name="rent_sale_property_step_7" id="rent_sale_property_step_7"> 
                    <!-- open space -->
                    <div class="sale_price_list">
                        <div class="row mt-3">
                            <div class="col-md-10">
                                <h5>Open Workspace</h5>
                            </div>
                        </div>
                        <div>
                            <div class="row mt-4">                                           
                                <div class="col-md-11">
                                    <div class="form-group">
                                        <label>Number of Seats</label>
                                        <select class="form-control" name="open_space_seats">
                                            <option value="">Select</option>
                                            @foreach(range(1,100) as $row)
                                                <option value="{{$row}}" @if($row==$data->open_space_seats) selected @endif>{{$row}} People</option>
                                            @endforeach
                                            
                                        </select>
                                        <span class="invalid-feedback open_space_seats"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>
                                    Price
                                </h5>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Duration</label>
                                    <div class="listspace-price-tab-duration">
                                        1 Day
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label>Price</label>
                                        <input class="form-control" type="text" name="open_space_daily_price" placeholder="Price" value="{{$data->open_space_daily_price}}">
                                        <span class="invalid-feedback open_space_daily_price"></span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Currency</label>
                                        <select class="form-control rounded" name="open_space_daily_currency">
                                            @foreach(getAllCurrencies() as $row)
                                                <option value="{{$row->code}}" @if($row->code==$data->open_space_daily_currency) selected @endif>{{$row->code}}</option>
                                            @endforeach
                                        </select>
                                        <span class="invalid-feedback open_space_daily_currency"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="listspace-price-tab-duration">
                                        1 Week
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="open_space_weekly_price" placeholder="Price" value="{{$data->open_space_weekly_price}}">
                                        <span class="invalid-feedback open_space_weekly_price"></span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control rounded" name="open_space_weekly_currency">
                                            @foreach(getAllCurrencies() as $row)
                                                <option value="{{$row->code}}" @if($row->code==$data->open_space_weekly_currency) selected @endif>{{$row->code}}</option>
                                            @endforeach
                                        </select>
                                        <span class="invalid-feedback open_space_weekly_currency"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="listspace-price-tab-duration">
                                        1 Month
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="open_space_monthly_price" placeholder="Price" value="{{$data->open_space_monthly_price}}"> 
                                        <span class="invalid-feedback open_space_monthly_price"></span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control rounded" name="open_space_monthly_currency">
                                            @foreach(getAllCurrencies() as $row)
                                                <option value="{{$row->code}}" @if($row->code==$data->open_space_monthly_currency) selected @endif>{{$row->code}}</option>
                                            @endforeach
                                        </select>
                                        <span class="invalid-feedback open_space_monthly_currency"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <hr>
                    </div>

                    <!-- dedicated space -->
                    <div class="sale_price_list">
                        <div class="row mt-3">
                            <div class="col-md-10">
                                <h5>Dedicated Desk</h5>
                            </div>
                            <div class="col-md-2">
                                <div class="checkbox switcher">
                                    <a href="javascript:;">
                                    <label for="dedicated_desk_available">
                                        <input type="checkbox" name="dedicated_desk_available" id="dedicated_desk_available" value="1" onchange="dedicatedDeskAvailable('dedicated_desk_available_div');" @if($data->dedicated_desk_available==1) checked @endif>
                                        <span><small></small></span>
                                    </label></a>
                                </div>
                            </div>
                        </div>
                        <div id="dedicated_desk_available_div" style="display: @if($data->dedicated_desk_available!=1) none @endif">
                            <div class="row  mt-4">                                           
                                <div class="col-md-11">
                                    <div class="form-group">
                                        <label>Number of Seats</label>
                                        <select class="form-control" name="dedicated_space_seats">
                                            <option value="">Select</option>
                                            @foreach(range(1,100) as $row)
                                                <option value="{{$row}}" @if($row==$data->dedicated_space_seats) selected @endif>{{$row}} People</option>
                                            @endforeach
                                            
                                        </select>
                                        <span class="invalid-feedback dedicated_space_seats"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <h5>
                                    Price
                                </h5>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <label>Duration</label>
                                    <div class="listspace-price-tab-duration">
                                        1 Day
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label>Price</label>
                                        <input class="form-control" type="text" name="dedicated_space_daily_price" placeholder="Price" value="{{$data->dedicated_space_daily_price}}">
                                        <span class="invalid-feedback dedicated_space_daily_price"></span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Currency</label>
                                        <select class="form-control rounded" name="dedicated_space_daily_currency">
                                            @foreach(getAllCurrencies() as $row)
                                                <option value="{{$row->code}}" @if($row->code==$data->dedicated_space_daily_currency) selected @endif>{{$row->code}}</option>
                                            @endforeach
                                        </select>
                                        <span class="invalid-feedback dedicated_space_daily_currency"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="listspace-price-tab-duration">
                                        1 Week
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="dedicated_space_weekly_price" placeholder="Price" value="{{$data->dedicated_space_weekly_price}}">
                                        <span class="invalid-feedback dedicated_space_weekly_price"></span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control rounded" name="dedicated_space_weekly_currency">
                                            @foreach(getAllCurrencies() as $row)
                                                <option value="{{$row->code}}" @if($row->code==$data->dedicated_space_weekly_currency) selected @endif>{{$row->code}}</option>
                                            @endforeach
                                        </select>
                                        <span class="invalid-feedback dedicated_space_weekly_currency"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="listspace-price-tab-duration">
                                        1 Month
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="dedicated_space_monthly_price" placeholder="Price" value="{{$data->dedicated_space_monthly_price}}">
                                        <span class="invalid-feedback dedicated_space_monthly_price"></span>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <select class="form-control rounded" name="dedicated_space_monthly_currency">
                                            @foreach(getAllCurrencies() as $row)
                                                <option value="{{$row->code}}" @if($row->code==$data->dedicated_space_monthly_currency) selected @endif>{{$row->code}}</option>
                                            @endforeach
                                        </select>
                                        <span class="invalid-feedback dedicated_space_monthly_currency"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <hr>
                    </div> 
                    <!-- private space -->
                    <div class="sale_price_list">
                        <div class="row mt-3">
                            <div class="col-md-10">
                                <h5>Private Office</h5>
                            </div>
                            <div class="col-md-2">
                                <div class="checkbox switcher">
                                    <a href="javascript:;">
                                    <label for="private_office_available">
                                        <input type="checkbox" name="private_office_available" id="private_office_available" value="1" onchange="privateOfficeAvailable('private_office_available_div');" @if($data->private_office_available==1) checked @endif>
                                        <span><small></small></span>
                                    </label></a>
                                </div>
                            </div>
                        </div>
                        <div id="private_office_available_div" class="mt-4" style="display: @if($data->private_office_available!=1) none @endif">
                            <div class="row private-offices-panel-main">
                                <div class="col-md-12">

                                    <div class="row mt-4">                              
                                        <div class="col-md-11">
                                            <div class="form-group">
                                                <label>Number of Offices</label>
                                                <select class="form-control" name="no_of_private_office">
                                                    <option value="">Select</option>
                                                    @foreach(range(1,100) as $row)
                                                        <option value="{{$row}}" @if($row==$data->no_of_private_office) selected @endif>{{$row}}</option>
                                                    @endforeach
                                                    
                                                </select>
                                                <span class="invalid-feedback no_of_private_office"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">                                           
                                        <div class="col-md-11">
                                            <div class="form-group">
                                                <label>Number of Seats</label>
                                                <select class="form-control" name="seating_capacity">
                                                    <option value="">Select</option>
                                                    @foreach(range(1,100) as $row)
                                                        <option value="{{$row}}" @if($row==$data->seating_capacity) selected @endif>{{$row}} People</option>
                                                    @endforeach
                                                    
                                                </select>
                                                <span class="invalid-feedback seating_capacity"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group mt-4">
                                        <h5>
                                            Price
                                        </h5>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>Duration</label>
                                            <div class="listspace-price-tab-duration">
                                                1 Day
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label>Price</label>
                                                <input class="form-control" type="text" name="private_space_daily_price" placeholder="Price" value="{{$data->private_space_daily_price}}">

                                                <span class="invalid-feedback private_space_daily_price"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Currency</label>
                                                <select class="form-control rounded" name="private_space_daily_currency">
                                                    @foreach(getAllCurrencies() as $row)
                                                        <option value="{{$row->code}}" @if($row->code==$data->private_space_daily_currency) selected @endif>{{$row->code}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="invalid-feedback private_space_daily_currency"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="listspace-price-tab-duration">
                                                1 Week
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <input class="form-control" type="text" name="private_space_weekly_price" placeholder="Price" value="{{$data->private_space_weekly_price}}">
                                                <span class="invalid-feedback private_space_weekly_price"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <select class="form-control rounded" name="private_space_weekly_currency">
                                                    @foreach(getAllCurrencies() as $row)
                                                        <option value="{{$row->code}}" @if($row->code==$data->dedicated_space_weekly_currency) selected @endif>{{$row->code}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="invalid-feedback private_space_weekly_currency"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="listspace-price-tab-duration">
                                                1 Month
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <input class="form-control" type="text" name="private_space_monthly_price" placeholder="Price" value="{{$data->private_space_monthly_price}}">
                                                <span class="invalid-feedback private_space_monthly_price"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <select class="form-control rounded" name="private_space_monthly_currency">
                                                    @foreach(getAllCurrencies() as $row)
                                                        <option value="{{$row->code}}" @if($row->code==$data->dedicated_space_monthlycurrency) selected @endif>{{$row->code}}</option>
                                                    @endforeach
                                                </select>
                                                <span class="invalid-feedback private_space_monthly_currency"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-md-7">
                                            <h5>Does the office have a whiteboard ?</h5>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="checkbox switcher">
                                                <a href="javascript:;">
                                                <label for="white_board_available">
                                                    <input type="checkbox" name="white_board_available" id="white_board_available" value="1" @if($data->white_board_available==1) checked @endif>
                                                    <span><small></small></span>
                                                </label></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="pt-3 pb-3">
                    <hr>
                </div>
                <div class="clearfix"></div>
    
                <div class="row float-right">
                    <div class="col-md-6 text-right">
                        <button class="red-btn rounded float-right submit_step_7" type="button">
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
