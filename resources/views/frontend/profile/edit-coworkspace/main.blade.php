@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
<div class="col-md-9 right-wrapper">
    <div class="inner-white-wrapper personal-details mb-3 overflow-hidden">
        <div class="row dp-flex align-items-center">
            <div class="col-md-6">
                <h3>
                	<a href="{{url(app()->getLocale().'/mycoworkspaces/'.$link)}}"><- Back to Listing</a>
                </h3>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
            <div class="col-md-12">
            	<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item mr-3">
					    <a class="nav-link active" href="#step1Form" role="tab" data-toggle="tab">Description</a>
					</li>
					<li class="nav-item">
					    <a class="nav-link" href="#step2Form" role="tab" data-toggle="tab">Location</a>
					</li>
					<li class="nav-item">
					    <a class="nav-link" href="#step3Form" role="tab" data-toggle="tab">Amenities</a>
					</li>
					<li class="nav-item">
					    <a class="nav-link" href="#step4Form" role="tab" data-toggle="tab">Timings</a>
					</li>
					<li class="nav-item">
					    <a class="nav-link" href="#step5Form" role="tab" data-toggle="tab">Contact Details</a>
					</li>
					<li class="nav-item">
					    <a class="nav-link" href="#step6Form" role="tab" data-toggle="tab">Photos</a>
					</li>
					<li class="nav-item">
					    <a class="nav-link" href="#step7Form" role="tab" data-toggle="tab">Set Price</a>
					</li>
			    </ul>
			    <!-- Tab panes -->
			    <div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="step1Form">
					    <div class="row mt-4">
					    	@include('frontend.profile.edit-coworkspace.step1')
					    </div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="step2Form">
					    <div class="row mt-4">
					    	@include('frontend.profile.edit-coworkspace.step2')
					    </div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="step3Form">
					    <div class="row mt-4">
					    	@include('frontend.profile.edit-coworkspace.step3')
					    </div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="step4Form">
					    <div class="row mt-4">
					    	@include('frontend.profile.edit-coworkspace.step4')
					    </div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="step5Form">
					    <div class="row mt-4">
					    	@include('frontend.profile.edit-coworkspace.step5')
					    </div>
					</div>

					<div role="tabpanel" class="tab-pane fade" id="step6Form">
					    <div class="row mt-4">
					    	@include('frontend.profile.edit-coworkspace.step6')
					    </div>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="step7Form">
					    <div class="row mt-4">
					    	@include('frontend.profile.edit-coworkspace.step7')
					    </div>
					</div>
			    </div>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
        </div>
    </div>
</div>