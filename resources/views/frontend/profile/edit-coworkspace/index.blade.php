@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
@extends('frontend.layouts.home')
@section('title','Edit Coworkspace')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/datepicker.css')}}">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<style type="text/css">
    .visibly-hidden {
        position: absolute !important;
        height: 1px; width: 1px;
        overflow: hidden;
        clip: rect(1px 1px 1px 1px); 
        clip: rect(1px, 1px, 1px, 1px);
        pointer-events: none;
    }

    .radio-label{
        cursor: pointer;
        user-select: none;
        background: white;
        border: 1px solid #ccc;
        padding: 10px;
    }

    input[type="radio"]:checked + label{
        border-color: #309cf3 !important;
        color: white;
        background-color: #309cf3 !important;
    }

    input[type="radio"]:checked + label{
        border-color: #309cf3 !important;
        color: white;
        background-color: #309cf3 !important;
    }
    .nav-link{
        padding: .5rem;
    }
</style>
@endsection

@section('content')
@include('frontend.layouts.default-header')
<section class="inner-body body-light-grey mt-70">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="list-inline breadcrum">
                    <li><a href="{{url(app()->getLocale().'/home/'.$link)}}">Home </a></li>
                    <span> / </span>
                    <li><a href="{{url(app()->getLocale().'/mycoworkspaces/'.$link)}}"> My Coworkspace</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <!-- Navigation Block -->
                <div class="inner-white-wrapper left-navigation mb-3">
@include('frontend.profile.user_profile_sidebar')
                </div>
            </div>
            <input type="hidden" name="property_id" value="{{$data->_id}}">
            <!-- Right Wrapper -->
            @include('frontend.profile.edit-coworkspace.main')
        </div>
    </div>
</section>
@include('frontend.layouts.footer')
@endsection

@section('scripts')
<script type="text/javascript" src="{{asset('js/datepicker.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script type="text/javascript">
    let propertySteps = false;
    const property_id = $('input[name="property_id"]').val();

function file_validation(id,max_size)
   {
       var fuData = document.getElementById(id);
       var FileUploadPath = fuData.value;
       
   
       if (FileUploadPath == ''){
           alert("Please upload Attachment");
       } 
       else {
           var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
           if (Extension == "jpg" || Extension == "jpeg" || Extension == "png" || Extension == "webp" ) {
   
                   if (fuData.files && fuData.files[0]) {
                       var size = fuData.files[0].size;
                       
                       if(size > max_size){   //1000000 = 1 mb
                           
                           Swal.fire({
                                title: 'Warning!',
                                text: "Maximum file size "+max_size/1000000+" MB",
                                icon: 'info',
                                confirmButtonText: 'Ok'
                            });
                            
                           $("#"+id).val('');
                           return false;
                            
                       }
                   }
   
           } 
           else 
           {
                
               $("#"+id).val('');
                Swal.fire({
                                title: 'Warning!',
                                text: "document only allows file types of  jpg , png  , jpeg , webp ",
                                icon: 'info',
                                confirmButtonText: 'Ok'
                            });
                            
                           $("#"+id).val('');
                           return false;
           }
       }   
   } 
   
    const readURL = function(input) {

        var id = $(input).attr('id');
         var max_size = 2000000;
           file_validation(id,max_size);


        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var id=input.id;
                $('#'+id+'_src').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    const privateOfficeAvailable = function(divid){
        if($('#'+divid).is(':visible')){
            $('#'+divid).hide();
        }else{
            $('#'+divid).show();
        }
    }

    const dedicatedDeskAvailable = function(divid){
        if($('#'+divid).is(':visible')){
            $('#'+divid).hide();
        }else{
            $('#'+divid).show();
        }
    }

    $(document).ready(function(){
        $("html, body").animate({ scrollTop: 0 }, "slow");

        $('input.timepicker').timepicker({
            timeFormat: 'HH:mm',
        });

        $(document).on('change','.operating_radio', function(){
            if($(this).val()=='open'){
                $('.'+$(this).data('id')).show();
            }else{
                $('.'+$(this).data('id')).hide();
            }
        });

        $(document).on('click','#add_more', function(){
            if($('.upload').length>=15){
                Swal.fire({
                    title: 'Warning!',
                    text: "You can upload maximum 15 pictures.",
                    icon: 'info',
                    confirmButtonText: 'Ok'
                });
                return false;
            }
            var id= $('.upload').length+1;
            $("#pictureresult").append('\
                <div class="upload col-md-3 mb-3">\
                    <input type="file" id="files'+id+'" name="files[]" class="input-file" onchange="readURL(this)" >\
                    <label for="files'+id+'">\
                        <img id="files'+id+'_src" src="{{asset("img/addmedia-upload.png")}}" style="height:120px;width:120px;">\
                    </label>\
                    <span class="remove removeDiv" style="cursor:pointer;position: absolute;top: -16px;right: 8px;font-size: 22px;"><i class="fa fa-times" aria-hidden="true"></i></span>\
                    <span class="invalid-feedback files'+id+'"></span>\
                </div>');
        });

        $(document).on('click','.removeDiv', function(){
            $(this).parent().remove();
        });

        $(document).on('click','.specialAmenitiesBlock', function(){
            $(this).toggleClass('active');
            $(this).find('.special_amenity_check').toggleClass('active');
        });


        $(document).on('change','.country_change', function(e){
            e.preventDefault();
            var country = $(this).val();
            var id = $(this).data('change');
            if(!country){
                return false;
            }
            var ajax_url = WEBSITE_URL+"/states/"+country;    
            $.ajax({
                url : ajax_url,
                type:'get',
                dataType : 'json',
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.coworkspace_master_form');
                },
                complete:function(){
                   stopLoader('.coworkspace_master_form'); 
                },
                success : function(data){
                    var html='<option value="">Select State</option>';
                        
                    if(data.states){
                        $.each(data.states, function(key, val){
                            html+='<option value="'+val+'">'+val+'</option>';
                        });
                    }
                    $('select[name="state"]').html(html);
                },
                error : function(data){
                    stopLoader('.coworkspace_master_form');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                    }
                }
            });
        });
        // save first form of property
        $(document).on('click','.submit_step_1', function(){
            var ajax_url = WEBSITE_URL+"/updateProperty/coworkspace/1";
            var $form = $('#property_step_1');
            $form.find('.is-invalid').removeClass('is-invalid');
            $form.find('.invalid-feedback').text('');

            var data = new FormData();
            data.append('title', $form.find('input[name="title"]').val());
            data.append('description', $form.find('textarea[name="description"]').val());
            data.append('description', $form.find('textarea[name="description"]').val());
            data.append('property_id', property_id);

            $.ajax({
                url : ajax_url,
                type:'post',
                data : data,
                dataType : 'json',
                async:true,
           		processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.listPropertySteps');
                },
                complete:function(){
                   stopLoader('.listPropertySteps'); 
                },
                success : function(data){
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then(function(){
                            window.location.reload();
                        });

                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                },
                error : function(data){
                    stopLoader('.listPropertySteps');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                        $.each(err_response.errors, function(i, obj){
                            $form.find('span.invalid-feedback.'+i+'').text(obj).show();
                            $form.find('input[name="'+i+'"]').addClass('is-invalid');
                            $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
                            $form.find('select[name="'+i+'"]').addClass('is-invalid');
                        });
                    }
                }
            });
        });

        $(document).on('click','.submit_step_2', function(){
            var ajax_url = WEBSITE_URL+"/updateProperty/coworkspace/2";
            var $form = $('#property_step_2');
            $form.find('.is-invalid').removeClass('is-invalid');
            $form.find('.invalid-feedback').text('');

            var data = new FormData($('#property_step_2')[0]);
            data.append('property_id', property_id);

            $.ajax({
                url : ajax_url,
                type:'post',
                data : data,
                async:true,
                dataType : 'json',
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.listPropertySteps');
                },
                complete:function(){
                   stopLoader('.listPropertySteps'); 
                },
                success : function(data){
                    console.log(data);
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then(function(){
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        });
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                },
                error : function(data){
                    stopLoader('.listPropertySteps');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                        $.each(err_response.errors, function(i, obj){
                            $form.find('span.invalid-feedback.'+i+'').text(obj).show();
                            $form.find('input[name="'+i+'"]').addClass('is-invalid');
                            $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
                            $form.find('select[name="'+i+'"]').addClass('is-invalid');
                        });
                    }
                }
            });
        });

        $(document).on('click','.submit_step_3', function(){
            var ajax_url = WEBSITE_URL+"/updateProperty/coworkspace/3";
            var data = new FormData();
            var count = 0;
            $('.specialAmenitiesBlock').each(function(i){
                if($(this).is('.active')){
                    if($(this).data('type')=='food'){
                        data.append('food_amenties[]', $(this).attr('data-id'));
                        count++;
                    }
                    else if($(this).data('type')=='facilities'){
                        data.append('facilities_amenties[]', $(this).attr('data-id'));
                        count++;
                    }
                    else if($(this).data('type')=='equipments'){
                        data.append('equipments_amenties[]', $(this).attr('data-id'));
                        count++;
                    }
                    else if($(this).data('type')=='seating'){
                        data.append('seating_amenties[]', $(this).attr('data-id'));
                        count++;
                    }
                    else if($(this).data('type')=='accessibility'){
                        data.append('accessibility_amenties[]', $(this).attr('data-id'));
                        count++;
                    }
                }
            });
            if(count==0){
                Swal.fire({
                    title: 'Warning!',
                    text: "Please select Atleast one from all Amenties Categories",
                    icon: 'info',
                    confirmButtonText: 'Ok'
                });
                return false;
            }
            data.append('property_id', property_id);

            $.ajax({
                url : ajax_url,
                type:'post',
                data : data,
                dataType : 'json',
                async:true,
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.listPropertySteps');
                },
                complete:function(){
                   stopLoader('.listPropertySteps'); 
                },
                success : function(data){
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then(function(){
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        });
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                },
                error : function(data){
                    stopLoader('.listPropertySteps');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                    }
                }
            });
        });

        $(document).on('click','.submit_step_4', function(){
            var ajax_url = WEBSITE_URL+"/updateProperty/coworkspace/4";
            var $form = $('#rent_sale_property_step_4');
            $form.find('.is-invalid').removeClass('is-invalid');
            $form.find('.invalid-feedback').text('');

            var data = new FormData($form[0]);
            data.append('property_id', property_id);

            $.ajax({
                url : ajax_url,
                type:'post',
                data : data,
                async:true,
                dataType : 'json',
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.listPropertySteps');
                },
                complete:function(){
                   stopLoader('.listPropertySteps'); 
                },
                success : function(data){
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then(function(){
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        });
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                },
                error : function(data){
                    stopLoader('.listPropertySteps');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                        $.each(err_response.errors, function(i, obj){
                            $form.find('span.invalid-feedback.'+i+'').text(obj).show();
                            $form.find('input[name="'+i+'"]').addClass('is-invalid');
                            $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
                            $form.find('select[name="'+i+'"]').addClass('is-invalid');
                        });
                    }
                }
            });
        });

        $(document).on('click','.submit_step_5', function(){
            var ajax_url = WEBSITE_URL+"/updateProperty/coworkspace/5";
            var $form = $('#rent_sale_property_step_5');
            $form.find('.is-invalid').removeClass('is-invalid');
            $form.find('.invalid-feedback').text('');

            var data = new FormData($form[0]);
            data.append('property_id', property_id);

            $.ajax({
                url : ajax_url,
                type:'post',
                data : data,
                async:true,
                dataType : 'json',
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.listPropertySteps');
                },
                complete:function(){
                   stopLoader('.listPropertySteps'); 
                },
                success : function(data){
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then(function(){
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        });
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                },
                error : function(data){
                    stopLoader('.listPropertySteps');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                        $.each(err_response.errors, function(i, obj){
                            $form.find('span.invalid-feedback.'+i+'').text(obj).show();
                            $form.find('input[name="'+i+'"]').addClass('is-invalid');
                            $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
                            $form.find('select[name="'+i+'"]').addClass('is-invalid');
                        });
                    }
                }
            });
        });

        $(document).on('click','.submit_step_6', function(){
            var ajax_url = WEBSITE_URL+"/updateProperty/coworkspace/6";
            var $form = $('#rent_sale_property_step_6');
            $form.find('.is-invalid').removeClass('is-invalid');
            $form.find('.invalid-feedback').text('');

            var data = new FormData($form[0]);
            data.append('property_id', property_id);

            var error = false;
            $.each($('#pictureresult [type=file]'), function(index, file) {
                if($('input[type=file]')[index].files[0]==undefined){
                    var id = $('input[type=file]')[index].id;
                    $('.invalid-feedback.'+(id)).text('The file is required.').show();
                    error = true;
                }
            });

            if(error){
                return false;
            }
            if($('.upload').length==0){
                Swal.fire({
                    title: 'Warning!',
                    text: "Please upload minimum 5 pictures.",
                    icon: 'info',
                    confirmButtonText: 'Ok'
                });
                return false;
            }
            if($('.upload').length<5){
                Swal.fire({
                    title: 'Warning!',
                    text: "Please upload minimum 5 pictures.",
                    icon: 'info',
                    confirmButtonText: 'Ok'
                });
                return false;
            }

            $.ajax({
                url : ajax_url,
                type:'post',
                data : data,
                enctype: 'multipart/form-data',
                dataType : 'json',
                async:true,
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.listPropertySteps');
                },
                complete:function(){
                   stopLoader('.listPropertySteps'); 
                },
                success : function(data){
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then(function(){
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        });
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                },
                error : function(data){
                    stopLoader('.listPropertySteps');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                        $.each(err_response.errors, function(key, value){
                            if(key.indexOf('.') != -1) {
                                let keys = key.split('.');                                
                                $('input[name="'+keys[0]+'[]"]').eq(keys[1]).addClass('is-invalid').parent().find('.invalid-feedback').text(value).show();                         
                            }
                            else {
                                $('input[name="'+key+'[]"]').eq(0).addClass('is-invalid').parent().find('.invalid-feedback').text(value).show();
                            }
                        });
                    }
                }
            });
        });

        $(document).on('click','.submit_step_7', function(){
            var ajax_url = WEBSITE_URL+"/updateProperty/coworkspace/7";
            var $form = $('#rent_sale_property_step_7');
            $form.find('.is-invalid').removeClass('is-invalid');
            $form.find('.invalid-feedback').text('');

            var data = new FormData($form[0]);
            data.append('property_id', property_id);
            var dedicated_desk_available = $('input[name="dedicated_desk_available"]:checked').attr('value') ? 1 : 0;
            var private_office_available = $('input[name="private_office_available"]:checked').attr('value') ? 1 : 0;
            var white_board_available = $('input[name="white_board_available"]:checked').attr('value') ? 1 : 0;
            
            data.append('dedicated_desk_available', dedicated_desk_available);
            data.append('private_office_available', private_office_available);
            data.append('white_board_available', white_board_available);
            

            $.ajax({
                url : ajax_url,
                type:'post',
                data : data,
                dataType : 'json',
                async:true,
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.listPropertySteps');
                },
                complete:function(){
                   stopLoader('.listPropertySteps'); 
                },
                success : function(data){
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then(function(){
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        });
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                },
                error : function(data){
                    stopLoader('.listPropertySteps');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                        $.each(err_response.errors, function(i, obj){
                            $form.find('span.invalid-feedback.'+i+'').text(obj).show();
                            $form.find('input[name="'+i+'"]').addClass('is-invalid');
                            $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
                            $form.find('select[name="'+i+'"]').addClass('is-invalid');
                        });
                    }
                }
            });
        });
    });

</script>
@endsection