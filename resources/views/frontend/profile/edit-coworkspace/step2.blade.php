
<section class="step2 listPropertySteps">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form class="loc-form" name="property_step_2" id="property_step_2">
                    <div class="p-4">
                        <h5>Let’s find your location on the map.</h5>
                        <p>Type your coworking space address or street address below.</p>

                        <div class="row"  hidden>
                                 <div class="col-md-6"  hidden>
                                   <input type="text" hidden name="latitude" id="lat" value="{{$data->latitude}}" readonly >
                                 </div>
                                 <div class="col-md-6">
                                     <input  type="text" hidden  name="longitude" id="lng" value="{{$data->longitude}}" readonly> 
                                 </div>
                            </div>
                        <div class="form-group" style="position: relative;">
                            <input class="form-control" type="text"  name="address" placeholder="Enter coworking space or street address." value="{{$data->address}}" onchange="getLocation()" id="address" >
                            <span class="invalid-feedback address"></span>
                            
                            
                             
                        </div>
                        <div class="form-group">
                          <div id="map" style="height: 200px;width: 100%"></div>
                        </div>
                        
                        <h5>Let’s find your location on the map.</h5>
                        <p>Type your coworking space address or street address below.</p>

                        <div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Location Name</label>
                                        <input type="text" name="location_name" class="form-control"   value="{{$data->location_name}}">
                                        <span class="invalid-feedback location_name" ></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Unit/Floor No.</label>
                                        <input type="text" name="unit_number" class="form-control" value="{{$data->unit_number}}"><span class="invalid-feedback unit_number"></span>
                                        
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Address Line 1</label>
                                        <input type="text" name="address_line_1" class="form-control" value="{{$data->address_line_1}}"><span class="invalid-feedback address_line_1"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Address Line 2</label>
                                        <input type="text" name="address_line_2" class="form-control" value="{{$data->address_line_2}}"><span class="invalid-feedback address_line_2"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Landmark</label>
                                        <input type="text" name="landmark" class="form-control" value="{{$data->landmark}}"><span class="invalid-feedback landmark"></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Town/City</label>
                                        <input type="text" name="city" class="form-control" value="{{$data->city}}"><span class="invalid-feedback city"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Country</label>
                                        <select class="form-control country_change" name="country" data-change="native_state">
                                            <option value="">Select Country</option>
                                            @foreach(getAllCountries() as $row)
                                                <option value="{{ $row->name }}" @if($row->name==$data->country) selected @endif>{{ __($row->name) }}</option>
                                            @endforeach
                                        </select><span class="invalid-feedback country"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>State</label>
                                        <select class="form-control" name="state">
                                            <option value=""> Select State</option>
                                            @foreach(@$state_data as $row)
                                                <option value="{{ $row }}" @if($row==$data->state) selected @endif>{{ __($row) }}</option>
                                            @endforeach
                                        </select><span class="invalid-feedback state"></span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Zip Code</label>
                                        <input type="text" name="zip_code" class="form-control" value="{{$data->zip_code}}"><span class="invalid-feedback zip_code"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pt-3 pb-3">
                        <hr>
                    </div>
                    <div class="clearfix"></div>
        
                    <div class="row float-right">
                        <div class="col-md-6 text-right">
                            <button class="red-btn rounded float-right submit_step_2" type="button">
                                Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
 

@if(env('APP_ACTIVE_MAP') =='patel_map')
<style>
    #address_result
    {
        z-index: 9;
        left: unset !important;
        top:unset !important;
        width:  100% !important;
    }


</style>
   <script src="https://mapapi.cloud.huawei.com/mapjs/v1/api/js?callback=initMap&key={{env('PATEL_MAP_KEY')}}"></script>
    <script>
        function initMap() {
            var mapOptions = {};



            var lat_long = <?php echo json_encode($data->location);?>;
            var title = '<?php echo ($data->address)?>';
            var locations = lat_long;
            console.log(lat_long);
            var myLatLng = {
                        lat: (locations[1] ? locations[1] : -25.363),
                        lng: (locations[0] ? locations[0] : 131.044)
                                };
            mapOptions.center = myLatLng;
            mapOptions.zoom = 8;
            mapOptions.language='ENG';
            var searchBoxInput = document.getElementById('address');
             var map = new HWMapJsSDK.HWMap(document.getElementById('map'), mapOptions);
            // Create the search box parameter.
            var acOptions = {
                location: {
                    lat: 48.856613,
                    lng: 2.352222
                },
                radius: 10000,
                customHandler: callback,
                language:'en'
            };
            var autocomplete;
            // Obtain the input element.
            // Create the HWAutocomplete object and set a listener.
            autocomplete = new HWMapJsSDK.HWAutocomplete(searchBoxInput, acOptions);
            autocomplete.addListener('site_changed', printSites);
            // Process the search result. 
            // index: index of a searched place.
            // data: details of a searched place. 
            // name: search keyword, which is in strong tags. 
            function callback(index, data, name) {
                
                var div
                div = document.createElement('div');
                div.innerHTML = name;
                return div;
            }
            // Listener.
            function printSites() {
                 
                
                var site = autocomplete.getSite();
                var latitude = site.location.lat;
                var longitude = site.location.lng;
                 var country_name = site.address.country;
                
                var results = "Autocomplete Results:\n";
                var str = "Name=" + site.name + "\n"
                    + "SiteID=" + site.siteId + "\n"
                    + "Latitude=" + site.location.lat + "\n"
                    + "Longitude=" + site.location.lng + "\n"
                    + "Address=" + site.formatAddress + "\n";

                    
                results = results + str;

                 


                $('#lat').val(latitude);
                 $('#lng').val(longitude);
              $('select[name=country]').val(country_name);
              $( "select[name=country]" ).trigger( "change" );
                console.log(latitude, longitude);
                document.getElementById('map').innerHTML='';
                /*start map zoom creation */
                /*console.log(results);*/
                var myLatLng = {
                  lat: latitude,
                  lng: longitude
                };

                var mapOptions = {};
                mapOptions.center = myLatLng;
                mapOptions.zoom = 5; 
                var map = new HWMapJsSDK.HWMap(document.getElementById('map'), mapOptions);
                mMarker.setMap(null);
                mMarker = null;


                mMarker = new HWMapJsSDK.HWMarker({
                              map: map,
                              position: myLatLng,
                              zIndex: 10,
                              label: {
                                text    : country_name,
                                offsetY : -30,
                                fontSize: '20px'
                              },
                              icon: {
                                opacity:1,
                                scale: 1.2,
                              }
                          });

                map.zoomIn();


                /*end map zoom creation */
            }

              mMarker = new HWMapJsSDK.HWMarker({
                map: map,
                position: myLatLng,
                zIndex: 10,
                label: {
                    text: title,
                    offsetY: -30,
                    fontSize: '20px'
                },
                icon: {
                    opacity:1,
                    scale: 1.2,
                }
            });

            
        }
        function getLocation()
        {

        }
    </script> 

     
    @elseif(env('APP_ACTIVE_MAP')=='google_map')
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&callback=initMap&libraries=places&v=weekly"
      defer
    ></script>
 <script type="text/javascript">
    
      function initMap() {

      var lat_long = <?php echo json_encode($data->location);?>;
      var locations = lat_long;
  const myLatLng = {
    lat: (locations[1] ? locations[1] : -25.363),
    lng: (locations[0] ? locations[0] : 131.044)
  };
    
      

  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 8,
    center: myLatLng
  });
    var input = document.getElementById('address');

 map.controls[google.maps.ControlPosition.TOP_RIGHT].push();

var autocomplete = new google.maps.places.Autocomplete(input);
autocomplete.bindTo('bounds', map);
autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);
            var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29),
          title: "Hello World!"
        });
        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindow.open(map, marker);
        });
  new google.maps.Marker({
    position: myLatLng,
    map,
    title: "Hello World!"
  });
}

function getLocation()
 {

  var address = $('#address').val();
  $('#address').addClass('error is-invalid');
  $('.invalid-feedback.address').text('Geo Location Not Set Properly .Please Select.');
  $('#address').focus();
//   alert(address);
  var geocoder = new google.maps.Geocoder();

  geocoder.geocode( { 'address': address}, function(results, status)
  {


    if (status == google.maps.GeocoderStatus.OK)
    {
      console.log(results[0],"afdcfsa");
      console.log(results[0].address_components);
      const countryObj=results[0].address_components.filter(val=>{
            if(val.types.includes('country')){
                return true
            }
            return false;
      });
     console.log(countryObj,'the country obj');


     var country_name = countryObj[0].long_name;
     console.log(country_name,'name');
      var latitude = results[0].geometry.location.lat();

      var longitude = results[0].geometry.location.lng();
      $('#address').removeClass('error is-invalid');
      $('.invalid-feedback.address').text('');
     $('#lat').val(latitude);
     $('#lng').val(longitude);
     /*$('#countryTo').val(country_name);*/
     /*$('#country_name').val(country_name);*/
    console.log(latitude, longitude);
    }
  });

}
</script>
@endif