<section class="step1 listPropertySteps col-md-12">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                               
                <h5>Title & Description</h5>
                <p>Give your workspace a name so that people can find it.</p>
                <div  name="property_step_1" id="property_step_1">
                    <form method="post" name="property_step_1" id="property_step_1">
                        <div class="loc-form">
                            <input class="form-control" type="text" name="title" placeholder="“The Ninja Workspace…”" value="{{$data->title}}">
                            <span class="invalid-feedback title"></span>

                            <textarea class="form-control mt-4 mb-3" rows="6" placeholder="“The workpsace is located in NY and i 5 minutes away from the nearest metro station…”" name="description">{{$data->description}}</textarea>
                            <span class="invalid-feedback description"></span>
                        </div>
                    </form>
                    <div class="pt-3 pb-3">
                        <hr>
                    </div>
                    <div class="clearfix"></div>
        
                    <div class="row float-right">
                        <div class="col-md-6 text-right">
                            <button class="red-btn rounded float-right submit_step_1" type="button">
                                Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>