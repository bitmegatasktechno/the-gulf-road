<section class="step6 col-md-12 listPropertySteps">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h5>Add minimum 5 Pictures of your Coworkspace</h5>
                <p>This helps the user better know about the Coworkspace and the chances of contacting you increase.</p>
                
                <form class="boxed second" name="rent_sale_property_step_6" id="rent_sale_property_step_6" enctype="multipart/form-data">
                    
                    <div id="pictureresult" class="row">
                        @foreach($data->files as $row)
                            <div class="upload col-md-3 mb-3">
                                <label for="files1">
                                    <input type="hidden" name="exist_file[]" value="{{$row}}">
                                    <img id="files1_src" class="already_exit_pic" data-name="{{$row}}" src="{{url('uploads/coworkspce/'.$row)}}" style="height:120px;width:120px;">
                                </label>
                                <span class="remove removeDiv" style="cursor:pointer;position: absolute;top: -16px;right: 8px;font-size: 22px;"><i class="fa fa-times" aria-hidden="true"></i></span>
                            </div>
                        @endforeach
                    </div>
                    
                    <br>
                    <button type="button" id="add_more" class="red-btn rounded" value="Add More Files">Add More</button>
                </form>

                <div class="pt-3 pb-3">
                    <hr>
                </div>
                <div class="clearfix"></div>
    
                <div class="row float-right">
                    <div class="col-md-6 text-right">
                        <button class="red-btn rounded float-right submit_step_6" type="button">
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>