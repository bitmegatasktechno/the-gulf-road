<section class="step3 col-md-12 listPropertySteps">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h5 >Amenities</h5>
                <p>List out the amenities that your workspace provides.</p>
                <div id="accordion">
                    <div class="card">
                        <div class="card-header" id="headingOne">
                            <h5 style="cursor: pointer;" class="mb-0" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Food
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <div class="row">
                                    @php
                                        $food_id = \App\Models\AmenityCategory::where('name','food')->value('_id');
                                    @endphp

                                    @php
                                    $fffffffood = getAllSpecialAmenities()->where('category_id',$food_id);
                                     
                                        $food_amenties = $data->food_amenties;
                                        if(!is_array($food_amenties)){
                                            $food_amenties = [];
                                        }
                                    @endphp
                                    @foreach(getAllSpecialAmenities()->where('category_id',$food_id) as $row)
                                        <div class="col-md-4 specialAmenitiesBlock @if(in_array($row->_id, $food_amenties)) active @endif" data-type="food" data-id="{{$row->_id}}">
                                            <a href="javascript:;">
                                                <div class="blog-card mb-4">
                                                    <span>
                                                        <img class="special_amenity_check @if(in_array($row->_id, $food_amenties)) active @endif" src="{{asset('img/ic_red_check.png')}}">
                                                    </span>

                                                    <img class="img-fluid d-block mx-auto" src="{{url('/uploads/specialamenities/'.$row->logo)}}" style="border:none">
                                                    <strong style="text-align: center;display: inherit;margin-top: 10px;">{{\Str::limit($row->name, 15)}}</strong>
	                                                        <p style="font-size: 11px !important;">{{\Str::limit($row->description, 30)}}</p>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">
                            <h5 style="cursor: pointer;" class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Facilities
                            </h5>
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-body">
                                <div class="row">
                                    @php
                                        $facility_id = \App\Models\AmenityCategory::where('name','facilities')->value('_id');
                                    @endphp
                                    @php
                                        $facilities_amenties = $data->facilities_amenties;
                                        if(!is_array($facilities_amenties)){
                                            $facilities_amenties = [];
                                        }
                                    @endphp

                                    @foreach(getAllSpecialAmenities()->where('category_id',$facility_id) as $row)
                                        <div class="col-md-4 specialAmenitiesBlock @if(in_array($row->_id, $facilities_amenties)) active @endif" data-type="facilities" data-id="{{$row->_id}}">
                                            <a href="javascript:;">
                                                <div class="blog-card mb-4">
                                                    <span>
                                                        <img class="special_amenity_check @if(in_array($row->_id, $facilities_amenties)) active @endif" src="{{asset('img/ic_red_check.png')}}">
                                                    </span>

                                                   <img class="img-fluid d-block mx-auto" src="{{url('/uploads/specialamenities/'.$row->logo)}}" style="border:none">
                                                    <strong style="text-align: center;display: inherit;margin-top: 10px;">{{\Str::limit($row->name, 15)}}</strong>
	                                                        <p style="font-size: 11px !important;">{{\Str::limit($row->description, 30)}}</p>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">
                            <h5 style="cursor: pointer;" class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Equipments
                           
                            </h5>
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                            <div class="card-body">
                                <div class="row">
                                    @php
                                        $equipments_id = \App\Models\AmenityCategory::where('name','equipments')->value('_id');
                                    @endphp

                                    @php
                                        $equipments_amenties = $data->equipments_amenties;
                                        if(!is_array($equipments_amenties)){
                                            $equipments_amenties = [];
                                        }
                                    @endphp

                                    @foreach(getAllSpecialAmenities()->where('category_id',$equipments_id) as $row)
                                        <div class="col-md-4 specialAmenitiesBlock @if(in_array($row->_id, $equipments_amenties)) active @endif" data-type="equipments" data-id="{{$row->_id}}">
                                            <a href="javascript:;">
                                                <div class="blog-card mb-4">
                                                    <span>
                                                        <img class="special_amenity_check @if(in_array($row->_id, $equipments_amenties)) active @endif" src="{{asset('img/ic_red_check.png')}}">
                                                    </span>

                                                   <img class="img-fluid d-block mx-auto" src="{{url('/uploads/specialamenities/'.$row->logo)}}" style="border:none">
                                                    <strong style="text-align: center;display: inherit;margin-top: 10px;">{{\Str::limit($row->name, 15)}}</strong>
	                                                        <p style="font-size: 11px !important;">{{\Str::limit($row->description, 30)}}</p>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFour">
                            <h5 style="cursor: pointer;"  class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Seating
                            
                            </h5>
                        </div>
                        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                            <div class="card-body">
                                <div class="row">
                                    @php
                                        $seating_id = \App\Models\AmenityCategory::where('name','seating')->value('_id');
                                    @endphp
                                    @php
                                        $seating_amenties = $data->seating_amenties;
                                        if(!is_array($seating_amenties)){
                                            $seating_amenties = [];
                                        }
                                    @endphp
                                    @foreach(getAllSpecialAmenities()->where('category_id',$seating_id) as $row)
                                        <div class="col-md-4 specialAmenitiesBlock @if(in_array($row->_id, $seating_amenties)) active @endif" data-type="seating" data-id="{{$row->_id}}">
                                            <a href="javascript:;">
                                                <div class="blog-card mb-4">
                                                    <span>
                                                        <img class="special_amenity_check @if(in_array($row->_id, $seating_amenties)) active @endif" src="{{asset('img/ic_red_check.png')}}">
                                                    </span>

                                                   <img class="img-fluid d-block mx-auto" src="{{url('/uploads/specialamenities/'.$row->logo)}}" style="border:none">
                                                    <strong style="text-align: center;display: inherit;margin-top: 10px;">{{\Str::limit($row->name, 15)}}</strong>
	                                                        <p style="font-size: 11px !important;">{{\Str::limit($row->description, 30)}}</p>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingFive">
                            <h5 style="cursor: pointer;" class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">Accessibility
                            
                            </h5>
                        </div>
                        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                            <div class="card-body">
                                <div class="row">
                                    @php
                                        $accessibility_id = \App\Models\AmenityCategory::where('name','accessibility')->value('_id');
                                    @endphp

                                    @php
                                        $accessibility_amenties = $data->accessibility_amenties;
                                        if(!is_array($accessibility_amenties)){
                                            $accessibility_amenties = [];
                                        }
                                    @endphp
                                    @foreach(getAllSpecialAmenities()->where('category_id',$accessibility_id) as $row)
                                        <div class="col-md-4 specialAmenitiesBlock @if(in_array($row->_id, $accessibility_amenties)) active @endif" data-type="accessibility" data-id="{{$row->_id}}">
                                            <a href="javascript:;">
                                                <div class="blog-card mb-4">
                                                    <span>
                                                        <img class="special_amenity_check @if(in_array($row->_id, $accessibility_amenties)) active @endif" src="{{asset('img/ic_red_check.png')}}">
                                                    </span>

                                                   <img class="img-fluid d-block mx-auto" src="{{url('/uploads/specialamenities/'.$row->logo)}}" style="border:none">
                                                    <strong style="text-align: center;display: inherit;margin-top: 10px;">{{\Str::limit($row->name, 15)}}</strong>
	                                                        <p style="font-size: 11px !important;">{{\Str::limit($row->description, 30)}}</p>
                                                </div>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="pt-3 pb-3">
                    <hr>
                </div>
                <div class="clearfix"></div>
    
                <div class="row float-right">
                    <div class="col-md-6 text-right">
                        <button class="red-btn rounded float-right submit_step_3" type="button">
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>