<section class="step4 col-md-12 listPropertySteps">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h5>Operating hours & Working days</h5>
                <p>Set the operating hours & working days of your coworkspace.</p>
                
                <form class="p-4 rent_sale_property_step_4" id="rent_sale_property_step_4">
                    <div class="row dp-flex align-items-center">
                        <div class="col-md-6">
                            <h3>Monday </h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <input class="visibly-hidden operating_radio closed_input" id="monday_closed_radio" type="radio" name="monday" value="closed" data-id="operating_monday_div" @if(@$data->availability['monday']=='closed') checked @endif>
                                <label class="radio-label" for="monday_closed_radio">Closed</label>

                                <input class="visibly-hidden operating_radio open_input" id="monday_open_radio" type="radio" name="monday" value="open" data-id="operating_monday_div" @if(@$data->availability['monday']=='open') checked @endif>
                                <label class="radio-label" for="monday_open_radio">Open</label>

                            </div>
                            <span class="invalid-feedback monday"></span>
                        </div>

                        <div class="row">
                            <div class="col-md-5 mt-2 mb-3 operating_monday_div" style="display: @if(@$data->availability['monday']=='closed') none @endif">
                            
                                <input class="form-control timepicker" type="text" name="monday_start_time" id="monday_start_time" value="{{$data->timings['monday_start_time']}}">

                                <span class="invalid-feedback monday_start_time"></span>
                            </div>
                            <div class="col-md-5 mt-2 mb-3 operating_monday_div" style="display: @if(@$data->availability['monday']=='closed') none @endif">
                                
                                <input class="form-control timepicker" type="text" name="monday_end_time" id="monday_end_time" value="{{$data->timings['monday_end_time']}}">
                                
                                <span class="invalid-feedback monday_end_time"></span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row dp-flex align-items-center">
                        <div class="col-md-6">
                            <h3>Tuesday</h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <input class="visibly-hidden operating_radio" id="tuesday_closed_radio" type="radio" name="tuesday" value="closed" data-id="operating_tuesday_div" @if(@$data->availability['tuesday']=='closed') checked @endif>
                                <label class="radio-label" for="tuesday_closed_radio">Closed</label>

                                <input class="visibly-hidden operating_radio" id="tuesday_open_radio" type="radio" name="tuesday" value="open" data-id="operating_tuesday_div" @if(@$data->availability['tuesday']=='open') checked @endif>
                                <label class="radio-label" for="tuesday_open_radio">Open</label>
                            </div><span class="invalid-feedback tuesday"></span>
                        </div>
                        <div class="row">
                            <div class="col-md-5 mt-2 mb-3 operating_tuesday_div" style="display: @if(@$data->availability['tuesday']=='closed') none @endif">
                            
                                <input class="form-control timepicker" type="text" name="tuesday_start_time" id="tuesday_start_time" value="{{$data->timings['tuesday_start_time']}}">
                                
                                <span class="invalid-feedback tuesday_start_time"></span>
                            </div>
                            <div class="col-md-5 mt-2 mb-3 operating_tuesday_div" style="display: @if(@$data->availability['tuesday']=='closed') none @endif">
                                
                                <input class="form-control timepicker" type="text" name="tuesday_end_time" id="tuesday_end_time" value="{{$data->timings['tuesday_end_time']}}">
                                
                                <span class="invalid-feedback tuesday_end_time"></span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row dp-flex align-items-center">
                        <div class="col-md-6">
                            <h3>Wednesday</h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <input class="visibly-hidden operating_radio" id="wednesday_closed_radio" type="radio" name="wednesday" value="closed" data-id="operating_wednesday_div" @if(@$data->availability['wednesday']=='closed') checked @endif>
                                <label class="radio-label" for="wednesday_closed_radio">Closed</label>

                                <input class="visibly-hidden operating_radio" id="wednesday_open_radio" type="radio" name="wednesday" value="open" data-id="operating_wednesday_div" @if(@$data->availability['wednesday']=='open') checked @endif>
                                <label class="radio-label" for="wednesday_open_radio">Open</label>
                            </div><span class="invalid-feedback wednesday"></span>
                        </div>
                        <div class="row">
                            <div class="col-md-5 mt-2 mb-3 operating_wednesday_div" style="display: @if(@$data->availability['wednesday']=='closed') none @endif">
                                
                                <input class="form-control timepicker" type="text" name="wednesday_start_time" id="wednesday_start_time" value="{{$data->timings['wednesday_start_time']}}">
                                
                                <span class="invalid-feedback wednesday_start_time"></span>
                            </div>
                            <div class="col-md-5 mt-2 mb-3 operating_wednesday_div" style="display: @if(@$data->availability['wednesday']=='closed') none @endif">
                                
                                <input class="form-control timepicker" type="text" name="wednesday_end_time" id="wednesday_end_time" value="{{$data->timings['wednesday_end_time']}}">
                                
                                <span class="invalid-feedback wednesday_end_time"></span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row dp-flex align-items-center">
                        <div class="col-md-6">
                            <h3>Thursday</h3>
                        </div>

                        <div class="col-md-6 text-right">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <input class="visibly-hidden operating_radio" id="thursday_closed_radio" type="radio" name="thursday" value="closed" data-id="thursday_operating_div" @if(@$data->availability['thursday']=='closed') checked @endif>
                                <label class="radio-label" for="thursday_closed_radio">Closed</label>

                                <input class="visibly-hidden operating_radio" id="thursday_open_radio" type="radio" name="thursday" value="open" data-id="thursday_operating_div" @if(@$data->availability['thursday']=='open') checked @endif>
                                <label class="radio-label" for="thursday_open_radio">Open</label>
                            </div><span class="invalid-feedback thursday"></span>
                        </div>
                        <div class="row">
                            <div class="col-md-5 mt-2 mb-3 thursday_operating_div" style="display: @if(@$data->availability['thursday']=='closed') none @endif">
                                
                                <input class="form-control timepicker" type="text" name="thursday_start_time" id="thursday_start_time" value="{{$data->timings['thursday_start_time']}}">
                                
                                <span class="invalid-feedback thursday_start_time"></span>
                            </div>
                            <div class="col-md-5 mt-2 mb-3 thursday_operating_div" style="display: @if(@$data->availability['thursday']=='closed') none @endif">
                                
                                <input class="form-control timepicker" type="text" name="thursday_end_time" id="thursday_end_time" value="{{$data->timings['thursday_end_time']}}">
                                
                                <span class="invalid-feedback thursday_end_time"></span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row dp-flex align-items-center">
                        <div class="col-md-6">
                            <h3>Friday</h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <input class="visibly-hidden operating_radio" id="friday_closed_radio" type="radio" name="friday" value="closed" data-id="friday_operating_div" @if(@$data->availability['friday']=='closed') checked @endif>
                                <label class="radio-label" for="friday_closed_radio">Closed</label>

                                <input class="visibly-hidden operating_radio" id="friday_open_radio" type="radio" name="friday" value="open" data-id="friday_operating_div" @if(@$data->availability['friday']=='open') checked @endif>
                                <label class="radio-label" for="friday_open_radio">Open</label>
                            </div><span class="invalid-feedback friday"></span>
                        </div>
                        <div class="row">
                            <div class="col-md-5 mt-2 mb-3 friday_operating_div" style="display: @if(@$data->availability['friday']=='closed') none @endif">
                                
                                <input class="form-control timepicker" type="text" name="friday_start_time" id="friday_start_time" value="{{$data->timings['friday_start_time']}}">
                                
                                <span class="invalid-feedback friday_start_time"></span>
                            </div>
                            <div class="col-md-5 mt-2 mb-3 friday_operating_div" style="display: @if(@$data->availability['friday']=='closed') none @endif">
                                
                                <input class="form-control timepicker" type="text" name="friday_end_time" id="friday_end_time" value="{{$data->timings['friday_end_time']}}">
                                
                                <span class="invalid-feedback friday_end_time"></span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row dp-flex align-items-center">
                        <div class="col-md-6">
                            <h3>Saturday</h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <input class="visibly-hidden operating_radio" id="saturday_closed_radio" type="radio" name="saturday" value="closed" data-id="saturday_operating_div" @if(@$data->availability['saturday']=='closed') checked @endif>
                                <label class="radio-label" for="saturday_closed_radio">Closed</label>

                                <input class="visibly-hidden operating_radio" id="saturday_open_radio" type="radio" name="saturday" value="open" data-id="saturday_operating_div" @if(@$data->availability['saturday']=='open') checked @endif>
                                <label class="radio-label" for="saturday_open_radio">Open</label>
                            </div><span class="invalid-feedback saturday"></span>
                        </div>
                        <div class="row">
                            <div class="col-md-5 mt-2 mb-3 saturday_operating_div" style="display: @if(@$data->availability['saturday']=='closed') none @endif">
                                
                                <input class="form-control timepicker" type="text" name="saturday_start_time" id="saturday_start_time" value="{{$data->timings['saturday_start_time']}}">
                                
                                <span class="invalid-feedback saturday_start_time"></span>
                            </div>
                            <div class="col-md-5 mt-2 mb-3 saturday_operating_div" style="display: @if(@$data->availability['saturday']=='closed') none @endif">
                                
                                <input class="form-control timepicker" type="text" name="saturday_end_time" id="saturday_end_time" value="{{$data->timings['saturday_end_time']}}">
                                
                                <span class="invalid-feedback saturday_end_time"></span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row dp-flex align-items-center">
                        <div class="col-md-6">
                            <h3>Sunday</h3>
                        </div>
                        <div class="col-md-6 text-right">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <input class="visibly-hidden operating_radio" id="sunday_closed_radio" type="radio" name="sunday" value="closed" data-id="sunday_operating_div" @if(@$data->availability['sunday']=='closed') checked @endif>
                                <label class="radio-label" for="sunday_closed_radio">Closed</label>

                                <input class="visibly-hidden operating_radio" id="sunday_open_radio" type="radio" name="sunday" value="open" data-id="sunday_operating_div" @if(@$data->availability['sunday']=='open') checked @endif>
                                <label class="radio-label" for="sunday_open_radio">Open</label>
                            </div><span class="invalid-feedback sunday"></span>
                        </div>
                        <div class="row">
                            <div class="col-md-5 mt-2 mb-3 sunday_operating_div" style="display: @if(@$data->availability['sunday']=='closed') none @endif">
                                
                                <input class="form-control timepicker" type="text" name="sunday_start_time" id="sunday_start_time" value="{{$data->timings['sunday_start_time']}}">
                                
                                <span class="invalid-feedback sunday_start_time"></span>
                            </div>
                            <div class="col-md-5 mt-2 mb-3 sunday_operating_div" style="display: @if(@$data->availability['sunday']=='closed') none @endif">
                                
                                <input class="form-control timepicker" type="text" name="sunday_end_time" id="sunday_end_time" value="{{$data->timings['sunday_end_time']}}">
                                
                                <span class="invalid-feedback sunday_end_time"></span>
                            </div>
                        </div>
                    </div>
                    <div class="pt-3 pb-3">
                        <hr>
                    </div>
                    <div class="clearfix"></div>
        
                    <div class="row float-right">
                        <div class="col-md-6 text-right">
                            <button class="red-btn rounded float-right submit_step_4" type="button">
                                Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>