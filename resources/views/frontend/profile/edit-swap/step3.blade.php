<section class="listPropertySteps col-md-12">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="mb-3">Amenities</h5>
                <p class="mb-5">Tell us about the amenities available at your Place. You can list additional items if they are not listed below.</p>
                
                @php
                    $amenities1 = $data->amenties;
                     
                    $amenities = [];
                    if(!empty($amenities1))
                    {
                        foreach($amenities1 as $v)
                        {
                              
                              if(isset($v['id']) && $v['id'] !=='')
                              {
                                $amenities[] = $v['id'];
                              }
                        }
                    }
                     
                    if(!is_array($amenities)){
                        $amenities = [];
                    }
                     
                @endphp
                <div class="row">
                    @foreach(getAllAmenities() as $row)
                        @if(in_array($row->_id, $amenities))
                            <div class="col-md-3">
                                <div class="facility-block amenitiesBlock active" data-id="{{$row->_id}}">
                                    <img src="{{url('uploads/amenities/'.$row->logo)}}">
                                    <p class="mb-0 mt-2">{{ucfirst($row->name)}}</p>
                                </div>
                            </div>
                        @else
                            <div class="col-md-3">
                                <div class="facility-block amenitiesBlock" data-id="{{$row->_id}}">
                                    <img src="{{url('uploads/amenities/'.$row->logo)}}">
                                    <p class="mb-0 mt-2">{{ucfirst($row->name)}}</p>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
                
                <div class="pt-3 pb-3">
                    <hr>
                </div>
                <div class="clearfix"></div>
    
                <div class="row float-right">
                    <div class="col-md-6 text-right">
                        <button class="red-btn rounded float-right submit_step_3" type="button">
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>