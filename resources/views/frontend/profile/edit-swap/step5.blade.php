<section class="step5 col-md-12 listPropertySteps">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h5>Add atleast 5 Pictures of your Swap Property</h5>
                <p>This helps the user better know about the Swap and the chances of contacting you increase.</p>
                
                <form class="boxed second" name="property_step_5" id="property_step_5" enctype="multipart/form-data">
                    
                    <div id="pictureresult" class="row">
                        @foreach($data->files as $row)
                            <div class="upload col-md-4 mb-4">
                                <label for="files1" class="p-0">
                                    <input type="hidden" name="exist_file[]" value="{{$row}}">
                                    <img id="files1_src" class="already_exit_pic" data-name="{{$row}}" src="{{url('uploads/swap/'.$row)}}" style="height:120px;width:100%;">
                                </label>
                                <span class="remove removeDiv" style="cursor:pointer;position: absolute;top: -16px;right: 72px;font-size: 22px;"><i class="fa fa-times" aria-hidden="true"></i></span>
                            </div>
                        @endforeach
                    </div>
                    
                    <br>
                    <button type="button" id="add_more" class="red-btn rounded" value="Add More Files">Add More</button>
                </form>

                <div class="pt-3 pb-3">
                    <hr>
                </div>
                <div class="clearfix"></div>
    
                <div class="row float-right">
                    <div class="col-md-6 text-right">
                        <button class="red-btn rounded float-right submit_step_5" type="button">
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>