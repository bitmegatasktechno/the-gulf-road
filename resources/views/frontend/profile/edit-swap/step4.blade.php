<section class="listPropertySteps col-md-12">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="mb-3">Description & Availability</h3>
                <p class="mb-5">Tell us more about your place and its surroundings. Set the availablity of your house for the users to know.</p>
                
                <form method="post" name="property_step_4" id="property_step_4" class="property_step_4 loc-form">
                    <div class="row">
                        <div class="col-md-12">
                            <h5>Describe your Home</h5>
                            <div class="loc-form">
                                <textarea class="form-control mt-3 mb-1" rows="6" placeholder="“My Home is Located Near the Lake and has a beautiful view of the whole town.”" name="property_description">{{$data->property_description}}</textarea>
                                <span class="invalid-feedback property_description"></span>
                            </div>
                        </div>
                    </div>

                     <div class="row mt-4">
                        <div class="col-md-12">
                            <h5>Describe your Neighbourhood</h5>
                            <div class="loc-form">
                                <textarea class="form-control mt-3 mb-1" rows="6" placeholder="“My Home is Located Near the Lake and has a beautiful view of the whole town.”" name="neighbourhood_description">{{$data->neighbourhood_description}}</textarea>
                                <span class="invalid-feedback neighbourhood_description"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-3">
                        <div class="col-md-12">
                           <h5 class="mt-4">Set Availability</h5>
                            <p class="mb-4">Add your period of availability of your House. Your house will be open to swap during this period.</p>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="full-width" id="available_dates">
                                     <input type="hidden" name="availability" id="my_hidden_input" value="{{$data->availability ? implode(',', $data->availability) : ''}}">
                                </div>

                                <span class="invalid-feedback availability"></span>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="pt-3 pb-3">
                        <hr>
                </div>
                <div class="clearfix"></div>
    
                <div class="row float-right">
                    <div class="col-md-6 text-right">
                        <button class="red-btn rounded float-right submit_step_4" type="button">
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>