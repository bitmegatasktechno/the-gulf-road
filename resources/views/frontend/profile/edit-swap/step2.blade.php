<section class="listPropertySteps col-md-12">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                
                <h3 class="mb-3">Size & Space</h3>
			   	<p class="mb-5">Tell us about the Size of your House & How much people can this place accomodate.</p>     
                <div class="loc-form">
                    <form method="post" name="property_step_2" id="property_step_2">
                      
	                    <h5>Surface Area & Space</h5>
	                    <p>What is the total Surface area of your House?</p>
	                    <div class="row">
	                        <div class="col-md-8">
	                            <div class="form-group">
	                                <input class="form-control" type="text" name="surface_area" placeholder="Enter Surface Area" value="{{$data->surface_area}}">
	                                <span class="invalid-feedback surface_area"></span>
	                            </div>
	                        </div>
	                        <div class="col-md-3">
	                            <div class="form-group">
	                                <select class="form-control rounded" name="surface_area_unit">
	                                    <option value="sqft" @if($data->surface_area_unit=='sqft') selected @endif>Sq Ft</option>
	                                    <option value="sqmtr"  @if($data->surface_area_unit=='sqmtr') selected @endif>Sq Mtr</option>
	                                </select>
	                                <span class="invalid-feedback surface_area_unit"></span>
	                            </div>
	                        </div>
	                    </div>
	                    
	                
	                    <div class="row mt-4">
	                        <div class="col-md-7">
	                            <p class="mb-0"><img class="mr-3" src="{{asset('img/rooms.png')}}"> No of Bedrooms</p>
	                            
	                        </div>
	                        <div class="col-md-5">
	                            <div class="number mb-2 pt-0">
	                                <span class="minus"><small>-</small></span>
	                                <input class="dp-inline-block no-border" type="text" readonly="" name="no_of_bedrooms" value="{{$data->no_of_bedrooms}}"  />
	                                <span class="plus"><small>+</small></span>
	                            </div>
	                        </div>
	                        <div class="col-md-12">
	                            <span class="invalid-feedback no_of_bedrooms"></span>
	                        </div>
	                    </div>
	                    <div class="row mt-4">
	                        <div class="col-md-7">
	                            <p class="mb-0"><img class="mr-3" src="{{asset('img/bathrooms.png')}}"> No of Bathrooms</p>
	                        </div>
	                        <div class="col-md-5">
	                            <div class="number mb-2 pt-0">
	                                <span class="minus"><small>-</small></span>
	                                <input class="dp-inline-block no-border" type="text" readonly="" name="no_of_bathrooms" value="{{$data->no_of_bathrooms}}" />
	                                <span class="plus"><small>+</small></span>
	                            </div>
	                        </div>
	                        <div class="col-md-12">
	                            <span class="invalid-feedback no_of_bathrooms"></span>
	                        </div>
	                    </div>

	                    <h5 class="mt-4">How many people can your House Accomodate?</h5>
	                    <p class="mb-4">Tell us about the Bed Space Available in your House.</p>
	                    
	                    <div class="row mt-3">
	                        <div class="col-md-7">
	                           <p class="mb-0"><img class="mr-3" src="{{asset('img/single-bed.png')}}"> Single Beds</p>
	                        </div>
	                        <div class="col-md-5">
	                            <div class="number mb-2 pt-0">
	                                <span class="minus bed_minus"><small>-</small></span>
	                                <input class="dp-inline-block no-border" type="text" readonly="" name="no_of_single_beds" value="{{$data->no_of_single_beds}}"  />
	                                <span class="plus bed_plus"><small>+</small></span>
	                            </div>
	                        </div>
	                        <div class="col-md-12">
	                            <span class="invalid-feedback no_of_single_beds"></span>
	                        </div>
	                    </div>

	                    <div class="row mt-3">
	                        <div class="col-md-7">
	                            <p class="mb-0"><img class="mr-3" src="{{asset('img/double-bed.png')}}"> Double Beds</p>
	                        </div>
	                        <div class="col-md-5">
	                            <div class="number mb-2 pt-0">
	                                <span class="minus bed_minus"><small>-</small></span>
	                                <input class="dp-inline-block no-border" type="text" readonly="" name="no_of_double_beds" value="{{$data->no_of_double_beds}}"  />
	                                <span class="plus bed_plus"><small>+</small></span>
	                            </div>
	                        </div>
	                        <div class="col-md-12">
	                            <span class="invalid-feedback no_of_double_beds"></span>
	                        </div>
	                    </div>
	                    <div class="row mt-3">
	                        <div class="col-md-7">
	                            <p class="mb-0"><img class="mr-3" src="{{asset('img/children-bed.png')}}"> Children Beds</p>
	                        </div>
	                        <div class="col-md-5">
	                            <div class="number mb-2 pt-0">
	                                <span class="minus bed_minus"><small>-</small></span>
	                                <input class="dp-inline-block no-border" type="text" readonly="" name="no_of_children_beds" value="{{$data->no_of_children_beds}}"  />
	                                <span class="plus bed_plus"><small>+</small></span>
	                            </div>
	                        </div>
	                        <div class="col-md-12">
	                            <span class="invalid-feedback no_of_children_beds"></span>
	                        </div>
	                    </div>
	                    <div class="row mt-3">
	                        <div class="col-md-7">
	                            <p style="color: #000000">How Many More people can your House accomodate?</p>
	                        </div>
	                        <div class="col-md-5">
	                            <div class="number mb-2 pt-0">
	                                <span class="minus bed_minus"><small>-</small></span>
	                                <input class="dp-inline-block no-border" type="text" readonly="" name="no_of_extra_beds" value="{{$data->no_of_extra_beds}}"  />
	                                <span class="plus bed_plus"><small>+</small></span>
	                            </div>
	                        </div>
	                        <div class="col-md-12">
	                            <span class="invalid-feedback no_of_extra_beds"></span>
	                        </div>
	                    </div>

	                    <div class="row mt-3">
	                        <div class="col-md-12" style="display: inline-flex;">
	                            <p>Your House can Accommodate </p> 
	                            <p class="red-like">&nbsp;<span class="total_beds">{{$data->total_accommodation ?? 0}} </span> People.</p>
	                        </div>
	                        <input type="hidden" name="total_accommodation" id="total_accommodation" value="{{$data->total_accommodation}}">
	                    </div>
                    </form>
                    <div class="pt-3 pb-3">
                        <hr>
                    </div>
                    <div class="clearfix"></div>
        
                    <div class="row float-right">
                        <div class="col-md-6 text-right">
                            <button class="red-btn rounded float-right submit_step_2" type="button">
                                Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>