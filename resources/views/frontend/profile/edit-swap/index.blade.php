@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
@extends('frontend.layouts.home')
@section('title','Edit Swap Property')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" />
@endsection

@section('content')
@include('frontend.layouts.default-header')
<section class="inner-body body-light-grey mt-70">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="list-inline breadcrum">
                    <li><a href="{{url(app()->getLocale().'/home/'.$link)}}">Home </a></li>
                    <span> / </span>
                    <li><a href="{{url(app()->getLocale().'/myswap/'.$link)}}"> My Swap Properties</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <!-- Navigation Block -->
                <div class="inner-white-wrapper left-navigation mb-3">
@include('frontend.profile.user_profile_sidebar')
                </div>
            </div>
            <input type="hidden" name="property_id" value="{{$data->_id}}">
            <!-- Right Wrapper -->
            @include('frontend.profile.edit-swap.main')
        </div>
    </div>
</section>
@include('frontend.layouts.footer')
@endsection

@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    let propertySteps = false;
    const property_id = $('input[name="property_id"]').val();
    let total_beds = '{{$data->total_accommodation}}';

function file_validation(id,max_size)
   {
       var fuData = document.getElementById(id);
       var FileUploadPath = fuData.value;
       
   
       if (FileUploadPath == ''){
           alert("Please upload Attachment");
       } 
       else {
           var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
           if (Extension == "jpg" || Extension == "jpeg" || Extension == "png" || Extension == "webp" ) {
   
                   if (fuData.files && fuData.files[0]) {
                       var size = fuData.files[0].size;
                       
                       if(size > max_size){   //1000000 = 1 mb
                           
                           Swal.fire({
                                title: 'Warning!',
                                text: "Maximum file size "+max_size/1000000+" MB",
                                icon: 'info',
                                confirmButtonText: 'Ok'
                            });
                            
                           $("#"+id).val('');
                           return false;
                            
                       }
                   }
   
           } 
           else 
           {
                
               $("#"+id).val('');
                Swal.fire({
                                title: 'Warning!',
                                text: "document only allows file types of  jpg , png  , jpeg , webp ",
                                icon: 'info',
                                confirmButtonText: 'Ok'
                            });
                            
                           $("#"+id).val('');
                           return false;
           }
       }   
   } 

    const readURL = function(input) {

         var id = $(input).attr('id');
         var max_size = 2000000;
           file_validation(id,max_size);

           
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var id=input.id;
                $('#'+id+'_src').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).ready(function(){
        $("html, body").animate({ scrollTop: 0 }, "slow");

        $('#available_dates').datepicker({
            multidate: true,
            format: 'yyyy-mm-dd',
            startDate: new Date(),
            maxViewMode:0,
            endDate: '+6m',
        });

        $(".minus").click(function() {
            var $input = $(this).parent().find("input");
            if(parseInt($input.val())==0){
                return false;
            }

            var count = parseInt($input.val()) - 1;
            count = count < 1 ? 0 : count;
            if($(this).hasClass('bed_minus')){
                total_beds--;
                $('.total_beds').html(total_beds);
                $('#total_accommodation').val(total_beds);
            }
            $input.val(count);
            $input.change();
            return false;
        });

        $(".plus").click(function() {
            var $input = $(this).parent().find("input");
            if(parseInt($input.val())==10){
                return false;
            }

            if($(this).hasClass('bed_plus')){
                total_beds++;
                $('.total_beds').html(total_beds);
                $('#total_accommodation').val(total_beds);
            }
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });


        $(document).on('click','.amenitiesBlock', function(){
            $(this).toggleClass('active');
        });

        $(document).on('click','.removeDiv', function(){
            $(this).parent().remove();
        });

        // save first form of property
        $(document).on('click','.submit_step_1', function(){
            var ajax_url = WEBSITE_URL+"/updateProperty/swap/1";
            var $form = $('#property_step_1');
            $form.find('.is-invalid').removeClass('is-invalid');
            $form.find('.invalid-feedback').text('');

            var data = new FormData($('#property_step_1')[0]);
            data.append('property_id', property_id);

            $.ajax({
                url : ajax_url,
                type:'post',
                data : data,
                dataType : 'json',
                async:true,
           		processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.listPropertySteps');
                },
                complete:function(){
                   stopLoader('.listPropertySteps'); 
                },
                success : function(data){
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then(function(){
                            window.location.reload();
                        });

                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                },
                error : function(data){
                    stopLoader('.listPropertySteps');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                        $.each(err_response.errors, function(i, obj){
                            $form.find('span.invalid-feedback.'+i+'').text(obj).show();
                            $form.find('input[name="'+i+'"]').addClass('is-invalid');
                            $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
                            $form.find('select[name="'+i+'"]').addClass('is-invalid');
                        });
                    }
                }
            });
        });

        $(document).on('click','.submit_step_2', function(){
            var ajax_url = WEBSITE_URL+"/updateProperty/swap/2";
            var $form = $('#property_step_2');
            $form.find('.is-invalid').removeClass('is-invalid');
            $form.find('.invalid-feedback').text('');

            var data = new FormData($('#property_step_2')[0]);
            data.append('property_id', property_id);

            $.ajax({
                url : ajax_url,
                type:'post',
                data : data,
                async:true,
                dataType : 'json',
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.listPropertySteps');
                },
                complete:function(){
                   stopLoader('.listPropertySteps'); 
                },
                success : function(data){
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then(function(){
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        });
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                },
                error : function(data){
                    stopLoader('.listPropertySteps');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                        $.each(err_response.errors, function(i, obj){
                            $form.find('span.invalid-feedback.'+i+'').text(obj).show();
                            $form.find('input[name="'+i+'"]').addClass('is-invalid');
                            $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
                            $form.find('select[name="'+i+'"]').addClass('is-invalid');
                        });
                    }
                }
            });
        });

        $(document).on('click','.submit_step_3', function(){
            var ajax_url = WEBSITE_URL+"/updateProperty/swap/3";
            var data = new FormData();
            var count = 0;
            $('.amenitiesBlock').each(function(i){
                if($(this).is('.active')){
                    data.append('amenties[]', $(this).attr('data-id'));
                    count++;
                }
            });
            if(count==0){
                Swal.fire({
                    title: 'Warning!',
                    text: "Please select Atleast one from all Amenties Categories",
                    icon: 'info',
                    confirmButtonText: 'Ok'
                });
                return false;
            }
            data.append('property_id', property_id);

            $.ajax({
                url : ajax_url,
                type:'post',
                data : data,
                dataType : 'json',
                async:true,
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.listPropertySteps');
                },
                complete:function(){
                   stopLoader('.listPropertySteps'); 
                },
                success : function(data){
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then(function(){
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        });
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                },
                error : function(data){
                    stopLoader('.listPropertySteps');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                    }
                }
            });
        });

        $(document).on('click','.submit_step_4', function(){
            var ajax_url = WEBSITE_URL+"/updateProperty/swap/4";
            var $form = $('#property_step_4');
            $form.find('.is-invalid').removeClass('is-invalid');
            $form.find('.invalid-feedback').text('');

            var data = new FormData($form[0]);
            data.append('property_id', property_id);

            $.ajax({
                url : ajax_url,
                type:'post',
                data : data,
                async:true,
                dataType : 'json',
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.listPropertySteps');
                },
                complete:function(){
                   stopLoader('.listPropertySteps'); 
                },
                success : function(data){
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then(function(){
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        });
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                },
                error : function(data){
                    stopLoader('.listPropertySteps');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                        $.each(err_response.errors, function(i, obj){
                            $form.find('span.invalid-feedback.'+i+'').text(obj).show();
                            $form.find('input[name="'+i+'"]').addClass('is-invalid');
                            $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
                            $form.find('select[name="'+i+'"]').addClass('is-invalid');
                        });
                    }
                }
            });
        });

        $(document).on('click','#add_more', function(){
            if($('.upload').length>=15){
                Swal.fire({
                    title: 'Warning!',
                    text: "You can upload maximum 15 pictures.",
                    icon: 'info',
                    confirmButtonText: 'Ok'
                });
                return false;
            }
            var id= $('.upload').length+1;
            $("#pictureresult").append('\
                <div class="upload col-md-4 mb-4">\
                    <input type="file" id="files'+id+'" name="files[]" class="input-file" onchange="readURL(this)" >\
                    <label for="files'+id+'" class="p-0">\
                        <img id="files'+id+'_src" src="{{asset("img/addmedia-upload.png")}}" style="height:120px;width:100%;">\
                    </label>\
                    <span class="remove removeDiv" style="cursor:pointer;position: absolute;top: -16px;right: 72px;font-size: 22px;"><i class="fa fa-times" aria-hidden="true"></i></span>\
                    <span class="invalid-feedback files'+id+'"></span>\
                </div>');
        });


        $(document).on('click','.submit_step_5', function(){
            var ajax_url = WEBSITE_URL+"/updateProperty/swap/5";
            var $form = $('#property_step_5');
            $form.find('.is-invalid').removeClass('is-invalid');
            $form.find('.invalid-feedback').text('');

            var data = new FormData($form[0]);
            data.append('property_id', property_id);

            var error = false;
            $.each($('#pictureresult [type=file]'), function(index, file) {
                if($('input[type=file]')[index].files[0]==undefined){
                    var id = $('input[type=file]')[index].id;
                    $('.invalid-feedback.'+(id)).text('The file is required.').show();
                    error = true;
                }
            });

            if(error){
                return false;
            }
            if($('.upload').length<5){
                Swal.fire({
                    title: 'Warning!',
                    text: "Please upload minimum 5 pictures.",
                    icon: 'info',
                    confirmButtonText: 'Ok'
                });
                return false;
            }

            $.ajax({
                url : ajax_url,
                type:'post',
                data : data,
                enctype: 'multipart/form-data',
                dataType : 'json',
                async:true,
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.listPropertySteps');
                },
                complete:function(){
                   stopLoader('.listPropertySteps'); 
                },
                success : function(data){
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then(function(){
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                        });
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                },
                error : function(data){
                    stopLoader('.listPropertySteps');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                        $.each(err_response.errors, function(key, value){
                            if(key.indexOf('.') != -1) {
                                let keys = key.split('.');                                
                                $('input[name="'+keys[0]+'[]"]').eq(keys[1]).addClass('is-invalid').parent().find('.invalid-feedback').text(value).show();                         
                            }
                            else {
                                $('input[name="'+key+'[]"]').eq(0).addClass('is-invalid').parent().find('.invalid-feedback').text(value).show();
                            }
                        });
                    }
                }
            });
        });
    });

</script>
@endsection