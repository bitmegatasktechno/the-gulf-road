        <ul class="list-inline">
            <li class="@if($loc=Request::segment(2) == 'profile') active @endif">
                <i class="mr-3 fontIcon"><i class="fa fa-address-book" aria-hidden="true"></i></i><a href="{{url(app()->getLocale().'/profile/'.$link)}}">User Details</a>
            </li>
            <li class="@if($loc=Request::segment(2) == 'myreview') active @endif">
                <i class="mr-3 fontIcon"><i class="fa fa-star" aria-hidden="true"></i></i><a href="{{url(app()->getLocale().'/myreview/'.$link)}}">My Reviews</a>
            </li>
            <li class="@if($loc=Request::segment(2) == 'mysubscription') active @endif">
                <i class="mr-3 fontIcon"><i class="fa fa-diamond" aria-hidden="true"></i></i><a href="{{url(app()->getLocale().'/mysubscription/'.$link)}}">Subscription Plans</a>
            </li>
            <li class="@if($loc=Request::segment(2) == 'myfav') active @endif">
                <i class="mr-3 fontIcon"><i class="fa fa-flag" aria-hidden="true"></i></i><a href="{{url(app()->getLocale().'/myfav/'.$link)}}">Show Favourites</a>
            </li>
            <li class="@if($loc=Request::segment(2) == 'mylistings') active @endif">
                <i class="mr-3 fontIcon"><i class="fa fa-home" aria-hidden="true"></i></i><a href="{{url(app()->getLocale().'/mylistings/'.$link)}}">My Properties</a>
            </li>
            <li class="@if($loc=Request::segment(2) == 'mycoworkspaces') active @endif">
                <i class="mr-3 fontIcon"><i class="fa fa-university" aria-hidden="true"></i></i><a href="{{url(app()->getLocale().'/mycoworkspaces/'.$link)}}">My Coworkspace</a>
            </li>
            <li class="@if($loc=Request::segment(2) == 'myswap') active @endif">
                <i class="mr-3 fontIcon"><i class="fa fa-handshake-o" aria-hidden="true"></i></i><a href="{{url(app()->getLocale().'/myswap/'.$link)}}">My Swap</a>
            </li>
            <li class="@if($loc=Request::segment(2) == 'listing') active @endif">
                                <i class="mr-3 fontIcon"><i class="fa fa-book" aria-hidden="true"></i></i><a href="{{url(app()->getLocale().'/listing/'.$link)}}">My Blogs</a>
                            </li>
                           @if(\Auth::user()->registerVia=='web')
            <li class="@if($loc=Request::segment(2) == 'change-password') active @endif">
                <i class="mr-3"><i class="fa fa-key" aria-hidden="true"></i></i><a href="{{url(app()->getLocale().'/change-password/'.$link)}}">Change Password</a>
            </li>
            @endif
            <li class="@if($loc=Request::segment(2) == 'enquiries') active @endif">
                <i class="mr-3 fontIcon"><i class="fa fa-info-circle" aria-hidden="true"></i></i><a href="{{url(app()->getLocale().'/enquiries/'.$link)}}">Enquiries</a>
            </li>
            <li class="@if($loc=Request::segment(2) == 'notifications') active @endif">
	                            <i class="mr-3 fontIcon"><i class="fa fa-bell" aria-hidden="true"></i></i><a href="{{url(app()->getLocale().'/notifications/'.$link)}}">Notification</a>
	                        </li>
							<li class="@if($loc=Request::segment(2) == 'booking') active @endif">
	                            <i class="mr-3 fontIcon"><i class="fa fa-shopping-bag" aria-hidden="true"></i></i><a href="{{url(app()->getLocale().'/booking/'.$link)}}">My Bookings</a>
	                        </li>
                            <li class="@if($loc=Request::segment(2) == 'userbooking') active @endif">
	                            <i class="mr-3 fontIcon"><i class="fa fa-users" aria-hidden="true"></i></i><a href="{{url(app()->getLocale().'/userbooking/'.$link)}}">User Bookings</a>
	                        </li>
        </ul>