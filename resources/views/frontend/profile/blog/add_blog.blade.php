 <style>
.mce-in{
  display:none;
}
 </style>
   <?php
    if(session('pic_not')==1){
      $pic=1;
      \Session::forget('pic_not');
    }else{
      $pic=0;
    }
   ?>
 <div class="col-md-8 right-wrapper">
        <div class="inner-white-wrapper personal-details mb-3 overflow-hidden">
          <div class="row dp-flex align-items-center">
            <div class="col-md-6">
              <h3>Write a Blog</h3>
            </div>
            <div class="col-md-6 text-right">
              
            </div>
            <div class="col-md-12">
              <hr style="border: 1px solid;width: 677px;margin-left: 170px;">
                  @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }} Blog Pic should be less than 10 MB.</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            
            <div class="col-md-12">
              <strong>Image</strong>
              <form id="file-upload-form" class="uploader profile mt-2" method="post" action="{{url(app()->getLocale().'/save_blog/'.$loc)}}" enctype="multipart/form-data">
                  @csrf
                  <input id="file-upload" type="file" class="imgselect" name="blog_pic" accept="image/*">
                  <div class="blog-preview-image" style="display:none;height: 200px;width: 200px;overflow: hidden;">
                    <img id="blah" src="#" class="placeshow" alt="your image" style="display:none;height: 100%;width: 100%;object-fit: cover;"/>
                  </div>
                  <label for="file-upload" id="file-drag">
                    <img id="file-image" src="#" alt="Preview" class="hidden">
                    <div id="start">
                      <div class="col-md-12 "><img src="{{asset('img/ic_share@2x.png')}}"></div>
                      <span id="file-upload-btn" class="">Drop your image here or <br> click to upload it</span>
                    </div>
                  </label>
                <span style="color:red">@if($pic==1) {{"Pic is Required and it must be Png, Jpg and jpeg"}} @endif</span>
            </div>
            <div class="col-md-12 mt-4">
              <strong class="dp-block mb-2">Title*</strong>
              <input class="form-control" type="text" name="title" placeholder="Insert the title here" value="{{session('title')}}" required>
            </div>
            <div class="col-md-12 mt-4 mb-5">
              <strong class="dp-block mb-2">Content*</strong>
              <textarea class="form-control" name="desription">{{session('desc')}}</textarea>
            </div>
            <div class="col-md-12 mt-4 mb-5">
              <input class="form-control submit1" type="submit" name="submit" value="Submit" style="background-color: #E4002B;color: white;">
            </div>
            </form> 
          </div>
        </div>
      </div>
    </div>
  </div>
</section>