<style>
p{
	margin-left: -6px;
}
.mce-notification-inner {
    display: none;
}

.nav-tabs .nav-link.active {
  border-bottom: 5px solid #e4002b!important;
  color:red;
}
.nav-tabs .nav-link:hover {
  border-bottom: 5px solid #e4002b!important;
  color:red;
}

	</style>
 <!-- Right Wrapper -->
 <div class="col-md-9 right-wrapper">
        <div class="inner-white-wrapper personal-details mb-3 overflow-hidden blogs-wrap mt-0 p-5">
          <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item mr-5">
              <a class="nav-link active" href="#profile" role="tab" data-toggle="tab">Approved Blogs</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#buzz" role="tab" data-toggle="tab">Blogs under moderation</a>
            </li>
            <li class="nav-item" style="    margin-left: 145px;">
            <a href="{{url(app()->getLocale().'/blog/'.$link)}}">Add Blog</a>
            </li>
          </ul>
          
          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="profile">
              <div class="row mt-5">
			  @if(count($getBlogsapprove)>0)
			  @foreach($getBlogsapprove as $row)
                <div class="col-md-6">
                  <div class="blog-card mb-5">
				  @if(!isset($row->pic) || $row->pic=='')
		          <img class="img-fluid" src="{{asset('/img/no-property.png')}}">
		           @else
		           <img class="img-fluid" src="{{url('uploads/blog/'.$row->pic)}}">
		           @endif
                    <div class="p-3">
                      <p>{{date('M d, Y',strtotime($row->created_at))}}</p>
                      <h5>{{\Str::limit(ucfirst($row->title), 25)}}</h5>
                      <p><?php
$text = $row->description;

 
echo \Str::limit(strip_tags($text, '<p>'),20);

 
?></p>
                      <p><br/><a href="{{url(app()->getLocale().'/blog_details/'.$link.'/'.$row->_id)}}" style="color:red;">READ MORE &gt;</a>
                      </p>
                    </div>
                  </div>
                </div>
				@endforeach
                    @else
                        <div class="" style="display: block;
                            max-height: 60px;
                            text-align: center;
                            background-color: #d6e9c6;
                            line-height: 4;
                            width: 100%;
                            ">
                            <p style="font-size: 16px;font-weight: 700;">No Result Found</p>
                        </div>
                    @endif

                
            
              </div>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="buzz">
              <div class="row mt-5">
			  @if(count($getBlogs)>0)
			  @foreach($getBlogs as $row)
                <div class="col-md-6">
                  <div class="blog-card mb-5">
				  @if(!isset($row->pic) || $row->pic=='')
		          <img class="img-fluid" src="{{asset('/img/no-property.png')}}">
		           @else
		           <img class="img-fluid" src="{{url('uploads/blog/'.$row->pic)}}">
		           @endif
                    <div class="p-3">
                      
                      <p>{{date('M d, Y',strtotime($row->created_at))}}</p>
                      <h5>{{\Str::limit(ucfirst($row->title), 25)}}</h5>
                      <p><?php
$text = $row->description;


// Allow <p> and <a>
echo \Str::limit(strip_tags($text, '<p>'),100);

// as of PHP 7.4.0 the line above can be written as:
// echo strip_tags($text, ['p', 'a']);
?></p>
 <a class="edit-icon1" href="{{url(app()->getLocale().'/edit_blog/'.$link.'/'.$row->_id)}}"><img  src="{{asset('img/edit.png')}}"></a>

<a class="delete-icon1" onClick="delete_blog('{{$row->_id}}')" href="javascript:;">
  <img  src="{{asset('img/delete.png')}}">
</a>
                      <p><br/><a href="{{url(app()->getLocale().'/blog_details/'.$link.'/'.$row->_id)}}" style="color:red;">READ MORE &gt;</a>
                      </p>
                    </div>
                  </div>
                </div>
				@endforeach
                    @else
                        <div class="" style="display: block;
                            max-height: 60px;
                            text-align: center;
                            background-color: #d6e9c6;
                            line-height: 4;
                            width: 100%;
                            ">
                            <p style="font-size: 16px;font-weight: 700;">No Result Found</p>
                        </div>
                    @endif

                
            
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
