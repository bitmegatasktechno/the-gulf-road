@php
$lang=Request::segment(1);
$loc=Request::segment(3);
$modules = \App\Models\Module::where('location',$loc)->get();
if($loc!='all'){
    $link=$loc;
}else{
    $link='all';
}
@endphp
@extends('frontend.layouts.home')
@section('title','Saved Coworkspace')
@section('content')
@include('frontend.layouts.default-header')
<section class="inner-body body-light-grey mt-70">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="list-inline breadcrum">
                    <li><a href="{{url(app()->getLocale().'/home/'.$link)}}">Home </a></li>
                    <span> / </span>
                    <li><a href="javascript:;"> Add Blog</a></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="inner-white-wrapper left-navigation mb-3">
                    @include('frontend.profile.user_profile_sidebar')
                </div>
            </div>
            @include('frontend.profile.blog.add_blog')
        </div>
    </div>
</section>
@include('frontend.layouts.footer')
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click','.delete_cospace', function(e){
            var val = $(this).data('id');
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url : WEBSITE_URL+"/delete-coworkspace",
                        type:'post',
                        data : {
                            'id':val
                        },
                        async:true,
                        dataType : 'json',
                        beforeSend:function(){
                            startLoader('.container');
                        },
                        complete:function(){
                           stopLoader('.container');
                        },
                        success : function(data){
                            if(data.status){
                                Swal.fire({
                                    title: 'Success!',
                                    text: data.message,
                                    icon: 'success',
                                    confirmButtonText: 'Ok'
                                }).then(function(){
                                    window.location.reload();
                                });
                            }else{
                                Swal.fire({
                                    title: 'Error!',
                                    text: data.message,
                                    icon: 'error',
                                    confirmButtonText: 'Ok'
                                });
                            }
                        },
                        error : function(data){
                            stopLoader('.container');
                            if(data.responseJSON){
                                var err_response = data.responseJSON;
                                if(err_response.errors==undefined && err_response.message) {
                                    Swal.fire({
                                        title: 'Error!',
                                        text: err_response.message,
                                        icon: 'error',
                                        confirmButtonText: 'Ok'
                                    });
                                }
                            }
                        }
                    });
                }
            });
        })
    });
    tinymce.init({
      height: 250,
      theme: 'modern',
      image_advtab: true,
      selector: 'textarea',
      menubar: '',  // skip file
      plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
      toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
      templates: [{ title: 'Test template 1', content: 'Test 1' },{ title: 'Test template 2', content: 'Test 2' }],
      content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css'
      ]
     });
    function readURL(input) {
        var fileName = document.getElementById("fileName").value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                    $('.blog-preview-image').show();
                };
                reader.readAsDataURL(input.files[0]);
            }
        }else{
            alert("Only PNG,JPEG is required");
            return false;
        }   
    }
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
                $('.blog-preview-image').show();
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".imgselect").change(function(){
        $(".placeshow").show();
       
        readURL(this);
    });
</script>
@endsection
