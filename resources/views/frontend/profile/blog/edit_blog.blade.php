<style>
.mce-in{
  display:none;
}
   </style>
   @php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
@extends('frontend.layouts.home')
@section('title','Saved Coworkspace')

@section('content')

@include('frontend.layouts.default-header')
    <section class="inner-body body-light-grey mt-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="list-inline breadcrum">
                        <li><a href="{{url(app()->getLocale().'/home/'.$link)}}">Home </a></li>
                        <span> / </span>
                        <li><a href="javascript:;"> Add Blog</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <!-- Navigation Block -->
                    <div class="inner-white-wrapper left-navigation mb-3">
                        @include('frontend.profile.user_profile_sidebar')
                    </div>
                </div>
 <!-- Right Wrapper -->
 <div class="col-md-8 right-wrapper">
        <div class="inner-white-wrapper personal-details mb-3 overflow-hidden">
          <div class="row dp-flex align-items-center">
            <div class="col-md-6">
              <h3>Edit a Blog</h3>
            </div>
            <div class="col-md-6 text-right">
              
            </div>
            <div class="col-md-12"><hr style="border: 1px solid;width: 677px;
    margin-left: 170px;"></div>
            <div class="col-md-12">
              <strong>Image</strong>
              <form id="file-upload-form" class="uploader profile mt-2" method="post" action="{{url(app()->getLocale().'/update_blog/'.$loc)}}" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="blog_id" value="{{$blogs->_id}}">
                  <img class="img-fluid old_iamge" src="{{url('uploads/blog/'.$blogs->pic)}}" style="    width: 100px;">
                  <input id="file-upload" type="file" class="imgselect" name="blog_pic" accept="image/*">
                  <img id="blah" src="#" class="placeshow" alt="your image" style="position: absolute;
    object-fit: cover;
    width: 128px;
    display:none;"/>
                  <label for="file-upload" id="file-drag">
                    <img id="file-image" src="#" alt="Preview" class="hidden">
                    <div id="start">
                      <div class="col-md-12 "><img src="{{asset('img/ic_share@2x.png')}}"></div>
                      <span id="file-upload-btn" class="">Drop your image here or <br> click to upload it</span>
                    </div>
                  </label>
                
            </div>
            <div class="col-md-5 mt-4">
              <strong class="dp-block mb-2">Title*</strong>
              <input class="form-control" type="text" name="title" placeholder="Insert the title here" value="{{$blogs->title}}" required>
            </div>
            <div class="col-md-12 mt-4 mb-5">
              <strong class="dp-block mb-2">Content*</strong>
              <textarea class="form-control"  name="desription">{{$blogs->description}}</textarea>
            </div>
            <div class="col-md-12 mt-4 mb-5">
             
              <input class="form-control" type="submit" name="submit" value="Submit" style="    background-color: #E4002B;
    color: white;">
            </div>
            </form> 
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
    @include('frontend.layouts.footer')
@endsection
@section('scripts')
    <script type="text/javascript">

        $(document).ready(function(){

            $(document).on('click','.delete_cospace', function(e){
                var val = $(this).data('id');
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url : WEBSITE_URL+"/delete-coworkspace",
                            type:'post',
                            data : {
                                'id':val
                            },
                            async:true,
                            dataType : 'json',
                            // processData: false,
                            // contentType: false,
                            beforeSend:function(){
                                startLoader('.container');
                            },
                            complete:function(){
                               stopLoader('.container');
                            },
                            success : function(data){
                                if(data.status){
                                    Swal.fire({
                                        title: 'Success!',
                                        text: data.message,
                                        icon: 'success',
                                        confirmButtonText: 'Ok'
                                    }).then(function(){
                                        window.location.reload();
                                    });
                                }else{
                                    Swal.fire({
                                        title: 'Error!',
                                        text: data.message,
                                        icon: 'error',
                                        confirmButtonText: 'Ok'
                                    });
                                }
                            },
                            error : function(data){
                                stopLoader('.container');
                                if(data.responseJSON){
                                    var err_response = data.responseJSON;
                                    if(err_response.errors==undefined && err_response.message) {
                                        Swal.fire({
                                            title: 'Error!',
                                            text: err_response.message,
                                            icon: 'error',
                                            confirmButtonText: 'Ok'
                                        });
                                    }
                                }
                            }
                        });
                    }
                });
            })
        });

        tinymce.init({
  selector: 'textarea',
  height: 500,
  theme: 'modern',
  plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });

function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
                $(".old_iamge").hide();
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $(".imgselect").change(function(){
        $(".placeshow").show();
       
        readURL(this);
    });

  
    </script>
@endsection