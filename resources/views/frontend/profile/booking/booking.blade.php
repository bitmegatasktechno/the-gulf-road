@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
@extends('frontend.layouts.home')
@section('title','Enquiries')
@section('content')

    @include('frontend.layouts.default-header')
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
    <section class="inner-body body-light-grey mt-70">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-12">
	                <ul class="list-inline breadcrum">
	                    <li><a href="{{url(app()->getLocale().'/home/'.$link)}}">Home </a></li>
	                    <span> / </span>
	                    <li><a href="{{url(app()->getLocale().'/change-password/'.$link)}}"> Booking</a></li>
	                </ul>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-md-3">
                <!-- Navigation Block -->
	                <div class="inner-white-wrapper left-navigation mb-3">
@include('frontend.profile.user_profile_sidebar')
	                </div>
	            </div>
	            <!-- Right Wrapper -->
	            <div class="col-md-9 right-wrapper">
	                <div class="inner-white-wrapper personal-details mb-3 overflow-hidden">
	                    <div class="row dp-flex align-items-center">
	                        <div class="col-md-6">
	                            <h3>User Booking</h3>
	                        </div>
	                        <div class="col-md-6 text-right">

	                        </div>
	                        <div class="col-md-12">
 	                        </div>
                        
	                        <div class="col-md-12 personal_information_form table table-responsive">
                            <table id="example3" class="table " >
                              <thead>
                              <tr>
                                <td>SrNo</td>
                                <td>Property</td>
                                <td>Type</td>
                                <td>From Date</td>
                                <td>To Date</td>
                                <td>No. Of Guest</td>
                                <td>User</td>
                                <td>Email</td>
                                <td>Phone</td>
                              </tr>
                            </thead>
                            <tbody>
                              <?php $i=1; ?>
                              <?php if(@$getAll){ ?>
                              @foreach($getAll as $getEnq)
                            
                              <tr>
                                <td>{{$i}}</td>
                                
                                <td>@if(@$getEnq->type=='rent' || @$getEnq->type=='buy'){{@$getEnq->property->property_title}} @else {{@$getEnq->swap->title}} @endif</td>
                                <td>{{@$getEnq->type}}</td>
                                <td>{{date('d M Y', strtotime(@$getEnq->startdate))}}</td>
                                <td>{{date('d M Y', strtotime(@$getEnq->enddate))}}</td>
                                <td>{{@$getEnq->guest}}</td>
                                <td>{{@$getEnq->user->name}}</td>
                                <td>{{@$getEnq->user->email}}</td>
                                <td>{{@$getEnq->user->phone}}</td>
                               


    

                              </tr>
                              <?php $i++; ?>
                              @endforeach
                            <?php } ?>
                            </tbody>
                            </table>
	                        </div>
	                        <div class="col-md-12">
 	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>

    @include('frontend.layouts.footer')

@endsection

@section('scripts')

@include('frontend.profile.common.js')

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>

<script type="text/javascript">
$("#viewdata").click(function(){
  $("#viewrating").modal('show');
  var name=$("#name").val();
  var email=$("#email").val();
  var phone=$("#phone").val();
  var message=$("#message").val();
  var property=$("#property").val();
  var date=$("#date").val();

  $("#property1").html(property);
  $("#user1").html(name);
  $("#email1").html(email);
  $("#phone1").html(phone);
  $("#message1").html(message);
  $("#date1").html(date);
});
function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};
$(function() {
              $('#example3').DataTable();

              var startdate = getUrlParameter('start_date');
            if(startdate){
              var enddate = getUrlParameter('end_date');
              var startdate = getUrlParameter('start_date');
            }else{
              var enddate = moment().format('MM/DD/YYYY');
              var startdate = moment().format('MM/DD/YYYY');
            }


              $('input[name="daterange"]').daterangepicker({
                opens: 'left'
              }, function(start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                      var startdate=start.format('MM/DD/YYYY');
                      var enddate=end.format('MM/DD/YYYY');
                      $('#start_date').val(startdate);
                      $('#end_date').val(enddate);
                      $('#filter_form').submit();
                      });


          });

      $('#reportrange span').html('{{@$start_date}}' + ' - ' + '{{@$end_date}}');

	// save user personal informations.
	$(document).on('click','.personal_info_submit_btn', function(e){
		e.preventDefault();
        // clear all errors
        $('#profile_form').find('.is-invalid').removeClass('is-invalid');
        $('#profile_form').find('.invalid-feedback').text('');

        var formFields = new FormData($('#profile_form')[0]);
        var ajax_url = WEBSITE_URL+"/change-password";

        $.ajax({
            url : ajax_url,
            type:'post',
            data : formFields,
            dataType : 'json',
            processData: false,
            contentType: false,
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            beforeSend:function(){
                startLoader('.page-content');
            },
            complete:function(){
                //
            },
            success : function(data){
                stopLoader('.page-content');
                if(data.status){
                	Swal.fire({
	                    title: 'Success!',
	                    text: data.message,
	                    icon: 'success',
	                    confirmButtonText: 'Ok'
	                }).then((result) => {
	                  window.location.reload();
	                });
                }else{
                	Swal.fire({
	                    title: 'Error!',
	                    text: data.message,
	                    icon: 'error',
	                    confirmButtonText: 'Ok'
	                });
                }
            },
            error : function(data){
                stopLoader('.page-content');
                if(data.responseJSON){
                    var err_response = data.responseJSON;
                    if(err_response.errors==undefined && err_response.message) {
                        Swal.fire({
                            title: 'Error!',
                            text: err_response.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }

                    $.each(err_response.errors, function(i, obj){
                        $('#profile_form').find('input[name="'+i+'"]').next().text(obj);
                        $('#profile_form').find('input[name="'+i+'"]').addClass('is-invalid');
                    });
                }
            }
        });
	});

  $("#filter4").on('change', function()
{
  $('#example3').DataTable().columns(4).search(this.value).draw();
  var count=$("#example3_info").html();
  var slug = count.split('of ').pop();
  var res = slug.substring(0, 5);
  var matches = res.match(/(\d+)/);
    $('#total_records').text(matches[0]);
});
</script>
@endsection
