@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
@extends('frontend.layouts.home')
@section('title','Saved Coworkspace')

@section('content')

@include('frontend.layouts.default-header')
    <section class="inner-body body-light-grey mt-70">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="list-inline breadcrum">
                        <li><a href="{{url(app()->getLocale().'/home/'.$link)}}">Home </a></li>
                        <span> / </span>
                        <li><a href="javascript:;"> My Review</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <!-- Navigation Block -->
                    <div class="inner-white-wrapper left-navigation mb-3">
                         @include('frontend.profile.user_profile_sidebar')
                    </div>
                </div>
                <!-- Right Wrapper -->
                @include('frontend.profile.review.list')
            </div>
        </div>
    </section>
    @include('frontend.layouts.footer')
@endsection

@section('scripts')
    <script type="text/javascript">

        $(document).ready(function(){

            $(document).on('click','.delete_cospace', function(e){
                var val = $(this).data('id');
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url : WEBSITE_URL+"/delete-coworkspace",
                            type:'post',
                            data : {
                                'id':val
                            },
                            async:true,
                            dataType : 'json',
                            // processData: false,
                            // contentType: false,
                            beforeSend:function(){
                                startLoader('.container');
                            },
                            complete:function(){
                               stopLoader('.container');
                            },
                            success : function(data){
                                if(data.status){
                                    Swal.fire({
                                        title: 'Success!',
                                        text: data.message,
                                        icon: 'success',
                                        confirmButtonText: 'Ok'
                                    }).then(function(){
                                        window.location.reload();
                                    });
                                }else{
                                    Swal.fire({
                                        title: 'Error!',
                                        text: data.message,
                                        icon: 'error',
                                        confirmButtonText: 'Ok'
                                    });
                                }
                            },
                            error : function(data){
                                stopLoader('.container');
                                if(data.responseJSON){
                                    var err_response = data.responseJSON;
                                    if(err_response.errors==undefined && err_response.message) {
                                        Swal.fire({
                                            title: 'Error!',
                                            text: err_response.message,
                                            icon: 'error',
                                            confirmButtonText: 'Ok'
                                        });
                                    }
                                }
                            }
                        });
                    }
                });
            })
        });
    </script>
@endsection
