<div class="col-md-9">
	<div class="card p-4">
    <ul class="nav nav-tabs" role="tablist">
			<li class="nav-item mr-3">
			    <a class="nav-link active" href="#" >My Review</a>
			</li>
	    </ul>
<style>

/* Rating Star Widgets Style */
.rating-stars ul {
  list-style-type:none;
  padding:0;

  -moz-user-select:none;
  -webkit-user-select:none;
}
.rating-stars ul > li.star {
  display:inline-block;

}

/* Idle State of the stars */
.rating-stars ul > li.star > i.fa {
  font-size:2.5em; /* Change the size of the stars */
  color:#ccc; /* Color on idle state */
}

/* Hover state of the stars */
.rating-stars ul > li.star.hover > i.fa {
  color:pink;
}

/* Selected state of the stars */
.rating-stars ul > li.star.selected > i.fa {
  color:red;
}

</style>

@if(count($ratings) < 1)
<br>
  <div class="col-md-12 text-center">
  No review yet.
</div> 
@endif

@foreach($ratings as $rating)
<div class="row mt-5 dp-flex align-items-center testimonial-inner">
  <div class="col-md-1">
  @if(!isset($rating->user->image) || $rating->user->image=='')
                                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 50px; width: 50px">
                                    @else
                                    @if(file_exists('uploads/profilePics/'.$rating->user->image))
                                    <img class="circle" src="{{url('uploads/profilePics/'.$rating->user->image)}}" style="height: 50px; width: 50px">
                                    @else
                                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 50px; width: 50px">
                                    @endif
                                    @endif
  </div>
  <div class="col-md-5">
      <h6 class="mb-0">{{@$rating->user->name}}</h6>
      <ul class="list-inline mb-0">
        @if(@$rating->average==1)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @elseif(@$rating->average==2)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @elseif(@$rating->average==3)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @elseif(@$rating->average==4)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @elseif(@$rating->average==5)
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
          @endif
      </ul>
  </div>
  <div class="col-md-6 text-right">
      <small>{{ date('d-F,Y', strtotime($rating->create)) }}</small>
  </div>
  <div class="col-md-12">
      <p class="mt-4">{{@$rating->review}}</p>
  </div>
  <div class="col-md-12">
      <p class="mt-4">Property Name: @if(@$rating->buy->property_title) {{@$rating->buy->property_title}} @elseif(@$rating->swap->title) {{@$rating->swap->title}} (Swap) @elseif(@$rating->co->title) {{@$rating->co->title}} (Co-Space) @endif</p>
  </div>
  <div class="col-md-12">
      <hr>
  </div>
</div>
@endforeach
<!-- <div class="col-md-12 text-right">
  <a class="ratings-color" href="">View More Reviews</a>
  <hr>
</div> -->

@section('scripts')
@include('frontend.profile.common.js')
<script type="text/javascript">
$(document).on('click','.sendreview', function(){
  var ajax_url = WEBSITE_URL+"/property/review";
  var user_id=$("#user_id").val();
  var review_type=$("#review_type").val();
  var type_id=$("#type_id").val();
  var hygine=$("#hygine").val();
  var atmosphere=$("#atmosphere").val();
  var location=$("#location").val();
  var cleanliness=$("#cleanliness").val();
  var service=$("#service").val();
  var review=$("#review").val();
  $.ajax({
    url:ajax_url,
    method:"POST",
    data:{
      user_id:user_id,
      review_type:review_type,
      type_id:type_id,
      atmosphere:atmosphere,
      hygine:hygine,
      location:location,
      review:review,
      cleanliness:cleanliness,
      service:service
    },
    headers:{
      'X-CSRF-TOKEN': '{{ csrf_token() }}',
    },
    success:function(data){
      stopLoader('.page-content');
      if(data.status){
        Swal.fire({
            title: 'Success!',
            text: data.msg,
            icon: 'success',
            confirmButtonText: 'Ok'
        }).then((result) => {
          window.location.reload();
        });
      }else{
        Swal.fire({
            title: 'Success!',
            text: data.msg,
            icon: 'success',
            confirmButtonText: 'Ok'
        }).then((result) => {
          window.location.reload();
        });
      }
      // alert(data.msg);
    }
  });
});
$(".enterreview").click(function(){
$("#viewrefund").modal('show');
});

$(document).ready(function(){

/* 1. Visualizing things on Hover - See next part for action on click */
$('#stars li').on('mouseover', function(){
var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

// Now highlight all the stars that's not after the current hovered star
$(this).parent().children('li.star').each(function(e){
  if (e < onStar) {
    $(this).addClass('hover');
  }
  else {
    $(this).removeClass('hover');
  }
});

}).on('mouseout', function(){
$(this).parent().children('li.star').each(function(e){
  $(this).removeClass('hover');
});
});


/* 2. Action to perform on click */
$('#stars li').on('click', function(){
var onStar = parseInt($(this).data('value'), 10); // The star currently selected
var stars = $(this).parent().children('li.star');

for (i = 0; i < stars.length; i++) {
  $(stars[i]).removeClass('selected');
}

for (i = 0; i < onStar; i++) {
  $(stars[i]).addClass('selected');
}

// JUST RESPONSE (Not needed)
var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
var msg = "";
if (ratingValue > 1) {
    msg = ratingValue;
}
else {
    msg = ratingValue;
}
responseMessage(msg);

});


});

$(document).ready(function(){

/* 1. Visualizing things on Hover - See next part for action on click */
$('#stars1 li').on('mouseover', function(){
var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

// Now highlight all the stars that's not after the current hovered star
$(this).parent().children('li.star').each(function(e){
if (e < onStar) {
  $(this).addClass('hover');
}
else {
  $(this).removeClass('hover');
}
});

}).on('mouseout', function(){
$(this).parent().children('li.star').each(function(e){
$(this).removeClass('hover');
});
});


/* 2. Action to perform on click */
$('#stars1 li').on('click', function(){
var onStar = parseInt($(this).data('value'), 10); // The star currently selected
var stars = $(this).parent().children('li.star');

for (i = 0; i < stars.length; i++) {
$(stars[i]).removeClass('selected');
}

for (i = 0; i < onStar; i++) {
$(stars[i]).addClass('selected');
}

// JUST RESPONSE (Not needed)
var ratingValue = parseInt($('#stars1 li.selected').last().data('value'), 10);
var msg = "";
if (ratingValue > 1) {
  msg = ratingValue;
}
else {
  msg = ratingValue;
}
responseMessage1(msg);

});


});

$(document).ready(function(){

/* 1. Visualizing things on Hover - See next part for action on click */
$('#stars2 li').on('mouseover', function(){
var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

// Now highlight all the stars that's not after the current hovered star
$(this).parent().children('li.star').each(function(e){
if (e < onStar) {
  $(this).addClass('hover');
}
else {
  $(this).removeClass('hover');
}
});

}).on('mouseout', function(){
$(this).parent().children('li.star').each(function(e){
$(this).removeClass('hover');
});
});


/* 2. Action to perform on click */
$('#stars2 li').on('click', function(){
var onStar = parseInt($(this).data('value'), 10); // The star currently selected
var stars = $(this).parent().children('li.star');

for (i = 0; i < stars.length; i++) {
$(stars[i]).removeClass('selected');
}

for (i = 0; i < onStar; i++) {
$(stars[i]).addClass('selected');
}

// JUST RESPONSE (Not needed)
var ratingValue = parseInt($('#stars2 li.selected').last().data('value'), 10);
var msg = "";
if (ratingValue > 1) {
  msg = ratingValue;
}
else {
  msg = ratingValue;
}
responseMessage2(msg);

});


});

$(document).ready(function(){

/* 1. Visualizing things on Hover - See next part for action on click */
$('#stars3 li').on('mouseover', function(){
var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

// Now highlight all the stars that's not after the current hovered star
$(this).parent().children('li.star').each(function(e){
if (e < onStar) {
  $(this).addClass('hover');
}
else {
  $(this).removeClass('hover');
}
});

}).on('mouseout', function(){
$(this).parent().children('li.star').each(function(e){
$(this).removeClass('hover');
});
});


/* 2. Action to perform on click */
$('#stars3 li').on('click', function(){
var onStar = parseInt($(this).data('value'), 10); // The star currently selected
var stars = $(this).parent().children('li.star');

for (i = 0; i < stars.length; i++) {
$(stars[i]).removeClass('selected');
}

for (i = 0; i < onStar; i++) {
$(stars[i]).addClass('selected');
}

// JUST RESPONSE (Not needed)
var ratingValue = parseInt($('#stars3 li.selected').last().data('value'), 10);
var msg = "";
if (ratingValue > 1) {
  msg = ratingValue;
}
else {
  msg = ratingValue;
}
responseMessage3(msg);

});


});

$(document).ready(function(){

/* 1. Visualizing things on Hover - See next part for action on click */
$('#stars4 li').on('mouseover', function(){
var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

// Now highlight all the stars that's not after the current hovered star
$(this).parent().children('li.star').each(function(e){
if (e < onStar) {
  $(this).addClass('hover');
}
else {
  $(this).removeClass('hover');
}
});

}).on('mouseout', function(){
$(this).parent().children('li.star').each(function(e){
$(this).removeClass('hover');
});
});


/* 2. Action to perform on click */
$('#stars4 li').on('click', function(){
var onStar = parseInt($(this).data('value'), 10); // The star currently selected
var stars = $(this).parent().children('li.star');

for (i = 0; i < stars.length; i++) {
$(stars[i]).removeClass('selected');
}

for (i = 0; i < onStar; i++) {
$(stars[i]).addClass('selected');
}

// JUST RESPONSE (Not needed)
var ratingValue = parseInt($('#stars4 li.selected').last().data('value'), 10);
var msg = "";
if (ratingValue > 1) {
  msg = ratingValue;
}
else {
  msg = ratingValue;
}
responseMessage4(msg);

});


});


function responseMessage(msg) {
$("#hygine").val(msg);
}
function responseMessage1(msg) {
$("#atmosphere").val(msg);
}
function responseMessage2(msg) {
$("#location").val(msg);
}
function responseMessage3(msg) {
$("#cleanliness").val(msg);
}
function responseMessage4(msg) {
$("#service").val(msg);
}
</script>
@endsection
