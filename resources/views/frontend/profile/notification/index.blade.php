@php
$lang=Request::segment(1);
$loc=Request::segment(3);
if($loc == 'ksa' || $loc == 'uae' )
{
   $loc21 = strtoupper($loc);
}else 
{
    $loc21 = ucfirst($loc);
}


  $modules = \App\Models\Module::where('location',$loc21)->get();
  if($loc!='all'){
      $link=$loc;
  }else{
      $link='all';
  }
                    @endphp
@extends('frontend.layouts.home')
@section('title','Enquiries')
@section('content')

@include('frontend.layouts.default-header')

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap4.min.css">
<style>
 
 
div#example3_filter{
  position: absolute;
    left: 60%;
    top: -37%;
}
</style>
    <section class="inner-body body-light-grey mt-70">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-12">
	                <ul class="list-inline breadcrum">
	                    <li><a href="{{url(app()->getLocale().'/home/'.$link)}}">Home </a></li>
	                    <span> / </span>
	                    <li><a href="{{url(app()->getLocale().'/change-password/'.$link)}}"> Notifications</a></li>
	                </ul>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-md-3">

	                <!-- Navigation Block -->
	                <div class="inner-white-wrapper left-navigation mb-3">
@include('frontend.profile.user_profile_sidebar')
	                </div>
	            </div>
	            <!-- Right Wrapper -->
	            <div class="col-md-9 right-wrapper">
	                <div class="inner-white-wrapper personal-details mb-3 overflow-hidden">
	                    <div class="row dp-flex align-items-center">
	                        <div class="col-md-6">
	                            <h3>Notifications</h3>
	                        </div>
	                        <div class="col-md-6 text-right">

	                        </div>
	                        <div class="col-md-12">
	                            <hr>
	                        </div>
                        
	                        <div class="col-md-12 personal_information_form">
                            <table id="example3" class="table">
                              <thead>
                              <tr>
                                <td>SrNo</td>
                                <td>From</td>
                                <td>Message</td>
                                <td>Date</td>

                              </tr>
                            </thead>
                            <tbody>
                              <?php $i=1; ?>
                              <?php if(@$getAll){ ?>
                              @foreach($getAll as $getEnq)
                            
                              <tr>
                                <td>{{$i}}</td>
                                <td>{{@$getEnq->user->name}}</td>
                                <td>@if(@$getEnq->message=='You got new Enquiry')
                                  <a href="{{url(app()->getLocale().'/enquiries/'.$link)}}">{{@$getEnq->message}}</a>
                                    @elseif(@$getEnq->message=='You got new Booking')
                                  <a href="{{url(app()->getLocale().'/userbooking/'.$link)}}">{{@$getEnq->message}}</a>
                                    @else
                                    <a href="{{url(app()->getLocale().'/user_chat/'.$link.'/'.@$getEnq->sender_id)}}">{{@$getEnq->message}}</a>@endif</td>
                                <td>{{date('M-d-Y', strtotime(@$getEnq->created_at))}}</td>
                               


    

                              </tr>
                              <?php $i++; ?>
                              @endforeach
                            <?php } ?>
                            </tbody>
                            </table>
	                        </div>
	                        <div class="col-md-12">
	                            <hr>
                                <a href="{{url(app()->getLocale().'/clearnot/'.$link)}}" style="right: 10px;text-align: right;display: inherit;">Clear</a>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>

    @include('frontend.layouts.footer')

@endsection

@section('scripts')

@include('frontend.profile.common.js')

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
$("#viewdata").click(function(){
  $("#viewrating").modal('show');
  var name=$("#name").val();
  var email=$("#email").val();
  var phone=$("#phone").val();
  var message=$("#message").val();
  var property=$("#property").val();
  var date=$("#date").val();

  $("#property1").html(property);
  $("#user1").html(name);
  $("#email1").html(email);
  $("#phone1").html(phone);
  $("#message1").html(message);
  $("#date1").html(date);
});
function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};
$(function() {
              $('#example3').DataTable();

              var startdate = getUrlParameter('start_date');
            if(startdate){
              var enddate = getUrlParameter('end_date');
              var startdate = getUrlParameter('start_date');
            }else{
              var enddate = moment().format('MM/DD/YYYY');
              var startdate = moment().format('MM/DD/YYYY');
            }


              $('input[name="daterange"]').daterangepicker({
                opens: 'left'
              }, function(start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
                      var startdate=start.format('MM/DD/YYYY');
                      var enddate=end.format('MM/DD/YYYY');
                      $('#start_date').val(startdate);
                      $('#end_date').val(enddate);
                      $('#filter_form').submit();
                      });


          });

      $('#reportrange span').html('{{@$start_date}}' + ' - ' + '{{@$end_date}}');

	// save user personal informations.
	$(document).on('click','.personal_info_submit_btn', function(e){
		e.preventDefault();
        // clear all errors
        $('#profile_form').find('.is-invalid').removeClass('is-invalid');
        $('#profile_form').find('.invalid-feedback').text('');

        var formFields = new FormData($('#profile_form')[0]);
        var ajax_url = WEBSITE_URL+"/change-password";

        $.ajax({
            url : ajax_url,
            type:'post',
            data : formFields,
            dataType : 'json',
            processData: false,
            contentType: false,
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            beforeSend:function(){
                startLoader('.page-content');
            },
            complete:function(){
                //
            },
            success : function(data){
                stopLoader('.page-content');
                if(data.status){
                	Swal.fire({
	                    title: 'Success!',
	                    text: data.message,
	                    icon: 'success',
	                    confirmButtonText: 'Ok'
	                }).then((result) => {
	                  window.location.reload();
	                });
                }else{
                	Swal.fire({
	                    title: 'Error!',
	                    text: data.message,
	                    icon: 'error',
	                    confirmButtonText: 'Ok'
	                });
                }
            },
            error : function(data){
                stopLoader('.page-content');
                if(data.responseJSON){
                    var err_response = data.responseJSON;
                    if(err_response.errors==undefined && err_response.message) {
                        Swal.fire({
                            title: 'Error!',
                            text: err_response.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }

                    $.each(err_response.errors, function(i, obj){
                        $('#profile_form').find('input[name="'+i+'"]').next().text(obj);
                        $('#profile_form').find('input[name="'+i+'"]').addClass('is-invalid');
                    });
                }
            }
        });
	});

  $("#filter4").on('change', function()
{
  $('#example3').DataTable().columns(4).search(this.value).draw();
  var count=$("#example3_info").html();
  var slug = count.split('of ').pop();
  var res = slug.substring(0, 5);
  var matches = res.match(/(\d+)/);
    $('#total_records').text(matches[0]);
});
</script>
@endsection
