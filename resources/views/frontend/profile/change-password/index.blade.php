@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
@extends('frontend.layouts.home')
@section('title','Change Password')
@section('content')

    @include('frontend.layouts.default-header')

    <section class="inner-body body-light-grey mt-70">
	    <div class="container">
	        <div class="row">
	            <div class="col-md-12">
	                <ul class="list-inline breadcrum">
	                    <li><a href="{{url(app()->getLocale().'/home/'.$link)}}">Home </a></li>
	                    <span> / </span>
	                    <li><a href="{{url(app()->getLocale().'/change-password/'.$link)}}"> Change Password</a></li>
	                </ul>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-md-3">


	                <!-- Navigation Block -->
	                <div class="inner-white-wrapper left-navigation mb-3">
@include('frontend.profile.user_profile_sidebar')
	                </div>
	            </div>
	            <!-- Right Wrapper -->
	            <div class="col-md-9 right-wrapper">
	                <div class="inner-white-wrapper personal-details mb-3 overflow-hidden">
	                    <div class="row dp-flex align-items-center">
	                        <div class="col-md-6">
	                            <h3>Change Password</h3>
	                        </div>
	                        <div class="col-md-6 text-right">

	                        </div>
	                        <div class="col-md-12">
	                            <hr>
	                        </div>
	                        <div class="col-md-8 personal_information_form">

	                        	<form class="new-cntct-frm" name="profile_form" id="profile_form">
	                                @csrf
	                                <div class="loc-form p-3">
					                    <div class="row">
					                        <div class="col-md-12">
		                                        <div class="form-group">
		                                            <label>Current Password</label>
		                                            <input type="password" class="form-control" placeholder="Enter Current Password" name="current_password">
		                                            <span class="invalid-feedback"></span>
		                                        </div>
		                                    </div>
		                                    <div class="clearfix"></div>

		                                    <div class="col-md-12">
								          		<div class="form-group">
										            <label>New Password</label>
										            <input type="password" class="form-control" placeholder="Enter New Password" name="password">
										            <span class="invalid-feedback"></span>
								          		</div>
								          	</div>
		                                    <div class="clearfix"></div>

		                                    <div class="col-md-12">
		                                        <div class="form-group">
		                                            <label>Confirm Password</label>
		                                            <input type="password" class="form-control" placeholder="Enter Confirm Password" name="password_confirmation">
		                                            <span class="invalid-feedback"></span>
		                                        </div>
		                                    </div>

					                        <div class="col-md-12 text-right">
					                        	<button type="button" class="red-btn rounded pt-3 pb-3 mt-3 mb-3 personal_info_submit_btn">Update</button>
					                        </div>
					                    </div>
					                </div>
							    </form>
	                        </div>
	                        <div class="col-md-12">
	                            <hr>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>

    @include('frontend.layouts.footer')

@endsection

@section('scripts')

@include('frontend.profile.common.js')
<script type="text/javascript">
	// save user personal informations.
	$(document).on('click','.personal_info_submit_btn', function(e){
		e.preventDefault();
        // clear all errors
        $('#profile_form').find('.is-invalid').removeClass('is-invalid');
        $('#profile_form').find('.invalid-feedback').text('');

        var formFields = new FormData($('#profile_form')[0]);
        var ajax_url = WEBSITE_URL+"/change-password";

        $.ajax({
            url : ajax_url,
            type:'post',
            data : formFields,
            dataType : 'json',
            processData: false,
            contentType: false,
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            beforeSend:function(){
                startLoader('.page-content');
            },
            complete:function(){
                //
            },
            success : function(data){
                stopLoader('.page-content');
                if(data.status){
                	Swal.fire({
	                    title: 'Success!',
	                    text: data.message,
	                    icon: 'success',
	                    confirmButtonText: 'Ok'
	                }).then((result) => {
	                  window.location.reload();
	                });
                }else{
                	Swal.fire({
	                    title: 'Error!',
	                    text: data.message,
	                    icon: 'error',
	                    confirmButtonText: 'Ok'
	                });
                }
            },
            error : function(data){
                stopLoader('.page-content');
                if(data.responseJSON){
                    var err_response = data.responseJSON;
                    if(err_response.errors==undefined && err_response.message) {
                        Swal.fire({
                            title: 'Error!',
                            text: err_response.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }

                    $.each(err_response.errors, function(i, obj){
                        $('#profile_form').find('input[name="'+i+'"]').next().text(obj);
                        $('#profile_form').find('input[name="'+i+'"]').addClass('is-invalid');
                    });
                }
            }
        });
	});
</script>
@endsection
