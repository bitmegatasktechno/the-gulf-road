<script type="text/javascript">
	const changeDefaultRole = function(event){
		var value = event.value;
		var ajax_url = WEBSITE_URL+"/change-default-role";
		var id = '1';

		$.ajax({
            url : ajax_url,
            type:'post',
            data : {
            	'id':id,
            	'value':value
            },
            headers: {
     			'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr("content")
     		},
            dataType : 'json',
            // processData: false,
            // contentType: false,
            beforeSend:function(){
                startLoader('.page-content');
            },
            complete:function(){
               stopLoader('.page-content'); 
            },
            success : function(data){
                if(data.status){
                	Swal.fire({
	                    title: 'Success!',
	                    text: data.message,
	                    icon: 'success',
	                    confirmButtonText: 'Ok'
	                }).then((result) => {
	                  window.location.reload();
	                });
                }else{
                	Swal.fire({
	                    title: 'Error!',
	                    text: data.message,
	                    icon: 'error',
	                    confirmButtonText: 'Ok'
	                }).then((result) => {
	                  window.location.reload();
	                });
                }
            },
            error : function(data){
                stopLoader('.page-content');
                if(data.responseJSON){
                    var err_response = data.responseJSON;  
                    if(err_response.errors==undefined && err_response.message) {
                        Swal.fire({
                            title: 'Error!',
                            text: err_response.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                }
            }
        });
	}

	const uploadProfileImage = function(){
		var ajax_url = WEBSITE_URL+"/upload-image";
		var data = new FormData($('#profile_pic_form')[0]);
		$('#profile_pic_form').find('.is-invalid').removeClass('is-invalid');
        $('#profile_pic_form').find('.invalid-feedback').text('');

		$.ajax({
            url : ajax_url,
            type:'post',
            data : data,
            dataType : 'json',
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            beforeSend:function(){
                startLoader('.page-content');
            },
            complete:function(){
               stopLoader('.page-content'); 
            },
            success : function(data){
                
                if(data.status){
                	Swal.fire({
	                    title: 'Success!',
	                    text: data.message,
	                    icon: 'success',
	                    confirmButtonText: 'Ok'
	                }).then((result) => {
	                  window.location.reload();
	                });
                }else{
                	Swal.fire({
	                    title: 'Error!',
	                    text: data.message,
	                    icon: 'error',
	                    confirmButtonText: 'Ok'
	                });
                }
            },
            error : function(data){
                stopLoader('.page-content');
                if(data.responseJSON){
                    var err_response = data.responseJSON;  
                    if(err_response.errors==undefined && err_response.message) {
                        Swal.fire({
                            title: 'Error!',
                            text: err_response.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                    $.each(err_response.errors, function(i, obj){
                        $('#profile_pic_form').find('input[name="'+i+'"]').next().next().text(obj).show();
                        $('#profile_pic_form').find('input[name="'+i+'"]').addClass('is-invalid');
                    });
                }
            }
        });
	}
</script>
