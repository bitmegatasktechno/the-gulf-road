@php
$lang=Request::segment(1);
$loc=Request::segment(5);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
@extends('frontend.layouts.home')

@section('title','Agent Profile-Step 2')


@section('content')

    <section class="registraion-process">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                     <h3>Complete Your Agent Profile <a class="float-right" href="{{url(app()->getLocale().'/profile/'.$link)}}">I'll do it later 
                    <img src="{{asset('img/Arrow.png')}}"></a></h3>
                    <p class="mb-5">Complete the Following form to create your Agent profile.</p>
                </div>
                @include('frontend.profile.requests.common.address-details')
            </div>
        </div>
    </section>
    @include('frontend.layouts.footer')
@endsection

@section('scripts')
    @include('frontend.profile.requests.common.address-js')
    <script type="text/javascript">

        $(document).ready(function(){

            $(document).on('click','.profile_step_submit', function(){
                var ajax_url = WEBSITE_URL+"/agent-profile/step/2/{{Request::segment(5)}}";
                var $form = $('#buddy_request_Form');
        
                var data = new FormData($form[0]);
                var gender = $('input[name="gender"]:checked').attr('value') ? $('input[name="gender"]:checked').attr('value'): '';
        
                data.append('gender', gender);
                $form.find('.is-invalid').removeClass('is-invalid');
                $form.find('.invalid-feedback').text('');
        
                $.ajax({
                    url : ajax_url,
                    type:'post',
                    data : data,
                    dataType : 'json',
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    beforeSend:function(){
                        startLoader('.registraion-process');
                    },
                    complete:function(){
                       stopLoader('.registraion-process'); 
                    },
                    success : function(data){
                        if(data.status){
                            Swal.fire({
                                title: 'Success!',
                                text: data.message,
                                icon: 'success',
                                confirmButtonText: 'Ok'
                            }).then((result) => {
                              window.location = data.url;
                            });
                        }else{
                            Swal.fire({
                                title: 'Error!',
                                text: data.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                    },
                    error : function(data){
                        stopLoader('.registraion-process');
                        if(data.responseJSON){
                            var err_response = data.responseJSON;  
                            if(err_response.errors==undefined && err_response.message) {
                                Swal.fire({
                                    title: 'Error!',
                                    text: err_response.message,
                                    icon: 'error',
                                    confirmButtonText: 'Ok'
                                });
                            }
                            $.each(err_response.errors, function(i, obj){
                                $form.find('span.invalid-feedback.'+i+'').text(obj).show();
        
                                $form.find('input[name="'+i+'"]').addClass('is-invalid');
                                $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
                                $form.find('select[name="'+i+'"]').addClass('is-invalid');
                            });
                        }
                    }
                });
            });
        });
    </script>
@endsection