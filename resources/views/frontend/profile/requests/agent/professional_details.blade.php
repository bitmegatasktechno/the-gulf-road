@php
$lang=Request::segment(1);
$loc=Request::segment(5);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
@extends('frontend.layouts.home')

@section('title','Agent Profile-Step 3')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('css/chosen.css')}}">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

@endsection
@section('content')

<section class="registraion-process">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                 <h3>Complete Your Agent Profile <a class="float-right" href="{{url(app()->getLocale().'/profile/'.$link)}}">I'll do it later 
                <img src="{{asset('img/Arrow.png')}}"></a></h3>
                <p class="mb-5">Complete the Following form to create your Agent profile.</p>
            </div>
            @include('frontend.profile.requests.common.professional-details',['role'=>'agent'])
        </div>
    </div>
</section>
@include('frontend.layouts.footer')
@endsection
@section('scripts')
<script type="text/javascript" src="{{asset('/js/chosen.jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/prism.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

<script type="text/javascript">

    function onChangeRealEstate(input){
        if(input.value=='yes'){
            $('.real_estate_yes_div').show();
        }else{
            $('.real_estate_yes_div').hide();
        }
    }
    $(document).ready(function(){
        $('.chosen-select').chosen();
        $('input.timepicker').timepicker({
            timeFormat: 'HH:mm',
        });

        $("#monday_start_time").timepicker({
            'change': function(){
                $("#monday_end_time").timepicker({'minTime': $(this).val()});
            }
        }); 

        $('.chosen-select-deselect').chosen({ allow_single_deselect: true});

        $(document).on('click','.profile_step_submit', function(){
            var ajax_url = WEBSITE_URL+"/agent-profile/step/3/{{Request::segment(5)}}";
            var $form = $('#buddy_request_Form');
    
            var data = new FormData($form[0]);
            
            data.append('monday', $('input[name="monday"]:checked').length);
            data.append('tuesday', $('input[name="tuesday"]:checked').length);
            data.append('wednesday', $('input[name="wednesday"]:checked').length);
            data.append('thursday', $('input[name="thursday"]:checked').length);
            data.append('friday', $('input[name="friday"]:checked').length);
            data.append('saturday', $('input[name="saturday"]:checked').length);
            data.append('sunday', $('input[name="sunday"]:checked').length);

            $form.find('.is-invalid').removeClass('is-invalid');
            $form.find('.invalid-feedback').text('');
    
            $.ajax({
                url : ajax_url,
                type:'post',
                data : data,
                dataType : 'json',
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.registraion-process');
                },
                complete:function(){
                   stopLoader('.registraion-process'); 
                },
                success : function(data){
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                          window.location = data.url;
                        });
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                },
                error : function(data){
                    stopLoader('.registraion-process');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                        $.each(err_response.errors, function(i, obj){
                            $form.find('span.invalid-feedback.'+i+'').text(obj).show();
    
                            $form.find('input[name="'+i+'"]').addClass('is-invalid');
                            $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
                            $form.find('select[name="'+i+'"]').addClass('is-invalid');
                        });
                    }
                }
            });
        });
    });
</script>
@endsection