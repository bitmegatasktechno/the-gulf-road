@php
$lang=Request::segment(1);
$loc=Request::segment(5);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
@extends('frontend.layouts.home')
@section('title','Agent Profile-Step 1')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/datepicker.css')}}">
    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/css/intlTelInput.css" rel="stylesheet" media="screen">
    <style>
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
.form-control:disabled, .form-control[readonly] {
    background-color: #f5f5f5;
    opacity: 1;
}
</style>

@endsection

@section('content')
    <section class="registraion-process">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>Complete your agent profile <a class="float-right" href="{{url(app()->getLocale().'/profile/'.$link)}}">I'll do it later 
                        <img src="{{asset('img/Arrow.png')}}"></a></h3>
                    <p class="mb-5">Complete the following form to create your agent profile.</p>
                </div>
                <div class="col-md-4">
                    <div class="regst-blk p-3">
                        <small>Profile completion <span class="float-right theme-color">0%</span></small>
                        <div class="progress-line mt-2"></div>
                        <ul class="list-inline mt-4">
                            <li class="lgt-line active">
                                <h4><strong></strong>Personal details</h4>
                                <p class="normal-line">Fill your personal info and identification details here.</p>
                            </li>
                            <li class="lgt-line">
                                <h4><strong></strong>Address</h4>
                                <p class="normal-line">Fill info about your current address and native country address here.</p>
                            </li>
                            <li class="lgt-line">
                                <h4><strong></strong>Professional details</h4>
                                <p class="normal-line">Fill info about your current job domain, skills & availability.</p>
                            </li>
                            <li class="lgt-line">
                                <h4><strong></strong>Upload documents</h4>
                                <p class="normal-line">Upload civil ID/ emirates ID/ passport or any national identification document.</p>
                            </li>
                            <li class="lgt-line">
                                <h4><strong></strong>Qualifications & Hobbies</h4>
                                <p class="">Fill info about your educational qualifications and hobbies.</p>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-8">
                    <form method="post" name="profileForm" id="profileForm">
                        @csrf

                        @include('frontend.profile.requests.common.personal-details')
                    </form>
                </div>
            </div>
        </div>
    </section>
    @include('frontend.layouts.footer')
@endsection

@section('scripts')
    <script type="text/javascript" src="{{asset('js/datepicker.js')}}"></script>
    @include('frontend.profile.requests.common.personal-js')
    <script type="text/javascript">

        $(document).ready(function(){

            $(document).on('click','.profile_step_submit', function(){

                $('#country_code').val($('.selected-dial-code').html());
                
                var ajax_url = WEBSITE_URL+"/agent-profile/step/1/{{Request::segment(5)}}";
                var $form = $('#profileForm');

                var data = new FormData($form[0]);
                var gender = $('input[name="gender"]:checked').attr('value') ? $('input[name="gender"]:checked').attr('value'): '';

                data.append('gender', gender);
                $form.find('.is-invalid').removeClass('is-invalid');
                $form.find('.invalid-feedback').text('');

                $.ajax({
                    url : ajax_url,
                    type:'post',
                    data : data,
                    dataType : 'json',
                    enctype: 'multipart/form-data',
                    processData: false,
                    contentType: false,
                    beforeSend:function(){
                        startLoader('.registraion-process');
                    },
                    complete:function(){
                       stopLoader('.registraion-process'); 
                    },
                    success : function(data){
                        if(data.status){
                            Swal.fire({
                                title: 'Success!',
                                text: data.message,
                                icon: 'success',
                                confirmButtonText: 'Ok'
                            }).then((result) => {
                              window.location = data.url;
                            });
                        }else{
                            Swal.fire({
                                title: 'Error!',
                                text: data.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                    },
                    error : function(data){
                        stopLoader('.registraion-process');
                        if(data.responseJSON){
                            var err_response = data.responseJSON;  
                            if(err_response.errors==undefined && err_response.message) {
                                Swal.fire({
                                    title: 'Error!',
                                    text: err_response.message,
                                    icon: 'error',
                                    confirmButtonText: 'Ok'
                                });
                            }
                            $.each(err_response.errors, function(i, obj){
                                $form.find('span.invalid-feedback.'+i+'').text(obj).show();

                                $form.find('input[name="'+i+'"]').addClass('is-invalid');
                                $form.find('textarea[name="'+i+'"]').addClass('is-invalid');
                                $form.find('select[name="'+i+'"]').addClass('is-invalid');
                            });
                        }
                    }
                });
            });
        });
    </script>
@endsection