@php
$lang=Request::segment(1);
$loc=Request::segment(5);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
@extends('frontend.layouts.home')
@section('title','Agent Profile-Step 4')
@section('content')
<section class="registraion-process">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                 <h3>Complete Your Agent Profile <a class="float-right" href="{{url(app()->getLocale().'/profile/'.$link)}}">I'll do it later 
                <img src="{{asset('img/Arrow.png')}}"></a></h3>
                <p class="mb-5">Complete the following form to create your agent profile.</p>
            </div>
            <div class="col-md-4">
                <div class="regst-blk p-3">
                    <small>Profile completion <span class="float-right theme-color">60%</span></small>
                    <div class="progress-line fourth mt-2"></div>
                    <ul class="list-inline mt-4">
                        <li class="lgt-line active">
                            <h4><strong></strong>Personal details</h4>
                            <p class="normal-line active">Fill your personal info and identification details here.</p>
                        </li>
                        <li class="lgt-line active">
                            <h4><strong></strong>Address</h4>
                            <p class="normal-line active">Fill info about your current address and native country address here.</p>
                        </li>
                        <li class="lgt-line active">
                            <h4><strong></strong>Professional details</h4>
                            <p class="normal-line active">Fill info about your current job domain, skills & availability.</p>
                        </li>
                        <li class="lgt-line active">
                            <h4><strong></strong>Upload documents</h4>
                            <p class="normal-line ">Upload civil ID/ emirates ID/ passport or any national identification document.</p>
                        </li>
                        <li class="lgt-line">
                            <h4><strong></strong>Qualifications & Hobbies</h4>
                            <p class="">Fill info about your educational qualifications and hobbies.</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-8">
                <div class="buddy-regst-form upload-doc p-4">
                    <h4 class="mb-3">Upload documents</h4>
                    <p class="">Upload civil ID/ emirates ID/ passport or any national identification document. </p>
                        <div class="errors_ul">
                                    
                        </div>
                        <div class="col-md-5">
                            <form method="post" name="profileForm" id="file-upload-form" class="profileForm uploader mt-5" enctype="multipart/form-data">
                                @csrf
                                <input id="file-upload" type="file" multiple="" name="documents[]" onchange="readURL(this)">
                                <span class="invalid-feedback documents"></span>
                                
                                <label for="file-upload" id="file-drag">
                                    <img id="file-image" src="#" alt="Preview" class="hidden">
                                    <div id="start">
                                        <div class="col-md-12">
                                            <img src="{{asset('img/ic_share@2x.png')}}">
                                        </div>
                                        <div id="notimage" class="hidden">Please select an image</div>
                                        <span id="file-upload-btn" class="">Upload document</span>
                                    </div>
                                </label>
                                <div class="image_prev">
                                   <!--  <ul class="image_prev_ul">
                                        
                                    </ul> -->
                                </div>
                            </form>
                        </div>
                        <div class=" row image_prev_ul">
                                        
                                    </div>
                        <div class="col-md-12 text-right">
                            <button type="button" class="profile_step_submit red-btn rounded pt-3 pb-3 mt-5 mb-3"><span>Save & Continue</span></button>
                        </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
@include('frontend.layouts.footer')
@endsection
@section('scripts')
<script type="text/javascript">   
    
    // function readURL(input) {
    //     if (input.files && input.files[0]) {
    //         if(input.files.length>5){
    //             Swal.fire({
    //                 title: 'Error!',
    //                 text: 'You can select maximum 5 docs.',
    //                 icon: 'error',
    //                 confirmButtonText: 'Ok'
    //             });
    //             return false;
    //         }
    //         var html = '';
    //         console.log(input.files)
    //         $(input.files ).each(function( index,val ) {
    //           html+='<li>'+val.name+'<span class="invalid-feedback documents.'+index+'"><span></li>';
    //         });
    //         $('.image_prev_ul').html(html);
    //     }
    // } 
     function readURL(input) {
      var total_file=document.getElementById("file-upload").files.length;
      if(total_file > 5)
      {
        Swal.fire({
            title: 'Error!',
            text: 'You can select maximum 5 docs.',
            icon: 'error',
            confirmButtonText: 'Ok'
        });
        $('#file-upload').val('');
        return false;
      }
      var files = document.getElementById("file-upload").files;
      $('.image_prev_ul').empty();
      for(var i=0;i<total_file;i++)
       {
        $('.image_prev_ul').append("<div  class='col-2'><img style='width:100px; height:100px'  class='rounded' src='"+URL.createObjectURL(event.target.files[i])+"'></div>");
       }
    }

    $(document).ready(function(){
        
        $(document).on('click','.profile_step_submit', function(){
            var ajax_url = WEBSITE_URL+"/agent-profile/step/4/{{Request::segment(5)}}";
            var $form = $('.profileForm');
    
            var data = new FormData($form[0]);
            $form.find('.is-invalid').removeClass('is-invalid');
            $form.find('.invalid-feedback').text('');
    
            $.ajax({
                url : ajax_url,
                type:'post',
                data : data,
                dataType : 'json',
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.registraion-process');
                },
                complete:function(){
                   stopLoader('.registraion-process'); 
                },
                success : function(data){
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                          window.location = data.url;
                        });
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                },
                error : function(data){
                    stopLoader('.registraion-process');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }else{
                            Swal.fire({
                                title: 'Error!',
                                text: 'You can Upload documents only with these extension : doc,pdf,docx,zip,jpeg,jpg,JPG,JPEG,png. And the maximum size should be less than 2MB',
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                        var html='';
                        $.each(err_response.errors, function(i, obj){
                            html+='<li><span class="invalid-feedback '+i+'">'+obj+'<span></li>';
                            
                        });
                        $('.errors_ul').html(html);
                        $(document).find('span.invalid-feedback').show();

                    }
                }
            });
        });
    });
</script>
@endsection