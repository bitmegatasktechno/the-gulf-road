@php
$lang=Request::segment(1);
$loc=Request::segment(3);
@endphp
@extends('frontend.layouts.home')
@section('title','Stripe Payment Property') 
@section('content')
@include('frontend.layouts.default-header')

@if(@$_GET['redirect_status'] =='succeeded')
 
<script>
    opener.transaction_subscr_after_stripe();
    window.close();
</script>

<div>
    <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Pay with Stripe Successful </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="window.close()">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img src="https://c.tenor.com/xVfFIHxAzW4AAAAC/success.gif" style="width:100%;">
            </div>
        </div>
    </div>
 @endif

@include('frontend.layouts.footer')
 
@endsection
     
  
 
    