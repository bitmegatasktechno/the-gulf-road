<script type="text/javascript">

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#profile_src').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    function whatsappNumberRadio(input){
        if(input.value=='yes'){
            $('.whatsapp_number_div').hide();
        }
        else if(input.value=='no'){
            $('.whatsapp_number_div').show();
        }
    }

    function drivingLicenceRadio(input){
        if(input.value=='no'){
            $('.driving_licence_div').hide();
        }
        else if(input.value=='yes'){
            $('.driving_licence_div').show();
        }
    }

    $(document).ready(function(){
        
        var todayDate = new Date();
        todayDate.setFullYear(todayDate.getFullYear() - 1);

        $('#dob').datepicker({
            endDate: todayDate,
            format: 'dd/mm/yyyy',
            autoHide:true
        });

        $('#working_since').datepicker({
            endDate: new Date(),
            format: 'yyyy',
            autoHide:true
        });
    });
</script>