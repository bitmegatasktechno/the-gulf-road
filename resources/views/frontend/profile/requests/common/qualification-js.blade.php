<script type="text/javascript">

    function addQualification(){
        if($('.qualifications_div').length>10){
            return false;
        }
        $('.qualifications_append_div').append('<div class="row qualifications_div mt-2"><div class="col-md-5">\
                                        <label>Course/Degree Name</label>\
                                        <input class="form-control" type="text" name="course_name[]">\
                                        <span class="error invalid-feedback course_name"></span>\
                                    </div>\
                                    <div class="col-md-5">\
                                        <label>University</label>\
                                        <input class="form-control" type="text" name="university_name[]">\
                                        <span class="error invalid-feedback university_name"></span>\
                                    </div><div class="col-md-2">\
                                    <label>&nbsp; </label>\
                                    <button type="button" class="light-grey-btn btn-danger delete_div">Delete</button>\
                                </div></div>');
    }

    function addHobby(){
        if($('.hobby_div').length>10){
            return false;
        }
        $('.hobby_append_div').append('<div class="row mt-2 hobby_div"><div class="col-md-6">\
                                <label>Hobby</label>\
                                <input class="form-control" type="text" name="hobby[]">\
                                <span class="error invalid-feedback hobby"></span>\
                            </div><div class="col-md-2">\
                                    <label>&nbsp; </label>\
                                    <button type="button" class="light-grey-btn btn-danger delete_div">Delete</button>\
                                </div></div>');
    }


    $(document).ready(function(){
        
        $(document).on('click','.delete_div', function(e){
            $(this).parent().parent().remove();
        });

        $(document).on('click','.profile_step_submit', function(){
            var ajax_url = WEBSITE_URL+"/{{$path}}/step/5/{{Request::segment(5)}}";
            var $form = $('#profileForm');
    		$form.find('.is-invalid').removeClass('is-invalid');
            $form.find('.invalid-feedback').text('');

            var data = new FormData();
            data.append('_token', "{{ csrf_token() }}");
            var course_name = document.getElementsByName('course_name[]');
            for (var i = 0; i <course_name.length; i++) {
                var inp=course_name[i];
                data.append('course_name[]', inp.value);
            }

            var university_name = document.getElementsByName('university_name[]');
            for (var i = 0; i <university_name.length; i++) {
                var unr=university_name[i];
                data.append('university_name[]', unr.value);
            }

            var hobby = document.getElementsByName('hobby[]');
            for (var i = 0; i <hobby.length; i++) {
                var hby=hobby[i];
                data.append('hobby[]', hby.value);
            }
    
            $.ajax({
                url : ajax_url,
                type:'post',
                data : data,
                dataType : 'json',
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.registraion-process');
                },
                complete:function(){
                   stopLoader('.registraion-process'); 
                },
                success : function(data){
                    if(data.status){
                        Swal.fire({
                            title: 'Success!',
                            text: data.message,
                            icon: 'success',
                            confirmButtonText: 'Ok'
                        }).then((result) => {
                          window.location = data.url;
                        });
                    }else{
                        Swal.fire({
                            title: 'Error!',
                            text: data.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }
                },
                error : function(data){
                    stopLoader('.registraion-process');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                        $.each(err_response.errors, function(key, value){
                            if(key.indexOf('.') != -1) {
                                let keys = key.split('.');                                
                                $('input[name="'+keys[0]+'[]"]').eq(keys[1]).addClass('is-invalid').next('span').text(value).show();                         
                            }
                            else {
                                $('input[name="'+key+'[]"]').eq(0).addClass('is-invalid').next('span').text(value).show();
                            }
                        });
                    }
                }
            });
        });
    });
</script>
