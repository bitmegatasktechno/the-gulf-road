<div class="col-md-4">
    <div class="regst-blk p-3">
        <small>Profile Completion <span class="float-right theme-color">80%</span></small>
        <div class="progress-line fifth mt-2"></div>
        <ul class="list-inline mt-4">

            <li class="lgt-line active">
                <h4><strong></strong>Personal details</h4>
                <p class="normal-line active">Fill your personal info and identification details here.</p>
            </li>
            <li class="lgt-line active">
                <h4><strong></strong>Address</h4>
                <p class="normal-line active">Fill info about your current address and native country address here.</p>
            </li>
            <li class="lgt-line active">
                <h4><strong></strong>Professional details</h4>
                <p class="normal-line active">Fill info about your current job domain, skills & availability.</p>
            </li>
            <li class="lgt-line active">
                <h4><strong></strong>Upload documents</h4>
                <p class="normal-line active">Upload civil ID/ emirates ID/ passport or any national identification document.</p>
            </li>
            <li class="lgt-line active">
                <h4><strong></strong>Qualifications & Hobbies</h4>
                <p class="">Fill info about your educational qualifications and hobbies.</p>
            </li>
        </ul>
    </div>
</div>
<div class="col-md-8">
    <div class="buddy-regst-form address p-4">
        <h4 class="mb-4">Qualifications & Hobbies</h4>
        
        <div class="row">
            <div class="col-md-12">
                <button type="button" class=" light-grey-btn btn-danger" onclick="addQualification()">+ Add Qualifications</button>
            </div>
        </div>
        
        <form method="post" name="profileForm" id="profileForm">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="row">

                <div class="col-md-12 qualifications_append_div">
                    
                    @if(@$qualifications->education!=null)
                        @foreach(@$qualifications->education as $row)
                            
                                <div class="row qualifications_div mt-2">
                                    <div class="col-md-5 course_name">
                                        <label>Course/Degree Name</label>
                                        <input class="form-control" type="text" name="course_name[]" value="{{$row['course_name']}}">
                                        <span class="error invalid-feedback course_name"></span>
                                    </div>
                                    <div class="col-md-5 university_name">
                                        <label>University</label>
                                        <input class="form-control" type="text" name="university_name[]" value="{{$row['university_name']}}">
                                        <span class="error invalid-feedback university_name"></span>
                                    </div>
                                    <div class="col-md-2">
                                        <label>&nbsp; </label>
                                        <button type="button" class="light-grey-btn btn-danger delete_div">Delete</button>
                                    </div>
                                </div>
                            
                        @endforeach
                    @endif
                </div>
            </div>

            <div class="row mt-2">
                <div class="col-md-12">
                    <button type="button" class=" light-grey-btn btn-danger" onclick="addHobby()">+ Add Hobbies</button>
                </div>
            </div>

           <div class="row">
                <div class="col-md-12 hobby_append_div">
                    @if(@$qualifications->hobbies!=null)
                        @foreach(@$qualifications->hobbies as $row)
                        
                            <div class="row mt-2 hobby_div">
                               <div class="col-md-6">
                                    <label>Hobby</label>
                                    <input class="form-control" type="text" name="hobby[]" value="{{$row}}">
                                    <span class="error invalid-feedback hobby"></span>
                                </div>
                                <div class="col-md-2">
                                    <label>&nbsp; </label>
                                    <button type="button" class="light-grey-btn btn-danger delete_div">Delete</button>
                                </div>
                            </div>
                           
                        @endforeach
                    @endif
                   
                </div>
            </div>
            <div class="col-md-12 text-right">
                <button type="button" class="red-btn rounded pt-3 pb-3 mt-3 mb-3 profile_step_submit"><span>Save & Continue</span></button>
            </div>
        </form>
    </div>
</div>
