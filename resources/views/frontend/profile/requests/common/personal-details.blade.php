<!--<div class="uplod-doc p-3 mb-3 parent_div">
    <h4 class="mb-3">Upload a Picture of yours</h4>
    <img src="{{asset('img/profile-avta.png')}}" id="profile_src" style="max-height: 50px;border-radius: 50%">
    <button type="button" class="img-upload-btn ml-3">
        <label for="upload" class="file-upload__label mb-0">
            <img class="mr-2" src="{{asset('img/upload.png')}}">
            Upload
        </label>
        <input id="upload" class="file-upload__input" type="file" name="profile_pic" onchange="readURL(this)">
        <br>
        <span class="invalid-feedback profile_pic"></span>
    </button>
</div> -->
<style type="text/css">
     .intl-tel-input.allow-dropdown.separate-dial-code {
     display: block!important; 
}
</style>
<div class="buddy-regst-form p-3">
    <h4 class="mb-3">Personal details</h4>
    <p>Select Gender</p>
    <div class="btn-grp-select parent_div">
        <span class="male-btnfemale-btn">
            <input type="radio" name="gender" id="size_1" value="male" @if(@$user->gender=='male') checked @endif/>
            <label for="size_1">Male</label>
        </span>
        <span class="male-btnfemale-btn">
            <input type="radio" name="gender" id="size_2" value="female" @if(@$user->gender=='female') checked @endif/>
            <label for="size_2">Female</label>
        </span>
        <span class="male-btnfemale-btn">
            <input type="radio" name="gender" id="size_3" value="other" @if(@$user->gender=='other') checked @endif/>
            <label for="size_3">Preferred not to say</label>
        </span>
        <br>
        <span class="invalid-feedback gender"></span>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label>Your Name</label>
        </div>
        <div class="col-md-4 parent_div">
            <input class="form-control" type="text" name="first_name" placeholder="Enter First Name" value="{{@$user->first_name??@$user->name}}">
             <span class="invalid-feedback first_name"></span>
        </div>
        <div class="col-md-4 parent_div">
            <input class="form-control" type="text" name="middle_name" placeholder="Enter Middle Name" value="{{@$user->middle_name}}">
             <span class="invalid-feedback middle_name"></span>
        </div>
        <div class="col-md-4 parent_div">
            <input class="form-control" type="text" name="last_name" placeholder="Enter Last Name" value="{{@$user->last_name}}">
            <span class="invalid-feedback last_name"></span>
        </div>

        <div class="col-md-4 parent_div">
            <label>Father's Name</label>
            <input class="form-control" type="text" name="father_name" placeholder="Enter Father's Name" value="{{@$user->father_name}}">
            <span class="invalid-feedback father_name"></span>
        </div>

        <div class="col-md-8 parent_div">
            <label>Country Code</label>
            <div class="row">
                <div class="col-md-4" style="padding-right: 0px!important; ">
                    <select class="form-control rounded" name="country_code">
                    @foreach(getAllCodeWithName() as $row)
                        <option value="{{ $row['dial_code'] }}">{{$row['name']}} {{$row['dial_code']}}</option>
                    @endforeach
                    </select>
                    <span class="invalid-feedback country_code"></span>
                </div>
                <div class="col=md-8">
                    <input class="form-control " type="number" name="phone" placeholder="Enter Phone Number" value="{{@$user->phone}}" minlength="6" maxlength="15" required="">
            <span class="invalid-feedback"></span>
                </div>
            </div>
        <!-- </div>  -->
        <!-- <div class="col-md-4 parent_div"> -->
            <!-- <label>Phone Number</label> -->
            
        </div>
        <div class="col-md-4"></div>

        <div class="col-md-12"><label>Whatsapp Number</label></div> 
        
        <div class="col-md-5">
            <div class="radio">
                <input id="radio-1" name="whatsapp_number_same" type="radio" value="yes" @if(@$userData->whatsapp_number_same=='yes') checked @endif onchange="whatsappNumberRadio(this)">
                <label for="radio-1" class="radio-label">Same as Phone Number</label>
            </div>
        </div>
        <div class="col-md-7">
            <div class="radio">
                <input id="radio-2" name="whatsapp_number_same" value="no" @if(@$userData->whatsapp_number_same=='no') checked @endif type="radio" onchange="whatsappNumberRadio(this)">
                <label  for="radio-2" class="radio-label">I have a Different Whatsapp Number</label>
            </div>
        </div>
        <span class="invalid-feedback whatsapp_number_same"></span>
        @php 
            $whatsapp_div ='none';
        @endphp

        @if(@$userData->whatsapp_number_same=='no')
            @php 
                $whatsapp_div ='block';
            @endphp
        @else
            @php 
                $whatsapp_div ='none';
            @endphp
        @endif
        <div class="col-md-12 whatsapp_number_div" style="display: {{$whatsapp_div}};">
             <div class="col-md-6 parent_div pl-0">
                <label>Whatsapp Number</label>
                <input class="form-control phone" type="number" name="whatsapp_number" minlength="6" maxlength="15" placeholder="Enter Whatsapp Number" value="{{@$userData->whatsapp_number}}">
                <span class="invalid-feedback whatsapp_number"></span>
            </div><br>
        </div>

        <div class="col-md-4 parent_div">
            <label>Nationality</label>
            <select class="form-control rounded" name="nationality">
                <option value="">Select Nationality</option>
                @foreach(getAllCodesWithName() as $row)
                    <option value="{{$row['name']}}" @if(@$user->country==$row['name']) selected @endif>{{$row['name']}}</option>
                @endforeach
            </select>
            <span class="invalid-feedback nationality"></span>
        </div>

        <div class="col-md-4 parent_div">
            <label>Marital Status</label>
            <select class="form-control rounded" name="marriage_status">
                <option value="">Select Marital Status</option>
                <option value="married" @if(@$userData->marriage_status=='married') selected @endif>Married</option>
                <option value="single" @if(@$userData->marriage_status=='single') selected @endif>Single</option>
                <option value="divorced" @if(@$userData->marriage_status=='divorced') selected @endif>Divorced</option>
                <option value="widowed" @if(@$userData->marriage_status=='widowed') selected @endif>Widowed</option>
            </select>
            <span class="invalid-feedback marriage_status"></span>
        </div>

        <div class="col-md-4 parent_div">
            <label>Date Of Birth</label>
            <input class=" form-control" id="dob" type="text" name="date_of_birth" placeholder="dd/mm/yyyy" value="{{@$user->date_of_birth}}" readonly>
            <span class="invalid-feedback date_of_birth"></span>
        </div>

        <div class="col-md-4 parent_div">
            <label>Preffered Language</label>
            <select class="form-control rounded" name="preffered_language">
                <option value="">Select Language</option>
                @foreach(getAllLanguage() as $row)
                    <option value="{{ $row['name'] }}"  @if(@$userData->preffered_language==$row['name']) selected @endif>{{ __(ucfirst($row['name'])) }}</option>
                @endforeach
            </select>
            <span class="invalid-feedback preffered_language"></span>
        </div>

        <div class="col-md-4 parent_div">
            <label>Preffered Currency</label>
            <select class="form-control rounded" name="preffered_currency">
                @foreach(getAllCurrencies() as $row)
                    <option value="{{ $row->name }}"  @if(@$userData->preffered_currency==$row->name) selected @endif>{{ucfirst($row->name)}}</option>
                @endforeach
            </select>
            <span class="invalid-feedback preffered_currency"></span>
        </div>

        <div class="col-md-4 parent_div">
            <label>Working In Region Since?</label>
            <input class=" form-control" id="working_since" type="text" name="working_since" placeholder="yyyy" value="{{@$userData->working_since}}" readonly>
            <span class="invalid-feedback working_since"></span>
        </div>

        <div class="col-md-6 parent_div">
            <label>Residence ID Number</label>
            <input class="form-control" type="text" name="residence_id_number" placeholder="Enter Residence ID Number" value="{{@$userData->residence_id_number}}">
            <span class="invalid-feedback residence_id_number"></span>
        </div>
        <div class="col-md-6 parent_div">
            <label>Passport Number</label>
            <input class="form-control" type="text" name="passport_number" placeholder="Enter Passport Number" value="{{@$userData->passport_number}}">
            <span class="invalid-feedback passport_number"></span>
        </div>

        <div class="col-md-12">
            <div class="lic-y-n parent_div">
                <p>Do You Have a Driving Licence ?</p>
                <div class="row">
                    <div class="col-md-2">
                        <label class="servicesChkBx ">
                        <input type="radio" autocomplete="off" value="yes" @if(@$userData->driving_licence=='yes') checked @endif name="driving_licence" onchange="drivingLicenceRadio(this)">
                        <span class="checkmark">
                        Yes
                        </span>
                        </label>
                    </div>
                    <div class="col-md-2">
                        <label class="servicesChkBx ">
                        <input type="radio" autocomplete="off" @if(@$userData->driving_licence=='no') checked @endif  value="no" name="driving_licence" onchange="drivingLicenceRadio(this)">
                        <span class="checkmark">
                        No
                        </span>
                        </label>
                    </div>
                    <span class="invalid-feedback driving_licence"></span>
                </div>
            </div>
        </div>
        @php 
            $licence_div ='none';
        @endphp

        @if(@$userData->driving_licence=='yes')
            @php 
                $licence_div ='block';
            @endphp
        @else
            @php 
                $licence_div ='none';
            @endphp
        @endif
        <div class="col-md-6 parent_div driving_licence_div mt-4" style="display: {{$licence_div}};">
            <label>Licence Number</label>
            <input class="form-control" type="" name="licence_number" placeholder="Enter Licence Number" value="{{@$userData->licence_number}}">
            <span class="invalid-feedback licence_number"></span>
        </div>

        <div class="col-md-12 text-right">
            <button type="button" class="red-btn rounded pt-3 pb-3 mt-3 mb-3 profile_step_submit">
                <span>Save & Continue</span>
            </button>
        </div>
    </div>
</div>
