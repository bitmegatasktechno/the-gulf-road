<script type="text/javascript">

    function setAddress(input){
        if(input.checked){
            $('input[name="native_location"]').val($('input[name="current_location"]').val());
            $('input[name="native_unit_number"]').val($('input[name="current_unit_number"]').val());
            $('input[name="native_address_1"]').val($('input[name="current_address_1"]').val());
            $('input[name="native_address_2"]').val($('input[name="current_address_2"]').val());
            $('input[name="native_landmark"]').val($('input[name="current_landmark"]').val());
            $('input[name="native_city"]').val($('input[name="current_city"]').val());
            $('select[name="native_country"]').val($('select[name="current_country"]').val());
            $('select[name="native_state"]').html('')
            $('#current_state option').clone().appendTo('#native_state');
            $('select[name="native_state"]').val($('select[name="current_state"]').val());
            $('input[name="native_zipcode"]').val($('input[name="current_zipcode"]').val());
        }else{
            $('input[name="native_location"]').val('');
            $('input[name="native_unit_number"]').val('');
            $('input[name="native_address_1"]').val('');
            $('input[name="native_address_2"]').val('');
            $('input[name="native_landmark"]').val('');
            $('input[name="native_city"]').val('');
            $('select[name="native_country"]').val('');
            $('select[name="native_state"]').val('');
            $('input[name="native_zipcode"]').val('');
        }
    }

    $(document).ready(function(){
        
        $(document).on('change','.country_change', function(e){
            e.preventDefault();
            var country = $(this).val();
            var id = $(this).data('change');
            if(!country){
                return false;
            }
            var ajax_url = WEBSITE_URL+"/states/"+country;    
            $.ajax({
                url : ajax_url,
                type:'get',
                dataType : 'json',
                processData: false,
                contentType: false,
                beforeSend:function(){
                    startLoader('.registraion-process');
                },
                complete:function(){
                   stopLoader('.registraion-process'); 
                },
                success : function(data){
                    var html='<option value="">Select State</option>';
                        
                    if(data.states){
                        $.each(data.states, function(key, val){
                            html+='<option value="'+val+'">'+val+'</option>';
                        });
                    }
                    $('#'+id).html(html);
                },
                error : function(data){
                    stopLoader('.registraion-process');
                    if(data.responseJSON){
                        var err_response = data.responseJSON;  
                        if(err_response.errors==undefined && err_response.message) {
                            Swal.fire({
                                title: 'Error!',
                                text: err_response.message,
                                icon: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                    }
                }
            });
        });
    });
</script>