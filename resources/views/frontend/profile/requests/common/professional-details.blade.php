<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">
<style type="text/css">
.label-info{
color: white;
background: #E4002B;
padding: 1px 5px 2px 5px;
}
</style>
<div class="col-md-4">
    <div class="regst-blk p-3">
        <small>Profile Completion <span class="float-right theme-color">40%</span></small>
        <div class="progress-line third mt-2"></div>
        <ul class="list-inline mt-4">
            <li class="lgt-line active">
                <h4><strong></strong>Personal details</h4>
                <p class="normal-line active">Fill your personal info and identification details here.</p>
            </li>
            <li class="lgt-line active">
                <h4><strong></strong>Address</h4>
                <p class="normal-line active">Fill info about your current address and native country address here.</p>
            </li>
            <li class="lgt-line active">
                <h4><strong></strong>Professional details</h4>
                <p class="normal-line">Fill info about your current job domain, skills & availability.</p>
            </li>
            <li class="lgt-line">
                <h4><strong></strong>Upload documents</h4>
                <p class="normal-line">Upload civil ID/ emirates ID/ passport or any national identification document.</p>
            </li>
            <li class="lgt-line">
                <h4><strong></strong>Qualifications & Hobbies</h4>
                <p class="">Fill info about your educational qualifications and hobbies.</p>
            </li>
        </ul>
    </div>
</div>
<div class="col-md-8">
    <div class="buddy-regst-form address p-4">
        <h4 class="mb-4">Professional details</h4>
        <p class="dark-color">Add languages you can speak or write or do both.</p>
        <form method="post" name="buddy_request_Form" id="buddy_request_Form">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <select data-placeholder="Choose a language..." multiple class="chosen-select form-control" name="languages[]">
                        @foreach(getAllLanguage() as $row)
                            <option value="{{ $row['name'] }}">{{ __(ucfirst($row['name'])) }}</option>
                        @endforeach
                    </select>
                     <span class="invalid-feedback languages"></span>
                </div>

                <div class="col-md-12">
                    <div class="lic-y-n">
                        <div class="row">
                            <div class="col-md-12">
                                <label class="mt-5"><p class="dark-color">Real Estate Agent.</p></label>
                            </div>
                            <div class="col-md-2" style="margin-top: -32px;">
                                <label class="servicesChkBx mt-0">
                                    <input type="radio" autocomplete="off" name="real_estate_agent" value="yes" onchange="onChangeRealEstate(this)">
                                    <span class="checkmark">Yes</span>
                                </label>
                            </div>
                            <div class="col-md-2" style="margin-top: -32px;">
                                <label class="servicesChkBx mt-0">
                                    <input type="radio" autocomplete="off" name="real_estate_agent" value="no" onchange="onChangeRealEstate(this)" checked="">
                                    <span class="checkmark">No</span>
                                </label>
                            </div>
                            <span class="invalid-feedback real_estate_agent"></span>
                        </div>
                    </div>
                </div>

                <div class="row col-md-12 real_estate_yes_div" style="display: none;">
                    <div class="col-md-5 mt-4">
                        <div class="radio">
                            <input id="radio-1" name="deal_property_in_uae" type="radio" checked="" value="inside" onchange="onChangeDealProperty(this)">
                            <label for="radio-1" class="radio-label">I Deal properties in UAE.</label>
                        </div>
                    </div>
                    <div class="col-md-7 mt-4">
                        <div class="radio">
                            <input id="radio-2" name="deal_property_in_uae" type="radio" value="outside" onchange="onChangeDealProperty(this)">
                            <label for="radio-2" class="radio-label">I Deal in properties outside UAE.</label>
                        </div>
                    </div>
                    <span class="invalid-feedback deal_property_in_uae"></span>
                    @if($role=='agent')
                        <div class="col-md-6 rera_number_div">
                            <label class="mt-3">RERA Number<small>(UAE Properties Only)</small></label>
                            <input class="form-control" type="text" name="rera_number" placeholder="Enter RERA Number">
                            <span class="invalid-feedback rera_number"></span>
                        </div>
                    @endif
                    <div class="col-md-6">
                        <label class="mt-3">Permit Number</label>
                        <input class="form-control" type="text" name="permit_number" placeholder="Enter Permit Number">
                        <span class="invalid-feedback permit_number"></span>
                    </div>
                    <div class="col-md-6 other_countries_div" style="display: none;">
                        <label class="mt-3">Other Countries</label>
                        <select data-placeholder="Choose Other Countries..." multiple class="chosen-select form-control" name="other_countries[]">
                        @foreach($localities as $row)
                            @if($row == "UAE")
                            @continue;
                            @endif
                            <option value="{{ $row }}">{{ __($row) }}</option>
                        @endforeach
                    </select>
                     <span class="invalid-feedback other_countries"></span>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="lic-y-n">
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="mt-5"><p class="dark-color">Charges</p></label>
                            </div>
                            <div class="col-sm-3">
                                <label class="mt-5"><p class="dark-color">Currency</p></label>
                            </div>
                            <div class="col-sm-3">
                                <label class="mt-5"><p class="dark-color">Type</p></label>
                            </div>
                        </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="number" id="charges" class='form-control' min="0" name="charges" required>
                                    <span class="invalid-feedback charges"></span>
                                </div>
                                <div class="col-md-3 form-group" >
                                    <select class="form-control rounded" name="charge_currency" id="charge_currency" required>
                                        <option value="USD">USD</option>
                                        <option value="AED">AED</option>
                                        <option value="KD">KD</option>
                                        <option value="SAR">SAR</option>
                                        <option value="OR">OR</option>
                                        <option value="QR">QR</option>
                                        <option value="BD">BD</option>
                                    </select>
                                </div>
                                <div class="col-md-3 form-group">
                                    <select class="form-control rounded" name="charge_type" id="charge_type" required>
                                        <option value="Hourly">Hourly</option>
                                        <option value="Daily">Daily</option>
                                    </select>
                                </div>
                            </div>
                        <!-- </div> -->
                    </div>
                </div>
                <div class="col-md-12">
                    <label class="mt-5">Set Timings & Availability</label>
                    
                    <div class="days-list mt-4">
                        <div class="timeandday">
                            <div class="row text-center">
                                    <div class="col-md-4 ">
                                        <label></label>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Start Time</label>
                                    </div>
                                    <div class="col-md-4">
                                        <label>End Time</label>
                                    </div>
                                </div>
                            <div class="dp-block no-border mb-3">
                                <div class="row align-items-center">
                                    <div class="col-md-4">
                                        <div class="chiller_cb">
                                            <input id="monday" type="checkbox" name="monday">
                                            <label class="m-0" for="monday">Monday</label>
                                            <span></span>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <input class="form-control m-0 timepicker monday_start_time" type="text" name="monday_start_time" id="monday_start_time">
                                        <span class="invalid-feedback monday_start_time"></span>
                                    </div>
                                    <div class="col-md-4">
                                        <input class="form-control m-0 timepicker monday_end_time" type="text" name="monday_end_time" id="monday_end_time">
                                        <span class="invalid-feedback monday_end_time"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="dp-block no-border mb-3">
                                <div class="row ">
                                    <div class="col-md-4">
                                        <div class="chiller_cb">
                                            <input id="tuesday" type="checkbox" name="tuesday">
                                            <label class="m-0" for="tuesday">Tuesday</label>
                                            <span></span>
                                        </div>
                                    </div>
                                   <div class="col-md-4">
                                        <input class="form-control  m-0 timepicker tuesday_start_time" type="text" name="tuesday_start_time" id="tuesday_start_time">
                                        <span class="invalid-feedback tuesday_start_time"></span>
                                    </div>
                                    <div class="col-md-4">
                                        <input class="form-control  m-0 timepicker tuesday_end_time" type="text" name="tuesday_end_time" id="tuesday_end_time">
                                        <span class="invalid-feedback tuesday_end_time"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="dp-block no-border mb-3">
                                <div class="row ">
                                    <div class="col-md-4">
                                        <div class="chiller_cb">
                                            <input id="wednesday" type="checkbox" name="wednesday">
                                            <label class="m-0" for="wednesday">Wednesday</label>
                                            <span></span>
                                        </div>
                                    </div>
                                   <div class="col-md-4">
                                        <input class="form-control  m-0 timepicker wednesday_start_time" type="text" name="wednesday_start_time" id="wednesday_start_time">
                                        <span class="invalid-feedback wednesday_start_time"></span>
                                    </div>
                                    <div class="col-md-4">
                                        <input class="form-control  m-0 timepicker wednesday_end_time" type="text" name="wednesday_end_time" id="wednesday_end_time">
                                        <span class="invalid-feedback wednesday_end_time"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="dp-block no-border mb-3">
                                <div class="row ">
                                    <div class="col-md-4">
                                        <div class="chiller_cb">
                                            <input id="thursday" type="checkbox" name="thursday">
                                            <label class="m-0" for="thursday">Thursday</label>
                                            <span></span>
                                        </div>
                                    </div>
                                   <div class="col-md-4">
                                        <input class="form-control  m-0 timepicker thursday_start_time" type="text" name="thursday_start_time" id="thursday_start_time">
                                        <span class="invalid-feedback thursday_start_time"></span>
                                    </div>
                                    <div class="col-md-4">
                                        <input class="form-control  m-0 timepicker thursday_end_time" type="text" name="thursday_end_time" id="thursday_end_time">
                                        <span class="invalid-feedback thursday_end_time"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="dp-block no-border mb-3">
                                <div class="row ">
                                    <div class="col-md-4">
                                        <div class="chiller_cb">
                                            <input id="friday" type="checkbox" name="friday">
                                            <label class="m-0" for="friday">Friday</label>
                                            <span></span>
                                        </div>
                                    </div>
                                   <div class="col-md-4">
                                        <input class="form-control  m-0 timepicker friday_start_time" type="text" name="friday_start_time" id="friday_start_time">
                                        <span class="invalid-feedback friday_start_time"></span>
                                    </div>
                                    <div class="col-md-4">
                                        <input class="form-control  m-0 timepicker friday_end_time" type="text" name="friday_end_time" id="friday_end_time">
                                        <span class="invalid-feedback friday_end_time"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="dp-block no-border mb-3">
                                <div class="row ">
                                    <div class="col-md-4">
                                        <div class="chiller_cb">
                                            <input id="saturday" type="checkbox" name="saturday">
                                            <label class="m-0" for="saturday">Saturday</label>
                                            <span></span>
                                        </div>
                                    </div>
                                   <div class="col-md-4">
                                        <input class="form-control  m-0 timepicker saturday_start_time" type="text" name="saturday_start_time" id="saturday_start_time">
                                        <span class="invalid-feedback saturday_start_time"></span>
                                    </div>
                                    <div class="col-md-4">
                                        <input class="form-control  m-0 timepicker saturday_end_time" type="text" name="saturday_end_time" id="saturday_end_time">
                                        <span class="invalid-feedback saturday_end_time"></span>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="dp-block no-border mb-3">
                                <div class="row ">
                                    <div class="col-md-4">
                                        <div class="chiller_cb">
                                            <input id="sunday" type="checkbox" name="sunday">
                                            <label class="m-0" for="sunday">Sunday</label>
                                            <span></span>
                                        </div>
                                    </div>
                                   <div class="col-md-4">
                                        <input class="form-control  m-0 timepicker sunday_start_time" type="text" name="sunday_start_time" id="sunday_start_time">
                                        <span class="invalid-feedback sunday_start_time"></span>
                                    </div>
                                    <div class="col-md-4">
                                        <input class="form-control  m-0 timepicker sunday_end_time" type="text" name="sunday_end_time" id="sunday_end_time">
                                        <span class="invalid-feedback sunday_end_time"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <label class="mt-4">Select Localities where you are able to provide Services</label>
                </div>
                <div class="col-md-12">
                    <select data-placeholder="Choose a Locality..." multiple class="chosen-select form-control" name="localities[]">
                        @foreach($localities as $row)
                            <option value="{{ $row }}">{{ __($row) }}</option>
                        @endforeach
                    </select>
                     <span class="invalid-feedback localities"></span>
                </div>
                @if($role=='buddy')
                <div class="col-md-12">
                    <label class="mt-4">Area of Specialization</label>
                </div>
                <div class="col-md-12">
                    <input type="text" class="form-control"  placeholder="Enter Location" name="area_of_spec" data-role="tagsinput">
                    <span class="invalid-feedback localities"></span>
                </div>
                @endif
            </div>
            <div class="col-md-12 text-right">
                <button type="button" class="profile_step_submit red-btn rounded pt-3 pb-3 mt-3 mb-3"><span>Save & Continue</span></button>
            </div>
        </form>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>  
<script src="https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript">
    function onChangeDealProperty(obj)
    {
       var val = $(obj).val();
       if(val == 'outside')
       {
        $('.other_countries_div').show();
        $('.rera_number_div').hide();
       }else{
         $('.other_countries_div').hide();
          $('.rera_number_div').show();
       }
    }
</script>
