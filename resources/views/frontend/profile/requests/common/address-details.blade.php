
<div class="col-md-4">
    <div class="regst-blk p-3">
        <small>Profile Completion <span class="float-right theme-color">20%</span></small>
        <div class="progress-line second mt-2"></div>
        <ul class="list-inline mt-4">
            <li class="lgt-line active">
                <h4><strong></strong>Personal details</h4>
                <p class="normal-line active">Fill your personal info and identification details here.</p>
            </li>
            <li class="lgt-line active">
                <h4><strong></strong>Address</h4>
                <p class="normal-line">Fill info about your current address and native country address here.</p>
            </li>
            <li class="lgt-line">
                <h4><strong></strong>Professional details</h4>
                <p class="normal-line">Fill info about your current job domain, skills & availability.</p>
            </li>
            <li class="lgt-line">
                <h4><strong></strong>Upload documents</h4>
                <p class="normal-line">Upload civil ID/ emirates ID/ passport or any national identification document.</p>
            </li>
            <li class="lgt-line">
                <h4><strong></strong>Qualifications & Hobbies</h4>
                <p class="">Fill info about your educational qualifications and hobbies.</p>
            </li>
        </ul>
    </div>
</div>

<div class="col-md-8">
    <div class="buddy-regst-form address p-4">
        <h4 class="mb-4">Add Address</h4>
        <p class="dark-color">Add Current Address</p>
        <form method="post" name="buddy_request_Form" id="buddy_request_Form">
            @csrf
            <div class="row">
                <div class="col-md-6">
                   
                <input type="hidden" name="latitude" class="form-control" id="lat" value="longitude">
                <input type="hidden" name="longitude" class="form-control" id="lng">
                    <label>Location Name</label>
                    <input class="form-control" type="text" onChange="getLocation()" id="address" name="current_location" value="{{@$userAddress->current_location}}">
                    <span class="invalid-feedback current_location"></span>
                </div>
                <div class="col-md-6">
                    <label>Unit/Floor No.</label>
                    <input class="form-control" type="text" name="current_unit_number" value="{{@$userAddress->current_unit_number}}">
                    <span class="invalid-feedback current_unit_number"></span>
                </div>
                <div class="col-md-6">
                    <label>Address Line 1</label>
                    <input class="form-control" type="text" name="current_address_1" value="{{@$userAddress->current_address_1}}">
                    <span class="invalid-feedback current_address_1"></span>
                </div>
                <div class="col-md-6">
                    <label>Address Line 2</label>
                    <input class="form-control" type="text" name="current_address_2" value="{{@$userAddress->current_address_2}}">
                    <span class="invalid-feedback current_address_2"></span>
                </div>
                <div class="col-md-6">
                    <label>Landmark</label>
                    <input class="form-control" type="text" name="current_landmark" value="{{@$userAddress->current_landmark}}">
                    <span class="invalid-feedback current_landmark"></span>
                </div>
                <div class="col-md-6">
                    <label>Town/City</label>
                    <input class="form-control" type="text" name="current_city" value="{{@$userAddress->current_city}}">
                    <span class="invalid-feedback current_city"></span>
                </div>
                <div class="col-md-4">
                    <label>Country</label>
                    <select class="form-control rounded country_change" name="current_country" id="current_country" data-change="current_state">
                        <option value="">Select Country</option>
                        @foreach($countries as $row)
                            <option value="{{ $row->name }}" @if(@$userAddress->current_country==$row->name) selected @endif>{{ __($row->name) }}</option>
                        @endforeach
                    </select>
                    <span class="invalid-feedback current_country"></span>
                </div>
                <div class="col-md-4">
                    <label>State</label>
                    <select class="form-control rounded " name="current_state" id="current_state">
                        <option value="">Select State</option>
                        @if(@$userAddress->current_state)
                            <option value="{{@$userAddress->current_state}}" selected="true">{{@$userAddress->current_state}}</option>
                        @endif
                    </select>
                    <span class="invalid-feedback current_state"></span>
                </div>
                <div class="col-md-4">
                    <label>Zip Code</label>
                    <input class="form-control" type="text" name="current_zipcode" value="{{@$userAddress->current_zipcode}}">
                    <span class="invalid-feedback current_zipcode"></span>
                </div>
            </div>
            <div id="map" style="display:none"></div>
            <div class="row">
                <div class="col-md-12 mb-4">
                    <p class="mt-4 dark-color">Native Country Address</p>
                    <div class="checkbox-block">
                        <div class="form-group mb-2 mt-3">
                            <input type="checkbox" id="java1" name="same_current_address" onchange="setAddress(this)" @if(@$userAddress->same_current_address=='on') checked @endif>
                            <label class="" for="java1">Same as Current Address</label>
                            <small class="dp-block ml-4 pl-2">Select this option if your Native Country Address is Same as Current Address</small>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-6">
                    <label>Location Name</label>
                    <input class="form-control" type="text" name="native_location" value="{{@$userAddress->native_location}}">
                    <span class="invalid-feedback native_location"></span>
                </div>
                <div class="col-md-6">
                    <label>Unit/Floor No.</label>
                    <input class="form-control" type="text" name="native_unit_number" value="{{@$userAddress->native_unit_number}}">
                    <span class="invalid-feedback native_unit_number"></span>
                </div>
                <div class="col-md-6">
                    <label>Address Line 1</label>
                    <input class="form-control" type="text" name="native_address_1" value="{{@$userAddress->native_address_1}}">
                    <span class="invalid-feedback native_address_1"></span>
                </div>
                <div class="col-md-6">
                    <label>Address Line 2</label>
                    <input class="form-control" type="text" name="native_address_2" value="{{@$userAddress->native_address_2}}">
                    <span class="invalid-feedback native_address_2"></span>
                </div>
                <div class="col-md-6">
                    <label>Landmark</label>
                    <input class="form-control" type="text" name="native_landmark" value="{{@$userAddress->native_landmark}}">
                    <span class="invalid-feedback native_landmark"></span>
                </div>
                <div class="col-md-6">
                    <label>Town/City</label>
                    <input class="form-control" type="text" name="native_city" value="{{@$userAddress->native_city}}">
                    <span class="invalid-feedback native_city"></span>
                </div>
                <div class="col-md-4">
                    <label>Country</label>
                    <select class="form-control rounded country_change" name="native_country" id="native_country" data-change="native_state">
                        <option value="">Select Country</option>
                        @foreach($countries as $row)
                            <option value="{{ $row->name }}" @if(@$userAddress->native_country==$row->name) selected @endif>{{ __($row->name) }}</option>
                        @endforeach
                    </select>
                    <span class="invalid-feedback native_country"></span>
                </div>
                <div class="col-md-4">
                    <label>State</label>
                    <select class="form-control rounded" name="native_state" id="native_state">
                        <option value="">Select State</option>
                        @if(@$userAddress->native_state)
                            <option value="{{@$userAddress->native_state}}" selected="true">{{@$userAddress->native_state}}</option>
                        @endif
                        
                    </select>
                    <span class="invalid-feedback native_state"></span>
                </div>
                <div class="col-md-4">
                    <label>Zip Code</label>
                    <input class="form-control" type="text" name="native_zipcode" value="{{@$userAddress->native_zipcode}}">
                    <span class="invalid-feedback native_zipcode"></span>
                </div>
            </div>
            <div class="col-md-12 text-right">
                <button type="button" class="red-btn rounded pt-3 pb-3 mt-3 mb-3 profile_step_submit"><span>Save & Continue</span></button>
            </div>
        </form>
    </div>
</div>

@if(env('APP_ACTIVE_MAP') =='patel_map')
<style>
    #address_result
    {
        z-index: 9;
        left: unset !important;
        top:unset !important;
        width:  100% !important;
    }


</style>
   <script src="https://mapapi.cloud.huawei.com/mapjs/v1/api/js?callback=initMap&key={{env('PATEL_MAP_KEY')}}"></script>
    <script>
        function initMap() {
            var mapOptions = {};



             
            var myLatLng = {
                        lat: (-25.363),
                        lng: (131.044)
                                };
            mapOptions.center = myLatLng;
            mapOptions.zoom = 6;
            mapOptions.language='ENG';
            var searchBoxInput = document.getElementById('address');
             var map = new HWMapJsSDK.HWMap(document.getElementById('map'), mapOptions);
            // Create the search box parameter.
            var acOptions = {
                location: {
                    lat: 48.856613,
                    lng: 2.352222
                },
                radius: 50000,
                customHandler: callback,
                language: 'en'
            };
            var autocomplete;
            // Obtain the input element.
            // Create the HWAutocomplete object and set a listener.
            autocomplete = new HWMapJsSDK.HWAutocomplete(searchBoxInput, acOptions);
            autocomplete.addListener('site_changed', printSites);
            // Process the search result. 
            // index: index of a searched place.
            // data: details of a searched place. 
            // name: search keyword, which is in strong tags. 
            function callback(index, data, name) {
                
                var div
                div = document.createElement('div');
                div.innerHTML = name;
                return div;
            }
            // Listener.
            function printSites() {
                 
                
                var site = autocomplete.getSite();
                var latitude = site.location.lat;
                var longitude = site.location.lng;
                var country_name = site.name;
                
                var results = "Autocomplete Results:\n";
                var str = "Name=" + site.name + "\n"
                    + "SiteID=" + site.siteId + "\n"
                    + "Latitude=" + site.location.lat + "\n"
                    + "Longitude=" + site.location.lng + "\n"
                    + "country=" + site.address.country + "\n"
                    + "Address=" + site.formatAddress + "\n";

                    
                results = results + str;

                 
                 console.log(site);


                $('#lat').val(latitude);
                $('#lng').val(longitude);
                $('#current_country').val(site.address.country);
                $( "#current_country" ).trigger( "change" );
 


                /*end map zoom creation */
            }

           

            
        }
        function getLocation()
        {

        }
    </script> 

     
    @elseif(env('APP_ACTIVE_MAP')=='google_map')

<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&callback=initMap&libraries=places&v=weekly"
      defer
    ></script>
<script>

function initMap() {
  const myLatLng = {
    lat: -25.363,
    lng: 131.044
  };
    
      

  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 4,
    center: myLatLng
  });
    var input = document.getElementById('address');

 map.controls[google.maps.ControlPosition.TOP_RIGHT].push();

var autocomplete = new google.maps.places.Autocomplete(input);
autocomplete.bindTo('bounds', map);
autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);
            var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29),
          title: "Hello World!"
        });
        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindow.open(map, marker);
        });
  new google.maps.Marker({
    position: myLatLng,
    map,
    title: "Hello World!"
  });
}

function getLocation()
 {
//   alert(address);
  var address = $('#address').val();
  $('#address').addClass('error is-invalid');
  $('.invalid-feedback.current_location').text('Geo Location Not Set Properly .Please Select.');
  $('#address').focus();
//   alert(address);
  var geocoder = new google.maps.Geocoder();

  geocoder.geocode( { 'address': address}, function(results, status)
  {


    if (status == google.maps.GeocoderStatus.OK)
    {
      console.log(results[0],"afdcfsa");
      console.log(results[0].address_components);
      const countryObj=results[0].address_components.filter(val=>{
            if(val.types.includes('country')){
                return true
            }
            return false;
      });
     console.log(countryObj,'the country obj');


     var country_name = countryObj[0].long_name;
     console.log(country_name,'name');
      var latitude = results[0].geometry.location.lat();

      var longitude = results[0].geometry.location.lng();
       $('#address').removeClass('error is-invalid');
      $('.invalid-feedback.current_location').text('');
      // alert(latitude +"-----"+ longitude);
     $('#lat').val(latitude);
     $('#lng').val(longitude);
      
     $('#current_country').val(country_name);
     $( "#current_country" ).trigger( "change" );

     //$('#countryTo').val(country_name);
     
    console.log(latitude, longitude);
    }
  });

}
    </script>
    @endif