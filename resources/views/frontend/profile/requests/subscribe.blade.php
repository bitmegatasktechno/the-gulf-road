@php


$lang=Request::segment(1);
$loc=Request::segment(3);

  



$type=Request::segment(5)??'user';
$subscriptions = \App\Models\Subscription::where('type','user')->orderBy('order','ASC')->get();
@endphp
@php
$current=date('Y-m-d');
$users = \App\Models\Transaction::where('user_id',\Auth::user()->id)->where('purchase_type',$type)->where('expiry_date','>=',$current )->count();
if($users){
  if(@$users > 0){
		$package="NO";
  }else{
    $package="YES";
  }
}else{
  $package="YES";
}


$link = $loc;
$currency_code = 'USD';
$currency_symbol = '$';

if($link=='uae')
{
    $currency = 'AED';
    $currency_code = 'AED';
    $currency_symbol = 'د.إ';
}
elseif($link=='kuwait')
{     
     $currency = 'KD';
     $currency_code = 'KWD';
     $currency_symbol = 'د.ك';
      //$currency_code = 'USD';// because in indian stripe account is not support kwd currency 

}
elseif($link=='ksa')
{     
     $currency = 'SAR';
     $currency_code = 'SAR';
     $currency_symbol = 'ر.س, ﷼';
}
elseif($link=='oman')
{
     $currency = 'OR';
     $currency_code = 'OMR';
     $currency_symbol = 'ر.ع.';
}
elseif($link=='qatar')
{
      $currency = 'KD';
      $currency_code = 'QAR';
      $currency_symbol = 'ر.ق';
}
elseif($link=='bahrain')
{
      $currency = 'BD';
      $currency_code = 'BHD';
      $currency_symbol = '.د.ب ';
}

//$currency_code = 'USD';
@endphp
<script src="https://www.paypal.com/sdk/js?client-id={{env('SB_CLIENT_ID')}}"> // Required. Replace SB_CLIENT_ID with your sandbox client ID.
  </script>
  @extends('frontend.layouts.home')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('css/datepicker.css')}}">
    <style>
    ul{
      font-size:16px;
    }
    .subscriberadio .radio input[type="radio"] {
  position: absolute;
  opacity: 0;
}
.subscriberadio .radio input[type="radio"] + .radio-label:before {
  content: '';
  background: #f4f4f4;
  border-radius: 100%;
  border: 1px solid #b4b4b4;
  display: inline-block;
  width: 1.4em;
  height: 1.4em;
  position: relative;
  top: -0.2em;
  margin-right: 1em;
  vertical-align: top;
  cursor: pointer;
  text-align: center;
  -webkit-transition: all 250ms ease;
  transition: all 250ms ease;
}
.subscriberadio .radio input[type="radio"]:checked + .radio-label:before {
  background-color: #E4002B;
  box-shadow: inset 0 0 0 4px #f4f4f4;
}
.subscriberadio .radio input[type="radio"]:focus + .radio-label:before {
  outline: none;
  border-color: #E4002B;
}
.subscriberadio .radio input[type="radio"]:disabled + .radio-label:before {
  box-shadow: inset 0 0 0 4px #f4f4f4;
  border-color: #b4b4b4;
  background: #b4b4b4;
}
.subscriberadio .radio input[type="radio"] + .radio-label:empty:before {
  margin-right: 0;
}
.plans-and-pricing .price-box {min-height: 480px;}
    </style>
@endsection

@section('content')
@include('frontend.layouts.default-header')
<section class="plans-and-pricing mt-5 bg-white subscribe">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h4 class="mb-2">Purchase one of the following plans to get started.</h4>
        <!-- <p class="mb-5">Enjoy unlimited property listing for 15 days then upgrade to suitable plan.</p> -->
      </div>
      <input type="hidden" name="loc" id="loc" value="{{@$loc}}">
      <?php $j=1; ?>
      @foreach($subscriptions as $subscription)
      <div class="col-md-4 box-design">
        <div class="price-box">
          <div class="pt-5 pb-4 pl-4 pr-4">
          <div class="text-center">
            <img style="height: 100px;" class="img-fluid" src="{{asset('../uploads/subscription/'.@$subscription->logo)}}">
            <h5>{{@$subscription->name}}</h5>
          </div>
          <!-- <p><img class="mr-2" src="{{asset('img/tick.png')}}">Valid for 1 months.</p>
          <p><img class="mr-2" src="{{asset('img/cross.png')}}">Property featured on Home page.</p> -->
          <?php echo @$subscription->content ?>
          <?php $i=1; ?>
          @foreach($subscription->sub_plan as $subscription_plan)
          


           @php
            $amount = $subscription_plan['amount'];

            $current_exchange = @Currency::convert()
              ->from('USD')
              ->to($currency_code)
              ->amount($amount)
              ->get();


              $current_exchange  = round($current_exchange);

              


              $sku_id = array('plan_id'=>$subscription->_id,'plan_amount'=>$current_exchange,'currency'=>$currency_code);
              $sku_encoded = json_encode($sku_id);
              $sku_encoded_base = base64_encode($sku_encoded);

            @endphp
          <hr class="mt-4">
        <div class="row">
        <div class="col-md-2">
        <div class="subscriberadio">
        <div class="radio">
         
           <input id="radio-{{$i}}{{$j}}" class="{{@$subscription->_id}}" name="radio" type="radio" onClick="selectPlan('{{@$subscription->_id}}',{{@$current_exchange}},{{@$subscription_plan['number']}},'{{$sku_encoded_base}}')"> 
           
          <label for="radio-{{$i}}{{$j}}" class="radio-label"></label>
        </div>
        </div>
        </div>
        <div class="col-md-10">
          <h3 class="" style="font-size: 14px;font-weight: normal;">Validity : {{@$subscription_plan['validity']}} days</h3>
          <!--<h3 class="" style="font-size:14px;font-weight: normal;">Total Properties listed : {{@$subscription_plan['count']}}</h3> -->
          <h3 class="" style="font-size:14px;font-weight: normal;">Price :  {{$currency_code}}    {{@$current_exchange}}</h3>
        </div>
        </div>
        <?php $i++; ?>
          @endforeach
          <h5 class="text-center" style="
    padding: 10px;
    background-color: #E4002B;
    cursor: pointer;
    color: white;
    font-weight: 400;
    border-radius: 4px;
    width: 100%;" onClick="payPlan('{{@$subscription->_id}}',{{@$subscription_plan['amount']}},{{@$subscription_plan['number']}})">Continue with {{@$subscription->name}}</h5>
        </div>
        </div>
      </div>
      <?php $j++; ?>
      @endforeach
     
    </div>
    <input type="hidden" id="plan_id22">
    <input type="hidden" id="plan_amount22">
    <input type="hidden" id="plan_number22">
     
    <!-- Modal -->
  <div class="modal" id="myModal" role="dialog" style="display:none">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
          <h4 class="modal-title">Subscription Plans</h4>
        </div>

        <div class="modal-body">
            <input type="hidden" id="plan_id">
            <input type="hidden" id="plan_amount2">
            <input type="hidden" id="plan_number">
            <input type="hidden" id="sku_id2">
            <input type="hidden" id="purchase_type" value="{{$type}}">
           <p><span>Plan Amount:  {{$currency_code}}  <span id="plan_amount"></span></span></p>
          <div id="paypal-button-container10"></div>
          <div>
            <!-- <button type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#StripeCardModal">Stripe - Pay Online</button>
               --><button type="button" class="btn btn-danger btn-block" onclick="stripeSubscribePopup()">Stripe - Pay Online</button>

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" onClick="clodebu()" data-dismiss="modal">Close</button>


        </div>
      </div>
      
    </div>
  </div>

    <div class="modal" id="myModal1" role="dialog" >
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" onClick="clodebu()">&times;</button>
          <h4 class="modal-title">Subscription Plans</h4>
        </div>
        <div class="modal-body">
            <input type="hidden" id="plan_id">
            <input type="hidden" id="plan_amount2">
            <input type="hidden" id="plan_number">
             
          <p><span>Plan Amount: $0</span></p>
          <button onClick="countinuefree()">Continue</button>
        </div>
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-default" onClick="clodebu()" data-dismiss="modal">Close</button>
        </div> -->
      </div>
      
    </div>
  </div>
     <div class="row mt-5">
                        <div class="col-md-5">
                            <a class="mt-2 dp-inline-block" href="{{url(app()->getLocale().'/profile/'.$loc)}}" >< Go back</a>
                        </div>
                    </div>
  </div>
</section>

<script>
  var pay_redir ='profile_request';
</script>
@include('frontend.layouts.footer')
@endsection
@section('scripts')
<script type="text/javascript">
  var pay_redir ='profile_request';
function selectPlan(id,amount,number,sku_id='')
{
    $("#plan_id22").val(id);
    $("#plan_amount22").val(amount);
    $("#plan_number22").val(number);
    $("#sku_id2").val(sku_id);

 }

function payPlan(id,amount,number){
    
    var planid= $("#plan_id22").val();
    var planamount=$("#plan_amount22").val();
	   var plannumber = $("#plan_number22").val();
 
     var checked = $('.'+id+':checked');
     console.log(id);
     if(checked.length == 0){
      alert('Plan is required.');
       return;
     }

  	$("#plan_id").val(planid);
      $("#plan_amount2").val(planamount);
  	$("#plan_number").val(plannumber);
  	$("#plan_amount").html(planamount);
   	if(planamount==0){
  	$("#myModal1").show();
  	}else{
     $("#myModal").show();
  	}
}

  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('bg-header');
    } else {
       $('header').removeClass('bg-header');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('fixed-top');
    } else {
       $('header').removeClass('fixed-top');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('#top-header-bar').removeClass('fixed-top');
    } else {
       $('#top-header-bar').addClass('fixed-top');
    }
});
  
</script>
<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery('#loading').fadeOut(1000);
});

</script>
<script>
    
  paypal.Buttons({
    style:{
      color: 'white',
      layout: 'horizontal',
      tagline: false,
      shape:   'rect'
    },
    createOrder: function(data, actions) {
      // This function sets up the details of the transaction, including the amount and line item details.
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: $("#plan_amount2").val()
          }
        }]
      });
    },
    onApprove: function(data, actions) {
      // This function captures the funds from the transaction.
      return actions.order.capture().then(function(details) {
        var ajax_url = WEBSITE_URL+"/listproperty/transaction";
        
        var loc=$("#loc").val();
        var redirect = WEBSITE_URL+"/profile/"+loc;
        var amount=$("#plan_amount2").val();
        var id=$("#plan_id").val();
        var number=$("#plan_number").val();
        var purchase_type = $('#purchase_type').val();
        $.ajax({
	            url : ajax_url,
	            method:"post",
              data:{
                amount:amount,
                id:id,
                number:number,
                purchase_type:purchase_type
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              beforeSend:function(){
	                startLoader();
	            },
	            complete:function(){
	               stopLoader(); 
	            },
	            success : function(data){
	                if(data.status){
                        Swal.fire({
	                        title: 'Success!',
	                        text: 'Transaction completed succesfully',
	                        icon: 'success',
	                        confirmButtonText: 'Ok'
	                    }).then((result) => {
				            if(result.value){
								$(".subscribe").hide();
								$("#myModal").hide();
                window.location.href = redirect;
				            }
				        });
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            }
	        });
        // This function shows a transaction success message to your buyer.
        
        // alert('Transaction completed by ' + details.payer.name.given_name);
      });
    }
  }).render('#paypal-button-container10');
  //This function displays Smart Payment Buttons on your web page.

  function clodebu(){
	 $("#myModal").hide();
   $("#myModal1").hide();
  }

  
  function countinuefree(){
	var ajax_url = WEBSITE_URL+"/listproperty/transaction";
        var amount=$("#plan_amount2").val();
        var id=$("#plan_id").val();
        var number=$("#plan_number").val();
        var loc=$("#loc").val();
        var purchase_type = $('#purchase_type').val();
        var sku_id  = $("#sku_id2").val();
        var redirect = WEBSITE_URL+"/"+purchase_type+"-request/"+loc;
        if(purchase_type == 'user')
        {
          var redirect = WEBSITE_URL+"/profile/"+loc;
        }
        
        $.ajax({
	            url : ajax_url,
	            method:"post",
              data:{
                amount:amount,
                id:id,
                number:number,
                purchase_type:purchase_type,
                sku_id:sku_id,
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              beforeSend:function(){
	                startLoader();
	            },
	            complete:function(){
	               stopLoader(); 
	            },
	            success : function(data){
	                if(data.status){
								$(".subscribe").hide();
								$("#myModal1").hide();
                window.location.href = redirect;
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            }
	        });
  }


  function stripeSubscribePopup()
{
    var id    = $("#plan_id").val();
    var amount  = $("#plan_amount2").val();
    var number  = $("#plan_number").val();
    var purchase_type = $('#purchase_type').val();
    var sku_id  = $("#sku_id2").val();
  var width = 600;
  var height = 700;
  var left = parseInt((screen.availWidth/2) - (width/2));
  var top = parseInt((screen.availHeight/2) - (height/2));
 
  var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,toolbar,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;
  myWindow = window.open(WEBSITE_URL+"/strip-payment-subscribe?popup=true&plan_id="+id+"&plan_amount="+amount+"&plan_number="+number+"&sku_id="+sku_id+"&purchase_type="+purchase_type,"subWind6Csspace",windowFeatures);
  window.transaction_subscr_after_stripe = function ()
{

        var ajax_url  = WEBSITE_URL+"/listproperty/transaction";
        var Amount    = $("#plan_amount2").val();
        var id        = $("#plan_id").val();
        var number    = $("#plan_number").val();
        var loc       = $("#loc").val();
        var sku_id    = $("#sku_id2").val();
        var purchase_type = $('#purchase_type').val();

         var redirect = WEBSITE_URL+"/profile/"+loc;
        $.ajax({
              url : ajax_url,
              method:"post",
              data:{
                amount:amount,
                id:id,
                number:number,
                purchase_type:purchase_type,
                sku_id:sku_id,
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        beforeSend:function(){
                  startLoader();
              },
              complete:function(){
                 stopLoader(); 
              },
              success : function(data){
                console.log(data);
                  if(data.status){
                        Swal.fire({
                          title: 'Success!',
                          text: 'Transaction completed successfully',
                          icon: 'success',
                          confirmButtonText: 'Ok'
                      }).then((result) => {
                    if(result.value){
                      $("#StripeCardModal").hide();
                      $(".subscribe").hide();
                      $(".modal-backdrop").hide();
                      $("body").removeClass('modal-open')
                      $(".modal").hide();
                      $("#myModal").hide();
                      
                      window.location.href = redirect;
                    }
                });
                  }else{
                      Swal.fire({
                          title: 'Error!',
                          text: data.message,
                          icon: 'error',
                          confirmButtonText: 'Ok'
                      });
                  }
              }
          });
}
    
}



</script>
<!-- <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">
$(function() {
    var $form = $(".require-validation");
    $('form.require-validation').bind('submit', function(e) {
        var $form = $(".require-validation"),
            inputSelector = ['input[type=email]',
                            'input[type=password]',
                            'input[type=text]',
                            'input[type=file]',
                            'textarea'].join(', '),
            $inputs       = $form.find('.required').find(inputSelector),
            $errorMessage = $form.find('.inp-error'),
            valid         = true;
            $errorMessage.addClass('d-none');
        $('.has-error').removeClass('has-error');

        $inputs.each(function(i, el) {
            var $input = $(el);
            if ($input.val() === '') {
                $input.parent().addClass('has-error');
                $errorMessage.removeClass('d-none');
                e.preventDefault();
            }
        });

        if (!$form.data('cc-on-file')) {
            var StripeKey = "{{env('STRIPE_KEY')}}";

            e.preventDefault();
            Stripe.setPublishableKey(StripeKey);
            Stripe.createToken({
                number: $('.card-number').val(),
                cvc:    $('.card-cvc').val(),
                exp_month: $('.card-expiry-month').val(),
                exp_year: $('.card-expiry-year').val()
            }, stripeResponseHandler);
        }

    });
  function stripeResponseHandler(status, response) {
         
        if (response.error) {
            $('.stripe-error').text(response.error.message);
        } else {

            var token = response['id'];
            $form.find('input[type=text]').empty(); 
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");

            var ajax_url    = WEBSITE_URL+"/listproperty/stripeorder";
            var amount      = $("#plan_amount2").val();
           
            $("#stipe_payment_btn").val(amount);
            var stipe_payment_btn = amount;
            var id          = $("#plan_id").val();
            var number      = $("#plan_number").val();
            var stripeToken       = token;
 
            $.ajax({
                type: 'post',
                url: ajax_url,
                data: {
                        amount:amount,
                        id:id,
                        number:number,
                        stipe_payment_btn:stipe_payment_btn,
                        stripeToken:token,
                      }, 
                      beforeSend:function(){
                  startLoader();
              },
              complete:function(){
                 stopLoader(); 
              },
                success: function (data) 
                {
                   
                  console.log(data);
                      
                  if(data.status==1)
                  {

                   transaction_after_stripe();
                    

                  }
                     
                }
            });

            
        }
    } 



    

});

function transaction_after_stripe()
{
      var ajax_url = WEBSITE_URL+"/listproperty/transaction";
        var amount=$("#plan_amount2").val();
        var id=$("#plan_id").val();
        var number=$("#plan_number").val();
        var loc=$("#loc").val();
        var purchase_type = $('#purchase_type').val();
        var redirect = WEBSITE_URL+"/profile/"+loc;
        $.ajax({
              url : ajax_url,
              method:"post",
              data:{
                amount:amount,
                id:id,
                number:number,
                purchase_type:purchase_type
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },
        beforeSend:function(){
                  startLoader();
              },
              complete:function(){
                 stopLoader(); 
              },
              success : function(data){
                console.log(data);
                  if(data.status){
                        Swal.fire({
                          title: 'Success!',
                          text: 'Transaction completed successfully',
                          icon: 'success',
                          confirmButtonText: 'Ok'
                      }).then((result) => {
                    if(result.value)
                    {
                      $("#StripeCardModal").hide();
                      $(".subscribe").hide();
                      $(".modal-backdrop").hide();
                      $("body").removeClass('modal-open')
                      $(".modal").hide();
                      $("#myModal").hide(); 
                      window.location.href = redirect;
                    }
                });
                  }else{
                      Swal.fire({
                          title: 'Error!',
                          text: data.message,
                          icon: 'error',
                          confirmButtonText: 'Ok'
                      });
                  }
              }
          });
}
</script> -->
@endsection
@include('frontend.auth.modals.stripe-payment')