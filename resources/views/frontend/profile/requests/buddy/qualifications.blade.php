@php
$lang=Request::segment(1);
$loc=Request::segment(3);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
@extends('frontend.layouts.home')

@section('title','Buddy Profile-Step 5')


@section('content')

    <section class="registraion-process">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>Complete Your Buddy Profile</h3>
                    <p class="mb-5">Complete the following form to create your buddy profile.</p>
                </div>
                @include('frontend.profile.requests.common.qualification-details')
            </div>
        </div>
    </section>
    @include('frontend.layouts.footer')
    @endsection

@section('scripts')
    @include('frontend.profile.requests.common.qualification-js', ['path'=>'buddy-profile'])
@endsection