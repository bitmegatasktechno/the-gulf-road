<div id="forgot-password-popup" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content signup-right">
            <span class="close-icon" data-dismiss="modal"></span>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4>{{ __('Forgot Password') }}</h4>
                        <div class="hr">
                            <hr>
                        </div>
                    </div>
                </div>

                <form id="forgot-password-form" method="post">
                    @csrf
                    <input type="submit" class="d-none"/>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="group-field">
                                <input class="form-control" type="email" name="email" placeholder="{{ __('Email') }}" required>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="signup-button">
                                <a href="javascript:;" id="forgot-password-form-submit" class="forgot_password_btn">{{ __('Submit') }}</a>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

