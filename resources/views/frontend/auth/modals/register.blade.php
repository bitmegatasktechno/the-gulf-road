<div id="register-popup" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content signup-right">
            <span class="close-icon" data-dismiss="modal"></span>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="modal-left_side">
                            <h2>{{ __('Join The Gulf Road and get your desired Property hassle free') }}</h2>
                            <h4>Why should you register?</h4>
                            <ul>
                                <li><img src="{{ asset('img/ic_check_orange.svg') }}">
                                    <p>{{ __('Maintaining an internet presence is essential for real estate businesses and ancillary service, but also a great way to enhance your property search for buy, rent, swap and co-working space.') }}</p>
                                </li>
                                <li><img src="{{ asset('img/ic_check_orange.svg') }}">
                                    <p>{{ __('Our platform offers you a reduced marketing cost compared to other sales channels.') }}</p>
                                </li>
                                <li><img src="{{ asset('img/ic_check_orange.svg') }}">
                                    <p>{{ __('Being part of an established online market place provides a level of trust between lister, user and with our professional listed buddies and Agents.') }}</p>
                                </li>
                                <li><img src="{{ asset('img/ic_check_orange.svg') }}">
                                    <p>{{ __('More resources under one roof!') }}</p>
                                </li>

                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="right_side-model">
                            <div class="row">
                    <div class="col-md-12 text-center">
                        <img class="img-responsive2" src="{{ asset('img/the-gulf-road-final-logo.png') }}" style="width: 200px;">
                        <br>
                        <br>
                    </div>
                                <div class="col-md-12">
                                    <h4>{{ __('Register') }}</h4>
                                    <div class="hr">
                                        <hr>
                                    </div>
                                    <div class="signup-social-btn">
                                       <!-- <a class="facebook-button" href="{{route('login.social.redirect',['locale'=>'en','provider'=>'facebook'])}}"><img src="{{ asset('img/ic_facebook.svg') }}"> {{ __('Facebook') }}</a> -->
                                        
                                        <a class="google-button"  style="width: 100%" href="{{route('login.social.redirect',['locale'=>'en','provider'=>'google'])}}"><img src="{{ asset('img/ic_google.svg') }}"> {{ __('Google') }}</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row or-opt">
                                <div class="col-md-5">
                                    <hr>
                                </div>
                                <div class="col-md-2"><span>OR</span></div>
                                <div class="col-md-5">
                                    <hr>
                                </div>
                            </div>

                            <form id="register-form" method="post">
                                @csrf
                                <div class="row">
                                
                                <div class="col-md-12">
                                    <div class="group-field">
                                        <input type="text" class="form-control" name="name" placeholder="{{ __('Name') }}" required>
                                        <div class="invalid-feedback name"></div>
                                    </div>
                                    <div class="group-field">
                                        <input type="email" class="form-control" name="email" placeholder="{{ __('Email') }}" required>
                                        <div class="invalid-feedback email"></div>
                                    </div>
                                                                        
                                    <div class="row">
                                        <div class="col-md-3">
                                            <select name="phone_code" class="country-code-js1 form-control" required>
                                                @foreach(getAllCodeWithName() as $row)
                                                    <option value="{{ $row['dial_code'] }}">{{$row['name']}} {{$row['dial_code']}}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback phone_code"></div>
                                        </div>
                                        <div class="col-md-9">
                                            
                                            <div class="group-field">
                                                <input type="number" class="form-control" name="phone" placeholder="{{ __('Number') }}" required>
                                                <div class="invalid-feedback phone"></div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    

                                    <div class="group-field">
                                        <input type="password" class="form-control" name="password" placeholder="{{ __('Set Your Password') }}" required>
                                        <div class="invalid-feedback password"></div>
                                    </div>
                                    <div class="group-field">
                                        <input type="password" class="form-control" name="password_confirmation" placeholder="{{ __('Retype Your Password') }}" required>
                                        <div class="invalid-feedback password_confirmation"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <select name="country" class="form-control" required>
                                                <option value="">{{ __('Select Your Country') }}</option>
                                                @foreach(getAllLocations() as $country)
                                                    <option value="{{ $country->name }}">{{ __($country->name) }}</option> 
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback country"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                                <div class="row">
                                <div class="col-md-12">
                                    <div class="signup-button">
                                        <a href="javascript:;" id="register-form-submit">{{ __('Sign Up') }}</a>
                                    </div>
                                    <p class="ask_for_acc">{{ __('Already a member') }}? <a href="" class="login-btn"> {{ __('Log In') }}</a></p>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom_img">
            <img src="{{ asset('img/skyline.svg') }}">
        </div>
    </div>
</div>
