
<style type="text/css">
.close-icon:after {
    font-size: 40px;
    float: right;
    display: inline;
    line-height: 24px;
    cursor: pointer;
    position: unset;
    background: #ff4251;
    color: #fff;
    border-radius: 50%;
    padding: 5px;
    box-shadow: rgb(0 0 0 / 16%) 0px 3px 6px, rgb(0 0 0 / 23%) 0px 3px 6px;
}
.modal-content.signup-right {
    min-height: 40vh;
}
.signup-right {
    padding: 30px;
}
input[type="submit"] {
    background: #ff4251;
    border: 0;
    padding: 8px;
    color: #fff;
}
.s-width {
    width: 300px;
}
@media (max-width: 767px){
    .signup-right {
    padding: 15px;
}
.s-width {
    width: 200px;
}
}
</style>

<div id="compare-popup" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content signup-right">
            <span class="close-icon" data-dismiss="modal"></span>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Compares</h4>
                        <div class="hr">
                            <hr>
                        </div>
                       
                    </div>
                </div>
               
                <form method="get" action="{{url(app()->getLocale().'/listproperty/'.$link.'/swap/compare/'.$data->_id)}}">
            @csrf
            <div id="otherswap" >
            <select name="otherswap" class="s-width">
            @foreach($all_swaps as $swap)
            <option value="{{@$swap->_id}}">{{@$swap->title}}</option>
            @endforeach
            </select>
            <input type="submit" name="compare" value="Compare">
            </div>
             </form>

            </div>
        </div>
    </div>
</div>
