<style type="text/css">
    .error{
        color: red;
    }
    .intl-tel-input.allow-dropdown.separate-dial-code {
    display: none;
}
</style>

<div id="enquiry-popup" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index: 1056;">
    @php
        $property = \App\Models\Property::where('_id', Request::segment(5))->first();
    @endphp
    <div class="modal-dialog">
        <div class="modal-content enquiry-modal-content">
            <span class="close-icon" data-dismiss="modal"></span>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Book Inspection </h4>
                        <div class="hr">
                            <hr>
                        </div>
                    </div>
                </div>
                <div class="bg-white rounded p-4 mb-3 with-shadow instant-booking cntct-agnt">
                    <h2><span>Contact The Gulf Road Team</span></h2>
                    <form name="enquirytouser" id="enquirytouser" method="post">
                        @csrf
                        <input type="hidden" name="touser" id="touser" value="{{@$property->user_id}}">
                        <input type="hidden" name="type" id="type" value="{{Request::segment(4)}}">
                        <input type="hidden" name="property" id="property" value="{{Request::segment(5)}}">
                        <div class="form-group mb-3">
                            <input class="full-width " type="text" name="name" placeholder="Name" id="name" required>
                            <div class="error " id="contact_name_error"></div>
                        </div>
                        <div class="form-group mb-3">
                            <div class="row">
                                <div class="col-md-4" style="padding-right: 0px; ">
                                    <select name="phone_code" id="phone_code" class=" form-control " style="height: 48px; font-size: 13px!important;" required>
                                    @foreach(getAllCodeWithName() as $row)
                                        <option value="{{ $row['dial_code'] }}">{{$row['name']}} {{$row['dial_code']}}</option>
                                    @endforeach
                                    </select>
                                    <div class="invalid-feedback phone_code"></div>
                                </div>
                                <div class="col-md-8" style="padding-left: 0px">
                                    <div class="">
                                        <input type="number" class="full-width" name="phone" id="phone" placeholder="{{ __('Number') }}" required>
                                        <div  class="error" id="contact_phone_error"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mb-3">
                            <input class="full-width " type="email" name="email" id="email" placeholder="Email Address" required>
                            <div  class="error" id="contact_email_error"></div>
                        </div>
                        <div class="form-group mb-3">
                            <textarea rows="4" placeholder="Note" class="full-width p-2 " name="content" id="content" required></textarea>
                            <div  class="error" id="content_content_error"></div>
                        </div>
                        <div class="form-group mt-2">
                            <div class="input-daterange" style="display: none;">
                                <div class="row">
                                <div class="col-md-6 pr-0">
                                    <input type="text" id="startDate1" name="startDate"  placeholder="" class="form-control" >
                                </div>
                                <div class="col-md-6 pl-0">
                                    <input type="text" id="endDate1" name="endDate" placeholder="" class="form-control" >
                                </div>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="g-recaptcha"  id="g-makeInspectionInquiryWidgetId"></div>
                                </div>
                              </div>
                         </div>

                        <button type="submit" id="formSbtBtn" class="red-btn full-width rounded mt-3" autocomplete="off" >Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="mainbookspace-popup" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index: 9999;">
    @php
    if(Request::segment(4) == 'coworkspace')
    {
        $property = \App\Models\CoworkspaceProperty::where('_id', Request::segment(5))->first();
    }
    @endphp
    <div class="modal-dialog">
        <div class="modal-content">
            <span class="close-icon" data-dismiss="modal"></span>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Book A Day Pass</h4>
                        <div class="hr">
                            <hr>
                        </div>
                    </div>
                </div>
                <div class="bg-white rounded p-3 mb-3 with-shadow instant-booking cntct-agnt">
                    <h2><span>Contact The Gulf Road Team</span></h2>

                    <form name="mainbooktouser" id="mainbooktouser" method="post">

                             @csrf
                            <input type="hidden" name="touser" id="touser" value="{{@$property->user_id}}">
                            <input type="hidden" name="type" id="type" value="{{Request::segment(4)}}">
                            <input type="hidden" name="property" id="property" value="{{Request::segment(5)}}">
                            <div class="col-md-12">
                                <div class="form-group mt-2">
                                    <div class="form-group">
                                        <input class="full-width mb-3" type="text" name="name" placeholder="Name" id="name" required>
                                        <div id="name_error"></div>
                                    </div>
                                    <div class="form-group mb-3">
                            <div class="row">
                                <div class="col-md-4" style="padding-right: 0px; ">
                                    <select name="phone_code" id="phone_code" class="country-code-js form-control " style="height: 48px; font-size: 13px!important;" required>
                                    @foreach(getAllCodeWithName() as $row)
                                        <option value="{{ $row['dial_code'] }}">{{$row['name']}} {{$row['dial_code']}}</option>
                                    @endforeach
                                    </select>
                                    <div class="invalid-feedback phone_code"></div>
                                </div>
                                <div class="col-md-8" style="padding-left: 0px">
                                    <div class="">
                                        <input type="number" class="full-width" name="phone" id="phone" placeholder="{{ __('Number') }}" required>
                                        <div  class="error" id="contact_phone_error"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                                    <div class="form-group"> 
                                        <input class="full-width mb-3" type="email" name="email" id="email" placeholder="Email address" required>
                                        <div id="email_error"></div>
                                    </div>
                                    <div class="form-group">
                                        <textarea rows="4" placeholder="Note" class="full-width p-2 mb-3" name="content" id="content" required></textarea>
                                        <div id="content_error"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class='input-group date' id='book_day_date'>
                                                   <input type='text' class="form-control" readonly="" required="" />
                                                   <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                   </span>
                                                </div>
                                                <div id="date_error"></div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class='input-group' id='book_day_time'>
                                                   <input type='text' class="form-control" required="" readonly="" />
                                                   <span class="input-group-addon">
                                                   <span class="glyphicon glyphicon-time"></span>
                                                   </span>
                                                </div>
                                                <div id="time_error"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="g-recaptcha"  id="g-bookADayWidgetId"></div>
                                            </div>
                                          </div>
                                     </div>
                                     
                                    

                                </div>
                                <button type="submit" class="red-btn full-width rounded mt-3" autocomplete="off">Book A Day Pass</button>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="bookspace-popup" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index: 99999;">
    <div class="modal-dialog">
        <div class="modal-content signup-right">
            <span class="close-icon" data-dismiss="modal"></span>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Book This Space</h4>
                        <div class="hr">
                            <hr>
                        </div>
                    </div>
                </div>
                <div class="bg-white rounded p-3 mb-3 with-shadow instant-booking cntct-agnt">
                    <h2><span>Contact The Gulf Road Team</span></h2>
                    <div class="row">
                    <form name="booktouser" id="booktouser" method="post">
                        @csrf
                        <input type="hidden" name="touser" id="touser">
                        <input type="hidden" name="type" id="type" value="{{Request::segment(4)}}">
                        <input type="hidden" name="property" id="property" value="{{Request::segment(5)}}">
                        <div class="col-md-12">
                            <div class="form-group mt-2">
                                <input class="full-width mb-3" type="text" name="name" placeholder="Name" id="name" required>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input id="phone" class="phone" name="phone"  placeholder="Phone Number" type="number">
                                        <span style="display: none;" id="valid-msg" class="hide">Valid</span>
                                        <span style="display: none;" id="error-msg" class="hide">Invalid number</span>
                                    </div>
                                </div>
                                <br>
                                <input class="full-width mb-3" type="email" name="email" id="email" placeholder="Email address" required>
                                <textarea rows="4" placeholder="Note" class="full-width p-2 mb-3" name="content" id="content" required></textarea>
                                <div class="row">
                                    <div class="col-md-12">
                                        <select name="property_type" id="property_type" class=" form-control" style="height: 48px;" required="">
                                            <option>Property Type</option>
                                            <option value="open">Open Workspace</option>
                                            <option value="dedicated">Dedicated Workspace</option>
                                            <option value="private">Private Workspace</option>        
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <div class="input-daterange">
                                    <div class="row">
                                    <div class="col-md-6 pr-0">
                                            <input id="startDate1" name="startDate"  placeholder="12 Sep, 2019" class="form-control datepicker" required/>
                                        </div>
                                        <div class="col-md-6 pl-0">
                                            <input id="endDate1" name="endDate1"  placeholder="12 Sep, 2019" class="form-control datepicker" required/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="red-btn full-width rounded mt-3" autocomplete="off">Submit</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="report-popup" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index: 1056;">
    <div class="modal-dialog">
        <div class="modal-content signup-right">
            <span class="close-icon" data-dismiss="modal"></span>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Report this listing</h4>
                        <div class="hr">
                            <hr>
                        </div>
                    </div>
                </div>
                <div class="bg-white rounded p-3 mb-3 with-shadow instant-booking cntct-agnt">
                    <!-- <h2><span>Contact The Gulf Road Team</span></h2> -->
                    <div class="row">
                    <form name="reportuser" id="reportuser" method="post">
                        @csrf
                        <input type="hidden" name="type" id="type" value="{{Request::segment(4)}}">
                        <input type="hidden" name="loc" id="loc" value="{{Request::segment(3)}}">
                        <input type="hidden" name="property" id="property" value="{{Request::segment(5)}}">
                        <div class="col-md-12">
                            <div class="form-group mt-2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <select name="reason" id="reason" class=" form-control" style="height: 48px;" required="">
                                            <option value="">Select a reason</option>
                                            <option value="Property Not Available">Property Not Available</option>
                                            <option value="Inaccurate Price">Inaccurate Price</option>
                                            <option value="No response from agent">No response from agent</option> 
                                            <option value="No property details">No property details</option>
                                            <option value="Poor quality and irrelevant photos">Poor quality and irrelevant photos</option>         
                                            <option value="Poorly typed description">Poorly typed description</option> 
                                            <option value="Inaccurate location">Inaccurate location</option> 
                                            <option value="Inaccurate number of bedrooms">Inaccurate number of bedrooms</option> 
                                            <option value="Incorrect property type">Incorrect property type</option> 
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <select name="user_type" id="user_type" class=" form-control" style="height: 48px;" required="">
                                            <option value="">Select user type</option>
                                            <option value="I am a potential renter">I am a potential renter</option>
                                            <option value="I am the landlord">I am the landlord</option>
                                            <option value="I am an agent">I am an agent</option>        
                                        </select>
                                    </div>
                                </div>

                                <textarea rows="4" placeholder="Note" class="full-width p-2 mb-3" name="content" id="content"></textarea>
                            </div>
                             <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="g-recaptcha"  id="g-makeReportFlagWidgetId"></div>
                                    </div>
                                  </div>
                             </div>
                            <button type="submit" class="red-btn full-width rounded mt-3" autocomplete="off">Submit</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
