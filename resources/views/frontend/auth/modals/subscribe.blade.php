<div id="subscribe-popup" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content signup-right">
            <span class="close-icon" data-dismiss="modal"></span>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <h4>Subscribe</h4>
                        <div class="hr">
                            <hr>
                        </div>
                       
                    </div>
                </div>
                   
                <form action="{{url(app()->getLocale().'/subscribe')}}" method="GET" enctype='multipart/form-data' id="subscribe-form">
                    @csrf
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="group-field">
                                <label>Email Address:</label>
                                <input class="form-control" type="email" name="email" id="email" placeholder="Email"  >
                                <div class="invalid-feedback"></div>
                                <div id="subscribe_email_error"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="signup-button">
                                
                                 <!-- <input type="submit" value="Submit" class="red-btn full-width rounded" > -->
                                 <!-- <button type="submit" name="subscribe_submit" id="subscribe_submit" value="Submit"  class="red-btn full-width rounded">Submit</button> -->
                                 <button type="submit" name="subscribe_submit" id="subscribe_submit" value="Submit"  class="red-btn full-width rounded">Submit</button>
 
                            </div>
                           
                        </div>
                    </div>

                  
                        
                 </form>

               
            </div>
        </div>
    </div>
</div>
 