<style>
    .group-field select {
    -webkit-appearance: none;
    background: url('{{ asset('img/down_black.svg')}}');
    background-repeat: no-repeat;
    background-position: 95% center;
    background-size: 12px;
}
</style>
 <div id="homeloan-popup" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content signup-right">
            <span class="close-icon" data-dismiss="modal" style="text-align: right;"></span>
            <div class="modal-body">
                <div class="row"><div class="col-12"  style="text-align: center;"><a class="" href="#">
                    <img src="{{ asset('img/the-gulf-road-final-logo.png') }}" style="height: auto;width: 68%;">
                </a></div></div><br/><br/>

                <div class="row">
                    <div class="col-md-12">
                        <h4>Home Loan</h4>
                        <div class="hr">
                            <hr>
                        </div>
                       
                    </div>
                </div>
               
                <form  method="get" id="homeloan_form">
                    
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="group-field">
                                <label>Name:</label>
                                <input class="form-control" type="text" name="name" id="name" placeholder="Name" required>
                                <div class="" id="homeloan_name_error"></div>
                            </div> 
                        </div>
                        <div class=" col-md-12">
                            <div class="group-field">
                                <label>Mobile Number:</label>
                                <!-- <input class="form-control" type="number" name="phone" id="phone" placeholder="Mobile Number" required> -->
                                <div class="row">
                                    <div class="col-md-5">
                                        <select name="phone_code" id="phone_code" class="country-code-js form-control " required>
                                         @foreach(getAllCodeWithName() as $row)
                                        <option value="{{ $row['dial_code'] }}">{{$row['name']}} {{$row['dial_code']}}</option>
                                        @endforeach
                                        </select>
                                        <div class="phone_code"  id="homeloan_phone_code_error"></div>
                                    </div>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="phone"  id="phone" placeholder="Mobile Number" pattern="[0-9]{7,13}" title="Phone Number must be in between 7 to 13 digits" required>
                                        
                                    </div>
                                </div>
                                
                            </div>
                            <div class=""  id="homeloan_phone_error"></div>
                        </div>
                        <div class="col-md-12">
                            <div class="group-field">
                                <label>Email Address:</label>
                                <input class="form-control" type="email" name="email" id="email" placeholder="Email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="Please enter a valid email address" required>
                                <div class=""  id="homeloan_email_error"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="group-field">
                                <label>Query:</label>
                                <textarea class="form-control" name="content1" id="content1" required></textarea>
                                <div class=""   id="homeloan_content1_error"></div>
                            </div>
                        </div>
                         <div class="col-md-12">
                            <div class="group-field">
                                <div class="g-recaptcha"  id="g-homeLoanWidgetId"></div>
                            </div>
                        </div>
                    </div>

                  <br>
                        <div class="col-md-12">
                            <div class="signup-button">
                                <button class="form-control buy_form_btn" type="submit" value="Submit">Submit</button>
                            </div>
                           
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

<!--  
 <script>
   function onSubmit(token) {
     document.getElementById("homeloan_form").submit();
   }
   function onClick(e) {
        e.preventDefault();
        grecaptcha.ready(function() {
          grecaptcha.execute('{{env("reCAPTCHA_site_key")}}', {action: 'submit'}).then(function(token) {
              // Add your logic to submit to your backend server here.
          });
        });
      } 
  </script> -->
