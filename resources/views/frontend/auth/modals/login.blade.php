<div id="login-popup" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content signup-right">
            <span class="close-icon" data-dismiss="modal"></span>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <img class="img-responsive2" src="{{ asset('img/the-gulf-road-final-logo.png') }}" style="width: 200px;">
                        <br>
                        <br>
                    </div>
                    <div class="col-md-12">
                        <h4>{{ __('Log in') }}</h4>
                        <div class="hr">
                            <hr>
                        </div>
                        <div class="signup-social-btn">
                            <!--<a class="facebook-button" href="{{route('login.social.redirect',['locale'=>'en','provider'=>'facebook'])}}"><img src="{{ asset('img/ic_facebook.svg') }}"> {{ __('Facebook') }}</a> -->
                            <a class="google-button"  style="width: 100%" href="{{route('login.social.redirect',['locale'=>'en','provider'=>'google'])}}"><img src="{{ asset('img/ic_google.svg') }}"> {{ __('Google') }}</a>
                        </div>
                    </div>
                </div>
                <div class="row or-opt">
                    <div class="col-md-5">
                        <hr>
                    </div>
                    <div class="col-md-2"><span>OR</span></div>
                    <div class="col-md-5">
                        <hr>
                    </div>
                </div>
                <form id="login-form" method="post">
                    @csrf
                    <input type="submit" class="d-none"/>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="group-field">
                                <input class="form-control" type="email" name="email" placeholder="Email" required>
                                <div class="invalid-feedback"></div>
                            </div>
                            <div class="group-field">
                                <input class="form-control" type="password" name="password" placeholder="Password" required>
                                <div class="invalid-feedback"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <!--<div class="col-md-6">
                            <div class="group-field">
                                <label class="container-checkbox">{{ __('Remember me') }}
                                    <input type="checkbox" name="remember" checked="checked">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div> -->
                        <div class="col-md-12">
                            <div class="signup-button">
                                <a href="" id="login-form-submit">{{ __('Sign In') }}</a>
                            </div>
                            <p class="ask_for_acc">{{ __('Don\'t have an account') }} ? <a href="javascript:;" onclick="showRegisterForm();" class="register-btn">{{ __('Register') }}</a>
                            </p>
                        </div>

                        <div class="col-md-12 text-center">
                            <a class="forgot-password" style="float:none;" href="javascript:;" onclick="showForgotPasswordForm();">{{ __('Forgot Password') }}?</a>
                        </div>

                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
