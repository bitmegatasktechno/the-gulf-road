<style>
    #home1 .line{
        height: 10px !important;
    }

    .desktopSideMenu .mpn {
        margin-bottom: 13%;
    }

    .desktopSideMenu .lname{
        font-size: 23px;
    }

    .desktopSideMenu .innert .col {
        border: 0px;
        border-radius: 0px;
    }
    .goog-te-gadget .goog-te-combo{
        color: black !important;
    }
    .intro-sec-text a:hover {
    background: #a0001e;
}
.intro-sec-text a {
    height: unset;
    min-width: 90px;
    font-size: 16px;
    display: inline-block;
    line-height: inherit;
    padding: 6px 15px;
    text-transform: uppercase;
    width: unset;
    background: #ff4251;
    color: #fff;
    background: #ff4251;
    color: #fff;

}
@media only screen and (min-device-width: 360px) and (max-device-width: 460px) {
.intro-sec-text {
    width: 100%;
    padding-left: 0%;
}
.intro-sec-text p {
    font-size: 14px;
}
.intro-sec-text a {
    font-size: 16px;
    padding: 6px 15px;
    width: 100%;
    margin: 4px;
}
.mobilesocialmedia ul li a {
    padding: 2px 15px;
}
}
@media only screen and (min-device-width: 800px) and (max-device-width: 1280px) {
            .intro-sec-text h1 {
    font-size: 22px;
}
.intro-sec-text p {
    font-size: 12px;
}
.intro-sec-text a {
        height: unset;
    min-width: 60px;
    font-size: 12px;
    display: inline-block;
    line-height: inherit;
    padding: 4px 6px;
    text-transform: uppercase;
    width: unset;
    background: #ff4251;
    color: #fff;

}
.intro-sec-text a:hover {
    background: #a0001e;
}
 .intro-sec-text p span {
    font-size: 18px;
    font-weight: 500;
}
}
</style>

<div class="intro-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <!-- <div class="col mobilemap">
                    <div class="row">
                        <div class="col-6 mpn">
                            <a href="{{url(app()->getLocale().'/home/KSA')}}">
                                <div class="innert saudiarabia">
                                    <div class="col">
                                        <img class="img-responsive2" src="{{ asset('img/saudiarabia11.png') }}">
                                    </div>
                                    <div class="col lname text-center">{{__('SAUDI ARABIA')}}</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-6 mpn">
                            <a href="{{url(app()->getLocale().'/home/Oman')}}">
                                <div class="innert oman">
                                    <div class="col">
                                        <img class="img-responsive2" src="{{ asset('img/oman11.png') }}">
                                    </div>
                                    <div class="col lname text-center">{{__('OMAN')}}</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-6 mpn">
                            <a href="{{url(app()->getLocale().'/home/UAE')}}">
                                <div class="innert uae">
                                    <div class="col">
                                        <img class="img-responsive2" src="{{ asset('img/uae11.png') }}">
                                    </div>
                                    <div class="col lname text-center">{{__('UAE')}}</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-6 mpn">
                            <a href="{{url(app()->getLocale().'/home/Qatar')}}">
                                <div class="innert quatar">
                                    <div class="col">
                                        <img class="img-responsive2" src="{{ asset('img/quatar11.png') }}">
                                    </div>
                                    <div class="col lname text-center">{{__('QATAR')}}</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-6 mpn">
                            <a href="{{url(app()->getLocale().'/home/Kuwait')}}">
                                <div class="innert kuwait">
                                    <div class="col">
                                        <img class="img-responsive2" src="{{ asset('img/kuwait11.png') }}">
                                    </div>
                                    <div class="col lname text-center">{{__('KUWAIT')}}</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-6 mpn">
                            <a href="{{url(app()->getLocale().'/home/Bahrain')}}">
                                <div class="innert bahrain">
                                    <div class="col">
                                        <img class="img-responsive2" src="{{ asset('img/bahrain11.png') }}">
                                    </div>
                                    <div class="col lname text-center">{{__('BAHRAIN')}}</div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div> -->
                <div class="intro-sec-text">
                    <h1>{{__('Listings That You Will Love')}}</h1>

                    
                    <p>
                        <span>{{__('Finding made easy!')}}</span>
                        <br/>                         {{__('Whether you’re just beginning your property journey or have had years of experience, thegulfroad.com is the number one place for people to come together to explore, research and share their passion for middle-east properties. Thegulfroad.com is market-leading digital advertising business specializing in property. We list properties for rent, and sale. We help you finding great agents to make your property search easier. We connect you with great buddies who can coach and guide you before or after your arrival in the new city. If you are looking for co-working space, theguflroad.com is the best to explore.')}}
                        <br/>
                        <span >Click here to explore properties in :</span>
                    </p>
                    <div class="row">
                        @foreach(getAllLocations() as $row)
                        <div class="col-md-2">
                            <a href="{{url(app()->getLocale().'/home/'.strtolower($row->name))}}">{{$row->name}}</a>
                        </div>
                        @endforeach
                    </div>
                    
                    <!-- <a href="{{url(app()->getLocale().'/home/Kuwait')}}">Click here to explore properties in Kuwait</a> -->
                </div>
            </div>
            <div class="col-md-4"></div>
         <!--   <div class="col-md-6">
                    <div class="row desktopSideMenu" style="display: none;">
                        <div class="col-6 mpn">
                            <a href="{{url(app()->getLocale().'/home/KSA')}}">
                                <div class="innert saudiarabia">
                                    <div class="col lname text-center">{{__('SAUDI ARABIA')}} <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-6 mpn">
                            <a href="{{url(app()->getLocale().'/home/Oman')}}">
                                <div class="innert oman">
                                    <div class="col lname text-center">{{__('OMAN')}} <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-6 mpn">
                            <a href="{{url(app()->getLocale().'/home/UAE')}}">
                                <div class="innert uae">
                                    <div class="col lname text-center">{{__('UAE')}} <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-6 mpn">
                            <a href="{{url(app()->getLocale().'/home/Qatar')}}">
                                <div class="innert quatar">
                                    <div class="col lname text-center">{{__('QATAR')}} <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-6 mpn">
                            <a href="{{url(app()->getLocale().'/home/Kuwait')}}">
                                <div class="innert kuwait">
                                    <div class="col lname text-center">{{__('KUWAIT')}} <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-6 mpn">
                            <a href="{{url(app()->getLocale().'/home/Bahrain')}}">
                                <div class="innert bahrain">
                                    <div class="col lname text-center">{{__('BAHRAIN')}} <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
</div>
                                </div>
                            </a>
                        </div>
                    </div>

      <div class="map-outter">

 <svg data-name="Layer 1" id="Layer_1" viewBox="0 0 1052.78 710.96" xmlns="http://www.w3.org/2000/svg">

    <a class="pointer" data-id="home1" onclick="clicked(this)" onmouseout="unhighlightme(this)" onmouseover="hightlightme(this)">
        <path style="opacity:0.1;" class="home1 cls-1"
              d="M588.7,130.89c-.23,1-.5,2-.64,2.94a5.11,5.11,0,0,0,3.27,5.68,7.07,7.07,0,0,0,3.61.56,5.87,5.87,0,0,1-.53,1.25,2.69,2.69,0,0,0-.33,2.15c.16,1.17.35,2.34.5,3.51a2.86,2.86,0,0,1-.07.92,4.31,4.31,0,0,0,.92,3.62,24.25,24.25,0,0,1,1.76,3.06H553.9c-.22-1.66-1.62-2.59-2.5-3.86-.55-.79-1-1.65-1.52-2.44a4.29,4.29,0,0,0-1.07-1.3,4.56,4.56,0,0,1-1.92-3c-.21-.9-.34-1.82-.49-2.73a.77.77,0,0,1,0-.45c.72-1.52.23-3.07.19-4.62a7.92,7.92,0,0,0-1.34-4.06c-1.79-2.9-3.49-5.86-5.21-8.8a1.14,1.14,0,0,0-.92-.62q-12.66-1.92-25.31-3.89l-23.61-3.63-4.11-.64c1.09-1,2-1.84,3-2.58a11.14,11.14,0,0,0,3.12-3.26c.29-.48.71-.89,1-1.35.48-.7,1-1.4,1.39-2.13.82-1.38,1.56-2.79,2.41-4.16.67-1.08,1.44-2.11,2.16-3.16a1.48,1.48,0,0,1,.29-.36,11.53,11.53,0,0,0,3-3.81c1.8-3,3.52-5.94,5.26-8.92a172.39,172.39,0,0,0,8.36-17c1.14-2.56,1.85-5.31,2.81-8A30.66,30.66,0,0,1,520.2,57c.23-.47.51-.92.77-1.38a8.92,8.92,0,0,1,4.91-4,26.49,26.49,0,0,1,3.31-.9,9.15,9.15,0,0,0,3.8-1.9,1.89,1.89,0,0,1,1.12-.4q9.95,0,19.88,0a3.21,3.21,0,0,1,1.15.21q6.48,2.53,13,5.11c.77.31,1.47.79,2.23,1.13a8.8,8.8,0,0,0,1.73.58,2.22,2.22,0,0,0,1.53-.4,7.2,7.2,0,0,1,2.57-.91,42.24,42.24,0,0,1,4.34.06c-.21.61-.43.95-1,1a5.2,5.2,0,0,0-3.28,1.34l-.18.19s0,0,0,.18a12.34,12.34,0,0,0,1.71-.15A7.62,7.62,0,0,1,584.1,58,18.62,18.62,0,0,1,589,63.6c.88,1.36,1.83,2.69,2.76,4a1.46,1.46,0,0,0,.5.42,2.65,2.65,0,0,1,1.35,2.77,16.18,16.18,0,0,1-.13,2.09,7,7,0,0,1-1.69,3.29c-1.11,1.36-2.1,2.82-3.15,4.22A2.25,2.25,0,0,1,588,81a5.68,5.68,0,0,0-2,1.79,2,2,0,0,1-2.17.53,3.35,3.35,0,0,1-2.24-1.7c-.57-1-1.17-2-1.76-3l-.28.08a13.71,13.71,0,0,0,.2,1.94,10.77,10.77,0,0,0,2.2,3.86c.72,1,.76,1.3.14,2.31-.26.43-.59.81-.94,1.28-.66-1.92-2.22-2.57-3.88-3.08A41.17,41.17,0,0,0,573,83.8a6.24,6.24,0,0,0-5.26,1.32,14.43,14.43,0,0,1-2.18,1.23,20.8,20.8,0,0,0-2.7,1.51,10.7,10.7,0,0,0-1.9,2.07c-.73.94-1.34,2-2.08,2.89s-1.54,1.74-2.34,2.58a.74.74,0,0,1-.47.2,1.73,1.73,0,0,0-1.53,1.25c-.21.46-.08.67.43.62.28,0,.55-.15.83-.19a7,7,0,0,1,1.81-.19,9.62,9.62,0,0,0,4.3-.32,6.81,6.81,0,0,1,.78-.15,9.14,9.14,0,0,0-.53,1c-.21.53-.4,1.06-.57,1.61a.7.7,0,0,0,.08.49c.48.75,2,1.3,2.74.71a12.53,12.53,0,0,1,2.47-1.71,5,5,0,0,0,2.52-2.37,1.35,1.35,0,0,1,2.28,0,5.63,5.63,0,0,0,2.23,2.18,3.52,3.52,0,0,0,2.65.45c.68-.18.85,0,.64.74a10.2,10.2,0,0,0,.46,7.37,26.94,26.94,0,0,1,2,7.1c.34,2.26.54,4.54,1,6.77a31.38,31.38,0,0,0,1.5,4.34.74.74,0,0,0,.37.35,1.78,1.78,0,0,1,1,1.28,4.5,4.5,0,0,0,3,3.09C587.22,130.34,588,130.6,588.7,130.89Z"
              style="fill: rgba(255, 255, 255, 0.6);"></path>
    </a>

    <a class="pointer" data-id="home2" onclick="clicked(this)" onmouseout="unhighlightme(this)"
       onmouseover="hightlightme(this)">
        <path style="opacity:0.1;" class="home2 cls-1"
              d="M643.71,239.54l-1.78,2.58a3.35,3.35,0,0,1-.43.5c-1,1-2,1.94-2.86,3s-1.58,2.15-1,3.62c.08.21-.11.53-.17.81a4.11,4.11,0,0,1-.59-.67c-.28-.5-.52-1-.76-1.54a10.73,10.73,0,0,1-.45-1.11c-.28-.77-.59-1.53-.81-2.32a14.71,14.71,0,0,1-.52-2.61,13.45,13.45,0,0,0-2.74-6.55c-.57-.83-1-1.73-1.56-2.6a2.55,2.55,0,0,0-.31-.47c-1.85-1.92-3.69-3.85-5.56-5.75a7.65,7.65,0,0,0-.93-.64c-.91-.67-1.86-1.29-2.7-2-.34-.3-.42-.9-.6-1.37a10.92,10.92,0,0,1-.77-3.51,6.14,6.14,0,0,1,.33-2.8,3.51,3.51,0,0,1,.45-.86,7.06,7.06,0,0,0,1.83-4.54,4.34,4.34,0,0,1,.86-2.42,4.86,4.86,0,0,0,.69-4.92,3.33,3.33,0,0,1-.18-1.49,11.49,11.49,0,0,0-.81-5.45c-.22-.7-.93-1.24-1.26-1.92-.61-1.25-1.1-2.55-1.64-3.83l-.13-.28c-.11,0-.19.12-.28.11-.33,0-.65-.07-1-.11,0-.31,0-.78.15-.89.78-.51.41-1.09.25-1.71-.23-.93-.39-1.89-.55-2.84a2.28,2.28,0,0,1,.17-.69l-1.17.16c-.67.09-1.34.16-2,.28-.22,0-.55.2-.58.35a23.54,23.54,0,0,0-.36,3c0,.64-.09,1.36.64,1.79.14.08.17.39.21.6a8.8,8.8,0,0,1,.15,1.45,6.31,6.31,0,0,1-.25,1.19,6.24,6.24,0,0,1-1.23-.36,2.77,2.77,0,0,0-2.32,0,3.87,3.87,0,0,1-2.93-.44.57.57,0,0,1-.09-.39,5.82,5.82,0,0,0-.74-3.07,3.26,3.26,0,0,1,1.33-3.92,11.27,11.27,0,0,0,1.1-.67c-.36-.28-.59-.5-.85-.66a9,9,0,0,0-5-1.2c-.07,0-.17,0-.21,0a2.89,2.89,0,0,1-.32-.29c.12-.09.24-.26.35-.25,1.52.14,3,.24,4.54.5a6.49,6.49,0,0,1,1.71.92c.46-.5.32-.76-.27-.89-.9-.2-1.8-.38-2.69-.63-.24-.07-.43-.36-.64-.54.26-.09.55-.31.78-.26,1.1.24,2.19.55,3.28.86a.89.89,0,0,1,.53.36c.63,1.27,2,1.38,3.06,1.79a5.05,5.05,0,0,0,2-.11c.46,0,.91-.1,1.37-.15s.94-.05.94-.73c0-.13.2-.25.28-.4a2.43,2.43,0,0,0,.4-.89A18.68,18.68,0,0,0,619,179c0-.21-.31-.47-.53-.56a1,1,0,0,1-.7-1,6.35,6.35,0,0,1,0-1.38c.07-.31.35-.77.59-.8.92-.13,1.12-1,1.7-1.47.28-.22.52-.56.83-.66a18,18,0,0,1,8.23-.86,1,1,0,0,0,1.13-.5c1.14-1.47,2.35-.91,3.43.32.87,1,1.17,1.22,2.38.66,1.51-.7,3-1.44,4.51-2.14a2.75,2.75,0,0,1,1-.29c.61,0,.82-.34.81-.87a3.85,3.85,0,0,0-.09-1.08c-.39-1.22.23-2.09.93-3a8.77,8.77,0,0,0,.45-.86c.12-.18.27-.41.46-.47.73-.25,1.48-.45,2.22-.64a1.56,1.56,0,0,1,2,1c.14.6.5.68,1,.81.63.19,1.37.23,1.68,1a2.52,2.52,0,0,1,.31,1.07,7.64,7.64,0,0,0,.39,3.18,3.49,3.49,0,0,1,.11,1.33c0,1.54-.14,3.08-.22,4.74,1.63-.38,2.55.79,3.61,1.49a3,3,0,0,1,.86,1.69c.05.19-.36.64-.65.78-.47.22-1,.31-1.14-.31a17.77,17.77,0,0,1-1.1,1.49c-.3.32-.65.34-.93-.15a4.94,4.94,0,0,1-.78-2.36c0-.86-.52-1.7-.64-2.57-.14-1-.08-2.06-.14-3.09a3.25,3.25,0,0,0-.28-.81c-.16-.43-.34-.86-.48-1.3a8.26,8.26,0,0,1-.18-.89c-1.06.05-1.18.86-1.53,1.46a1,1,0,0,1-.48.43c-.09,0-.29-.24-.41-.4-.44-.57-.86-1.15-1.32-1.77-.15.35-.26.69-.43,1a1.15,1.15,0,0,1-1.84.42,2,2,0,0,0-.55-.35c-.14,0-.45.1-.45.18-.06.64-.07,1.29-.09,1.94,0,.12.06.24.05.36-.06.92-.15,1.8,1.2,1.88.47,0,.9.5,1.39.64,1.23.35.94,1.25.36,1.87a3.08,3.08,0,0,1-.95.51c-.67.33-1.37.6-2,1a2.2,2.2,0,0,1-2.91-.32,2.71,2.71,0,0,0-2.69-.61c-.53.1-.81.36-.59.91.42,1,.85,2.08,1.33,3.09a.86.86,0,0,0,.67.29c1,0,1.15.18.76,1.24l1.75,1.5c.73.62.73.79,0,1.43,0,0,0,.13-.06.23a6.11,6.11,0,0,1,1.13-.26,4.47,4.47,0,0,1,1,.13c-.51-1.33-1-2.55-1.43-3.78a.76.76,0,0,1,.2-.6c.46-.53,1-1,1.44-1.52a3.07,3.07,0,0,1,2.58-.6.86.86,0,0,1,.52,1.24,1.29,1.29,0,0,0,.29,1.7,2.24,2.24,0,0,1,.33,1.13,6.81,6.81,0,0,1,0,1.35,4.84,4.84,0,0,1-.27.87c0,.15,0,.31-.1.59,2-.69,3.48-2.22,5.6-2.36,0,.25.07.48.11.73l1.35-.24a2.6,2.6,0,0,1-2.39,1.27c-.88-.27-1.37.12-1.31,1,0,.29.07.57.11.85a11.59,11.59,0,0,1-1.11.62c-.2.09-.57.18-.67.07-.47-.49-.78-.13-1,.2s.07.54.21.8a1.22,1.22,0,0,0,.09,1.34,2.52,2.52,0,0,1,.26,1.73,1.77,1.77,0,0,1-1,1c-1.39.58-1.68,1-1.35,2.57.13.59.39,1.17-.31,1.62a1,1,0,0,0,.22.7,2,2,0,0,1,.77,1.33c.06.36.16.63-.34.87-.24.12-.31.78-.29,1.2a9.74,9.74,0,0,0,.28,1.85,1.37,1.37,0,0,0,.43.6,6.44,6.44,0,0,1,1,.88.94.94,0,0,1,0,.92,3.62,3.62,0,0,0-.81,2.22,47.22,47.22,0,0,1-1,4.66c-.06.26-.23.52-.19.76a9.15,9.15,0,0,1-.46,3.12,14.58,14.58,0,0,0-.26,3.95,10.79,10.79,0,0,1-.77,5.45c-.1.23.06.59.11.89a2.39,2.39,0,0,1,.11.62c-.2,1.18-.49,2.35-.62,3.54a16.55,16.55,0,0,0,.14,2.09c0,.64-.13,1.28-.1,1.91C643.48,237.43,643.61,238.4,643.71,239.54Zm8.18-61.88c.51,1.32.51,1.32,2,1.72-.08-.33-.17-.69-.25-1.06A1.21,1.21,0,0,0,651.89,177.66Z"
              style="fill: rgba(255, 255, 255, 0.6);"></path>
    </a>

    <a class="pointer" data-id="home3" onclick="clicked(this)" onmouseout="unhighlightme(this)"
       onmouseover="hightlightme(this)">
        <path style="opacity:0.1;" class="home3 cls-1"
              d="M668.94,226.08a1.31,1.31,0,0,1,.61,1.68,20,20,0,0,0-.59,2.4,18.26,18.26,0,0,0-.21,2.77c0,.47.28.93.3,1.4a30.13,30.13,0,0,0,.05,3.43,39,39,0,0,1,.36,6.83c-.08,4.55.28,9.1.48,13.65a.74.74,0,0,0,.21.49c1.4,1,1.92,2.52,2.56,4a2,2,0,0,1-.17,2.4c-1.35,1.27-1.08,2.78-.44,4.1a23,23,0,0,0,2.84,4.2,6.24,6.24,0,0,1,1.41,5.44,5.57,5.57,0,0,0,.3,4.24,3.58,3.58,0,0,1,.38,2.53,3.81,3.81,0,0,1-1.38,2.17,1.17,1.17,0,0,0-.25,1.51c1.67,4,3.41,7.93,4.9,12a10.83,10.83,0,0,0,4.37,5.63c1.54,1,2.92,2.19,4.77,2.65,1.65.4,3.24,1,4.88,1.5a3.76,3.76,0,0,0,1.6.09c3.91-.74,7.82-1.5,11.71-2.32.83-.17,1.84-.41,2-1.42.29-1.56.41-3.14.57-4.72.18-1.82-.58-2.6-2.51-3.18-1-.31-2.15-.41-3.22-.66-.34-.08-.92-.38-.9-.51a2.35,2.35,0,0,1,.57-1.09,1.28,1.28,0,0,1,.72-.37,19.92,19.92,0,0,1,3.9-.35,6.38,6.38,0,0,1,3.69.95,1.82,1.82,0,0,1-.27,3.19,4.15,4.15,0,0,0-.62.46l.5,2.38c.11.52.77.78,1,.35a3.67,3.67,0,0,1,1.72-1.4,1.5,1.5,0,0,0,.61-.92,7.33,7.33,0,0,0,.14-1.63,2.39,2.39,0,0,1,1.8-2.5c.39-.13.78-.28,1.17-.38a2,2,0,0,0,1.58-1.7,16.33,16.33,0,0,1,2.11-5.51,21.46,21.46,0,0,0,1.63-5.5c.53-2.24,1.1-4.44,3.24-5.79a3,3,0,0,0,.9-1.06c.88-1.49,1.75-3,2.56-4.52a9.12,9.12,0,0,0,.61-1.85c.13-.48.22-.87.79-1.12A4.28,4.28,0,0,0,733.8,265a5.89,5.89,0,0,1-.51-3.28c.16-1.44.27-2.89.33-4.34,0-1.26,0-2.53,0-3.79a8.06,8.06,0,0,0-.16-2.84,8.17,8.17,0,0,1-.53-3.77,3.12,3.12,0,0,0-2.61-3.37,2.23,2.23,0,0,1-1-.32,6.22,6.22,0,0,1-2.09-1.73,7.65,7.65,0,0,1-.53-2.94c-.16-1.91-.19-3.84-.36-5.75s-.44-3.56-2-4.95a3.63,3.63,0,0,1-1.11-4.13,8.17,8.17,0,0,0,.73-4.63,1.56,1.56,0,0,1,.57-1.24,18.86,18.86,0,0,1,2-1c.53-.3,1.23-.6,1.44-1.08.61-1.4.27-2.15-1-3a1.49,1.49,0,0,1-.57-1.68c.09-.36.44-.92.7-.93a3.85,3.85,0,0,1,1.53.46c.19.08.3.32.47.46.69.58,1.35.39,1.64-.48a10.39,10.39,0,0,0,.38-1.35,2,2,0,0,0-.82-2.31,1.78,1.78,0,0,1-.61-2.79q.6-.89,1.26-1.74a2.1,2.1,0,0,0,.38-2.08,7.93,7.93,0,0,1-.22-1.2c-.08-.53-.11-1.08-.23-1.61-.3-1.32-1-2.56-.65-4,.25-1.09,0-1.51-1-2.09a15.13,15.13,0,0,1-2.51-1.56,4.16,4.16,0,0,0-4.45-.8,4.92,4.92,0,0,1-3.68-.27,2.35,2.35,0,0,1-1.66-2.54c.05-.43,0-.89,0-1.32a1.27,1.27,0,0,0-.78-1.38,5,5,0,0,1-2.23-4.81c.09-.77.11-1.91-.35-2.32a26.92,26.92,0,0,0-4.13-2.83,19.77,19.77,0,0,0-2.53-1.11c-.83-.35-1.58-.62-2.38.2a2.71,2.71,0,0,1-1.66.58,5.21,5.21,0,0,0-4.13,3.41,2.51,2.51,0,0,1-2.93,2,1.58,1.58,0,0,0-.85.16c-1.77.83-3.53,1.69-5.31,2.54.1.29.44.77.35,1.15-.4,1.68-.87,3.34-1.43,5-.23.68-.79.67-1.25.09-.32-.4-.65-.79-1-1.19l-.35.15a32.86,32.86,0,0,0,.5,3.28,4.15,4.15,0,0,1-.41,3.38,28.75,28.75,0,0,0-1.85,3.73c-.59,1.37-.2,2.09,1.18,2.6a.75.75,0,0,1,.44.56,18,18,0,0,1-.29,2.85,1.66,1.66,0,0,1-1.69,1.32,10.38,10.38,0,0,0-.22-2.26,2.21,2.21,0,0,0-1.09-1.06c-.53-.27-.73.28-.95.65a2.11,2.11,0,0,1-.45.58,2.17,2.17,0,0,0-.68,2.86,1.54,1.54,0,0,1-.37,2.12,2.82,2.82,0,0,0-.93,2.45c0,.85.17,1.7.22,2.55a5.28,5.28,0,0,0,1.28,3,2.43,2.43,0,0,0,3,1c.26-.07.83.09.83.18a3.58,3.58,0,0,1-.15,1.5c-.06.14-.71,0-1.08,0-.08,0-.15-.06-.23-.09a1.21,1.21,0,0,0-1.84.88,9.8,9.8,0,0,0-.41,1.68c-.14,1.14-.22,2.29-.31,3.44s-1,.93-1.67,1.14-.91-.31-1-.82a9.89,9.89,0,0,0-.44-2.8c-.69-1.31-.09-2.32.3-3.42a5.19,5.19,0,0,0,.23-.62,1.69,1.69,0,0,0-.57-1.84,2.93,2.93,0,0,1-.72-.88,1,1,0,0,0-1.25-.55,3.55,3.55,0,0,1,0,1.31,2.14,2.14,0,0,1-.95.93,12.38,12.38,0,0,1-1.67.45,1.5,1.5,0,0,0-1.22,1.88c.7,0,1.41,0,2.11,0a.91.91,0,0,1,.67.5,6.86,6.86,0,0,1-.29,4.2c-.53,1.17-.53,1.14.41,2.07a2.61,2.61,0,0,1,.63,1.08,18,18,0,0,1,.49,2.29c0,.27,0,.74-.19.81a1.09,1.09,0,0,1-.93-.08c-.21-.13-.3-.5-.41-.77-.48-1.25-.86-2.56-1.45-3.76s-1.48-1.23-2.39-.32C669.69,225,669.35,225.55,668.94,226.08Z"
              style="fill: rgba(255, 255, 255, 0.6);"></path>
    </a>

    <a class="pointer" data-id="home4" onclick="clicked(this)" onmouseout="unhighlightme(this)"
       onmouseover="hightlightme(this)">
        <path style="opacity:0.1;" class="home4 cls-1"
              d="M839.5,413.56c-12-2.07-27-3.91-39-5.79l-40.36-6.27c-6.43-1-12.86-2-19.3-3a2.6,2.6,0,0,1-1.83-1.18q-18.29-25.21-36.62-50.39c-1.19-1.63-1-3.57-.91-5.44a1.36,1.36,0,0,1,.42-.7,3,3,0,0,0,.54-3.9c-.42-.81-.37-1,.49-1.63a3.89,3.89,0,0,1,1,3.8,7,7,0,0,0-.1,2.3c0,.65.32,1.14,1.25,1.08,0-.67-.07-1.32,0-2a2.68,2.68,0,0,1,.32-.8c.25.16.62.27.72.5a2,2,0,0,1,.19,1.19c-.28,1.12.36,1.12,1.34,1.12a2.27,2.27,0,0,1,.28-2.76c.65-.7,1.07-.7,1.31.23a11,11,0,0,1,.14,2.57c0,1.31-.08,2.63,0,3.93.11,1.48.39,2.95.6,4.42a1.27,1.27,0,0,0,1.38,1.16c.49,0,1,.16,1.46.18a3.44,3.44,0,0,1,2.7,1.17,1.2,1.2,0,0,0,1,.34c-.11-.35-.23-.69-.39-1.19,1.5.29,3-.21,4.38.87a3,3,0,0,0,1.76.34c1.5,0,3-.05,4.49-.2a13.51,13.51,0,0,0,2.17-.6c.72-.2,1.44-.42,2.17-.59.43-.11,1,0,1.29-.25a6,6,0,0,1,3.19-1.65.86.86,0,0,0,.54-.3c.94-1.51,2.75-1.92,3.93-3.14a2.49,2.49,0,0,0,1-2,1.22,1.22,0,0,1,.51-1,1.54,1.54,0,0,1,1.21.27c.87.65,1.63,1.43,2.49,2.08.24.18.7,0,1.06.07a8.17,8.17,0,0,1,1.93.16,3.74,3.74,0,0,0,3,0,2.57,2.57,0,0,1,1.4,0,20.14,20.14,0,0,0,3.41.48,11.27,11.27,0,0,0,2.75-.79l.09-.45a14.57,14.57,0,0,0,1,1.23c.67.7,1.41,1.18,2.37.41a.92.92,0,0,1,.59-.16c1.42.09,1.42.1,1.52-1.45a3.88,3.88,0,0,1,1.35.31,6.21,6.21,0,0,1,2,1.82,2.37,2.37,0,0,0,2.71,1.27c1.35-.16,2.69-.42,4-.7,1-.22,2-.44,2.71.64a1.22,1.22,0,0,0,1,.4c1.41-.25,2.23.51,3,1.52.14.19.42.47.54.43,1.3-.43,2.7.32,4-.4A2.93,2.93,0,0,1,787,351c2,0,4,.14,6,.17.21,0,.43-.2.63-.34a7.77,7.77,0,0,1,4.07-1.52c.86,0,1.78-.12,2.09-1.23a.56.56,0,0,1,.5-.22c1.5.67,2.5-.38,3.59-1a19,19,0,0,0,2.86-1.86c1.13-1,2.56-1.27,3.78-1.92s1.83.23,2.68.37c2.76-2.15,4.8-4.56,4.48-8.37l2.1.83c-.19-.57-.33-1-.47-1.44l.2-.19,1.66,1c.27-.89-.12-1.45-.93-1.46a5.72,5.72,0,0,1-3.76-1.6,6.29,6.29,0,0,0-1.47-.67l1.39-.94c.14.24.29.49.58,1l.71-1.72h.25c0,.3.09.6.11.9.08,1.05.5,1.45,1.56,1.47l.42,0c.54.07,1.35.41,1.56.19a17.14,17.14,0,0,0,2.13-2.93c.1-.16-.27-.63-.44-.95s-.25-.37-.38-.56a3.68,3.68,0,0,1,.5-.3,3.42,3.42,0,0,0,1.36-2.66,13.42,13.42,0,0,1-.55-2,2,2,0,0,1,.57-1.28,10.94,10.94,0,0,1,1.78-1.54,1,1,0,0,1,.89-.05,1.2,1.2,0,0,0,1.61.05c.53-.43.23-.8,0-1.26-.11-.21.14-.76.38-1,.51-.41,1.15-.65,1.7-1,1.17-.74,2.3-1.53,3.48-2.25a12.27,12.27,0,0,1,2.37-1.2,6.83,6.83,0,0,0,3.3-1.88,20,20,0,0,1,2.9-2.41,9.57,9.57,0,0,0,2.17-2.63,63.39,63.39,0,0,1,4.25-5.7,1.27,1.27,0,0,1,1-.15,6.44,6.44,0,0,1,.59,1.85c0,.22-.32.5-.5.77.93.15,1.44-.32,1.25-1.16a5.84,5.84,0,0,0-.54-1.36,2,2,0,0,1,1.7-3,5.19,5.19,0,0,1,.14-1.2c.07-.2.48-.47.61-.42,1.21.51,1.15-.46,1.31-1.11a15.31,15.31,0,0,0,.21-1.61l.65.53c1.54-.51,1.17-2,1.7-3.09s.66-2.27,2.11-2.4a5.3,5.3,0,0,1,1.61-3.69,3.22,3.22,0,0,1-.42,2.93c-.2.32-.41,1-.31,1.06a2.05,2.05,0,0,0,1.27.41,2.71,2.71,0,0,0,2.76-2.13,4,4,0,0,1,2.85-2.72c.23-.07.56-.13.65-.29a5,5,0,0,1,2.88-2.21c.12,0,.2-.25.31-.26,2.23-.35,3.2-2.21,4.51-3.68,1-1.12,2.08-2.17,3.16-3.22a4.76,4.76,0,0,0,1.62-2.94l-1.2,1.23-.33-.2a11.94,11.94,0,0,0,3-7.2c1,.89,1.59.43,2.23-.19s.93-.39,1.19.3a6.46,6.46,0,0,1,.55,4.27c-.28,1-.11,2.16-.29,3.22a6.52,6.52,0,0,1-.86,2.4,1.9,1.9,0,0,0,.06,2.56c.88,1.11.86,1.23-.2,2.11-.79.65-.41,1.49-.44,2.25s.06,1.64,1.22,1.71c.24,0,.53.33.67.57a1.69,1.69,0,0,0,2.3.93,2.91,2.91,0,0,1,2,.17c2.61.77,2.51,2.87,2.6,4.85a15.15,15.15,0,0,1-.23,4.67c-.35,1.26.33,2.1.44,3.13a20.31,20.31,0,0,1-.36,3.93c-.23,2.33-.52,4.66-.61,7a7.84,7.84,0,0,0,.68,2.71c.52,1.5.31,1.82-1.24,1.69a3,3,0,0,0-.52,0c-.06,0-.12.06-.19.09s-.07.16,0,.19c.93,1.23.93,1.23-.25,2.29a3.27,3.27,0,0,0-.56.62,2.21,2.21,0,0,1-2.41,1.08c-1.55-.24-2,.15-1.87,1.74a1.13,1.13,0,0,1-.92,1.37,5.41,5.41,0,0,0-1.56,1.18c-.4.33-.75.73-1.16,1.05s-1,.28-1.42-.27c-1-1.24-2-2.41-3-3.64a7.91,7.91,0,0,1-1.14-1.74c-.13-.3.11-.77.18-1.16a5.06,5.06,0,0,1,1,.22,10.71,10.71,0,0,0,1.27.71c.21.06.79-.37.77-.42-.58-1.69-.32-3.74-2.25-4.84-1.36-.77-1.81-.83-2.75.39-.7.93-1.43,1.36-2.59.83-.26-.12-.93,0-1,.22-.63,1.71-2.14,3.22-1.34,5.3a9.73,9.73,0,0,0,.3,1c.85,1.67.34,3.44.38,5.17a1.12,1.12,0,0,1-.36.61c-1,1.07-1,1.06-.06,2.06a3.31,3.31,0,0,1-.4.44,4.53,4.53,0,0,0-1,6.63,6.07,6.07,0,0,1,1.4,4.16,3.62,3.62,0,0,1-1,2.6,11.08,11.08,0,0,0-1.73,2.85c-.09.17.17.67.4.83.42.29,1,.41,1.41.67a2.44,2.44,0,0,0,2.76.05,2.92,2.92,0,0,1,1.08-.39c1.31-.22,1.88.3,1.86,1.62a4,4,0,0,0,1.4,3.47,3.5,3.5,0,0,1,.63.7c.43.62.36,1.1-.43,1.36-2.28.77-4.54,1.57-6.83,2.3-1.16.37-1.45.17-1.73-1.07-.17-.74-.48-.76-1.22-.8-1.85-.09-3.4.68-5,1.29-.56.21-1.1.45-1.66.64-1.4.49-2.8,1-4.22,1.44-.8.24-1.08.64-.62,1.35a3.07,3.07,0,0,0,1.19,1.13,3.07,3.07,0,0,1,1.76,3.37,11.19,11.19,0,0,0-.17,2,7.64,7.64,0,0,0,.63,2.24,5.64,5.64,0,0,1-.14,5.79,22,22,0,0,1-2.27,2.75,8.1,8.1,0,0,0-2.26,3.95,1.85,1.85,0,0,1-1.48,1.58,2.13,2.13,0,0,0-1.16.95,11.17,11.17,0,0,0-1.12,2.7,13,13,0,0,1-3.1,5.76,10.16,10.16,0,0,0-3.14,7.65c0,1.28.08,2.58,0,3.85a8.44,8.44,0,0,0,.48,4.52Zm48.19-111a29.82,29.82,0,0,0,3.74-1.33,2,2,0,0,0,.69-1.56c0-.39-.59-.85-1-1.11a9.12,9.12,0,0,0-1.66-.61c-.8-.29-1.22,0-1.45.81a7.06,7.06,0,0,1-.87,1.45c-.23.37-.71.88-.61,1.1A4.94,4.94,0,0,0,887.69,302.52Z"
              style="fill: rgba(255, 255, 255, 0.6);"></path>
    </a>

    <a class="pointer" data-id="home5" onmouseover="hightlightme(this)">
        <path style="opacity:0.1;" class="home5 cls-1"
              d="M870.69,446.91c4.27,7.23,10.61,17.71,14.14,25.29,1.61,3.45.38,4.88-.65,8.57-4.81,17.27-10.28,34.35-15.57,51.49-2.14,6.92-7.11,19.22-10.08,25.78-1.64,3.63-1.64,2.72-5.13,4-39.51,14-79.07,27.8-118.88,40.88a6.53,6.53,0,0,0,.83,2.56c6.95,15.3,14.08,28.92,20.81,44.31,2.78,6.36,7,12.21,7.78,19.34,0,.36.56.66.81,1a26.55,26.55,0,0,1,2.36,3.57q4.41,9.41,8.62,18.88c1,2.2,2.11,2.72,4.68,2,4.75-1.3,9.71-1.88,14.53-3a11.84,11.84,0,0,0,4.89-2.16,12.71,12.71,0,0,1,7.6-3.4c1.23-.08,2.85-.34,3.56-1.14,2.81-3.1,6.6-3.47,10.33-3.79a106.28,106.28,0,0,1,13-.34c1.78.07,3.49,1.21,5.27,1.77,1.44.45,3,1.23,4.39,1a22.66,22.66,0,0,0,7.38-2.12c5.88-3.15,9.9-7.9,11.17-14.45a5,5,0,0,0-.77-3.27c-1.91-3.12-1.7-4.55,1-7.18a17.49,17.49,0,0,0,4.07-5,8.58,8.58,0,0,1,7.6-5.4c9-.83,17.94-1.42,26.91-2.11.81-.06,1.95.07,2.37-.38,2.8-3,6.1-5.89,7.95-9.44a37.89,37.89,0,0,0,3.37-12.1c.92-6.43,4.78-10.77,10.28-13.38A57.3,57.3,0,0,1,952.07,603a31.65,31.65,0,0,0,6.53-.83c2.62-.45,3.4-1.89,2.71-4.47-1.15-4.3-3.81-8.4-2.06-13.15a1.83,1.83,0,0,0-.28-1.29,13.18,13.18,0,0,1-1.61-8.51c.07-.41-.26-.88-.23-1.31.11-1.51-.11-3.21.55-4.47a33.62,33.62,0,0,0,4.34-15.1c0-1.58,1.22-3.25,2.2-4.67,2.38-3.47,4.95-6.83,7.45-10.23.58-.79,1-2,1.8-2.25a22,22,0,0,1,4.63-.61c-.1,1.23-.13,2.47-.31,3.69-.09.6-.67,1.18-.63,1.75.06.85.18,2,.77,2.47,4,3,10.35,1.08,11.94-3.53a44.21,44.21,0,0,1,2.83-7c2.2-4,4.91-7.77,7-11.83a41.14,41.14,0,0,1,18.87-18.52,7.37,7.37,0,0,0,3-2.56c3.84-6.49,7.52-13.06,11.25-19.62.31-.55.56-1.13.86-1.68a91.83,91.83,0,0,1,4.58-8.17c2.4-3.5,2.22-7.33,1.85-11.13-.1-1-1.59-2-2.69-2.68a9.38,9.38,0,0,0-3.46-.86c-7-.9-11.68-5.25-15.24-10.57-2.29-3.42-3.91-7.18-7.63-9.54-.93-.59-1.44-1.83-2.09-2.8-3.43-5.11-6.8-10.27-10.32-15.32-.62-.9-1.86-1.4-2.83-2.06a18.89,18.89,0,0,1-2.93-1.95,5.35,5.35,0,0,0-6.25-1.39,8.57,8.57,0,0,1-8.64-1.14,12.9,12.9,0,0,0-8.82-2.47,16.46,16.46,0,0,1-5.21-.32c-6.33-1.86-12.53-4.16-18.89-5.89-9.28-2.53-16.1-8-21.59-15.46-1.67-2.25-3.15-4.74-5.22-6.58-5-4.43-8-9.95-10.43-15.92-.79-1.95-1.76-3.84-2.72-5.9-3,2.54-5.64,4.91-8.49,6.94-.86.61-3.25.74-3.54.27a17.71,17.71,0,0,1-1.7-5.1c-.24-.94.15-2.4-.4-2.81a4.5,4.5,0,0,0-3.28-.55c-3,.68-4.65,3.56-4.53,6.83.11,2.74-.13,5.48-.17,8.23,0,2.37,0,4.73,0,7.1,0,1.81.32,3.73-.16,5.42-.58,2,.09,2.67,1.94,2.82.73.06,1.49-.06,2.21,0,2.67.37,2.7,2.74,3.16,4.49.15.59-1.23,2.08-2.15,2.32a28.32,28.32,0,0,1-7.25,1c-3.69,0-6.92.73-9.88,3.55.52.68.9,1.27,1.37,1.78a5,5,0,0,1,.93,5.76q-4.08,9.74-8.33,19.41c-.61,1.39-1.65,2.63-2.13,4.05-1.08,3.18-.11,2.64-.76,5.92Z"
              style="fill: rgba(255, 255, 255, 0.6);"></path>
    </a>

    <a class="pointer" data-id="saudiarab" id="saudi" onmouseover="hightlightme(this)">
        <path style="opacity:0.1;" class="saudiarab cls-1"
              d="M837.13,434.11c4.27,7.23,10.61,17.71,14.14,25.29,1.61,3.45.38,4.88-.65,8.57-4.81,17.27-10.28,34.35-15.57,51.49-2.14,6.92-7.11,19.22-10.08,25.78-1.64,3.63-1.64,2.72-5.13,4-41,14.48-82.06,28.87-123.39,42.38C670.75,600,644,604.1,617,605c-22.46.72-42,9.39-59.36,21.56-13.21,9.27-22.54,23.59-29.45,38.86a21.85,21.85,0,0,1-8.53,9.31c-8.47,4.87-13.54,3.68-19-4.44-3.46-5.17-7.29-6.27-13.06-5.21-3.82.7-7.95,0-11.91-.41-6.64-.62-13.29-1.28-19.88-2.28-4.28-.65-8.47-1.92-12.71-2.89-2.78-.64-5.6-1.88-8.35-1.74-14.26.75-28.57,1.33-42.72,3.08-6.66.82-12.31.49-18-3.12-8.65-5.5-16.92-1.34-18.09,9.12-.33,2.94-.22,6.36,1,9,2.56,5.52,1.07,9.45-3.16,13.13-2.88,2.51-5.45,5.37-8.33,7.88-3.89,3.38-7.4,2.71-9.79-1.75a12.17,12.17,0,0,1-1.59-4.27c-.93-9.18-7.1-15.55-11.85-22.71-1.55-2.33-3.45-4.89-3.74-7.51-.61-5.45-3.41-9.16-7.39-12.39-2.67-2.16-5.15-4.6-8-6.54-9.53-6.53-15.54-15.48-19.35-26.28-1.14-3.25-3.86-5.93-5.75-8.93-1.3-2.06-3-4.09-3.52-6.37a111.63,111.63,0,0,0-17.45-39.45c-4.46-6.33-10.87-11.6-17.25-16.16-5.25-3.74-11.77-5.69-17.69-8.51a12.45,12.45,0,0,1-3.32-2.22c-10.53-10.36-20.22-21.39-26.77-34.79-1.06-2.16-2.21-4.92-1.75-7,1.52-6.93-1.16-12.55-3.79-18.61-1.55-3.58-1.32-8.14-1.14-12.22s1.25-8.29,2-12.43c1.14-6.55-.84-12.28-3.87-18.07-3.86-7.4-6.82-15.26-10.4-22.82-6.47-13.69-13.17-27.34-28.16-33.82-5.78-2.5-9.95-8.4-17.35-7.79-1.36.11-3.28-3.27-4.51-5.31-2.74-4.54-4.89-9.48-7.89-13.83-2.22-3.22-3.11-6-.9-9.41,2.67-4.08,1.93-8-.62-11.76-2.31-3.45-4.57-6.95-7-10.6L102.56,318l-4.9-7.57,6.17-3.33c-1.73-1.24-3.11-2.6-4.76-3.35-4.69-2.12-7.49-5.06-6.29-10.71.24-1.11-.89-2.73-1.76-3.82-2.74-3.47-6.52-6.41-8.34-10.28-6-12.69-13.87-24-22.37-35-5.22-6.81-8.56-15-13.07-22.43S38,207,33.11,200c-.9-1.28-3.23-2.1-4.94-2.17a111.58,111.58,0,0,0-14.28.31c-3.8.33-5-1.94-3.64-4.4,8.86-15.71,6.87-33.89,11.91-50.46,1.55-5.11,4.25-6.71,9.28-5.77,9.19,1.73,18.48,3,27.63,4.93,6.7,1.43,12.67.69,17.4-4.36,5.57-6,10.53-12.49,16.17-18.38,2.74-2.86,6.1-5.7,9.74-7,5.33-1.92,11.14-2.57,16.78-3.53,3.93-.66,6.49-2.44,8-6.33,2.39-6.16,6-11.57,12.15-14.66,3.77-1.88,2.41-4.18.54-6.41-8-9.5-16-19-24-28.45-2.1-2.48-4.26-4.91-6.21-7.5-3.55-4.72-2.92-6.78,2.79-8.59,9.09-2.89,18.24-5.6,27.38-8.35,12.61-3.79,25.24-7.5,37.83-11.34,2.35-.72,5.08-1.36,6.76-2.95,8.73-8.26,18.71-5.36,28.54-3.56,5.81,1.06,11.51,2.9,17.35,3.69,9.24,1.26,16.72,6.08,24.43,10.7,15,9,30.43,17.4,45.3,26.65a254,254,0,0,1,27,19.25c27.62,22.65,54.77,45.86,82.5,68.38,3.81,3.09,9.76,4.43,14.87,4.9,22.05,2,44.17,3.26,66.25,5,9.13.73,18.2,2.09,27.3,3.14,6.57.75,10.06,4.63,11.69,10.85,2.66,10.18,2.77,10.22,13.5,10.63,5.34.2,10.7.21,16.05.19,4.24,0,6.87,2,7.93,6,1.13,4.32,1.94,8.73,3.28,13,.46,1.45,2,2.68,3.24,3.75,4.62,3.92,8.11,8.15,5.87,14.83-.34,1,.1,3.18.76,3.47,3.68,1.59,7.46,3.46,11.37,3.93,5.51.66,5.09,2.7,3.53,4.64,1.69,5.76,3.06,10.44,4.71,16.1,2.75,0,6.5-.67,9.78.28,1.68.49,2.89,3.73,3.68,5.95,1.24,3.5,1.94,7.19,2.87,10.8l1.55.18,5.1-7.67c-1.34,9.13,3.77,13.85,8.94,18.67,4.91,4.58,3.64,10.5-2.84,12.72-4.43,1.52-5.5,4.23-3.25,7.82,4.73,7.53,9.41,15.17,15,22,3.83,4.7,6.64,9.21,8.19,15.26a55.09,55.09,0,0,0,9,18.88c2,2.7,7.52,3.33,11.57,3.84,5,.63,10,.15,15.74.15L679,360.68c7.44-.14,10.52,4.11,13.66,9.38,8.9,14.92,24.55,34.91,34.59,49.06h0c27,5.33,78,13.1,105.1,17.87Z"
              style="fill: rgba(255, 255, 255, 0.6);"></path>
        <path style="" class="cls-1"
              d="M305.87,442h3.7a9.38,9.38,0,0,0-2.52-10c4.1-1.77,5.48-5.15,5.48-9.75l.77,3.93h.49l.76-4.15a9.94,9.94,0,0,0,.75,2.8,12.61,12.61,0,0,0,2.55,2.32l1.5-6.66c-.41,3,.94,4.81,3.47,6.09l.5-1.71c0,1.42-.05,2.65,2.07,2.37l1.71-7.47c-.26,4.29,1.73,6.68,5.28,8.18l.42-2.49H333l.25,2.57c2.7-2.43,2.91-5.49,3.19-8.74.27,1.37.51,2.52.72,3.68a6.91,6.91,0,0,0,3.67,5,2.35,2.35,0,0,0,2-.12c2.52-1.52,3.54-4,3.82-6.79.06-.61.07-1.21.14-2.28.49,3.52.57,6.72,3.31,9.26l.47-3.19.3,3c2.4-.65,2.55-.86,2.65-3,1.44-30.87,5-61.43,13.2-91.32a27.16,27.16,0,0,0,.53-4.26c2.12,9.48,4.68,19.14,6.37,28.95C375.77,371,377.7,384,379,397c1.28,12.45,1.66,25,2.43,37.48,0,.57.07,1.14.1,1.77a9.34,9.34,0,0,0,.94.14,6.29,6.29,0,0,0,.86-.09V399.64l1.53-.37c-.45-1.44.92-3.25-1.19-4.63a28.42,28.42,0,0,1,2.93,0c1.32.15,2.11-.27,2.16-1.76h5.88c.38,2.21.38,2.21,1.39,2.33v1.88l1.62.09c-.06,2.73,2.51,2.19,3.9,3.33V402l1.58.32V432c2,.59,2.26.44,2.27-1.49,0-3.64,0-7.28,0-10.92,0-1.81.19-3.62.33-5.42,0-.34.17-.91.37-1,1.43-.37.95-1.37.93-2.31s.13-2.13.22-3.33h2.59c.82-2.17,1.67-4,2.23-6a21.79,21.79,0,0,0,.53-4.29c.08-1.09,0-2.18,0-3.27l.55,0c0,1.28.06,2.56,0,3.84a10.56,10.56,0,0,0,1.22,5.41,34.11,34.11,0,0,1,1.41,4.28h2.81c0,1.52,0,2.91,0,4.29,0,.32.13.89.3.93,1.37.37,1.15,1.44,1.16,2.41l.33,25.55c0,.36,0,.72.07,1.17h2.38l.15-1.8,8.15-.79c0-4.59,0-9,0-13.33,0-.44-.56-1-1-1.28-2.13-1.66-4.3-3.27-6.48-4.92.2-.23.28-.42.39-.45,2.29-.41,4.21-1.74,6.31-2.6a7.75,7.75,0,0,0,1.88-1.24,1.6,1.6,0,0,1,2.33,0,7.71,7.71,0,0,0,1.71,1.19c2.22,1,4.47,1.94,6.92,3-2.16,1.63-4.19,3.25-6.31,4.73a2.73,2.73,0,0,0-1.26,2.6c.07,3.64,0,7.28,0,10.94a8.39,8.39,0,0,0,7.34-5.67c1.12-3.62,2.26-7.24,3.67-10.83v13.93c.67,0,1.35.12,1.61-.14,1.56-1.58,3.5-1,5.32-1.08,1.14,0,2.28,0,3.44,0,0-1.95,0-3.76,0-5.57,0-2.29-.14-4.59-.08-6.88.11-4.41.21-8.81.49-13.21.36-5.74,1.27-11.44,1.13-17.21,0-.21,0-.55.06-.61,1.9-1.06,1.28-3.09,1.73-4.67,1.13-3.93,2.06-7.93,3.4-11.83-.58,2.59-1.16,5.17-1.75,7.82,1,.42,1.56.28,1.79-.86.4-2,.91-4,1.38-5.94a1.49,1.49,0,0,1,.66-1c-.54,2.32-1,4.65-1.63,6.95-.24.94-.22,1.61.75,2,3.65,1.54,4.49,4.22,4.39,8.13-.3,12.2-.11,24.41-.11,36.62v2h2.47v-1.74c0-6.42-.72-12.94.16-19.24,1.6-11.42,4.73-22.39,15.39-29.22a2.32,2.32,0,0,0,.84-1.61c.12-1.44,0-2.91,0-4.13,1.91,6.21.91,12.8,1.14,19.33a6.67,6.67,0,0,0,.28,2.22c.1-.5.19-1,.32-1.64l2.34-.36v-9.52c2.51.27,4,1.68,5.71,3.26a34.22,34.22,0,0,1,9,13.18c1,2.77,1.61,5.7,2.37,8.57,2.05,7.74.44,15.64.94,23.45.07,1,0,2.07,0,3.2h3.22v-5.66l2.38-.19V400.56l-2.1-.26-.08-.58,2.2-.4a6.16,6.16,0,0,0-.32-1.94c-.87-1.71.24-4-2.13-5.38-1.92-1.1-1.7-2.33-.32-3.95a28.21,28.21,0,0,0,2.3-3.63l-.29-.51,1.71.57c1.37-1.06.55-2.76.56-4.36,0-3.64.16-7.29.17-10.93,0-2.5-.12-5-.2-7.49,0-.92.21-1.43,1.22-1,0,2.4,0,4.78,0,7.16,0,.52-.06,1-.06,1.56,0,4.36-.05,8.73,0,13.09a6.1,6.1,0,0,0,.74,2.06l1.92-.79a6,6,0,0,0,2.08,4.72c1.28,1.18,1.17,2.31-.5,3.48a3.83,3.83,0,0,0-1.87,3.7,15.32,15.32,0,0,1-.72,3.65l2.27.45-.06.54-2.12.26v32.69h3.53c.54-5.8,1.07-11.59,1.59-17.33h7.76c0,2,0,4.07,0,6.18,0,1.15.33,1.65,1.61,1.63,4.54-.08,9.07,0,13.6,0,.41,0,.81-.05,1.35-.09v-2.9l2.49-.14V351.9h22.47v71.67h2.32c.16-3.34.32-6.65.48-10.08h6c.4,6.3.8,12.56,1.24,19.29a41.74,41.74,0,0,1,5.08-1.23c2.72-.32,4.19-3.15,6.92-3.24.55.22,1.09.46,1.64.67a6.42,6.42,0,0,1,2,.82c2.59,2.39,5.86,2.39,9,2.83,2.41.33,4.8.85,7.26,1.29l-7.38,6v2h9.53V461.3H306.12c0-2.68,0-5.32,0-8a10.35,10.35,0,0,0-.24-1.63Zm63-93.17c.17-.34-.87-.52-.68-.91.27.31,2,.31,2.23.63-.1-2.5-.25-2.65-2.23-2.16v1.53l1,.88c-1.84.12-.91,1.35-1,1.85l2.74.39C371.1,349.71,370.67,348.64,368.89,348.84ZM555,355.6c.47,2.85.83,5.48,1.33,8.09.94,4.81,2,9.6,4.28,14,.64,1.22,1.34,3,2.89,2.65a5.06,5.06,0,0,0,2.79-2.63c3.47-6.9,4.52-14.44,5.61-22.12A39.32,39.32,0,0,0,555,355.6Zm-71.15,33.31A2.14,2.14,0,1,0,486,391.1,2.14,2.14,0,0,0,483.88,388.91ZM364,366.76l-2.86-4.95-.66,4.95Zm10.05,0-.71-4.86-2.83,4.86Zm-5.95-12.58c.61,1.81,2,2.39,4.12,1.89l-.42-1.89Zm-5.76,2c2.41.33,3.6-.26,3.89-1.83C363.11,354.3,362.7,353.8,362.37,356.15Zm.59-2.51c1.17-.45,2.18-.52,3.36-1V351.6C363.32,351.59,363.07,350.92,363,353.64Zm.65-2.65a9.86,9.86,0,0,1,1.45-.12,8.52,8.52,0,0,1,1.31.08,9.49,9.49,0,0,0,0-1c0-.33-.1-.67-.14-1C363.63,348.65,363.43,348.63,363.61,351Zm4.91,1.76,3.29,1.18-.54-2.4h-3.1c0,.45,0,.81,0,1.17a3.8,3.8,0,0,0,.13.57C367.84,352.71,367.84,352.48,368.52,352.75Zm11.27,65-.25-3.27-2.32,2.37Zm-13.36-69.52-.09-1.88c-2.17-.72-2.18.64-2.32,2ZM355,414.61l-.23,3.14,2.54-.92Zm14.88-68.77c-.2-2.24-.24-2.28-1.62-2v2Zm5.9,48.71,2.2.71L377.6,392Zm-19.24.69,2.18-.69L356.94,392Zm9.8-51.48c-1.53-.06-1.76.29-1.5,2.17l1.5-.12Z"
              id="fig" xmlns="http://www.w3.org/2000/svg" style="fill: rgba(255, 255, 255, 0.04);"></path>
    </a>

</svg>

<div class="mypopup" id="saudiarab" style="display: none;">
    <h3>{{__('Saudi Arabia')}}</h3>
    <div class="line mpni"></div>
</div>
<div class="mypopup" id="home1" style="display: none;top: -4px !important;">
    <h3>{{__('Kuwait')}}</h3>
    <div class="line mpni"></div>
    <img src="{{ asset('img/kuwait.png') }}" style="max-width: 200px;">
</div>
<div class="mypopup" id="home2" style="display: none;">
    <h3>{{__('Bahrain')}} </h3>
    <div class="line mpni"></div>
    <img src="{{ asset('img/bahrain.png') }}" style="max-width: 200px;">
</div>
<div class="mypopup" id="home3" style="display: none;">
    <h3>{{__('Qatar')}} </h3>
    <div class="line mpni"></div>
    <img src="{{ asset('img/qatar.png') }}" style="max-width: 200px;">
</div>
<div class="mypopup" id="home4" style="display: none;">
    <h3>{{__('UAE')}} </h3>
    <div class="line mpni"></div>
    <img src="{{ asset('img/uae.png') }}" style="max-width: 200px;">
</div>
<div class="mypopup" id="home5" style="display: none;">
    <h3>{{__('Oman')}} </h3>
    <div class="line mpni"></div>
    <img src="{{ asset('img/oman.png') }}" style="max-width: 200px;">
</div>
</div>
      </div>-->
        </div>
    </div>
</div>
