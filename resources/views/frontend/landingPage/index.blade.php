<!DOCTYPE html>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" {{ (str_replace('_', '-', app()->getLocale()) == 'zh') || (str_replace('_', '-', app()->getLocale()) == 'ar') ? "dir=rtl" : "dir=ltr" }}>

<head>
    <meta charset="utf-8">
    <title>{{ config('app.name') }}</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <link rel="shortcut icon" href="{{ asset('img/ic_favi.png') }}">
    <!-- Bootstrap CSS File -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />
	<!-- Bootstrap CSS File -->
	<link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{ asset('lib/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Libraries CSS Files -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- Main Stylesheet File -->
    <link href="{{ asset('css/developer.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet">
    <link href="{{ asset('css/waitMe.min.css') }}" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
<script type="text/javascript">
    var WEBSITE_URL="{{url('/').'/'.app()->getLocale()}}";
 </script>
<style type="text/css">

footer.fixed-bottom p {
    text-align: center;
    margin: 0;
}
.navbar {padding: 0.5rem 1rem !important;}
footer.fixed-bottom{
  bottom: -23px !important;
}

.intro-sec-text{width: 85%;padding-left:6%;}

</style>
    <style>
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  -webkit-appearance: none;
  margin: 0;
}

/* Firefox */
input[type=number] {
  -moz-appearance: textfield;
}
</style>

</head>
<body class="landingpageBody" >
    <section class="home-bannar">
       <div class="col">
        
       <!--  <video autoplay muted loop id="myVideo">
          <source src="video.mp4" type="video/mp4">
        </video> -->
        <img src="{{url('uploads/banner/'.$media->image)}}" style="height: 120vh !important">
        <div clss="top-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 mpn">
                        @include('frontend.landingPage.header')
                    </div>
                </div>
            </div>
        </div>
        
        @include('frontend.landingPage.intro')
        <input type="hidden" value="{{@$status}}" id="status">
        <footer class="no-bg small-none fixed-bottom ">
            <nav class="navbar navbar-expand-lg footer">
                <div class="navbar-collapse justify-content-md-center">
                    <ul class="navbar-nav" >
                         <!-- @foreach(getAllLocations() as $row)
                         <li class="nav-item"><a class="nav-link pointer" href="{{url(app()->getLocale().'/home/'.$row->name)}}" data-id="home4" onclick="clicked(this)" onmouseout="unhighlightme(this)" onmouseover="hightlightme(this)">{{$row->name}}</a>
                        </li>
                         @endforeach -->
                        <!-- <li class="nav-item"><a class="nav-link pointer" href="{{url(app()->getLocale().'/home/UAE')}}" data-id="home4" onclick="clicked(this)" onmouseout="unhighlightme(this)" onmouseover="hightlightme(this)">{{__('UAE')}}</a>
                        </li>
                        <li class="nav-item"><a class="nav-link pointer" href="{{url(app()->getLocale().'/home/Kuwait')}}" data-id="home1" onclick="clicked(this)" onmouseout="unhighlightme(this)" onmouseover="hightlightme(this)">{{__('Kuwait')}} </a>
                        </li>
                        <li class="nav-item"><a class="nav-link pointer" href="{{url(app()->getLocale().'/home/KSA')}}" data-id="saudiarab" onclick="clicked(this)" onmouseout="unhighlightme(this)" onmouseover="hightlightme(this)">{{__('KSA')}} </a>
                        </li>
                        <li class="nav-item"><a class="nav-link pointer" href="{{url(app()->getLocale().'/home/Oman')}}" data-id="home5" onclick="clicked(this)" onmouseout="unhighlightme(this)" onmouseover="hightlightme(this)">{{__('Oman')}} </a>
                        </li>
                        <li class="nav-item"><a class="nav-link pointer" href="{{url(app()->getLocale().'/home/Qatar')}}" data-id="home3" onclick="clicked(this)" onmouseout="unhighlightme(this)" onmouseover="hightlightme(this)">{{__('Qatar')}} </a>
                        </li>
                        <li class="nav-item"><a class="nav-link pointer" href="{{url(app()->getLocale().'/home/Bahrain')}}" data-id="home2" onclick="clicked(this)" onmouseout="unhighlightme(this)" onmouseover="hightlightme(this)">{{__('Bahrain')}} </a>
                        </li> -->
                    </ul>

                     <!-- <ul class="navbar-nav">
                    
                    <li class="nav-item"><a class="nav-link pointer alertmsg" href="#" data-id="home4" onmouseout="unhighlightme(this)" onmouseover="hightlightme(this)">{{__('UAE')}}</a>
                    </li>
                    <li class="nav-item"><a class="nav-link pointer" href="{{url(app()->getLocale().'/home/Kuwait')}}" data-id="home1" onclick="clicked(this)" onmouseout="unhighlightme(this)" onmouseover="hightlightme(this)">{{__('Kuwait')}} </a>
                    </li>
                    <li class="nav-item"><a class="nav-link pointer alertmsg" href="#" data-id="saudiarab" onmouseout="unhighlightme(this)" onmouseover="hightlightme(this)">{{__('KSA')}} </a>
                    </li>
                    <li class="nav-item"><a class="nav-link pointer alertmsg" href="#" data-id="home5" onmouseout="unhighlightme(this)" onmouseover="hightlightme(this)">{{__('Oman')}} </a>
                    </li>
                    <li class="nav-item"><a class="nav-link pointer alertmsg" href="#" data-id="home3" onmouseout="unhighlightme(this)" onmouseover="hightlightme(this)">{{__('Qatar')}} </a>
                    </li>
                    <li class="nav-item"><a class="nav-link pointer alertmsg" href="#" data-id="home2" onmouseout="unhighlightme(this)" onmouseover="hightlightme(this)">{{__('Bahrain')}} </a>
                    </li>
                </ul> -->

                </div>

            </nav>

        <!--  <p>* Disclaimer: The depiction of boundaries, geographic names and related data shown on this map are not warranted to be error free nor do they imply an official expression of opinion or endorsement by Thegulfroad.com</p> -->
        </footer>


        <div class="mobilesocialmedia">
            <ul class="">
                <li class="">
                    <a class="fb-xfbml-parse-ignore nav-link" href="/">
                    <i class="fa fa-facebook-official"></i>
                    </a>
                </li>
                <li class="">
                    <a class="fb-xfbml-parse-ignore nav-link" href="/">
                    <i class="fa fa-linkedin-square"></i>
                    </a>
                </li>
               <!--  <li class="">
                    <a class="fb-xfbml-parse-ignore nav-link" href="/">
                    <i class="fa fa-google"></i>
                    </a>
                </li> -->
                <li class="">
                    <a class="fb-xfbml-parse-ignore nav-link" href="/">
                    <i class="fa fa-twitter"></i>
                    </a>
                </li>
                <li class="">
                    <a class="fb-xfbml-parse-ignore nav-link" href="/">
                    <i class="fa fa-whatsapp"></i>
                    </a>
                </li>

                <li class="">
                    <a class="fb-xfbml-parse-ignore nav-link" href="https://thegulfroad.com/uploads/app/XQC3ir4H0nDI9TaJgdPL.apk">
                    <i class="fa fa-android"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div> 
    </section>

    @include('frontend.auth.modals.login')
    @include('frontend.auth.modals.subscribe')
    @include('frontend.auth.modals.enquiry')
    @include('frontend.auth.modals.homeloan')
    @include('frontend.auth.modals.register')
    @include('frontend.auth.modals.skip')
    
    @include('frontend.auth.modals.forgot-password')

    <script src="{{asset('lib/prefixfree.min.js')}}"></script>
    <script src="{{asset('lib/jquery-3.3.1.slim.min.js')}}"></script>
    <script src="{{ asset('js/script.js') }}"></script>
    <script type="text/javascript" src="{{asset('lib/jquery.min.js')}}"></script>
    <script src="{{ asset('js/waitMe.min.js') }}"></script>
    <script src="{{ asset('js/developer.js') }}"></script>
    <script src="{{ asset('lib/bootstrap.bundle.min.js') }}"></script>
     
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet">
    <script src="{{asset('lib/sweetalert2.js')}}"></script>
    <script src="{{ asset('js/landingpage.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $(".alertmsg").on("click",function(){ 
                alert("Stay tuned we will be live shortly in other regions. Work in progress except Kuwait. Sorry for inconvenience");
            });
        //     $('.country-code-js').select2({
        //     	'width':'100%',
        //     	'height':'40px'
        //     });
        var status=$("#status").val();
        if(status==1){
            Swal.fire({
                title: 'Error!',
                text: 'Your account is blocked by admin',
                icon: 'error',
                confirmButtonText: 'Ok'
            }).then((result) => {
                $("#status").val(0);
                window.location.href = "{{route('home','en')}}";
            });
        }
        });
    </script>
</body>
</html>
