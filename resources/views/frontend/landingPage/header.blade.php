<link rel="shortcut icon" href="{{ asset('img/ic_favi.png') }}">
<style>
.goog-logo-link {
   display:none !important;
}

.goog-te-gadget {
   color: transparent !important;
}

.goog-te-gadget .goog-te-combo {
   margin: 0px 0!important;
}
@media (max-width: 767px)
{
    .navbar-brand img.img-responsive2 {
    min-height: unset;
    width: 250px;
    
}
}

 

</style>
<header class="navbar navbar-expand" style="padding: 10px 0px;">
    <div class="col-lg" style="padding: 0px 0px;">
        <a class="navbar-brand mr-0 mr-md-2" href="{{url(app()->getLocale().'/home')}}">
            <img class="img-responsive2" src="{{ asset('img/the-gulf-road-final-logo.png') }}" style="width: 250px;" >
        </a>
    </div>
    <div class="col-lg  text-right">
        <ul class="navbar-nav justify-content-end">
            <li class="nav-item social">
                <a class="fb-xfbml-parse-ignore nav-link" target="_blank" href="https://www.facebook.com/The-Gulf-Road-108377097722424/">
                    <i class="fa fa-facebook-official"></i>
                </a>
            </li>
            <li class="nav-item social">
                <a class="fb-xfbml-parse-ignore nav-link" target="_blank" href="https://www.linkedin.com/in/thegulfroad-tgr-01122a1b8/">
                    <i class="fa fa-linkedin-square"></i>
                </a>
            </li>
            <!-- <li class="nav-item social">
                <a class="fb-xfbml-parse-ignore nav-link" target="_blank" href="https://google.com">
                    <i class="fa fa-google"></i>
                </a>
            </li> -->
            <li class="nav-item social">
                <a class="fb-xfbml-parse-ignore nav-link" target="_blank" href="https://twitter.com/thegulfroad">
                    <i class="fa fa-twitter"></i>
                </a>
            </li>
            <li class="nav-item social">
                <a class="fb-xfbml-parse-ignore nav-link" target="_blank" href="https://web.whatsapp.com/">
                    <i class="fa fa-whatsapp"></i>
                </a>
            </li>
            <li class="nav-item social">
                <a  title="Download App" class="fb-xfbml-parse-ignore nav-link" download="thegulfroad.apk" target="_blank" href="{{url('uploads/app/XQC3ir4H0nDI9TaJgdPL.apk')}}">
                    <i class="fa fa-android"></i>
                </a>
            </li>
            <li class="nav-item">
                <div id="google_translate_element2" style="width: 147px;"></div>
            </li>
            @guest
                <li class="nav-item login-menu">
                    <a href="javascript:;" onclick="showLoginForm();">{{__('Log in')}}</a>
                </li>
                <li class="nav-item register-menu">
                    <a href="javascript:;" onclick="showRegisterForm();">{{__('Register')}}</a>
                </li>
            @endguest
        </ul>
    </div>
</header>
<script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script> 
<script type="text/javascript">  
	function googleTranslateElementInit() {  
			new google.translate.TranslateElement( 
					{pageLanguage: 'en', includedLanguages : 'en,zh-CN,ph,fr,ar'},  
					'google_translate_element2' 
			);  
	}  
</script> 
<script>
                window.addEventListener('load', () => {
                      
                registerSW();
                });

                // Register the Service Worker
                async function registerSW() {
                if ('serviceWorker' in navigator) {
                    try {
                        console.log('SW registration Success');
                    await navigator
                            .serviceWorker
                            .register('serviceworker.js');
                    }
                    catch (e) {
                    console.log('SW registration failed');
                    }
                }
                }
            </script> 