@extends('frontend.layouts.home')
@section('title','Contact Us')
@section('content')
@include('frontend.layouts.default-header')
<style>
#loading {
position: fixed;
width: 100%;
height: 100vh;
background: #fff url('images/loader.gif') no-repeat center center;
z-index: 9999;
}

.intl-tel-input #phone {
    width: 25em;
}

.goog-te-gadget .goog-te-combo{
  color:black !important;
}
  </style>
  <div id="loading"></div>
<!-- contact ineer -->
<script
    src="https://www.paypal.com/sdk/js?client-id={{env('SB_CLIENT_ID')}}"> // Required. Replace SB_CLIENT_ID with your sandbox client ID.
  </script>

@php

    $link=Request::segment(3);
    $subscriptions = \App\Models\Subscription::where('type','premium')->get();

    $currency = 'USD';
    $currency_code = 'USD';
    if($link=='uae')
    {
        $currency = 'AED';
        $currency_code = 'AED';

    }
    elseif($link=='kuwait')
    {
         $currency = 'KD';
         $currency_code = 'KWD';
         //$currency_code = 'USD';// because in indian stripe account is not support kwd currency 
    }
    elseif($link=='ksa')
    {
         $currency = 'SAR';
         $currency_code = 'SAR';
    }
    elseif($link=='oman')
    {
         $currency = 'OR';
         $currency_code = 'OMR';
    }
    elseif($link=='qatar')
    {
        $currency = 'KD';
        $currency_code = 'QAR';
    }
    elseif($link=='kuwait')
    {
         $currency = 'QR';
         $currency_code = 'KWD';
    }
    elseif($link=='bahrain')
    {
         $currency = 'BD';
         $currency_code = 'BHD';
    }
  //$currency_code = 'USD';
@endphp

<section class="plans-and-pricing mt-5 bg-white package" >
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h4 class="mb-2">View one of the following plans to get started.</h4>
        <p class="mb-5">Enjoy unlimited property listing as per the plan and then upgrade and elevate yourself.</p>
      </div>

      @foreach($subscriptions as $subs)
      <div class="col-md-4">
        <div class="price-box">
          <div class="pt-5 pb-4 pl-4 pr-4">
          <div class="text-center">
            <img src="{{url('uploads/subscription/'.$subs->logo)}}"  style="height: auto;width: 50%;">
            <h5>{{$subs->name}}</h5>
          </div>
          <p style="text-align: center; margin-top: 15px;">Validity Days: {{$subs->sub_plan[0]['validity']}}</p>
          <p>@php echo $subs->content; @endphp</p>
          <hr class="mt-4">

          @php
            $amount = $subs->sub_plan[0]['amount'];
            $current_exchange = @Currency::convert()
              ->from('USD')
              ->to($currency_code)
              ->amount($amount)
              ->get();
              $current_exchange  = round($current_exchange);
            @endphp
          <h3 class="text-center">{{@$currency_code}} {{$current_exchange}}</h3>
        </div>
          <!-- <button>Continue with Standard</button> -->
        </div>
      </div>
     @endforeach
    </div>
  </div>
</section>




<script src='https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js'></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<!-- <script src="js/script.js"></script> -->
<!-- <script src="lib/jquery.min.js"></script> -->
<!-- <script src="lib/bootstrap.bundle.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/owl.carousel.min.js"></script>
<script type="text/javascript">
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('bg-header');
    } else {
       $('header').removeClass('bg-header');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('fixed-top');
    } else {
       $('header').removeClass('fixed-top');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('#top-header-bar').removeClass('fixed-top');
    } else {
       $('#top-header-bar').addClass('fixed-top');
    }
});
  
</script>
<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery('#loading').fadeOut(1000);
});

</script>

@foreach($subscriptions as $subs)
<script>
  paypal.Buttons({
    style:{
      color: 'white',
      layout: 'horizontal',
      tagline: false,
      shape:   'rect'
    },
    createOrder: function(data, actions) {
      // This function sets up the details of the transaction, including the amount and line item details.
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: '{{$subs->sub_plan[0]['amount']}}'
          }
        }]
      });
    },
    onApprove: function(data, actions) {
      // This function captures the funds from the transaction.
      return actions.order.capture().then(function(details) {
        // This function shows a transaction success message to your buyer.
        // alert('Transaction completed by ' + details.payer.name.given_name);
        var ajax_url = WEBSITE_URL+"/list/premium?is_premium={{$subs->_id}}";
            $.ajax({
              url : ajax_url,
              method:"get",
              data:{
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success : function(data){
                  if(data.status){
                        Swal.fire({
                          title: 'Success!',
                          text: 'Transaction completed successfully',
                          icon: 'success',
                          confirmButtonText: 'Ok'
                      }).then((result) => {
                    if(result.value){
                        $(".package").hide();
                        $(".submit_step_8").trigger('click');
                    }
                });
                  }else{
                      Swal.fire({
                          title: 'Error!',
                          text: data.message,
                          icon: 'error',
                          confirmButtonText: 'Ok'
                      });
                  }
              }
          });       
        });
      }        
  }).render('#paypal-cospace-{{$subs->_id}}');
  //This function displays Smart Payment Buttons on your web page.
</script>

@endforeach

@include('frontend.layouts.footer')
