@extends('frontend.layouts.home')
@section('title','About Us')
@section('content')
@include('frontend.layouts.default-header')
<style>
#loading {
position: fixed;
width: 100%;
height: 100vh;
background: #fff url("{{asset('/img/loading.gif')}}") no-repeat center center;
z-index: 9999;
}
.who-we-are-wrap1 p
{
  text-align: justify;
}
  </style>
  <div id="loading"></div>
<!-- Bannar Section -->
<section class="about-us-page">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h4>Privacy Policy</h4>
        <!-- <p>Deals in the renting or purchase of house, furniture, electronics,<br> cars and many more. Explore a suitable job in middle east and<br> happening events to keep you entertaining.</! -->
      </div>
    </div>
  </div>
</section>
<!-- Next Sectin -->
<section class="who-we-are-wrap1 mt-3">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h4>Privacy Policy</h4>
        <p>Below we set out our privacy policy which will govern the way in which we process any personal information that you provide to us. We will notify you if the way in which we process your information is to change at any time.</p>
        <p>You can access our home page and browse our site without disclosing your personal data save information collected by cookies that we use (see below). </p>
        <h4>Who may process data</h4>
        <p>Your personal information (which includes your name, address and any other details you provide to us which concern you as an individual, Agent or Buddy) may be processed both by us and by other companies within our group. Our website also includes a link to Rackspace and such company may collect personal data about visitors to our site. Each of the companies authorised to process your information as mentioned above will do so in accordance with this privacy policy.</p>
        <h4>Purpose of processing</h4>
        <p>We will use your information for the purpose of fulfilling service orders placed by you, processing any other transactions authorised or made by you with us, informing you of special offers and providing other marketing information to you which we think you may find of interest, undertaking services or customer research/development.</p>
        <h4>Disclosure of information</h4>
        <p>In the unlikely event that a liquidator, administrator or receiver is appointed over us or all or any part of our assets that insolvency practitioner may transfer your information to a third party purchaser of the business provided that purchaser undertakes to use your information for the same purposes as set out in this policy. We undertake not to provide your personal information to third parties save in accordance with this policy. Your information will not be disclosed to government or local authorities or other government institutions save as required by law or other binding regulations.</p>
        <h4>Cookies</h4>
        <p>We may send a small file to your computer when you visit our website. This will enable us to identify your computer, track your behaviour on our website and to identify your particular areas of interest so as to enhance your future visits to this website. We may use cookies to collect and store personal data and we link information stored by cookies with personal data you supply to us. Save for the use of cookies, we do not automatically log data or collect data save for information you specifically provide to us. You can set your computer browser to reject cookies but this may preclude your use of certain parts of this website.</p>
        <p>Third party vendors, including Google, use cookies to serve ads based on a user's prior visits to your website. Google's use of the DART cookie enables it and its partners to serve ads to your users based on their visit to your sites and/or other sites on the Internet. Users may opt out of the use of the DART cookie by visiting the advertising opt-out page.</p>
        <h4>Security measures</h4>
        <p>We have implemented security policies, rules and technical measures to protect the personal data that we have under our control from unauthorised access, improper use and disclosure, unauthorised destruction or accidental loss.</p>
        <h4>Access to information</h4>
        <p>You may ask us whether we are storing personal information about you by emailing our admin department via the Contact page and, if you wish and upon payment of a fee of $10, we will provide you with a copy of the personal data we hold about you by email. We may ask for proof of your identity before providing any information and reserve the right to refuse to provide information requested if identity is not established.</p>
        <h4>Copyright</h4>
        <p>All website design, text, graphics, the selection and arrangement there of are Copyright © {{date('Y')}}, THEGULFROAD.com, ALL RIGHTS RESERVED.</p>
        <h4>Trademarks</h4>
        <p>THEGULFROAD.com is a trademark of THEGULFROAD Ltd or its subsidiaries and may be registered in certain parts of the world.</p>
        <h4>Disclaimer of warranty and liability</h4>
        <p>The following provisions may be curtailed or disallowed by the laws of certain jurisdictions. In such case, the terms hereof are to be read as excluding or limiting such term so as to satisfy such law.</p>
        <p>We do not represent or warrant that the information accessible via this website is accurate, complete or current. We have no liability whatsoever in respect of any use which you make of such information.</p>
        <p>The information provided on this website has not been written to meet your individual requirements and it is your sole responsibility to satisfy yourself prior to ordering any products or services from us that they are suitable for your purposes.</p>
        <p>Whilst we make all reasonable attempts to exclude viruses from the website, we cannot ensure such exclusion and no liability is accepted for viruses. Thus, you are recommended to take all appropriate safeguards before downloading information or images from this website.</p>
        <p>All warranties, express or implied, statutory or otherwise are hereby excluded.
Neither we nor any of our employees or affiliated entities will be liable for any kind of damages and howsoever arising including, without limitation, loss of profits, compensatory, consequential, direct, exemplary, incidental, indirect, punitive or special, damages or any liability which you may have to a third party, even if we have been advised of the possibility of such loss.
</p>
<p>We are not responsible for the direct or indirect consequences of you linking to any other website from this website.

</p>
        <h4>Consent and inquiries</h4>
        <p>In order to access the information on this website, you must signal acceptance of the terms and disclaimer set out above. If you do not accept any of these terms, leave this website now. If you have any enquiry or concern about our privacy policy or the way in which we are handling personal data please contact our admin department via the Contact page. If at any time you wish us to cease processing your information please send a message to our admin department and insert "unsubscribe" as the subject heading.</p>
      </div>
    </div>
  </div>
</section>
<!-- Next Section -->
@include('frontend.layouts.footer')


<script src='https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js'></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="{{asset('js/script.js')}}"></script> 
<script src="{{asset('lib/bootstrap.bundle.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/owl.carousel.min.js"></script>
<script language="javascript">
  function toggleDiv(divid)
  {
  
    varon = divid + 'on';
  varoff = divid + 'off';

  if(document.getElementById(varon).style.display == 'none')
  {
  document.getElementById(varon).style.display = 'block';
  document.getElementById(varoff).style.display = 'none';
  }
  
  else
  { 
  document.getElementById(varoff).style.display = 'block';
  document.getElementById(varon).style.display = 'none';
  }
} 
</script>
<script type="text/javascript">
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('.header').addClass('bg-header');
    } else {
       $('.header').removeClass('bg-header');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('fixed-top');
    } else {
       $('header').removeClass('fixed-top');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('#top-header-bar').removeClass('fixed-top');
    } else {
       $('#top-header-bar').addClass('fixed-top');
    }
});
  
</script>
<script type="text/javascript">
  jQuery("#testimonial").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  // autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:2,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 2,
      nav:true
    },

    1366: {
      items: 2,
      nav:true
    }
  }
});
  jQuery("#owl-example-second").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  // autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:4,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 4,
      nav:true
    },

    1366: {
      items: 4,
      nav:true
    }
  }
});
</script>
<script type="text/javascript">
  jQuery("#owl-example").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  // autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:4,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 4,
      nav:true
    },

    1366: {
      items: 4,
      nav:true
    }
  }
});
  jQuery("#carouselone").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:3,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 3,
      nav:true
    },

    1366: {
      items: 3,
      nav:true
    }
  }
});
  jQuery(".owl-carousel").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  // autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:3,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 3,
      nav:true
    },

    1366: {
      items: 3,
      nav:true
    }
  }
});
jQuery(document).ready(function() {
    jQuery('#loading').fadeOut(1000);
});
</script>
@endsection
