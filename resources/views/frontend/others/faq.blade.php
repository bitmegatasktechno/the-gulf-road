@extends('frontend.layouts.home')
@section('title','Find A Question')
@section('content')
@include('frontend.layouts.default-header')
<style>
#loading {
position: fixed;
width: 100%;
height: 100vh;
background: #fff url('{{asset('/img/loading.gif')}}') no-repeat center center;
z-index: 9999;
}
  </style>
  
  <div id="loading"></div>
   <!-- Content Area -->
   <div class="sub_page pt-lg-5 mt-5">
      <!-- FAQs Content Section -->
      <section class="faqs-content">
         <div class="container">
          <div class="row">
            <div class=col-md-12>
              <h1 class="mb-3">FAQ's</h1>
            </div>
          </div>
            
            <div class="row">
               <div class="col">
                  <div class="accordion" id="faq-collapes">
                     
                     <div class="card border-0">
                       <div class="card-header bg-transparent border m-0" id="">
                         <h5 class="mb-0 position-relative">
                           <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#faq_two" aria-expanded="false" aria-controls="faq_two">
                              Buy / Rent 
                           </button>
                         </h5>
                       </div>
                       <div id="faq_two" class="collapse" aria-labelledby="" data-parent="#faq-collapes">
                         <div class="card-body">
                          <p >1. Flexible listing on the global marketplace</p>
                          <p >2. More search options</p>
                          <p >3. Connections and chat options</p>
                          <p >4. Location indicators</p>
                          <p >5. Cost estimation tools</p>
                         </div>
                       </div>
                     </div>
                     <div class="card border-0">
                       <div class="card-header bg-transparent border m-0" id="">
                         <h5 class="mb-0 position-relative">
                           <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#faq_three" aria-expanded="false" aria-controls="faq_three">
                               Agent / Buddy
                           </button>
                         </h5>
                       </div>
                       <div id="faq_three" class="collapse" aria-labelledby="" data-parent="#faq-collapes">
                         <div class="card-body">
                          <p >1. Dedicated login and portal</p>
                          <p >2. Listing of properties under portfolio</p>
                          <p >3. Individual listing to offer your other services</p>
                          <p >4. Chance to become buddy and earn more</p>
                         </div>
                       </div>
                     </div>

                     <div class="card border-0">
                        <div class="card-header bg-transparent border m-0" id="">
                          <h5 class="mb-0 position-relative">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#faq_four" aria-expanded="false" aria-controls="faq_four">
                              Co-space  
                            </button>
                          </h5>
                        </div>
                        <div id="faq_four" class="collapse" aria-labelledby="" data-parent="#faq-collapes">
                          <div class="card-body">
                            <p >1. Dedicated login and portal</p>
                            <p >2. Listing of properties under portfolio</p>
                            <p >3. Individual listing to offer your other services</p>
                            <p >4. Chance to become buddy and earn more</p>
                          </div>
                        </div>
                      </div>
                      <div class="card border-0">
                        <div class="card-header bg-transparent border m-0" id="">
                          <h5 class="mb-0 position-relative">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#faq_five" aria-expanded="false" aria-controls="faq_five">
                              Swap 
                            </button>
                          </h5>
                        </div>
                        <div id="faq_five" class="collapse" aria-labelledby="" data-parent="#faq-collapes">
                          <div class="card-body">
                            <p >1. Dedicated login and portal</p>
                            <p >2. Listing of properties under portfolio</p>
                            <p >3. Custom listing and pricing plans</p>
                            <p >4. Flexible listing on the global marketplace</p>
                            <p >5. More search options</p>
                            <p >6. Connections and chat options</p>
                            <p >7. Location indicators</p>
                          </div>
                        </div>
                      </div>
                      <div class="card border-0">
                        <div class="card-header bg-transparent border m-0" id="">
                          <h5 class="mb-0 position-relative">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#faq_six" aria-expanded="false" aria-controls="faq_six">
                              Others 
                            </button>
                          </h5>
                        </div>
                        <div id="faq_six" class="collapse" aria-labelledby="" data-parent="#faq-collapes">
                          <div class="card-body">
                            <p >1. Thegulfroad.com's social-distancing and other COVID-19-related guidelines apply</p>
                            <p >2. To protect your payment, never transfer money or communicate outside of the thegulfroad.com website.</p>
                          </div>
                        </div>
                      </div>
                   </div>
               </div>
            </div>
         </div>
      </section>
   </div>
   
@include('frontend.layouts.footer')


<script src='https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js'></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="{{asset('js/script.js')}}"></script> 
<script src="{{asset('lib/bootstrap.bundle.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/owl.carousel.min.js"></script>
<script language="javascript">
  function toggleDiv(divid)
  {
  
    varon = divid + 'on';
  varoff = divid + 'off';

  if(document.getElementById(varon).style.display == 'none')
  {
  document.getElementById(varon).style.display = 'block';
  document.getElementById(varoff).style.display = 'none';
  }
  
  else
  { 
  document.getElementById(varoff).style.display = 'block';
  document.getElementById(varon).style.display = 'none';
  }
} 
</script>
<script type="text/javascript">
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('.header').addClass('bg-header');
    } else {
       $('.header').removeClass('bg-header');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('fixed-top');
    } else {
       $('header').removeClass('fixed-top');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('#top-header-bar').removeClass('fixed-top');
    } else {
       $('#top-header-bar').addClass('fixed-top');
    }
});
  
</script>
<script type="text/javascript">
  jQuery("#testimonial").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  // autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:2,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 2,
      nav:true
    },

    1366: {
      items: 2,
      nav:true
    }
  }
});
  jQuery("#owl-example-second").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  // autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:4,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 4,
      nav:true
    },

    1366: {
      items: 4,
      nav:true
    }
  }
});
</script>
<script type="text/javascript">
  jQuery("#owl-example").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  // autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:4,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 4,
      nav:true
    },

    1366: {
      items: 4,
      nav:true
    }
  }
});
  jQuery("#carouselone").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:3,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 3,
      nav:true
    },

    1366: {
      items: 3,
      nav:true
    }
  }
});
  jQuery(".owl-carousel").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  // autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:3,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 3,
      nav:true
    },

    1366: {
      items: 3,
      nav:true
    }
  }
});
jQuery(document).ready(function() {
    jQuery('#loading').fadeOut(1000);
});
</script>
@endsection
