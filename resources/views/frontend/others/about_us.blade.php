@extends('frontend.layouts.home')
@section('title','About Us')
@section('content')
@include('frontend.layouts.default-header')


@php
$lang=Request::segment(1);
$loc=Request::segment(3);
if($loc == 'ksa' || $loc == 'uae' )
{
   $loc21 = strtoupper($loc);
}else 
{
    $loc21 = ucfirst($loc);
}
$link=$loc; 

 
@endphp
<style>
#loading {
position: fixed;
width: 100%;
height: 100vh;
background: #fff url("{{asset('/img/loading.gif')}}") no-repeat center center;
z-index: 9999;
}
.about-us-page {
    padding: 135px 0 90px 0;
}
.about-cont p {
    text-align: justify;
}
.who-we-are-wrap {
    padding: 90px 0 90px;
}
.tqafb {
    padding: 80px 0;
    background: #f1f1f1;
}
.our-vision {
    padding: 90px 0;
}
.abot-mission {
    background: #f1f1f1;
}
@media only screen and (max-width: 768px){

  .about-us-page {
    padding: 100px 0 50px 0;
}
.who-we-are-wrap {
    padding: 30px 0;
}
.who-we-are-wrap h4, .wrapp_second h4 {
    font-size: 30px;
}
.our-vision p {
    font-size: 30px;
}
form#aboutUsForm img {
    padding: 10px;
}
.form-control {
    padding: 0.375rem 0rem;
    padding-right: 16px;
}
.abot-mission p {
    font-size: 20px;
    text-align: justify;
}
}
  </style>
  
  <div id="loading"></div>
<!-- Bannar Section -->
<section class="about-us-page">
  <div class="container">
    <div class="row"> 
      <div class="col-md-12">
        <h4>An attractive perspective of<br> the middle east</h4>
        <p>Deals in listing of homes, agents and buddies to help you rent, buy or swap your house in Middle East. Explore best co-space for your office. The unique feature of distance and cost calculator differentiates us and thegulfroad.com is your one stop shop to settle in a new country.</p>
      </div>
    </div>
  </div>
</section>
<!-- Next Sectin -->
<section class="who-we-are-wrap">
  <div class="container fluidy">
    <div class="row">
      <div class="col-md-6 about-cont"> 
        <h4>Who We Are</h4>
        <p>The Gulf road is a middle east crafted platform which provides the smartest way to search for every type of property for rent, buy or swap in UAE and other GCC countries. With the largest selection of properties, hotel apartments and other resources, The Gulf Road is where people and property connect.</p>
        <p>When you are searching for a property for rentals; finding the right property can be a frustrating, confusing and time-intensive process and in some cases looking overseas just makes it much harder! The Gulf Road provides hand-selected group of ‘Gulf Buddies’ experienced multilingual professionals with deep knowledge about your searched place provides assistance to tenants in finding the appropriate properties making it the most easy, quick and comprehensive experience to them. </p>
        <p>Tenants in search of rental accommodations will save time with the ability to browse through thousands of listings from hundreds of landlords all in one space supported by buddies and registered pre-approved agents landing at righteous properties. </p>
        <h4>Find the Right Property</h4>
        <p>We deal in listing of properties for rent, sale  co-space and swap in middle east and we are happy to help in finding your dream home.</p>
      </div>
      <div class="col-md-6 about-cont-display">
        <div class="about-right-img">
          <img src="{{asset('img/landing-page/about-6.png')}}" alt="About the Gulf Road" class="img-rounded">
      </div>
      </div>      
    </div>
  </div>
</section>
<!-- Next Section -->

<!-- <section class="wrapp_second">
  <div class="container">
    <div class="row">
      <div class=" col-md-6">
        <h4>Find the Right Property</h4>
        <p>We deal in listing of properties for rent, sale  co-space and swap in middle east and we are happy to help in finding your dream home.</p>
      </div>
    </div>
  </div>
</section> -->

<!-- Next section -->
<section class="tqafb">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h4>Top Quality Agents, Friendly Buddies at your<br> Service, Anytime</h4>
        <p>Agents and Buddies help you find the Right place to stay</p>
      </div>
      <div class="offset-md-1 col-md-10">
        <div class="row">
          <div class="col-md-6 text-center">
            <img class="mb-4 mt-4" src="{{asset('img/landing-page/businessman.png')}}">
            <h5>Top Quality<br> Real Estate Agents</h5>
            <span>Top Rated Real Estate agents provide you with the Right home that you can truely call Yours</span>
          </div>
          <div class="col-md-6 text-center">
            <img class="mb-4 mt-4" src="{{asset('img/landing-page/high-five.png')}}">
            <h5>Fun & Friendly <br> Buddies</h5>
            <span>Buddies are People with a Great Knoweledge about a Neighbourhood that you can cosult</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Next section -->
<section class="our-vision">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h4 class="text-center">Our Vision</h4>
        <p class="text-center"><span>Hassle free search</span> for not only a place of staying but  a place of living!  </p>
        <div class="main-search mt-5 p-0">
        <div class="row align-items-center">
        <div class="col-md-3 col-sm-6 col-12 col-lg-3 ">
 			<select class="selectpicker select-bord bord-right mr-2" onChange="$('#aboutUsForm').attr('action','{{url(app()->getLocale().'/buy-property/')}}'+'/'+$(this).val());">
         <option value="">Select Country</option>
            @foreach(getAllLocations() as $row)
                <option value="{{strtolower($row->name)}}">{{ucfirst($row->name)}}</option>
            @endforeach
			</select>        
        </div>
        <div class="col-md-9 col-sm-6 col-12 col-lg-9 pl-0">
          <form id="aboutUsForm" class="form-inline flex-nowrap justify-content-between p-0 w-100" method="get" action="{{url(app()->getLocale().'/buy-property/'.$link)}}">	
          <div id="map" style="display:none"></div>	  
			<img class="" src="{{asset('img/search.png')}}">
            
            <input class="form-control mr-sm-2 form-cont" type="text" id="address" onChange="getLocation()" placeholder="“Search properties by location ”" name="location">
                                            <input type="hidden" name="longitude" id="longitude">
                                            <input type="hidden" name="latitude" id="latitude">
            <span class="register-menu list-property p-0" >
              <button class="list-property-button" type="submit">Search</button>
            </span>
          </form>
        </div>
        </div>
      </div>
      </div>
      <div class="col-md-12">
        <div class="row mt-5">
          <div class="col-sm-3">
            <img class="img-fluid rounded" src="{{asset('img/landing-page/about-1.webp')}}">
          </div>
          <div class="col-sm-3">
            <img class="img-fluid rounded" src="{{asset('img/landing-page/swap-house-image.webp')}}">
          </div>
          <div class="col-sm-3">
            <img class="img-fluid rounded" src="{{asset('img/landing-page/Bitmap22.webp')}}">
          </div>
          <div class="col-sm-3">
            <img class="img-fluid rounded" src="{{asset('img/landing-page/agentsbuildings.webp')}}">
          </div>
        </div>
         
      </div>
    </div>
  </div>
</section>
<!-- Next Section -->
<section class="abot-mission">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h4>Our Mission</h4>
        <p>To be the first choice for our consumers and partners in their journey of discovering and renting a place. We do that with data and technology and above all the passion of our buddies, while delivering value to our customers. </p>
      </div>
    </div>
  </div>
</section>
@include('frontend.layouts.footer')


<script src='https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js'></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="{{asset('js/script.js')}}"></script> 
<script src="{{asset('lib/bootstrap.bundle.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/owl.carousel.min.js"></script>
<script language="javascript">
  function toggleDiv(divid)
  {
  
    varon = divid + 'on';
  varoff = divid + 'off';

  if(document.getElementById(varon).style.display == 'none')
  {
  document.getElementById(varon).style.display = 'block';
  document.getElementById(varoff).style.display = 'none';
  }
  
  else
  { 
  document.getElementById(varoff).style.display = 'block';
  document.getElementById(varon).style.display = 'none';
  }
} 
</script>
<script type="text/javascript">
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('.header').addClass('bg-header');
    } else {
       $('.header').removeClass('bg-header');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('fixed-top');
    } else {
       $('header').removeClass('fixed-top');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('#top-header-bar').removeClass('fixed-top');
    } else {
       $('#top-header-bar').addClass('fixed-top');
    }
});
  
</script>
<script type="text/javascript">
  jQuery("#testimonial").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  // autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:2,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 2,
      nav:true
    },

    1366: {
      items: 2,
      nav:true
    }
  }
});
  jQuery("#owl-example-second").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  // autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:4,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 4,
      nav:true
    },

    1366: {
      items: 4,
      nav:true
    }
  }
});
</script>
<script type="text/javascript">
  jQuery("#owl-example").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  // autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:4,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 4,
      nav:true
    },

    1366: {
      items: 4,
      nav:true
    }
  }
});
  jQuery("#carouselone").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:3,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 3,
      nav:true
    },

    1366: {
      items: 3,
      nav:true
    }
  }
});
  jQuery(".owl-carousel").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  // autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:3,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 3,
      nav:true
    },

    1366: {
      items: 3,
      nav:true
    }
  }
});
jQuery(document).ready(function() {
    jQuery('#loading').fadeOut(1000);
});
</script>




    @if(env('APP_ACTIVE_MAP') =='patel_map')
<style>
    #address_result
    {
        z-index: 9;
    }
</style>
   <script src="https://mapapi.cloud.huawei.com/mapjs/v1/api/js?callback=initMap&key={{env('PATEL_MAP_KEY')}}"></script>
    <script>
        function initMap() {
            var mapOptions = {};
            mapOptions.center = {lat: 48.856613, lng: 2.352222};
            mapOptions.zoom = 8;
            var searchBoxInput = document.getElementById('address');
             var map = new HWMapJsSDK.HWMap(document.getElementById('map'), mapOptions);
            // Create the search box parameter.
            var acOptions = {
                location: {
                    lat: 48.856613,
                    lng: 2.352222
                },
                radius: 10000,
                customHandler: callback
            };
            var autocomplete;
            // Obtain the input element.
            // Create the HWAutocomplete object and set a listener.
            autocomplete = new HWMapJsSDK.HWAutocomplete(searchBoxInput, acOptions);
            autocomplete.addListener('site_changed', printSites);
            // Process the search result. 
            // index: index of a searched place.
            // data: details of a searched place. 
            // name: search keyword, which is in strong tags. 
            function callback(index, data, name) {
                console.log(index, data, name);
                var div
                div = document.createElement('div');
                div.innerHTML = name;
                return div;
            }
            // Listener.
            function printSites() {
                 
                
                var site = autocomplete.getSite();
                var latitude = site.location.lat;
                var longitude = site.location.lng;
                var country_name = site.name;
                
                var results = "Autocomplete Results:\n";
                var str = "Name=" + site.name + "\n"
                    + "SiteID=" + site.siteId + "\n"
                    + "Latitude=" + site.location.lat + "\n"
                    + "Longitude=" + site.location.lng + "\n"
                    + "Address=" + site.formatAddress + "\n";

                    
                results = results + str;

                $('#latitude').val(latitude);
                $('#longitude').val(longitude);
                $('#countryTo').val(country_name);
                $('#country_name').val(country_name);
                console.log(latitude, longitude);
                /*console.log(results);*/
            }
            
        }
        function getLocation()
        {

        }
    </script> 

     
    @elseif(env('APP_ACTIVE_MAP')=='google_map')
     
    
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&callback=initMap&libraries=places&v=weekly"
      defer
    ></script>
 
<script>

function initMap() {
  const myLatLng = {
    lat: -25.363,
    lng: 131.044
  };
    
      

  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 4,
    center: myLatLng
  });
    var input = document.getElementById('address');

 map.controls[google.maps.ControlPosition.TOP_RIGHT].push();

var autocomplete = new google.maps.places.Autocomplete(input);
autocomplete.bindTo('bounds', map);
autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);
            var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29),
          title: "Hello World!"
        });
        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindow.open(map, marker);
        });
  new google.maps.Marker({
    position: myLatLng,
    map,
    title: "Hello World!"
  });
}

function getLocation()
 {
//   alert(address);
  var address = $('#address').val();
  // alert(address);
  var geocoder = new google.maps.Geocoder();

  geocoder.geocode( { 'address': address}, function(results, status)
  {


    if (status == google.maps.GeocoderStatus.OK)
    {
      console.log(results[0],"afdcfsa");
      console.log(results[0].address_components);
      const countryObj=results[0].address_components.filter(val=>{
            if(val.types.includes('country')){
                return true
            }
            return false;
      });
     console.log(countryObj,'the country obj');


     var country_name = countryObj[0].long_name;
     console.log(country_name,'name');
      var latitude = results[0].geometry.location.lat();

      var longitude = results[0].geometry.location.lng();
      // alert(latitude +"-----"+ longitude);
     $('#latitude').val(latitude);
     $('#longitude').val(longitude);
     $('#countryTo').val(country_name);
     $('#country_name').val(country_name);
    console.log(latitude, longitude);
    }
  });

}
    </script>

    
   @endif

@endsection
