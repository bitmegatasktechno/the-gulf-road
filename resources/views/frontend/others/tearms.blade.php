@extends('frontend.layouts.home')
@section('title','About Us')
@section('content')
@include('frontend.layouts.default-header')
<style>
#loading {
position: fixed;
width: 100%;
height: 100vh;
background: #fff url("{{asset('/img/loading.gif')}}") no-repeat center center;
z-index: 9999;
}
  </style>
  <div id="loading"></div>
<section class="about-us-page">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h4>Terms of Use</h4>
      </div>
    </div>
  </div>
</section>
<section class="who-we-are-wrap1 mt-3">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h4>Effective Date: - 1 March, 2020</h4>
        <p>• If you wish to use this Website, you must agree to the terms below as the exclusive basis which governs usage of the Website. If you do not agree to any of the terms, do not use this Website.</p>
        <p>• THIS AGREEMENT is made between THEGULFROAD Web Publishing and you ("the User"). </p>
        <h4>DEFINITIONS</h4>
        <p>• "Effective Date" - the date on which this set of terms and conditions entered effect. </p>
        <p>• "Material" - content published on the Website or otherwise provided to Thegulfroad. For the avoidance of doubt, it includes all content posted on the Website by the User or otherwise provided to THEGULFROAD by the User.</p>
        <p>• "Registration Details" - the details which a User must provide on registering for the Website including name, phone numbers, email address, age or address.</p>
        <p>• "Service" - the provision of the Website as a property portal, Agent Portal, Buddy Portal and Travel information portal.</p>
        <p>• "Unacceptable" - Material which under the laws of any jurisdiction from which the Website may be accessed may be considered either:-• Illegal, illicit, indecent, obscene, racist, offensive, pornographic, insulting, false, unreliable, misleading, alleged to be or actually defamatory or in infringement of third party rights (of whatever nature and including, without limitation, any Intellectual Property Rights).</p>
        <p>•	In breach of any applicable regulations, standards or codes of practice (not with standing that compliance may not be compulsory).</p>
        <p>• To contravene legislation, including without limitation, that relating to weapons, animals or alcohol might harm THEGULFROAD's reputation.</p>
        <p>•	"User"- any party who uses the Website.</p>
        <p>•	"Website" - THEGULFROAD's website located at www.THEGULFROAD.com</p>
        <p>•	"THEGULFROAD " - THEGULFROAD Web Publishing Group which is the owner of the Website and whose registered office is World trade center towers.</p>
        <h4>TERMS WHICH APPLY TO USERS</h4>
        <p>•	In registering for this Website, the User must provide true, accurate, current and complete Registration Details which the User must update after any changes (except age) before using the Website for further services in the future.</p>
        <p>•	The User hereby warrants to THEGULFROAD that it is at least eighteen years of age and legally able to enter into contracts.</p>
        <p>THEGULFROAD reserves the discretion to withdraw any Material from the Website without prior notice and to refuse any Material posted by a User.</p>
        <p>•	The User's Registration Details and data relating to its use of the Website will be recorded by THEGULFROAD but this information shall not be disclosed to third parties (otherwise than on an aggregated, anonymous basis) nor used for any purpose unrelated to the Website.</p>
        <p>•	THEGULFROAD may send a small file to the User's computer when it visits the Website. This "cookie" will enable THEGULFROAD to identify the User's computer, track its behaviour on the Website and to identify the User's particular areas of interest so as to enhance the User's future visits to the Website. The cookie will not enable THEGULFROAD to identify the User and THEGULFROAD shall not use it otherwise than in relation to this Website. The User can set its computer browser to reject cookies but this may preclude use of certain parts of this Website.</p>
        <p>•	The User hereby authorises THEGULFROAD to use any information which it submits to this Website to inform the User of special offers, occasional third party offers and for other marketing and related purposes. THEGULFROAD will not use User data for any other purposes than as set out in this Agreement except that THEGULFROAD may disclose this data if compelled to do so by law, or at the request of a law enforcement agency or governmental authority.</p>
        <p>• If the User does not wish THEGULFROAD to use its information as set out in Clauses 2.3 and 2.4 above, it should leave the Website before submitting its Personal details.</p>
        <p>•	If the User does not want THEGULFROAD to use its email address or SMS to send information concerning the Website and related matters, the User should send a message to THEGULFROAD and insert unsubscribe as the subject heading.</p>
        <p>•	By agreeing to the terms, you give us permission to verify the authenticity of your details by calling you at the submitted phone number, the call will be recorded for quality assurance.</p>
        <p>•	THEGULFROAD reserves the right to suspend or terminate a User's account where, in its absolute discretion, it deems such suspension appropriate. In the event of such suspension or termination, THEGULFROAD will notify the User by email and the User must not seek to re-register either directly or indirectly through a related entity.</p>
        <p>•	For the avoidance of doubt, THEGULFROAD is providing a service not goods.</p>
        <p>•	THEGULFROAD owns all Intellectual Property Rights in the Website and the Service, including without limitation, the design, text, graphics, the selection and arrangement thereof.</p>
        <p>•	THEGULFROAD takes reported and actual infringement of Intellectual Property Rights and fraud extremely seriously and whilst Users cannot hold THEGULFROAD liable in relation to such issues, THEGULFROAD requests all Users to report such matters immediately and THEGULFROAD shall inform the appropriate authorities.</p>
        <p>•	Users will be invited to send comments to THEGULFROAD by email relating to the integrity and performance of other Users.</p>
        <h4>	THE RESTRICTIONS APPLY TO ALL USERS</h4>
        <p>•	User agrees not to transmit any material designed to interrupt, damage, destroy or limit the functionality of the Website or the Service.</p>
        <p>•	User agrees not to use any automated software to view the Service without consent and to only access the Service manually.</p>
        <p>•	User agrees not to use the Service other than for its own personal use or as an agent listing properties for sale and to rent.</p>
        <p>•	User agrees not to attempt to copy any Material or reverse engineer any processes without THEGULFROAD's consent.</p>
        <p>•	User agrees not to use any Service in any manner that is illegal, immoral or harmful to THEGULFROAD.</p>
        <p>•	User agrees not to use any Service in breach of any policy or other notice on the Website.</p>
        <p>•	User agrees not to remove or alter any copyright notices that appear on the Website.</p>
        <p>•	User agrees not to publish any Material that may encourage a breach of any relevant laws or regulations.</p>
        <p>•	User agrees not to interfere with any other User's enjoyment of the Website or the Service.</p>
        <p>•	User agrees not to transmit materials protected by copyright without the permission of the owner.</p>
        <p>•	User agrees not to conduct itself in an offensive or abusive manner whilst using the Website or the Service.</p>
        <p>•	By submitting Material on the Website or otherwise, User grants THEGULFROAD a royalty-free, perpetual, irrevocable and non-exclusive right and license to use, reproduce, distribute, display, modify and edit the Material. THEGULFROAD will not pay the User any fees for the Material and reserves the right in its sole discretion to remove or edit the Material at any time. User also warrants and represents that it has all rights necessary to grant THEGULFROAD these rights.</p>
        <p>•	THEGULFROAD permits the User to post Material on the Website in accordance with THEGULFROAD's procedures provided that Material is not illegal, obscene, abusive, threatening, defamatory or otherwise objectionable to THEGULFROAD.</p>
        <h4>LIMITATION OF LIABILITY</h4>
        <p>•	THEGULFROAD is not liable for any indirect loss, consequential loss, loss of profits, revenue, data or goodwill howsoever arising suffered by any User arising in any way in connection with this Agreement or for any liability of a User to any third party.</p>
        <p>•	Whilst THEGULFROAD will make all reasonable attempts to exclude viruses from the Website, it cannot ensure such exclusion and no liability is accepted for viruses. Thus, the User is recommended to take all appropriate safeguards before downloading information or any Material from the Website.</p>
        <p>•	THEGULFROAD shall not be liable for ensuring that the Material on the Website is not Unacceptable Material and the User in making any financial or other decision accepts that it does so exclusively at its own risk.</p>
        <p>•	THEGULFROAD shall not be liable for any interruption to the Service, whether intentional or otherwise.</p>
        <p>•	THEGULFROAD is not liable for any failure in respect of its obligations hereunder which result directly or indirectly from failure or interruption in software or services provided by third parties. </p>
        <p>THEGULFROAD is not responsible for the direct or indirect consequences of a User linking to any other website from the Website.</p>
        <p>•	None of the clauses herein shall apply so as to restrict liability for death or personal injury resulting from the negligence of THEGULFROAD or its appointed agents.</p>
        <p>• No matter how many claims are made and whatever the basis of such claims, THEGULFROAD's maximum aggregate liability to a User under or in connection with this Agreement in respect of any direct loss (or any other loss to the extent that such loss is not excluded by Clauses 3.1-3.6 above or otherwise) whether such claim arises in contract or in tort shall not exceed a sum equal to twice the value of any amount paid to THEGULFROAD by the User in relation to which such claim arises.</p>
        <h4>WARRANTIES AND INDENTITY</h4>
        <p>•	THEGULFROAD does not represent or warrant that the information accessible via the Website is accurate, complete or current. THEGULFROAD has no liability whatsoever in respect of any use which the User makes of such information.</p>
        <p>• Material has not been written to meet the individual requirements of the User and it is the User's sole responsibility to satisfy itself prior to entering into any transaction or decision that the Material is suitable for its purposes.</p>
        <p>• All warranties, express or implied, statutory or otherwise are hereby excluded.</p>
        <p>• The User hereby agrees to indemnify THEGULFROAD against all liabilities, claims and expenses that may arise from any breach of this Agreement by the User.</p>
        <h4>GENERAL</h4>
        <p>•	Subject to Clause 5.2, this written Agreement and any other expressly incorporated document constitute the entire agreement between the parties hereto relating to the subject matter hereof and neither party has relied on any representation made by the other party unless such representation is expressly included herein. Nothing in this Clause 5.1 shall relieve either party of liability for fraudulent misrepresentations and neither party shall be entitled to any remedy for either any negligent or innocent misrepresentation except to the extent (if any) that a court or arbitrator may allow reliance on the same as being fair and reasonable.</p>
        <p>•	THEGULFROAD reserves the right to alter its terms of business from time to time. The Effective Date at the time the User is reading these terms is set out at the top of this Agreement. Prior to using the Website again in the future, Users should check that the effective date has not altered. If it has, the User should examine the new set of terms and only use the Website if it accepts the new terms.</p>
        <p>•	If any provision of this Agreement or part thereof shall be void for whatever reason, it shall be deemed deleted and the remaining provisions shall continue in full force and effect.</p>
        <p>•	THEGULFROAD reserves the right to assign or subcontract any or all of its rights and obligations under this Agreement.</p>
        <p>•	The User may not assign or otherwise transfer its rights or obligations under this Agreement without THEGULFROAD's prior written consent.</p>
        <p>•	Any notice given pursuant hereto may be served personally or by email to the last known email address of the addressee. It is the responsibility of Users promptly to update THEGULFROAD of any change of address or email address. Such notice shall be deemed to have been duly served upon and received by the addressee, when served personally, at the time of such service, when sent by email 24 hours after the same shall has been sent, or if sent by post 72 hours after put into the post correctly addressed and pre-paid.</p>
        <p>•	THEGULFROAD shall not be liable for any loss suffered by the other party or be deemed to be in default for any delays or failures in performance hereunder resulting from acts or causes beyond its reasonable control or from any acts of God, acts or regulations of any governmental or supra-national authority.</p>
        <p>•	Any delay or forbearance by THEGULFROAD in enforcing any provisions of this Agreement or any of its rights hereunder shall not be construed as a waiver of such provision or right thereafter to enforce the same.</p>
        <p>•	The headings in this Agreement are solely used for convenience and shall not have any legal or contractual significance.</p>
        <p>•	This Agreement shall be governed by and construed in accordance with the laws of Dubai, and the parties submit to the non-exclusive jurisdiction of the Courts of Dubai, save that THEGULFROAD may take action in any relevant jurisdiction to enforce its Intellectual Property Rights.</p>
      </div>
    </div>
  </div>
</section>
@include('frontend.layouts.footer')
<script src='https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js'></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="{{asset('js/script.js')}}"></script> 
<script src="{{asset('lib/bootstrap.bundle.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/owl.carousel.min.js"></script>
<script language="javascript">
  function toggleDiv(divid)
  {
  
    varon = divid + 'on';
  varoff = divid + 'off';

  if(document.getElementById(varon).style.display == 'none')
  {
  document.getElementById(varon).style.display = 'block';
  document.getElementById(varoff).style.display = 'none';
  }
  
  else
  { 
  document.getElementById(varoff).style.display = 'block';
  document.getElementById(varon).style.display = 'none';
  }
} 
</script>
<script type="text/javascript">
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('.header').addClass('bg-header');
    } else {
       $('.header').removeClass('bg-header');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('fixed-top');
    } else {
       $('header').removeClass('fixed-top');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('#top-header-bar').removeClass('fixed-top');
    } else {
       $('#top-header-bar').addClass('fixed-top');
    }
});
  
</script>
<script type="text/javascript">
  jQuery("#testimonial").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  // autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:2,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 2,
      nav:true
    },

    1366: {
      items: 2,
      nav:true
    }
  }
});
  jQuery("#owl-example-second").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  // autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:4,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 4,
      nav:true
    },

    1366: {
      items: 4,
      nav:true
    }
  }
});
</script>
<script type="text/javascript">
  jQuery("#owl-example").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  // autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:4,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 4,
      nav:true
    },

    1366: {
      items: 4,
      nav:true
    }
  }
});
  jQuery("#carouselone").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:3,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 3,
      nav:true
    },

    1366: {
      items: 3,
      nav:true
    }
  }
});
  jQuery(".owl-carousel").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  // autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:3,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 3,
      nav:true
    },

    1366: {
      items: 3,
      nav:true
    }
  }
});
jQuery(document).ready(function() {
    jQuery('#loading').fadeOut(1000);
});
</script>
@endsection
