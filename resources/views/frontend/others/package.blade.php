<script
    src="https://www.paypal.com/sdk/js?client-id={{env('SB_CLIENT_ID')}}"> // Required. Replace SB_CLIENT_ID with your sandbox client ID.
  </script>

  <style type="text/css">
 .plans-and-pricing .price-box h5 {
    position: relative;
    top: 28px;
    left: 0;
    right: 0;
    margin: 0 auto !important;
    width: calc(100% - 50px) !important;
    color: #000000;
    font-size: 21px;
    font-weight: bold;
    letter-spacing: 0;
    text-transform: uppercase;
}

.plans-and-pricing .price-box p {
    margin-top: 30px;
}
.box-design {
    margin-bottom: 30px;
}
.plans-and-pricing .price-box {
    box-shadow: 0 2px 5px 2px rgb(64 60 67 / 20%);}
  </style>

@php
    $subscriptions = \App\Models\Subscription::where('type','premium')->get();


    $currency = 'USD';
    $currency_code = 'USD';
    if($link=='uae')
    {
        $currency = 'AED';
        $currency_code = 'AED';

    }
    elseif($link=='kuwait')
    {
         $currency = 'KD';
         $currency_code = 'KWD';
         //$currency_code = 'USD';// because in indian stripe account is not support kwd currency 
    }
    elseif($link=='ksa')
    {
         $currency = 'SAR';
         $currency_code = 'SAR';
    }
    elseif($link=='oman')
    {
         $currency = 'OR';
         $currency_code = 'OMR';
    }
    elseif($link=='qatar')
    {
        $currency = 'KD';
        $currency_code = 'QAR';
    }
    elseif($link=='kuwait')
    {
         $currency = 'QR';
         $currency_code = 'KWD';
    }
    elseif($link=='bahrain')
    {
         $currency = 'BD';
         $currency_code = 'BHD';
    }
  //$currency_code = 'USD';
@endphp

<section class="plans-and-pricing mt-5 bg-white package" style="display: none;">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h4 class="mb-2">Do you want to make your property premium?</h4>
        <p class="mb-5">Enjoy various options to list your property and showcase in Thegulfroad boutique. Premium properties have the highest chance of being picked with only a fraction of the cost. We'll lead you the way home.</p>
      </div>


      @foreach($subscriptions as $subs)
      <div class="col-md-4 box-design">
        <div class="price-box" style="padding: 0">
          <div class="pt-5 pb-4 pl-4 pr-4">
          <div class="text-center">
            <img src="{{url('uploads/subscription/'.$subs->logo)}}" style="height: auto;width: 50%;">
            <div class="price-title"><h5>{{$subs->name}}</h5></div>
          </div>
          <p style="text-align: center;">Validity Days: {{$subs->sub_plan[0]['validity']}}</p>
          <p>@php echo $subs->content; @endphp</p>
          <hr class="mt-4">
          <h3 class="text-center">
            <div id="paypal-cospace-{{$subs->_id}}"></div>
            @php
            $amount = $subs->sub_plan[0]['amount'];
            $plannumber = $subs->sub_plan[0]['number'];
            $current_exchange = @Currency::convert()
              ->from('USD')
              ->to($currency_code)
              ->amount($amount)
              ->get();
              $current_exchange  = round($current_exchange);
            @endphp
            {{@$currency_code}} {{$current_exchange}}</h3>
            @if($current_exchange)

            @php
            $sku_id = array('plan_id'=>$subs->_id,'plan_amount'=>$current_exchange,'currency'=>$currency_code,'number'=>$plannumber);
            $sku_encoded = json_encode($sku_id);
            $sku_encoded_base = base64_encode($sku_encoded);
            @endphp
            
            <!-- <button type="button" class="btn btn-danger btn-block" data-subsid="{{$subs->_id}}" onclick="buy_premium('{{$subs->_id}}','{{$subs->sub_plan[0]['amount']}}')" data-toggle="modal" data-target="#StripePremiumCardModal" data-premiumamount{{$subs->_id}}="{{$subs->sub_plan[0]['amount']}}">Stripe - Pay Online</button> -->

            <button type="button" class="btn btn-danger btn-block"  onclick="stripePopupPremium('{{$subs->_id}}','{{$current_exchange}}','{{$sku_encoded_base}}')"  >Stripe - Pay Online</button>
          @else

          <button type="button" onClick="countinuefree1('{{$subs->_id}}')" style="display: block;">Continue</button>
          @endif
        </div>
          <!-- <button>Continue with Standard</button> -->
        </div>
      </div>
     @endforeach
    </div>

     <div class="row mt-5">
        <div class="col-md-5">
          <a class="mt-2 dp-inline-block" href="javascript:;" onclick="showScreen('step4');">< Go back</a>
        </div>
        <div class="col-md-6 text-right">
          <button class="red-btn rounded float-right submit_step_8" type="button">
            Continue <img class="filter-white" src="{{asset('img/black_arrow.png')}}">
          </button>
        </div>
    </div>
  </div>
</section>


    <script src='https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js'></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<!-- <script src="js/script.js"></script> -->
<!-- <script src="lib/jquery.min.js"></script> -->
<!-- <script src="lib/bootstrap.bundle.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/owl.carousel.min.js"></script>
<script type="text/javascript">


    //This function displays Smart Payment Buttons on your web page.
  function countinuefree1(id)
  {
      var ajax_url = WEBSITE_URL+"/list/premium?is_premium="+id;
            $.ajax({
              url : ajax_url,
              method:"get",
              data:{

              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              success : function(data){
                  if(data.status){
                        Swal.fire({
                          title: 'Success!',
                          text: 'Transaction completed successfully',
                          icon: 'success',
                          confirmButtonText: 'Ok'
                      }).then((result) => {
                    if(result.value){
                        $(".package").hide();
                        $(".submit_step_8").trigger('click');
                    }
                });
                  }else{  
                      Swal.fire({
                          title: 'Error!',
                          text: data.message,
                          icon: 'error',
                          confirmButtonText: 'Ok'
                      });
                  }
              }
          }); 
  }



  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('bg-header');
    } else {
       $('header').removeClass('bg-header');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('fixed-top');
    } else {
       $('header').removeClass('fixed-top');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('#top-header-bar').removeClass('fixed-top');
    } else {
       $('#top-header-bar').addClass('fixed-top');
    }
});
  
</script>
<script type="text/javascript">

jQuery(document).ready(function() {
    jQuery('#loading').fadeOut(1000);

  
});
function buy_premium(valuess,amount)
  {
        
    $('#prem_sub11').val(valuess);
    $('#prem_amount11').val(amount);
   }
</script>
<script type="text/javascript">

$(function() {

  


    var $form = $(".require-validation");
    $('form.premium-require-validation').bind('submit', function(e) {
        var $form = $(".premium-require-validation"),
            inputSelector = ['input[type=email]',
                            'input[type=password]',
                            'input[type=text]',
                            'input[type=file]',
                            'textarea'].join(', '),
            $inputs       = $form.find('.required').find(inputSelector),
            $errorMessage = $form.find('.inp-error'),
            valid         = true;
            $errorMessage.addClass('d-none');
        $('.has-error').removeClass('has-error');

        $inputs.each(function(i, el) {
            var $input = $(el);
            if ($input.val() === '') {
                $input.parent().addClass('has-error');
                $errorMessage.removeClass('d-none');
                e.preventDefault();
            }
        });

        if (!$form.data('cc-on-file')) {
            var StripeKey = "{{env('STRIPE_KEY')}}";

            e.preventDefault();
            Stripe.setPublishableKey(StripeKey);
            Stripe.createToken({
                number: $('.premium-require-validation .card-number').val(),
                cvc:    $('.premium-require-validation .card-cvc').val(),
                exp_month: $('.premium-require-validation .card-expiry-month').val(),
                exp_year: $('.premium-require-validation .card-expiry-year').val()
            }, stripeResponseHandlerPremium);
        }

    });

 
  function stripeResponseHandlerPremium(status, response)
  {
         
        if (response.error) {
            $('.premium-require-validation .stripe-error').text(response.error.message);
        } else {

            var token = response['id'];
             

            var ajax_url    = WEBSITE_URL+"/listproperty/stripeorderpremium";
            var amount      = $("#prem_amount11").val();
            var stipe_payment_btn = amount;
            var id          = $("#prem_sub11").val();
            var stripeToken       = token;
             $.ajax({
                type: 'post',
                url: ajax_url,
                data: {
                        amount:amount,
                        id:id,
                        stipe_payment_btn:stipe_payment_btn,
                        stripeToken:token,
                      }, 
                success: function (data) 
                {
                  console.log(data);
                  if(data.status==1)
                  {

                    

                   premium_transation_after_stripe(id);
                    

                  }
                     
                }
            });

            
        }
    } 



    

});

function premium_transation_after_stripe (id)
{
   var ajax_url = WEBSITE_URL+"/list/premium?is_premium="+id;
            $.ajax({
              url : ajax_url,
              method:"get",
              data:{ 
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}' 
              },
              beforeSend:function(){
                  startLoader();
              },
              complete:function(){
                 stopLoader(); 
              },
              success : function(data){
                  if(data.status){
                        Swal.fire({
                          title: 'Success!',
                          text: 'Transaction completed successfully',
                          icon: 'success',
                          confirmButtonText: 'Ok'
                      }).then((result) => {
                    if(result.value){
                        $(".package").hide();
                        $(".subscribe").hide();
                        $(".modal-backdrop").hide();
                        $("body").removeClass('modal-open')
                        $(".modal").hide();
                        $("#myModal").hide();
                        $("#StripePremiumCardModal").hide();
                        $(".submit_step_8").trigger('click');
                    }
                });
                  }else{
                      Swal.fire({
                          title: 'Error!',
                          text: data.message,
                          icon: 'error',
                          confirmButtonText: 'Ok'
                      });
                  }
              }
          });
}

 
</script>

@foreach($subscriptions   as $subs) 
<script>
  paypal.Buttons({
    style:{
      color: 'white',
      layout: 'horizontal',
      tagline: false,
      shape:   'rect'
    },
    createOrder: function(data, actions) {
      // This function sets up the details of the transaction, including the amount and line item details.
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: '{{$subs->sub_plan[0]['amount']}}'
          }
        }]
      });
    },
    onApprove: function(data, actions) {
      // This function captures the funds from the transaction.
      return actions.order.capture().then(function(details) {
        // This function shows a transaction success message to your buyer.
        // alert('Transaction completed by ' + details.payer.name.given_name);
        var ajax_url = WEBSITE_URL+"/list/premium?is_premium={{$subs->_id}}";
						$.ajax({
	            url : ajax_url,
	            method:"get",
              data:{ 
              },
              headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}' 
              },

	            success : function(data){
	                if(data.status){
                        Swal.fire({
	                        title: 'Success!',
	                        text: 'Transaction completed successfully',
	                        icon: 'success',
	                        confirmButtonText: 'Ok'
	                    }).then((result) => {
				            if(result.value){
        								$(".package").hide();
                        $(".submit_step_8").trigger('click');
				            }
				        });
	                }else{
	                    Swal.fire({
	                        title: 'Error!',
	                        text: data.message,
	                        icon: 'error',
	                        confirmButtonText: 'Ok'
	                    });
	                }
	            }
	        });				
        });
      }        
  }).render('#paypal-cospace-{{$subs->_id}}');


</script>

@endforeach