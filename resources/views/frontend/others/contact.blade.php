@extends('frontend.layouts.home')
@section('title','Contact Us')
@section('content')
@include('frontend.layouts.default-header')
<style>
#loading {
position: fixed;
width: 100%;
height: 100vh;
background: #fff url("{{asset('/img/loading.gif')}}") no-repeat center center;
z-index: 9999;
}

.intl-tel-input #phone {
    width: 25em;
}

  </style>
  <div id="loading"></div>
<!-- contact ineer -->
<section class="get-in-tch">
  <div class="container">
    <div class="row">
      <div class="offset-md-1 col-md-5">
        <img class="mb-4" style="height: 50px;" src="{{asset('img/landing-page/contact-image.png')}}">
        <h4>Get in Touch <br>with us</h4>
        <p class="mb-4">Contact us if you have a query Regarding our Platform or any other help</p>
        <h5>DUBAI</h5>
        <p class="mb-3">Leave us a message and we will get back to you.<br>
        </p>

        <span class="dp-block"><img  style="width: 30px;color: rgba(14,14,14,0.55);
    font-size: 16px;" src="{{asset('img/kw-flag.webp')}}"><a href="tel:+971547920141" style="color: rgba(14,14,14,0.55);
    font-size: 16px;">+971 54 7920141</a></span>
        <span class="dp-block"><img style="width: 30px;color: rgba(14,14,14,0.55);
    font-size: 16px;"  src="{{asset('img/kw-flag.webp')}}"><a href="tel:+971553076620" style="color: rgba(14,14,14,0.55);
    font-size: 16px;">+971 55 307 6620</a></span>
        <span class="dp-block"><img style="width: 30px;color: rgba(14,14,14,0.55);
    font-size: 16px;"  src="{{asset('img/uk-flag.webp')}}"><a href="tel:+447436596523" style="color: rgba(14,14,14,0.55);
    font-size: 16px;">+44 7436 596 523</a></span>
         <span class="dp-block"><a href="mailto:info@thegulfroad.com" style="color: rgba(14,14,14,0.55);
    font-size: 16px;">info@thegulfroad.com</a> </span>
      </div>
      <div class="col-md-5">
        <div class="address p-4">
          <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <form name="sendc" id="sendc" method="post">
            <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                    <input class="form-control" type="text" name="name" id="name" placeholder="Name" required>
                    <div id="name_error" ></div>
                  </div>
                </div>
              <div class="col-md-12">
                <div class="form-group">
                  <input id="phone" class="phone form-control" name="phone" placeholder="Phone number" type="text" onkeypress="return isNumber(event)" maxlength="13">
                    <div id="phone_error" ></div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <input class="form-control" type="email" name="email" id="email" placeholder="Email address" required>
                  <div id="email_error" ></div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <textarea class="form-control" rows="6" id="message" name="message" placeholder="Write your message here" required></textarea>
                  <div id="message_error"></div>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <!-- Google reCAPTCHA box -->

        
                   <div class="g-recaptcha" data-sitekey="{{env('reCAPTCHA_site_key')}}"></div>
                </div>
              </div>
              <div class="col-md-12">
                <button class="red-btn rounded full-width pt-2 pb-2 sendcontact" type="submit">Send Message</button>
              </div>
              <!-- Google reCAPTCHA box -->
    
              
            </form>
          </div>
      </div>
    </div>
  </div>
</section>
 
@include('frontend.layouts.footer')
@section('scripts')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

<script type="text/javascript">
  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
  }
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('.header').addClass('bg-header');
    } else {
       $('.header').removeClass('bg-header');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('fixed-top');
    } else {
       $('header').removeClass('fixed-top');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('#top-header-bar').removeClass('fixed-top');
    } else {
       $('#top-header-bar').addClass('fixed-top');
    }
});
  
</script>
<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery('#loading').fadeOut(1000);
});

$(document).ready(function() {
  $('form[id="sendc"]').validate({
    rules: {
      name: 'required',
      message: 'required',
      phone: {
        minlength: 7,
        required: true,
      },
      email: {
        required: true,
        email:true
      },
    },
    messages: {
      name: 'This field is required',
      number: {
        required: "This field is required",
        minlength: jQuery.validator.format("At least 7 characters required!")
      },
      message: 'This field is required',
      email: 'Must be valid email address',
    },
    errorPlacement: function(error, element) {
      console.log(element.attr("name"));
       if (element.attr("name") == "name" ){
         error.appendTo('#name_error');
       }else if(element.attr("name") == "phone"){
         error.appendTo('#phone_error');
       }else if(element.attr("name") == 'email'){
         error.appendTo('#email_error');
       }else if(element.attr("name") == 'message'){
         error.appendTo('#message_error');
       }
    },
    submitHandler: function(form) {
      if (grecaptcha.getResponse() == ""){
       
      Swal.fire({
                 title: 'Error!',
                text: "Please Fill Captcha Code First !",
                icon: 'error',
                confirmButtonText: 'Ok'
            })
      return false;
    } 
      var ajax_url = WEBSITE_URL+"/contactsend";
      var name=$("#name").val();
      var email=$("#email").val();
      var phone=$("#phone").val();
      var message=$("#message").val();
      var phone_code=$(".selected-dial-code").html();
      $.ajax({
        url:ajax_url,
        method:"POST",
        data:{
          name:name,
          email:email,
          phone:phone,
          phone_code:phone_code,
          message:message
        },
        headers:{
          'X-CSRF-TOKEN': '{{ csrf_token() }}',
        },
        beforeSend:function(){
            startLoader();
        },
        complete:function(){
           stopLoader(); 
        },
        success:function(data){
          if(data.status){
            Swal.fire({
                title: 'Success!',
                text: data.message,
                icon: 'success',
                confirmButtonText: 'Ok'
            }).then((result) => {
              window.location.reload();
            });
          }else{
            Swal.fire({
                title: 'Success!',
                text: data.message,
                icon: 'success',
                confirmButtonText: 'Ok'
            }).then((result) => {
              window.location.reload();
            });
          }
        }
      });
    }
  });
});
</script>
@endsection
