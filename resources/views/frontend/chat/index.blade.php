@php
$lang=Request::segment(1);
$loc=Request::segment(3);
$user=Request::segment(4);
$modules = \App\Models\Module::where('location',$loc)->get();
if($loc!='all'){
    $link=$loc;
}else{
    $link='all';
}
@endphp
@extends('frontend.layouts.home')
@section('content')
@include('frontend.layouts.default-header')
<style type="text/css">
  .custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 3px 12px;
    cursor: pointer;
    position: relative;
    top: -6px;
    font-size: 11px;
    color: #777;
}
</style>
<section class="body-light-grey pt-2 pb-5 chat-page">
	<div class="container" style="margin-top:5%">
  <a href="{{Session::get('previous_url')}}">Back</a> 
		<div class="row">
			<!-- <div class="col-md-4">
				<div class="bg-white rounded">
					<div class="pl-3 pr-3"> 
					</div>
					<ul class="nav nav-pills flex-column user-chat-box mt-2" id="myTab" role="tablist">
            @if(@$getOtherUsers!='[]')
            @foreach($getOtherUsers as $getUser)
            @if($getUser->sender_id==\Auth::user()->id)
            @if(@$getUser->reciever->name)
					  <li class="nav-item">
					    <a href="{{url(app()->getLocale().'/user_chat/'.$link.'/'.$getUser->reciever_id)}}" class="nav-link " id="home-tab">
					    <div class="row dp-flex align-items-center">
					    	<div class="col-md-2">
                  <div style="width: 56px;height: 56px;">
                    @if(!isset($getUser->reciever->image) || $getUser->reciever->image=='')
                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="width: 100%;height:100%;object-fit: cover;">
                    @else
                    @if(file_exists('uploads/profilePics/'.$getUser->reciever->image))
                    <img class="circle" src="{{url('uploads/profilePics/'.$getUser->reciever->image)}}" style="width: 100%;height:100%;object-fit: cover;">
                    @else
                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="width: 100%;height:100%;object-fit: cover;">
                    @endif
                    @endif
                  </div>
					    	</div>
					    	<div class="col-md-10">
					    		<h4 class="mb-0">{{@$getUser->reciever->name}}
                  </h4>
					    		<span>@php echo \Str::limit(@$getUser->message, 10, '...'); @endphp</span>
					    	</div>
					    </div>	
					    </a>
            </li>
            @endif
            @else
            <li class="nav-item">
					    <a href="{{url(app()->getLocale().'/user_chat/'.$link.'/'.$getUser->sender_id)}}" class="nav-link " id="home-tab" >
					    <div class="row dp-flex align-items-center">
					    	<div class="col-md-2">
                  <div style="width: 56px;height: 56px;">
                  @if(!isset($getUser->sender->image) || $getUser->sender->image=='')
                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="width: 100%;height:100%;object-fit: cover;">
                  @else
                    @if(file_exists('uploads/profilePics/'.$getUser->sender->image))
                      <img class="circle" src="{{url('uploads/profilePics/'.$getUser->sender->image)}}" style="width: 100%;height:100%;object-fit: cover;">
                    @else
                      <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="width: 100%;height:100%;object-fit: cover;">
                    @endif
                  @endif
					    	  </div>
					    	</div>
					    	<div class="col-md-10">
					    		<h4 class="mb-0">{{@$getUser->sender->name}}
                  </h4>
					    		<span>@php echo \Str::limit(@$getUser->message, 10, '...'); @endphp</span>
					    	</div>
					    </div>	
					    </a>
            </li>
            @endif
            @endforeach
            @endif
					</ul>
				</div>
			</div> -->
			<div class="col-md-12">
				<div class="bg-white p-3 rounded contact-chat overflow-hidden">
					<div class="tab-content" id="myTabContent">
				  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
				  	<div class="row dp-flex align-items-center">
				  		<div class="col-md-1">
                <div style="width: 56px;height: 56px;">
                  @if(!isset($username->image) || $username->image=='')
                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="width: 100%; height:100%; object-fit:cover;">
                  @else
                    @if(file_exists('uploads/profilePics/'.$username->image))
                    <img class="circle" src="{{url('uploads/profilePics/'.$username->image)}}" style="width: 100%; height:100%; object-fit:cover;">
                    @else
                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="width: 100%; height:100%; object-fit:cover;">
                    @endif
                  @endif
                </div>
				  		</div>
				  		<div class="col-md-11">
				  			<h4 class="mb-0">{{@$username->name}}
                </h4>
				  		</div>
				  		<div class="col-md-12">
				  			<hr>
				  		</div>
				  	</div>
				  	<div class="row mt-4">
				  		<div class="col-md-12">
				  			<div class="chatWindow customScroll" style="overflow-y: scroll;max-height: 75%;height: 450px;">
                  <div class="text-center mb-4">
                  </div>
                  @if(@$getAllMessage!='[]')
            @foreach($getAllMessage as $getMessage)
            @if($getMessage->reciever_id==\Auth::user()->id)
              <div class="row mb-5">
                 <div class="col-md-8">
                    <div class="chatOtherSide">
                      <span class="profilePic"> 
                        @if(!isset($username->image) || $username->image=='')
                          <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="width: 45px;">
                        @else
                        @if(file_exists('uploads/profilePics/'.$username->image))
                          <img class="circle" src="{{url('uploads/profilePics/'.$username->image)}}" style="width: 45px;">
                        @else
                          <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="width: 45px;">
                        @endif
                      @endif
                      </span>
                       <p class="mb-0"><span style="word-break: break-all;color:white">@php echo @$getMessage->message; @endphp</span> <span class="" style="color:white">{{@$getMessage->created_at->diffForHumans()}}</span></p>
                    </div>
                 </div>
              </div>
              @else
              <div class="row justify-content-end mb-5">
                <div class="col-md-8">
                   <div class="chatMySide">
                      <p class="mb-0 white-color">
                      <span style="word-break: break-all;color:white">@php echo @$getMessage->message; @endphp</span><span style="color:white;">{{@$getMessage->created_at->diffForHumans()}}</span></p>
                   </div>
                   <div>
                      @if(!isset($getMessage->sender->image) || $getMessage->sender->image=='')
                        <img class="own-pic-chat circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="height:46px;">
                      @else
                      @if(file_exists('uploads/profilePics/'.$getMessage->sender->image))
                        <img class="own-pic-chat circle" src="{{url('uploads/profilePics/'.$getMessage->sender->image)}}" style="height:46px;">
                      @else
                        <img class="own-pic-chat circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="height:46px;">
                      @endif
                      @endif
                   </div>
                </div>
              </div>
             @endif
             @endforeach
             @endif   
            </div>
            <form name="send_message" method="POST" action="{{url(app()->getLocale())}}/send_message" enctype="multipart/form-data">
              @csrf
                <label for="file-upload" class="custom-file-upload" id="custom_file_upload" style="display:none;"></label>
               <div class="chatInput">
                    <a class="mr-3" href="javascript://">
                      <img src="{{asset('img/attach.png')}}" alt="img">
                      <input type="file"  id="media" name="media" style="opacity: 0; position: absolute; width: 10px; margin: -12px -34px;" accept="image/*">
                    </a>
                     <input type="text" style="z-index: 9999;" placeholder="Type your message here …" id="message" name="message" required>
                     <input type="hidden" name="reciever" id="reciever" value="{{@$username->_id}}">
                  	<button style="border:0px;background: transparent;" type="submit">
                      <img class="send-msg-img" src="{{asset('img/Send_message.png')}}" > 
                    </button>
               </div>
            </form>
			  		</div>
				  	</div>
				  </div>
				  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
				  	<div class="row dp-flex align-items-center">
				  		<div class="col-md-1">
				  			<img class="circle" src="https://dummyimage.com/48x48/ccc/fff">
				  		</div>
				  		<div class="col-md-11">
				  			<h4 class="mb-0">Tyler Waters<small class="float-right">1hr</small></h4>
					    	<p class="mb-0">Last Seen 2:33PM</p>
				  		</div>
				  		<div class="col-md-12">
				  			<hr>
				  		</div>
				  	</div>
				  	<div class="row dp-flex align-items-center">
				  		<div class="mr-3 ml-3 display-flex-own full-width brdr overflow-hidden rounded">
				  			<div class="col-md-2 p-0">
				  			<img class="" src="https://dummyimage.com/112x95/ccc/fff">
				  		</div>
				  		<div class="col-md-10">
				  			<h5>3 BHK Apartment<strong class="float-right">240,000 AED / yr</strong></h5>
				  			<h4 class="mb-1 mt-1">Fully Furnished Apartment</h4>
					    	<span><i class="fa fa-map-marker mr-2"></i>Hayat Townhouses, Town Square, R…</span>
				  		</div>
				  		</div>
				  	</div>
				  	<div class="row mt-4">
				  		<div class="col-md-12">
				  			<div class="chatWindow customScroll">
                  <div class="text-center mb-4">
                     <span class="yesterday">Yesterday</span>
                  </div>
                  <div class="row mb-5">
                     <div class="col-md-8">
                        <div class="chatOtherSide">
                           <span class="profilePic"> <img class="rounded-circle" src="https://dummyimage.com/48x48/ccc/fff" alt="img"> </span>
                           <p class="mb-0">Online chat which provides. </p>
                           <span class="time">07:44 am</span>
                        </div>
                         
                     </div>
                  </div>
                  <div class="row justify-content-end mb-5">
                        <div class="col-md-8">
                           <div class="chatMySide">
                              <p class="mb-0 white-color">Hey. Yeah sure we can schedule a tour. How about this saturday at 11 am? and regarding price, sure we can negotiate. Let me know the timing is good or not.</p>
                              <span class="time">07:50 am</span>
                              <span class="seenMsg"> Seen by Dale Ortega at 04:32 pm</span>
                           </div>
                            <img class="own-pic-chat circle" src="https://dummyimage.com/48x48/ccc/fff">
                        </div>
                     </div>
                     <div class="row mb-5">
                           <div class="col-md-8">
                              <div class="chatOtherSide">
                                 <span class="profilePic"> <img class="rounded-circle" src="https://dummyimage.com/48x48/ccc/fff" alt="img"> </span>
                                 <p class="mb-0">Online chat which provides its users maximum functionality to simplify the search for contacts and discussion in real time through our site , allows you to find your soul mate and build relationships quickly with single women and men. </p>
                                 <span class="time">07:44 am</span>
                              </div>
                               
                           </div>
                        </div>

                        <div class="row mb-5">
                              <div class="col-md-8">
                                 <div class="chatOtherSide pt-3 pb-3 pr-5 pl-5">
                                    <span class="profilePic"> <img class="rounded-circle" src="https://dummyimage.com/48x48/ccc/fff" alt="img"> </span>

                                    <ul class="typingLoading mb-0">
                                       <li></li>
                                       <li></li>
                                       <li></li>
                                    </ul>
                                    
                                 </div>
                                  
                              </div>
                           </div>
                           
                     
               </div>
               <div class="chatInput">

                  <a class="mr-3" href="javascript://">
                  <img src="{{asset('img/camera_message.svg')}}" alt="img">
                  </a>
                  <a class="mr-3" href="javascript://">
                     <img src="{{asset('img/attachment_message.svg')}}" alt="img">
                     </a>
                     <input type="text" placeholder="Type your message here …">
               </div>
				  		</div>
				  	</div>
				  </div>

				  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
				  <h2>Contact</h2>
				    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque, eveniet earum. Sed accusantium eligendi molestiae quo hic velit nobis et, tempora placeat ratione rem blanditiis voluptates vel ipsam? Facilis, earum!</p>
				  
				  </div>
				  <div class="tab-pane fade" id="contact1" role="tabpanel" aria-labelledby="contact-tab">
				  <h2>Contact</h2>
				    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque, eveniet earum. Sed accusantium eligendi molestiae quo hic velit nobis et, tempora placeat ratione rem blanditiis voluptates vel ipsam? Facilis, earum!</p>
				  
				  </div>
				  <div class="tab-pane fade" id="contact2" role="tabpanel" aria-labelledby="contact-tab">
				  <h2>Contact</h2>
				    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque, eveniet earum. Sed accusantium eligendi molestiae quo hic velit nobis et, tempora placeat ratione rem blanditiis voluptates vel ipsam? Facilis, earum!</p>
				  
				  </div>
				</div>
				</div>
			</div>
		</div>
	</div>
</section>
@include('frontend.layouts.footer')
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/owl.carousel.min.js"></script> -->
<script language="javascript">
  function toggleDiv(divid)
  {
  
    varon = divid + 'on';
  varoff = divid + 'off';

  if(document.getElementById(varon).style.display == 'none')
  {
  document.getElementById(varon).style.display = 'block';
  document.getElementById(varoff).style.display = 'none';
  }
  
  else
  { 
  document.getElementById(varoff).style.display = 'block';
  document.getElementById(varon).style.display = 'none';
  }
} 
</script>
<script type="text/javascript">
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('.header').addClass('bg-header');
    } else {
       $('.header').removeClass('bg-header');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('fixed-top');
    } else {
       $('header').removeClass('fixed-top');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('#top-header-bar').removeClass('fixed-top');
    } else {
       $('#top-header-bar').addClass('fixed-top');
    }
});
  
</script>
<script type="text/javascript">
$("#media").change(function(){
  $("#message").val(' ');
  $("#custom_file_upload").show().text(this.files[0].name);
});
$('.chatWindow').scrollTop($('.chatWindow')[0].scrollHeight);
function send_message(){
var ajax_url = WEBSITE_URL+"/send_message";
var reciever=$("#reciever").val();
var message=$("#message").val();
$.ajax({
  url:ajax_url,
  method:"POST",
  data:{
    reciever:reciever,
    message:message,
  },
  headers:{
    'X-CSRF-TOKEN': '{{ csrf_token() }}',
  },
  success:function(data){
    if(data.status){
      window.location.reload();
    }else{
      Swal.fire({
          title: 'Success!',
          text: data.msg,
          icon: 'success',
          confirmButtonText: 'Ok'
      }).then((result) => {
        window.location.reload();
      });
    }
  }
});
}
</script>
@endsection
