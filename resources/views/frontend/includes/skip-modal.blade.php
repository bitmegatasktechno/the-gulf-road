<div id="register-popup" class="modal1 skipp-page-cls">
	<div class="modal-dialog">
	    <!-- Modal content-->
	    <div class="signup-right">
		<span class="close-icon"></span>
		<a href="" class="skipp-section">Skip <img src="{{ asset('img/Arrow.png') }}"></a>
		<div class="modal-body">
		    <div class="row">
		        <div class="col col-md-4">
		            <div class="modal-left_side skipp-left-bar">
		                <img src="{{ asset('img/address.png') }}">
		                <h2>Start Connecting, Start Selling.</h2>
		                <p>Create an Agent Profile to list properties or become a Buddy and Earn Money.</p>

		            </div>
		        </div>
		        <div class="col col-md-4 text-center">
		            <div class="box-section-skip">
		                <img src="{{ asset('img/Agent.png') }}">
		                <h2>Become an Agent</h2>
		                <p>Create your agent Profile & List Properties on the Platform hassel Free. Keep Track
		                    of your Properties & Projects. <a href="">Learn More.</a></p>
		                <button class="btn btn-large button skip-btn">Become an Agent Today <img
		                        src="img/arrow-white.png"></button>
		            </div>
		        </div>
		        <div class="col col-md-4 text-center">
		            <div class="box-section-skip">
		                <img src="{{ asset('img/Buddy.png') }}">
		                <h2>Become a Buddy</h2>
		                <p>Create your Buddy profile and Become the Property Scout of your Area. Earn upto AED
		                    1500 / Consultation. <a href="">Learn More.</a></p>
		                <button class="btn btn-large button skip-btn">Create your Buddy Profile <img
		                        src="img/arrow-white.png"></button>
		            </div>
		        </div>
		    </div>
		</div>
	    </div>

	</div>
</div>