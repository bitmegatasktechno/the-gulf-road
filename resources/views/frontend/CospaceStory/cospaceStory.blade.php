@extends('frontend.layouts.home')
@section('title','Story Details')
@section('css')
	<link  href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
@endsection
@section('content')
    
	@include('frontend.layouts.default-header')
	
    
  

	<section class="body-light-grey pt-2 pb-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="list-inline breadcrum mb-0">
                        <li><a href="{{url(app()->getLocale().'/home')}}">Home </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

      <style>
.img{
  object-fit:cover;
}
  </style>
    <section class="property-image-block body-light-grey pt-0">
      <div class="container">
  <div class="row">
    <div class="col-md-12">
    <div class="img" >
    @if(!isset($CospaceStory->logo) || $CospaceStory->logo=='')
    <img class="img-fluid" src="{{asset('img/house2.png')}}" style="width: 100%;    height: 100%;">
      @else
      @if(file_exists('uploads/cospaceStories/'.$CospaceStory->logo))
      <img class="img-fluid" src="{{url('uploads/cospaceStories/'.$CospaceStory->logo)}}" style="width: 100%;
    height: 100%;
    object-fit: cover;">
      @else
      <img class="img-fluid" src="{{asset('img/house2.png')}}" style="width: 100%;
    height: 100%;
    object-fit: cover;">
      @endif
     @endif
      </div>
    </div>
        </div></div>
      </div>
    </div>
      </div>
    </div>
  </div>


</section>




<!-- Next Section -->
<section class="body-light-grey p-0">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="bg-white mb-3 p-3 rounded">

                    <h2>{{$CospaceStory->name}}</h2>
                    <?php
            $text = $CospaceStory->description;
            echo $text;
            ?>
            </p>

                </div>
            </div>
        </div>
    </div>
</section>
	
    @include('frontend.layouts.footer')

@endsection
@section('scripts')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
	<script type="text/javascript">

		$(document).ready(function () {
		    $(".show_hide").on("click", function () {
				var dots = document.getElementById("dots");
				if (dots.style.display === "none") {
					dots.style.display = "inline";
				} else {
					dots.style.display = "none";
				}
		        var txt = $(".content").is(':visible') ? 'Read More' : 'Read Less';
		        $(this).text(txt);
		        $('.content').toggle();
		    });
		});
	</script>
@endsection