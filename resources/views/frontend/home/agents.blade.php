@php
$lang=Request::segment(1);
$loc=Request::segment(3);if($loc == 'ksa' || $loc == 'uae' )
{
   $loc21 = strtoupper($loc);
}else 
{
    $loc21 = ucfirst($loc);
}
 $type=Request::segment(4);
$modules = \App\Models\Module::where('location', $loc21)->get();
if($loc!='all'){
    $link=$loc;
}else{
    $link='all';
}
@endphp
@extends('frontend.layouts.home')
@section('title','Home')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.carousel.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.theme.default.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.theme.green.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
  .property-card.with-bg-image .fav-icon{
    top: 1px !important;
    right: -5px !important;
  }
 /* .owl-carousel .owl-dots.disabled, .owl-carousel .owl-nav.disabled{
    display:block !important;
  }
  .owl-dots button.owl-dot.active {
      background-color: white !important;
  }*/
  #loading {
    width: 100%;
    height: 100vh;
    z-index: 9999;
    position: fixed;
    background: #fff url('images/loader.gif') no-repeat center center;
  }
 /* .owl-carousel .owl-item img {
      display: block !important;
      width: 100px !important;
  }*/
  .owl-item .item .buddy-wrapper:hover {
      box-shadow: 0px 4px 12px #000000ad;
      transition: all .3s ease-in 0s;
  }
  .owl-item .super-agent-block:hover {
      box-shadow: 0px 4px 12px #000000ad;
      transition: all .3s ease-in 0s;
  }
</style>
@endsection
@section('content')
@include('frontend.layouts.header')
@php
  $loc=Request::segment(3);
@endphp
@if($loc=='uae' || $loc=='kuwait' || $loc=='ksa' || $loc=='oman' || $loc=='qatar' || $loc=='bahrain')
@include('frontend.home.filter')
@else
@include('frontend.home.agent_filter')
@endif
<section class="lp-bottom-bannar">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="red-bg">
          <h3>Become an agent</h3>
          <p class="">Upload and manage your properties and be seen by millions of buyers in middle east. Real estate agents sell, lease, manage and buy properties on behalf of clients and to advertise to millions - Thegulfroad.com helps. We also help real estate agents to offer advice to their clients, whether they be buying, selling, renting or owning, to guide them in their journey to find a property that suits them and their needs best. </p>
          <div class="text-right mt-4">
            <a href="#agent_loc"><button class="first-btn mr-3">KNOW MORE</button></a>
            @if(Auth::user())  
            <a href="/en/profile/<?php echo $link ?>">
            @else 
            <a href="javascript:;" onclick="showLoginForm();">
            @endif
            <button class="second-btn">BECOME AGENT > </button></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@if(count($agents)>0)
<section class="super-agents pt-5 pb-5">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3>Top Agents</h3>
        <p>Most popular agents</p>
      </div>
      <div class="col-md-12 p-0">
        <div id="owl-example" class="owl-carousel">
        @foreach($agents as $agent)
          <a href="{{url(app()->getLocale().'/agent/'.$link.'/'.$agent->_id)}}">
          <div>
          <div class="col-md-12">
            <div class="super-agent-block pb-4">
              <div class="img-block">
              @if(!isset($agent->image) || $agent->image=='')
              <img class="dp-block" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 150px;margin-left:auto;margin-right: auto;width: auto !important;">
              @else
              @if(file_exists('uploads/profilePics/'.$agent->image))
              <img class="dp-block" src="{{url('uploads/profilePics/'.$agent->image)}}" style="height: 150px;margin-left:auto;margin-right: auto;width: auto !important;">
              @else
              <img class="dp-block" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 150px;margin-left:auto;margin-right: auto;width: auto !important;">
              @endif
              @endif
              <div class="star-block">
                <div class="row mt-1 mb-1">
                    <div class="col-md-7">
                        <ul class="list-inline mb-0 pl-3 pr-3 dp-flex align-itmes-center">
                        @if(@$agent->rating==1)
                          <li><img src="{{asset('img/white_star.png')}}"></li>
                        @elseif(@$agent->rating==2)
                          <li><img src="{{asset('img/white_star.png')}}"></li>
                          <li><img src="{{asset('img/white_star.png')}}"></li>
                        @elseif(@$agent->rating==3)
                          <li><img src="{{asset('img/white_star.png')}}"></li>
                          <li><img src="{{asset('img/white_star.png')}}"></li>
                          <li><img src="{{asset('img/white_star.png')}}"></li>
                        @elseif(@$agent->rating==4)
                          <li><img src="{{asset('img/white_star.png')}}"></li>
                          <li><img src="{{asset('img/white_star.png')}}"></li>
                          <li><img src="{{asset('img/white_star.png')}}"></li>
                          <li><img src="{{asset('img/white_star.png')}}"></li>
                        @elseif(@$agent->rating==5)
                          <li><img src="{{asset('img/white_star.png')}}"></li>
                          <li><img src="{{asset('img/white_star.png')}}"></li>
                          <li><img src="{{asset('img/white_star.png')}}"></li>
                          <li><img src="{{asset('img/white_star.png')}}"></li>
                          <li><img src="{{asset('img/white_star.png')}}"></li>
                        @endif
                        <li class="white-color text" style="color:white !important;">{{$agent->rating != 0 ? $agent->rating : 'Not rated yet'}}</li>
                        </ul>
                    </div>
                    <div class="col-md-5 pl-0">
                        <ul class="list-inline mb-0 pl-3 pr-3 text-right">
                            <li class="">
                              <img style="height: 10px;" src="{{asset('img/mail_white_agent_ic.png')}}">
                            </li>
                            <li class="ml-2">
                              <img style="height: 10px;" src="{{asset('img/call_white_agent_ic.png')}}">
                            </li>
                            <li class="ml-2">
                              <img style="height: 10px;" src="{{asset('img/whatsapp_white_agent_ic.png')}}">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="plr-10">
            <h4>{{\Str::limit(@$agent->name, 20, '...')}}</h4>
            <h6>{{@$agent->country}}  ·  {{ucfirst(@$agent->gender)}}  ·  {{@$agent->age}} yrs</h6>
            <p class="mb-1">Languages known</p>
            @if($agent->agentProfessional)
                @php
                    $languages = $agent->agentProfessional->languages;
                    $lang = 0;
                @endphp

                @if(is_array($languages))
                    
                    @foreach($languages as $subrow)
                        @if($lang<2)
                            <span class="mr-3">{{$subrow}}</span>
                            @php
                                $lang++;
                            @endphp
                        @else
                            <span class="mr-1 text-center">+1</span>
                            @break
                        @endif
                    @endforeach
                @else
                    <span>N/A</span>
                @endif
            @endif
                            <hr>
                            <p class="mb-1">LOCALITIES</p>
                            <span class="mr-3 light-blue">{{$agent->country}}</span>
                            <p class="mb-1 mt-3">PROPERTIES</p>
                            <span class="full-width">
                                <div class="row">
                                    <div class="col-md-6 pr-0 text-center bdr-right">
                                        <strong class="dp-block mb-0">{{$agent->saleProperties->count()}}</strong>
                                        <small class="dp-block mb-0">For Sale</small>
                                    </div>
                                    <div class="col-md-6 pr-0 text-center">
                                        <strong class="dp-block mb-0">{{$agent->rentProperties->count()}}</strong>
                                        <small class="dp-block mb-0">For Rent</small>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            @endforeach
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
    <div class="col-md-12 show-more text-center">
        <a href="/en/agents/<?php echo $link ?>?country=<?php echo $link ?>&location=">
          <button class="red-bordered mt-5">SHOW MORE <i class="fa fa-angle-right"></i></button>
        </a>
    </div>
</section>
@endif
<section class="lp-2 find-buddy-location" id="agent_loc">
  <div class="container">
    <div class="row d-flex align-items-center">
      <div class="col-md-6">
        <h4>We know how to advertise great homes</h4>
        <h5>When you choose thegulfroad.com to rent, sell or swap your home or advertise your co-office space, you are aligning yourself with the ultimate professional, and assuring yourself the best visibility and results. Our reach in middle east makes an impact.</h5>
        <div class="row d-flex align-items-center mb-3">
          <div class="col-md-4">
            <p><img class="mr-2" src="{{asset('img/landing-page/our-services.png')}}">Our Blog</p>
          </div>
          <div class="col-md-4">
            <p><img class="mr-2" src="{{asset('img/landing-page/our-blog.png')}}">Our Services</p>
          </div>
          <div class="col-md-4">
            <p><img class="mr-2" src="{{asset('img/landing-page/contact-us.png')}}">Contact Us</p>
          </div>
        </div>
        <p>That’s why we go beyond the typical listings, by sourcing insights straight from locals and offering neighborhood map overlays, to give people a deeper understanding of what living in a home and neighborhood is like.</p>
        <p>At thegulfroad.com we believe don’t Sell/Buy Homes Alone! Advertising on your website thegulfroad.com, you can reap the benefits of: </p>
        <p>• Build awareness</p>
        <p>• Generate quality leads</p>
        <p>• Increase credibility along with brand</p>
        <p>• Help buyers and sellers throughout their buying or selling journey</p>
        <p>• Full information control</p>
      </div>
      <div class="col-md-6">
        <img class="img-fluid d-flex mx-auto" src="{{asset('img/landing-page/22.jpg')}}">
      </div>
    </div>
  </div>
  <br>
</section>
<section class="partners-logo" style="display: none;">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      <h4>Featured Agencies</h4>
      </div>
      <div class="col-md-12 p-0">
      <div id="owl-example1" class="owl-carousel">
        <img src="{{asset('img/landing-page/partner-1.png')}}">
        <img src="{{asset('img/landing-page/partner-2.png')}}">
        <img src="{{asset('img/landing-page/partner-3.png')}}">
        <img src="{{asset('img/landing-page/partner-4.png')}}">
        <img src="{{asset('img/landing-page/partner-5.png')}}">
        <img src="{{asset('img/landing-page/partner-1.png')}}">
      </div>
      </div>
    </div>
  </div>
</section>
<section class="benifits-wrap property-range">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3>Our Range Of Property Services</h3>
      </div>
      <div class="col-md-4">
        <div class="row">
          <div class="col-md-3 col-3">
            <img class="mx-auto dp-block img-fluid" src="https://img.icons8.com/carbon-copy/100/000000/home.png"/>
          </div>
          <div class="col-md-9 col-9">
            <h4 class="mt-0 mb-2">Property Search</h4>
            <p class="">Trusted by a community of thousands of users we help you to search the right home or the right person.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="row">
          <div class="col-md-3 col-3">
            <img class="mx-auto dp-block img-fluid"  src="https://img.icons8.com/ios/100/000000/person-at-home.png"/>
          </div>
          <div class="col-md-9 col-9">
            <h4 class="mt-0 mb-2">Property Management</h4>
            <p class="">There's no doubt about it: rentals can be a pretty mixed bag. As a renter, you’re never quite sure what you're going to get with a private landlord.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="row">
          <div class="col-md-3 col-3">
            <img class="mx-auto dp-block img-block" src="https://img.icons8.com/carbon-copy/100/000000/order-delivered.png"/>
          </div>
          <div class="col-md-9 col-9">
            <h4 class="mt-0 mb-2">Great Agents And Agencies</h4>
            <p class="">No matter what type of real estate needs you have, finding the local real estate professional you want to work with is the first step.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="row">
          <div class="col-md-3 col-3">
            <img  class="mx-auto dp-block img-fluid"  src="https://img.icons8.com/dotty/80/000000/bell.png"/>
          </div>
          <div class="col-md-9 col-9">
            <h4 class="mt-0 mb-2">Get important Notifications</h4>
            <p class="">Receive advices, listings, and other information.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="row">
          <div class="col-md-3 col-3">
            <img class="mx-auto dp-block img-fluid" src="https://img.icons8.com/ios/50/000000/smart-home-shield.png"/>
          </div>
          <div class="col-md-9 col-9">
            <h4 class="mt-0 mb-2">Transparency</h4>
            <p class="">Finding the right home improvement pro can be a challenge, so it's important to choose someone with local expertise and.</p>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="row">
          <div class="col-md-3 col-3">
            <img class="mx-auto dp-block img-fluid" src="https://img.icons8.com/ios/50/000000/mobile-home.png"/>
          </div>
          <div class="col-md-9 col-9">
            <h4 class="mt-0 mb-2">Near by me, cost and distance</h4>
            <p class="">Search near top-rated schools and explore neighborhoods and also a unique way to calculate distance and cost of what you rent or buy.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<br>
@if(count($saleProperties)>0 || count($rentProperties)>0)
@foreach($modules as $module)
@if($module->type=='rent')
<section class="prem-prop-wrap">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3>Popular Properties</h3>
      </div>
      @foreach($saleProperties as $saleProperty)
      <div class="col-md-6">
      @if(isset(Auth::user()->id))
      @php
      $fav = \App\Models\Favourate::where('propert_id',$saleProperty->_id)->where('user_id',Auth::user()->id)->first();
      @endphp
      @endif
      @if(@$fav)
      <i class="fa fa-heart fav-icon" style="right: 25px;z-index: 999;" onClick="makefav('{{$saleProperty->_id}}')"></i>
      @else
      <i class="fa fa-heart-o fav-icon" style="right: 25px;z-index: 999;" onClick="makefav('{{$saleProperty->_id}}')"></i>
      @endif
      <a href="{{url(app()->getLocale().'/property/'.$link.'/buy/'.$saleProperty->_id)}}">
        <div class="property-card with-bg-image bg-white rounded">
          <div class="overlay" style="height: 48%;width: 100%;bottom: 0px;position: absolute;background: #00000038;"></div>
          <button class="red-btn">Premium</button>
          @if(!isset($saleProperty->files[0]) || $saleProperty->files[0]=='')
           <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
            @else
            @if(file_exists('uploads/properties/'.$saleProperty->files[0]))
            <img class="img-fluid property-img" src="{{url('uploads/properties/'.$saleProperty->files[0])}}">
            @else
            <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
            @endif
            @endif
          <div class="pl-3 pr-3 content-wrap">
            <h2>{{@$saleProperty->property_title}}</h2>
            <ul class="list-inline red-list mb-4">
              <li>{{@$saleProperty->price}} AED / yr</li>
              <li class="mr-3"><img style="filter: brightness(0) invert(1);" class="mr-1" src="{{asset('img/red-bed.png')}}"> {{@$saleProperty->no_of_bedrooms}}</li>
              <li class="mr-3"><img style="filter: brightness(0) invert(1);" class="mr-1" src="{{asset('img/red-tub.png')}}"> {{@$saleProperty->no_of_bathrooms}}</li>
              <li class="mr-3"><img style="filter: brightness(0) invert(1);" class="mr-1" src="{{asset('img/red-car.png')}}"> {{@$saleProperty->no_of_open_parking}}</li>
            </ul>
          </div>
        </div>
        </a>
      </div>
      @endforeach
      @foreach($rentProperties as $rentProperty)
      <div class="col-md-4">
      @if(isset(Auth::user()->id))
        @php
        $fav = \App\Models\Favourate::where('propert_id',$rentProperty->_id)->where('user_id',Auth::user()->id)->first();
        @endphp
        @endif
        @if(@$fav)
        <i class="fa fa-heart fav-icon" onClick="makefav('{{$rentProperty->_id}}')"></i>
        @else
        <i class="fa fa-heart-o fav-icon" onClick="makefav('{{$rentProperty->_id}}')"></i>
      @endif
      <a href="{{url(app()->getLocale().'/property/'.$link.'/rent/'.$rentProperty->_id)}}">
        <div class="property-card with-bg-image bg-white rounded">
          <div class="overlay" style="height: 48%;width: 100%;bottom: 0px;position: absolute;background: #00000038;"></div>
          <button class="red-btn">Premium</button>
          @if(!isset($rentProperty->files[0]) || $rentProperty->files[0]=='')
           <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
            @else
            @if(file_exists('uploads/properties/'.$rentProperty->files[0]))
            <img class="img-fluid property-img" src="{{url('uploads/properties/'.$rentProperty->files[0])}}">
            @else
            <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
            @endif
            @endif
          <div class="pl-3 pr-3 content-wrap">
            <h2>{{@$rentProperty->property_title}}</h2>
            <ul class="list-inline red-list mb-4">
              <li>{{@$rentProperty->rent}} AED / yr</li>
              <li class="mr-3"><img style="filter: brightness(0) invert(1);" class="mr-1" src="{{asset('img/red-bed.png')}}"> {{@$rentProperty->no_of_bedrooms}}</li>
              <li class="mr-3"><img style="filter: brightness(0) invert(1);" class="mr-1" src="{{asset('img/red-tub.png')}}"> {{@$rentProperty->no_of_bathrooms}}</li>
              <li class="mr-3"><img style="filter: brightness(0) invert(1);" class="mr-1" src="{{asset('img/red-car.png')}}"> {{@$rentProperty->no_of_open_parking}}</li>
            </ul>
          </div>
        </div>
        </a>
      </div>
      @endforeach
    </div>
  </div>
  <div class="col-md-12 show-more text-center">
      <a href="/en/buy-property/<?php echo $link ?>?country=<?php echo $link ?>&location="><button class="red-bordered mt-5">SHOW MORE <i class="fa fa-angle-right"></i></button></a>
  </div>
</section>
@endif
@endforeach
@endif
<section class="lp-top-footer buddies">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
      <div style="border: 10px solid #000;width:100%;height: 290px;">
        <iframe width="100%" height="270px" src="{{@$video->video}}" title="How it works Agent." frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
        </iframe>
      </div>
      </div>
      <div class="col-md-7">
        <h4 style="color:red;margin-top: 40px;">How it works</h4>
        <h6> Thegulfroad.com is a user friendly platform to access properties, agents, co-space, swap options and buddies.</h6>
        <p>• Advertise your inventory or house as top agent</p>
        <p>• Connect with the seeker on our secure and safe platform</p>
        <p>• Negotiate the best deal for them</p>
        <p>• Keep earning and increasing your visibility</p>
      </div>
    </div>
  </div>
</section>
<section class="lp-footer-top-sec">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="bg-block p-4"  style="    height: 203px;">
        <a href="{{url(app()->getLocale().'/landing/'.$link.'/cospace/')}}">
          <div class="row">
            <div class="col-md-3 col-3">
               <img class="img-fluid" src="{{asset('img/search.png')}}" style="width: 100%;">
            </div>
            <div class="col-md-9 pl-0 col-9">
              <h4> Looking for a Co Working Space?</h4>
              <p> Search the best shared based office spaces. Look out for the amenities available and choose the plan. Enquire us and we will ensure that you get nothing but the best.</p>
            </div>
          </div>
          </a>
        </div>
      </div>
      <div class="col-md-4">
        <div class="bg-block p-4" style="height: 203px;">
        <a href="{{url(app()->getLocale().'/landing/'.$link.'/rent/')}}">
          <div class="row">
            <div class="col-md-3 col-3">
              <img class="img-fluid" src="{{asset('/img/buyicon.png')}}" style="width: 100%;">
            </div>
            <div class="col-md-9 pl-0 col-9">
              <h4>Looking to rent or buy a home?</h4>
              <p>Thegulfroad.com offers you listing of the best homes with the cost and distance calculator options. Its's one stop for all your needs.</p>
            </div>
          </div>
          </a>
        </div>
      </div>
      <div class="col-md-4">
        <div class="bg-block p-4" style="    height: 203px;">
        <a href="{{url(app()->getLocale().'/landing/'.$link.'/rent/')}}">
          <div class="row">
            <div class="col-md-3 col-3">
              <img class="img-fluid" src="{{asset('img/cospace.png')}}" style="width: 100%;">
            </div>
            <div class="col-md-9 pl-0 col-9">
              <h4>Looking for buddies and agents?</h4>
              <p>A reliable network of buddies and agents listed make your life easier. Choose the buddy of your choice who would help you settle down and choose an agent to help in you search of new home.</p>
            </div>
          </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</section>
@include('frontend.layouts.footer')
@endsection
</body>
</html>
