@php
$lang=Request::segment(1);
$loc=Request::segment(3);
$type=Request::segment(4);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
<style>
.owl-carousel .owl-dots.disabled, .owl-carousel .owl-nav.disabled {
    display: block;
}
button.owl-dot.active{
    display:none;
}
</style>
@if(count($agents)>0)
<section class="super-agents">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>Super Agents</h3>
                    <p>Most popular agents</p>
                    
                </div>
                <div class="col-md-12 p-0">
                    <div id="owl-example" class="owl-carousel">
                    @foreach($agents as $key=>$agent)
                    @php
                            $totaluser = \App\Models\Review::where('is_approved',1)->where('review_type',"6")->where('type_id',$agent->_id)->count();
                            $ratinggiven1=\App\Models\Review::where('is_approved',1)->where('review_type',"6")->where('type_id',$agent->_id)->sum('average');
                            $totalrating=@$totaluser*5;
                            if($totalrating == 0){
                                $totalrating=1;
                            }
                            $avg=number_format(@$ratinggiven1/@$totalrating*5);
                            @endphp
                    <a href="{{url(app()->getLocale().'/agent/'.$link.'/'.$agent->_id)}}">
                        <div>
                            <div class="col-md-12">
                                <div class="super-agent-block pb-4">
                                    <div class="img-block">
                                    @if(!isset($agent->image) || $agent->image=='')
                                    <img class="dp-block img-fluid" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 150px;margin-left:auto;margin-right: auto; width: auto!important;">
                                    @else
                                    @if(file_exists('uploads/profilePics/'.$agent->image))
                                    <img class="dp-block img-fluid" src="{{url('uploads/profilePics/'.$agent->image)}}" style="height: 150px;margin-left:auto;margin-right: auto; width: auto!important;">
                                    @else
                                    <img class="dp-block img-fluid" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 150px;margin-left:auto;margin-right: auto; width: auto!important;">
                                    @endif
                                    @endif
                                        <div class="star-block">
                                            <div class="row mt-1 mb-1">
                                                <div class="col-md-7">
                                                    <ul class="list-inline mb-0 pl-3 pr-3 dp-flex align-itmes-center">
                                                        @if(@$avg==1)
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        @elseif(@$avg==2)
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        @elseif(@$avg==3)
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        @elseif(@$avg==4)
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        @elseif(@$avg==5)
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        @endif
                                                        @if(@$avg)
                                                        <li class="white-color text">{{@$avg}}</li>
                                                        @else
                                                        <li class="white-color text">Not Rated yet</li>
                                                        @endif
                                                    </ul>
                                                </div>
                                                <div class="col-md-5 pl-0">
                                                    <ul class="list-inline mb-0 pl-3 pr-3 text-right">
                                                        <li class=""><img style="height: 10px;" src="{{asset('img/mail_white_agent_ic.png')}}"></li>
                                                        <li class="ml-2"><img style="height: 10px;" src="{{asset('img/call_white_agent_ic.png')}}"></li>
                                                        <li class="ml-2"><img style="height: 10px;" src="{{asset('img/whatsapp_white_agent_ic.png')}}"></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="plr-10">
                                        <h4>{{\Str::limit(@$agent->name, 20, '...')}}</h4>
                                        <h6>{{@$agent->country}}  ·  {{ucfirst(@$agent->gender)}}  ·  {{@$agent->age}} yrs</h6>
                                        <p class="mb-1">Languages known</p>
                                        
                                        @foreach($agent['professionals'] as $prof)
                                        @if($prof->role == 'AGENT')
                                            @foreach($prof->languages as $lang)
                                            <span class='mr-3'>{{$lang}}</span>
                                            @endforeach
                                        @endif
                                        @endforeach
                                            
                                           
                                        <hr>
                                        <p class="mb-1">LOCALITIES</p>
                                        @foreach($agent['professionals'] as $prof)
                                        @if($prof->role == 'AGENT')
                                            @foreach($prof->localities as $local)
                                            <span class='mr-3 light-blue'>{{$local}}</span>
                                            @endforeach
                                        @endif
                                        @endforeach
                                        
                                        <p class="mb-1 mt-3">PROPERTIES</p>
                                        <span class="full-width">
                                            <div class="row">
                                            <div class="col-md-6 pr-0 text-center bdr-right">
                                    <strong class="dp-block mb-0">{{$agent->saleProperties->count()}}</strong>
                                    <small class="dp-block mb-0">For Sale</small>
                                </div>
                                <div class="col-md-6 pr-0 text-center">
                                    <strong class="dp-block mb-0">{{$agent->rentProperties->count()}}</strong>
                                    <small class="dp-block mb-0">For Rent</small>
                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
