@php
$lang=Request::segment(1);
$loc=Request::segment(3);
$type=Request::segment(4);
$modules = \App\Models\Module::where('location',$loc)->get();
if($loc!='all'){
    $link=$loc;
}else{
    $link='all';
}
@endphp
@if(count($buddies)>0)
<section class="top-rated-buddies mb-5 pb-5">
<img src="{{asset('img/ic_dots.png')}}" style="position: absolute;margin-top: 20%;opacity: 60%;">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <h3>Top Rated Buddies</h3>
                <p>Buddies that our users like the most</p>
            </div>
            <div class="col-md-2">
                <img src="{{asset('/img/info-button.png')}}" style="position: absolute;margin-top: 10px;margin-left: 8px;width: 17px;">
                <button class="red-btn full-width rounded" autocomplete="off" data-toggle="modal" data-target="#how_it_works">How it works?</button>
            </div>
            
            <div class="col-md-12 p-0">
                <div class="carousel-wrap">
                    <div class="owl-carousel"> 
                @foreach($buddies as $key=>$buddy)
                    <a href="{{url(app()->getLocale().'/buddy/'.$link.'/'.$buddy->_id)}}">
                        <div class="item">
                            <div class="col-md-12">
                                <div class="buddy-wrapper bg-white rounded p-3 mt-4">
                                    <div class="row">
                                        <div class="col-md-4">
                                        @if(!isset($buddy->image) || $buddy->image=='')
                                            <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="width: 100px;height: 100px;">
                                        @else
                                        @if(file_exists('uploads/profilePics/'.$buddy->image))
                                            <img class="circle" src="{{url('uploads/profilePics/'.$buddy->image)}}" style="width: 100px;height: 100px;">
                                            @else
                                            <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="width: 100px;height: 100px;">
                                            @endif
                                        @endif
                                        </div>
                                        <div class="col-md-8">
                                            <h4>{{\Str::limit(@$buddy->name, 20, '...')}}</h4>
                                            <h6>{{@$buddy->country}}  ·  {{ucfirst(@$buddy->gender)}}  ·  {{@$buddy->age}} yrs</h6>
                                            <ul class="list-inline mb-2">
                                            @if($buddy->rating ==1)
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                            @endif
                                            @if($buddy->rating ==2)
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                            @endif
                                            @if($buddy->rating ==3)
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                            @endif
                                            @if($buddy->rating ==4)
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                            @endif
                                            @if($buddy->rating ==5)
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                            @endif
                                            <li class="white-color text" style="color:black !important;">{{$buddy->rating}}</li>
                                            </ul>
                                            <h6><i class="mr-2">
                                                @php $connections = \App\Models\UserEnquiry::where('user_id',$buddy->id)->where('type','buddy')->groupBy('sender_id')->get(); @endphp
                                                <img src="{{asset('img/Connections_icon.png')}}"></i><strong> {{count($connections)}} </strong>Connection</h6>
                                        </div>
                                        <div class="col-12">
                                            <small class="dp-block">LANGUAGES KNOWN</small>

                                            
                                            @foreach($buddy['professionals'] as $prof)
                                            @if($prof->role == 'BUDDY')
                                                @foreach($prof->languages as $lang)
                                                <span class='mr-3'>{{$lang}}</span>
                                                @endforeach
                                            @endif
                                            @endforeach
                                           
                                            <div>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <small>CONSULTATION CHARGES</small>
                                            <h6 style="font-size: 18px;">{{@$buddy->charge_currency??'AED'}} {{@$buddy->charges}}<small class="fs-16"> / {{@$buddy->charge_type??'Hourly'}}</small></h6>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <ul class="list-inline mt-4">
                                                <li><i class="mr-3"><img src="{{asset('img/call_buddy.png')}}"></i></li>
                                                <li><i class="mr-3"><img src="{{asset('img/mail-buddy.png')}}"></i></li>
                                                <li><i class="mr-3"><img src="{{asset('img/whatsapp-buddy.png')}}"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </a>
                       @endforeach
                        </div>
                        </div>
                        </div>
                    
             
        </div>
    </div>
</section>
@endif 


<!-- Modal -->
<div class="modal fade how_it_modal" id="how_it_works" tabindex="-1" role="dialog" aria-labelledby="how_it_worksLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header border-0 pb-0">
        <h5 class="modal-title" id="how_it_worksLabel">How It Works</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p >1. Register or login to your Thegulfroad.com</p>
        <p >2. Visit your profile section</p>
        <p >3. Click Register as a Buddy</p>
        <p >4. Once you fill in the relevant details your application will be sent to our team</p>
        <p >5. Upon approval you will be listed and ready to connect!</p>
        <p >* Services to register/login as a buddy are available only in some countries.</p>
      </div>
    </div>
  </div>
</div>