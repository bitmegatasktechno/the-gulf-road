@php
$lang=Request::segment(1);
$loc=Request::segment(3);
if($loc == 'ksa' || $loc == 'uae' )
{
   $loc21 = strtoupper($loc);
}else 
{
    $loc21 = ucfirst($loc);
}
 $type=Request::segment(4);
$modules = \App\Models\Module::where('location', $loc21)->get();
if($loc!='all'){
    $link=$loc;
}else{
    $link='all';
}
                    @endphp
@extends('frontend.layouts.home')
@section('title','Home')

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.carousel.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.theme.default.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.theme.green.min.css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style type="text/css">
  

.owl-item .item .buddy-wrapper:hover {
    box-shadow: 0px 4px 12px #000000ad;
    transition: all .3s ease-in 0s;
}

.owl-item .super-agent-block:hover {
    box-shadow: 0px 4px 12px #000000ad;
    transition: all .3s ease-in 0s;
}


</style>

@endsection
@section('content')
    
    @include('frontend.layouts.header')

    <!-- BAnnar Section -->
    @php
	 
     $loc=Request::segment(3);
    @endphp
    @if($loc=='uae' || $loc=='kuwait' || $loc=='ksa' || $loc=='oman' || $loc=='qatar' || $loc=='bahrain')
    @include('frontend.home.filter')
	
    <!-- BAnnar Section -->
    @else
    @include('frontend.home.rent_filter')
 
    @endif
	
    <!-- Handpicked homes -->
    
    @include('frontend.home.handpicked-home')

    <!-- Section Featured Listing -->
    @include('frontend.home.handpicked')
    
    <!-- Introduction -->
    @include('frontend.home.intro')
    @foreach($modules as $module)
            @if($module->type=='buddy')
    <!-- Top rated buddies section -->
    @include('frontend.home.top-buddy')
    @endif
	@endforeach
    @foreach($modules as $module)
            @if($module->type=='agent')
    <!-- Super Agents -->
    @include('frontend.home.super-agents')
    @endif
    @endforeach
    <!-- Places to visit -->
    @include('frontend.home.we-here')
    
    <!-- Blog Section -->
    @include('frontend.home.blogs')

    <!-- We are here -->
    
    <!-- Testimonials -->
    @include('frontend.home.testimonials')

    @include('frontend.layouts.footer')
	<div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
				<h4 class="modal-title">Properties Virtual Tours for Rent</h4>
                    <button type="button" class="close stopvideo" data-dismiss="modal" aria-hidden="true">&times;</button>
                    
                </div>
                <!-- <div class="modal-body">
                    <iframe id="cartoonVideo" width="460" height="315" src="//www.youtube.com/embed/YE7VzlLtp-4" frameborder="0" allowfullscreen></iframe>
				</div> -->
				<video id="video1" width="460" controls style="padding: 30px;width: 493px;">
  <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
  <source src="mov_bbb.ogg" type="video/ogg">
  Your browser does not support HTML5 video.
</video>
            </div>
        </div>
	</div>

	<div id="myModal1" class="modal fade">
	
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
				<h4 class="modal-title">Properties Virtual Tours for Sale</h4>
                    <button type="button" class="close stopvideo" data-dismiss="modal" aria-hidden="true">&times;</button>
                    
                </div>
                <!-- <div class="modal-body">
                    <iframe id="cartoonVideo1" width="460" height="315" src="//www.youtube.com/embed/YE7VzlLtp-4" frameborder="0" allowfullscreen></iframe>
				</div> -->
				<video id="video" width="460" controls style="padding: 30px;width: 493px;">
  <source src="https://www.w3schools.com/html/mov_bbb.mp4" type="video/mp4">
  <source src="mov_bbb.ogg" type="video/ogg">
  Your browser does not support HTML5 video.
</video>
            </div>
        </div>
	</div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/owl.carousel.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <script type="text/javascript">
        $(window).scroll(function(){
	        if ($(this).scrollTop() > 100) {
	        	$('header').addClass('bg-header');
	        } else {
	        	$('header').removeClass('bg-header');
	        }
        });
        $(window).scroll(function(){
	        if ($(this).scrollTop() > 100) {
	        	$('header').addClass('fixed-top');
	        } else {
	        	$('header').removeClass('fixed-top');
	        }
        });
        $(window).scroll(function(){
	        if ($(this).scrollTop() > 100) {
	        	$('#top-header-bar').removeClass('fixed-top');
	        } else {
	        	$('#top-header-bar').addClass('fixed-top');
	        }
        });
    </script>
    <script type="text/javascript">
    	$(document).ready(function(){
    		var start = moment();
            var end = moment().add(30, 'days');
    		$('#reportrange span').html(start.format('DD/MM/YYYY') + '-' + end.format('DD/MM/YYYY'));
    		$('#swap_start_date').val(start.format('YYYY-MM-DD'));
		  	$('#swap_end_date').val(end.format('YYYY-MM-DD'));

			$('#reportrange').daterangepicker({
		        startDate: start,
    			endDate: end,
		        opens: 'left',
				minDate: new Date(),
				maxDate: '+8m',
		    }, 
		  	function(start, end, label) {
		  		$('#swap_start_date').val(start.format('YYYY-MM-DD'));
		  		$('#swap_end_date').val(end.format('YYYY-MM-DD'));
		  		$('#reportrange span').html(start.format('DD/MM/YYYY') + '-' + end.format('DD/MM/YYYY'));
			});

    		$(document).on('click','.reset_buy_filter', function(){
    			$('#buy_form')[0].reset();
    		});

    		$(document).on('click','.reset_rent_filter', function(){
    			$('#rent_form')[0].reset();
    		});

    		$(document).on('click','.reset_buy_advance_filter', function(){
    			$('#buy_advance_form')[0].reset();
    		});

    		$(document).on('click','.reset_rent_advance_filter', function(){
    			$('#rent_advance_form')[0].reset();
    		});

    		$(document).on('click','.reset_swap_filter', function(){
    			$('#swap_form')[0].reset();
    		});
    		$(document).on('click','.reset_cospace_filter', function(){
    			$('#coworkspace_form')[0].reset();
    		});

    		$(document).on('click','.changeBackgroundImage', function(e){
    			var name = $(this).data('name');
    			name = name ? name : 'buy';
    			$('.bannar-section').removeClass('buy rent sale swap cospace agents buddies travel');
    			$('.bannar-section').addClass(name);
    			$('.home-filter-title').hide();
    			$('.title_'+name).show();

    		});
    	});


       $(".stopvideo").on('click',function(){
	location.reload();
});

(function() {
  var hidden = "hidden";

  // Standards:
  if (hidden in document)
    document.addEventListener("visibilitychange", onchange);
  else if ((hidden = "mozHidden") in document)
    document.addEventListener("mozvisibilitychange", onchange);
  else if ((hidden = "webkitHidden") in document)
    document.addEventListener("webkitvisibilitychange", onchange);
  else if ((hidden = "msHidden") in document)
    document.addEventListener("msvisibilitychange", onchange);
  // IE 9 and lower:
  else if ("onfocusin" in document)
    document.onfocusin = document.onfocusout = onchange;
  // All others:
  else
    window.onpageshow = window.onpagehide
    = window.onfocus = window.onblur = onchange;

  function onchange (evt) {
    document.getElementById("video").pause();
  }

  // set the initial state (but only if browser supports the Page Visibility API)
  if( document[hidden] !== undefined )
    onchange({type: document[hidden] ? "blur" : "focus"});
})();

(function() {
  var hidden = "hidden";

  // Standards:
  if (hidden in document)
    document.addEventListener("visibilitychange", onchange);
  else if ((hidden = "mozHidden") in document)
    document.addEventListener("mozvisibilitychange", onchange);
  else if ((hidden = "webkitHidden") in document)
    document.addEventListener("webkitvisibilitychange", onchange);
  else if ((hidden = "msHidden") in document)
    document.addEventListener("msvisibilitychange", onchange);
  // IE 9 and lower:
  else if ("onfocusin" in document)
    document.onfocusin = document.onfocusout = onchange;
  // All others:
  else
    window.onpageshow = window.onpagehide
    = window.onfocus = window.onblur = onchange;

  function onchange (evt) {
    document.getElementById("video1").pause();
  }

  // set the initial state (but only if browser supports the Page Visibility API)
  if( document[hidden] !== undefined )
    onchange({type: document[hidden] ? "blur" : "focus"});
})();
    </script>
@endsection
