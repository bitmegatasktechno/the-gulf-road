@php
$lang=Request::segment(1);
$loc=Request::segment(3);
if($loc == 'ksa' || $loc == 'uae' )
{
   $loc21 = strtoupper($loc);
}else 
{
    $loc21 = ucfirst($loc);
}
 $type=Request::segment(4);
$modules = \App\Models\Module::where('location', $loc21)->get();
if($loc!='all'){
  $link=$loc;
}else{
  $link='all';
}
@endphp
@extends('frontend.layouts.home')
@section('title','Home')

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.carousel.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.theme.default.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.theme.green.min.css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


@endsection
@section('content')
<style>
.owl-carousel .owl-dots.disabled, .owl-carousel .owl-nav.disabled{
  display:block !important;
}
.owl-dots button.owl-dot.active {
    background-color: white !important;
}
#loading {
position: fixed;
width: 100%;
height: 100vh;
background: #fff url('{{asset('/img/loading.gif')}}') no-repeat center center;
z-index: 9999;
}
.owl-carousel .owl-item img {
    display: block !important;
    width: 100px !important;
}
  </style>
  <div id="loading"></div>
    @include('frontend.layouts.header')

    <!-- BAnnar Section -->
    @php
      $loc=Request::segment(3);
    @endphp
    @if($loc=='uae' || $loc=='kuwait' || $loc=='ksa' || $loc=='oman' || $loc=='qatar' || $loc=='bahrain')
    @include('frontend.home.filter')
    <!-- BAnnar Section -->
    @else
    @include('frontend.home.swap_filter')
    @endif
<!-- Next Section -->
@php
                            $ratinguser = \App\Models\Review::where('is_approved',1)->where('review_type',"3")->orderBy('average','ASC')->first();
                            @endphp
                            @php
                            $ratinguser1 = \App\Models\Review::where('is_approved',1)->where('review_type',"3")->orderBy('average','ASC')->first();
                            @endphp
<section class="latest-swap-reviews">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h4>Our latest Swaps properties listed. Choose one for you!</h4>
        <p>Listings made easy and pre-verified.</p>
      </div>
    </div>
  </div>
</section>


<!-- Next Section -->

<section class="prem-prop-wrap">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3>Featured Swaps</h3>
      </div>
      @php $i = 0 ; @endphp
      @foreach($allProperties as $row)
       @php $i++; @endphp

       @php
         $is_premium = \App\Models\Subscription::where('_id',$row->is_premium)->where('type','premium')->first();
          
          if($is_premium && isset($is_premium->sub_plan) ){
              $row->available_from = str_replace('/', '-',$row->available_from);

              $lastDate = strtotime($row->available_from." +".$is_premium->sub_plan[0]['validity']."days");

              if($lastDate < strtotime('Now') ){
                  continue;
              }
          }else{
            continue;
          }

       @endphp

        @if($i < 3)
        <div class="col-md-6">
        @else
        <div class="col-md-4">
        @endif
            @if(isset(Auth::user()->id))
                    @php
                    $fav = \App\Models\Favourate::where('propert_id',$row->_id)->where('user_id',Auth::user()->id)->first();
                    @endphp
            @endif
            @if(@$fav)
            <i class="fa fa-heart fav-icon" style="right: 25px;" onClick="makefav('{{$row->_id}}')"></i>
            @else
            <i class="fa fa-heart-o fav-icon" style="right: 25px;" onClick="makefav('{{$row->_id}}')"></i>
            @endif

      <a href="{{url(app()->getLocale().'/property/'.$link.'/swap/'.$row->_id)}}">
        <div class="property-card with-bg-image bg-white rounded">
          <div class="overlay" style="
    height: 48%;
    width: 100%;
    background: #00000038;
    position: absolute;
    bottom: 0px;
"></div> 
          <button class="red-btn">Premium</button>

           @if(!isset($row->files[0]) || $row->files[0]=='')
            <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
            @else
            @if(file_exists('uploads/swap/'.$row->files[0]))
            <img class="img-fluid property-img" src="{{url('uploads/swap/'.$row->files[0])}}">
            @else
            <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
            @endif    
            @endif

          <!-- <img class="img-fluid property-img" src="{{asset('img/landing-page/Rectangle4.png')}}"> -->
          <div class="pl-3 pr-3 content-wrap">
            <h2>{{\Str::limit(ucfirst($row->title), 35)}}</h2>
            <p class="mb-1" style="color:white;"><i class="mr-2 fa fa-map-marker"></i> 
                {{\Str::limit(ucfirst($row->location_name), 30)}}, {{$row->country}}
            </p>

                            <ul class="list-inline mb-3">
                            @if(@$avg==1)
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    @endif
                                    @if(@$avg==2)
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    @endif
                                    @if(@$avg==3)
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    @endif
                                    @if(@$avg==4)
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    @endif
                                    @if(@$avg==5)
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                    @endif
                                @if(@$avg == 0)
                                <li><img  src="{{asset('img/white_star.png')}}"></li>
                                <li><img src="{{asset('img/white_star.png')}}"></li>
                                <li><img  src="{{asset('img/white_star.png')}}"></li>
                                <li><img  src="{{asset('img/white_star.png')}}"></li>
                                <li><img  src="{{asset('img/white_star.png')}}"></li>
                                
                                @endif
                                    <li class="white-color text" style="color:white !important;">{{@$avg}}</li>
                            </ul>
          </div>
        </div>
        </a>
      </div>
      @endforeach
    </div>
  </div>
  <div class="col-md-12 show-more text-center">
                    <a href="/en/swap/<?php echo $link ?>?country=<?php echo $link ?>&location="><button class="red-bordered mt-5">SHOW MORE <i class="fa fa-angle-right"></i></button></a>
                </div>
</section>


<section class="swap-reviews-tab" style="display: none;">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" href="#profile" role="tab" data-toggle="tab">Swap Review </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#buzz" role="tab" data-toggle="tab">Trustpilot</a>
          </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content bg-white">
          <div role="tabpanel" class="tab-pane active" id="profile">
            <div class="p-5">
              <div class="row">
                <div class="col-md-5">
                  <img class="img-fluid" src="{{asset('img/swap-home.jpeg')}}">
                </div>
                <div class="col-md-1">
                @if(!isset($ratinguser->user->image) || $ratinguser->user->image=='')
                                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 48px;">
                                    @else
                                    @if(file_exists('uploads/profilePics/'.$ratinguser->user->image))
                                    <img class="circle" src="{{url('uploads/profilePics/'.$ratinguser->user->image)}}" style="height: 48px;">
                                    @else
                                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 48px;">
                                    @endif
                                    @endif
                  
                </div>
                <div class="col-md-5 pl-0">
                  <div class="review-content">
                    <h4>{{ucfirst(@$ratinguser->user->name ?? 'Unknown')}}</h4>
                    <span>A home in the beautiful hills</span>
                    <h6>Stayed {{@$ratinguser->created_at->format('M Y')}} <br><br>
                    @if(@$ratinguser->average==1)
                    <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                @elseif(@$ratinguser->average==2)
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                @elseif(@$ratinguser->average==3)
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                @elseif(@$ratinguser->average==4)
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                @elseif(@$ratinguser->average==5)
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                @endif
                  </h6>
                    <p>{{@$ratinguser->review}}</p>
                    <a href="{{url(app()->getLocale().'/agent/'.$link.'/'.@$ratinguser->user_id)}}"><button class="red-btn rounded">VIEW THIS HOME</button></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="buzz">
            <div class="p-5">
              <div class="row">
                <div class="col-md-5">
                  <img class="img-fluid" src="{{asset('img/landing-page/Mask.png')}}">
                </div>
                <div class="col-md-1">
                @if(!isset($ratinguser1->user->image) || $ratinguser1->user->image=='')
                                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 48px;">
                                    @else
                                    @if(file_exists('uploads/profilePics/'.$ratinguser1->user->image))
                                    <img class="circle" src="{{url('uploads/profilePics/'.$ratinguser1->user->image)}}" style="height: 48px;">
                                    @else
                                    <img class="circle" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 48px;">
                                    @endif
                                    @endif
                </div>
                <div class="col-md-5 pl-0">
                  <div class="review-content">
                  <h4>{{ucfirst(@$ratinguser1->user->name ?? 'Unknown')}}</h4>
                    <span>A home in the beautiful hills</span>
                    <h6>Stayed {{@$ratinguser1->created_at->format('M Y')}} <br><br>
                    @if(@$ratinguser1->average==1)
                    <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                @elseif(@$ratinguser1->average==2)
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                @elseif(@$ratinguser1->average==3)
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                @elseif(@$ratinguser1->average==4)
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                @elseif(@$ratinguser1->average==5)
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                <img class="mr-1" src="{{asset('img/landing-page/ic_star-active.png')}}">
                @endif</h6>
                    <p>{{@$ratinguser1->review}}</p>
                    <a href="{{url(app()->getLocale().'/agent/'.$link.'/'.@$ratinguser1->user_id)}}"><button class="red-btn rounded">VIEW THIS HOME</button></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Next Section -->
<section class="travel-transform">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h4>Swapping transforms you.<br>
Let us transform the way you swap your home.</h4>
      </div>
    </div>
  </div>
</section>

<!-- Next Section -->
<section class="register-people-earning">
  <div class="container">
    <div class="row d-flex align-items-center">
      <div class="col-md-6">
        <img class="img-fluid small-mb-3" src="{{asset('img/landing-page/Bitmap22.png')}}" style="border-radius: 25px;">
      </div>
      <div class="col-md-6">
        <img class="img-fluid" src="{{asset('img/create-profiles.png')}}">
        <h4>Experiences</h4>
        <h5>you’ll love</h5>
        <p>Enjoy the experience of comparing what you have to swap with.It's different experience to live and swap house with someone. Every home is full of character, comfort and convenience. Somewhere truly to write home about.</p>
      </div>
    </div>

    <div class="row d-flex align-items-center mt-5 mb-5">
      <div class="col-md-4">
        <img class="img-fluid" src="{{asset('img/landing-page/people.png')}}">
        <h4>Locations</h4>
        <h5>you’ll love</h5>
        <p>Everyone loves great locations to work and live in. We help you connecting and living at the place of your choice. You might even make some new friends.</p>
      </div>
      <div class="offset-md-2 col-md-6">
        <img class="img-fluid small-mb-3" src="{{asset('img/landing-page/Bitmap33.png')}}" style="border-radius: 25px;">
      </div>
      
    </div>

    <div class="row d-flex align-items-center">
      <div class="col-md-6">
        <img class="img-fluid small-mb-3" src="{{asset('img/landing-page/Bitmap44.png')}}" style="border-radius: 25px;">
      </div>
      <div class="col-md-6">
        <img class="img-fluid" src="{{asset('img/landing-page/Places-img.png')}}">
        <h4>Prices</h4>
        <h5>you’ll love</h5>
        <p>Swap for a lesser price and we would try best to find an affordable and most comparable home as per your requirements. Our compare feature just makes that easier to negotiate the price and save money.</p>
      </div>
    </div>
  </div>
</section>
<!-- Next Section -->
<section class="just-few-things">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h4>Just a few things to happen when listing your home.</h4>
      </div>
      <div class="col-md-4 text-center">
        <img src="{{asset('img/landing-page/plane.png')}}">
        <p>Compare feature with the home being swapped.</p>
      </div>
      <div class="col-md-4 text-center">
        <img src="{{asset('img/landing-page/award.png')}}">
        <p>Affordable subscription plans.</p>
      </div>
      <div class="col-md-4 text-center">
        <img src="{{asset('img/landing-page/ratings.png')}}">
        <p>Verified details, a chat option and reviews.</p>
      </div>
    </div>
  </div>
</section>
<!-- Next Section -->
<section class="quick-guide-swapping">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h4>Find how to swap start with baby steps!</h4>
        <p>Discover a new way to travel - start swapping in a few easy steps</p>
        <!-- <button class="red-bordered rounded">HOW IT WORKS</button> -->
      </div>
    </div>
  </div>
</section>


<section class="travel-trns"></section>

<!-- Next Section -->
<section class="ready-to-dip-toe" style="display: none;">
  <div class="container">
    <div class="row">
      <div class="offset-md-2 col-md-8">
        <div class="bg-white p-4 rounded text-center">
          <h4>Ready to dip a toe?</h4>
          <p class="mb-4">Our free trial will help you get off to a swimming start</p>
          <p><img class="mr-2" src="{{asset('img/landing-page/check-icon.png')}}">All the tools you need to find a swap</p>
          <p><img class="mr-2" src="{{asset('img/landing-page/check-icon.png')}}">Try free for the first two weeks*</p>
          <p><img class="mr-2" src="{{asset('img/landing-page/check-icon.png')}}">Friendly experts to help you get started</p>
          <a href="{{url(app()->getLocale().'/swap/'.$link.'?country='.$link.'&location=')}}"><button class="red-btn rounded mb-4">LET'S START SWAPPING</button></a>
          <p><small>*You can cancel online, at anytime</small></p>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Next Section -->
<section class="partners-logo">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h4>Look who's talking about us</h4>
      </div>
      @php
      $icons = \App\Models\CompanyIcon::where('status',1)->get();
      @endphp

      @foreach($icons as $icon)
      <div class="col-md-2 col-4">
        <img src="{{asset('../uploads/companiesIcons/'.$icon->logo)}}">
      </div>
      @endforeach

    </div>
  </div>
</section>

<!-- Next Section -->
<section class="lovely-people">
  <div class="container">
    <div class="row">
      <div class="offset-md-1 col-md-5">
        <h4>Our lovely people are always here for you</h4>
      </div>
      <div class="col-md-6">
        <p>Please get in touch if you…</p>
        <p><img class="mr-3" style="filter: brightness(0) invert(1);" src="{{asset('img/chat_ic_agent.png')}}"> Have any questions before you join</p>
        <p><img class="mr-3" src="{{asset('img/user1.png')}}"> Have any questions after you join</p>
        <p><img class="mr-3" src="{{asset('img/people.png')}}"> Have any questions even if you don't want to join.</p>
        <a href="{{url(app()->getLocale().'/contact/'.$link)}}"><button>Contact Us</button></a>
      </div>
    </div>
  </div>
</section>

<!-- Next Section -->
<section class="our-pop-count" style="display: none;">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h4>Our popular countries</h4>
        <p>Thousands of homes to swap in 110+ countries</p>
        <a href="">United States </a>
        <a class="mr-4" href="">Italy</a>
        <a class="mr-4" href="">Australia </a>
        <a class="mr-4" href="">Spain</a>
        <div class="mt-4">
        <a href="{{url(app()->getLocale())}}"><button class="red-bordered rounded">Explore countries</button></a>
        </div>                
      </div>
    </div>
  </div>
</section>

@include('frontend.layouts.footer')
@endsection


<script src='https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js'></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="{{asset('js/script.js')}}"></script> 
<script src="{{asset('lib/bootstrap.bundle.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/owl.carousel.min.js"></script>
<script type="text/javascript">
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('bg-header');
    } else {
       $('header').removeClass('bg-header');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('fixed-top');
    } else {
       $('header').removeClass('fixed-top');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('#top-header-bar').removeClass('fixed-top');
    } else {
       $('#top-header-bar').addClass('fixed-top');
    }
});
  
</script>
<script type="text/javascript">


jQuery(document).ready(function() {
    jQuery('#loading').fadeOut(1000);
});
</script>

</body>

</html>
