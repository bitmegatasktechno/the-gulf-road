<style>
.property-card.with-bg-image .fav-icon{
  top: 1px !important;
  right: -5px !important;
}
</style>
@php
$lang=Request::segment(1);
$loc=Request::segment(3);
if($loc == 'ksa' || $loc == 'uae' )
{
   $loc21 = strtoupper($loc);
}else 
{
    $loc21 = ucfirst($loc);
}
 $type=Request::segment(4);
$modules = \App\Models\Module::where('location', $loc21)->get();
if($loc!='all'){
$link=$loc;
}else{
$link='all';
}
                    @endphp
@extends('frontend.layouts.home')
@section('title','Home')

@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.carousel.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.theme.default.min.css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/assets/owl.theme.green.min.css" />
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


@endsection
@section('content')
<style>
#loading {
position: fixed;
width: 100%;
height: 100vh;
background: #fff url('{{ asset('img/loading.gif') }}') no-repeat center center;
z-index: 9999;
}


.owl-item .item .buddy-wrapper:hover {
    box-shadow: 0px 4px 12px #000000ad;
    transition: all .3s ease-in 0s;
}

.owl-item .super-agent-block:hover {
    box-shadow: 0px 4px 12px #000000ad;
    transition: all .3s ease-in 0s;
}


  </style>
<div id="loading"></div>  
    @include('frontend.layouts.header')

    <!-- BAnnar Section -->
    @php
      $loc=Request::segment(3);
    @endphp
    @if($loc=='uae' || $loc=='kuwait' || $loc=='ksa' || $loc=='oman' || $loc=='qatar' || $loc=='bahrain')
    @include('frontend.home.filter')
    <!-- BAnnar Section -->
    @else
    @include('frontend.home.buddies_filter')
    @endif

<!-- Super Agents -->
<section class="super-agents pt-5 pb-5" @if(count($buddies) < 1) style="display: none;" @endif>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3>Top Buddies</h3>
        <p>Most popular</p>
      </div>
      <div class="col-md-12 p-0">
        <div id="owl-example" class="owl-carousel">
        @foreach($buddies as $agent)
        @php
                            $totaluser = \App\Models\Review::where('is_approved',1)->where('review_type',"7")->where('type_id',$agent->_id)->count();
                            $ratinggiven1=\App\Models\Review::where('is_approved',1)->where('review_type',"7")->where('type_id',$agent->_id)->sum('average');
                            $totalrating=@$totaluser*5;
                            if($totalrating == 0){
                                $totalrating=1;
                            }
                            $avg=number_format(@$ratinggiven1/@$totalrating*5);
                            @endphp
                    <a href="{{url(app()->getLocale().'/buddy/'.$link.'/'.$agent->_id)}}">
                        <div>
                            <div class="col-md-12">
                                <div class="super-agent-block pb-4">
                                    <div class="img-block">
                                    @if(!isset($agent->image) || $agent->image=='')
                                    <img class="dp-block img-fluid" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 150px;margin-left:auto;margin-right: auto;width: auto;">
                                    @else
                                    @if(file_exists('uploads/profilePics/'.$agent->image))
                                    <img class="dp-block img-fluid" src="{{url('uploads/profilePics/'.$agent->image)}}" style="height: 150px;margin-left:auto;margin-right: auto;width: auto;">
                                    @else
                                    <img class="dp-block img-fluid" src="{{asset('/img/ic_user_placeholder.png')}}" style="height: 150px;margin-left:auto;margin-right: auto;width: auto;">
                                    @endif
                                    @endif
                                        <div class="star-block">
                                            <div class="row mt-1 mb-1">
                                                <div class="col-md-7">
                                                    <ul class="list-inline mb-0 pr-3 dp-flex align-itmes-center">
                                                        @if(@$avg==1)
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        @elseif(@$avg==2)
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        @elseif(@$avg==3)
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        @elseif(@$avg==4)
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        @elseif(@$avg==5)
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        <li><img src="{{asset('img/white_star.png')}}"></li>
                                                        @endif
                                                        <li class="white-color text" style="color:white !important;">{{@$avg ? $avg : 'Not rated yet'}}</li>
                                                    </ul>
                                                </div>
                                                <div class="col-md-5 pl-0">
                                                    <ul class="list-inline mb-0 pl-3 pr-3 text-right">
                                                        <li class=""><img style="height: 10px;" src="{{asset('img/mail_white_agent_ic.png')}}"></li>
                                                        <li class="ml-2"><img style="height: 10px;" src="{{asset('img/call_white_agent_ic.png')}}"></li>
                                                        <li class="ml-2"><img style="height: 10px;" src="{{asset('img/whatsapp_white_agent_ic.png')}}"></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="plr-10">
                                        <h4>{{\Str::limit(@$agent->name, 20, '...')}}</h4>
                                        <h6>{{@$agent->country}}  ·  {{ucfirst(@$agent->gender)}}  ·  {{@$agent->age}} yrs</h6>
                                        <small class="dp-block">LANGUAGE KNOWN</small>
                            @if($agent->buddyProfessional)
                                @php
                                    $languages = $agent->buddyProfessional->languages;
                                    $lang = 0;
                                @endphp

                                @if(is_array($languages))
                                    @foreach($languages as $subrow)
                                        @if($lang<3)
                                            <span class="mr-1">{{$subrow}}</span>
                                            @php
                                                $lang++;
                                            @endphp
                                        @else
                                            <span class="mr-1 text-center">+1</span>
                                            @break
                                        @endif
                                    @endforeach
                                @else
                                    <span class="mr-1">N/A</span>
                                @endif
                            @endif
                                        <hr>
                                        <p class="mb-1">LOCALITIES</p>
                                        
                                        <span class="mr-3 light-blue">{{$agent->country}}</span>
                                            
                                        
                                        <p class="mb-1 mt-3">PROPERTIES</p>
                                        <span class="full-width">
                                            <div class="row">
                                            <div class="col-md-6 pr-0 text-center bdr-right">
                                    <strong class="dp-block mb-0">{{$agent->rentProperties->count()}}</strong>
                                    <small class="dp-block mb-0">For Sale</small>
                                </div>
                                <div class="col-md-6 pr-0 text-center">
                                    <strong class="dp-block mb-0">{{$agent->saleProperties->count()}}</strong>
                                    <small class="dp-block mb-0">For Rent</small>
                                </div>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </a>
                        @endforeach
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
   <!--  <div class="col-md-12 show-more text-center">
        <a href="/en/buddies/<?php echo $link ?>?country=<?php echo $link ?>&location=">
            <button class="red-bordered mt-5">SHOW MORE <i class="fa fa-angle-right"></i></button>
        </a>
    </div> -->
</section>
<section class="become-a-buddy">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
        <h3>Become a Buddy</h3>
        <p>Let yourself be connected to help others<br> to live where you are now!</p>
          @if(Auth::user())  
            <a href="{{url('/').'/en/profile/'.$link}}">
            @else 
            <a href="javascript:;" onclick="showLoginForm();">
            @endif
          <button class="budy-btn ml-4">BECOME BUDDY <span class="ml-3">></span> </button></a>
      </div>
    </div>
  </div>
</section>
<section class="find-buddy-location" style="background-color: white;">
  <div class="container">
    <div class="row d-flex align-items-center">
    <div class="col-md-8">
        <h4 style="color:red;font-weight: 600">Find Buddy At Any Location!</h4>
        <p>Have you ever started a new job in a new country or planning to relocate and felt completely lost on the first day? Were you able to “hit the ground running,” or did it take a long time for you to come up to speed with your new country’s environment, processes, and unspoken rules? Coming to a new country can be exciting, but it can also be very stressful and overwhelming.</p>
        <p>The unique concept and platform of Thegulfroad helps you find a buddy who can coach you, mentor you and help you to settle down in your new city. Assigning a buddy who can help ease the transition into your new country can be very beneficial for all involved, especially during the early days.
        </p>
        <p>Buddies should have the skills and knowledge to perform the following types of tasks: </p>
        <ul style="list-style-type:none;">
            <li><i class="fa fa-briefcase" aria-hidden="true"></i>Teaching/or tutoring, such as explaining unfamiliar tasks</li>
            <li><i class="fa fa-car" aria-hidden="true"></i>Explaining how to use transportation or mode of commutation,understanding where you want to live</li>
            <li><i class="fa fa-car" aria-hidden="true" style="opacity: 0;"></i> and expectations, making travel arrangements, and the like</li>
            <li><i class="fa fa-users" aria-hidden="true"></i>Socializing with the new people in town on norms, culture, and unwritten guidelines of the city</li>
            <li><i class="fa fa-handshake-o" aria-hidden="true"></i>Sharing insights on how things are done in the city</li>
            <li><i class="fa fa-coffee" aria-hidden="true"></i>Involving the new guests in social or informal activities, such as lunch, coffee, and such</li>
            <li><i class="fa fa-plane" aria-hidden="true"></i>Being comfortable and settle quickly upon your arrival or even before you arrive</li>
        </ul>
        <!-- <a href="https://dev.thegulfroad.com/en/buddies/<?php echo $link ?>?country=<?php echo $link ?>&location=" style="color:red;">Explore Buddies</a> -->
        <a href="{{url('en/buddies/'.$link.'?country='.$link.'&location=')}}" style="color:red; text-decoration:none!important;">Explore Buddies</a>
      </div>
      <div class="col-md-4">
        <img src="{{ asset('img/buddiesLanding3.jpg') }}" style="width: 100%;height: auto;" class="img-responsive">
      </div>
    </div>
  </div>
</section>
<section class="benifits-wrap">
  <div class="container">
    <div class="row">
      <div class="col-md-4 text-center">
        <i class="fa fa-home" style="font-size: 55px;" aria-hidden="true"></i>
        <h4>Searching stay!</h4>
        <p>We help you search in a new place of stay based on your requirement</p>
      </div>
      <div class="col-md-4 text-center">
        <i class="fa fa-university" style="font-size: 55px;" aria-hidden="true"></i>

        <h4>Searching school!</h4>
        <p>We help you search a school where you children can go</p>
      </div>
      <div class="col-md-4 text-center">
        <i class="fa fa-handshake-o" style="font-size: 55px;" aria-hidden="true"></i>
        <h4>Sharing insights!</h4>
        <p>We help you share our insights about the place, norms, culture, and unwritten guidelines</p>
      </div>
      <div class="col-md-4 text-center">
        <i class="fa fa-coffee" style="font-size: 55px;" aria-hidden="true"></i>

        <h4>Places to eat and socialize!</h4>
        <p class="mb-0">We help you socialize with the local community and make friends and learn new languages</p>
      </div>
      <div class="col-md-4 text-center">
        <i class="fa fa-map-o" style="font-size: 55px;" aria-hidden="true"></i>

        <h4>City guide!</h4>
        <p class="mb-0">We help you in understanding the city better</p>
      </div>
      <div class="col-md-4 text-center">
        <i class="fa fa-newspaper-o" style="font-size: 55px;" aria-hidden="true"></i>

        <h4>Special arrangement!</h4>
        <p class="mb-0">We help you by making special arrangements for you to buy furniture, electronics and other household goods!</p>
      </div>
    </div>
  </div>
</section>

<!-- Next Section -->
<section class="register-people-earning">
  <div class="container">
    <div class="row d-flex align-items-center">
      <div class="col-md-6">
        <img class="img-fluid small-mb-3" style="border-radius:10px;" src="{{asset('img/buddiesregister.jpeg')}}">
      </div>
      <div class="col-md-6">
        <img class="img-fluid" src="{{asset('img/landing-page/register.png')}}">
        <h4>Register</h4>
        <h5>you’ll love</h5>
        <p>We analyse your needs and budget to make arrangements for your stay and living as comfortable as possible. In the crucial first weeks we provide you with the moral support as well know-how of the place and where you should take your apartment which satifies all your needs.</p>
      </div>
    </div>
    <div class="row d-flex align-items-center mt-5 mb-5">
      <div class="col-md-4">
        <img class="img-fluid" src="{{asset('img/landing-page/people.png')}}">
        <h4>People</h4>
        <h5>you’ll love</h5>
        <p>Get in touch and meet with new people and make friends. Socializing is a real necessity and especially when you are alone. Come and get together to feel the culture of your new destined city.</p>
      </div>
      <div class="offset-md-2 col-md-6">
        <img class="img-fluid small-mb-3" style="border-radius:10px;" src="{{asset('img/buddiespeople.jpeg')}}">
      </div>
    </div>
    <div class="row d-flex align-items-center">
      <div class="col-md-6">
        <img class="img-fluid small-mb-3" style="border-radius:10px;" src="{{asset('img/buddiesearning.jpeg')}}">
      </div>
      <div class="col-md-6">
        <img class="img-fluid" src="{{asset('img/landing-page/earning.png')}}">
        <h4>Earnings</h4>
        <h5>you’ll love</h5>
        <p>As a buddy you earn a side income and what’s better than helping and earning on something which you are aware off. Your experiences, challenges and comforts are shared with the new comers and for this you get paid!</p>
      </div>
    </div>
  </div>
</section>
<!-- Next Section -->
@if(count($saleProperties)>0 || count($rentProperties)>0)
@foreach($modules as $module)
@if($module->type=='rent')
<section class="prem-prop-wrap">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3>Popular Properties</h3>
      </div>
      @foreach($saleProperties as $saleProperty)
      <div class="col-md-6">
      @if(isset(Auth::user()->id))
                    @php
                    $fav = \App\Models\Favourate::where('propert_id',$saleProperty->_id)->where('user_id',Auth::user()->id)->first();
                    @endphp
                    @endif
                    @if(@$fav)
                    <i class="fa fa-heart fav-icon" onClick="makefav('{{$saleProperty->_id}}')"></i>
                    @else
                    <i class="fa fa-heart-o fav-icon" onClick="makefav('{{$saleProperty->_id}}')"></i>
                    @endif
      <a href="{{url(app()->getLocale().'/property/'.$link.'/buy/'.$saleProperty->_id)}}">
        <div class="property-card with-bg-image bg-white rounded">
            <div class="overlay" style="height: 48%;width: 100%;bottom: 0px;position: absolute;background: #00000038;"></div>
          <button class="red-btn">Premium</button>
          @if(!isset($saleProperty->files[0]) || $saleProperty->files[0]=='')
           <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
            @else
            @if(file_exists('uploads/properties/'.$saleProperty->files[0]))
            <img class="img-fluid property-img" src="{{url('uploads/properties/'.$saleProperty->files[0])}}">
            @else
            <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
            @endif
            @endif
          <div class="pl-3 pr-3 content-wrap">
            <h2>{{@$saleProperty->property_title}}</h2>
            <ul class="list-inline red-list mb-4">
              <li>{{@$saleProperty->price}} AED / yr</li>
              <li class="mr-3"><img style="filter: brightness(0) invert(1);" class="mr-1" src="{{asset('img/red-bed.png')}}"> {{@$saleProperty->no_of_bedrooms}}</li>
              <li class="mr-3"><img style="filter: brightness(0) invert(1);" class="mr-1" src="{{asset('img/red-tub.png')}}"> {{@$saleProperty->no_of_bathrooms}}</li>
              <li class="mr-3"><img style="filter: brightness(0) invert(1);" class="mr-1" src="{{asset('img/red-car.png')}}"> {{@$saleProperty->no_of_open_parking}}</li>
            </ul>
          </div>
        </div>
        </a>
      </div>
      @endforeach
      @foreach($rentProperties as $rentProperty)
      <div class="col-md-4">
      @if(isset(Auth::user()->id))
                    @php
                    $fav = \App\Models\Favourate::where('propert_id',$rentProperty->_id)->where('user_id',Auth::user()->id)->first();
                    @endphp
                    @endif
                    @if(@$fav)
                    <i class="fa fa-heart fav-icon" style="right: 25px;z-index: 999;" onClick="makefav('{{$rentProperty->_id}}')"></i>
                    @else
                    <i class="fa fa-heart-o fav-icon" style="right: 25px;z-index: 999;" onClick="makefav('{{$rentProperty->_id}}')"></i>
                    @endif
      <a href="{{url(app()->getLocale().'/property/'.$link.'/rent/'.$rentProperty->_id)}}">
        <div class="property-card with-bg-image bg-white rounded">
        <div class="overlay" style="height: 48%;bottom: 0px;width: 100%;position: absolute;background: #00000038;"></div>
          <button class="red-btn">Premium</button>
          @if(!isset($rentProperty->files[0]) || $rentProperty->files[0]=='')
           <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
            @else
            @if(file_exists('uploads/properties/'.$rentProperty->files[0]))
            <img class="img-fluid property-img" src="{{url('uploads/properties/'.$rentProperty->files[0])}}">
            @else
            <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
            @endif
            @endif
          <!-- <img class="img-fluid property-img" src="{{asset('img/landing-page/Rectangle4.png')}}"> -->
          <div class="pl-3 pr-3 content-wrap">
            <h2>{{@$rentProperty->property_title}}</h2>
            <ul class="list-inline red-list mb-4">
              <li>{{@$rentProperty->rent}} AED / yr</li>
              <li class="mr-3"><img style="filter: brightness(0) invert(1);" class="mr-1" src="{{asset('img/red-bed.png')}}"> {{@$rentProperty->no_of_bedrooms}}</li>
              <li class="mr-3"><img style="filter: brightness(0) invert(1);" class="mr-1" src="{{asset('img/red-tub.png')}}"> {{@$rentProperty->no_of_bathrooms}}</li>
              <li class="mr-3"><img style="filter: brightness(0) invert(1);" class="mr-1" src="{{asset('img/red-car.png')}}"> {{@$rentProperty->no_of_open_parking}}</li>
            </ul>
          </div>
        </div>
        </a>
      </div>

      @endforeach
    </div>
  </div>
  <div class="col-md-12 show-more text-center">
        <a href="{{url(app()->getLocale().'/buy-property/'.$link.'?country='.$link.'&location=')}}"><button class="red-bordered mt-5">SHOW MORE <i class="fa fa-angle-right"></i></button></a>
    </div>
</section>
@endif
@endforeach
@endif

<!-- Next Section -->
<section class="lp-top-footer buddies">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div style="border: 10px solid #000;width:100%;height: 193px;">
      <iframe width="100%" height="175px" src="{{@$video->video}}" title="How it works Buddy." frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
        </iframe>
</div>
      </div>
      <div class="col-md-7">
        <p>Watch out this video to know how this concept works.<br>This is unique and special way to earn and help others and also making new connections and friends. We work on a principle of <b><i>“The currency of real networking is not greed but generosity.”</i></b> and thus we make sure that all the buddies on this portal are verified and of utmost ethical professional standards.<br>The rigours mechanism of their selection is one of our key focus area. We have a fun team whose moto is work smart and play hard.</p>
      </div>
    </div>
  </div>
</section>

@include('frontend.layouts.footer')


<script src='https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js'></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="{{ asset('js/script.js') }}"></script>
<script src="{{ asset('lib/bootstrap.bundle.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/owl.carousel.min.js"></script>
<script type="text/javascript">
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('bg-header');
    } else {
       $('header').removeClass('bg-header');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('header').addClass('fixed-top');
    } else {
       $('header').removeClass('fixed-top');
    }
});
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
       $('#top-header-bar').removeClass('fixed-top');
    } else {
       $('#top-header-bar').addClass('fixed-top');
    }
});
  
</script>
<script type="text/javascript">
  jQuery("#testimonial").owlCarousel({
      loop: true,
      margin: 0,
      responsiveClass: true,
      // autoHeight: true,
      autoplayTimeout: 7000,
      smartSpeed: 800,
      nav: true,
      items:2,
      responsiveClass:true,
      responsive: {
        0: {
          items: 1,
          nav:true
        },

        600: {
          items: 2,
          nav:true
        },

        1024: {
          items: 2,
          nav:true
        },

        1366: {
          items: 2,
          nav:true
        }
      }
});
  jQuery("#owl-example-second").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  // autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:4,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 4,
      nav:true
    },

    1366: {
      items: 4,
      nav:true
    }
  }
});
</script>
<script type="text/javascript">
    jQuery("#owl-example").owlCarousel({
        loop: false,
        rewind: true,
        margin: 0,
        responsiveClass: true,
        autoplayTimeout: 7000,
        smartSpeed: 800,
        nav: true,
        items:4,
        responsiveClass:true,
        responsive: {
            0: {
                items: 1,
                nav:true
            },
            600: {
                items: 2,
                nav:true
            },
            1024: {
                items: 4,
                nav:true
            },
            1366: {
                items: 4,
                nav:true
            }
        }
    });
  jQuery("#carouselone").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:3,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 3,
      nav:true
    },

    1366: {
      items: 3,
      nav:true
    }
  }
});
  jQuery(".owl-carousel").owlCarousel({
  loop: true,
  margin: 0,
  responsiveClass: true,
  // autoHeight: true,
  autoplayTimeout: 7000,
  smartSpeed: 800,
  nav: true,
  items:4,
  responsiveClass:true,
  responsive: {
    0: {
      items: 1,
      nav:true
    },

    600: {
      items: 2,
      nav:true
    },

    1024: {
      items: 4,
      nav:true
    },

    1366: {
      items: 4,
      nav:true
    }
  }
});

jQuery(document).ready(function() {
    jQuery('#loading').fadeOut(1000);
});
</script>

</body>

</html>
