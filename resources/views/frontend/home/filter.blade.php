
@php
$lang=Request::segment(1);
$loc=Request::segment(3);
if($loc == 'ksa' || $loc == 'uae' )
{
   $loc21 = strtoupper($loc);
}else 
{
    $loc21 = ucfirst($loc);
}
 $type=Request::segment(4);
$modules = \App\Models\Module::where('location', $loc21)->get();
 
if($loc!='all'){
    $link=$loc;
}else{
    $link='all';
}

 
@endphp
@if($type=='buy') 
    @php
        $banner = \App\Models\Banner::where('banner_type','buy')->first();   
    @endphp
    @if($banner)
        <section class="bannar-section buy" style="background-image: url('{{ asset('../uploads/banner/'.$banner->banner_file) }}');">
    @endif
@endif
@if($type=='rent')  
  @php
    $banner = \App\Models\Banner::where('banner_type','rent')->first();
  @endphp        
  <section class="bannar-section buy" style="background-image: url('{{ asset('../uploads/banner/'.$banner->banner_file) }}');">
@endif
@if($type=='swap') 
  @php      
    $banner = \App\Models\Banner::where('banner_type','swap')->first();
  @endphp   
    <section class="bannar-section buy" style="background-image: url('{{ asset('../uploads/banner/'.$banner->banner_file) }}');">
@endif
@if($type=='cospace')  
    @php
      $banner = \App\Models\Banner::where('banner_type','cospace')->first();
    @endphp        
    <section class="bannar-section buy" style="background-image: url('{{ asset('../uploads/banner/'.$banner->banner_file) }}');">
@endif

@if($type=='agents')  
    @php 
      $banner = \App\Models\Banner::where('banner_type','agent')->first();  
      
      
    @endphp     
    <section class="bannar-section buy" style="background-image: url('{{ asset('../uploads/banner/'.$banner->banner_file) }}');">
@endif
@if($type=='buddies')  
    @php 
        $banner = \App\Models\Banner::where('banner_type','buddies')->first();  
    @endphp     
<section class="bannar-section buy" style="background-image: url('{{ asset('../uploads/banner/'.$banner->banner_file) }}');">
@endif
 
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="black-background pl-4 pr-4 pt-3 pb-3 rounded mt-5">
                    <h3 class="white-color mb-3 mt-1 home-filter-title title_buy" @if($type=='buy') style="display: block;" @else style="display: none;" @endif>Search Properties For Sale</h3>
                    <h3 class="white-color mb-3 mt-1 home-filter-title title_rent" @if($type=='rent') style="display: block;" @else style="display: none;" @endif>Search Properties For Rent</h3>
                    <h3 class="white-color mb-3 mt-1 home-filter-title title_swap" @if($type=='swap') style="display: block;" @else style="display: none;" @endif>Find A Home To Swap</h3>
                    <h3 class="white-color mb-3 mt-1 home-filter-title title_cospace" @if($type=='cospace') style="display: block;" @else style="display: none;" @endif>Find a Co-Workspace for your Team</h3>
                    <h3 class="white-color mb-3 mt-1 home-filter-title title_agents" @if($type=='agents') style="display: block;" @else style="display: none;" @endif>Find Real Estate Agents</h3>
                    <h3 class="white-color mb-3 mt-1 home-filter-title title_buddies" @if($type=='buddies') style="display: block;" @else style="display: none;" @endif>Find Buddies for your desired neighbourhood</h3>
                    <h3 class="white-color mb-3 mt-1 home-filter-title title_travel" style="display: none;">Find the Perfect Vacations with us</h3>

                    <ul class="nav nav-tabs" role="tablist">
                        @if(!empty($modules))
                        @foreach($modules as $module)
                        @if(@$module->type=='buy')
                        <li class="nav-item changeBackgroundImage" data-name="buy">
                            <a class="nav-link @if($type=='buy') active @endif white-color" href="{{url(app()->getLocale().'/landing/'.$link.'/buy')}}"><i class="mr-3"><img src="{{asset('img/home.png')}}"></i>Buy</a>
                        </li>
                        @endif
                        @endforeach
                        @foreach($modules as $module)
                        @if(@$module->type=='rent')
                        <li class="nav-item changeBackgroundImage" data-name="rent">
                            <a class="nav-link white-color @if($type=='rent') active @endif" href="{{url(app()->getLocale().'/landing/'.$link.'/rent')}}"><i class="mr-3"><img src="{{asset('img/buildings.png')}}"></i>Rent</a>
                        </li>
                        @endif
                        @endforeach
                        @foreach($modules as $module)
                        @if(@$module->type=='swap')
                        <li class="nav-item changeBackgroundImage" data-name="swap">
                            <a class="nav-link white-color @if($type=='swap') active @endif" href="{{url(app()->getLocale().'/landing/'.$link.'/swap')}}"><i class="mr-3"><img src="{{asset('img/swap.png')}}"></i>Swap</a>
                        </li>
                        @endif
                        @endforeach
                        @foreach($modules as $module)
                        @if(@$module->type=='cospace')
                        <li class="nav-item changeBackgroundImage" data-name="cospace">
                            <a class="nav-link white-color @if($type=='cospace') active @endif" href="{{url(app()->getLocale().'/landing/'.$link.'/cospace')}}"><i class="mr-3"><img src="{{asset('img/flat.png')}}"></i>Co-space</a>
                        </li>
                        @endif
                        @endforeach
                        @foreach($modules as $module)
                        @if(@$module->type=='agent')
                        <li class="nav-item changeBackgroundImage" data-name="agents">
                            <a class="nav-link white-color @if($type=='agents') active @endif" href="{{url(app()->getLocale().'/landing/'.$link.'/agents')}}" ><i class="mr-3"><img src="{{asset('img/seller.png')}}"></i>Agents</a>
                        </li>
                        @endif
                        @endforeach
                        @foreach($modules as $module)
                        @if(@$module->type=='buddy')
                        <li class="nav-item changeBackgroundImage" data-name="buddies">
                            <a class="nav-link white-color @if($type=='buddies') active @endif" href="{{url(app()->getLocale().'/landing/'.$link.'/buddies')}}" ><i class="mr-3"><img src="{{asset('img/buddies.png')}}"></i>Buddies</a>
                        </li>
                        @endif
                        @endforeach
                        @endif
                    </ul>
                    <div class="tab-content">
                        @if(!empty($modules))
                        @foreach($modules as $module)
                        @if(@$module->type=='buy' && $type=='buy')
                        <div role="tabpanel" class="tab-pane @if($type=='buy') active @else fade @endif buy-tab" id="buy">
                            <form method="get" name="buy_form" id="buy_form" action="{{url(app()->getLocale().'/buy-property/'.$link)}}">
                                <input type="hidden" name="nearby" class="sale_nearby" value="0">
                                <div class="row">
                                    <div class="col-md-2 pr-0">
                                        <select name="country" id="buy_country" onChange="changeloc_buy('{{$lang}}','{{$loc}}','{{$type}}', '{{url('/')}}')" >
                                            <option value="" selected disabled hidden>Country</option>
                                            @foreach(getAllLocations() as $row)
                                                <option value="{{strtolower($row->name)}}" @if(strtolower($defaultCountry)==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-8 pr-0 pl-0">
                                        <div class="search-box">
                                            <div id="map" style="display:none"></div>
                                            <input class="full-width" type="text" 
                                            id="address" onChange="getLocation()" placeholder="“Search properties by location ”" name="location">
                                            <input type="hidden" name="longitude" id="longitude">
                                            <input type="hidden" name="latitude" id="latitude">
                                            <img class="search-icon" src="{{asset('img/search.png')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-2 pl-0">
                                        <button class="full-width buy_form_btn" type="submit">Search</button>
                                    </div>
                                    <div class="col-md-3 mt-3 pr-0">
                                        <div class="inputbox select inputbox-label-inside">
                                            <select name="house_type" id="buy_house">
                                                <option value="" selected disabled hidden>Select</option>
                                                @foreach(getAllHouseTypes() as $row)
                                                    <option value="{{$row->name}}">{{ucfirst($row->name)}}</option>
                                                @endforeach
                                            </select>
                                            <label for="buy_house">PROPERTY TYPE</label>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2 mt-3 pr-0">
                                        <div class="inputbox select inputbox-label-inside">
                                            <select name="bedrooms" id="buy_beds" >
                                                <option value="" selected disabled hidden>Beds</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                            </select>
                                            <label for="buy_beds">BEDS</label>
                                        </div>
                                    </div>
                                </div>
                             </form>
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="mt-3 mb-0 blue-color">
                                        <a class="white-color" data-toggle="collapse" href="#collapseBuyExample" role="button" aria-expanded="false" aria-controls="collapseBuyExample">
                                            More Options 
                                            <i class="fa fa-angle-down ml-3 mr-2" area-hidden="true"></i>
                                        </a> 
                                        <span>| </span>
                                        <span class="reset_buy_filter" style="cursor: pointer;">Reset Search</span>
                                    </p>
                                </div>
                                <div class="col-md-6 text-right">
                                    <div class="checkbox-block">
                                        <div class="form-group mb-2 mt-3">
                                            <input type="checkbox" id="sale_nearby" class="click-nearby" data-id="sale">
                                            @if(\Auth::user())
                                            <label class="white-color" for="sale_nearby">Nearby Surroundings</label>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="more-filters">
                                    <div class="container">
                                        <div class="row">
                                            <div class="collapse full-width" id="collapseBuyExample">
                                                <div class="bg-white p-4 full-width">
                                                    <form method="get" name="buy_advance_form" id="buy_advance_form" action="{{url(app()->getLocale().'/buy-property/'.$link)}}">
                                                        <input type="hidden" name="nearby" class="sale_nearby" value="0">
                                                        <input type="hidden" name="country" value="{{$link}}">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <select name="city" >
                                                                    <option value="">Select City</option>
                                                                    @foreach(getCountryCities($loc21) as $row)
                                                                        <option value="{{$row->name}}" @if(strtolower($defaultCountry)==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select name="house_type">
                                                                    <option value="">Select Property Type</option>
                                                                    @foreach(getAllHouseTypes() as $row)
                                                                        <option value="{{$row->name}}">{{ucfirst($row->name)}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select name="furnishing_type">
                                                                    <option value="">Select Furnishing Type</option>
                                                                    
                                                                    @foreach(getFurnishingTypes() as $key=>$value)
                                                                        <option value="{{$key}}">{{ucfirst($value)}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select name="amenties">
                                                                    <option value="">Select Amenieties</option>
                                                                    
                                                                    @foreach(getAllAmenities() as $row)
                                                                        <option value="{{$row->id}}">{{ucfirst($row->name)}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            
                                                            <div class="col-md-12 pt-4 pb-4">
                                                                <hr>
                                                            </div>
                                                            <div class="col-md-2" style="display:none;">
                                                                <h6>Listed By</h6>
                                                                <div class="checkbox-block">
                                                                    <div class="form-group mb-2">
                                                                        <input type="checkbox" id="buy_user_list" name="listed_by" value="user">
                                                                        <label for="buy_user_list">USER</label>
                                                                    </div>
                                                                    <div class="form-group mb-2">
                                                                       <input type="checkbox" id="buy_agent_list" name="listed_by" value="agent">
                                                                        <label for="buy_agent_list">AGENT</label>
                                                                    </div>
                                                                    <div class="form-group mb-2">
                                                                        <input type="checkbox" id="buy_buddy_list" name="listed_by" value="buddy">
                                                                        <label for="buy_buddy_list">BUDDY</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <h6>Keyword Search</h6>
                                                                <input class="full-width" type="text" name="property_title" placeholder="Eg.Pool,Security,Ref ID Number ">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <h6>Bathrooms</h6>
                                                                <select name="bathrooms">
                                                                    <option value="">Any Bathrooms</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1" style="display:none;">
                                                                <h6>Size</h6>
                                                                <input class="full-width mb-3" type="text" name="min_size" placeholder="Min">
                                                                <input class="full-width" type="text" name="max_size" placeholder="Max">
                                                            </div>
                                                            <div class="col-md-3" style="display: none;">
                                                                <h6>Real Estate Agent</h6>
                                                                <input class="full-width" type="text" name="agent_name" placeholder="Eg DAMAC properties">
                                                            </div>

                                                            <div class="col-md-12 text-right">
                                                                <button type="button" class="red-bordered mr-3 rounded reset_buy_advance_filter">Reset</button>
                                                                <button type="submit" class="red-btn rounded advance_buy_filter">Search</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @elseif(@$module->type=='rent' && $type=='rent')
                        <div role="tabpanel" class="tab-pane @if($type=='rent') active @else fade @endif buy-tab rent-tab" id="rent">
                            <form method="get" name="rent_form" id="rent_form" action="{{url(app()->getLocale().'/rent-property/'.$link)}}">
                                <input type="hidden" name="nearby" class="rent_nearby" value="0">
                                <div class="row">
                                    <div class="col-md-2 pr-0">
                                        <select name="country" id="rent_country" onChange="changeloc_rent('{{$lang}}','{{$loc}}','{{$type}}', '{{url('/')}}')" >
                                            <option value="" selected disabled hidden>Country</option>
                                            @foreach(getAllLocations() as $row)
                                                <option value="{{strtolower($row->name)}}" @if(strtolower($defaultCountry)==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-8 pr-0 pl-0">
                                        <div class="search-box">
                                            <div id="map" style="display:none"></div>
                                            <input class="full-width" type="text" 
                                            id="address" onChange="getLocation()" placeholder="“Search properties by location”" name="location">
                                            <input type="hidden" name="longitude" id="longitude">
                                            <input type="hidden" name="latitude" id="latitude">
                                            <img class="search-icon" src="{{asset('img/search.png')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-2 pl-0">
                                        <button class="full-width rent_form_btn" type="submit">Search</button>
                                    </div>

                                    <div class="col-md-3 mt-3 pr-0">
                                        <div class="inputbox select inputbox-label-inside">
                                            <select name="house_type" id="rent_house">
                                                <option value="" selected disabled hidden>Select</option>
                                                @foreach(getAllHouseTypes() as $row)
                                                    <option value="{{$row->name}}">{{ucfirst($row->name)}}</option>
                                                @endforeach
                                            </select>
                                            <label for="rent_house">PROPERTY TYPE</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2 mt-3 pr-0">
                                        <div class="inputbox select inputbox-label-inside">
                                            <select name="rent_frequenecy" id="rent_frequenecy">
                                                <option value="" selected disabled hidden>Any</option>
                                                
                                                @foreach(getAllRentFrequency() as $key=>$value)
                                                    <option value="{{$key}}">{{ucfirst($value)}}</option>
                                                @endforeach
                                            </select>
                                            <label for="rent_frequenecy">RENT FREQUENCY</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2 mt-3 pr-0">
                                        <div class="inputbox select inputbox-label-inside">
                                            <select name="bedrooms" id="rent_beds">
                                                <option value="" selected disabled hidden>Beds</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                            </select>
                                            <label for="rent_beds">BEDS</label>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="mt-3 mb-0 blue-color">
                                        <a class="white-color" data-toggle="collapse" href="#collapseRentExample" role="button" aria-expanded="false" aria-controls="collapseRentExample">
                                            More Options 
                                            <i class="fa fa-angle-down ml-3 mr-2" area-hidden="true"></i>
                                        </a> 
                                        <span>| </span>
                                        <span class="reset_rent_filter" style="cursor: pointer;">Reset Search</span>
                                    </p>
                                </div>
                                <div class="col-md-6 text-right">
                                    <div class="checkbox-block">
                                        <div class="form-group mb-2 mt-3">
                                            <input type="checkbox" id="rent_nearby" class="click-nearby" data-id="rent" >
                                            @if(\Auth::user())
                                            <label class="white-color" for="rent_nearby">Nearby Surroundings</label>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="more-filters">
                                    <div class="container">
                                        <div class="row">
                                            <div class="collapse full-width" id="collapseRentExample">
                                                <div class="bg-white p-4 full-width">
                                                    <form method="get" name="rent_advance_form" id="rent_advance_form" action="{{url(app()->getLocale().'/rent-property/'.$link)}}">
                                                        <input type="hidden" name="nearby" class="rent_nearby" value="0">
                                                        <input type="hidden" name="country" value="{{$link}}">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                 <select name="city" >
                                                                    <option value="">Select City</option>
                                                                    @foreach(getCountryCities() as $row)
                                                                        <option value="{{($row->name)}}" @if(strtolower($defaultCountry)==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select name="house_type">
                                                                    <option value="">Select Property Type</option>
                                                                    @foreach(getAllHouseTypes() as $row)
                                                                        <option value="{{$row->name}}">{{ucfirst($row->name)}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select name="furnishing_type">
                                                                    <option value="">Select Furnishing Type</option>
                                                                    @foreach(getFurnishingTypes() as $key=>$value)
                                                                        <option value="{{$key}}">{{ucfirst($value)}}</option>
                                                                    @endforeach
                                                                    
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select name="amenties">
                                                                    <option value="">Select Amenieties</option>
                                                                    
                                                                    @foreach(getAllAmenities() as $row)
                                                                        <option value="{{$row->id}}">{{ucfirst($row->name)}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-12 pt-4 pb-4">
                                                                <hr>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <h6>Listed By</h6>
                                                                <div class="checkbox-block">
                                                                    <div class="form-group mb-2">
                                                                        <input type="checkbox" id="rent_user_list" name="listed_by" value="user">
                                                                        <label for="rent_user_list">USER</label>
                                                                    </div>
                                                                    <div class="form-group mb-2">
                                                                       <input type="checkbox" id="rent_agent_list" name="listed_by" value="agent">
                                                                        <label for="rent_agent_list">AGENT</label>
                                                                    </div>
                                                                    <div class="form-group mb-2">
                                                                        <input type="checkbox" id="rent_buddy_list" name="listed_by" value="buddy">
                                                                        <label for="rent_buddy_list">BUDDY</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <h6>Keyword Search</h6>
                                                                <input class="full-width" type="text" name="property_title" placeholder="Eg.Pool,Security,Ref ID Number ">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <h6>Bathrooms</h6>
                                                                <select name="bathrooms">
                                                                    <option value="">Any Bathrooms</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <h6>Size</h6>
                                                                <input class="full-width mb-3" type="text" name="min_size" placeholder="Min">
                                                                <input class="full-width" type="text" name="max_size" placeholder="Max">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <h6>Rent Frequency</h6>
                                                                <select name="rent_frequenecy">
                                                                    <option value="" selected disabled hidden>Any</option>
                                                                    
                                                                    @foreach(getAllRentFrequency() as $key=>$value)
                                                                        <option value="{{$key}}">{{ucfirst($value)}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>

                                                            <div class="col-md-12 text-right">
                                                                <button type="button" class="red-bordered mr-3 rounded reset_rent_advance_filter">Reset</button>
                                                                <button type="submit" class="red-btn rounded advance_rent_filter">Search</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @elseif(@$module->type=='swap' && $type=='swap')
                        <div role="tabpanel" class="tab-pane @if($type=='swap') active @else fade @endif buy-tab swap home_swap_tab" id="swap">
                            <form method="get" name="swap_form" id="swap_form" action="{{url(app()->getLocale().'/swap/'.$link)}}">
                                <input type="hidden" name="nearby" class="swap_nearby" value="0">
                                <div class="row">
                                    <div class="col-md-2 pr-0">
                                        <select name="country" id="swap_country" onChange="changeloc_swap('{{$lang}}','{{$loc}}','{{$type}}','{{url('/')}}')">
                                            <option value="" selected disabled hidden>Country</option>
                                            @foreach(getAllLocations() as $row)
                                                <option value="{{strtolower($row->name)}}" @if(strtolower($defaultCountry)==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-8 pr-0 pl-0">
                                        <div class="search-box">
                                            <div id="map" style="display:none"></div>
                                            <input class="full-width" type="text" 
                                            id="address" onChange="getLocation()" placeholder="“Search swap properties by location”" name="location">
                                            <input type="hidden" name="longitude" id="longitude">
                                            <input type="hidden" name="latitude" id="latitude">
                                            <img class="search-icon" src="{{asset('img/search.png')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-2 pl-0">
                                        <button class="full-width buy_form_btn" type="submit">Search</button>
                                    </div>
                                    <div class="col-md-3 mt-3 pr-0">
                                        <div class="inputbox select inputbox-label-inside">
                                            <select name="house_type" id="swap_house">
                                                <option value="" selected disabled hidden>Select</option>
                                                @foreach(getAllHouseTypes() as $row)
                                                    <option value="{{strtolower($row->name)}}">{{ucfirst($row->name)}}</option>
                                                @endforeach
                                            </select>
                                            <label for="swap_house">PROPERTY TYPE</label>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2 mt-3 pr-0">
                                        <div class="inputbox select inputbox-label-inside">
                                            <select name="capacity" id="swap_capacity" >
                                                <option value="" selected disabled hidden>Guest</option>
                                                @foreach(range(1, 10) as $row)
                                                    <option value="{{$row}}">{{$row}} Guest</option>
                                                @endforeach
                                                <option value="10+">Above 10 Guest</option>
                                            </select>
                                            <label for="swap_capacity">Guest</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="mt-3 mb-0 blue-color"> 
                                            <span class="reset_swap_filter" style="cursor: pointer;">Reset Search</span>
                                        </p>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div class="checkbox-block">
                                            <div class="form-group mb-2 mt-3">
                                                <input type="checkbox" id="swap_nearby" class="click-nearby" data-id="swap">
                                                @if(\Auth::user())
                                                <label class="white-color" for="swap_nearby">
                                                Nearby Surroundings
                                                </label>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        @elseif(@$module->type=='cospace' && $type=='cospace')
                        <div role="tabpanel" class="tab-pane @if($type=='cospace') active @else fade @endif buy-tab space" id="co-space">
                            <form method="get" name="coworkspace_form" id="coworkspace_form" action="{{url(app()->getLocale().'/coworkspace/'.$link)}}">
                                <input type="hidden" name="nearby" class="cospace_nearby" value="0">
                                <div class="row">
                                    <div class="col-md-2 pr-0">
                                        <select name="country" id="cospace_country" onChange="changeloc_cospace('{{$lang}}','{{$loc}}','{{$type}}', '{{url('/')}}')" >
                                            <option value="" selected disabled hidden>Country</option>
                                            @foreach(getAllLocations() as $row)
                                                <option value="{{strtolower($row->name)}}" @if(strtolower($defaultCountry)==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-8 pr-0 pl-0">
                                        <div class="search-box">
                                            <div id="map" style="display:none"></div>
                                            <input class="full-width" type="text" 
                                            id="address" onChange="getLocation()" placeholder="“Search coworkspace properties by location”" name="location">
                                            <input type="hidden" name="longitude" id="longitude">
                                            <input type="hidden" name="latitude" id="latitude">
                                            <img class="search-icon" src="{{asset('img/search.png')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-2 pl-0">
                                        <button class="red-btn full-width" type="submit">Search</button>
                                    </div>
                                    <div class="col-md-2 mt-3 pr-0">
                                        <div class="inputbox select inputbox-label-inside">
                                            <select name="type" id="cospace_type">
                                                
                                                <option value="open">Open Workspace</option>
                                                <option value="dedicated">Dedicated Workspace</option>
                                                <option value="private">Private Workspace</option>
                                            </select>
                                            <label for="cospace_type">COWORKSPACE TYPE</label>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-2 mt-3 pr-0">
                                       <div class="inputbox select inputbox-label-inside">
                                            <select name="people" id="cospace_peopele">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3-5">3-5</option>
                                                <option value="5-15">5-15</option>
                                                <option value="15+">More than 15</option>
                                            </select>
                                            <label for="cospace_peopele">NO OF PEOPLE</label>
                                        </div>
                                    </div> -->
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="mt-3 mb-0 blue-color"> 
                                            <span class="reset_cospace_filter" style="cursor: pointer;">Reset Search</span>
                                        </p>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div class="checkbox-block">
                                            <div class="form-group mb-2 mt-3">
                                                <input type="checkbox" name="nearby" id="cospace_nearby" class="click-nearby" data-id="cospace">
                                                @if(\Auth::user())
                                                <label class="white-color" for="cospace_nearby">
                                                Nearby Surroundings</label>@endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        @elseif(@$module->type=='agent' && $type=='agents')
                        <div role="tabpanel" class="tab-pane @if($type=='agents') active @else fade @endif buy-tab agents" id="agents">
                            <form method="get" name="agents_form" id="agents_form" action="{{url(app()->getLocale().'/agents/'.$link)}}">
                                <div class="row">
                                    <div class="col-md-2 pr-0">
                                        <select name="country" id="agent_country" onChange="changeloc_agent('{{$lang}}','{{$loc}}','{{$type}}', '{{url('/')}}')" >
                                            <option value="" selected disabled hidden>Country</option>
                                            @foreach(getAllLocations() as $row)
                                                <option value="{{strtolower($row->name)}}" @if(strtolower($defaultCountry)==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-8 pr-0 pl-0">
                                        <div class="search-box">
                                            <div id="map" style="display:none"></div>
                                            <input class="full-width" type="text" 
                                            id="address" onChange="getLocation()" placeholder="Find agents by location" name="location">
                                            <input type="hidden" name="longitude" id="longitude">
                                            <input type="hidden" name="latitude" id="latitude">
                                            <img class="search-icon" src="{{asset('img/search.png')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-2 pl-0">
                                        <button class="full-width">Search</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        &nbsp;
                                    </div>
                                    <div class="col-md-6">
                                        &nbsp;
                                    </div>
                                </div>
                            </form>
                        </div>
                        @elseif(@$module->type=='buddy' && $type=='buddies')
                        <div role="tabpanel" class="tab-pane @if($type=='buddies') active @else fade @endif buy-tab buddies" id="buddies">
                            <form method="get" name="buddy_form" id="buddy_form" action="{{url(app()->getLocale().'/buddies/'.$link)}}">
                                <div class="row">
                                    <div class="col-md-2 pr-0">
                                        <select name="country" id="buddy_country" onChange="changeloc_buddy('{{$lang}}','{{$loc}}','{{$type}}', '{{url('/')}}')" >
                                            <option value="" selected disabled hidden>Country</option>
                                           @foreach(getAllLocations() as $row)
                                                <option value="{{strtolower($row->name)}}" @if(strtolower($defaultCountry)==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-8 pr-0 pl-0">
                                        <div class="search-box">
                                            <div id="map" style="display:none"></div>
                                            <input class="full-width" type="text" 
                                            id="address" onChange="getLocation()" placeholder="Find buddies by location" name="location">
                                            <input type="hidden" name="longitude" id="longitude">
                                            <input type="hidden" name="latitude" id="latitude">
                                            <img class="search-icon" src="{{asset('img/search.png')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-2 pl-0">
                                        <button class="full-width">Search</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        &nbsp;
                                    </div>
                                    <div class="col-md-6">
                                        &nbsp;
                                    </div>
                                </div>
                            </form>
                        </div>
                        @endif
                        @endforeach
                        @endif
                        <div role="tabpanel" class="tab-pane fade buy-tab travel" id="travel">
                            <div class="row">
                                <div class="col-md-2 pr-0">
                                    <select name="country" >
                                        <option value="" selected disabled hidden>Country</option>
                                       @foreach(getAllLocations() as $row)
                                            <option value="{{strtolower($row->name)}}" @if(strtolower($defaultCountry)==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-5 pr-0 pl-0">
                                    <div class="search-box">
                                        <input class="full-width" type="text" placeholder="Where Would you like to Travel?" name="location">
                                        <img class="search-icon" src="{{asset('img/search.png')}}">
                                    </div>
                                </div>
                                <div class="col-md-3 pr-0 ">
                                    <div class="search-box no-line">
                                        <input class="full-width" type="" placeholder="Travel Duration" name="">
                                        <img class="search-icon" src="{{asset('img/calendar.png')}}">
                                    </div>
                                </div>
                                <div class="col-md-2 pl-0">
                                    <button class="full-width">Search</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    &nbsp;
                                </div>
                                <div class="col-md-6">
                                    &nbsp;
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
 
 
    @if(env('APP_ACTIVE_MAP') =='patel_map')
<style>
    #address_result
    {
        z-index: 9;
    }
</style>
   <script src="https://mapapi.cloud.huawei.com/mapjs/v1/api/js?callback=initMap&key={{env('PATEL_MAP_KEY')}}"></script>
    <script>
        function initMap() {
            var mapOptions = {};
            mapOptions.center = {lat: 48.856613, lng: 2.352222};
            mapOptions.zoom = 8;
            var searchBoxInput = document.getElementById('address');
             var map = new HWMapJsSDK.HWMap(document.getElementById('map'), mapOptions);
            // Create the search box parameter.
          var acOptions = {
                location: {
                    lat: 48.856613,
                    lng: 2.352222
                },
                radius: 50000,
                customHandler: callback,
                language: 'en'
            };

            var autocomplete;
            // Obtain the input element.
            // Create the HWAutocomplete object and set a listener.
            autocomplete = new HWMapJsSDK.HWAutocomplete(searchBoxInput, acOptions);
            autocomplete.addListener('site_changed', printSites);
            // Process the search result. 
            // index: index of a searched place.
            // data: details of a searched place. 
            // name: search keyword, which is in strong tags. 
            function callback(index, data, name) {
                console.log(index, data, name);
                var div
                div = document.createElement('div');
                div.innerHTML = name;
                return div;
            }
            // Listener.
            function printSites() {
                 
                
                var site = autocomplete.getSite();
                var latitude = site.location.lat;
                var longitude = site.location.lng;
                var country_name = site.name;
                
                var results = "Autocomplete Results:\n";
                var str = "Name=" + site.name + "\n"
                    + "SiteID=" + site.siteId + "\n"
                    + "Latitude=" + site.location.lat + "\n"
                    + "Longitude=" + site.location.lng + "\n"
                    + "Address=" + site.formatAddress + "\n";

                    
                results = results + str;

                $('#latitude').val(latitude);
                $('#longitude').val(longitude);
                /*var countryname  = site.address.country;
                $('select[name="country"]').val(countryname.toLowerCase());
                $( "#current_country" ).trigger( "change" );


                $('#countryTo').val(country_name);
                $('#country_name').val(country_name);*/
                console.log(latitude, longitude);
                /*console.log(results);*/
            }
            
        }
        function getLocation()
        {

        }
    </script> 

     
    @elseif(env('APP_ACTIVE_MAP')=='google_map')
     
    
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&callback=initMap&libraries=places&v=weekly"
      defer
    ></script>
 
<script>

function initMap() {
  const myLatLng = {
    lat: -25.363,
    lng: 131.044
  };
    
      

  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 4,
    center: myLatLng
  });
    var input = document.getElementById('address');

 map.controls[google.maps.ControlPosition.TOP_RIGHT].push();

var autocomplete = new google.maps.places.Autocomplete(input);
autocomplete.bindTo('bounds', map);
autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);
            var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29),
          title: "Hello World!"
        });
        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindow.open(map, marker);
        });
  new google.maps.Marker({
    position: myLatLng,
    map,
    title: "Hello World!"
  });
}

function getLocation()
 {
//   alert(address);
  var address = $('#address').val();
  // alert(address);
  var geocoder = new google.maps.Geocoder();

  geocoder.geocode( { 'address': address}, function(results, status)
  {


    if (status == google.maps.GeocoderStatus.OK)
    {
      console.log(results[0],"afdcfsa");
      console.log(results[0].address_components);
      const countryObj=results[0].address_components.filter(val=>{
            if(val.types.includes('country')){
                return true
            }
            return false;
      });
     console.log(countryObj,'the country obj');


     var country_name = countryObj[0].long_name;
     console.log(country_name,'name');
      var latitude = results[0].geometry.location.lat();

      var longitude = results[0].geometry.location.lng();
      // alert(latitude +"-----"+ longitude);
     $('#latitude').val(latitude);
     $('#longitude').val(longitude);
     $('#countryTo').val(country_name);
     $('#country_name').val(country_name);
    console.log(latitude, longitude);
    }
  });

}
    </script>

    
   @endif
