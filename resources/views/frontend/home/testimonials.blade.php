@php
$lang=Request::segment(1);
$loc=Request::segment(3);
$type=Request::segment(4);
                        $modules = \App\Models\Module::where('location',$loc)->get();
                        if($loc!='all'){
                            $link=$loc;
                        }else{
                            $link='all';
                        }
                    @endphp
                    @if(count($reviews)>0)
<section class="blogs" >
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>Reviews</h3>
                    <p>What our customers say about us.</p>
                </div>
                <div class="col-md-12 p-0">
                    <div id="testimonial" class="owl-carousel">
                        <div>
                            @if(@$reviews)
                            @foreach($reviews as $review)
                            <div class="col-md-12">
                                <div class="testimonial-block p-4">
                                    <img class="comma" src="{{asset('img/home-quote.png')}}">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img class="circle" src="{{url('uploads/testimonials/'.$review->logo)}}" style="height: 93px;">
                                        </div>
                                        <div class="col-md-9">
                                            <h4>{{@$review->name}}</h4>
                                           
                                            <h6> {{@$review->content}}</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            
                            @endforeach
                            @endif
                </div>
            </div>
        </div>
    </section>
@endif