<section class="places-to-visit">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>Places to Visit</h3>
                    <p>Explore the must-see places</p>
                </div>
            </div>
        </div>
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-md-12">
                    <img class="dp-block img-fluid" src="{{asset('img/map-section.png')}}">
                </div>
            </div>
        </div>
    </section>
