<section class="we-are-here" style="padding: 25px 0;">
<img class="triangle1" src="{{asset('img/ic_triangle_right.png')}}" style="position: absolute;right: 0px;transform: scaleX(-1);opacity: 60%;">
<img class="triangle2" src="{{asset('img/ic_dots.png')}}" style="position: absolute;right: 0px;transform: scaleX(-1);margin-top: 24%;opacity: 60%;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>We Are Here</h3>
                    <p class="pb-3">Countries we are live in</p>
                </div>
                <div class="col-md-3 col-6">
                    <div class="boxImgWrap">
                        <a href="{{url(app()->getLocale().'/home/uae')}}"><img class="img-fluid" src="{{asset('img/UAE_Weah.webp')}}" alt=""></a>
                        <div class="name">UAE</div>
                    </div>
                </div>
                <div class="col-md-4 col-6">
                    <div class="boxImgWrap">
                        <a href="{{url(app()->getLocale().'/home/kuwait')}}"><img class="img-fluid" src="{{asset('img/Kuwait_Weah.webp')}}" alt=""></a>
                        <div class="name">Kuwait</div>
                    </div>
                    <div class="boxImgWrap mt-1">
                        <a href="{{url(app()->getLocale().'/home/oman')}}"><img class="img-fluid" src="{{asset('img/Oman_weah.webp')}}" alt=""></a>
                        <div class="name">Oman</div>
                    </div>
                </div>
                <div class="col-md-5 col-12">
                    <div class="boxImgWrap small-mt-3">
                        <a href="{{url(app()->getLocale().'/home/ksa')}}"><img class="img-fluid" src="{{asset('img/KSA_Weah.webp')}}" alt=""></a>
                        <div class="name">KSA</div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-6">
                            <div class="boxImgWrap mt-2">
                                <a href="{{url(app()->getLocale().'/home/qatar')}}"><img class="img-fluid" src="{{asset('img/Qatar_weah.webp')}}" alt=""></a>
                                <div class="name">Qatar</div>
                            </div>
                        </div>
                        <div class="col-md-6 col-6">
                            <div class="boxImgWrap mt-2">
                                <a href="{{url(app()->getLocale().'/home/bahrain')}}"><img class="img-fluid" src="{{asset('img/Baharin_Weah.webp')}}" alt=""></a>
                                <div class="name">Bahrain</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
