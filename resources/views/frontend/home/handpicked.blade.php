<style>
i.fa.fa-heart{
    padding-top: 2px !important;
    padding-left: 227px !important;
}
</style>
@php
$lang=Request::segment(1);
$loc=Request::segment(3);
if($loc == 'ksa' || $loc == 'uae' )
{
   $loc21 = strtoupper($loc);
}else 
{
    $loc21 = ucfirst($loc);
}
$type=Request::segment(4);
$modules = \App\Models\Module::where('location',$loc21)->get();
if($loc21!='all'){
    $link=$loc;
}else{
    $link='all';
}

 
@endphp
@if(count($rentProperties)>0 || count($saleProperties)>0)
<section class="handpicked-home featured bg-white" style="padding: 71px 0;">
<img class="triangle1" src="{{asset('img/ic_triangle_right.png')}}" style="position: absolute;right: 0px;transform: scaleX(-1);opacity: 60%;">
<img class="triangle2" src="{{asset('img/ic_triangle.png')}}" style="position: absolute;margin-top: 96%;opacity: 60%;">
<div class="container">
    @foreach($modules as $module)
        @if($module->type=='rent')
        <div class="row">
            <div class="col-md-12">
                <h3>Featured listings</h3>
                <span class="dp-block mb-4">Popular rated properties</span>
                <p class="linebar">Rent</p>
            </div>
            @php $count_rent=0; @endphp
            @foreach($rentProperties as $key=>$rentProperty)
            @php
        
                $totaluser = \App\Models\Review::where('is_approved',1)->where('review_type',"2")->where('type_id',$rentProperty->_id)->count();
                $ratinggiven1=\App\Models\Review::where('is_approved',1)->where('review_type',"2")->where('type_id',$rentProperty->_id)->sum('average');
                $totalrating=@$totaluser*5;
                if($totalrating == 0){
                    $totalrating=1;
                }
                $avg=number_format(@$ratinggiven1/@$totalrating*5);
                $is_premium = \App\Models\Subscription::where('_id',$rentProperty->is_premium)->where('type','premium')->first();            
                $is_premiumProp = 1;
                if($is_premium && isset($is_premium->sub_plan) ){
                    $rentProperty->available_from = str_replace('/', '-',$rentProperty->available_from);
                    $lastDate = strtotime($rentProperty->available_from." +".$is_premium->sub_plan[0]['validity']."days");
                    if($lastDate < strtotime('Now') ){
                        $is_premiumProp = 0;
                    }
                }else{
                    $is_premiumProp = 0;
                }
                $count_rent = $count_rent + 1;
                if($count_rent > 8)
                {
                    continue;
                }
            @endphp

            <div class="col-md-3">
                <div class="property-card bg-white rounded" style="height: 388px;">
                @if($is_premiumProp)
                    <button class="red-btn red-btn-new"><img src="{{asset('img/Premium_Tick.png')}}" > &nbsp; Premium</button>
                @endif
                @if(isset(Auth::user()->id))
                @php
                $fav = \App\Models\Favourate::where('propert_id',$rentProperty->_id)->where('user_id',Auth::user()->id)->first();
                @endphp
                @endif
                @if(@$fav)
                    <i class="fa fa-heart" onClick="makefav('{{$rentProperty->_id}}')"></i>
                @else
                    <i class="fa fa-heart-o" onClick="makefav('{{$rentProperty->_id}}')"></i>
                @endif
                    <a href="{{url(app()->getLocale().'/property/'.$link.'/rent/'.$rentProperty->_id)}}">
                @if(!isset($rentProperty->files[0]) || $rentProperty->files[0]=='')
                    <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
                @else
                    @if(file_exists('uploads/properties/'.$rentProperty->files[0]))
                        <img class="img-fluid property-img" src="{{url('uploads/properties/'.$rentProperty->files[0])}}">
                    @else
                        <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
                    @endif
                @endif
                <div class="pl-3 pr-3">

                <h5>{{@$rentProperty->house_type}}</h5>
                <h2>{{\Str::limit(@$rentProperty->property_title, 30, '...')}}</h2>
                <p>{{\Str::limit(ucfirst($rentProperty->location_name), 60)}}, {{$rentProperty->country}}</p>
                <h6>{{@$rentProperty->rent}} {{$rentProperty->rent_currency ? strtoupper($rentProperty->rent_currency) : 'AED'}} / <sapn>{{$rentProperty->rent_frequency}}</sapn></h6>
                <ul class="list-inline">
                @if(@$avg==1)
                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                @elseif(@$avg==2)
                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                @elseif(@$avg==3)
                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                @elseif(@$avg==4)
                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                @elseif(@$avg==5)
                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                    <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                @endif
                @if(@$avg == 0)
                    <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                    <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                    <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                    <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                    <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>                                
                @endif
                    <li class="white-color text" style="color:black !important;">{{@$avg}}</li>
                </ul>
                <ul class="list-inline red-list mb-4">
                    <li class="mr-3"><img class="mr-1" src="{{asset('img/red-bed.png')}}"> {{@$rentProperty->no_of_bedrooms}}</li>
                    <li class="mr-3"><img class="mr-1" src="{{asset('img/red-tub.png')}}"> {{@$rentProperty->no_of_bathrooms}}</li>
                    <li class="mr-3"><img class="mr-1" src="{{asset('img/red-car.png')}}"> {{@$rentProperty->no_of_open_parking}}</li>
                </ul>
            </div>
        </a>
                    </div>
                </div>
                @endforeach
                @php @endphp
                @if($count_rent > 0)
                <div class="col-md-12 show-more text-center">
                    <a href="{{url(app()->getLocale().'/rent-property/'.$link.'?country='.$link.'&location=')}}"><button class="red-bordered mt-3">SHOW MORE <i class="fa fa-angle-right"></i></button></a>
                </div>
                @endif
                <div class="col-md-12 mt-4 mb-4">
                    <hr>
                </div>
            </div>
            @endif
            @endforeach
            @if(count($saleProperties)>0)
            @foreach($modules as $module)
            @if($module->type=='buy')
            <div class="row">
                <div class="col-md-12">
                    <p class="linebar">Sale</p>
                </div>
                @php $count_sale = 0; @endphp
                @foreach($saleProperties as $saleProperty)
                @php
                            $totaluser = \App\Models\Review::where('is_approved',1)->where('review_type',"1")->where('type_id',$saleProperty->_id)->count();
                            $ratinggiven1=\App\Models\Review::where('is_approved',1)->where('review_type',"1")->where('type_id',$saleProperty->_id)->sum('average');
                            $totalrating=@$totaluser*5;
                            if($totalrating == 0){
                                $totalrating=1;
                            }
                            $avg=number_format(@$ratinggiven1/@$totalrating*5);


                            $is_premium = \App\Models\Subscription::where('_id',$saleProperty->is_premium)->where('type','premium')->first();
                            
                            $saleProperty->is_premium = 1;

                            if($is_premium && isset($is_premium->sub_plan) ){
                                $saleProperty->available_from = str_replace('/', '-',$saleProperty->available_from);

                                $lastDate = strtotime($saleProperty->available_from." +".$is_premium->sub_plan[0]['validity']."days");

                                if($lastDate < strtotime('Now') ){
                                    $saleProperty->is_premium = 0;
                                }
                            }else{
                                $saleProperty->is_premium = 0;
                            }
                            $count_sale = $count_sale + 1;
                            if($count_sale > 8)
                            {
                                continue;
                            }



                            @endphp
                <div class="col-md-3">
                    <div class="property-card bg-white rounded" style="height: 388px;">
                    @if($saleProperty->is_premium != '' && $saleProperty->is_premium != 0)
                                    <button class="red-btn red-btn-new"><img src="{{asset('img/Premium_Tick.png')}}" > &nbsp; Premium</button>
                                    @endif
                    @if(isset(Auth::user()->id))
                    @php
                    $fav = \App\Models\Favourate::where('propert_id',$saleProperty->_id)->where('user_id',Auth::user()->id)->first();
                    @endphp
                    @endif
                    @if(@$fav)
                    <i class="fa fa-heart" onClick="makefav('{{$saleProperty->_id}}')"></i>
                    @else
                    <i class="fa fa-heart-o" onClick="makefav('{{$saleProperty->_id}}')"></i>
                    @endif
                    <a href="{{url(app()->getLocale().'/property/'.$link.'/buy/'.$saleProperty->_id)}}">
                    @if(!isset($saleProperty->files[0]) || $saleProperty->files[0]=='')
                        <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
                    @else
                    @if(file_exists('uploads/properties/'.$saleProperty->files[0]))
                        <img class="img-fluid property-img" src="{{url('uploads/properties/'.$saleProperty->files[0])}}">
                        @else
                        <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
                        @endif
                    @endif
                        <div class="pl-3 pr-3">
                        <h5>{{@$saleProperty->house_type}}</h5>
                        <h2>{{\Str::limit(@$saleProperty->property_title, 30, '...')}}</h2>
                        <p>{{\Str::limit(ucfirst($saleProperty->location_name), 60)}}, {{$saleProperty->country}}</p>
                        <h6>{{number_format(@$saleProperty->price)}} {{$saleProperty->price_currency ? strtoupper($saleProperty->price_currency) : 'N/A'}}</h6>
                            <ul class="list-inline">
                            @if(@$avg==1)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==2)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==3)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==4)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg==5)
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                @endif
                                @if(@$avg == 0)
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                
                                @endif
                                <li class="white-color text" style="color:black !important;">{{@$avg}}</li>
                            </ul>
                            <ul class="list-inline red-list mb-4">
                            <li class="mr-3"><img class="mr-1" src="{{asset('img/red-bed.png')}}"> {{@$saleProperty->no_of_bedrooms}}</li>
                            <li class="mr-3"><img class="mr-1" src="{{asset('img/red-tub.png')}}"> {{@$saleProperty->no_of_bathrooms}}</li>
                            <li class="mr-3"><img class="mr-1" src="{{asset('img/red-car.png')}}"> {{@$saleProperty->no_of_open_parking}}</li>
                            </ul>
                        </div>
                        </a>
                    </div>
                </div>
                @endforeach
                @if($count_sale > 0)
                <div class="col-md-12 show-more text-center">
                <a href="{{url(app()->getLocale().'/buy-property/'.$link.'?country='.$link.'&location=')}}"><button class="red-bordered mt-3">SHOW MORE <i class="fa fa-angle-right"></i></button></a>
                </div>
                @endif

            </div>
            @endif
            @endforeach
            @endif
        </div>
    </section>
    @endif
