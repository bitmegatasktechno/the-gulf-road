@php
$lang=Request::segment(1);
 
$type=Request::segment(4);

$loc=Request::segment(3);
if($loc == 'ksa' || $loc == 'uae' )
{
   $loc21 = strtoupper($loc);
}else 
{
    $loc21 = ucfirst($loc);
}

$modules = \App\Models\Module::where('location',$loc21)->get();
if($loc!='all'){
    $link=$loc;
}else{
    $link='all';
}
                    @endphp
<style>
button.owl-dot.active{
    display:none;
}
</style>
@if(count($blogs)>0)
<section class="blogs" >
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <h3>Blogs</h3>
                    <p>Browse our latest blogs</p>
                </div>
                <div class="col-md-2">
                <a href="{{url(app()->getLocale().'/blog-listing/'.$link)}}"><button class="red-btn full-width rounded" autocomplete="off">Show More</button></a>
            </div>
                <div class="col-md-12 p-0">

                    <div id="owl-example-second" class="owl-carousel">
                    @foreach($blogs as $blog)
                    
                    <a href="{{url(app()->getLocale().'/blog-details/'.$link.'/'.$blog->_id)}}">
                        <div>
                            <div class="col-md-12">
                                <div class="blog-wrapper mt-4">
                                @if(!isset($blog->pic) || $blog->pic=='')
		 <img class="rounded" src="{{asset('/img/no-property.png')}}" style="height: 166px;">
		  @else
		  <img class="rounded" src="{{url('uploads/blog/'.$blog->pic)}}" style="height: 166px;">
		   @endif
                                    <!-- <img class="rounded" src="https://dummyimage.com/300x152/ccc/fff"> -->
                                    <h4 class="mt-3">{{@$blog->title}}</h4>
                                    <!-- <span>Posted 1 day ago</span> -->
                                </div>
                            </div>
                        </div>
                        </a>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
