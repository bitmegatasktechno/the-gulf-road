<section class="intro-section mb-5">
        <div class="container">
            <div class="introduction-block">
                <div class="row">
                    <div class="col-md-5 pr-0">
                        <div class="bg-white rounded intro-wrapper p-4">
                            <h4>Introducing</h4>
                            <h2>The Gulf Road <i><span>Boutique</span></i></h2>
                            <p>A selection of finest hotel apartments, beach side properties and prime location properties.</p>
                            <a href="{{url(app()->getLocale().'/'.$type.'-property/'.strtolower($link).'?country='.$link.'&is_premium=1')}}"><button class="red-bordered mt-3 rounded" autocomplete="off">EXPLORE <i class="fa fa-angle-right ml-2"></i></button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    

