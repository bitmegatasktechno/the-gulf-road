<style>
i.fa.fa-heart{
    padding-top: 2px !important;
    padding-left: 310px !important;
}
</style>
@php
  $lang=Request::segment(1);
$loc=Request::segment(3);
if($loc == 'ksa' || $loc == 'uae' )
{
   $loc21 = strtoupper($loc);
}else 
{
    $loc21 = ucfirst($loc);
}
 $type=Request::segment(4);
$modules = \App\Models\Module::where('location', $loc21)->get();
if($loc!='all'){
  $link=$loc;
}else{
  $link='all';
}

 @endphp
         
    @if(count($allProperties)>0)
        @foreach($modules as $module) 
               
            @if($module->type=='rent')
<section class="handpicked-home" style="padding: 71px 0;">
        <div class="container">
            <div class="row side-btn">
                <div class="col-md-12">
                <h3 style="margin-left: 16px;">Handpicked Homes</h3>
                    <p style="margin-left: 16px;">Promoted homes you might  be interested in</p>
                    <div class="owl-slider">
                        <div id="carouselone" class="owl-carousel">
                       
                        @foreach($allProperties as $allProperty)
                            @php
                            $totaluser = \App\Models\Review::where('is_approved',1)->where('type_id',$allProperty->_id)->count();
                            $ratinggiven1=\App\Models\Review::where('is_approved',1)->where('type_id',$allProperty->_id)->sum('average');
                            $totalrating=@$totaluser*5;
                            if($totalrating == 0){
                                $totalrating=1;
                            }
                            $avg=number_format(@$ratinggiven1/@$totalrating*5);

                            $is_prop_premium = 1;

                            $is_premium = \App\Models\Subscription::where('_id',$allProperty->is_premium)->where('type','premium')->first();
                            
                            if($is_premium && isset($is_premium->sub_plan) ){
                                $allProperty->available_from = str_replace('/', '-',$allProperty->available_from);

                                $lastDate = strtotime($allProperty->available_from." +".$is_premium->sub_plan[0]['validity']."days");

                                if($lastDate < strtotime('Now') ){
                                    $is_prop_premium = 0;
                                }
                            }else{
                                $is_prop_premium = 0;
                            }

                            @endphp
                            <div class="item">
                                <div class="col-md-12">
                                    <div class="property-card bg-white rounded" style="height: 459px;">
                                        <span class="populated-home"><img src="{{asset('img/ic_promoted.png')}}" >&nbsp;&nbsp;Promoted</span>
                                    @if($is_prop_premium)
                                    <button class="red-btn red-btn-new"><img src="{{asset('img/Premium_Tick.png')}}"  > &nbsp; Premium</button>
                                    @endif
                                    @if(isset(Auth::user()->id))
                                        @php
                                        $fav = \App\Models\Favourate::where('propert_id',$allProperty->_id)->where('user_id',Auth::user()->id)->first();
                                        @endphp
                                    @endif
                                    @if(@$fav)
                                        <i class="fa fa-heart" onClick="makefav('{{$allProperty->_id}}')"></i>
                                    @else
                                        <i class="fa fa-heart-o" onClick="makefav('{{$allProperty->_id}}')"></i>
                                    @endif
                                    <a href="{{url(app()->getLocale().'/property/'.$link.'/rent/'.$allProperty->_id)}}">
                                    @if(!isset($allProperty->files[0]) || $allProperty->files[0]=='')
                                    <img class="img-fluid property-img" src="{{asset('/img/house2.png')}}">
                                    @else
                                    @if(file_exists('uploads/properties/'.$allProperty->files[0]))
                                    <img class="img-fluid property-img" src="{{url('uploads/properties/'.$allProperty->files[0])}}">
                                    @else
                                    <img class="img-fluid property-img" src="{{asset('img/house2.png')}}">
                                    @endif
                                    @endif
                                        <!-- <img class="img-fluid property-img" src="https://dummyimage.com/370x205/ccc/fff"> -->
                                        <div class="pl-3 pr-3">
                                            <h5>{{@$allProperty->house_type}}</h5>
                                            <h2>{{\Str::limit(@$allProperty->property_title, 30, '...')}}</h2>
                                            <p>{{\Str::limit(ucfirst($allProperty->location_name), 60)}}, {{$allProperty->country}}</p>
                                            <h6>
                                                @if($type=='buy')
                                                {{$allProperty->price ? number_format($allProperty->price) : 0}} {{$allProperty->price_currency ?? 'N/A'}}  / yr
                                                @elseif($type='rent')
                                                {{$allProperty->rent ? $allProperty->rent : '0'}} {{$allProperty->rent_currency ? strtoupper($allProperty->rent_currency) : 'N/A'}} / {{$allProperty->rent_frequency ? $allProperty->rent_frequency : 'N/A'}}
                                                @endif
                                            </h6>
                                            <ul class="list-inline">
                                                @if(@$avg==1)
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                @elseif(@$avg==2)
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                @elseif(@$avg==3)
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                @elseif(@$avg==4)
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                @elseif(@$avg==5)
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                <li><img src="{{asset('img/ic_rating_filled.png')}}"></li>
                                                @endif
                                @if(@$avg == 0)
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                <li><img style="background: #dedede;" src="{{asset('img/white_star.png')}}"></li>
                                
                                @endif
                                                <li>{{@$avg}}</li>
                                            </ul>
                                            <ul class="list-inline red-list mb-4">
                                                <li class="mr-3"><img class="mr-1" src="{{asset('img/red-bed.png')}}"> {{@$allProperty->no_of_bedrooms}}</li>
                                                <li class="mr-3"><img class="mr-1" src="{{asset('img/red-tub.png')}}"> {{@$allProperty->no_of_bathrooms}}</li>
                                                <li class="mr-3"><img class="mr-1" src="{{asset('img/red-car.png')}}"> {{@$allProperty->no_of_open_parking}}</li>
                                            </ul>
                                        </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                         
                        
                            @endforeach
                    </div>
                    </div>
                </div>

                 

                @if($type=='buy')
                <div class="col-md-12 show-more text-center">
                <a href="{{url(app()->getLocale().'/buy-property/'.$link)}}"><button class="red-bordered mt-5">SHOW MORE <i class="fa fa-angle-right"></i></button></a>
                </div>
                @else
                <div class="col-md-12 show-more text-center">
                <a href="{{url(app()->getLocale().'/rent-property/'.$link)}}"><button class="red-bordered mt-5">SHOW MORE <i class="fa fa-angle-right"></i></button></a>
                </div>
                @endif
                <div class="col-md-12 mt-4 mb-4">
                    <hr>
                </div>
                <!-- <div class="offset-md-1 col-md-5">
                <a href="#myModal" class="" data-toggle="modal"> <button class="normal-btn full-width rounded"><i class="mr-4"><img src="{{asset('img/red-360.png')}}"></i>Properties Virtual Tours for Rent</button></a>
                </div>
                <div class="col-md-5">
                <a href="#myModal1" class="" data-toggle="modal"> <button class="normal-btn full-width rounded"><i class="mr-4"><img src="{{asset('img/red-360.png')}}"></i>Properties Virtual Tours for Sale</button></a>
                </div> -->

                <div class="offset-md-1 col-md-5">
                <a href="{{url(app()->getLocale().'/rent-property/'.$link)}}"> <button class="normal-btn full-width rounded"><i class="mr-4"><img src="{{asset('img/red-360.png')}}"></i>Properties Virtual Tours for Rent</button></a>
                </div>
                <div class="col-md-5">
                <a href="{{url(app()->getLocale().'/buy-property/'.$link)}}"> <button class="normal-btn full-width rounded"><i class="mr-4"><img src="{{asset('img/red-360.png')}}"></i>Properties Virtual Tours for Sale</button></a>
                </div>

            </div>
        </div>
    </section>

    @endif
    @endforeach
    @endif

