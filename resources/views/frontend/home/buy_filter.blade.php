@php
$loc=Request::segment(3);
    if($loc!='all'){
    $link=$loc;
    }else{
    $link='all';
    }
@endphp
<section class="bannar-section buy">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="black-background pl-4 pr-4 pt-3 pb-3 mt-5 rounded">
                    <h3 class="white-color mb-3 mt-1 home-filter-title title_buy">Find Real Estate Agents</h3>
                    <h3 class="white-color mb-3 mt-1 home-filter-title title_rent" style="display: none;">Search Properties For Rent</h3>
                    <h3 class="white-color mb-3 mt-1 home-filter-title title_swap" style="display: none;">Find A Home To Swap</h3>
                    <h3 class="white-color mb-3 mt-1 home-filter-title title_cospace" style="display: none;">Find a Co-Workspace for your Team</h3>
                    <h3 class="white-color mb-3 mt-1 home-filter-title title_agents" style="display: none;">Find Real Estate Agents</h3>
                    <h3 class="white-color mb-3 mt-1 home-filter-title title_buddies" style="display: none;">Find Buddies for your desired neighbourhood</h3>
                    <h3 class="white-color mb-3 mt-1 home-filter-title title_travel" style="display: none;">Find the Perfect Vacations with us</h3>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item changeBackgroundImage" data-name="buy">
                            <a class="nav-link active white-color" href="{{url(app()->getLocale().'/landing/'.$link.'/buy')}}" ><i class="mr-3"><img src="{{asset('img/home.png')}}"></i>Buy</a>
                        </li>
                        <li class="nav-item changeBackgroundImage" data-name="rent">
                            <a class="nav-link white-color" href="{{url(app()->getLocale().'/landing/'.$link.'/rent')}}" ><i class="mr-3"><img src="{{asset('img/buildings.png')}}"></i>Rent</a>
                        </li>
                        <li class="nav-item changeBackgroundImage" data-name="swap">
                            <a class="nav-link white-color" href="{{url(app()->getLocale().'/landing/'.$link.'/swap')}}"><i class="mr-3"><img src="{{asset('img/swap.png')}}"></i>Swap</a>
                        </li>
                        <li class="nav-item changeBackgroundImage" data-name="cospace">
                            <a class="nav-link white-color" href="{{url(app()->getLocale().'/landing/'.$link.'/cospace')}}"><i class="mr-3"><img src="{{asset('img/flat.png')}}"></i>co-space</a>
                        </li>
                        <li class="nav-item changeBackgroundImage" data-name="agents">
                            <a class="nav-link  white-color" href="{{url(app()->getLocale().'/landing/'.$link.'/agents')}}" ><i class="mr-3"><img src="{{asset('img/seller.png')}}"></i>Agents</a>
                        </li>
                        <li class="nav-item changeBackgroundImage" data-name="buddies">
                            <a class="nav-link white-color" href="{{url(app()->getLocale().'/landing/'.$link.'/buddies')}}" ><i class="mr-3"><img src="{{asset('img/buddies.png')}}"></i>Buddies</a>
                        </li>
                        <!-- <li class="nav-item changeBackgroundImage" data-name="travel">
                            <a class="nav-link white-color" href="#travel" role="tab" data-toggle="tab"><i class="mr-3"><img src="{{asset('img/departures.png')}}"></i>Travel</a>
                        </li> -->
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <!-- Buy Tabs -->
                        <div role="tabpanel" class="tab-pane active buy-tab" id="buy">
                            <form method="get" name="buy_form" id="buy_form" action="{{url(app()->getLocale().'/buy-property/'.$link)}}">
                                <div class="row">
                                    <div class="col-md-2 pr-0">
                                        <select name="country" id="buy_country">
                                            <option value="" selected disabled hidden>Country</option>
                                            @foreach(getAllLocations() as $row)
                                                <option value="{{strtolower($row->name)}}" @if(strtolower($defaultCountry)==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-8 pr-0 pl-0">
                                        <div class="search-box">
                                            <input class="full-width" type="text" placeholder="“Search properties by location”" name="location">
                                            <img class="search-icon" src="{{asset('img/search.png')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-2 pl-0">
                                        <button class="full-width buy_form_btn" type="submit">Search</button>
                                    </div>
                                    <div class="col-md-3 mt-3 pr-0">
                                        <div class="inputbox select inputbox-label-inside">
                                            <select name="house_type" id="buy_house">
                                                <option value="" selected disabled hidden>Select</option>
                                                @foreach(getAllHouseTypes() as $row)
                                                    <option value="{{$row->name}}">{{ucfirst($row->name)}}</option>
                                                @endforeach
                                            </select>
                                            <label for="buy_house">PROPERTY TYPE</label>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2 mt-3 pr-0">
                                        <div class="inputbox select inputbox-label-inside">
                                            <select name="bedrooms" id="buy_beds" >
                                                <option value="" selected disabled hidden>Beds</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                            </select>
                                            <label for="buy_beds">BEDS</label>
                                        </div>
                                    </div>
                                </div>
                             </form>
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="mt-3 mb-0 blue-color">
                                        <a class="white-color" data-toggle="collapse" href="#collapseBuyExample" role="button" aria-expanded="false" aria-controls="collapseBuyExample">
                                            More Options 
                                            <i class="fa fa-angle-down ml-3 mr-2" area-hidden="true"></i>
                                        </a> 
                                        <span>| </span>
                                        <span class="reset_buy_filter" style="cursor: pointer;">Reset Search</span>
                                    </p>
                                </div>
                                <div class="col-md-6 text-right">
                                    <div class="checkbox-block">
                                        <div class="form-group mb-2 mt-3">
                                            <input type="checkbox" name="nearby" id="sale_nearby">
                                            <label class="white-color" for="sale_nearby">Nearby Surroundings</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="more-filters">
                                    <div class="container">
                                        <div class="row">
                                            <div class="collapse full-width" id="collapseBuyExample">
                                                <div class="bg-white p-4 full-width">
                                                    <form method="get" name="buy_advance_form" id="buy_advance_form" action="{{url(app()->getLocale().'/buy-property/'.$link)}}">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <select name="country">
                                                                    <option value="">Select Country</option>
                                                                    @foreach(getAllLocations() as $row)
                                                                        <option value="{{strtolower($row->name)}}" @if(strtolower($defaultCountry)==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select name="house_type">
                                                                    <option value="">Select Property Type</option>
                                                                    @foreach(getAllHouseTypes() as $row)
                                                                        <option value="{{$row->name}}">{{ucfirst($row->name)}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select name="furnishing_type">
                                                                    <option value="">Select Furnishing Type</option>
                                                                    
                                                                    @foreach(getFurnishingTypes() as $key=>$value)
                                                                        <option value="{{$key}}">{{ucfirst($value)}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select name="amenties">
                                                                    <option value="">Select Amenieties</option>
                                                                    
                                                                    @foreach(getAllAmenities() as $row)
                                                                        <option value="{{$row->name}}">{{ucfirst($row->name)}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            
                                                            <div class="col-md-12 pt-4 pb-4">
                                                                <hr>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <h6>Listed By</h6>
                                                                <div class="checkbox-block">
                                                                    <div class="form-group mb-2">
                                                                        <input type="checkbox" id="buy_user_list" name="listed_by" value="user">
                                                                        <label for="buy_user_list">USER</label>
                                                                    </div>
                                                                    <div class="form-group mb-2">
                                                                       <input type="checkbox" id="buy_agent_list" name="listed_by" value="agent">
                                                                        <label for="buy_agent_list">AGENT</label>
                                                                    </div>
                                                                    <div class="form-group mb-2">
                                                                        <input type="checkbox" id="buy_buddy_list" name="listed_by" value="buddy">
                                                                        <label for="buy_buddy_list">BUDDY</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <h6>Keyword Search</h6>
                                                                <input class="full-width" type="text" name="property_title" placeholder="Eg.Pool,Security,Ref ID Number ">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <h6>Bathrooms</h6>
                                                                <select name="bathrooms">
                                                                    <option value="">Any Bathrooms</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <h6>Size</h6>
                                                                <input class="full-width mb-3" type="text" name="min_size" placeholder="Min">
                                                                <input class="full-width" type="text" name="max_size" placeholder="Max">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <h6>Real Estate Agent</h6>
                                                                <input class="full-width" type="text" name="agent_name" placeholder="Eg DAMAC properties">
                                                            </div>

                                                            <div class="col-md-12 text-right">
                                                                <button type="button" class="red-bordered mr-3 rounded reset_buy_advance_filter">Reset</button>
                                                                <button type="submit" class="red-btn rounded advance_buy_filter">Search</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Rent Tab -->
                        <div role="tabpanel" class="tab-pane fade buy-tab rent-tab" id="rent">
                            <form method="get" name="rent_form" id="rent_form" action="{{url(app()->getLocale().'/rent-property')}}">
                                <div class="row">
                                    <div class="col-md-2 pr-0">
                                        <select name="country" id="rent_country">
                                            <option value="" selected disabled hidden>Country</option>
                                            @foreach(getAllLocations() as $row)
                                                <option value="{{$row->name}}" @if(strtolower($defaultCountry)==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-8 pr-0 pl-0">
                                        <div class="search-box">
                                            <input class="full-width" type="text" placeholder="“Search properties by location”" name="location">
                                            <img class="search-icon" src="{{asset('img/search.png')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-2 pl-0">
                                        <button class="full-width rent_form_btn" type="submit">Search</button>
                                    </div>

                                    <div class="col-md-3 mt-3 pr-0">
                                        <div class="inputbox select inputbox-label-inside">
                                            <select name="house_type" id="rent_house">
                                                <option value="" selected disabled hidden>Select</option>
                                                @foreach(getAllHouseTypes() as $row)
                                                    <option value="{{$row->name}}">{{ucfirst($row->name)}}</option>
                                                @endforeach
                                            </select>
                                            <label for="rent_house">PROPERTY TYPE</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2 mt-3 pr-0">
                                        <div class="inputbox select inputbox-label-inside">
                                            <select name="rent_frequenecy" id="rent_frequenecy">
                                                <option value="" selected disabled hidden>Any</option>
                                                
                                                @foreach(getAllRentFrequency() as $key=>$value)
                                                    <option value="{{$key}}">{{ucfirst($value)}}</option>
                                                @endforeach
                                            </select>
                                            <label for="rent_frequenecy">RENT FREQUENCY</label>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-5 mt-3">
                                        <div class="inputbox select inputbox-label-inside">
                                            <select name="country" id="country" required>
                                                <option value="" selected disabled hidden>Select</option>
                                                <option value="DE">Yearly</option>
                                                <option value="AT">yearly</option>
                                                <option value="CH">yearly</option>
                                                <option value="SE">yearly</option>
                                            </select>
                                            <label for="country">Price</label>
                                        </div>
                                    </div> -->
                                    
                                    <!-- <div class="col-md-2 mt-3 pl-0"> -->
                                    <div class="col-md-2 mt-3 pr-0">
                                        <div class="inputbox select inputbox-label-inside">
                                            <select name="bedrooms" id="rent_beds">
                                                <option value="" selected disabled hidden>Beds</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                            </select>
                                            <label for="rent_beds">BEDS</label>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="mt-3 mb-0 blue-color">
                                        <a class="white-color" data-toggle="collapse" href="#collapseRentExample" role="button" aria-expanded="false" aria-controls="collapseRentExample">
                                            More Options 
                                            <i class="fa fa-angle-down ml-3 mr-2" area-hidden="true"></i>
                                        </a> 
                                        <span>| </span>
                                        <span class="reset_rent_filter" style="cursor: pointer;">Reset Search</span>
                                    </p>
                                </div>
                                <div class="col-md-6 text-right">
                                    <div class="checkbox-block">
                                        <div class="form-group mb-2 mt-3">
                                            <input type="checkbox" name="nearby" id="rent_nearby">
                                            <label class="white-color" for="rent_nearby">Nearby Surroundings</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="more-filters">
                                    <div class="container">
                                        <div class="row">
                                            
                                            <div class="collapse full-width" id="collapseRentExample">
                                                <div class="bg-white p-4 full-width">
                                                    <form method="get" name="rent_advance_form" id="rent_advance_form" action="{{url(app()->getLocale().'/rent-property')}}">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <select name="country">
                                                                    <option value="">Select Country</option>
                                                                    @foreach(getAllLocations() as $row)
                                                                        <option value="{{$row->name}}" @if(strtolower($defaultCountry)==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select name="house_type">
                                                                    <option value="">Select Property Type</option>
                                                                    @foreach(getAllHouseTypes() as $row)
                                                                        <option value="{{$row->name}}">{{ucfirst($row->name)}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select name="furnishing_type">
                                                                    <option value="">Select Furnishing Type</option>
                                                                    @foreach(getFurnishingTypes() as $key=>$value)
                                                                        <option value="{{$key}}">{{ucfirst($value)}}</option>
                                                                    @endforeach
                                                                    
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <select name="amenties">
                                                                    <option value="">Select Amenieties</option>
                                                                    
                                                                    @foreach(getAllAmenities() as $row)
                                                                        <option value="{{$row->name}}">{{ucfirst($row->name)}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            
                                                            <div class="col-md-12 pt-4 pb-4">
                                                                <hr>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <h6>Listed By</h6>
                                                                <div class="checkbox-block">
                                                                    <div class="form-group mb-2">
                                                                        <input type="checkbox" id="rent_user_list" name="listed_by" value="user">
                                                                        <label for="rent_user_list">USER</label>
                                                                    </div>
                                                                    <div class="form-group mb-2">
                                                                       <input type="checkbox" id="rent_agent_list" name="listed_by" value="agent">
                                                                        <label for="rent_agent_list">AGENT</label>
                                                                    </div>
                                                                    <div class="form-group mb-2">
                                                                        <input type="checkbox" id="rent_buddy_list" name="listed_by" value="buddy">
                                                                        <label for="rent_buddy_list">BUDDY</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <h6>Keyword Search</h6>
                                                                <input class="full-width" type="text" name="property_title" placeholder="Eg.Pool,Security,Ref ID Number ">
                                                            </div>
                                                            <div class="col-md-3">
                                                                <h6>Bathrooms</h6>
                                                                <select name="bathrooms">
                                                                    <option value="">Any Bathrooms</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                                    <option value="6">6</option>
                                                                    <option value="7">7</option>
                                                                    <option value="8">8</option>
                                                                    <option value="9">9</option>
                                                                    <option value="10">10</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <h6>Size</h6>
                                                                <input class="full-width mb-3" type="text" name="min_size" placeholder="Min">
                                                                <input class="full-width" type="text" name="max_size" placeholder="Max">
                                                            </div>
                                                            <div class="col-md-3">
                                                            	<h6>Rent Frequency</h6>
                                                                <select name="rent_frequenecy">
					                                                <option value="" selected disabled hidden>Any</option>
					                                                
					                                                @foreach(getAllRentFrequency() as $key=>$value)
					                                                    <option value="{{$key}}">{{ucfirst($value)}}</option>
					                                                @endforeach
					                                            </select>
                                                            </div>

                                                            <div class="col-md-12 text-right">
                                                                <button type="button" class="red-bordered mr-3 rounded reset_rent_advance_filter">Reset</button>
                                                                <button type="submit" class="red-btn rounded advance_rent_filter">Search</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                        <!-- Swap TAb -->
                        <div role="tabpanel" class="tab-pane fade buy-tab swap home_swap_tab" id="swap">
                            <form method="get" name="swap_form" id="swap_form" action="{{url(app()->getLocale().'/swap')}}">
                                <div class="row">
                                    <div class="col-md-2 pr-0">
                                        <select name="country" id="swap_country">
                                            <option value="" selected disabled hidden>Country</option>
                                            @foreach(getAllLocations() as $row)
                                                <option value="{{$row->name}}" @if(strtolower($defaultCountry)==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-8 pr-0 pl-0">
                                        <div class="search-box">
                                            <input class="full-width" type="text" placeholder="“Search swap properties by location”" name="location">
                                            <img class="search-icon" src="{{asset('img/search.png')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-2 pl-0">
                                        <button class="full-width buy_form_btn" type="submit">Search</button>
                                    </div>
                                    <div class="col-md-3 mt-3 pr-0">
                                        <div class="inputbox select inputbox-label-inside">
                                            <select name="house_type" id="swap_house">
                                                <option value="" selected disabled hidden>Select</option>
                                                @foreach(getAllHouseTypes() as $row)
                                                    <option value="{{strtolower($row->name)}}">{{ucfirst($row->name)}}</option>
                                                @endforeach
                                            </select>
                                            <label for="swap_house">PROPERTY TYPE</label>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2 mt-3 pr-0">
                                        <div class="inputbox select inputbox-label-inside">
                                            <select name="capacity" id="swap_capacity" >
                                                <option value="" selected disabled hidden>Guest</option>
                                                @foreach(range(1, 20) as $row)
				                                    <option value="{{$row}}">{{$row}}</option>
				                                @endforeach
                                            </select>
                                            <label for="swap_capacity">Guest</label>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-3 mt-3 pr-0">
                                        <div class="inputbox select inputbox-label-inside" style="position: relative;">
                                            <div id="reportrange">
                                                <i class="fa fa-calendar"></i>&nbsp;
                                                <span></span> <i class="fa fa-caret-down"></i>
                                            </div>
                                            <input type="hidden" name="start_date" id="swap_start_date" value="">
                                            <input type="hidden" name="end_date" id="swap_end_date" value="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="mt-3 mb-0 blue-color"> 
                                            <span class="reset_swap_filter" style="cursor: pointer;">Reset Search</span>
                                        </p>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div class="checkbox-block">
                                            <div class="form-group mb-2 mt-3">
                                                <input type="checkbox" name="nearby" id="swap_nearby">
                                                <label class="white-color" for="swap_nearby">
                                                Nearby Surroundings
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <!-- Co-space Tab -->
                        <div role="tabpanel" class="tab-pane fade buy-tab space" id="co-space">
                            <form method="get" name="coworkspace_form" id="coworkspace_form" action="{{url(app()->getLocale().'/coworkspace')}}">
                                <div class="row">
                                    <div class="col-md-2 pr-0">
                                        <select name="country">
                                            <option value="" selected disabled hidden>Country</option>
                                            @foreach(getAllLocations() as $row)
                                                <option value="{{$row->name}}" @if(strtolower($defaultCountry)==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-8 pr-0 pl-0">
                                        <div class="search-box">
                                            <input class="full-width" type="text" placeholder="“Search coworkspace properties by location”" name="location">
                                            <img class="search-icon" src="{{asset('img/search.png')}}">
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2 pl-0">
                                        <button class="red-btn full-width" type="submit">Search</button>
                                    </div>

                                    <div class="col-md-2 mt-3 pr-0">
                                        <div class="inputbox select inputbox-label-inside">
                                            <select name="type" id="cospace_type">
                                                <option value="">Any</option>
                                                <option value="open">Open Workspace</option>
                                                <option value="dedicated">Dedicated Workspace</option>
                                                <option value="private">Private Workspace</option>
                                            </select>
                                            <label for="cospace_type">COWORKSPACE TYPE</label>
                                        </div>
                                    </div>
                                    <div class="col-md-2 mt-3 pr-0">
                                       <div class="inputbox select inputbox-label-inside">
                                            <select name="people" id="cospace_peopele">
                                                <option value="">Any</option>
                                                @foreach(range(1, 100) as $row)
                                                    <option value="{{$row}}">{{$row}} People</option>
                                                @endforeach
                                            </select>
                                            <label for="cospace_peopele">NO OF PEOPLE</label>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p class="mt-3 mb-0 blue-color"> 
                                            <span class="reset_cospace_filter" style="cursor: pointer;">Reset Search</span>
                                        </p>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <div class="checkbox-block">
                                            <div class="form-group mb-2 mt-3">
                                                <input type="checkbox" name="nearby" id="cospace_nearby">
                                                <label class="white-color" for="cospace_nearby">
                                                Nearby Surroundings</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- Agents Tabs -->
                        <div role="tabpanel" class="tab-pane fade buy-tab agents" id="agents">
                            <form method="get" name="agents_form" id="agents_form" action="{{url(app()->getLocale().'/agents')}}">

                                <div class="row">
                                    <div class="col-md-2 pr-0">
                                        <select name="country">
                                            <option value="" selected disabled hidden>Country</option>
                                            @foreach(getAllLocations() as $row)
                                                <option value="{{$row->name}}" @if(strtolower($defaultCountry)==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-8 pr-0 pl-0">
                                        <div class="search-box">
                                            <input class="full-width" type="text" placeholder="Find agents by location" name="location">
                                            <img class="search-icon" src="{{asset('img/search.png')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-2 pl-0">
                                        <button class="full-width">Search</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        &nbsp;
                                    </div>
                                    <div class="col-md-6">
                                        &nbsp;
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- Buddies Tab -->
                        <div role="tabpanel" class="tab-pane fade buy-tab buddies" id="buddies">
                            <form method="get" name="buddy_form" id="buddy_form" action="{{url(app()->getLocale().'/buddies')}}">

                                <div class="row">
                                    <div class="col-md-2 pr-0">
                                        <select name="country">
                                            <option value="" selected disabled hidden>Country</option>
                                           @foreach(getAllLocations() as $row)
                                                <option value="{{$row->name}}" @if(strtolower($defaultCountry)==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-8 pr-0 pl-0">
                                        <div class="search-box">
                                            <input class="full-width" type="text" placeholder="Find buddies by location" name="location">
                                            <img class="search-icon" src="{{asset('img/search.png')}}">
                                        </div>
                                    </div>
                                    <div class="col-md-2 pl-0">
                                        <button class="full-width">Search</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        &nbsp;
                                    </div>
                                    <div class="col-md-6">
                                        &nbsp;
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- Travel Tab -->
                        <div role="tabpanel" class="tab-pane fade buy-tab travel" id="travel">
                            <div class="row">
                                <div class="col-md-2 pr-0">
                                    <select name="country">
                                        <option value="" selected disabled hidden>Country</option>
                                       @foreach(getAllLocations() as $row)
                                            <option value="{{$row->name}}" @if(strtolower($defaultCountry)==strtolower($row->name)) selected @endif>{{ucfirst($row->name)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-5 pr-0 pl-0">
                                    <div class="search-box">
                                        <input class="full-width" type="text" placeholder="Where Would you like to Travel?" name="location">
                                        <img class="search-icon" src="{{asset('img/search.png')}}">
                                    </div>
                                </div>
                                <div class="col-md-3 pr-0 ">
                                    <div class="search-box no-line">
                                        <input class="full-width" type="" placeholder="Travel Duration" name="">
                                        <img class="search-icon" src="{{asset('img/calendar.png')}}">
                                    </div>
                                </div>
                                <div class="col-md-2 pl-0">
                                    <button class="full-width">Search</button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    &nbsp;
                                </div>
                                <div class="col-md-6">
                                    &nbsp;
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
