<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">

            <div class="portlet-title">
                <div class="caption">Addresses Details</div>
            </div>

            <div class="portlet-body">
                <div class="col-md-12">
                    <table class="table table-bordered table-striped">
                        <tbody>
                        <tr>
                            <td>
                                <strong>Current Address</strong>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
	                        <tr>
	                            <td>
	                                <strong>Location Name:</strong>
	                            </td>
	                            <td>
	                                {{ $user->addresses->current_location ?? 'N/A'}}
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>
	                                <strong>Unit/Floor No:</strong>
	                            </td>
	                            <td>
	                                {{ $user->addresses->current_unit_number ?? 'N/A'}}
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>
	                                <strong>Address Line 1:</strong>
	                            </td>
	                            <td>
	                                {{ $user->addresses->current_address_1 ?? 'N/A'}}
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>
	                                <strong>Address Line 2:</strong>
	                            </td>
	                            <td>
	                                {{ $user->addresses->current_address_2 ?? 'N/A'}}
	                            </td>
	                        </tr>

	                        
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
	                        <tr>
	                            <td>
	                                <strong>Landmark:</strong>
	                            </td>
	                            <td>
	                                {{ $user->addresses->current_landmark ?? 'N/A'}}
	                            </td>
	                        </tr>

	                        <tr>
	                            <td>
	                                <strong>Town/City:</strong>
	                            </td>
	                            <td>
	                                {{ $user->addresses->current_city ?? 'N/A'}}
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>
	                                <strong>State:</strong>
	                            </td>
	                            <td>
	                                {{ $user->addresses->current_state ?? 'N/A'}}
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>
	                                <strong>Zip Code:</strong>
	                            </td>
	                            <td>
	                                {{ $user->addresses->current_zipcode ?? 'N/A'}}
	                            </td>
	                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <table class="table table-bordered table-striped">
                        <tbody>
                        <tr>
                            <td>
                                <strong>Native Address</strong>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
	                        <tr>
	                            <td>
	                                <strong>Location Name:</strong>
	                            </td>
	                            <td>
	                                {{ $user->addresses->native_location ?? 'N/A'}}
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>
	                                <strong>Unit/Floor No:</strong>
	                            </td>
	                            <td>
	                                {{ $user->addresses->native_unit_number ?? 'N/A'}}
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>
	                                <strong>Address Line 1:</strong>
	                            </td>
	                            <td>
	                                {{ $user->addresses->native_address_1 ?? 'N/A'}}
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>
	                                <strong>Address Line 2:</strong>
	                            </td>
	                            <td>
	                                {{ $user->addresses->native_address_2 ?? 'N/A'}}
	                            </td>
	                        </tr>

	                        
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
	                        <tr>
	                            <td>
	                                <strong>Landmark:</strong>
	                            </td>
	                            <td>
	                                {{ $user->addresses->native_landmark ?? 'N/A'}}
	                            </td>
	                        </tr>

	                        <tr>
	                            <td>
	                                <strong>Town/City:</strong>
	                            </td>
	                            <td>
	                                {{ $user->addresses->native_city ?? 'N/A'}}
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>
	                                <strong>State:</strong>
	                            </td>
	                            <td>
	                                {{ $user->addresses->native_state ?? 'N/A'}}
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>
	                                <strong>Zip Code:</strong>
	                            </td>
	                            <td>
	                                {{ $user->addresses->native_zipcode ?? 'N/A'}}
	                            </td>
	                        </tr>
                        </tbody>
                    </table>
                </div>
                
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>

        </div>

    </div>
</div>
