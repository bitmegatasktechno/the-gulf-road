<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue" id="stage_show">
            <div class="portlet-title">
                <div class="caption ">
                    Status
                </div>
            </div>
            <div class="portlet-body stage-col-main">
                <div class="col-md-3">
                    <div class="general-item-list">
                        <div class="item">
                            <div class="item-head">
                                <div class="item-details">
                                    Default Logged-In Role
                                </div>
                            </div>
                            <div class="item-body">
                                <span class="badge badge-success">{{$user->defaultLoginRole ?? 'N/A'}}</span></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="general-item-list">
                        <div class="item">
                            <div class="item-head">
                                <div class="item-details">
                                    Available Roles
                                </div>
                            </div>
                            <div class="item-body">
                                @if(isset($user->role))
                                    @foreach($user->role as $key=>$value)
                                        <span class="badge badge-success">
                                            {{$value}}
                                        </span>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="portlet box blue" id="stage_show">
            <div class="portlet-title">
                <div class="caption ">
                    Top List
                </div>
            </div>
            <div class="portlet-body stage-col-main">
                <div class="col-md-3">
                    <div class="general-item-list">
                        <div class="item">
                            <div class="item-head">
                                <div class="item-details">
                                    Current Status
                                </div>
                            </div>
                            <div class="item-body">
                                @if(isset($user->top_list))
                                    @foreach($user->top_list as $key=>$value)
                                        <span data-type="{{$value}}" class="badge badge-success removeTop">
                                            TOP {{$value}} <i class="fa fa-times" aria-hidden="true"></i>

                                        </span>
                                    @endforeach
                                @endif    
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="general-item-list">
                        <div class="item">
                            <div class="item-head">
                                <div class="item-details">
                                    Available Status
                                </div>
                            </div>
                            <div class="item-body">
                                @if(isset($user->role))
                                    @foreach($user->role as $key=>$value)
                                        <span data-type="{{$value}}" class="badge badge-success makeTop">
                                            TOP {{$value}}
                                        </span>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(".makeTop").on('click', function(){
        $.ajax({
          url: "/admin/user/updateTop/add/{{$user->_id}}/"+$(this).data('type'),
        }).done(function() {
          location.reload();
        });
    });
    $(".removeTop").on('click', function(){
        $.ajax({
          url: "/admin/user/updateTop/remove/{{$user->_id}}/"+$(this).data('type'),
        }).done(function() {
          location.reload();
        });
    });
</script>