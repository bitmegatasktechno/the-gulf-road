<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">

            <div class="portlet-title">
                <div class="caption">Additional Informations</div>
            </div>

            <div class="portlet-body">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
                        <tr>
                            <td>
                                <strong>Whatsapp Number:</strong>
                            </td>
                            <td>
                                {{ @$user->profiles->whatsapp_number ? @$user->profiles->whatsapp_number : 'N/A'}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Residence ID Number:</strong>
                            </td>
                            <td>
                                {{ $user->profiles->residence_id_number ?? 'N/A'}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Passport Number:</strong>
                            </td>
                            <td>
                                {{ $user->profiles->passport_number ?? 'N/A'}}
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <strong>Driving Licence Available?</strong>
                            </td>
                            <td>
                                {{ @$user->profiles->driving_licence ?? 'N/A'}}
                            </td>
                        </tr>
                        @if(@$user->profiles->driving_licence=='yes')
	                        <tr>
	                            <td>
	                                <strong>Licence Number:</strong>
	                            </td>
	                            <td>
	                                {{ $user->profiles->licence_number ?? 'N/A'}}
	                            </td>
	                        </tr>
                        @endif
                        
                        
                        
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
                        <tr>
                            <td>
                                <strong>Marital Status:</strong>
                            </td>
                            <td>
                                {{ $user->profiles->marriage_status ?? 'N/A'}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Preffered Currency :</strong>
                            </td>
                            <td>
                                {{ $user->profiles->preffered_currency ?? 'N/A'}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Preffered Language :</strong>
                            </td>
                            <td>
                                {{ $user->profiles->preffered_language ?? 'N/A'}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Working In Region Since?</strong>
                            </td>
                            <td>
                                {{ $user->profiles->working_since ?? 'N/A'}}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>

        </div>

    </div>
</div>
