<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">

            <div class="portlet-title">
                <div class="caption">Qualifications & Hobbies</div>
            </div>

            <div class="portlet-body">
                <div class="col-md-12">
                    <table class="table table-bordered table-striped">
                        <tbody>
	                        <tr>
	                            <td >
	                                <strong>Qualifications</strong>
	                            </td>
                        	</tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <table class="table table-bordered">
					    <thead>
					      	<tr>
						        <th class="col-md-6">Course/Degree Name</th>
						        <th class="col-md-6">University</th>
					      	</tr>
					    </thead>
					    <tbody>
                            @if(isset($$user->qualifications->education))
					      	@foreach($user->qualifications->education as $row)
                        		<tr>
		                            <td>
		                                <strong>{{$row['course_name' ?? 'N/A']}}</strong>
		                            </td>
		                            <td>
		                                <strong>{{$row['university_name' ?? 'N/A']}}</strong>
		                            </td>
		                        </tr>
                        	@endforeach
                            @endif
					    </tbody>
					</table>
                </div>
                <div class="col-md-12">
                    <table class="table table-bordered table-striped">
                        <tbody>
                        	<tr>
	                            <td class="col-md-2">
	                                <strong>Hobbies</strong>
	                            </td>

	                            <td >
                                    @if(isset($$user->qualifications->hobbies))
	                                @foreach($user->qualifications->hobbies as $row)
					                	<span class="badge badge-success">{{$row}}</span>
					                @endforeach
                                    @endif
	                            </td>
                        	</tr>
                        </tbody>
                    </table>
                </div>

                
                
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>

        </div>

    </div>
</div>
