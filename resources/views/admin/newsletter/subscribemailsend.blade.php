@extends('admin.layouts.app')
@section('title', 'Subscribe')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('admin/css/jquery.dataTables.min.css')}}">
@endsection
@section('content')
    <style type="text/css">
    .modal-dialog{
        width: 800px;
    }
    .dataTables_empty{
        text-align: center !important;
    }
    .logo-td{
        padding-top: 24px !important;
    }
    .btn + .btn {
         margin-left: 0px !important; 
    }
    .width-50{
        width: 50% !important;
    }
    td {
        font-size:14px;
    }
    </style>

    <!-- BEGIN PAGE HEADER-->

<form role="form"  name="filter_listing"  name="filter_listing">

    <div class="page-head">
        <div class="page-title">
            <h1>
                Manage Subscribe
            </h1>
        </div>

        <div class="pull-right">
            <textarea hidden="" id="recipients" name="emails"></textarea>
            <button class="btn btn-primary send-email">Send Newsletters To Subscribers</button>
        </div>


    </div>

    <!-- END PAGE HEADER-->
    <div class="portlet light md-shadow-z-2-i">
        <div class="portlet-title">
        </div>
        <div class="portlet-body">
            <!-- Old jquery Datatable-->

	<!-- <table class="table table-striped table-bordered table-hover dataTable no-footer" id="datatable_ajax" aria-describedby="datatable_ajax_info" role="grid">
		<thead>
			<tr role="row" class="heading">
				<th width="5%" class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1">
					Sr. No
				</th>
				<th width="5%" class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1">
                    Email
				</th>
                <th width="5%" class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1">
                    Created At
                </th>
			</tr>
		</thead>
		<tbody id="dynamicContent">
			
@if(count($data)>0)
	@php
	$i= 1;
	@endphp

	@foreach($data as $row)
	<tr>
		<td>{{$i}}</td>
        <td>{{$row->email ? $row->email : 'N/A'}}</td>
        <td>{{$row->created_at->setTimezone(config('constants.TIMEZONE'))->format('F d, Y h:i a')}}</td>
	</tr>
	@php $i++; @endphp
	@endforeach
@endif
		</tbody>
	</table> -->

    <!-- Old jquery Datatable-->

    <table class="table table-striped table-bordered table-hover no-footer" role="grid">
        <thead>
            <tr role="row" class="heading">
                <th style="width: 70px;" class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1">
                    <input type="checkbox" name="addall">
                </th>
                <th style="width: 70px;" class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1">
                    Sr. No
                </th>
                <th  class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1">
                    Email
                </th>
                <th  class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1">
                    Created At
                </th>
            </tr>
        </thead>
        <tbody id="dynamicContent">
            
@if(count($data)>0)
    @php
    $i= 1;
    @endphp

    @foreach($data as $row)
    <tr>
        <td id="all_email_users">
            <input type="checkbox" name="email" value="{{$row->email ? $row->email : 'N/A'}}">
        </td>
        <td>{{$i}}</td>
        <td>{{$row->email ? $row->email : 'N/A'}}</td>
        <td>{{$row->created_at->setTimezone(config('constants.TIMEZONE'))->format('F d, Y h:i a')}}</td>
    </tr>
    @php $i++; @endphp
    @endforeach
@endif
        </tbody>
    </table>
    {{ $data->links() }}



</form>
        </div>
    </div>
    <div class="clearfix"></div>
@endsection


@section('pagejs')

<script src="{{ asset('admin/js/sweetalert2@9.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('admin/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript">
    const datatable = $('#datatable_ajax').DataTable();
    var jqxhr = {abort: function () {  }};

    $(document).ready(function(){
        $(document).on('click','.change-status', function(e){
            Swal.fire({
              title: 'Are you sure?',
              text: "You want to change status!",
              showCancelButton: true,
              confirmButtonText: 'Yes, change it!',
              cancelButtonText: 'No, cancel!',
              reverseButtons: true
            }).then((result) => {
            if (result.value) {
                var id = $(this).data("id");
                var status = $(this).data("status");
                $.ajax({
                    async : false,
                    url: 'specialization/status', //url
                    type: 'post', //request method
                    data: {'status':status,'update':'status','id':id},
                    success: function(data) {
                        if(data.status){
                            show_FlashMessage(data.message,'success');
                            setTimeout(function(){ window.location.reload() }, 1000);
                        }else{
                            show_FlashMessage(data.message,'error');
                        }
                    },
                    error: function(xhr) {
                        show_FlashMessage(xhr.message,'error');
                    }
                });
            }
            });
        });

        $(document).on('click','.delete-record', function(e){
            Swal.fire({
              title: 'Are you sure?',
              text: "You want to delete this!",
              showCancelButton: true,
              confirmButtonText: 'Yes, delete it!',
              cancelButtonText: 'No, cancel!',
              reverseButtons: true
            }).then((result) => {
            if (result.value) {
                var id = $(this).data("id");
                $.ajax({
                    async : false,
                    url: "{{url('admin/transaction')}}"+'/'+id, //url
                    type: 'post', //request method
                    data: {'id':id,'_method':'delete'},
                    success: function(data) {
                        if(data.status){
                            show_FlashMessage(data.message,'success');
                            setTimeout(function(){ window.location.reload() }, 1000);
                        }else{
                            show_FlashMessage(data.message,'error');
                        }
                    },
                    error: function(xhr) {
                        
                    }
                });
            }
            });
        });
    });
</script>

<!--New Script-->
 

<script type="text/javascript">
        $("input[name=email]").change(function() {
  updateAllChecked();
});

$("input[name=addall]").change(function() {
  if (this.checked) {
    $("input[name=email]").prop('checked', true).change();
  } else {
    $("input[name=email]").prop('checked', false).change();
  }
});

function updateAllChecked() {
  $('#recipients').text('');
  $("input[name=email]").each(function() {
    if (this.checked) {
      let old_text = $('#recipients').text() ? $('#recipients').text() + ',' : '';
      $('#recipients').text(old_text + $(this).val());
    }
  })
}
</script>

<script type="text/javascript">
    $(".send-email").click(function(refresh){
        refresh.preventDefault();
           var  emails = $("#recipients").val();
           /*alert(emails);*/
            $.ajax({
               type:'POST',
                headers:{
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },

               url:"{{url('admin/sendmailtosubscriber')}}",
               data:{emails:emails},
               beforeSend:function(){
                    startLoader('.page-content');
                },
                complete:function(){
                    stopLoader('.page-content');
                },
               success:function(data){
                  show_FlashMessage(data.message);

               }
            });
    });
  
</script>
@endsection
