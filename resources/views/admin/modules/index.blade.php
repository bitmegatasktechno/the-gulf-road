@extends('admin.layouts.app')
@section('title', 'Users')
@section('content')
    <style type="text/css">
    .modal-dialog{
        width: 800px;
    }
    .dataTables_empty{
        text-align: center !important;
    }
    .logo-td{
        padding-top: 24px !important;
    }
    .btn + .btn {
         margin-left: 0px !important; 
    }
    .width-50{
        width: 50% !important;
    }
    td {
        font-size:14px;
    }
    </style>
    <!-- BEGIN PAGE HEADER-->
    <div class="page-head">
        <div class="page-title">
            <h1>
                Modules
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <i class="fa fa-circle"></i>
            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
        </li>
    </ul>
    <!-- END PAGE HEADER-->
    <div class="portlet light md-shadow-z-2-i">
        <!-- <div class="portlet-title">
            <div class="caption">
                <a href="{{ asset( 'handset.csv') }}" class="btn  btn-primary pull-right black" style="margin-top: 4px;margin-right: 5px;"><i class="fa fa-cloud-download"></i>&nbsp;&nbsp;Download CSV</a>
            </div>
        </div> -->
        <div class="portlet-body">
            @include('admin.modules.table')
        </div>
    </div>
    <div class="clearfix"></div>
@endsection


@section('pagejs')

<script src="{{ asset('admin/js/sweetalert2@9.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    var filter_data = $("form[name=filter_listing]").serialize();
    var jqxhr = {abort: function () {  }};

    $(document).ready(function () {

        $(document).on('click','.change-status', function(e){
            Swal.fire({
              title: 'Are you sure?',
              text: "You want to change status!",
              showCancelButton: true,
              confirmButtonText: 'Yes, change it!',
              cancelButtonText: 'No, cancel!',
              reverseButtons: true
            }).then((result) => {
            if (result.value) {
                var id = $(this).data("id");
                var status = $(this).data("status");
                $.ajax({
                    async : false,
                    url: 'user/status', //url
                    type: 'post', //request method
                    data: {'status':status,'update':'status','id':id},
                    success: function(data) {
                        if(data.status){
                            loadListings(full_path + '/users?page=', 'filter_listing');
                            show_FlashMessage(data.message,'success');
                        }else{
                            show_FlashMessage(data.message,'error');
                        }
                    },
                    error: function(xhr) {
                        
                    }
                });
            }
            });
        });

        $(document).on('click','.delete-user', function(e){
            Swal.fire({
              title: 'Are you sure?',
              text: "You want to delete this user!",
              showCancelButton: true,
              confirmButtonText: 'Yes, Delete it!',
              cancelButtonText: 'No, Cancel!',
              reverseButtons: true
            }).then((result) => {
            if (result.value) {
                var id = $(this).data("id");
                $.ajax({
                    async : false,
                    url: 'user/delete', //url
                    type: 'post', //request method
                    data: {
                       'id':id
                    },
                    success: function(data) {
                        if(data.status){
                            loadListings(full_path + '/users?page=', 'filter_listing');
                            show_FlashMessage(data.message,'success');
                        }else{
                            show_FlashMessage(data.message,'error');
                        }
                    },
                    error: function(xhr) {
                        
                    }
                });
            }
            });
        });

        $(document).on('keyup', '#name, #email, #phone', function () {
            if($(this).val().length > 2)
            {
                loadListings(full_path + '/users?page=', 'filter_listing');
            }

            if($(this).val().length == 0)
            {
                loadListings(full_path + '/users/?page=', 'filter_listing');
            }
        });

        $(document).on('change', '#filterStatus', function () {
            loadListings(full_path + '/users/?page=', 'filter_listing');
        });

        $(document).on("click", ".pagination li a", function (e){
            e.preventDefault();
            startLoader('.page-content');
            var url = $(this).attr('href');
            var page = url.split('page=')[1];   ;       
            loadListings(url, 'filter_listing');
        });

        function loadListings(url,filter_form_name){

            var filtering = $("form[name=filter_listing]").serialize();
            //abort previous ajax request if any
            jqxhr.abort();
            jqxhr =$.ajax({
                type : 'get',
                url : url,
                data : filtering,
                dataType : 'html',
                beforeSend:function(){
                    startLoader('.page-content');
                },
                success : function(data){
                    data = data.trim();
                    $("#dynamicContent").empty().html(data);
                },
                error : function(response){
                    stopLoader('.page-content');
                    //console.log('Error',"Unable to fetch the list");
                },
                complete:function(){
                    stopLoader('.page-content');
                }
            });
        }

        // reset form data
        $(document).on('click', '.filter-cancel', function (e) {
            e.preventDefault();
            $("form[name='filter_listing']")[0].reset();
            loadListings(full_path +'/users/?page=', 'filter_listing');
        });
    });

        function check(loc,type){
        $.ajax({
                    async : false,
                    url: "{{url('admin/modules/check')}}", //url
                    type: 'get', //request method
                    data: {'loc':loc,'type':type},
                    success: function(data) {
                        if(data.status){
                            show_FlashMessage(data.message,'success');
                            setTimeout(function(){ window.location.reload() }, 1000);
                        }else{
                            show_FlashMessage(data.message,'error');
                        }
                    },
                    error: function(xhr) {
                        
                    }
                });
    }
</script>
@endsection
