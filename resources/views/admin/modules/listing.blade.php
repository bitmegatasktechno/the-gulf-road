@if(count($data)>0)
	@php
	$i= 1;
	@endphp

	@foreach($data as $row)
<tr>
	<td>{{$i}}</td>
	<td>{{$row->name ? $row->name : 'N/A'}}</td>
    <td><input type="checkbox" name="rent" @foreach($modules as $module) @if($module->location==$row->name && $module->type=='rent') checked @endif @endforeach onClick="check('{{$row->name}}','rent')" id="check{{$row->_id}}"></td>
    <td><input type="checkbox" name="buy" @foreach($modules as $module) @if($module->location==$row->name && $module->type=='buy') checked @endif @endforeach onClick="check('{{$row->name}}','buy')" id="check{{$row->_id}}"></td>
    <td><input type="checkbox" name="swap" @foreach($modules as $module) @if($module->location==$row->name && $module->type=='swap') checked @endif @endforeach onClick="check('{{$row->name}}','swap')" id="check{{$row->_id}}"></td>
    <td><input type="checkbox" name="cospace" @foreach($modules as $module) @if($module->location==$row->name && $module->type=='cospace') checked @endif @endforeach onClick="check('{{$row->name}}','cospace')" id="check{{$row->_id}}"></td>
    <td><input type="checkbox" name="agent" @foreach($modules as $module) @if($module->location==$row->name && $module->type=='agent') checked @endif @endforeach onClick="check('{{$row->name}}','agent')" id="check{{$row->_id}}"></td>
    <td><input type="checkbox" name="buddy" @foreach($modules as $module) @if($module->location==$row->name && $module->type=='buddy') checked @endif @endforeach onClick="check('{{$row->name}}','buddy')" id="check{{$row->_id}}"></td>
	
</tr>
@php $i++; @endphp
	@endforeach
@endif