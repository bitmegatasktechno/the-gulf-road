@extends('admin.layouts.app')
@section('title', 'Add Banner Image')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/admin/css/jquery.colorpicker.css')}}">
@endsection

@section('content')
<!-- BEGIN PAGE HEADER-->
<div class="page-head">
    <div class="page-title">
        <h1>
            Add Banner Image
        </h1>
    </div>
</div>
<ul class="page-breadcrumb breadcrumb">
    <li>
        <i class="fa fa-circle"></i>
        <a href="{{ route('admin.dashboard') }}">Dashboard</a>
        <i class="fa fa-circle"></i>
        <a href="{{ url('admin/banners') }}">Banner Images</a>
    </li>
</ul>
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light md-shadow-z-2-i">
            <div class="portlet-body form">
                <form role="form" method="POST" name="add_form" action="{{route('main_banner.store')}}"  enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-body">
                                <div class="form-group name">
                                    <label> Title</label>&nbsp;<span style="color: red;">*</span>
                                    <input type="text" name="title" class="form-control" placeholder="Enter Title">
                                    <span class="error"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                   
<div class="row">
                   <div class="col-md-12">
                            <div class="form-body">
                                <div class="form-group banner">
                                    <label>Banner (Type: jpeg, png, jpg, gif, svg) Max Size:2MB, Preferred Size: 1440*490 (width*height)</label>
                                    <span style="color: red;"> *</span>
                                    <input type="file" name="banner" class="form-control" >
                                    <span class="error"></span>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div></div>
                                            <div class="clearfix"></div>
                    <div class="clearfix"></div>
                    <div class="form-actions">
                        <button type="submit" class="btn blue">Submit</button>
                        <a href="{{url('admin/banners')}}"><button type="button" class="btn default">Cancel</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>
@endsection
@section('pagejs')

@endsection