
<style type="text/css">
    .upload-image input {
  display:none;
}

.upload-image {
  width: 10%;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  /*margin-left: auto;*/
  margin-right: auto;
  padding: 1rem;
  border-radius: 1rem;
  color: #000;
  font-family: sans-serif;
  transition: all .5s;
  border: 1px solid #e5e5e5;
      margin-top: 6px;
    background-color: lightgray;
}

</style>
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
            <div class="portlet-body form">
                <form role="form" method="GET" enctype="multipart/form-data" action="{{route('change_background_video')}}">
            
                    <div class="col-md-12">
                        <div class="form-body">

                            



                            <div class="form-group background_video">
                                <label>Home Page Video</label>
                                <label class="upload-image mt-2" for="background_video_id">
                                  <span>Select File</span>
                                  <input type="file" id="background_video_id" name="background_video_input" class="form-control" >
                                </label>
                                <!-- <input type="file" id="background_video_id" name="background_video_input" class="form-control" > -->

                                <input type="hidden" id="background_video_input" name="background_video" value="{{$cmsContent->background_video ?? ''}}">

                                    <span class="error"></span>

                                <span class="error"></span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <!-- <label>Image Preview</label> -->
                            <div class="clearfix"></div>
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">

                                    <input type="hidden" name="background_video_file" id="image">
                                    <!-- <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+video" alt="Logo" > -->
                                     <iframe  src="{{$cmsContent->background_video}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen style="width: 200px; height: 150px;" id="background_video_input_prev" class="background_video_input_prev"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-actions">
                        <button type="button" class="btn blue" onclick="changeVideo()">Submit</button>
                        <a href="{{url('admin')}}"><button type="button" class="btn default">Cancel</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@section('pagejs')

<script type="text/javascript">
    function changeVideo(banner)
    {
        $.ajax({
            async : true,
            beforeSend:function(){
                startLoader('body');
                },
            complete:function(){
                stopLoader('body');
            },
            url: "{{url('admin/change/video')}}", //url
            type: 'get', //request method
            data:{'background_video' : $('#background_video_input').val()},
            success: function(data) {
                if(data.status){
                    show_FlashMessage(data.message,'success');
                    setTimeout(function(){ window.location.reload() }, 1000);
                }else{
                    show_FlashMessage(data.message,'error');
                    stopLoader('body');
                }
            },
            error: function(xhr) {
                stopLoader('body');        
            }
        });
    }


</script>
@endsection