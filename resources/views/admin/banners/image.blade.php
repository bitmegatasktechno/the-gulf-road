
<style type="text/css">
    .upload-image input {
  display:none;
}

.upload-image {
  width: 10%;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  /*margin-left: auto;*/
  margin-right: auto;
  padding: 1rem;
  border-radius: 1rem;
  color: #000;
  font-family: sans-serif;
  transition: all .5s;
  border: 1px solid #e5e5e5;
      margin-top: 6px;
    background-color: lightgray;
}

</style>
<!-- END PAGE HEADER-->
<div class="row">
  <div class="col-md-12">
    <div class="portlet-body form">
      <form role="form" method="POST" enctype="multipart/form-data" action="{{route('change_homepage_banner')}}">
        @csrf
        <div class="col-md-12">
          <div class="form-body">
            <div class="form-group background_video">
              <label>Home Page Banner (Type: jpeg, png, jpg) Max Size:4MB, Preferred Size: 1920*1080 (width*height)</label>
              <label class="upload-image mt-2" for="homepage_banner">
                <span>Select File</span>
                <input type="file"  id="homepage_banner" name="banner" onchange="readURL(this)">
              </label>
              <span class="error"></span>
              <div class="clearfix"></div>
            </div>
           <img src="{{@$media->image ? url('uploads/banner/'.$media->image) : ''}}" id="image_preview" style="width: 200px; height: auto;">
          </div>
        </div>
        <div class="clearfix"></div>
        <div class="form-actions">
          <button type="submit" class="btn blue">Submit</button>
          <a href="{{url('admin')}}"><button type="button" class="btn default">Cancel</button></a>
        </div>
      </form>
    </div>
  </div>
</div>
@section('pagejs')
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                    $("#image_preview").attr('src', e.target.result)
                    .width(200)
                    .height('auto')
                    .css({'display':'block',"margin-top":"10px","border":"3px solid"});
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

</script>
@endsection