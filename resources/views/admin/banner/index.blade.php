@extends('admin.layouts.app')
@section('title', 'Change Banner')
@section('content')
<!-- BEGIN PAGE HEADER-->
<div class="page-head">
    <div class="page-title">
        <h1>
            Change Banner
        </h1>
    </div>
</div>
<ul class="page-breadcrumb breadcrumb">
    <li>
        <i class="fa fa-circle"></i>
        <a href="{{ route('admin.dashboard') }}">Dashboard</a>
        <i class="fa fa-circle"></i>
        <a href="{{ url('admin/banner') }}">Banner</a>
    </li>
</ul>
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light md-shadow-z-2-i">
            <div class="portlet-body form">
                <form role="form" method="POST" action="{{route('post_banner')}}" name="add_form" id="add_form" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-body">
                                <div class="form-group name">
                                    <label> Banner Type</label>&nbsp;<span style="color: red;">*</span>
                                    <select name="banner_type" class="form-control">
                                        <option value="buy">Buy</option>
                                        <option value="rent">Rent</option>
                                        <option value="swap">Swap</option>
                                        <option value="cospace">Co Space</option>
                                        <option value="agent">Agent</option>
                                        <option value="buddies">Buddies</option>
                                    </select>
                                    <span class="error"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-body">
                                <div class="form-group logo">
                                    <label>Logo</label><span style="color: red;"> *</span>
                                    <input type="file" name="banner_file" class="form-control" required>
                                    <span class="error"></span>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        @foreach($banner as $row)
                        <div class="col-md-2">
                            <div class="form-body">
                                <div class="form-group">
                                    <label>{{strtoupper($row->banner_type)}}</label>
                                     <img style="height:auto;width: 100%;"   src="{{url('uploads/banner/'.$row->banner_file)}}">
                                    <!--<img style="height:auto;width: 100%;" src="/uploads/banner/{{$row->banner_file}}">-->
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <div class="clearfix"></div>
                    <div class="form-actions">
                        <button type="submit" class="btn blue">Submit</button>
                        <a href="{{url('admin/amenities')}}"><button type="button" class="btn default">Cancel</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>
@endsection
@section('pagejs')

@endsection