<div class="page-sidebar-wrapper">
	<div class="page-sidebar md-shadow-z-2-i  navbar-collapse collapse">

		<ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

			<li class="sidebar-toggler-wrapper">
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				<div class="sidebar-toggler margin-bottom-20">
				</div>
				<!-- END SIDEBAR TOGGLER BUTTON -->
			</li>
			<li class="start @if($active == 'dashboard') active @endif">
				<a href="{{url('/admin/dashboard')}}">
					<span><i class="icon-home"></i></span>
					<span class="title">Dashboard</span>
				</a>
			</li>

			<li class="@if($active == 'users') active open @endif">
				<a href="javascript:;">
				<i class="fa fa-users"></i>
				<span class="title">All Users</span>
				<span class=""></span>
				<span class="arrow"></span>
				<span class="@if($active == 'users') selected @endif"></span>
				</a>
				<ul class="sub-menu class="@if($active == 'user') style="display: block;" @endif >
					<li class="@if($subactive=='users') active @endif">
						<a href="{{url('/admin/users')}}">
						<span class="title submenu_custom_css"><i class="fa fa-user"></i>Users</span></a>
					</li>
					<li class="@if($subactive=='agents') active @endif">
						<a href="{{url('/admin/agents')}}">
						<span class="title submenu_custom_css"><i class="fa fa-user-secret"></i> Agents</span></a>
					</li>
					<li class="@if($subactive=='buddies') active @endif">
						<a href="{{url('/admin/buddies')}}">
						<span class="title submenu_custom_css"><i class="fa fa-user-plus"></i> Buddies</span></a>
					</li>

				</ul>
			</li>

			<li class="@if($active == 'subscription') active open @endif">
				<a href="javascript:;">
				<i class="fa fa-star" aria-hidden="true"></i>
				<span class="title">Subscription</span>
				<span class=""></span>
				<span class="arrow"></span>
				<span class="@if($active == 'subscription') selected @endif"></span>
				</a>
				<ul class="sub-menu class="@if($active == 'subscription') style="display: block;" @endif >
					<li class="@if($subactive=='rent') active @endif">
						<a href="{{url('/admin/subscription/rent')}}">
						<span class="title submenu_custom_css"><i class="fa fa-cubes" aria-hidden="true"></i> Rent - Sale</span></a>
					</li>
					<li class="@if($subactive=='swap') active @endif">
						<a href="{{url('/admin/subscription/swap')}}">
						<span class="title submenu_custom_css"><i class="fa fa-random" aria-hidden="true"></i> Swap</span></a>
					</li>
					<li class="@if($subactive=='cospace') active @endif">
						<a href="{{url('/admin/subscription/cospace')}}">
						<span class="title submenu_custom_css"><i class="fa fa-university" aria-hidden="true"></i> Co-Space</span></a>
					</li>
					<li class="@if($subactive=='premium') active @endif">
						<a href="{{url('/admin/subscription/premium')}}">
						<span class="title submenu_custom_css"><i class="fa fa-diamond" aria-hidden="true"></i></i> Premium</span></a>
					</li>
					<li class="@if($subactive=='user') active @endif">
						<a href="{{url('/admin/subscription/user')}}">
						<span class="title submenu_custom_css"><i class="fa fa-address-book" aria-hidden="true"></i> User Profile</span></a>
					</li>
					<li class="@if($subactive=='revenue') active @endif">
						<a href="{{url('/admin/revenue')}}">
						<span class="title submenu_custom_css"><i class="fa fa-industry" aria-hidden="true"></i> Revenue</span></a>
					</li>
				</ul>
			</li>

			<!-- <li class="start @if($active == 'revenue') active @endif">
				<a href="{{url('/admin/revenue')}}">
					<span><i class="icon-map"></i></span>
					<span class="title">Revenue</span>
				</a>
			</li> -->

			<li class="@if($active == 'user_requests') active open @endif">
				<a href="javascript:;">
				<i class="fa fa-hourglass"></i>
				<span class="title">Profile Requests</span>
				<span class=""></span>
				<span class="arrow"></span>
				<span class="@if($active == 'user_requests') selected @endif"></span>
				</a>
				<ul class="sub-menu class="@if($active == 'user_requests') style="display: block;" @endif >
					<li class="@if($subactive=='agent_request') active @endif">
						<a href="{{url('/admin/applications/agents')}}">
						<span class="title submenu_custom_css"><i class="fa fa-user-secret"></i> Agent</span></a>
					</li>
					<li class="@if($subactive=='buddy_request') active @endif">
						<a href="{{url('/admin/applications/buddy')}}">
						<span class="title submenu_custom_css"><i class="fa fa-user-plus"></i> Buddy</span></a>
					</li>
				</ul>
			</li>

			<li class="start @if($active == 'contact') active @endif">
				<a href="{{url('/admin/contact')}}">
					<span><i class="fa fa-phone"></i></span>
					<span class="title">Contact</span>
				</a>
			</li>

			<li class="@if($active == 'locations') active open @endif">
				<a href="javascript:;">
				<i class="icon-map"></i>
				<span class="title">Locations</span>
				<span class=""></span>
				<span class="arrow"></span>
				<span class="@if($active == 'users') selected @endif"></span>
				</a>
				<ul class="sub-menu class="@if($active == 'locations') style="display: block;" @endif >
					<li class="@if($subactive=='countries') active @endif">
						<a href="{{url('/admin/countries')}}">
						<span class="title submenu_custom_css"><i class="icon-map"></i>Countries</span></a>
					</li>
					<li class="@if($subactive=='cities') active @endif">
						<a href="{{url('/admin/cities')}}">
						<span class="title submenu_custom_css"><i class="icon-map"></i> Cities</span></a>
					</li>

				</ul>
			</li>


			<li class="start @if($active == 'banner') active @endif">
				<a href="{{url('/admin/banner')}}">
					<span><i class="fa fa-picture-o" aria-hidden="true"></i></span>
					<span class="title">Banner</span>
				</a>
			</li>

			<li class="start @if($active == 'main_banner') active @endif">
				<a href="{{url('/admin/main_banner')}}">
					<span><i class="fa fa-picture-o" aria-hidden="true"></i></span>
					<span class="title">Main Banner</span>
				</a>
			</li>


			<li class="start @if($active == 'reviews') active @endif">
				<a href="{{url('/admin/reviews')}}">
					<span><i class="fa fa-commenting" aria-hidden="true"></i></span>
					<span class="title">Reviews</span>
				</a>
			</li>

			<li class="start @if($active == 'reports') active @endif">
				<a href="{{url('/admin/reports')}}">
					<span><i class="fa fa-ban" aria-hidden="true"></i></span>
					<span class="title">Reports</span>
				</a>
			</li>

			<!-- <li class="start @if($active == 'languages') active @endif">
				<a href="{{url('/admin/languages')}}">
					<span><i class="fa fa-language" aria-hidden="true"></i></span>
					<span class="title">Languages</span>
				</a>
			</li> -->

			<li class="start @if($active == 'testimonials') active @endif">
				<a href="{{url('/admin/testimonials')}}">
					<span><i class="fa fa-cubes" aria-hidden="true"></i></span>
					<span class="title">Testimonials</span>
				</a>
			</li>

			<li class="start @if($active == 'companiesIcons') active @endif">
				<a href="{{url('/admin/companiesIcons')}}">
					<span><i class="fa fa-cubes" aria-hidden="true"></i></span>
					<span class="title">Brands Icons</span>
				</a>
			</li>

			<li class="start @if($active == 'currencies') active @endif">
				<a href="{{url('/admin/currencies')}}">
					<span><i class="fa fa-dollar"></i></span>
					<span class="title">Currencies</span>
				</a>
			</li>
			<li class="start @if($active == 'awards') active @endif">
				<a href="{{url('/admin/awards')}}">
					<span><i class="fa fa-trophy" aria-hidden="true"></i></span>
					<span class="title">Add Awards</span>
				</a>
			</li>

			<li class="start @if($active == 'video') active @endif">
				<a href="{{url('/admin/video')}}">
					<span><i class="fa fa-film" aria-hidden="true"></i></span>
					<span class="title">Landing Video</span>
				</a>
			</li>

			<li class="start @if($active == 'modules') active @endif">
				<a href="{{url('/admin/modules')}}">
					<span><i class="fa fa-folder-open" aria-hidden="true"></i></span>
					<span class="title">Modules Management</span>
				</a>
			</li>

			<li class="start @if($active == 'blogs') active @endif">
				<a href="{{url('/admin/blogs')}}">
					<span><i class="fa fa-laptop" aria-hidden="true"></i></span>
					<span class="title">Blogs</span>
				</a>
			</li>

			<li class="start @if($active == 'enquery') active @endif">
				<a href="{{url('/admin/enquery')}}">
					<span><i class="fa fa-exclamation-circle" aria-hidden="true"></i></span>
					<span class="title">Enquiry</span>
				</a>
			</li>

			<li class="start @if($active == 'newsletter') active @endif">
				<a href="{{url('/admin/newsletter')}}">
					<span><i class="fa fa-bolt" aria-hidden="true"></i></span>
					<span class="title">Newsletter</span>
				</a>
			</li>

			<li class="start @if($active == 'booking') active @endif">
				<a href="{{url('/admin/booking')}}">
					<span><i class="fa fa-shopping-bag" aria-hidden="true"></i></span>
					<span class="title">Booking</span>
				</a>
			</li>

			<li class="start @if($active == 'homeloan') active @endif">
				<a href="{{url('/admin/homeloan')}}">
					<span><i class="fa fa-tint" aria-hidden="true"></i></span>
					<span class="title">Homeloan</span>
				</a>
			</li>

			<li class="start @if($active == 'subscribe') active @endif">
				<a href="{{url('/admin/subscribe')}}">
					<span><i class="fa fa-check-square" aria-hidden="true"></i></span>
					<span class="title">Subscribe</span>
				</a>
			</li>

			<!-- <li class="@if($active == 'properties') active open @endif">
				<a href="javascript:;">
				<i class="fa fa-home"></i>
				<span class="title">Enquiries and Consultations</span>
				<span class=""></span>
				<span class="arrow"></span>
				<span class="@if($active == 'properties') selected @endif"></span>
				</a>
				<ul class="sub-menu class="@if($active == 'properties') style="display: block;" @endif >

					<li class="@if($subactive=='rent') active @endif">
						<a href="{{url('/admin/properties/rent')}}">
							<span class="title submenu_custom_css"><i class="fa fa-home"></i> Enquiries</span>
						</a>
					</li>

					<li class="@if($subactive=='sale') active @endif">
						<a href="{{url('/admin/properties/sale')}}">
							<span class="title submenu_custom_css"><i class="fa fa-home"></i> Subscriptions</span>
						</a>
					</li>

					<li class="@if($subactive=='rentsale') active @endif">
						<a href="{{url('/admin/properties/rentsale')}}">
							<span class="title submenu_custom_css"><i class="fa fa-home"></i> Newsletters</span>
						</a>
					</li>

					<li class="@if($subactive=='coworkspace') active @endif">
						<a href="{{url('/admin/coworkspaces')}}">
							<span class="title submenu_custom_css"><i class="fa fa-building"></i> Contact Us</span>
						</a>
					</li>

					<li class="@if($subactive=='swap') active @endif">
						<a href="{{url('/admin/swap')}}">
							<span class="title submenu_custom_css"><i class="fa fa-building"></i> Report Listing</span>
						</a>
					</li>
				</ul>
			</li> -->

			<li class="@if($active == 'properties') active open @endif">
				<a href="javascript:;">
				<i class="fa fa-home"></i>
				<span class="title">Properties Lists</span>
				<span class=""></span>
				<span class="arrow"></span>
				<span class="@if($active == 'properties') selected @endif"></span>
				</a>
				<ul class="sub-menu class="@if($active == 'properties') style="display: block;" @endif >

					<li class="@if($subactive=='rent') active @endif">
						<a href="{{url('/admin/properties/rent')}}">
							<span class="title submenu_custom_css"><i class="fa fa-home"></i> Rent</span>
						</a>
					</li>

					<li class="@if($subactive=='sale') active @endif">
						<a href="{{url('/admin/properties/sale')}}">
							<span class="title submenu_custom_css"><i class="fa fa-home"></i> Sale</span>
						</a>
					</li>

					<li class="@if($subactive=='rentsale') active @endif">
						<a href="{{url('/admin/properties/rentsale')}}">
							<span class="title submenu_custom_css"><i class="fa fa-home"></i> Rent + Sale</span>
						</a>
					</li>

					<li class="@if($subactive=='coworkspace') active @endif">
						<a href="{{url('/admin/coworkspaces')}}">
							<span class="title submenu_custom_css"><i class="fa fa-building"></i> Coworkspace</span>
						</a>
					</li>

					<li class="@if($subactive=='swap') active @endif">
						<a href="{{url('/admin/swap')}}">
							<span class="title submenu_custom_css"><i class="fa fa-building"></i> Swap</span>
						</a>
					</li>
				</ul>
			</li>

			<li class="@if($active == 'propertycms') active open @endif">
				<a href="javascript:;">
				<i class="fa fa-home"></i>
				<span class="title">Property CMS</span>
				<span class=""></span>
				<span class="arrow"></span>
				<span class="@if($active == 'propertycms') selected @endif"></span>
				</a>
				<ul class="sub-menu class="@if($active == 'propertycms') style="display: block;" @endif >

				<li class="@if($subactive=='specialization') active @endif">
						<a href="{{url('/admin/specialization')}}">
						<span class="title submenu_custom_css"><i class="fa fa-home"></i> Specialization</span></a>
					</li>

					<li class="@if($subactive=='housetypes') active @endif">
						<a href="{{url('/admin/housetypes')}}">
						<span class="title submenu_custom_css"><i class="fa fa-home"></i> House Types</span></a>
					</li>

					<li class="@if($subactive=='amenitycategory') active @endif">
						<a href="{{url('/admin/amenitycategories')}}">
						<span class="title submenu_custom_css"><i class="fa fa-cogs"></i> Amenity Category</span></a>
					</li>

					<li class="@if($subactive=='specialamenities') active @endif">
						<a href="{{url('/admin/specialamenities')}}">
						<span class="title submenu_custom_css"><i class="fa fa-cogs"></i>Special Amenities</span></a>
					</li>

					<li class="@if($subactive=='amenities') active @endif">
						<a href="{{url('/admin/amenities')}}">
						<span class="title submenu_custom_css"><i class="fa fa-cogs"></i>House Amenities</span></a>
					</li>

					<li class="@if($subactive=='flooring') active @endif">
						<a href="{{url('/admin/flooring')}}">
						<span class="title submenu_custom_css"><i class="fa fa-building"></i> Flooring Types</span></a>
					</li>

					<li class="@if($subactive=='cospaceStories') active @endif">
						<a href="{{url('/admin/cospaceStories')}}">
						<span class="title submenu_custom_css"><i class="fa fa-building"></i> Cospace Stories</span></a>
					</li>

				</ul>
			</li>

			<li class="start">
				<a href="{{url('admin/profile')}}">
					<span><i class="icon-lock"></i></span>
					<span class="title"> Change Password</span>
				</a>
			</li>
			<li class="start">
				<a href="{{ route('admin.logout') }}">
                    <span><i class="icon-key"></i></span>
					<span class="title">Logout</span>
                </a>
			</li>
		</ul>
	</div>
</div>
