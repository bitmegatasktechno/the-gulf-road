<?php
$type=Request::segment(5);
?>
 <style>
.mce-in{
  display:none;
}
   </style>
@extends('admin.layouts.app')
@section('title', 'Edit Suscription')
@section('content')
<script type="text/javascript" src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc"></script>
<!-- BEGIN PAGE HEADER-->
<div class="page-head">
    <div class="page-title">
        <h1>
            Edit Suscription
        </h1>
    </div>
</div>
<ul class="page-breadcrumb breadcrumb">
    <li>
        <i class="fa fa-circle"></i>
        <a href="{{ route('admin.dashboard') }}">Dashboard</a>
        <i class="fa fa-circle"></i>
        <!-- <a href="{{ url('admin/housetypes') }}">Suscription</a> -->
    </li>
</ul>
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light md-shadow-z-2-i">
            <div class="portlet-body form">
                <form role="form" method="POST" name="add_form" id="add_form" action="{{url('admin/subscription/update')}}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="type" value="{{$type}}">
                <input type="hidden" name="id" value="{{$sub->_id}}">
                <div class="col-md-6">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Plan Name</label>&nbsp;<span style="color: red;" >*</span>
                                <input type="text" name="plan" class="form-control" placeholder="Enter Name" value="{{$sub->name}}">
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>

                      <div class="col-md-6">
                        <div class="form-body">
                            <div class="form-group logo">
                                <label>Icon</label><span style="color: red;" > *</span>
                                <input type="file" name="logo" class="form-control">
                                <span class="error"></span>
                                <div class="clearfix"></div>
                            </div>
                        </div>         
                    </div>

                    <input type="hidden" name="plan_number1" value="1">
                    <h4>Sub Plan - 1</h4>
                    <div class="col-md-4">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Validity Days</label>&nbsp;<span style="color: red;">*</span>
                                <input type="number" id="validity1" name="validity1" class="form-control" placeholder="Enter Validity Days" value="{{$sub->sub_plan[0]['validity']??''}}" required>
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Amount ($)</label>&nbsp;<span style="color: red;" >*</span>
                                <input type="number" id="amount1" name="amount1" class="form-control" placeholder="Enter Amount" value="{{$sub->sub_plan[0]['amount']}}" required>
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>

                    @if($type!='user' && $type!='premium' )
                    <div class="col-md-3">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Total property can be listed</label>&nbsp;<span style="color: red;"></span>
                                <input type="number" id="count1" name="count1" class="form-control" placeholder="Enter Count" value="{{$sub->sub_plan[0]['count']}}">
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>
                    @else
                    <input type="hidden" name="count1" class="form-control" value="0">
                    @endif
                    <div class="col-md-1">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>&nbsp;</label><br>
                                <span class="clear clear btn btn-success" style="font-size: 13px;" onClick="clear1()">Clear</span>
                            </div>
                        </div>
                    </div>
                    <br><br><br><br><br><br>
                    
                    <div id="second" style="display:{{isset($sub->sub_plan[1]) ? 'block' : 'none'}};">
                    <input type="hidden" name="plan_number2" value="2">
                    <h4>Sub Plan - 2</h4>
                     <div class="col-md-4">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Validity Days</label>&nbsp;<span style="color: red;">*</span>
                                <input type="number" id="validity2" name="validity2" class="form-control" placeholder="Enter Validity Days" value="{{$sub->sub_plan[1]['validity']??''}}">
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Amount ($)</label>&nbsp;<span style="color: red;" >*</span>
                                <input type="number" id="amount2" name="amount2" class="form-control" placeholder="Enter Amount" value="{{$sub->sub_plan[1]['amount']??''}}">
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>

                    @if($type!='user')
                    <div class="col-md-3">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Total property can be listed</label>&nbsp;<span style="color: red;"></span>
                                <input type="number" id="count2" name="count2" class="form-control" placeholder="Enter Count" value="{{$sub->sub_plan[1]['count']??''}}">
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>
                    @else
                    <input type="hidden" name="count2" class="form-control" value="0">
                    @endif
                    <div class="col-md-1">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>&nbsp;</label><br>
                                <span class="clear clear btn btn-success" style="font-size: 13px;" onClick="clear2()">Clear</span>
                            </div>
                        </div>
                    </div>
                    </div>
                    <br><br><br><br><br><br>
                    
                    <div id="third" style="display:{{isset($sub->sub_plan[2]) ? 'block' : 'none'}};">
                    <input type="hidden" name="plan_number3" value="3">
                    <h4>Sub Plan - 3</h4>
                     <div class="col-md-4">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Validity Days</label>&nbsp;<span style="color: red;">*</span>
                                <input type="number" id="validity3" name="validity3" class="form-control" placeholder="Enter Validity Days" value="{{$sub->sub_plan[2]['validity']??''}}">
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Amount ($)</label>&nbsp;<span style="color: red;" >*</span>
                                <input type="number" id="amount3" name="amount3" class="form-control" placeholder="Enter Amount" value="{{$sub->sub_plan[2]['amount']??''}}">
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>

                    @if($type!='user')
                    <div class="col-md-3">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Total property can be listed</label>&nbsp;<span style="color: red;"></span>
                                <input type="number" id="count3" name="count3" class="form-control" placeholder="Enter Count" value="{{$sub->sub_plan[2]['count']??''}}">
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>
                    @else
                    <input type="hidden" name="count" class="form-control" value="0">
                    @endif
                    <div class="col-md-1">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>&nbsp;</label><br>
                                <span class="clear clear btn btn-success" style="font-size: 13px;" onClick="clear3()">Clear</span>
                            </div>
                        </div>
                    </div>
                    </div>
                    
                    @if($type!='user' && $type!='premium')
                    
                  <button type="button" onClick="secondsec()">Second Sub Plan</button>
                  
                 
                  <button type="button" onClick="thirdsec()">Third Sub Plan</button>
                    @endif
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="form-body">
                            <div class="form-group name">
                                <label> Content</label>&nbsp;<span style="color: red;">*</span>
                                <textarea name="desription" class="form-control" id="desription">{{$sub->content}}</textarea>
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>


                    
                    
                    

                    <div class="clearfix"></div>
                    <div class="form-actions">
                        <button type="submit" class="btn blue">Submit</button>
                        <a href="{{url('admin/subscription/'.$type)}}"><button type="button" class="btn default">Cancel</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>
@endsection
@section('pagejs')
<script type="text/javascript">
function secondsec(){
    $("#second").toggle();
}
function thirdsec(){
    $("#third").toggle();
}
        tinymce.init({
  selector: 'textarea',
  height: 500,
  theme: 'modern',
  plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });
    $(document).ready(function () {


    });

    function clear1(){
        $("#validity1").val('');
        $("#amount1").val('');
        $("#count1").val('');
    }

    function clear2(){
        $("#validity2").val('');
        $("#amount2").val('');
        $("#count2").val('');
    }

    function clear3(){
        $("#validity3").val('');
        $("#amount3").val('');
        $("#count3").val('');
    }
</script>
@endsection