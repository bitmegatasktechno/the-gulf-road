@php
$type=Request::segment(3);
if($type=='rent'){
    $subscriptions = \App\Models\Subscription::where('type','rent')->get();
}
if($type=='cospace'){
  
    $subscriptions = \App\Models\Subscription::where('type','cospace')->get();
}
if($type=='swap'){
    $subscriptions = \App\Models\Subscription::where('type','swap')->get();
}
if($type=='user'){
    $subscriptions = \App\Models\Subscription::where('type','user')->get();
}
if($type=='premium'){
    $subscriptions = \App\Models\Subscription::where('type','premium')->get();
}
@endphp

<?php
$type=Request::segment(3);
?>
@extends('admin.layouts.app')
@section('title', 'Subscription')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('admin/css/jquery.dataTables.min.css')}}">
@endsection
@section('content')
    <style type="text/css">
    .bottum-edit a{
  color:#777;
  margin-right: 10px;
  margin-top:10px;
  display:inline-block;

}
    .modal-dialog{
        width: 800px;
    }
    .dataTables_empty{
        text-align: center !important;
    }
    .logo-td{
        padding-top: 24px !important;
    }
    .btn + .btn {
         margin-left: 0px !important; 
    }
    .width-50{
        width: 50% !important;
    }
    td {
        font-size:14px;
    }
    ul{
        font-size: 16px;
    }
    </style>
    <!-- BEGIN PAGE HEADER-->
    <!-- <div class="page-head">
        <div class="page-title">
            <h1>
                Manage Subscription
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <i class="fa fa-circle"></i>
            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
        </li>
    </ul> -->
    <!-- END PAGE HEADER-->
    <div class="portlet light md-shadow-z-2-i">
        <div class="portlet-title">
            <div class="caption">
                <a href="{{url('admin/subscription/create/'.$type)}}" class="btn  btn-primary pull-right black" style="margin-top: 4px;margin-right: 5px;"><i class="fa fa-plus"></i>&nbsp;&nbsp; Add Subscription</a>
            </div>
        </div>
        <div class="portlet-body">
        <div class="">
        <div class="row">
        @if(@$subscriptions)
      @foreach($subscriptions as $subscription)
      @php
$type=Request::segment(3);
if($type=='rent'){
    $transaction = \App\Models\Transaction::where('purchase_type','rent')->where('plan_id',$subscription->_id)->count();
}
if($type=='cospace'){
  
    $transaction = \App\Models\Transaction::where('purchase_type','cospace')->where('plan_id',$subscription->_id)->count();
}
if($type=='swap'){
    $transaction = \App\Models\Transaction::where('purchase_type','swap')->where('plan_id',$subscription->_id)->count();
}
if($type=='premium'){
    $transaction = \App\Models\Transaction::where('purchase_type','premium')->where('plan_id',$subscription->_id)->count();
}
if($type=='user'){
    $transaction = \App\Models\Transaction::where('purchase_type','user')->where('plan_id',$subscription->_id)->count();
}
@endphp
    <div class="col-md-4" >
<section class="plans-and-pricing mt-5 bg-white subscribe" style="border: 1px solid;">
        <div class="price-box">
          <div class="pt-5 pb-4 pl-4 pr-4">
          <div class="text-center">
            <img style="width: 100px;" class="img-fluid" src="{{asset('../uploads/subscription/'.@$subscription->logo)}}">
            <h3>{{@$subscription->name}}</h3>
          </div>
          <?php echo @$subscription->content ?>
          @foreach($subscription->sub_plan as $subscription_plan)
          <hr class="" style="margin: 7px 0 !important;">
          <span class="text-center" style="font-size: 14px;display: block;
    text-align: center;margin-bottom:8px;">Validity : {{@$subscription_plan['validity']}} days</span>
    @if($subactive != 'premium')
         <span class="text-center" style="font-size:14px;display: block;
    text-align: center;margin-bottom:8px;">Total Properties listed : {{@$subscription_plan['count']}}</span>
    @endif
          <span class="text-center" style="font-size:14px;display: block;
    text-align: center;margin-bottom:8px;">Price : ${{@$subscription_plan['amount']}}</span>
          @endforeach
        </div>
        </div>
</section>
<div class="bottum-edit">
<a class="achorsubscription" href="{{route('subscribe.users',['type'=>@$subscription->type,'plan_id'=>@$subscription->_id])}}"><span>Total User Subscribe : {{@$transaction}}</span></a>
<a href="{{url('admin/subscription/'.$subscription->_id.'/edit/'.$subscription->type)}}" class="view achoredit">
								<i class="icon-pencil"></i> Edit
</a>
<a class="view delete-record achordelete" data-id="{{$subscription->_id}}"><i class="icon-trash"></i> Delete</a>
</div>
</div>
@endforeach


@endif
</div>
</div>
        </div>
    </div>
    <div class="clearfix"></div>
@endsection


@section('pagejs')

<script src="{{ asset('admin/js/sweetalert2@9.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('admin/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript">
    const datatable = $('#datatable_ajax').DataTable();
    var jqxhr = {abort: function () {  }};

    $(document).ready(function(){
        $(document).on('click','.change-status', function(e){
            Swal.fire({
              title: 'Are you sure?',
              text: "You want to change status!",
              showCancelButton: true,
              confirmButtonText: 'Yes, change it!',
              cancelButtonText: 'No, cancel!',
              reverseButtons: true
            }).then((result) => {
            if (result.value) {
                var id = $(this).data("id");
                var status = $(this).data("status");
                $.ajax({
                    async : false,
                    url: 'testimonials/status', //url
                    type: 'post', //request method
                    data: {'status':status,'update':'status','id':id},
                    success: function(data) {
                        if(data.status){
                            show_FlashMessage(data.message,'success');
                            setTimeout(function(){ window.location.reload() }, 1000);
                        }else{
                            show_FlashMessage(data.message,'error');
                        }
                    },
                    error: function(xhr) {
                        show_FlashMessage(xhr.message,'error');
                    }
                });
            }
            });
        });

        $(document).on('click','.delete-record', function(e){
            Swal.fire({
              title: 'Are you sure?',
              text: "You want to delete this!",
              showCancelButton: true,
              confirmButtonText: 'Yes, delete it!',
              cancelButtonText: 'No, cancel!',
              reverseButtons: true
            }).then((result) => {
            if (result.value) {
                var id = $(this).data("id");
                $.ajax({
                    async : false,
                    url: "{{url('admin/subscription/delete')}}"+'/'+id, //url
                    type: 'post', //request method
                    data: {'id':id,'_method':'delete'},
                    success: function(data) {
                        if(data.status){
                            show_FlashMessage(data.message,'success');
                            setTimeout(function(){ window.location.reload() }, 1000);
                        }else{
                            show_FlashMessage(data.message,'error');
                        }
                    },
                    error: function(xhr) {
                        
                    }
                });
            }
            });
        });
    });
</script>
@endsection
