<?php
$type=Request::segment(4);
?>
 <style>
.mce-in{
  display:none;
}
   </style>
@extends('admin.layouts.app')
@section('title', 'Add Subscription')
@section('content')
<script type="text/javascript" src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=qagffr3pkuv17a8on1afax661irst1hbr4e6tbv888sz91jc"></script>
<!-- BEGIN PAGE HEADER-->
<div class="page-head">
    <div class="page-title">
        <h1>
            Add Subscription
        </h1>
    </div>
</div>
<ul class="page-breadcrumb breadcrumb">
    <li>
        <i class="fa fa-circle"></i>
        <a href="{{ route('admin.dashboard') }}">Dashboard</a>
      <!--   <i class="fa fa-circle"></i>
        <a href="{{ url('admin/housetypes') }}">Subscription</a> -->
    </li>
</ul>
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light md-shadow-z-2-i">
            <div class="portlet-body form">
                <form role="form" method="POST" name="add_form" id="add_form" action="{{url('admin/subscription/save')}}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="type" value="{{$type}}">
                <div class="col-md-4">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Plan Name</label>&nbsp;<span style="color: red;" >*</span>
                                <input type="text" name="plan" class="form-control" placeholder="Enter Name" required>
                                @if($errors->any())
                                <span class="error" style="color:red">{{$errors->first()}}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                      <div class="col-md-4">
                        <div class="form-body">
                            <div class="form-group logo">
                                <label>Icon</label><span style="color: red;" > *</span>
                                <input type="file" name="logo" class="form-control" required>
                                <span class="error"></span>
                                <div class="clearfix"></div>
                            </div>
                        </div>         
                    </div>
                   

                    @if($getTrans == 1)
                    <div class="col-md-4">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Order</label>&nbsp;<span style="color: red;" >*</span>
                                
                                <select name="order" class="form-control" required>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                                
    
                            </div>
                        </div>
                    </div>
                    @elseif($getTrans == 2)
                    <div class="col-md-4">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Order</label>&nbsp;<span style="color: red;" >*</span>
                                
                                <select name="order" class="form-control" required>
                                    <option value="3">3</option>
                                </select>
                                
    
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="col-md-4">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Order</label>&nbsp;<span style="color: red;" >*</span>
                                
                                <input type="text" class="form-control" name="order" value="1" readonly>
                                
    
                            </div>
                        </div>
                    </div>
                    
                    @endif
                    <input type="hidden" name="plan_number1" value="1">
                    <h4>Sub Plan - 1</h4>
                    <div class="col-md-4">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Validity Days</label>&nbsp;<span style="color: red;">*</span>
                                <input type="number" min="1" name="validity1" class="form-control" placeholder="Enter Validity Days" required>
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Amount ($)</label>&nbsp;<span style="color: red;" >*</span>
                                <input type="number" min="0" name="amount1" class="form-control" placeholder="Enter Amount" required>
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>

                    @if($type!='user' && $type!='premium' )
                    <div class="col-md-4">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Total property can be listed</label>&nbsp;<span style="color: red;"></span>
                                <input type="number" min="1" name="count1" class="form-control" placeholder="Enter Count">
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>
                    @else
                    <input type="hidden" name="count1" class="form-control" value="0">
                    @endif

                    <div id="second" style="display:none">
                    <input type="hidden" name="plan_number2" value="2">
                    <h4>Sub Plan - 2</h4>
                     <div class="col-md-4">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Validity Days</label>&nbsp;<span style="color: red;">*</span>
                                <input type="number" name="validity2" class="form-control" placeholder="Enter Validity Days">
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Amount ($)</label>&nbsp;<span style="color: red;" >*</span>
                                <input type="number" min="0" name="amount2" class="form-control" placeholder="Enter Amount">
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>

                    @if($type!='user')
                    <div class="col-md-4">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Total property can be listed</label>&nbsp;<span style="color: red;"></span>
                                <input type="number" min="1" name="count2" class="form-control" placeholder="Enter Count">
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>
                    @else
                    <input type="hidden" name="count2" class="form-control" value="0">
                    @endif
                    </div>

                    <div id="third" style="display:none">
                    <input type="hidden" name="plan_number3" value="3">
                    <h4>Sub Plan - 3</h4>
                     <div class="col-md-4">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Validity Days</label>&nbsp;<span style="color: red;">*</span>
                                <input type="number" min="1" name="validity3" class="form-control" placeholder="Enter Validity Days">
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Amount ($)</label>&nbsp;<span style="color: red;" >*</span>
                                <input type="number" min="0" name="amount3" class="form-control" placeholder="Enter Amount">
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>

                    @if($type!='user')
                    <div class="col-md-4">
                        <div class="form-body">
                            <div class="form-group name">
                                <label>Total property can be listed</label>&nbsp;<span style="color: red;"></span>
                                <input type="number" min="1" name="count3" class="form-control" placeholder="Enter Count">
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>
                    @else
                    <input type="hidden" name="count" class="form-control" value="0">
                    @endif
                    </div>
                    @if($type!='user' && $type!='premium')
                  <button type="button" onClick="secondsec()">Second Sub Plan</button>
                  <button type="button" onClick="thirdsec()">Third Sub Plan</button>
                    @endif
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="form-body">
                            <div class="form-group name">
                                <label> Content</label>&nbsp;<span style="color: red;">*</span>
                                <textarea name="desription" class="form-control" id="desription"></textarea>
                                <span class="error"></span>
                            </div>
                        </div>
                    </div>


                    
                    
                    

                    <div class="clearfix"></div>
                    <div class="form-actions">
                        <button type="submit" class="btn blue">Submit</button>
                        <a href="{{url('admin/subscription/'.$type)}}"><button type="button" class="btn default">Cancel</button></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>
@endsection
@section('pagejs')
<script type="text/javascript">
function secondsec(){
    $("#second").toggle();
}
function thirdsec(){
    $("#third").toggle();
}
        tinymce.init({
  selector: 'textarea',
  height: 500,
  theme: 'modern',
  plugins: 'print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount tinymcespellchecker a11ychecker imagetools mediaembed  linkchecker contextmenu colorpicker textpattern help',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });
    $(document).ready(function () {


    });
</script>
@endsection