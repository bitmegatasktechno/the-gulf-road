@extends('admin.layouts.app')
@section('title', 'Swap Details')
@section('css')
<link rel="stylesheet" href="{{asset('admin/css/jquery.fancybox.css')}}" type="text/css" media="screen" />

@endsection
@section('content')
    
    <!-- BEGIN PAGE HEADER-->
    <div class="page-head">
        <div class="page-title">
            <h1>
                Swap Details
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <i class="fa fa-circle"></i>
            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
            <i class="fa fa-circle"></i>
            <a href="{{ url('admin/swap') }}">Swap Listing</a>

        </li>
    </ul>
    <!-- END PAGE HEADER-->
    <div class="portlet light md-shadow-z-2-i">
        <div class="portlet-body">
            @include('admin.properties.swap.details.section')
        </div>
    </div>
    <div class="clearfix"></div>
@endsection

@section('js')
    <script type="text/javascript" src="{{asset('admin/js/jquery.fancybox.pack.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".fancybox").fancybox({});
        });
    </script>
@endsection