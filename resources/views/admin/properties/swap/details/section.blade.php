<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Basic Details</div>
            </div>
            <div class="portlet-body">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>Title:</strong>
                                </td>
                                <td>
                                    {{ ucfirst($data->title) ?? 'N/A'}}
                                </td>
                            </tr><tr>
                                <td>
                                    <strong>House Type:</strong>
                                </td>
                                <td>
                                    {{ ucfirst($data->house_type) ?? 'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Location Address:</strong>
                                </td>
                                <td>
                                   {{ $data->location_name ?? 'N/A'}}
                                </td>
                            </tr>

                            
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>Country:</strong>
                                </td>
                                <td>
                                   {{ ucfirst($data->country) ?? 'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Accesibility:</strong>
                                </td>
                                <td>
                                   {{ $data->location_accessible ? ucfirst(str_replace('_',' ', $data->location_accessible)) :'N/A'}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>


<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Size & Space</div>
            </div>
            <div class="portlet-body">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>Surface Area:</strong>
                                </td>
                                <td>
                                	{{$data->surface_area ?? 0}}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>Surface Area Unit:</strong>
                                </td>
                                <td>
                                    {{$data->surface_area_unit ?? 'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong> No of Bedrooms:</strong>
                                </td>
                                <td>
                                    {{$data->no_of_bedrooms ?? 0}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong> No of Bathrooms:</strong>
                                </td>
                                <td>
                                    {{$data->no_of_bathrooms ?? 0}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>No of Single Beds:</strong>
                                </td>
                                <td>
                                    {{$data->no_of_single_beds ?? 0}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>No of Double Beds:</strong>
                                </td>
                                <td>
                                    {{$data->no_of_double_beds ?? 0}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>No of Children Beds:</strong>
                                </td>
                                <td>
                                    {{$data->no_of_children_beds ?? 0}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>No of Extra Beds:</strong>
                                </td>
                                <td>
                                    {{$data->no_of_extra_beds ?? 0}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Total Accommodations:</strong>
                                </td>
                                <td>
                                    {{$data->total_accommodation ?? 0}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>


<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Amenities</div>
            </div>
            <div class="portlet-body">
                <div class="tab-container">
                    @php
                        $amenities = $data->amenties;                       
                        if(!is_array($amenities)){
                            $amenities = [];
                        }
                    @endphp
                    <div class="row">
                       <!--  @foreach(getAllAmenities() as $row)
                            @if(in_array($row->_id, $amenities))
                                <div class="col-md-3">
                                    <div class="facility-block" style="text-align: center;border: 1px solid grey;">
                                        <img src="{{url('uploads/amenities/'.$row->logo)}}" style="height: 24px">
                                        <p class="mb-0 mt-2">{{ucfirst($row->name)}}</p>
                                    </div>
                                </div>
                            @endif
                        @endforeach -->

                        @foreach($amenities as $row)
                                <div class="col-md-3">
                                    <div class="facility-block" style="text-align: center;border: 1px solid grey;">
                                        <img src="{{url('uploads/amenities/'.$row['logo'])}}" style="height: 24px">
                                        <p class="mb-0 mt-2">{{ucfirst($row['name'])}}</p>
                                    </div>
                                </div>
                        @endforeach
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>


<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Description</div>
            </div>
            <div class="portlet-body">
                <div class="col-md-12">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td class="col-md-3">
                                    <strong>Home Description:</strong>
                                </td>
                                <td>
                                    {{$data->property_description ?? 0}}
                                </td>
                            </tr>

                            <tr>
                                <td class="col-md-3"> 
                                    <strong>Neighbourhood Description:</strong>
                                </td>
                                <td>
                                    {{$data->neighbourhood_description ?? 'N/A'}}
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Photos</div>
            </div>
            <div class="portlet-body">
                <div class="tab-container">
                    <p style="padding-left: 15px;" class="mb-4">
                        @foreach(@$data->files as $row)
                            <a class=" mb-4" rel="group" href="{{url('uploads/swap/'.$row)}}" style="display: inline-block;" target="_blank">
                                <img src="{{url('uploads/swap/'.$row)}}" alt="" style="width: 200px;height: 100px;" />
                            </a>
                        @endforeach
                    </ul>

                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>