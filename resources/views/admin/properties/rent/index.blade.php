@extends('admin.layouts.app')
@section('title', 'Properties')
@section('content')
    <style type="text/css">
    .modal-dialog{
        width: 800px;
    }
    .dataTables_empty{
        text-align: center !important;
    }
    .logo-td{
        padding-top: 24px !important;
    }
    .btn + .btn {
         margin-left: 0px !important; 
    }
    .width-50{
        width: 50% !important;
    }
    td {
        font-size:14px;
    }
    </style>
    <!-- BEGIN PAGE HEADER-->
    <div class="page-head">
        <div class="page-title">
            <h1>
                Rent Properties Listing
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <i class="fa fa-circle"></i>
            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
        </li>
    </ul>
    <!-- END PAGE HEADER-->
    <div class="portlet light md-shadow-z-2-i">
        
        <div class="portlet-body">
            @include('admin.properties.rent.table')
        </div>
    </div>
    <div class="clearfix"></div>
@endsection


@section('pagejs')

<script src="{{ asset('admin/js/sweetalert2@9.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    var filter_data = $("form[name=filter_listing]").serialize();
    var jqxhr = {abort: function () {  }};
    $(document).ready(function () {

        $(document).on('click','.delete-user', function(e){
            Swal.fire({
              title: 'Are you sure?',
              text: "You want to delete this user!",
              showCancelButton: true,
              confirmButtonText: 'Yes, Delete it!',
              cancelButtonText: 'No, Cancel!',
              reverseButtons: true
            }).then((result) => {
            if (result.value) {
                var id = $(this).data("id");
                $.ajax({
                    async : false,
                    url: "{{url('admin/property/delete')}}", //url
                    type: 'post', //request method
                    data: {
                       'id':id
                    },
                    success: function(data) {
                        if(data.status){
                            loadListings(full_path + '/properties/rent?page=', 'filter_listing');
                            show_FlashMessage(data.message,'success');
                        }else{
                            show_FlashMessage(data.message,'error');
                        }
                    },
                    error: function(xhr) {
                        
                    }
                });
            }
            });
        });

        $(document).on('keyup', '#name', function () {
            if($(this).val().length > 2)
            {
                loadListings(full_path + '/properties/rent?page=', 'filter_listing');
            }

            if($(this).val().length == 0)
            {
                loadListings(full_path + '/properties/rent?page=', 'filter_listing');
            }
        });

        $(document).on('change', '#filterStatus,#filterHouseType,#filterFurnishedType,#filterCountry,#approved,#populated', function () {
            loadListings(full_path + '/properties/rent?page=', 'filter_listing');
        });

        $(document).on("click", ".pagination li a", function (e){
            e.preventDefault();
            startLoader('.page-content');
            var url = $(this).attr('href');
            var page = url.split('page=')[1];   ;       
            loadListings(url, 'filter_listing');
        });

        function loadListings(url,filter_form_name){

            var filtering = $("form[name=filter_listing]").serialize();
            //abort previous ajax request if any
            jqxhr.abort();
            jqxhr =$.ajax({
                type : 'get',
                url : url,
                data : filtering,
                dataType : 'html',
                beforeSend:function(){
                    startLoader('.page-content');
                },
                success : function(data){
                    data = data.trim();
                    $("#dynamicContent").empty().html(data);
                },
                error : function(response){
                    stopLoader('.page-content');
                    //console.log('Error',"Unable to fetch the list");
                },
                complete:function(){
                    stopLoader('.page-content');
                }
            });
        }

        // reset form data
        $(document).on('click', '.filter-cancel', function (e) {
            e.preventDefault();
            $("form[name='filter_listing']")[0].reset();
            loadListings(full_path +'/properties/rent?page=', 'filter_listing');
        });
    });
</script>
<script src="{{ asset('admin/js/sweetalert2@9.js') }}" type="text/javascript"></script>
<script type="text/javascript">
  function changeStatus(obj,id)
  {
    var status = 0;
    var text = 'Demote';
    if($(obj).prop('checked') == true)
    {
      status = 1;
      text = 'Promote';
    }
    Swal.fire({
      title: text+"?",
      text: "Please ensure and then confirm!",
      showCancelButton: !0,
      confirmButtonText: "Yes, "+text+" it!",
      cancelButtonText: "No, cancel!",
      reverseButtons: !0
    }).then(function (e) {
      if (e.value) {
        $.ajax({
          type: 'GET',
          url: "{{url('admin/properties/rent/populate')}}/"+id+"?status="+status, 
          dataType: 'JSON',
          success: function (results) {
            if(results.success){
                show_FlashMessage(results.msg,'success');
                setTimeout(function(){ location.reload()}, 3000);
            }else{
                show_FlashMessage( results.msg, "error");
            }     
          }
        });
      }else{
        e.dismiss;
      }
    },
    function (dismiss) {
      return false;
    })
  }
  function approve(obj,id)
  {
    var status = 0;
    var text = 'Disapprove';
    if($(obj).prop('checked') == true)
    {
      status = 1;
      text = 'Approve';
    }
    Swal.fire({
      title: text+"?",
      text: "Please ensure and then confirm!",
      showCancelButton: !0,
      confirmButtonText: "Yes, "+text+" it!",
      cancelButtonText: "No, cancel!",
      reverseButtons: !0
    }).then(function (e) {
      if (e.value) {
        $.ajax({
          type: 'GET',
          url: "{{url('admin/properties/rent/approve')}}/"+id+"?status="+status, 
          dataType: 'JSON',
          success: function (results) {
            if(results.success){
                show_FlashMessage(results.msg,'success');
                setTimeout(function(){ location.reload()}, 3000);
            }else{
                show_FlashMessage( results.msg, "error");
            }     
          }
        });
      }else{
        e.dismiss;
      }
    },
    function (dismiss) {
      return false;
    })
  }
</script>
@endsection
