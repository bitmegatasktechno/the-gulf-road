<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Contact Details</div>
            </div>
            <div class="portlet-body">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td class="col-md-2">
                                    <strong>Email:</strong>
                                </td>
                                <td>
                                    {{ ucfirst($data->title) ?? 'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-2">
                                    <strong>Phone:</strong>
                                </td>
                                <td>
                                   {{ $data->contact_number ?? 'N/A'}}
                                </td>
                            </tr>

                            
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td class="col-md-2">
                                    <strong>Website URL:</strong>
                                </td>
                                <td>
                                   {{ $data->website_url ?? 'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-2">
                                    <strong>Facebook URL:</strong>
                                </td>
                                <td>
                                   {{ $data->facebook_url ?? 'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-2">
                                    <strong>Twitter URL:</strong>
                                </td>
                                <td>
                                   {{ $data->twitter_url ?? 'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-2">
                                    <strong>Instagram URL:</strong>
                                </td>
                                <td>
                                   {{ $data->instagram_url ?? 'N/A'}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Basic Details</div>
            </div>
            <div class="portlet-body">
                <div class="col-md-12">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td class="col-md-2">
                                    <strong>Title:</strong>
                                </td>
                                <td>
                                    {{ ucfirst($data->title) ?? 'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-2">
                                    <strong>Description:</strong>
                                </td>
                                <td>
                                   {{ $data->description ?? 'N/A'}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>


<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Location</div>
            </div>
            <div class="portlet-body">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>Address:</strong>
                                </td>
                                <td>
                                    {{ $data->address ?? 'N/A'}}
                                </td>
                            </tr><tr>
                                <td>
                                    <strong>Location Name:</strong>
                                </td>
                                <td>
                                    {{ $data->location_name ?? 'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Unit Number:</strong>
                                </td>
                                <td>
                                   {{ $data->unit_number ?? 'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Address Line 1:</strong>
                                </td>
                                <td>
                                   {{ $data->address_line_1 ?? 'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Address Line 2:</strong>
                                </td>
                                <td>
                                   {{ $data->address_line_2 ?? 'N/A'}}
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>City:</strong>
                                </td>
                                <td>
                                   {{ $data->city ?? 'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>State:</strong>
                                </td>
                                <td>
                                   {{ $data->state ?? 'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Country:</strong>
                                </td>
                                <td>
                                   {{ $data->country ?? 'N/A'}}
                                </td>
                            </tr>
                            
                            <tr>
                                <td>
                                    <strong>Zip Code:</strong>
                                </td>
                                <td>
                                   {{ $data->zip_code ?? 'N/A'}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>


<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Availability & Timings (24 Hours Format)</div>
            </div>
            <div class="portlet-body">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>Monday:</strong>
                                </td>
                                <td>
                                	@if($data->availability['monday']=='closed')
                                		<span class="label label-sm label-danger">Closed</span>
                                	@else
                                		<span class="label label-sm label-success">Open</span>

                                		{{$data->timings['monday_start_time']}} - {{$data->timings['monday_end_time']}}
                                	@endif
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Tuesday:</strong>
                                </td>
                                <td>
                                    @if($data->availability['tuesday']=='closed')
                                		<span class="label label-sm label-danger">Closed</span>
                                	@else
                                		<span class="label label-sm label-success">Open</span>

                                		{{$data->timings['tuesday_start_time']}} - {{$data->timings['tuesday_end_time']}}
                                	@endif
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Wednesday:</strong>
                                </td>
                                <td>
                                    @if($data->availability['wednesday']=='closed')
                                		<span class="label label-sm label-danger">Closed</span>
                                	@else
                                		<span class="label label-sm label-success">Open</span>

                                		{{$data->timings['wednesday_start_time']}} - {{$data->timings['wednesday_end_time']}}
                                	@endif
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Thursday:</strong>
                                </td>
                                <td>
                                	@if($data->availability['thursday']=='closed')
                                		<span class="label label-sm label-danger">Closed</span>
                                	@else
                                		<span class="label label-sm label-success">Open</span>

                                		{{$data->timings['thursday_start_time']}} - {{$data->timings['thursday_end_time']}}
                                	@endif
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            

                            <tr>
                                <td>
                                    <strong>Friday:</strong>
                                </td>
                                <td>
                                    @if($data->availability['friday']=='closed')
                                		<span class="label label-sm label-danger">Closed</span>
                                	@else
                                		<span class="label label-sm label-success">Open</span>

                                		{{$data->timings['friday_start_time']}} - {{$data->timings['friday_end_time']}}
                                	@endif
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Saturday:</strong>
                                </td>
                                <td>
                                    @if($data->availability['saturday']=='closed')
                                		<span class="label label-sm label-danger">Closed</span>
                                	@else
                                		<span class="label label-sm label-success">Open</span>

                                		{{$data->timings['saturday_start_time']}} - {{$data->timings['saturday_end_time']}}
                                	@endif
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Sunday:</strong>
                                </td>
                                <td>
                                    
                                	@if($data->availability['sunday']=='closed')
                                		<span class="label label-sm label-danger">Closed</span>
                                	@else
                                		<span class="label label-sm label-success">Open</span>

                                		{{$data->timings['sunday_start_time']}} - {{$data->timings['sunday_end_time']}}
                                	@endif
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>


<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Photos</div>
            </div>
            <div class="portlet-body">
                <div class="tab-container">
                    <p style="padding-left: 15px;">
                    	@foreach(@$data->files as $row)
                    		
                			<a class="fancybox" rel="group" href="{{url('uploads/coworkspce/'.$row)}}" style="display: inline-block;">
                				<img src="{{url('uploads/coworkspce/'.$row)}}" alt="" style="width: 200px;height: 100px;" />
                			</a>
                    		
                    	@endforeach
                    </ul>

                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>


<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Pricing</div>
            </div>
            <div class="portlet-body">
                <div class="col-md-12">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>Open Workspace</strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>Number of Seats:</strong>
                                </td>
                                <td colspan="2">
                                	{{$data->open_space_seats ?? 0}}
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <strong>Duration</strong>
                                </td>
                                <td>
                                	<strong>Price</strong>
                                	
                                </td>
                                <td>
                                	<strong>Currency</strong>
                                	
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Daily
                                </td>
                                <td>
                                	{{$data->open_space_daily_price ?? 0}}
                                </td>
                                <td>
                                	{{$data->open_space_daily_currency ?? 'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Weekly
                                </td>
                                <td>
                                	{{$data->open_space_weekly_price ?? 0}}
                                </td>
                                <td>
                                	{{$data->open_space_weekly_currency ?? 'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Monthly
                                </td>
                                <td>
                                	{{$data->open_space_monthly_price ?? 0}}
                                </td>
                                <td>
                                	{{$data->open_space_monthly_currency ?? 'N/A'}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="col-md-12">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>Dedicated Workspace - 
                                    	@if($data->dedicated_desk_available==1)
                                    		Available
                                    	@else
                                    		Not Available
                                    	@endif
                                    </strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                @if($data->dedicated_desk_available==1)
	                <div class="col-md-6">
	                    <table class="table table-bordered table-striped">
	                        <tbody>
	                            <tr>
	                                <td>
	                                    <strong>Number of Seats:</strong>
	                                </td>
	                                <td colspan="2">
	                                	{{$data->dedicated_space_seats ?? 0}}
	                                </td>
	                            </tr>

	                            <tr>
	                                <td>
	                                    <strong>Duration</strong>
	                                </td>
	                                <td>
	                                	<strong>Price</strong>
	                                	
	                                </td>
	                                <td>
	                                	<strong>Currency</strong>
	                                	
	                                </td>
	                            </tr>
	                            <tr>
	                                <td>
	                                    Daily
	                                </td>
	                                <td>
	                                	{{$data->dedicated_space_daily_price ?? 0}}
	                                </td>
	                                <td>
	                                	{{$data->dedicated_space_daily_currency ?? 'N/A'}}
	                                </td>
	                            </tr>
	                            <tr>
	                                <td>
	                                    Weekly
	                                </td>
	                                <td>
	                                	{{$data->dedicated_space_weekly_price ?? 0}}
	                                </td>
	                                <td>
	                                	{{$data->dedicated_space_weekly_currency ?? 'N/A'}}
	                                </td>
	                            </tr>
	                            <tr>
	                                <td>
	                                    Monthly
	                                </td>
	                                <td>
	                                	{{$data->dedicated_space_monthly_price ?? 0}}
	                                </td>
	                                <td>
	                                	{{$data->dedicated_space_monthly_currency ?? 'N/A'}}
	                                </td>
	                            </tr>
	                        </tbody>
	                    </table>
	                </div>
	            @endif

	            <div class="col-md-12">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>Private Workspace - 
                                    	@if($data->private_office_available==1)
                                    		Available
                                    	@else
                                    		Not Available
                                    	@endif
                                    </strong>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                @if($data->private_office_available==1)
	                <div class="col-md-6">
	                    <table class="table table-bordered table-striped">
	                        <tbody>
	                            <tr>
	                                <td>
	                                    <strong>Number of Seats:</strong>
	                                </td>
	                                <td colspan="2">
	                                	{{$data->seating_capacity ?? 0}}
	                                </td>
	                            </tr>
	                            <tr>
	                                <td>
	                                    <strong>Number of Private Offices:</strong>
	                                </td>
	                                <td colspan="2">
	                                	{{$data->no_of_private_office ?? 0}}
	                                </td>
	                            </tr>
	                            <tr>
	                                <td>
	                                    <strong>Does the office have a whiteboard ?</strong>
	                                </td>
	                                <td colspan="2">
	                                	{{($data->white_board_available==1) ? 'Yes' : 'No'}}
	                                </td>
	                            </tr>

	                            <tr>
	                                <td>
	                                    <strong>Duration</strong>
	                                </td>
	                                <td>
	                                	<strong>Price</strong>
	                                	
	                                </td>
	                                <td>
	                                	<strong>Currency</strong>
	                                	
	                                </td>
	                            </tr>
	                            <tr>
	                                <td>
	                                    Daily
	                                </td>
	                                <td>
	                                	{{$data->private_space_daily_price ?? 0}}
	                                </td>
	                                <td>
	                                	{{$data->private_space_daily_currency ?? 'N/A'}}
	                                </td>
	                            </tr>
	                            <tr>
	                                <td>
	                                    Weekly
	                                </td>
	                                <td>
	                                	{{$data->private_space_weekly_price ?? 0}}
	                                </td>
	                                <td>
	                                	{{$data->private_space_weekly_currency ?? 'N/A'}}
	                                </td>
	                            </tr>
	                            <tr>
	                                <td>
	                                    Monthly
	                                </td>
	                                <td>
	                                	{{$data->private_space_monthly_price ?? 0}}
	                                </td>
	                                <td>
	                                	{{$data->private_space_monthly_currency ?? 'N/A'}}
	                                </td>
	                            </tr>
	                        </tbody>
	                    </table>
	                </div>
	            @endif
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Food Amenities</div>
            </div>
            <div class="portlet-body">
                @php
                    $food_id = \App\Models\AmenityCategory::where('name','food')->value('_id');
                @endphp
                @foreach(getAllSpecialAmenities()->where('category_id',$food_id) as $row)
                    @if(in_array($row->_id, $data->food_amenties))
                        <div class="col-md-3" style="border: 1px solid lightgrey">
                            <div class="row mb-4">
                                <div class="col-md-12">
                                    <img class="img-fluid main-img" src="{{url('/uploads/specialamenities/'.$row->logo)}}" style="width:200px;height:80px;">
                                </div>
                                <div class="col-md-12">
                                    <strong>{{\Str::limit($row->name, 15)}}</strong>
                                </div>
                                <div class="col-md-12">
                                    <p>{{\Str::limit($row->description, 15)}}</p>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Facilities Amenities</div>
            </div>
            <div class="portlet-body">
                @php
                        $facility_id = \App\Models\AmenityCategory::where('name','facilities')->value('_id');
                    @endphp
                    @foreach(getAllSpecialAmenities()->where('category_id',$facility_id) as $row)
                        @if(in_array($row->_id, $data->facilities_amenties))
                           <div class="col-md-3" style="border: 1px solid lightgrey">
                                <div class="row mb-4">
                                    <div class="col-md-12">
                                        <img class="img-fluid main-img" src="{{url('/uploads/specialamenities/'.$row->logo)}}" style="width:200px;height:80px;">
                                    </div>
                                    <div class="col-md-12">
                                        <strong>{{\Str::limit($row->name, 15)}}</strong>
                                    </div>
                                    <div class="col-md-12">
                                        <p>{{\Str::limit($row->description, 15)}}</p>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>


<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Equipments Amenities</div>
            </div>
            <div class="portlet-body">
                @php
                        $equipments_id = \App\Models\AmenityCategory::where('name','equipments')->value('_id');
                    @endphp
                    @foreach(getAllSpecialAmenities()->where('category_id',$equipments_id) as $row)
                        @if(in_array($row->_id, $data->equipments_amenties))
                            <div class="col-md-3" style="border: 1px solid lightgrey">
                                <div class="row mb-4">
                                    <div class="col-md-12">
                                        <img class="img-fluid main-img" src="{{url('/uploads/specialamenities/'.$row->logo)}}" style="width:200px;height:80px;">
                                    </div>
                                    <div class="col-md-12">
                                        <strong>{{\Str::limit($row->name, 15)}}</strong>
                                    </div>
                                    <div class="col-md-12">
                                        <p>{{\Str::limit($row->description, 15)}}</p>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Accessibility Amenities</div>
            </div>
            <div class="portlet-body">
                @php
                    $accessibility_id = \App\Models\AmenityCategory::where('name','accessibility')->value('_id');
                @endphp
                @foreach(getAllSpecialAmenities()->where('category_id',$accessibility_id) as $row)
                    @if(in_array($row->_id, $data->accessibility_amenties))
                        <div class="col-md-3" style="border: 1px solid lightgrey">
                            <div class="row mb-4">
                                <div class="col-md-12">
                                    <img class="img-fluid main-img" src="{{url('/uploads/specialamenities/'.$row->logo)}}" style="width:200px;height:80px;">
                                </div>
                                <div class="col-md-12">
                                    <strong>{{\Str::limit($row->name, 15)}}</strong>
                                </div>
                                <div class="col-md-12">
                                    <p>{{\Str::limit($row->description, 15)}}</p>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>