@extends('admin.layouts.app')
@section('title', 'Coworkspace Details')
@section('css')
<link rel="stylesheet" href="{{asset('admin/css/jquery.fancybox.css')}}" type="text/css" media="screen" />

@endsection
@section('content')
    
    <!-- BEGIN PAGE HEADER-->
    <div class="page-head">
        <div class="page-title">
            <h1>
                Coworkspace Details
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <i class="fa fa-circle"></i>
            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
            <i class="fa fa-circle"></i>
            <a href="{{ url('admin/coworkspaces') }}">Coworkspace Listing</a>

        </li>
    </ul>
    <!-- END PAGE HEADER-->
    <div class="portlet light md-shadow-z-2-i">
        <div class="portlet-body">
            @include('admin.properties.coworkspace.details.section')
        </div>
    </div>
    <div class="clearfix"></div>
@endsection

@section('js')
    <script type="text/javascript" src="{{asset('admin/js/jquery.fancybox.pack.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".fancybox").fancybox({});
        });
    </script>
@endsection