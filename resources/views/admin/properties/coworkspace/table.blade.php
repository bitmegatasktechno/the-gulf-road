<form name="filter_listing">
	<table class="table table-striped table-bordered table-hover dataTable no-footer" id="datatable_ajax" aria-describedby="datatable_ajax_info" role="grid">
		<thead>
			<tr role="row" class="heading">
				<th width="5%" class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1">
					Sr. No
				</th>
				<th width="12%" class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1">
					Property Title
				</th>
				<th width="12%" class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1">
					House Type
				</th>
				<th width="12%" class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1">
					Furnishing Type
				</th>
				<th width="10%" class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1">
					Country
				</th>
				<th width="12%" class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1">
					User Email
				</th>
				<th width="12%" class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1">
					Premium
				</th>
				<th width="10%" class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1">
					Homepicked Home
				</th>
				<th width="10%" class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1">
					Approved
				</th>
				<th width="10%" class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1">
					Actions
				</th>
				<th width="10%" class="sorting" tabindex="0" aria-controls="datatable_ajax" rowspan="1" colspan="1">
					Created At
				</th>
			</tr>
			<tr role="row" class="filter">
				<td rowspan="1" colspan="1"></td>
				<td rowspan="1" colspan="1">
					<input type="text" class="form-control form-filter input-sm" name="name" id="name" placeholder="Search Name">
				</td>
				<td rowspan="1" colspan="1">
					<select name="filterHouseType" class="form-control" id="filterHouseType">
						<option value="">Select</option>
						@foreach(getAllHouseTypes() as $row)
                            <option value="{{strtolower($row->name)}}">{{ucfirst($row->name)}}</option>
                        @endforeach
					</select>
				</td>
				<td rowspan="1" colspan="1">
					<select name="filterFurnishedType" class="form-control" id="filterFurnishedType">
						<option value="">Select</option>
						<option value="semi_furnished">Semi Furnished</option>
						<option value="fully_furnished">Fully Furnished</option>
						<option value="unfurnished">Unfurnished</option>
					</select>
				</td>

				<td rowspan="1" colspan="1">
					<select name="filterCountry" class="form-control" id="filterCountry">
						<option value="">Select</option>
						@foreach(getAllLocations() as $subrow)
							<option value="{{$subrow->name}}"> {{$subrow->name}}</option>
						@endforeach
					</select>
				</td>
				<td rowspan="1" colspan="1">
					
				</td>
				<td rowspan="1" colspan="1">
					
				</td>
				<td rowspan="1" colspan="1">
					<select name="populated" class="form-control" id="populated">
						<option value="">Select</option>
						<option value="1">Yes</option>
						<option value="0">No</option>
					</select>
				</td>
				<td rowspan="1" colspan="1">
					<select name="approved" class="form-control" id="approved">
						<option value="">Select</option>
						<option value="1">Yes</option>
						<option value="0">No</option>
					</select>
				</td>
				<td rowspan="1" colspan="1">
					<button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset</button>
				</td>
				<td rowspan="1" colspan="1">
					
				</td>
			</tr>
		</thead>
		<tbody id="dynamicContent">
			@include('admin.properties.coworkspace.listing')
		</tbody>
	</table>
</form>