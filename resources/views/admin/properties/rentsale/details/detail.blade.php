<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Basic Details</div>
            </div>
            <div class="portlet-body">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>Property Listing Type:</strong>
                                </td>
                                <td>
                                    {{ ucfirst($data->listing_type) ?? 'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>House Type:</strong>
                                </td>
                                <td>
                                   {{ ucfirst($data->house_type) ?? 'N/A'}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
                        <tr>
                            <td>
                                <strong>Location:</strong>
                            </td>
                            <td>
                                {{ ($data->location_name) ?? 'N/A'}}
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <strong>House Number:</strong>
                            </td>
                            <td>
                                {{ ($data->house_number) ?? 'N/A'}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>LandMark:</strong>
                            </td>
                            <td>
                                {{ ($data->landmark) ?? 'N/A'}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Country:</strong>
                            </td>
                            <td>
                                {{ ($data->country) ?? 'N/A'}}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>

        </div>

    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Dimensions Details</div>
            </div>
            <div class="portlet-body">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>Build up area:</strong>
                                </td>
                                <td>
                                    {{ $data->build_up_area ?? 'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Carpet area:</strong>
                                </td>
                                <td>
                                   {{ $data->carpet_area ?? 'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>No of bedrooms:</strong>
                                </td>
                                <td>
                                   {{ $data->no_of_bedrooms ?? 'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>No of bathrooms:</strong>
                                </td>
                                <td>
                                   {{ $data->no_of_bathrooms ?? 'N/A'}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
                        <tr>
                            <td>
                                <strong>Furnishing:</strong>
                            </td>
                            <td>
                                {{ ucfirst(str_replace('_',' ', $data->furnishing_type)) ?? 'N/A'}}
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <strong>Flooring type:</strong>
                            </td>
                            <td>
                                {{ ($data->flooring_type) ?? 'N/A'}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Is There space for parking?</strong>
                            </td>
                            <td>
                                {{ ucfirst($data->parking_available) ?? 'N/A'}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>No of covered parkings:</strong>
                            </td>
                            <td>
                                {{ ($data->no_of_covered_parking) ?? 'N/A'}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>No of open parkings:</strong>
                            </td>
                            <td>
                                {{ ($data->no_of_open_parking) ?? 'N/A'}}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Amenities</div>
            </div>
            <div class="portlet-body">
                @php
                    $amenities = $data->amenties;
                    if(!is_array($amenities)){
                        $amenities = [];
                    }
                @endphp
                <div class="row">
                    @foreach(getAllAmenities() as $row)
                        @if(in_array($row->_id, $amenities))
                            <div class="col-md-3">
                                <div class="facility-block" style="text-align: center;border: 1px solid grey;">
                                    <img src="{{url('uploads/amenities/'.$row->logo)}}">
                                    <p class="mb-0 mt-2">{{ucfirst($row->name)}}</p>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
                
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>

        </div>

    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Description & Rules</div>
            </div>
            <div class="portlet-body">
                <div class="col-md-12">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td class="col-md-3">
                                    <strong>Property Title:</strong>
                                </td>
                                <td>
                                    {{ $data->property_title ?? 'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-3">
                                    <strong>Property Description:</strong>
                                </td>
                                <td>
                                   {{ $data->property_description ?? 'N/A'}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
                        <tr>
                            <td>
                                <strong>Is Smoking allowed inside the house?</strong>
                            </td>
                            <td>
                                {{ ucfirst($data->smoking_allowed) ?? 'N/A'}}
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <strong>Are Parties Allowed?</strong>
                            </td>
                            <td>
                                {{ ucfirst($data->parties_allowed) ?? 'N/A'}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Are Pets Allowed?</strong>
                            </td>
                            <td>
                                {{ ucfirst($data->pets_allowed) ?? 'N/A'}}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Photos</div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    @foreach($data->files as $row)
                        <div class="upload col-md-3">
                            <a target="_blank" href="{{url('uploads/properties/'.$row)}}">
                            <label for="files1">
                                <input type="hidden" name="already_exit_pic[]" value="{{$row}}">
                                <img id="files1_src" class="already_exit_pic" data-name="{{$row}}" src="{{url('uploads/properties/'.$row)}}" style="height:120px;width:120px;">
                            </label>
                            </a>
                        </div>
                    @endforeach
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">Prices</div>
            </div>
            <div class="portlet-body">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td >
                                    <strong>Price:</strong>
                                </td>
                                <td>
                                    {{ $data->price ? number_format($data->price) : 'N/A'}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Discount Available:</strong>
                                </td>
                                <td>
                                    @if($data->discount_available)
                                        Yes
                                    @else
                                        No
                                    @endif
                                </td>
                            </tr>
                            @if($data->discount_available)
                            <tr>
                                <td>
                                    <strong>Price After Discount:</strong>
                                </td>
                                <td>
                                    {{$data->discount_price}}
                                </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="clearfix"></div>