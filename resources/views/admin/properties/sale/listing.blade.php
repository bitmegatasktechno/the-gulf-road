@if(count($data)>0)
@php
$i= ($data->currentPage() - 1) * $data->perPage() + 1;
@endphp

@foreach($data as $row)
<tr>
	<td>{{$i}}</td>
	<td>{{$row->property_title ? $row->property_title : 'N/A'}}</td>
	<td>{{$row->house_type ? $row->house_type : 'N/A'}}</td>
	<td>
		{{$row->furnishing_type ? ucfirst(str_replace('_',' ', $row->furnishing_type)) : 'N/A'}}
	</td>
	<td>
		{{$row->country ? $row->country : 'N/A'}}
	</td>
	<td>
		{{isset($row->user['email']) ? $row->user['email'] : 'N/A'}}
	</td>
	<td>
		{{$row->is_premium ? 'Premium' : 'Not Premium'}}
	</td>
	<td>
		<label class="switch">
            <input type="checkbox" {{$row->is_populated == 1 ? 'checked' : ''}} onchange="changeStatus(this,'{{$row->id}}')">
            <span class="slider round"></span>
        </label>
	</td>
	<td>
		<label class="switch">
            <input type="checkbox" {{$row->is_approved == 1 ? 'checked' : ''}} onchange="approve(this,'{{$row->id}}')">
            <span class="slider round"></span>
        </label>
	</td>
	<td>
		<div class="actions">
			<div class="btn-group">
				<a class="label label-primary dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="true">
					Actions <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-right">
					<li>
						<a href="{{url('admin/property-details/sale/'.$row->_id)}}" class="view">
							<i class="icon-eye"></i> View
						</a>
					</li>
					<li>
						<a class="view delete-user" data-id="{{$row->_id}}">
							<i class="icon-trash"></i> Delete
						</a>
					</li>
				
				</ul>
			</div>
		</div>
	</td>
	<td>{{$row->created_at->setTimezone(config('constants.TIMEZONE'))->format('F d, Y h:i a')}}</td>
</tr>
@php $i++; @endphp
@endforeach
<tr>
	<td colspan="10">{{ $data->links() }}</td>
</tr>
@else
<tr>
	<td colspan="10">No Data Available</td>
</tr>
@endif