@if(count($data)>0)
	@php
	$i= 1;
	@endphp

	@foreach($data as $row)
	<tr>
		<td>{{@$i}}</td>
		<td>{{@$row->user->name ? @$row->user->name : 'N/A'}}</td>
		<td>{{@$row->review ? @$row->review : 'N/A'}}</td>

		<td>@if(@$row->review_type==1){{'Buy'}} @elseif(@$row->review_type==2) {{'Rent'}}
		@elseif(@$row->review_type==3){{'Swap'}}@elseif(@$row->review_type==4){{'CoSpace'}}@elseif(@$row->review_type==6){{'Agent'}}
	@elseif(@$row->review_type==7){{'Buddy'}} @endif</td>
		<td>@if(@$row->review_type==1)<a href="{{url('admin/property-details/sale/'.$row->type_id)}}">{{@$row->buy->property_title}}</a>
			 @elseif(@$row->review_type==2) <a href="{{url('admin/property-details/rent/'.$row->type_id)}}">{{@$row->buy->property_title}}</a>
		@elseif(@$row->review_type==3)<a href="{{url('admin/swap/detail/'.$row->type_id)}}">{{@$row->swap->title}}</a>
		@elseif(@$row->review_type==4)<a href="{{url('admin/coworkspace/detail/'.$row->type_id)}}">{{@$row->co->title}}</a>
		@elseif(@$row->review_type==6)<a href="{{url('admin/user/detail/'.$row->type_id)}}">{{@$row->user_type->name}}</a>
	@elseif(@$row->review_type==7)<a href="{{url('admin/user/detail/'.$row->type_id)}}">{{@$row->user_type->name}}</a> @endif</td>
		<td>
			@if(@$row->is_approved==1)
				<a class="label label-sm label-success " data-status="0" data-id="{{$row->id}}">Published</a>
			@else
				<a class="label label-sm label-danger" data-status="1" data-id="{{$row->id}}">UnPublished</a>
			@endif
		</td>
		<td>
			<div class="actions">
				<div class="btn-group">
					<a class="label label-primary dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="true">
						Actions <i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu pull-right">

						<li>
							@if(@$row->is_approved==1)
								<a class="view change-status" data-status="0" data-id="{{$row->_id}}">
									<i class="icon-eye"></i> UnPublish
								</a>
							@else
								<a class="view change-status" data-status="1" data-id="{{$row->_id}}">
									<i class="icon-eye"></i> Publish
								</a>
							@endif
						</li>
					</ul>
				</div>
			</div>
		</td>
	</tr>
	@php $i++; @endphp
	@endforeach
@endif
