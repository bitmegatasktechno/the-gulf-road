<!DOCTYPE html>

<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Login - {{ config('app.name', 'Laravel') }}</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="{{ asset('admin/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('admin/css/simple-line-icons.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('admin/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('admin/css/uniform.default.css') }}" rel="stylesheet" type="text/css"/>

<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="{{ asset('admin/css/login.css') }}" rel="stylesheet" type="text/css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="{{asset('admin/global/css/components-rounded.css')}}" id="style_components" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin/global/css/plugins.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin/admin/layout/css/layout.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin/admin/layout/css/themes/default.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{asset('admin/admin/layout/css/custom.css')}}" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<!-- <link rel="shortcut icon" href="favicon.ico"/> -->
    <link rel="shortcut icon" href="{{asset('img/ic_favi.png')}}"/>

<style type="text/css">
    .help-block{
        color: red;
    }
</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login" style="background-image: url('../assets/img/bg_image.png');background-repeat: no-repeat;
    background-size: cover;">
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="javascript:;">
    <img src="{{asset('/img/logo.png')}}" alt="Logo"/>
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    
    <!-- BEGIN LOGIN FORM -->
    <form method="POST" action="{{ route('admin.login') }}" class="login-form">
        @csrf        
        <h3 class="form-title">Sign In</h3>

        @include('flash::message')
        
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Email</label>
            <input class="form-control @error('email') is-invalid @enderror form-control-solid placeholder-no-fix" type="text" autocomplete="off" value="{{ old('email') }}" placeholder="Email" name="email"/>

            @error('email')
                <span id="email-error" class="help-block"> {{ $message }} </span>
            @enderror
        </div>

        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="password-field position-relative">
                <input class="form-control @error('password') is-invalid @enderror form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" id="password"/>
                <i class="fa fa-eye position-absolute" aria-hidden="true" onclick="changePasswordType('1')" id="eye_icon"></i>
            </div>

            
            @error('password')
                <span id="email-error" class="help-block"> {{ $message }} </span>
            @enderror
        </div>

        <div class="form-actions">
            <button type="submit" class="btn btn-success uppercase">Login</button>
            <label class="rememberme check">
        </div>
    </form>
    
    <!-- END REGISTRATION FORM -->
</div>
<div class="copyright">
     {{date('Y')}} © All Copyright Reserved.
</div>
<!-- END LOGIN -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../admin/global/plugins/respond.min.js"></script>
<script src="../../admin/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="{{ asset('admin/js/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/js/jquery-migrate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/js/jquery.blockui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/js/jquery.uniform.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/plugins/jquery.cokie.min.js') }}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset('admin/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('admin/js/metronic.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/js/layout.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
<script src="{{ asset('admin/admin/pages/scripts/login.js') }}" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    function changePasswordType(check)
    {
        if(check == '1')
        {
            $('#password').prop('type', 'text');
            $('#eye_icon').prop('class','fa  fa-eye-slash position-absolute');
            $('#eye_icon').attr('onclick','changePasswordType("2")');
        }else{
            $('#password').prop('type', 'password');
            $('#eye_icon').prop('class','fa  fa-eye position-absolute');
            $('#eye_icon').attr('onclick','changePasswordType("1")');
        }
    }
    jQuery(document).ready(function() {     
    Metronic.init(); // init metronic core components
    Layout.init(); // init current layout
    Login.init();
    Demo.init();
    
});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>