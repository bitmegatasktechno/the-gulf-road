@if(count($data)>0)
	@php
	$i= 1;
	@endphp

	@foreach($data as $row)
	<tr>
		<td>{{$i}}</td>
		<td>@if(isset($row->user->name)) {{$row->user->name ? $row->user->name : 'N/A'}} @endif</td>
        <td>.{{@$row->subscription->name ? @$row->subscription->name : 'N/A'}}</td>
        <td>
        	@if($row->purchase_type == 'rent')
        	{{'Rent & Buy '}} 
        	@elseif($row->purchase_type == 'swap')
        	{{'Swap '}} 
        	@elseif($row->purchase_type == 'cospace')
        	{{'Co Space '}} 
        	@elseif($row->purchase_type == 'user')
        	{{'Users '}}
        	@elseif($row->purchase_type == 'buddy')
        	{{'Buddy'}}
        	@elseif($row->purchase_type == 'agent')
        	{{'Agent'}} 
        	@elseif($row->purchase_type == 'premium')
        	{{'Premium'}} 
        	@endif
        </td>
        <td>${{$row->amount ? $row->amount : 'N/A'}}</td>
        <td>{{date('d M Y', strtotime(@$row->purchase_date))}}</td>
        <td>{{date('d M Y', strtotime(@$row->expiry_date))}}</td>
        <td>.{{$row->validity ? $row->validity  : 'N/A'}}</td>
		<td>{{$row->count ? $row->count : 'N/A'}}</td>
		<!--<td><a href="{{route('property_listing',['plan_id'=>@$row->plan_id,'type'=>@$row->purchase_type])}}">{{$row->listed ? $row->listed : 'N/A'}}</a></td> -->
		
	</tr>
	@php $i++; @endphp
	@endforeach
@endif