@extends('admin.layouts.app')
@section('title', 'Revenue')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('admin/css/jquery.dataTables.min.css')}}">
@endsection
@section('content')
    <style type="text/css">
    .modal-dialog{
        width: 800px;
    }
    .dataTables_empty{
        text-align: center !important;
    }
    .logo-td{
        padding-top: 24px !important;
    }
    .btn + .btn {
         margin-left: 0px !important; 
    }
    .width-50{
        width: 50% !important;
    }
    td {
        font-size:14px;
    }
    </style>
    <!-- BEGIN PAGE HEADER-->
 <!-- <div class="page-head">
        <div class="page-title">
            <h1>
                Manage Revenue
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <i class="fa fa-circle"></i>
            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
        </li>
    </ul> -->
    <!-- END PAGE HEADER-->
    <div class="portlet light md-shadow-z-2-i">
        <div class="portlet-title">
        <div class = "row">

<div class= "col-md-4">
  <div class="pl-3 pr-3">
    <div class="form-group">
        <label for="exampleSelectInfo">Type</label>
        <select class="form-control border-info" id="filter4">
            <option value="e">All</option>
            <option value="Rent & Buy">Rent & Buy</option>
            <option value="Swap">Swap</option>
            <option value="Co Space">Co Space</option>
            <option value="Users">Users</option>
        </select>
    </div>
    </div>
</div>

<div class= "col-md-4">
  <div class="pl-3 pr-3">
    <div class="form-group">
        <label for="exampleSelectInfo">Plan Name</label>
        <select id="sel_plan" class="form-control border-info">
            <option value="e">- Select -</option>
        </select>
    </div>
    </div>
</div>

<div class= "col-md-4">
  <div class="pl-3 pr-3">
    <div class="form-group">
        <label for="exampleSelectInfo">Plan Validity</label>
        <select id="sel_val" class="form-control border-info">
            <option value="e">- Select -</option>
        </select> 
    </div>
    </div> 
</div>

<div class= "col-md-4">


  <div class="form-group">
          <label for="exampleSelectInfo">Filter</label>
          <form  id = "filter_form" method = "get" action = "{{route('revenue')}}">
<input type = "hidden" name = "start_date" id = "start_date">
<input type = "hidden" name = "end_date" id = "end_date">
 <input date-range-picker id="daterange" name="daterange" value="{{date('m/d/Y',strtotime(@$start_date))}} - {{date('m/d/Y',strtotime(@$end_date))}} " class="form-control date-picker border-info" type="text" clearable="true" options="dateRangeOptions" />
 </form>
      </div>
</div>
<div class= "col-md-4" style="margin-top: 31px;">
<a href="{{route('revenue')}}" class="reset">Reset</a>

</div>
<div class= "col-md-4" style="margin-top: 31px;">
Total Revenue : $<label id="total"></label>

</div>
</div>
        </div>
        <div class="portlet-body">
            @include('admin.revenue.table')
        </div>
    </div>
    <div id="myModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog" style="max-width: 40%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="text-align: center;">Add Location</h4>
                </div>
                <div class="modal-body">
                    <form method="post" name="add_form" id="add_form" onsubmit="addForm()">
                        @csrf
                        
                        <div class="form-group name">
                            <label for="recipient-name" class="col-form-label">Name :</label>
                            <input type="text" class="form-control" name="name" required="true" > 
                            <span class="error"></span>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary add_form_btn">Save</button>
                </div>
            </div>
        </div>
    </div>


    <div id="editmyModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" style="max-width: 40%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="text-align: center;">Edit Location</h4>
                </div>
                <div class="modal-body">
                    <form method="post" name="edit_form" id="edit_form" onsubmit="editForm()">
                        @csrf
                        <input type="hidden" name="_method" value="put">
                        <input type="hidden" name="id" value="" id="hidden_id">

                        <div class="form-group name">
                            <label for="recipient-name" class="col-form-label">Name :</label>
                            <input type="text" class="form-control" name="name" required="true" > 
                            <span class="error"></span>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary edit_form_btn">Save</button>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
@endsection


@section('pagejs')


<script src="{{ asset('admin/js/sweetalert2@9.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('admin/js/jquery.dataTables.min.js')}}"></script>

  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="https://datatables.net/media/css/site-examples.css">
<!-- buttons -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
  <script src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum().js"></script>

  
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


<script type="text/javascript">
if ($.fn.DataTable.isDataTable('#datatable_ajax1')) {
      $('#datatable_ajax1').DataTable().destroy();
}



 
    $(document).ready(function () {
        $('#myModal,#editmyModal').on('hidden.bs.modal', function (e) {
            $('#add_form')[0].reset();
            $('#edit_form')[0].reset();
            $(document).find('span.error').empty().hide();
        });

        // add sub category
        $(document).on('click','.add_location', function(){
            $('#myModal').modal('show');
        });

        $(document).on('click','.edit_btn', function(){
            var location = $(this).data('location');
            var id = $(this).data('id');
            $('#editmyModal').find('input[name="id"]').val(id);
            $('#editmyModal').find('input[name="name"]').val(location);
            $('#editmyModal').modal('show');
        });


        $('#add_form').on('submit', function(e){
            e.preventDefault();
            addForm();
        });

         $('#edit_form').on('submit', function(e){
            e.preventDefault();
            editForm();
        });


        // add sub category
        $(document).on('click','.add_form_btn', function(e){
            e.preventDefault();
            addForm();
        });

        $(document).on('click','.edit_form_btn', function(e){
            e.preventDefault();
            editForm();
        });

        function addForm(){
            $(document).find('span.error').empty().hide();

            var allData = new FormData($('#add_form')[0]);
            $.ajax({
                async : false,
                url: "{{url('admin/locations')}}", //url
                type: 'post', //request method
                data: allData,
                processData: false,  // Important!
                contentType: false,
                beforeSend:function(){
                    startLoader('.modal-content');
                },
                complete:function(){
                    stopLoader('.modal-content');
                },
                success: function(data) {
                    if(data.status){
                        show_FlashMessage(data.message,'success');
                        setTimeout(function(){ 
                            window.location.reload(); 
                        }, 1000);
                    }else{
                        stopLoader('.portlet');
                        show_FlashMessage(data.message,'error');
                    }
                },
                error: function(error){
                    
                    if(error.status == 0 || error.readyState == 0) {
                        return;
                    }
                    else if(error.status == 401){
                        errors = $.parseJSON(error.responseText);
                        window.location = errors.redirectTo;
                    }
                    else if(error.status == 422) {
                        errors = error.responseJSON;
                        $.each(errors.errors, function(key, value) {
                            if(key.indexOf('.') != -1) {
                                let keys = key.split('.');
                                /*let keys_length = keys.length;*/
                                $('.'+keys[0]+'_'+keys[1]).find('span.error').empty().addClass('text-danger').text(value).finish().fadeIn();
                            }
                            else {
                                $('.'+key).find('span.error').empty().addClass('text-danger').text(value).finish().fadeIn();
                            }
                        });
                        
                    }
                    else if(error.status == 400) {
                        errors = error.responseJSON;
                        if(errors.hasOwnProperty('message')) {
                            show_FlashMessage(errors.message, 'error');
                        }
                        else {
                            show_FlashMessage('Something went wrong!', 'error');
                        }
                    }
                    else {
                        show_FlashMessage('Something went wrong!', 'error');
                    }
                    //stop ajax loader
                    stopLoader('.portlet');
                }
            });
        }

        function editForm(){
            $(document).find('span.error').empty().hide();
            var formdata = new FormData($('#edit_form')[0]);
            var id = $('#edit_form').find('input[name="id"]').val();

            $.ajax({
                async : false,
                url: "{{url('admin/locations')}}"+'/'+id, //url
                type: 'post', //request method
                data: formdata,
                processData: false,  // Important!
                contentType: false,
                beforeSend:function(){
                    startLoader('.modal-content');
                },
                complete:function(){
                    stopLoader('.modal-content');
                },
                success: function(data) {
                    if(data.status){
                        show_FlashMessage(data.message,'success');
                        setTimeout(function(){ 
                            window.location.reload(); 
                        }, 1000);
                    }else{
                        show_FlashMessage(data.message,'error');
                        return false;
                    }
                },
                error: function(error){
                    
                    if(error.status == 0 || error.readyState == 0) {
                        return;
                    }
                    else if(error.status == 401){
                        errors = $.parseJSON(error.responseText);
                        window.location = errors.redirectTo;
                    }
                    else if(error.status == 422) {
                        errors = error.responseJSON;
                        $.each(errors.errors, function(key, value) {
                            if(key.indexOf('.') != -1) {
                                let keys = key.split('.');
                                /*let keys_length = keys.length;*/
                                $('.'+keys[0]+'_'+keys[1]).find('span.error').empty().addClass('text-danger').text(value).finish().fadeIn();
                            }
                            else {
                                $('.'+key).find('span.error').empty().addClass('text-danger').text(value).finish().fadeIn();
                            }
                        });
                        
                    }
                    else if(error.status == 400) {
                        errors = error.responseJSON;
                        if(errors.hasOwnProperty('message')) {
                            show_FlashMessage(errors.message, 'error');
                        }
                        else {
                            show_FlashMessage('Something went wrong!', 'error');
                        }
                    }
                    else {
                        show_FlashMessage('Something went wrong!', 'error');
                    }
                    //stop ajax loader
                    stopLoader('.portlet');
                }
            });
        }

        $(document).on('click','.change-status', function(e){
            Swal.fire({
              title: 'Are you sure?',
              text: "You want to change status!",
              showCancelButton: true,
              confirmButtonText: 'Yes, change it!',
              cancelButtonText: 'No, cancel!',
              reverseButtons: true
            }).then((result) => {
            if (result.value) {
                var id = $(this).data("id");
                var status = $(this).data("status");
                $.ajax({
                    async : false,
                    url: 'location/status', //url
                    type: 'post', //request method
                    data: {'status':status,'update':'status','id':id},
                    success: function(data) {
                        if(data.status){
                            show_FlashMessage(data.message,'success');
                            setTimeout(function(){ window.location.reload() }, 1000);
                        }else{
                            show_FlashMessage(data.message,'error');
                        }
                    },
                    error: function(xhr) {
                        show_FlashMessage(xhr.message,'error');
                    }
                });
            }
            });
        });

        $(document).on('click','.delete-record', function(e){
            Swal.fire({
              title: 'Are you sure?',
              text: "You want to delete this!",
              showCancelButton: true,
              confirmButtonText: 'Yes, delete it!',
              cancelButtonText: 'No, cancel!',
              reverseButtons: true
            }).then((result) => {
            if (result.value) {
                var id = $(this).data("id");
                $.ajax({
                    async : false,
                    url: "{{url('admin/locations')}}"+'/'+id, //url
                    type: 'post', //request method
                    data: {'id':id,'_method':'delete'},
                    success: function(data) {
                        if(data.status){
                            show_FlashMessage(data.message,'success');
                            setTimeout(function(){ window.location.reload() }, 1000);
                        }else{
                            show_FlashMessage(data.message,'error');
                        }
                    },
                    error: function(xhr) {
                        
                    }
                });
            }
            });
        });
    });


</script>
<script type="text/javascript">

$(document).ready(function() {
  $('#datatable_ajax1').DataTable({

    dom: 'Bfrtip',
    "buttons": [
      'excel'
    ]
  });
});
$(document).ready(function() {
      var sum = $('#datatable_ajax1').DataTable().column(4).data().sum();
      $('#total').html(sum);
    });
</script>
<script>

$(document).ready(function()
            {
                $('input[name="daterange"]').daterangepicker({
                    format: 'DD/MM/YYYY',
                    minDate: '10/01/2020',
                    maxDate: moment().format('DD/MM/YYYY'),
                    opens: 'center',
                    drops: 'down',
                    buttonClasses: ['btn', 'btn-sm'],
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-default'
                        });

                $('#daterange').on('apply.daterangepicker', function(ev, picker)
                    {
                        var startDate = picker.startDate.format('YYYY-MM-DD');
                        var endDate = picker.endDate.format('YYYY-MM-DD');
                        $('#start_date').val(startDate);
                        $('#end_date').val(endDate);
                        $('#filter_form').submit();

                    });

            });

        $('#reportrange span').html('{{$start_date}}' + ' - ' + '{{$end_date}}');

      $(document).ready(function(){

$("#filter4").change(function(){
    var type = $(this).val();
    $.ajax({
        url: "{{url('admin/select_plan')}}",
        type: "get",
        data: {type:type},
        dataType: 'json',
        success:function(response){

            var len = response.users_arr;
            var le = len.length;
            $("#sel_plan").empty();
            $("#sel_val").empty();
            $("#sel_val").append("<option value='.'>All</option>");
            $("#sel_plan").append("<option value='.'>All</option>");
            for( var i = 0; i<le; i++){
                var name = len[i]['name'];
                
                $("#sel_plan").append("<option value='"+name+"'>"+name+"</option>");

            }
        }
    });
});

$("#sel_plan").change(function(){
    var name = $(this).val();
    var type = $("#filter4").val();
    $.ajax({
        url: "{{url('admin/select_val')}}",
        type: "get",
        data: {name:name,type:type},
        dataType: 'json',
        success:function(response){

            var len = response.users_arr1;
            var le = len.length;
            $("#sel_val").empty();
            $("#sel_val").append("<option value='.'>All</option>");
            for( var i = 0; i<le; i++){
                var name = len[i]['name'];
                
                $("#sel_val").append("<option value='"+name+"'>"+name+"Days</option>");

            }
        }
    });
});

});
  $("#filter4").on('change', function()
  {
    $('#datatable_ajax1').DataTable().destroy();
    recreateDataTable();
    $('#datatable_ajax1').DataTable().columns(3).search(this.value).draw();
    
  });

  $("#sel_plan").on('change', function()
  {
    $('#datatable_ajax1').DataTable().destroy();
    recreateDataTable();
    $('#datatable_ajax1').DataTable().columns(2).search(this.value).draw();
  });

    $("#sel_val").on('change', function()
  {
    $('#datatable_ajax1').DataTable().destroy();
    recreateDataTable();
    $('#datatable_ajax1').DataTable().columns(7).search(this.value).draw();

  });


function recreateDataTable(){
  $('#datatable_ajax1').DataTable({
    dom: 'Bfrtip',
    "buttons": [
      'excel'
    ],
    drawCallback: function () {
        var api = this.api();
        var sum  = api.column( 4, { page: 'current'} ).data().sum();
        $('#total').html(sum);
    }
  });
}


</script>
@endsection
