<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">

            <div class="portlet-title">
                <div class="caption">Professional details</div>
            </div>

            <div class="portlet-body">
                <div class="col-md-12">
                    <table class="table table-bordered table-striped">
                        <tbody>
	                        <tr>
	                            <td class="col-md-2">
	                                <strong>Languages</strong>
	                            </td>
	                            <td>
	                            	@if(isset($user->agentProfessional->languages) && is_array($user->agentProfessional->languages))
		                                @foreach($user->agentProfessional->languages as $row)
		                                	<span class="badge badge-success">{{$row}}</span>
		                                @endforeach
		                            @else
		                            	<span class="badge badge-success">N/A</span>
		                            @endif
	                            </td>
                        	</tr>
                        	<tr>
	                            <td class="col-md-2">
	                                <strong>Localities</strong>
	                            </td>
	                            <td>
	                            	@if(isset($user->agentProfessional->localities) && is_array($user->agentProfessional->localities))
		                                @foreach($user->agentProfessional->localities as $row)
		                                	<span class="badge badge-success">{{$row}}</span>
		                                @endforeach
	                                @else
		                            	<span class="badge badge-success">N/A</span>
		                            @endif
	                            </td>
                        	</tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
	                        <tr>
	                            <td>
	                                <strong>Are you a Real Estate Agent?</strong>
	                            </td>
	                            <td>
	                                {{ $user->agentProfessional->real_estate_agent ?? 'N/A'}}
	                            </td>
	                        </tr>
	                        @if($user->agentProfessional->real_estate_agent=='yes')
	                        	<tr>
		                            <td>
		                                <strong>RERA No:</strong>
		                            </td>
		                            <td>
		                                {{ $user->agentProfessional->rera_number ?? 'N/A'}}
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                                <strong>Permit No:</strong>
		                            </td>
		                            <td>
		                                {{ $user->agentProfessional->permit_number ?? 'N/A'}}
		                            </td>
		                        </tr>

		                        <tr>
		                            <td>
		                                <strong>Deal properties Inside/Outside UAE:</strong>
		                            </td>
		                            <td>
		                                {{ $user->agentProfessional->deal_property_in_uae ?? 'N/A'}}
		                            </td>
		                        </tr>
	                        @endif
	                        
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <table class="table table-bordered table-striped">
                        <tbody>
	                        <tr>
	                            <td>
	                                <strong>Timings & Availability</strong>
	                            </td>
	                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <table class="table table-bordered">
					    <thead>
					      	<tr>
						        <th>Day</th>
						        <th>Available</th>
						        <th>Start Time (24 Hr Format)</th>
						        <th>End Time (24 Hr Format)</th>
					      	</tr>
					    </thead>
					    <tbody>
					      	<tr>
						        <td>Sunday</td>
						        <td>{{($user->agentProfessional->availability['sunday']==1) ? 'YES' : 'NO'}}</td>
						        <td>{{$user->agentProfessional->timing['sunday_start_time']}}</td>
						        <td>{{$user->agentProfessional->timing['sunday_end_time']}}</td>
					      	</tr>

					      	<tr>
						        <td>Monday</td>
						        <td>{{($user->agentProfessional->availability['monday']==1) ? 'YES' : 'NO'}}</td>
						        <td>{{$user->agentProfessional->timing['monday_start_time']}}</td>
						        <td>{{$user->agentProfessional->timing['monday_end_time']}}</td>
					      	</tr>

					      	<tr>
						        <td>Tuesday</td>
						        <td>{{($user->agentProfessional->availability['tuesday']==1) ? 'YES' : 'NO'}}</td>
						        <td>{{$user->agentProfessional->timing['tuesday_start_time']}}</td>
						        <td>{{$user->agentProfessional->timing['tuesday_end_time']}}</td>
					      	</tr>

					      	<tr>
						        <td>Wednesday</td>
						        <td>{{($user->agentProfessional->availability['wednesday']==1) ? 'YES' : 'NO'}}</td>
						        <td>{{$user->agentProfessional->timing['wednesday_start_time']}}</td>
						        <td>{{$user->agentProfessional->timing['wednesday_end_time']}}</td>
					      	</tr>
					      	<tr>
						        <td>Thursday</td>
						        <td>{{($user->agentProfessional->availability['thursday']==1) ? 'YES' : 'NO'}}</td>
						        <td>{{$user->agentProfessional->timing['thursday_start_time']}}</td>
						        <td>{{$user->agentProfessional->timing['thursday_end_time']}}</td>
					      	</tr>

					      	<tr>
						        <td>Friday</td>
						        <td>{{($user->agentProfessional->availability['friday']==1) ? 'YES' : 'NO'}}</td>
						        <td>{{$user->agentProfessional->timing['friday_start_time']}}</td>
						        <td>{{$user->agentProfessional->timing['friday_end_time']}}</td>
					      	</tr>

					      	<tr>
						        <td>Saturday</td>
						        <td>{{($user->agentProfessional->availability['saturday']==1) ? 'YES' : 'NO'}}</td>
						        <td>{{$user->agentProfessional->timing['saturday_start_time']}}</td>
						        <td>{{$user->agentProfessional->timing['saturday_end_time']}}</td>
					      	</tr>
					    </tbody>
					</table>
                </div>
                
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>

        </div>

    </div>
</div>
