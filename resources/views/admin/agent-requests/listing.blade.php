@if(count($users)>0)
@php
$i= ($users->currentPage() - 1) * $users->perPage() + 1;
@endphp

@foreach($users as $user)
<tr>
	<td>{{$i}}</td>
	<td>{{$user->name ? $user->name : 'N/A'}}</td>
	<td>{{$user->email ? $user->email : 'N/A'}}</td>
	<td>
		{{$user->phone ? $user->phone : 'N/A'}}
	</td>
	<td>
		<a class="label label-sm label-danger">Pending</a>
	</td>
	<td>
		<div class="actions">
			<div class="btn-group">
				<a class="label label-primary dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="true">
					Actions <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-right">
					<li>
						<a href="{{url('admin/agent-application/'.$user->_id)}}" class="view">
							<i class="icon-eye"></i> View
						</a>
					</li>
					<li>
						<a class="view change-status" data-status="1" data-id="{{$user->_id}}">
							<i class="icon-info"></i> Approve Application
						</a>
					</li>
				</ul>
			</div>
		</div>
	</td>
	<td>{{$user->created_at->setTimezone(config('constants.TIMEZONE'))->format('F d, Y h:i a')}}</td>
</tr>
@php $i++; @endphp
@endforeach
<tr>
	<td colspan="8">{{ $users->links() }}</td>
</tr>
@else
<tr>
	<td colspan="8">No Data Available</td>
</tr>
@endif