@extends('admin.layouts.app')
@section('title', 'Agent Application Details')
@section('content')
    
    <!-- BEGIN PAGE HEADER-->
    <div class="page-head">
        <div class="page-title">
            <h1>
                Agent Application Details
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <i class="fa fa-circle"></i>
            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
            <i class="fa fa-circle"></i>
            <a href="{{ url('admin/applications/agents') }}">Agents Applications Listing</a>

        </li>
    </ul>
    <!-- END PAGE HEADER-->
    <div class="portlet light md-shadow-z-2-i">

        <div class="portlet-body">
            @include('admin.user-detail-common.basic')
            @include('admin.user-detail-common.additional')
            @include('admin.user-detail-common.address')
            @include('admin.agent-requests.details.professional')
            @include('admin.user-detail-common.qualification')
        </div>
    </div>
    <div class="clearfix"></div>
@endsection


@section('pagejs')
@endsection
