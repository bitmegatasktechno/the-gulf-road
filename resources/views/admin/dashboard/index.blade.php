@extends('admin.layouts.app')
@section('title','Dashboard')
@section('content')
<!-- BEGIN PAGE HEADER-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(linechart);

      function linechart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Users', 'Agents','Buddies'],
          @foreach($lineGraphpData as $key => $val)

            ['{{$key}}',{{isset($val['users']) ? count($val['users']) : 0}},{{isset($val['agents']) ? count($val['agents']) : 0}},{{isset($val['buddies']) ? count($val['buddies']) : 0 }}],
          @endforeach
        ]);

        var options = {
          title: 'Users Registration',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>

<script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
          ['Date', 'Bookings'],
          @foreach($bookingDatewise as $key => $val)
            ["{{$key}}", {{ count($val) }} ],
          @endforeach
        ]);

        var options = {
          width: 800,
          legend: { position: 'none' },
          chart: {
                title: 'Bookings'
            }
        };

        var chart = new google.charts.Bar(document.getElementById('columnchart_values'));
        // Convert the Classic options to Material options.
        chart.draw(data, google.charts.Bar.convertOptions(options));
      };
    </script>


    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(pieChart);

      function pieChart() {

        var data = google.visualization.arrayToDataTable([
          ['Property', 'Count'],
          ['Rent',{{$rentCount}}],
          ['Buy',{{$buyCount}}],
          ['Swap',{{$swapCount}}],
          ['Coworkspace',{{$coworkCount}}],
        ]);

        var options = {
          title: 'Properties'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>

<div class="page-head">
    <div class="page-title">
        <h1 style="font-weight: 600;font-size: 28px">Dashboard</h1>
    </div>
</div>
<!-- END PAGE HEADER-->
<!-- BEGIN DASHBOARD STATS -->
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
        <div class="dashboard-stat blue-madison">
            <div class="visual">
                <i class="icon-user"></i>
            </div>
            <div class="details">
                <div class="number">
                   {{@$data['users']}}
                </div>
                <div class="desc">
                    Total Users
                </div>
            </div>
            <a class="more" href="{{url('admin/users')}}">
            View more <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat red-intense">
            <div class="visual">
                <i class="fa fa-user-secret"></i>
            </div>
            <div class="details">
                <div class="number">
                     {{@$data['agents']}}
                </div>
                <div class="desc">
                     Total Agents
                </div>
            </div>
            <a class="more" href="{{url('admin/agents')}}">
            View more <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat green-haze">
            <div class="visual">
                <i class="fa fa-user-plus"></i>
            </div>
            <div class="details">
                <div class="number">
                    {{@$data['buddy']}}
                </div>
                <div class="desc">
                     Total Buddies
                </div>
            </div>
            <a class="more" href="{{url('admin/buddies')}}">
            View more <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat purple-plum">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    {{@$data['agent-requests']}}
                </div>
                <div class="desc">
                    Pending Agents Requests
                </div>
            </div>
            <a class="more" href="{{url('admin/applications/agents')}}">
            View more <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat purple-plum">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    {{@$data['buddy-requests']}}
                </div>
                <div class="desc">
                    Pending Buddies Requests
                </div>
            </div>
            <a class="more" href="{{url('admin/applications/buddy')}}">
            View more <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
        <div class="dashboard-stat blue-madison">
            <div class="visual">
                <i class="icon-home"></i>
            </div>
            <div class="details">
                <div class="number">
                   {{getTotalSaleProperties()}}
                </div>
                <div class="desc">
                    Total Properties Sale
                </div>
            </div>
            <a class="more" href="{{url('admin/properties/sale')}}">
            View more <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
        <div class="dashboard-stat red-intense">
            <div class="visual">
                <i class="icon-home"></i>
            </div>
            <div class="details">
                <div class="number">
                   {{getTotalRentProperties()}}
                </div>
                <div class="desc">
                    Total Properties Rent
                </div>
            </div>
            <a class="more" href="{{url('admin/properties/rent')}}">
            View more <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
        <div class="dashboard-stat red-intense">
            <div class="visual">
                <i class="icon-home"></i>
            </div>
            <div class="details">
                <div class="number">
                   {{$rentBuyCount}}
                </div>
                <div class="desc">
                    Total Properties Rent + Sale
                </div>
            </div>
            <a class="more" href="{{url('admin/properties/rent')}}">
            View more <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
        <div class="dashboard-stat red-intense">
            <div class="visual">
                <i class="fa fa-building"></i>
            </div>
            <div class="details">
                <div class="number">
                   {{$coworkCount}}
                </div>
                <div class="desc">
                    Total Coworkspace Properties
                </div>
            </div>
            <a class="more" href="{{url('admin/coworkspaces')}}">
                View more <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
        <div class="dashboard-stat red-intense">
            <div class="visual">
                <i class="fa fa-building"></i>
            </div>
            <div class="details">
                <div class="number">
                   {{$swapCount}}
                </div>
                <div class="desc">
                    Total Swap Properties
                </div>
            </div>
            <a class="more" href="{{url('admin/swap')}}">
                View more <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-md-12 col-sm-12">
		<!-- BEGIN REGIONAL STATS PORTLET-->
		<div class="portlet light ">
			<div class="portlet-title">
				<div class="caption caption-md">
					<i class="icon-bar-chart theme-font-color hide"></i>
					<span class="caption-subject theme-font-color bold uppercase">Statistics</span>
				</div>
				<div class="actions" style="display: none;">
					<div class="btn-toolbar margin-bottom-10">
						<div class="btn-group pull-right">
							<a href="" class="btn btn-circle grey-salsa btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							Select Region <span class="fa fa-angle-down">
							</span>
							</a>
							<ul class="dropdown-menu pull-right">
								@foreach(getAllLocations() as $row)
									<li>
										<a href="javascript:;">
											{{$row->name}}
										</a>
									</li>
								@endforeach
								
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="portlet-body row">
                <div class="col-sm-12">
                    <div id="curve_chart" style="width: 100%; height: 400px;"></div>
                    
                </div>
                <div class="col-sm-6">
                
                    <div id="columnchart_values" style="width: 100%; height: 400px;"></div>
                </div>
                <div class="col-sm-6">
                
                    <div id="piechart" style="width: 100%; height: 500px;"></div>
                </div>
			</div>
			</div>
		</div>
		<!-- END REGIONAL STATS PORTLET-->
	</div>
</div>
<!-- END DASHBOARD STATS -->
<div class="clearfix">
</div>
@endsection
@section('pagejs')

<script type="text/javascript">
    var map;
   	function initMap() {
        map = new google.maps.Map(document.getElementById('region_statistics_content'), {
			center: {lat: -34.397, lng: 150.644},
			zoom: 8,
        });
        var heatmap = new google.maps.visualization.HeatmapLayer({
		  	data: [
		  		new google.maps.LatLng(37.782551, -122.445368),
		  	]
		});
		heatmap.setMap(heatmap.getMap() ? null : map);
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC0duKE_WA4QVvM1U5ZSCZ5dHLQqYywM_0&callback=initMap&libraries=visualization"></script>
@endsection