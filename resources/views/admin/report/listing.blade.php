@if(count($data)>0)
	@php
	$i= 1;
	@endphp
	@foreach($data as $row)


	<tr>
		<td>{{@$i}}</td>

		<td>{{@$row->user->name ? @$row->user->name : 'N/A'}}</td>
		<td>{{@$row->user_type ?? 'N/A'}}</td>
		<td>{{@$row->user->countryCode ? @$row->user->countryCode : ''}}{{@$row->user->phone ? @$row->user->phone : 'N/A'}}</td>
		<td>{{@$row->user->email ? @$row->user->email : 'N/A'}}</td>
		<td>{{@$row->reason ?? 'N/A'}}</td>
		<td>{{@$row->message ?? 'N/A'}}</td>
		<td>{{@$row->type ? @$row->type : 'N/A'}}</td>
		<td>{{@$row->property()}}</td>
	</tr>
	@php $i++; @endphp
	@endforeach
@endif
