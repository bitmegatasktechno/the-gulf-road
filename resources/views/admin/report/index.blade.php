@extends('admin.layouts.app')
@section('title', 'Reports')
@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('admin/css/jquery.dataTables.min.css')}}">
@endsection
@section('content')
    <style type="text/css">
    .modal-dialog{
        width: 800px;
    }
    .dataTables_empty{
        text-align: center !important;
    }
    .logo-td{
        padding-top: 24px !important;
    }
    .btn + .btn {
         margin-left: 0px !important;
    }
    .width-50{
        width: 50% !important;
    }
    td {
        font-size:14px;
    }
    </style>
    <!-- BEGIN PAGE HEADER-->
    <div class="page-head">
        <div class="page-title">
            <h1>
                Manage Reports
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <i class="fa fa-circle"></i>
            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
        </li>
    </ul>
    <!-- END PAGE HEADER-->
    <div class="portlet light md-shadow-z-2-i">
       <!--  <div class="portlet-title">
            <div class="caption">
                <a href="javascript:;" class="btn  btn-primary pull-right black add_location" style="margin-top: 4px;margin-right: 5px;"><i class="fa fa-plus"></i>&nbsp;&nbsp; Add Language</a>
            </div>
        </div> -->
        <div class="portlet-body">
            @include('admin.report.table')
        </div>
    </div>
    <div class="clearfix"></div>
@endsection


@section('pagejs')

<script src="{{ asset('admin/js/sweetalert2@9.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('admin/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript">
    $('#datatable_ajax').DataTable();
    var jqxhr = {abort: function () {  }};

    $(document).ready(function () {
        $('#myModal,#editmyModal').on('hidden.bs.modal', function (e) {
            $('#add_form')[0].reset();
            $('#edit_form')[0].reset();
            $(document).find('span.error').empty().hide();
        });

        // add sub category
        $(document).on('click','.add_location', function(){
            $('#myModal').modal('show');
        });

        $(document).on('click','.edit_btn', function(){
            var name = $(this).data('name');
            var id = $(this).data('id');
            $('#editmyModal').find('input[name="id"]').val(id);
            $('#editmyModal').find('input[name="name"]').val(name);
            $('#editmyModal').modal('show');
        });


        $('#add_form').on('submit', function(e){
            e.preventDefault();
            addForm();
        });

         $('#edit_form').on('submit', function(e){
            e.preventDefault();
            editForm();
        });


        // add sub category
        $(document).on('click','.add_form_btn', function(e){
            e.preventDefault();
            addForm();
        });

        $(document).on('click','.edit_form_btn', function(e){
            e.preventDefault();
            editForm();
        });

        function addForm(){
            $(document).find('span.error').empty().hide();

            var allData = new FormData($('#add_form')[0]);
            $.ajax({
                async : false,
                url: "{{url('admin/reviews')}}", //url
                type: 'post', //request method
                data: allData,
                processData: false,  // Important!
                contentType: false,
                beforeSend:function(){
                    startLoader('.modal-content');
                },
                complete:function(){
                    stopLoader('.modal-content');
                },
                success: function(data) {
                    if(data.status){
                        show_FlashMessage(data.message,'success');
                        setTimeout(function(){
                            window.location.reload();
                        }, 1000);
                    }else{
                        stopLoader('.portlet');
                        show_FlashMessage(data.message,'error');
                    }
                },
                error: function(error){

                    if(error.status == 0 || error.readyState == 0) {
                        return;
                    }
                    else if(error.status == 401){
                        errors = $.parseJSON(error.responseText);
                        window.location = errors.redirectTo;
                    }
                    else if(error.status == 422) {
                        errors = error.responseJSON;
                        $.each(errors.errors, function(key, value) {
                            if(key.indexOf('.') != -1) {
                                let keys = key.split('.');
                                /*let keys_length = keys.length;*/
                                $('.'+keys[0]+'_'+keys[1]).find('span.error').empty().addClass('text-danger').text(value).finish().fadeIn();
                            }
                            else {
                                $('.'+key).find('span.error').empty().addClass('text-danger').text(value).finish().fadeIn();
                            }
                        });

                    }
                    else if(error.status == 400) {
                        errors = error.responseJSON;
                        if(errors.hasOwnProperty('message')) {
                            show_FlashMessage(errors.message, 'error');
                        }
                        else {
                            show_FlashMessage('Something went wrong!', 'error');
                        }
                    }
                    else {
                        show_FlashMessage('Something went wrong!', 'error');
                    }
                    //stop ajax loader
                    stopLoader('.portlet');
                }
            });
        }

        function editForm(){
            $(document).find('span.error').empty().hide();
            var formdata = new FormData($('#edit_form')[0]);
            var id = $('#edit_form').find('input[name="id"]').val();

            $.ajax({
                async : false,
                url: "{{url('admin/languages')}}"+'/'+id, //url
                type: 'post', //request method
                data: formdata,
                processData: false,  // Important!
                contentType: false,
                beforeSend:function(){
                    startLoader('.modal-content');
                },
                complete:function(){
                    stopLoader('.modal-content');
                },
                success: function(data) {
                    if(data.status){
                        show_FlashMessage(data.message,'success');
                        setTimeout(function(){
                            window.location.reload();
                        }, 1000);
                    }else{
                        show_FlashMessage(data.message,'error');
                        return false;
                    }
                },
                error: function(error){

                    if(error.status == 0 || error.readyState == 0) {
                        return;
                    }
                    else if(error.status == 401){
                        errors = $.parseJSON(error.responseText);
                        window.location = errors.redirectTo;
                    }
                    else if(error.status == 422) {
                        errors = error.responseJSON;
                        $.each(errors.errors, function(key, value) {
                            if(key.indexOf('.') != -1) {
                                let keys = key.split('.');
                                /*let keys_length = keys.length;*/
                                $('.'+keys[0]+'_'+keys[1]).find('span.error').empty().addClass('text-danger').text(value).finish().fadeIn();
                            }
                            else {
                                $('.'+key).find('span.error').empty().addClass('text-danger').text(value).finish().fadeIn();
                            }
                        });

                    }
                    else if(error.status == 400) {
                        errors = error.responseJSON;
                        if(errors.hasOwnProperty('message')) {
                            show_FlashMessage(errors.message, 'error');
                        }
                        else {
                            show_FlashMessage('Something went wrong!', 'error');
                        }
                    }
                    else {
                        show_FlashMessage('Something went wrong!', 'error');
                    }
                    //stop ajax loader
                    stopLoader('.portlet');
                }
            });
        }

        $(document).on('click','.change-status', function(e){
            Swal.fire({
              title: 'Are you sure?',
              text: "You want to change status!",
              showCancelButton: true,
              confirmButtonText: 'Yes, change it!',
              cancelButtonText: 'No, cancel!',
              reverseButtons: true
            }).then((result) => {
            if (result.value) {
                var id = $(this).data("id");
                var status = $(this).data("status");
                $.ajax({
                    async : false,
                    url: 'reviews/status', //url
                    type: 'post', //request method
                    data: {'status':status,'update':'status','id':id},
                    success: function(data) {
                        if(data.status){
                            show_FlashMessage(data.message,'success');
                            setTimeout(function(){ window.location.reload() }, 1000);
                        }else{
                            show_FlashMessage(data.message,'error');
                        }
                    },
                    error: function(xhr) {
                        show_FlashMessage(xhr.message,'error');
                    }
                });
            }
            });
        });

        $(document).on('click','.delete-record', function(e){
            Swal.fire({
              title: 'Are you sure?',
              text: "You want to delete this!",
              showCancelButton: true,
              confirmButtonText: 'Yes, delete it!',
              cancelButtonText: 'No, cancel!',
              reverseButtons: true
            }).then((result) => {
            if (result.value) {
                var id = $(this).data("id");
                $.ajax({
                    async : false,
                    url: "{{url('admin/languages')}}"+'/'+id, //url
                    type: 'post', //request method
                    data: {'id':id,'_method':'delete'},
                    success: function(data) {
                        if(data.status){
                            show_FlashMessage(data.message,'success');
                            setTimeout(function(){ window.location.reload() }, 1000);
                        }else{
                            show_FlashMessage(data.message,'error');
                        }
                    },
                    error: function(xhr) {

                    }
                });
            }
            });
        });
    });
</script>
@endsection
