<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">

            <div class="portlet-title">
                <div class="caption">Professional details</div>
            </div>

            <div class="portlet-body">
                <div class="col-md-12">
                    <table class="table table-bordered table-striped">
                        <tbody>
	                        <tr>
	                            <td class="col-md-2">
	                                <strong>Languages</strong>
	                            </td>
	                            <td>
	                                @foreach($user->buddyProfessional->languages as $row)
	                                	<span class="badge badge-success">{{$row}}</span>
	                                @endforeach
	                            </td>
                        	</tr>
                        	<tr>
	                            <td class="col-md-2">
	                                <strong>Localities</strong>
	                            </td>
	                            <td>
	                                @foreach($user->buddyProfessional->localities as $row)
	                                	<span class="badge badge-success">{{$row}}</span>
	                                @endforeach
	                            </td>
                        	</tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
	                        <tr>
	                            <td>
	                                <strong>Are you a Real Estate Agent?</strong>
	                            </td>
	                            <td>
	                                {{ $user->buddyProfessional->real_estate_agent ?? 'N/A'}}
	                            </td>
	                        </tr>
	                        @if($user->buddyProfessional->real_estate_agent=='yes')
	                        	<tr>
		                            <td>
		                                <strong>RERA No:</strong>
		                            </td>
		                            <td>
		                                {{ $user->buddyProfessional->rera_number ?? 'N/A'}}
		                            </td>
		                        </tr>
		                        <tr>
		                            <td>
		                                <strong>Permit No:</strong>
		                            </td>
		                            <td>
		                                {{ $user->buddyProfessional->permit_number ?? 'N/A'}}
		                            </td>
		                        </tr>

		                        <tr>
		                            <td>
		                                <strong>Deal properties Inside/Outside UAE:</strong>
		                            </td>
		                            <td>
		                                {{ $user->buddyProfessional->deal_property_in_uae ?? 'N/A'}}
		                            </td>
		                        </tr>
	                        @endif
	                        
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <table class="table table-bordered table-striped">
                        <tbody>
	                        <tr>
	                            <td>
	                                <strong>Timings & Availability</strong>
	                            </td>
	                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12">
                    <table class="table table-bordered">
					    <thead>
					      	<tr>
						        <th>Day</th>
						        <th>Available</th>
						        <th>Start Time (24 Hr Format)</th>
						        <th>End Time (24 Hr Format)</th>
					      	</tr>
					    </thead>
					    <tbody>
					      	<tr>
						        <td>Sunday</td>
						        <td>{{($user->buddyProfessional->availability['sunday']==1) ? 'YES' : 'NO'}}</td>
						        <td>{{$user->buddyProfessional->timing['sunday_start_time']}}</td>
						        <td>{{$user->buddyProfessional->timing['sunday_end_time']}}</td>
					      	</tr>

					      	<tr>
						        <td>Monday</td>
						        <td>{{($user->buddyProfessional->availability['monday']==1) ? 'YES' : 'NO'}}</td>
						        <td>{{$user->buddyProfessional->timing['monday_start_time']}}</td>
						        <td>{{$user->buddyProfessional->timing['monday_end_time']}}</td>
					      	</tr>

					      	<tr>
						        <td>Tuesday</td>
						        <td>{{($user->buddyProfessional->availability['tuesday']==1) ? 'YES' : 'NO'}}</td>
						        <td>{{$user->buddyProfessional->timing['tuesday_start_time']}}</td>
						        <td>{{$user->buddyProfessional->timing['tuesday_end_time']}}</td>
					      	</tr>

					      	<tr>
						        <td>Wednesday</td>
						        <td>{{($user->buddyProfessional->availability['wednesday']==1) ? 'YES' : 'NO'}}</td>
						        <td>{{$user->buddyProfessional->timing['wednesday_start_time']}}</td>
						        <td>{{$user->buddyProfessional->timing['wednesday_end_time']}}</td>
					      	</tr>
					      	<tr>
						        <td>Thursday</td>
						        <td>{{($user->buddyProfessional->availability['thursday']==1) ? 'YES' : 'NO'}}</td>
						        <td>{{$user->buddyProfessional->timing['thursday_start_time']}}</td>
						        <td>{{$user->buddyProfessional->timing['thursday_end_time']}}</td>
					      	</tr>

					      	<tr>
						        <td>Friday</td>
						        <td>{{($user->buddyProfessional->availability['friday']==1) ? 'YES' : 'NO'}}</td>
						        <td>{{$user->buddyProfessional->timing['friday_start_time']}}</td>
						        <td>{{$user->buddyProfessional->timing['friday_end_time']}}</td>
					      	</tr>

					      	<tr>
						        <td>Saturday</td>
						        <td>{{($user->buddyProfessional->availability['saturday']==1) ? 'YES' : 'NO'}}</td>
						        <td>{{$user->buddyProfessional->timing['saturday_start_time']}}</td>
						        <td>{{$user->buddyProfessional->timing['saturday_end_time']}}</td>
					      	</tr>
					    </tbody>
					</table>
                </div>
                
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>

        </div>

    </div>
</div>
