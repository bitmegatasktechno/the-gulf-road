@extends('admin.layouts.app')
@section('title', 'User Details')
@section('content')
    
    <!-- BEGIN PAGE HEADER-->
    <div class="page-head">
        <div class="page-title">
            <h1>
                User Details
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <i class="fa fa-circle"></i>
            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
            <i class="fa fa-circle"></i>
            <a href="{{ url('admin/users') }}">Users Listing</a>

        </li>
    </ul>
    <!-- END PAGE HEADER-->
    <div class="portlet light md-shadow-z-2-i">
        <!-- <div class="portlet-title">
            <div class="caption">
                <a href="{{ asset( 'handset.csv') }}" class="btn  btn-primary pull-right black" style="margin-top: 4px;margin-right: 5px;"><i class="fa fa-cloud-download"></i>&nbsp;&nbsp;Download CSV</a>
            </div>
        </div> -->
        <div class="portlet-body">
            @include('admin.users.details.detail')
        </div>
    </div>
    <div class="clearfix"></div>
@endsection


@section('pagejs')
@endsection
