<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue" id="stage_show">
            <div class="portlet-title">
                <div class="caption ">
                    Status
                </div>
            </div>
            <div class="portlet-body stage-col-main">
                <div class="col-md-3">
                    <div class="general-item-list">
                        <div class="item">
                            <div class="item-head">
                                <div class="item-details">
                                    Default Logged-In Role
                                </div>
                            </div>
                            <div class="item-body">
                                <span class="badge badge-success">{{$user->defaultLoginRole ?? 'N/A'}}</span></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="general-item-list">
                        <div class="item">
                            <div class="item-head">
                                <div class="item-details">
                                    Available Roles
                                </div>
                            </div>
                            <div class="item-body">
                                @if(isset($user->role))
                                    @foreach($user->role as $key=>$value)
                                        <span class="badge badge-success">
                                            {{$value}}
                                        </span>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue">

            <div class="portlet-title">
                <div class="caption">Basic Informations</div>
            </div>

            <div class="portlet-body">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
                        <tr>
                            <td>
                                <strong>Gender:</strong>
                            </td>
                            <td>
                                {{ $user->gender ? $user->gender : 'N/A'}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Full Name:</strong>
                            </td>
                            <td>
                                {{ $user->name ?? 'N/A'}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Email:</strong>
                            </td>
                            <td>
                                {{ $user->email ?? 'N/A'}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Country Code:</strong>
                            </td>
                            <td>
                                {{ $user->countryCode ?? 'N/A'}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Country :</strong>
                            </td>
                            <td>
                                {{ $user->country ?? 'N/A'}}
                            </td>
                        </tr>
                        
                        
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tbody>
                        <tr>
                            <td>
                                <strong>Phone:</strong>
                            </td>
                            <td>
                                {{ (isset($user->phone) && $user->phone!='') ? $user->phone : 'N/A'}}
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <strong>Register Via:</strong>
                            </td>
                            <td>
                                {{ $user->registerVia ?? 'N/A'}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Father Name:</strong>
                            </td>
                            <td>
                                {{ $user->father_name ?? 'N/A'}}
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <strong>Date Of Birth:</strong>
                            </td>
                            <td>
                                {{ $user->date_of_birth ?? 'N/A'}}
                            </td>
                        </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>

                <div class="col-md-12">
                    <table class="table table-bordered table-striped">
                        <tbody>
                        
                        <tr>
                            <td class="col-md-2">
                                <strong>Address :</strong>
                            </td>
                            <td>
                                {{ $user->address ?? 'N/A'}}
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-2">
                                <strong>About :</strong>
                            </td>
                            <td>
                                {{ $user->about ?? 'N/A'}}
                            </td>
                        </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>

        </div>

    </div>
</div>

<div class="clearfix"></div>

