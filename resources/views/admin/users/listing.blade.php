@if(count($users)>0)
@php
$i= ($users->currentPage() - 1) * $users->perPage() + 1;
@endphp

@foreach($users as $user)
<tr>
	<td>{{$i}}</td>
	<td>{{$user->name ? $user->name : 'N/A'}}</td>
	<td>{{$user->email ? $user->email : 'N/A'}}</td>
	<td>
		{{$user->phone ? $user->phone : 'N/A'}}
	</td>
	<td>
		@if($user->status)
			<a class="label label-sm label-success" data-status="0" data-id="{{$user->_id}}">Enabled</a>
		@else
			<a class="label label-sm label-danger" data-status="1" data-id="{{$user->_id}}">Disabled</a>
		@endif
	</td>
	<td>
		<div class="actions">
			<div class="btn-group">
				<a class="label label-primary dropdown-toggle" href="javascript:;" data-toggle="dropdown" aria-expanded="true">
					Actions <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-right">
					<li>
						<a href="{{url('admin/user/detail/'.$user->_id)}}" class="view">
							<i class="icon-eye"></i> View
						</a>
					</li>

					

					<li>
						@if($user->status)
							<a class="view change-status" data-status="0" data-id="{{$user->_id}}">
								<i class="icon-info"></i> Change Status
							</a>
						@else
							<a class="view change-status" data-status="1" data-id="{{$user->_id}}">
								<i class="icon-info"></i> Change Status
							</a>
						@endif
					</li>

					<li>
						<a class="view delete-user" data-id="{{$user->_id}}">
								<i class="icon-trash"></i> Delete
							</a>
					</li>
				</ul>
			</div>
		</div>
	</td>
	<td>{{$user->created_at->setTimezone(config('constants.TIMEZONE'))->format('F d, Y h:i a')}}</td>
</tr>
@php $i++; @endphp
@endforeach
<tr>
	<td colspan="8">{{ $users->links() }}</td>
</tr>
@else
<tr>
	<td colspan="8">No Data Available</td>
</tr>
@endif