@extends('admin.layouts.app')
@section('title', 'Contact')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('admin/css/jquery.dataTables.min.css')}}">
@endsection
@section('content')
    <style type="text/css">
    .modal-dialog{
        width: 800px;
    }
    .dataTables_empty{
        text-align: center !important;
    }
    .logo-td{
        padding-top: 24px !important;
    }
    .btn + .btn {
         margin-left: 0px !important; 
    }
    .width-50{
        width: 50% !important;
    }
    td {
        font-size:14px;
    }
    </style>
    <!-- BEGIN PAGE HEADER-->
    <div class="page-head">
        <div class="page-title">
            <h1>
                Contact
            </h1>
        </div>
    </div>
    <ul class="page-breadcrumb breadcrumb">
        <li>
            <i class="fa fa-circle"></i>
            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
        </li>
    </ul>
    <!-- END PAGE HEADER-->
    <div class="portlet light md-shadow-z-2-i">
        <div class="portlet-body">
            @include('admin.contact.table')
        </div>
    </div>


    <div id="editmyModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" style="max-width: 40%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="text-align: center;">Edit Currency</h4>
                </div>
                <div class="modal-body">
                    <form method="post" name="edit_form" id="edit_form" onsubmit="editForm()">
                        @csrf
                        <input type="hidden" name="_method" value="put">
                        <input type="hidden" name="id" value="" id="hidden_id">

                        <div class="form-group code">
                            <label for="recipient-name" class="col-form-label">Code :</label>
                            <input type="text" class="form-control" name="code" required="true" > 
                            <span class="error"></span>
                        </div>
                        <div class="form-group name">
                            <label for="recipient-name" class="col-form-label">Name :</label>
                            <input type="text" class="form-control" name="name" required="true" > 
                            <span class="error"></span>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary edit_form_btn">Save</button>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
@endsection


@section('pagejs')

<script src="{{ asset('admin/js/sweetalert2@9.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('admin/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript">

    $('#datatable_ajax').DataTable();
    
    var jqxhr = {abort: function () {  }};

    $(document).ready(function () {
        $('#myModal,#editmyModal').on('hidden.bs.modal', function (e) {
            $('#add_form')[0].reset();
            $('#edit_form')[0].reset();
            $(document).find('span.error').empty().hide();
        });

        $(document).on('click','.edit_btn', function(){
            var name = $(this).data('name');
            var code = $(this).data('code');
            var id = $(this).data('id');
            $('#editmyModal').find('input[name="id"]').val(id);
            $('#editmyModal').find('input[name="name"]').val(name);
            $('#editmyModal').find('input[name="code"]').val(code);
            $('#editmyModal').modal('show');
        });

    });
</script>
@endsection
