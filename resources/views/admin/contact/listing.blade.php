@if(count($data)>0)
	@php
	$i= 1;
	@endphp

	@foreach($data as $row)
	<tr>
		<td>{{$i}}</td>
		<td>{{$row->name ? $row->name : 'N/A'}}</td>
		<td>{{$row->phone_code ? $row->phone_code : 'N/A'}}</td>
		<td>{{$row->phone ? $row->phone : 'N/A'}}</td>
		<td>{{$row->email ? $row->email : 'N/A'}}</td>
		<td>{{$row->message ? $row->message : 'N/A'}}</td>
		<td>{{$row->created_at}}</td>

	</tr>
	@php $i++; @endphp
	@endforeach
@endif