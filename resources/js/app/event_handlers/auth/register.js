import RegisterController  from '../../controllers/auth/register';
import AlertService from '../../services/Alerts';

export default function() {
    $('#register-form-submit').click(function(e) {
        e.preventDefault();
        $('#register-form').submit();
    });

    $('#register-form').submit(function(e) {
        e.preventDefault();

        $('#register-form').find('.is-invalid').removeClass('is-invalid');
        $('#register-form').find('.invalid-feedback').text('');

        let formFields = $(this).serializeObject();

        let registerController = new RegisterController;
        registerController.attemptRegister(formFields)
            .then((response) => {
                console.log(response)
                console.log('success attemptRegister')
                // var formFields = new FormData($('#user_form')[0]);
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url : app_url+"/register",
                    type:'post',
                    data : formFields,
                    dataType : 'json',
                    processData: false,
                    contentType: false,
                    beforeSend:function(){
                        // runLoader('.page-content');
                    },
                    complete:function(){
                        //stopLoader('.page-content');
                    },
                    success : function(data){
                        console.log('success data: '+data);  
                    },
                    error : function(data){
                        console.log('error data: '+data);
                        //stopLoader('.page-content');
                        // if(data.responseJSON){
                        //     var err_response = data.responseJSON;
                        //         if(err_response.message) {
                        //             alertify.error(err_response.message);
                        //         }
                        //         // $('span.form_error').hide();
                        //         // $('.field-holder').find('input').css('border-color','');
                        //         // $.each(err_response, function(i, obj){
                        //         //     $('input[name='+i+']').parent('.field-holder').find('span.form_error').slideDown(400).html(obj);
                        //         //     $('input[name='+i+']').css('border-color','red');
                        //         // });
                        // }
                    }
                });
            })
            .catch(err => {
                let errors = err.getErrors();

                if (!errors) {
                    return AlertService.error(err.getMessage())
                        .then(e => {
                            alert('hello');
                        });
                }

                let i;
                for(i in errors) {
                    $('#register-form').find(`[name="${i}"]`).next().text(errors[i][0]);
                    $('#register-form').find(`[name="${i}"]`).addClass('is-invalid');
                }
            });
    });

    $('.register-btn').click(e => {
        e.preventDefault();
        $('#register-popup').modal('show');
    })

    $('#register-popup').on('show.bs.modal', function() {
        $('#login-popup').modal('hide');
    });

    $.ajaxSetup({ cache: true });
    $.getScript('https://connect.facebook.net/en_US/sdk.js', function(){
        FB.init({
            appId      : '1170085699843524',
            cookie     : true,
            xfbml      : true,
            version    : 'v2.8'
        });

        FB.AppEvents.logPageView();

        FB.getLoginStatus(function(response) {
            alert(response.status);
        });
    });
};
