import LoginController  from '../../controllers/auth/login';

export default function() {
    $('#login-form-submit').click(function(e) {
        e.preventDefault();
        $('#login-form').submit();
    });


    $('#login-form').submit(function(e) {
        e.preventDefault();

        $('#login-form').find('.is-invalid').removeClass('is-invalid');
        $('#login-form').find('.invalid-feedback').text('');

        let formFields = $(this).serializeObject();

        let loginController = new LoginController;
        loginController.attemptLogin(formFields.email, formFields.password)
            .then((response) => {
                console.log(response)
                console.log('success attemptLogin')
            })
            .catch(err => {
                let errors = err.getErrors();
                let i;

                for(i in errors) {
                    $('#login-form').find(`[name="${i}"]`).next().text(errors[i][0]);
                    $('#login-form').find(`[name="${i}"]`).addClass('is-invalid');
                }
            });
    });

    $('.login-btn').click(e => {
        e.preventDefault();
        $('#login-popup').modal('show');
    })

    $('#login-popup').on('show.bs.modal', function() {
        $('#register-popup').modal('hide');
    });
};
