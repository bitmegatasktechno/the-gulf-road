class ValidationError extends Error
{
    constructor(message, errors = false) {
        super(message);

        this.errors = errors;
    }

    getErrors() {
        return this.errors;
    }

    getMessage() {
        return this.message;
    }
}

export default ValidationError;
