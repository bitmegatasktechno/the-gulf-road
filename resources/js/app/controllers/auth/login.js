import ValidationError from "../../exceptions/ValidationError";
import Http from '../../services/http';

const validationService = require('../../services/validations');

class LoginController {

    constructor() {

    }

    async checkLoginValidation(email, password) {
        let validationResult = await validationService.validate({
            email: email,
            password: password
        }, {
            email: {
                presence: {
                    allowEmpty: false,
                    message: "field is Invalid"
                },
                email: true
            },
            password: {
                presence: {
                    allowEmpty: false,
                    message: "field is Invalid"
                }
            }
        });

        // if true it means there errors are there
        if (validationResult) {
            throw new ValidationError("Validation Error", validationResult);
        }
    }

    attemptLogin(email, password) {
        return this.checkLoginValidation(email, password)
            .then(async () => {

                await Http.post('/login/', {
                    email: email,
                    password: password
                })
                    .then(response => {
                        return true;
                    })
                    .catch(err => {
                        throw new ValidationError("Validation Error", err.response.data.errors);
                    });

            })
            .catch(err => { throw err; })
    }
}

export default LoginController;
