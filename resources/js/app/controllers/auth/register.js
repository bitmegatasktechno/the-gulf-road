import ValidationError from "../../exceptions/ValidationError";
import Http from '../../services/http';

const validationService = require('../../services/validations');

class RegisterController {

    constructor() {

    }

    async checkRegisterValidation(formFields) {
        let validationResult = await validationService.validate(formFields, {
            email: {
                presence: {
                    allowEmpty: false,
                    message: "field is Invalid"
                },
                email: true
            },
            name: {
                presence: {
                    allowEmpty: false,
                    message: "field is Invalid"
                }
            },
            password: {
                presence: {
                    allowEmpty: false,
                    message: "field is Invalid"
                }
            },
            password_confirmation: {
                presence: {
                    allowEmpty: false,
                    message: "field is Invalid"
                }
            },
            phone: {
                presence: {
                    allowEmpty: false,
                    message: "field is Invalid"
                }
            },
            country: {
                presence: {
                    allowEmpty: false,
                    message: "field is Invalid"
                }
            }
        });

        // if true it means there errors are there
        if (validationResult) {
            throw new ValidationError("Validation Error", validationResult);
        }
    }

    attemptRegister(formFields) {
        return this.checkRegisterValidation(formFields)
            .then(async () => {

                await Http.post('/register/', formFields)
                    .then(response => {
                        return true;
                    })
                    .catch(err => {
                        if (typeof err.response.data.errors != 'undefined' && Object.keys(err.response.data.errors).length) {
                            throw new ValidationError("Validation Error", err.response.data.errors);
                        } else if (typeof err.response.data.message != 'undefined') {
                            throw new ValidationError(err.response.data.message);
                        } else {
                            throw new ValidationError("Something went wrong");
                        }
                    });

            })
            .catch(err => { throw err; });
    }
}

export default RegisterController;
