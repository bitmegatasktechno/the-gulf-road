const config = require('../config/app');

module.exports = {
    getConfig: function(config_key) {
        return config[config_key];
    }
};
