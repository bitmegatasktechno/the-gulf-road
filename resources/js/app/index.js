require('../bootstrap');

import registerLoginEventHandler from './event_handlers/auth/login';
import registerSignupEventHandler from './event_handlers/auth/register';
import app_url from '../../config/app';

$(document).ready(function() {
    // Login Events
    registerLoginEventHandler();
    // Signup Events
    registerSignupEventHandler();
});
