var validate = require("validate.js");

module.exports = {
    validate: (fields, attributes, options = {}) => {

        let validator = validate(fields, attributes, options);

        return typeof validator !== 'undefined' ? validator : false;
    },

    validateAsync: (fields, attributes, options = {}) => {
        return validate.async(fields, attributes, options);
    },
};
