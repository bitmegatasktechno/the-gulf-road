import Swal from 'sweetalert2';

export default class Alert {
    static async  error(message = "Something went wrong") {
        return await Swal.fire({
            title: message,
            // text: 'You will not be able to recover this imaginary file!',
            icon: 'error',
            confirmButtonText: 'OK'
        }).then((result) => {
            if (result.value) {
                return result;
                // For more information about handling dismissals please visit
                // https://sweetalert2.github.io/#handling-dismissals
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                Swal.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        })
    }

}
