import axios from 'axios';
const getConfig = require('../helpers/functions').getConfig;

class Http {
    static get(url) {
        let urlAddress = getConfig('app_url') + url;

        return axios.get(urlAddress);
    }

    static post(url, data) {
        let urlAddress = getConfig('app_url') + url;

        return axios.post(urlAddress, data);
    }
}

export default Http;
