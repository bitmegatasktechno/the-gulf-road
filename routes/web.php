<?php
 
use App\Http\Controllers\StripeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// admin Routes Start here
Route::group(['prefix'=>'admin'], function(){

	Route::middleware(['admin.guest'])->group(function () {
        Route::get('/login', function () {
            return view('admin.auth.login'); // return login page to admin if not logged in already
        });
        Route::post('/login', 'Admin\DashboardController@login')->name('admin.login');
    });

    // auth admin routes
	Route::middleware(['admin.auth'])->group(function () {
        Route::get('/', function () {
            return redirect('admin/login');
        });
        
        Route::get('logout', 'Admin\DashboardController@logout')->name('admin.logout');
        Route::get('/dashboard','Admin\DashboardController@index')->name('admin.dashboard');

        Route::get('/profile/{loc?}', 'Admin\DashboardController@getProfile')->name('admin.profile');
    	Route::post('/profile', 'Admin\DashboardController@postProfile')->name('admin.postProfile');

    	Route::get('/users','Admin\UsersController@users'); // users listing
        Route::get('/user/detail/{id}','Admin\UsersController@userDetail'); // user detail
        Route::get('/user/add_awards/{id}','Admin\UsersController@add_awards'); // user detail

        Route::get('/user/updateTop/{type}/{id}/{top}','Admin\UsersController@userTopUpdate'); // user detail
        

        Route::post('/user/save_awards','Admin\UsersController@save_awards'); // user detail
        Route::post('/user/status','Admin\UsersController@userStatusUpdate'); // user Status update
        Route::post('/user/delete','Admin\UsersController@deleteUser'); // user Status update

    	Route::get('/agents','Admin\UsersController@agents'); // users listing
        Route::get('/agent/detail/{id}','Admin\UsersController@agentDetail'); // user detail
    	Route::get('/buddies','Admin\UsersController@buddies'); // users listing
        Route::get('/buddy/detail/{id}','Admin\UsersController@buddyDetail'); // user detail

        Route::get('applications/{user}','Admin\UsersController@showUpgradeRequests');

        Route::get('buddy-application/{id}','Admin\UsersController@buddyApplicationDetails');
        Route::post('buddy-application-status','Admin\UsersController@buddyApplicationStatus');

        Route::get('agent-application/{id}','Admin\UsersController@agentApplicationDetails');
        Route::post('agent-application-status','Admin\UsersController@agentApplicationStatus');

        Route::resource('countries','Admin\LocationController');
        Route::post('country/status','Admin\LocationController@status');
        Route::resource('cities','Admin\CityController');
        Route::post('city/status','Admin\CityController@status');

        Route::resource('languages','Admin\LanguageController');
        Route::post('language/status','Admin\LanguageController@status');

		Route::resource('reviews','Admin\ReviewController');
        Route::post('reviews/status','Admin\ReviewController@status');

        Route::resource('reports','Admin\ReportController');

        Route::resource('amenities','Admin\AmenitiesController');
        Route::post('amenity/status','Admin\AmenitiesController@status');

        Route::resource('amenitycategories','Admin\AmenityCategoryController');
        Route::post('amenitycategory/status','Admin\AmenityCategoryController@status');

        Route::resource('specialamenities','Admin\SpecialAmenitiesController');
        Route::post('specialamenity/status','Admin\SpecialAmenitiesController@status');

        Route::get('revenue','Admin\RevenueController@index')->name('revenue');
        Route::get('property_listing','Admin\RevenueController@property_listing')->name('property_listing');
        Route::get('select_plan','Admin\RevenueController@select_plan')->name('select_plan');
        Route::get('select_val','Admin\RevenueController@select_val')->name('select_val');

        Route::resource('flooring','Admin\FloorController');
        Route::post('floor/status','Admin\FloorController@status');

        Route::resource('housetypes','Admin\HouseTypeController');
        Route::post('housetype/status','Admin\HouseTypeController@status');

        Route::resource('specialization','Admin\SpecializationController');
        Route::post('specialization/status','Admin\SpecializationController@status');

        Route::resource('testimonials','Admin\TestimonialController');
        Route::post('testimonials/status','Admin\TestimonialController@status');

        Route::get('newsletter','Admin\NewsController@news');
        Route::get('homeloan','Admin\NewsController@homeloan');
        Route::get('homeloan/{id}','Admin\NewsController@deleteHomeloan'); 
        Route::get('subscribe','Admin\NewsController@subscribe');
        Route::get('sendmailtosubscriber','Admin\NewsController@subscribeMailSend');

        Route::post('sendmailtosubscriber','Admin\NewsController@newsletterEmailSend')->name('post_newsletter_email');
        
        Route::get('enquery','Admin\NewsController@enquery');
        Route::get('booking','Admin\NewsController@booking');


        Route::get('contact','Admin\ContactUsController@index')->name('contact');

        Route::get('banner','Admin\BannerController@index')->name('change_banner');
        Route::post('banner','Admin\BannerController@addBanner')->name('post_banner');
        Route::resource('main_banner','Admin\MainBannerController');
        Route::post('change/homepage/banner','Admin\MainBannerController@changeHomepageBanner')->name('change_homepage_banner');
        ROute::get('change/default/banner','Admin\MainBannerController@changeHomepageBanner')->name('change_default_banner');

        Route::resource('currencies','Admin\CurrencyController');
        Route::post('currency/status','Admin\CurrencyController@status');

        Route::get('subscription/trans/{type?}/{plan_id?}','Admin\SubscriptionController@transact')->name('subscribe.users');
        Route::get('subscription/{type?}','Admin\SubscriptionController@index');
        Route::get('subscription/create/{type?}','Admin\SubscriptionController@create');
        Route::post('subscription/save','Admin\SubscriptionController@save');
        Route::delete('subscription/delete/{id}','Admin\SubscriptionController@destroy');
        Route::get('subscription/{id}/edit/{type}','Admin\SubscriptionController@edit');
        Route::post('subscription/update','Admin\SubscriptionController@update');

        Route::get('video','Admin\VideoController@index');
        Route::get('add_video','Admin\VideoController@add_video');
        Route::post('save_video','Admin\VideoController@save_video');
        Route::get('edit_video','Admin\VideoController@edit_video');
        Route::post('delete_video','Admin\VideoController@delete_video');
        Route::post('update_video','Admin\VideoController@update_video');

        Route::get('blogs','Admin\BlogController@index');
        Route::post('blogs/status','Admin\BlogController@status');

        Route::get('properties/{slug}','Admin\PropertyController@index');
        Route::get('properties/{slug}/populate/{id}','Admin\PropertyController@populate');
        Route::get('properties/{slug}/approve/{id}','Admin\PropertyController@approve');
        Route::get('property-details/{slug}/{id}','Admin\PropertyController@show');
        Route::post('property/delete','Admin\PropertyController@destroy');
        Route::get('property-details/premium/{slug}/{id}','Admin\PropertyController@premium');

        

        Route::resource('awards','Admin\AwardController');
        Route::post('awards/status','Admin\AwardController@status');

        Route::get('coworkspaces','Admin\PropertyController@listCoworkspaces');
        Route::get('coworkspace/detail/{id}','Admin\PropertyController@detailCoworkspace');
        Route::post('coworkspace/delete','Admin\PropertyController@destroyCoworkspace');

        Route::get('swap','Admin\PropertyController@listSwapProperties');
        Route::get('swap/detail/{id}','Admin\PropertyController@detailSwapProperties');
        Route::post('swap/delete','Admin\PropertyController@destroySwap');

        Route::get('modules','Admin\ModuleController@listModules');
        Route::get('modules/check','Admin\ModuleController@checkboxResult');

        Route::get('modules','Admin\ModuleController@listModules');

        Route::resource('cospaceStories','Admin\CospacestoryController');
        Route::any('cospaceStories/status','Admin\CospacestoryController@status');


        Route::resource('companiesIcons','Admin\CompanyiconController');
        Route::post('companiesIcons/status','Admin\CompanyiconController@status');

	});
});


// Frontend Routes request/
Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);

Route::group(['prefix' => '{locale}','middleware' => ['setlocale']], function() {

    Route::get('/', 'HomeController@index')->name('home'); // landing page route


    /*
        stest strpe payment
    */


    Route::get('/stripe-payment','HomeController@stripeHandleGet');
    Route::get('/stripe-payment','HomeController@stripeHandlePost');
     /*
        stest strpe payment
    */
    Route::get('/custom-logout', 'Auth\LoginController@logout'); // logout
    Route::get('/about/{loc?}','HomeController@about')->name('about');
    Route::get('/tearms/{loc?}','HomeController@tearms')->name('tearms');
    Route::get('/privacy/{loc?}','HomeController@privacy')->name('privacy');
    Route::get('/contact/{loc?}','HomeController@contact')->name('contact');
    Route::get('/package/{loc?}','HomeController@package')->name('package');
    Route::get('/faq/{loc?}','HomeController@faq')->name('faq');
    

    // admin not allowded to access user pages.
    Route::group(['middleware'=>'admin.restrict'], function(){
        Route::post('forgot-password', 'Auth\ForgotPasswordController@sendResetLinkEmail');

        // social login routes
        Route::get('/login/{provider}', 'User\SocialAuthController@redirect')
        ->name('login.social.redirect');
        Route::get('/login/{provider}/callback', 'User\SocialAuthController@callback')
        ->name('login.social.callback');

        Route::post('/register','Auth\RegisterController@register'); // register routes
        Route::post('/login','Auth\LoginController@login'); // login routes
        Route::get('logout', 'Auth\LoginController@logout')->name('logout');// logout route
        Route::get('states/{country}','User\ProfileController@getState');

        Route::post('contact_owner','HomeController@contact_owner');
        Route::get('newslatter','HomeController@newslatter');
        Route::get('subscribe','HomeController@subscribe');
        Route::get('homeloan','HomeController@homeloan');
        Route::get('share/{id?}/{type?}','HomeController@share');
        Route::post('book_consult','HomeController@book_consult');
        Route::post('enquirytouser','HomeController@enquirytouser');
        Route::post('contactsend','HomeController@postContactSend');
        Route::match(['get','post'],'report_user/{loc?}/{type}/{id}','HomeController@report_user');

        
        

        Route::get('clearnot/{loc?}','HomeController@clearnot');

        Route::get('list/premium','HomeController@Listpremium');

        Route::get('agents/{loc?}','Agent\AgentController@getAgentsLists');
        Route::get('favourate','HomeController@favourate');
        Route::get('buddies/{loc?}','Buddies\BuddiesController@getBuddiesLists');
        Route::get('coworkspace/{loc?}','HomeController@getCoworkspaceLists');
        Route::get('buy-property/{loc?}','HomeController@buyPropertyListing');
        Route::get('rent-property/{loc?}','HomeController@rentPropertyListing');
        Route::get('swap/{loc?}','HomeController@getSwapLists');
        Route::get('property/{loc}/{type}/{id}','HomeController@propertyDetails');
		Route::post('property/buy/send_enquiry',['as'=>'send_enquiry','uses'=>'HomeController@sendEnquiry']);
		Route::post('property/review',['as'=>'send_review','uses'=>'HomeController@sendReview']);
        Route::get('buddy/{loc?}/{id}','HomeController@getBuddyDetails')->name('buddy_detail');
        Route::get('agent/{loc?}/{id}','HomeController@getAgentDetails');
        
        Route::get('filter-property/{loc?}','HomeController@filterPropertyListing')->name('filterList');
        Route::get('tab-property/{loc?}','HomeController@tabPropertyListing')->name('tabList');
        Route::get('rating-list/{loc?}','HomeController@rateListing')->name('rateListing');
        Route::get('rating-list-buddy/{loc?}','HomeController@rateListingBuddy')->name('rateListingBuddy');
        Route::post('agent/review',['as'=>'agent_review','uses'=>'HomeController@agentReview']);
        Route::post('agent/book',['as'=>'agent_book','uses'=>'HomeController@agentBook']);

       
        
		
        
    	Route::get('/blog_listing/{loc?}','BlogController@blog_listing');
    	Route::get('/blog_details/{loc?}/{id}','BlogController@blog_details');
		 
		
        Route::get('/blog-listing/{loc?}','BlogController@blog_listing');
        
        Route::get('/blog-details/{loc?}/{id}','BlogController@blog_details');

        

        Route::post('book_property','BookProperty\BookPropertyController@postBookProperty');



        Route::get('/landing/cospaceStory/{id}', 'HomeController@cospaceStory');

        Route::get('/home/{slug?}', 'HomeController@home')->name('user.home'); // landing page route
        Route::get('/landing/{slug?}/buddies', 'HomeController@buddies_landing')->name('user.buddies');
        Route::get('/landing/{slug?}/agents', 'Agent\AgentController@getLandingAgentsList')->name('user.agents');
        Route::get('/landing/{slug?}/cospace', 'HomeController@cospace_landing')->name('user.cospace');
        Route::get('/landing/{slug?}/swap', 'HomeController@swap_landing')->name('user.swap');
        Route::get('/landing/{slug?}/rent', 'HomeController@rent_landing')->name('user.rent');
        Route::get('/landing/{slug?}/buy', 'HomeController@buy_landing')->name('user.buy');
        Route::get('property-gallery/{id}', 'HomeController@gallery');
        Route::post('getLongLat', 'HomeController@getLongLat');
        Route::get('sendenquiry1', 'HomeController@sendenquiry1');
        

        Route::get('/cost-calculator1/{loc?}/{type}/{id}', 'HomeController@cost_calculate1');
        Route::post('/cost-calculator1', 'HomeController@cost_calculate1_post');
        Route::get('/cost-calculator2/{loc?}/{type}/{id}', 'HomeController@cost_calculate2');
        Route::post('/cost-calculator2', 'HomeController@cost_calculate2_post');
        Route::get('/cost-calculator3/{loc?}/{type}/{id}', 'HomeController@cost_calculate3');
        Route::post('/cost-calculator3', 'HomeController@cost_calculate3_post');

        Route::get('listproperty/{loc?}/swap/compare/{id}','HomeController@compare');
        // after user logged in Routes
        Route::group(['middleware'=>'auth'], function(){
            Route::get('skip','User\ProfileController@skip');

            Route::get('/profile/{loc?}', 'User\ProfileController@getProfile'); // profile page


			Route::get('/enquiries/{loc?}', ['as'=>'enquiry_list','uses'=>'User\ProfileController@enquiryList']);
			Route::get('/enquiry_action/{id}',['as'=>'enquiry_action','uses'=>'HomeController@enquiry_action']);
			Route::get('/enquiry_action_one/{id}',['as'=>'enquiry_action_one','uses'=>'HomeController@enquiry_action_one']);

            Route::get('/blog/{loc?}','BlogController@index');
            Route::post('save_blog/{loc?}','BlogController@save_blog')->name('save_blog');
            Route::get('/listing/{loc?}','BlogController@listing');
            Route::get('/edit_blog/{loc?}/{id}','BlogController@edit_blog');
            Route::post('/update_blog/{loc?}','BlogController@update_blog');
            Route::get('delete_blog/{loc?}','BlogController@delete_blog');

            Route::get('/myreview/{loc?}','HomeController@myreview');

            Route::get('/myfav/{loc?}', 'HomeController@myfav');

            Route::get('notifications/{loc?}','HomeController@notifications');

            Route::get('booking/{loc?}','HomeController@booking');
            Route::get('userbooking/{loc?}','HomeController@booking2');

            Route::get('/mysubscription/{loc?}','SubscriptionController@index');
            Route::get('/mysubscription_property/{loc?}/{id?}/{type?}','SubscriptionController@mysubscription_property');

            Route::post('/update-profile', 'User\ProfileController@postProfile'); // profile
            Route::post('/update-aboutme', 'User\ProfileController@saveAboutUser'); //  about me
            Route::post('/update-address', 'User\ProfileController@updateAddress'); // address
            Route::post('/update-qualification', 'User\ProfileController@updateQualification'); // hobby
            Route::post('/update-additional', 'User\ProfileController@updateAdditionalData');
            Route::post('/update-professional', 'User\ProfileController@updateProfessionalData');
            Route::post('/update-documents', 'User\ProfileController@updateDocuments');


            Route::get('/change-password/{loc?}', 'User\ProfileController@showChangePassword');
            Route::post('/change-password', 'User\ProfileController@updateChangePassword');
            Route::post('/upload-image', 'User\ProfileController@updateProfileImage');

            Route::get('/request/{loc?}/subscription/{type?}','User\ProfileController@subscription');
            Route::get('/renew/{loc?}/subscription/{type?}','User\ProfileController@renewSubscription');
            Route::post('renew/subscription','User\ProfileController@renew');
            // buddy request
            Route::get('/buddy-request/{loc?}','User\ProfileController@buddyRequest');
            Route::get('buddy-profile/step/{step}/{loc?}','User\ProfileController@buddyProfileRequestSteps');
            Route::post('buddy-profile/step/{step}/{loc?}','User\ProfileController@saveBuddyProfileSteps');
            Route::post('change-default-role','User\ProfileController@changeDefaultRole');

            // agent request
            Route::get('/agent-request/{loc?}','User\ProfileController@agentRequest');
            Route::get('agent-profile/step/{step}/{loc?}','User\ProfileController@agentProfileRequestSteps');
            Route::post('agent-profile/step/{step}/{loc?}','User\ProfileController@saveAgentProfileSteps');

            // list property
            Route::get('listproperty/{loc?}','User\PropertyController@index');
            Route::post('listproperty/transaction','User\PropertyController@transaction');
            //stripe payment
             Route::post('listproperty/stripeorder','User\PropertyController@stripeorder');
            //stripe payment with premium
             Route::post('listproperty/stripeorderpremium','User\PropertyController@stripeorderpremium');
             Route::get('strip-payment-premium-init','User\PropertyController@stripePaymentPremiumInit');
             Route::get('strip-payment-premium-success','User\PropertyController@stripePaymentPremiumSuccess');

             //stripe payment  init
             Route::get('strip-payment-init','User\PropertyController@stripePaymentInit');
             Route::get('stripe-success','User\PropertyController@stripePaymentSuccess');
 
//stripe payment  for subsccribe  
             Route::get('strip-payment-subscribe','User\PropertyController@stripePaymentSubscribe');
             Route::get('stripe-subscribe-success','User\PropertyController@stripePaymentSubscribeSuccess');
 

            // rent-sale
            Route::get('listproperty/{loc?}/rent-sale','User\PropertyController@createRentSaleProperty');
            Route::post('listproperty/rentsale/{step}','User\PropertyController@validateRentSaleProperty');

            // swap
            Route::get('listproperty/{loc?}/swap','User\PropertyController@createSwapProperty');
            Route::post('listproperty/swap/{step}','User\PropertyController@validateSaveSwapProperty');
            

            // coworkspace
            Route::get('listproperty/{loc?}/coworkspace','User\PropertyController@createCoworkspaceProperty');
            Route::post('listproperty/coworkspace/{step}','User\PropertyController@validateSaveCoworkspaceProperty');


            // my properties
            Route::get('mylistings/{loc?}','User\PropertyController@myListings');
            Route::get('mycoworkspaces/{loc?}','User\PropertyController@myCoworkspaces');
            Route::get('myswap/{loc?}','User\PropertyController@mySwapProperties');

            Route::get('editproperty/{loc?}/rent-sale/{id}','User\PropertyController@getRentSaleEditProperty');
            Route::get('editproperty/{loc?}/coworkspace/{id}','User\PropertyController@editCoworkspace');
            Route::get('editproperty/{loc?}/swap/{id}','User\PropertyController@editSwapProperty');

            Route::post('updateProperty/rentsale/{step}','User\PropertyController@updateRentSaleProperty');
            Route::post('updateProperty/coworkspace/{step}','User\PropertyController@updateCoworkspaceProperty');
            Route::post('updateProperty/swap/{step}','User\PropertyController@updateSwapProperty');

            Route::post('delete-property','User\PropertyController@deleteProperty');
            Route::post('delete-coworkspace','User\PropertyController@deleteCoworkspaceProperty');
            Route::post('delete-swap','User\PropertyController@deleteSwapProperty');

            ///////get Cities According to Country//////////////////////////////////
            Route::post('get/cities','User\PropertyController@getCities')->name('getCities');
            //////////////////////////////////////////////////////////////////////

            Route::get('user_chat/{loc?}/{id?}','User\ChatController@user_chat');
            Route::post('send_message','User\ChatController@send_message');
            Route::get('all_message/{loc?}/{id}','User\ChatController@all_message');
        });
    });
    //Route::get('/email/resend', 'Auth\VerificationController@resend')->name('verification.resend');
});


Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::get('verifyemail/{token}', 'Auth\VerificationController@verifyEmail')->name('verifyemail');
// redirect to index page with locale selected
Route::get('/', function () {
    return redirect(app()->getLocale());
});


