const fs = require('fs');
const mix = require('laravel-mix');
const webpack = require('webpack');
const dotenv = require('dotenv');

const example = dotenv.parse(fs.readFileSync('.env.example'));
const env = dotenv.parse(fs.readFileSync('.env'));

mix.webpackConfig({
    plugins: [
        new webpack.DefinePlugin({
            "process.env": Object.keys(example).reduce((definitions, key) => {
                const existing = process.env[key];

                if (existing) {
                    definitions[key] = JSON.stringify(existing);
                    return definitions;
                }

                const value = env[key];
                if (value) definitions[key] = JSON.stringify(value);

                return definitions;
            }, {})
        }),
    ]
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const publicPath = "public/assets";

// mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css');

mix.js('resources/js/app/index', publicPath + '/js/app');
