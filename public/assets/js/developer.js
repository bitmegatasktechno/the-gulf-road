$(document).ready(function () {
    $(document).on('click','.logout-btn-submit', function(e){
        e.preventDefault();
        window.location = WEBSITE_URL+'/logout';
        // Swal.fire({
        //     title: 'Alert!',
        //     text: 'Are you sure you want to logout ?',
        //     icon: 'error',
        //     confirmButtonText: 'Ok',
        //     showCancelButton: true,
        //     focusConfirm: false,
        // }).then((result) => {
        //     if(result.value){
        //         window.location = WEBSITE_URL+'/logout';
        //     }
        // });
    });
});

/**
* Start the loader on the particular element
*/
function startLoader(element) {
    // check if the element is not specified
    if(typeof element == 'undefined') {
        element = "body";
    }

    // set the wait me loader
    $(element).waitMe({
        effect : 'bounce',
        text : 'Please Wait..',
        bg : 'rgba(255,255,255,0.7)',
        color : 'rgba(230, 0, 42, 0.5) !important',// change color if want any color for loader
        sizeW : '20px',
        sizeH : '20px',
        source : ''
    });
}

/**
* Start the loader on the particular element
*/
function stopLoader(element) {
    // check if the element is not specified
    if(typeof element == 'undefined') {
        element = 'body';
    }

    // close the loader
    $(element).waitMe("hide");
}
