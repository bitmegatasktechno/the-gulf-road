$(document).ready(function(){

    // registration modal submit click event
    $('.forgot_password_btn').click(function(e) {
        e.preventDefault();
        $('#forgot-password-form').submit();
    });

    // on submit of register form event
    $('#forgot-password-form').submit(function(e) {
        e.preventDefault();
        // clear all errors
        $('#forgot-password-form').find('.is-invalid').removeClass('is-invalid');
        $('#forgot-password-form').find('.invalid-feedback').text('');

        let formFields = new FormData($('#forgot-password-form')[0]);
        let ajax_url = WEBSITE_URL+"/forgot-password";
        
        $.ajax({
            url : ajax_url,
            type:'post',
            data : formFields,
            dataType : 'json',
            processData: false,
            contentType: false,
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            beforeSend:function(){
                startLoader('.modal-content');
            },
            complete:function(){
                // 
            },
            success : function(data){
                stopLoader('.modal-content');
                if(data.status){
                	Swal.fire({
	                    title: 'Check Your Email!',
	                    text: data.message,
	                    icon: 'success',
	                    confirmButtonText: 'Ok'
	                }).then((result) => {
	                  window.location.reload();
	                });
                }else{
                    stopLoader('.modal-content');
                	Swal.fire({
	                    title: 'Error!',
	                    text: data.message,
	                    icon: 'error',
	                    confirmButtonText: 'Ok'
	                });
                }
            },
            error : function(data){
                stopLoader('.modal-content');
                if(data.responseJSON){
                    var err_response = data.responseJSON;  
                    if(err_response.errors==undefined && err_response.message) {
                        Swal.fire({
                            title: 'Error!',
                            text: err_response.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    } 
                                              
                    $.each(err_response.errors, function(i, obj){
                        $('#forgot-password-form').find('input[name="'+i+'"]').next().text(obj);
                        $('#forgot-password-form').find('input[name="'+i+'"]').addClass('is-invalid');
                    });
                }
            }
        });
    });

    // registration modal submit click event
    $('#register-form-submit').click(function(e) {
        e.preventDefault();
        $('#register-form').submit();
    });

    // on submit of register form event
    $('#register-form').submit(function(e) {
        e.preventDefault();

        // clear all errors
        $('#register-form').find('.is-invalid').removeClass('is-invalid');
        $('#register-form').find('.invalid-feedback').text('');

        let formFields = new FormData($('#register-form')[0]);
        let ajax_url = WEBSITE_URL+"/register";
         
        
        $.ajax({
            url : ajax_url,
            type:'post',
            data : formFields,
            dataType : 'json',
            processData: false,
            contentType: false,
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            beforeSend:function(){
                startLoader('.modal-content');
            },
            complete:function(){
                // 
            },
            success : function(data){
 
                stopLoader('.modal-content');
                Swal.fire({
                    title: 'Check Your Email!',
                    text: data.message,
                    icon: 'success',    
                    confirmButtonText: 'Ok'
                }).then((result) => {
                  window.location.reload();
                });
            },
            error : function(data){
                stopLoader('.modal-content');
                if(data.responseJSON){
                    var err_response = data.responseJSON;  
                    if(err_response.errors==undefined && err_response.message) {
                        Swal.fire({
                            title: 'Error!',
                            text: err_response.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    } 
                                              
                    $.each(err_response.errors, function(i, obj){
                        $('#register-form').find('.invalid-feedback.'+i+'').text(obj).show();
                        $('#register-form').find('input[name="'+i+'"]').addClass('is-invalid');
                        $('#register-form').find('textarea[name="'+i+'"]').addClass('is-invalid');
                        $('#register-form').find('select[name="'+i+'"]').addClass('is-invalid');
                    });
                }
            }
        });
    });

    
    // login modal code

    // login button submit click event
    $('#login-form-submit').click(function(e) {
        e.preventDefault();
        $('#login-form').submit();
    });

    // login form submit event
    $('#login-form').submit(function(e) {
        e.preventDefault();
        $('#login-form').find('.is-invalid').removeClass('is-invalid');
        $('#login-form').find('.invalid-feedback').text('');

        let loginformFields = new FormData($('#login-form')[0]);
        let loginAjaxUrl = WEBSITE_URL+"/login";

        $.ajax({
            url : loginAjaxUrl,
            type:'post',
            data : loginformFields,
            dataType : 'json',
            processData: false,
            contentType: false,
            headers: {'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')},
            beforeSend:function(){
                startLoader('.modal-content');
            },
            complete:function(){
                // 
            },
            success : function(data){
                if(data.status){
                    setTimeout(function(){ 
                    	window.location = data.redirect_to; 
                    }, 1000);
                }else{
                    stopLoader('.modal-content');
                }
            },
            error : function(data){
                stopLoader('.modal-content');
                if(data.responseJSON){
                    var err_response = data.responseJSON;
                    if(err_response.errors==undefined && err_response.message) {
                        Swal.fire({
                            title: 'Error!',
                            text: err_response.message,
                            icon: 'error',
                            confirmButtonText: 'Ok'
                        });
                    }              
                    $.each(err_response.errors, function(i, obj){
                        $('#login-form').find('input[name="'+i+'"]').next().text(obj);
                        $('#login-form').find('input[name="'+i+'"]').addClass('is-invalid');
                    });
                }
            }
        });
    });

    // to show register modal event
    $('.register-btn').click(e => {
        e.preventDefault();
        $('#login-popup').modal('hide');
        $('#register-popup').modal('show');
    });

    // event before showing register form modal.
    $('#register-popup').on('show.bs.modal', function() {
        $('#login-popup').modal('hide');
    });
    
    $('.login-btn').click(e => {
        e.preventDefault();
        $('#login-popup').modal('show');
        $('#register-popup').modal('hide');
    })

    $('#login-popup').on('show.bs.modal', function() {
        $('#register-popup').modal('hide');
    });

    // on hidden of login modal hide all errors and reset form
    $('#login-popup').on('hidden.bs.modal', function (e) {
        $('#login-form').find('.is-invalid').removeClass('is-invalid');
        $('#login-form').find('.invalid-feedback').text('');
        $('#login-form').trigger("reset");
    });

    // on hidden of register modal , hide all errors and reset form
    $('#register-popup').on('hidden.bs.modal', function (e) {
        $('#register-form').find('.is-invalid').removeClass('is-invalid');
        $('#register-form').find('.invalid-feedback').text('');
        $('#register-form').trigger("reset");
    });
});

const showLoginForm = function(){
	hideAllModal();
	$('#login-popup').modal('show');
}

const showEnquiryForm = function($userid){
    hideAllModal();
    $('#enquiry-popup #touser').val($userid);
	$('#enquiry-popup').modal('show');
}

const showReportForm = function($userid){
    hideAllModal();
    $('#report-popup #touser').val($userid);
    $('#report-popup').modal('show');
}


const showBookSpaceForm = function($userid){
    hideAllModal();
    $('#bookspace-popup #touser').val($userid);
    $('#bookspace-popup').modal('show');
}


const showMainBookSpaceForm = function($userid){
    hideAllModal();
    $('#touser').val($userid);
    $('#mainbookspace-popup').modal('show');
}


const showSubscriptionForm = function(){
	hideAllModal();
	$('#subscribe-popup').modal('show');
}

const showHomeLoanForm  = function(){
	hideAllModal();
	$('#homeloan-popup').modal('show');
}

const showCompareForm = function(){
	hideAllModal();
	$('#compare-popup').modal('show');
}

const showRegisterForm = function(){
	hideAllModal();
    $('#register-popup').modal('show');
    // $('#skip-popup').modal('show');
}

const showRegisterForm_user = function(){
	hideAllModal();
    $('#register-popup').modal('show');
    // $('#skip-popup').modal('show');
}

const showForgotPasswordForm = function(){
	hideAllModal();
	$('#forgot-password-popup').modal('show');
}

const hideAllModal = function(){
	$('.modal').modal('hide');
	addModalScroll();
}

const addModalScroll = function(){
    $('.modal').css('overflow-y','auto');
}