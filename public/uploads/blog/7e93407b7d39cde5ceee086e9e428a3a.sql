-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 17, 2020 at 05:22 PM
-- Server version: 5.7.21
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `game`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `pinggame_report_day`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `pinggame_report_day`;
CREATE TABLE IF NOT EXISTS `pinggame_report_day` (
`idate` date
,`totalgame` decimal(32,0)
,`win` bigint(21)
,`uniqueuser` bigint(21)
,`amount` double
,`first_win` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `pinggame_report_month`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `pinggame_report_month`;
CREATE TABLE IF NOT EXISTS `pinggame_report_month` (
`idate` varchar(7)
,`totalgame` decimal(32,0)
,`win` bigint(21)
,`uniqueuser` bigint(21)
,`amount` double
,`first_win` bigint(21)
);

-- --------------------------------------------------------

--
-- Table structure for table `ping_game_day_report`
--

DROP TABLE IF EXISTS `ping_game_day_report`;
CREATE TABLE IF NOT EXISTS `ping_game_day_report` (
  `idate` varchar(45) NOT NULL,
  `cnt` int(11) DEFAULT '0',
  `win` int(11) NOT NULL DEFAULT '0',
  `user` int(11) DEFAULT '0',
  `amount` float NOT NULL DEFAULT '0',
  `first_win` int(11) NOT NULL DEFAULT '0',
  `totalgame` int(11) DEFAULT NULL,
  `uniqueuser` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ping_game_details`
--

DROP TABLE IF EXISTS `ping_game_details`;
CREATE TABLE IF NOT EXISTS `ping_game_details` (
  `user_id` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `cnt` int(11) NOT NULL DEFAULT '0',
  `win` int(11) NOT NULL DEFAULT '0',
  `totalattempt` int(11) NOT NULL DEFAULT '0',
  `best_rank` int(11) DEFAULT NULL,
  `best_score` int(11) NOT NULL DEFAULT '0',
  `dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ping_game_leader`
--

DROP TABLE IF EXISTS `ping_game_leader`;
CREATE TABLE IF NOT EXISTS `ping_game_leader` (
  `user_id` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  `ctime` int(11) NOT NULL DEFAULT '0',
  `score` int(11) NOT NULL DEFAULT '0',
  `cnt` int(11) NOT NULL DEFAULT '0',
  `indate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `win` varchar(45) NOT NULL DEFAULT 'NO',
  `c_score` int(11) NOT NULL DEFAULT '0',
  `c_time` int(11) NOT NULL DEFAULT '0',
  `ads` int(11) NOT NULL DEFAULT '3',
  `amount` float NOT NULL DEFAULT '0',
  `notify` int(11) NOT NULL DEFAULT '0' COMMENT '0:Default|1:In|2:Out|3:Nofity',
  `seven_days` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ping_game_leader_backup`
--

DROP TABLE IF EXISTS `ping_game_leader_backup`;
CREATE TABLE IF NOT EXISTS `ping_game_leader_backup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  `ctime` int(11) NOT NULL DEFAULT '0',
  `score` varchar(45) NOT NULL DEFAULT '0',
  `cnt` int(11) NOT NULL DEFAULT '0',
  `indate` datetime NOT NULL,
  `win` varchar(45) NOT NULL DEFAULT 'NO',
  `amount` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5751 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ping_game_leader_hack`
--

DROP TABLE IF EXISTS `ping_game_leader_hack`;
CREATE TABLE IF NOT EXISTS `ping_game_leader_hack` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `ctime` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `indate` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ping_game_month_report`
--

DROP TABLE IF EXISTS `ping_game_month_report`;
CREATE TABLE IF NOT EXISTS `ping_game_month_report` (
  `idate` varchar(45) NOT NULL,
  `cnt` int(11) NOT NULL DEFAULT '0',
  `win` int(11) NOT NULL DEFAULT '0',
  `user` int(11) NOT NULL DEFAULT '0',
  `amount` float NOT NULL DEFAULT '0',
  `first_win` int(11) NOT NULL DEFAULT '0',
  `totalgame` int(11) DEFAULT NULL,
  `uniqueuser` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ping_game_outside_report`
--

DROP TABLE IF EXISTS `ping_game_outside_report`;
CREATE TABLE IF NOT EXISTS `ping_game_outside_report` (
  `dt` date NOT NULL,
  `cnt` int(11) NOT NULL DEFAULT '1',
  `ucnt` int(11) NOT NULL DEFAULT '0',
  `totalsignup` int(11) NOT NULL DEFAULT '0',
  `signup` int(11) NOT NULL DEFAULT '0',
  `login` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`dt`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ping_game_outside_uid`
--

DROP TABLE IF EXISTS `ping_game_outside_uid`;
CREATE TABLE IF NOT EXISTS `ping_game_outside_uid` (
  `user_id` varchar(45) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ping_game_uid`
--

DROP TABLE IF EXISTS `ping_game_uid`;
CREATE TABLE IF NOT EXISTS `ping_game_uid` (
  `user_id` varchar(45) NOT NULL,
  `id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ping_game_winners`
--

DROP TABLE IF EXISTS `ping_game_winners`;
CREATE TABLE IF NOT EXISTS `ping_game_winners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `email_status` int(11) DEFAULT '0' COMMENT '0:not sent|1:sent',
  `amount` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1501 DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ping_seven_days_score`
--

DROP TABLE IF EXISTS `ping_seven_days_score`;
CREATE TABLE IF NOT EXISTS `ping_seven_days_score` (
  `indate` date NOT NULL,
  `score` int(11) NOT NULL,
  PRIMARY KEY (`indate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure for view `pinggame_report_day`
--
DROP TABLE IF EXISTS `pinggame_report_day`;

CREATE ALGORITHM=UNDEFINED DEFINER=`admin`@`%` SQL SECURITY DEFINER VIEW `pinggame_report_day`  AS  select cast(`ping_game_leader_backup`.`indate` as date) AS `idate`,sum(`ping_game_leader_backup`.`cnt`) AS `totalgame`,count((case when (`ping_game_leader_backup`.`win` = 'YES') then 1 else NULL end)) AS `win`,count(distinct `ping_game_leader_backup`.`user_id`) AS `uniqueuser`,sum(`ping_game_leader_backup`.`amount`) AS `amount`,count((case when (`ping_game_leader_backup`.`amount` = '5') then 1 else NULL end)) AS `first_win` from `ping_game_leader_backup` group by cast(`ping_game_leader_backup`.`indate` as date) ;

-- --------------------------------------------------------

--
-- Structure for view `pinggame_report_month`
--
DROP TABLE IF EXISTS `pinggame_report_month`;

CREATE ALGORITHM=UNDEFINED DEFINER=`admin`@`%` SQL SECURITY DEFINER VIEW `pinggame_report_month`  AS  select date_format(`ping_game_leader_backup`.`indate`,'%Y-%m') AS `idate`,sum(`ping_game_leader_backup`.`cnt`) AS `totalgame`,count((case when (`ping_game_leader_backup`.`win` = 'YES') then 1 else NULL end)) AS `win`,count(distinct `ping_game_leader_backup`.`user_id`) AS `uniqueuser`,sum(`ping_game_leader_backup`.`amount`) AS `amount`,count((case when (`ping_game_leader_backup`.`amount` = '5') then 1 else NULL end)) AS `first_win` from `ping_game_leader_backup` group by date_format(`ping_game_leader_backup`.`indate`,'%Y-%m') ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
