<?php

namespace App\Console\Commands;
use Spatie\Geocoder\Facades\Geocoder;
use App\Models\User;
use Illuminate\Console\Command;
class EmailLower extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:EmailLower';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'change capital email to lower case';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(){
        foreach (User::whereIn('role',['BUDDY', 'AGENT'])->cursor() as $key => $user) {
            if($user->address){
                $client = new \GuzzleHttp\Client();
                $geoInfo = Geocoder::getCoordinatesForAddress($user->address);
                $user->latitude = $geoInfo['lat'];
                $user->longitude = $geoInfo['lng'];
                $user->save();
            }
        }
    }
}
