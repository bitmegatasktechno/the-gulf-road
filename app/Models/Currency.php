<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Currency extends Model
{
    public $timestamps = false;

    protected $collection = 'currencies';
    protected $primaryKey = '_id';
    protected $guarded = [];
}
