<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Consultation extends Model
{
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $collection = 'consultation';
    protected $primaryKey = '_id';
    protected $fillable = [
        'name', 'number','startdate','enddate','status','user_id','type','type_id','content','email','code'
    ];
}
