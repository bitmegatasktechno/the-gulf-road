<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class SpecialAmenity extends Model
{
    public $timestamps = false;

    protected $collection = 'special_amenities';
    protected $primaryKey = '_id';
    protected $guarded = [];

    public function category(){
    	return $this->belongsTo(AmenityCategory::class,'category_id','_id');
    }
}
