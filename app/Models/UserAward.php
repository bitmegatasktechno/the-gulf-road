<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class UserAward extends Model
{
    public $timestamps = true;

    protected $collection = 'user_awards';
    protected $primaryKey = '_id';
    protected $guarded = [];

    public function awardss(){
        return $this->belongsTo(Award::class, 'award_id','_id');
    }
}
