<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Auth\User as Authenticatable;
use Mail;
use App\Mail\{
    NewUserNotification, ForgotPasswordNotification
};

class Admin extends Authenticatable
{
    use Notifiable;
    protected $guard = 'admin';
    
    protected $collection = 'admins';
    protected $primaryKey = '_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'password', 
        'role',
        'status'
    ];
}