<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class UserAddress extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'user_id',
        'same_current_address',
        // current
        'current_location',
        'current_unit_number',
        'current_address_1',
        'current_address_2',
        'current_landmark',
        'current_city',
        'current_country',
        'current_state',
        'current_zipcode',
        // native
        'native_location',
        'native_unit_number',
        'native_address_1',
        'native_address_2',
        'native_landmark',
        'native_city',
        'native_country',
        'native_state',
        'native_zipcode',  
    ];


    public function getLatLong($address)
  {
    $result=[];
      $client = new \GuzzleHttp\Client();
    try{

        $location = [];
      $location['lng']='-79.3831843';
      $location['lat']='43.653226';


        if(env('APP_ACTIVE_MAP')=='google_map')
        {
          
        $res = $client->request('GET', 'https://maps.googleapis.com/maps/api/geocode/json', [
          'query' => [
          'address' => $address,
          'key' => env('GOOGLE_MAP_KEY')
          ]
        ]);
        $result = json_decode($res->getBody()->getContents());
        if(isset($result->results[0]))
        {
          $location['lng']= $result->results[0]->geometry->location->lng;
          $location['lat']= $result->results[0]->geometry->location->lat;
        }
        }else if(env('APP_ACTIVE_MAP')=='patel_map')
        {



           
          $res = $client->request('POST', 'https://siteapi.cloud.huawei.com/mapApi/v1/siteService/geocode?key='.(env('PATEL_MAP_KEY')), [
          'json' => [
             
            'address'     => ($address),
            'language'    => 'en'
          ]
        ]);
        
        $result = json_decode($res->getBody()->getContents());
          
         if($result->returnDesc =='OK')
        {
          $location['lng']= $result->sites[0]->location->lng;
          $location['lat']= $result->sites[0]->location->lat;
        }
 

          
        }
        return $location;


      }catch(GuzzleException $e){
      dd($e->getMessage());
      Log::info("Merchant Timezone not found for use Id.. " . $request['user_id']);
      Log::info("Timezone Error_desc...". $e->getFile());
      Log::info("Timezone Error_message..". $e->getMessage());
    }
  }
}
