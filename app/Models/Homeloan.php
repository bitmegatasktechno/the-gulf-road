<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Homeloan extends Eloquent
{
    public $timestamps = true;

    protected $collection = 'homeloan';
    protected $primaryKey = '_id';
    protected $guarded = [];
}
