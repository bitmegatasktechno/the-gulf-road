<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Award extends Model
{
    public $timestamps = true;

    protected $collection = 'awards';
    protected $primaryKey = '_id';
    protected $guarded = [];

    // public function user(){
    // 	return $this->belongsTo(\App\Models\User::class,'user_id','_id');
    // }
}
