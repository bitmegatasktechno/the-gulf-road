<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use Auth;

class CoworkspaceProperty extends Model
{
    public $timestamps = true;

    protected $collection = 'coworkspace_properties';
    protected $primaryKey = '_id';
    protected $guarded = [];

    public function user(){
    	return $this->belongsTo(\App\Models\User::class,'user_id','_id');
    }
    public function transaction(){
    	return $this->belongsTo(\App\Models\Transaction::class,'transaction_id','_id');
    }
    public function getLatLong()
  	{
  		$address = $this->address;
	    $result=[];
	      $client = new \GuzzleHttp\Client();
	    try{

	    	$location = [];
			$location['lng']='-79.3831843';
			$location['lat']='43.653226';

	    	if(env('APP_ACTIVE_MAP')=='google_map')
	    	{
	    		
				$res = $client->request('GET', 'https://maps.googleapis.com/maps/api/geocode/json', [
					'query' => [
					'address' => $address,
					'key' => env('GOOGLE_MAP_KEY')
					]
				]);
				$result = json_decode($res->getBody()->getContents());
				if(isset($result->results[0]))
				{
					$location['lng']= $result->results[0]->geometry->location->lng;
					$location['lat']= $result->results[0]->geometry->location->lat;
				}
	    	}else if(env('APP_ACTIVE_MAP')=='patel_map')
	    	{



	    		 
	    		$res = $client->request('POST', 'https://siteapi.cloud.huawei.com/mapApi/v1/siteService/geocode?key='.(env('PATEL_MAP_KEY')), [
					'json' => [
						 
						'address' 		=> ($address),
						'language' 		=> 'en'
					]
				]);
				
				$result = json_decode($res->getBody()->getContents());
				  
				 if($result->returnDesc =='OK')
				{
					$location['lng']= $result->sites[0]->location->lng;
					$location['lat']= $result->sites[0]->location->lat;
				}
 

	    		
	    	}
 	      return $location;


	    }catch(GuzzleException $e){
	      dd($e->getMessage());
	      Log::info("Merchant Timezone not found for use Id.. " . $request['user_id']);
	      Log::info("Timezone Error_desc...". $e->getFile());
	      Log::info("Timezone Error_message..". $e->getMessage());
	    }
	}
	public function getDistance()
	{
		if(Auth::user())
		{
			$user = Auth::user();
			$lon1   = $this->longitude; 
			$lon2  = $user->longitude; 
			$lat1  = $this->latitude; 
			$lat2  = $user->latitude; 
			if (($lat1 == $lat2) && ($lon1 == $lon2)) {
				$km = 0;
			}else{
		    	$theta = $lon1 - $lon2;
		    	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
			    $dist = acos($dist);
			    $dist = rad2deg($dist);
			    $miles = $dist * 60 * 1.1515;
		      	$km=$miles*1.609344;
		    }
			return $km; 
		}
	}
}
