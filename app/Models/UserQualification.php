<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class UserQualification extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'user_id',
        'education',
        'hobbies'
    ];

    protected $casts = [
        'user_id' => 'string',
    ];
}
