<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Transaction extends Model
{
    public $timestamps = true;

    protected $collection = 'transaction';
    protected $primaryKey = '_id';
    protected $guarded = [];

    // public function user(){
    // 	return $this->belongsTo(\App\Models\User::class,'user_id','_id');
    // }

    public function subscription(){
        return $this->belongsTo(\App\Models\Subscription::class,'plan_id','_id');
      }

      public function user(){
        return $this->belongsTo(\App\Models\User::class,'user_id','_id');
      }
}
