<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class City extends Eloquent
{
    public $timestamps = false;

    protected $collection = 'cities';
    protected $primaryKey = '_id';
    protected $guarded = [];

    public function location(){
        return $this->belongsTo(Location::class, 'location_id','_id');
    }
}
