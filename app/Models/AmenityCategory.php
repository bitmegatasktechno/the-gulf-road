<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class AmenityCategory extends Model
{
    public $timestamps = false;

    protected $collection = 'amenity_categories';
    protected $primaryKey = '_id';
    protected $guarded = [];

    public function amentities(){
    	return $this->hasMany(SpecialAmenity::class, 'category_id','_id');
    }
}
