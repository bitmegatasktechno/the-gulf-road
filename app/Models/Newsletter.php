<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Newsletter extends Eloquent
{
    public $timestamps = true;

    protected $collection = 'newsletter';
    protected $primaryKey = '_id';
    protected $guarded = [];
}
