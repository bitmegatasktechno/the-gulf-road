<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Subscribe extends Eloquent
{
    public $timestamps = true;

    protected $collection = 'subscribe';
    protected $primaryKey = '_id';
    protected $guarded = [];
}
