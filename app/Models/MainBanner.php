<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class MainBanner extends Model
{
    protected $table = 'main_banners';
    protected $fillable = ['title', 'image'];
}
