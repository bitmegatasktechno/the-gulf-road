<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Review extends Model
{
    public $timestamps = true;

    protected $collection = 'user_reviews';
    protected $primaryKey = '_id';
    protected $fillable = [
        'user_id', 'hygine','service','location','cleanliness','atmosphere','review','review_type','type_id','is_approved','create','average'
    ];

    public function user(){
      return $this->belongsTo(\App\Models\User::class,'user_id','_id');
    }

    public function user_type(){
      return $this->belongsTo(\App\Models\User::class,'type_id','_id');
    }


    public function buy(){
      return $this->belongsTo(\App\Models\Property::class,'type_id','_id');
    }

    public function swap(){
      return $this->belongsTo(\App\Models\SwapProperty::class,'type_id','_id');
    }

    public function co(){
      return $this->belongsTo(\App\Models\CoworkspaceProperty::class,'type_id','_id');
    }
}
