<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Amenity extends Model
{
    public $timestamps = false;

    protected $collection = 'amenities';
    protected $primaryKey = '_id';
    protected $guarded = [];
}
