<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class ContactUs extends Model
{
    public $timestamps = false;

    protected $collection = 'contact_us';
    protected $primaryKey = '_id';
    protected $guarded = [];
}
