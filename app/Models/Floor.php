<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Floor extends Model
{
    public $timestamps = false;

    protected $collection = 'flooring';
    protected $primaryKey = '_id';
    protected $guarded = [];
}
