<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Banner extends Model
{
    public $timestamps = false;

    protected $collection = 'banner';
    protected $primaryKey = '_id';
    protected $guarded = [];
}
