<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Favourate extends Model
{
    public $timestamps = false;

    protected $collection = 'favourate';
    protected $primaryKey = '_id';
    protected $guarded = [];

    public function property(){
        return $this->belongsTo(\App\Models\Property::class,'propert_id','_id');
    }

    public function swap(){
    	return $this->belongsTo(\App\Models\SwapProperty::class,'propert_id','_id');
    }

    public function cospace(){
    	return $this->belongsTo(\App\Models\CoworkspaceProperty::class,'propert_id','_id');
    }

}
