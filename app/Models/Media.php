<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media';
    protected $fillable = ['title','image','video','media_type'];
}
