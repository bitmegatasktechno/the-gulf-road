<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class CospaceStory extends Model
{
    public $timestamps = false;

    protected $collection = 'cospace_story';
    protected $primaryKey = '_id';
    protected $guarded = [];
    protected $fillable = [
        '_id', 'name','status','description','logo'
    ];
}
