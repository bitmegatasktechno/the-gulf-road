<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class UserEnquiry extends Model
{
    public $timestamps = true;

    protected $collection = 'user_enquiries';
    protected $primaryKey = '_id';
    protected $fillable = [
        'name', 'email','phone','message','property_id','is_action','create','user_id'
    ];

    public function property(){
    	return $this->belongsTo(\App\Models\Property::class,'property_id','_id');
    }

    public function swap(){
    	return $this->belongsTo(\App\Models\SwapProperty::class,'property_id','_id');
    }

    public function cospace(){
    	return $this->belongsTo(\App\Models\CoworkspaceProperty::class,'property_id','_id');
    }

    public function sender(){
    	return $this->belongsTo(\App\Models\User::class,'sender_id','_id');
    }

    public function reciever(){
    	return $this->belongsTo(\App\Models\User::class,'user_id','_id');
    }
}
