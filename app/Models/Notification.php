<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Notification extends Eloquent
{
    public $timestamps = true;

    protected $collection = 'notifications';
    protected $primaryKey = '_id';
    protected $guarded = [];

    public function user(){
        return $this->belongsTo(\App\Models\User::class,'sender_id','_id');
      }
}
