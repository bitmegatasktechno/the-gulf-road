<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class BookProperty extends Model
{
    public $timestamps = true;

    protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $collection = 'book_property';
    protected $primaryKey = '_id';

    public function user(){
        return $this->belongsTo(\App\Models\User::class,'user_id','_id');
      }

      public function owner(){
        return $this->belongsTo(\App\Models\User::class,'touser','_id');
      }

      public function property(){
        return $this->belongsTo(\App\Models\Property::class,'property_id','_id');
      }

      public function swap(){
        return $this->belongsTo(\App\Models\Property::class,'property_id','_id');
      }
}
