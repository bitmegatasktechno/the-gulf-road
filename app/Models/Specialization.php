<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Specialization extends Model
{
    public $timestamps = true;

    protected $collection = 'specialization';
    protected $primaryKey = '_id';
    protected $guarded = [];

    // public function user(){
    // 	return $this->belongsTo(\App\Models\User::class,'user_id','_id');
    // }
}
