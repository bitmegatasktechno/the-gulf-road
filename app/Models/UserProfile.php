<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class UserProfile extends Model
{
    public $timestamps = false;
    protected $collection = 'user_profiles';
    protected $primaryKey = '_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'user_id',
        'profile_pic',
        'marriage_status',
        'preffered_language',
        'preffered_currency',
        'working_since',
        'residence_id_number',
        'passport_number',
        'driving_licence',
        'licence_number',
        'whatsapp_number',
        'whatsapp_number_same',
    ];

    protected $casts = [
        'user_id' => 'string',
    ];
}
