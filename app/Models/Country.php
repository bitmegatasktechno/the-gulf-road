<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Country extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'iso'
    ];
}
