<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class ContactOwner extends Model
{
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $collection = 'contact_owner';
    protected $primaryKey = '_id';
    protected $fillable = [
        'startdate', 'enddate','message','type','status','user_id','property_id'
    ];
}
