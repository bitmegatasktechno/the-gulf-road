<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Alexmg86\LaravelSubQuery\Traits\LaravelSubQueryTrait;
use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Auth\User as Authenticatable;
use Mail;
use App\Mail\{
    NewUserNotification, ForgotPasswordNotification
};

class User extends Authenticatable
{
    // use LaravelSubQueryTrait;
    use Notifiable;

    protected $collection = 'users';
    protected $primaryKey = '_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'first_name',
        'middle_name',
        'last_name',
        'father_name',
        'gender',
        'email', 
        'password', 
        'phone', 
        'country',
        'countryCode',
        'role',
        'image',
        'provider_id',
        'provider',
        'registerVia',
        'status',
        'defaultLoginRole',
        'email_token',
        'email_verified',
        'logged_first_time',
        'about',
        'address',
        'buddyProfile',
        'agentProfile',
        'buddyProfileSteps',
        'agentProfileSteps',
        'date_of_birth',
        'rating',
        'charges',
        'charge_currency',
        'charge_type'
    ];


    /**
    * get all countties associated with user
    */
    public function userCountry(){
        return $this->belongsTo('Country', 'country', 'iso');
    }

    public function getAgeAttribute(){
        $birthDate = $this->date_of_birth;
        if(!$birthDate){
            return '0';
        }
        $birthDate = explode("/", $birthDate);

        //get age from date or birthdate
        $age = (date("Y") - $birthDate[2]);
        return $age;
    }

    /**
    * get image path of uploaded image.
    **/
    public function getImagePathAttribute(){
        if($this->image=='' || $this->image==null){
            return '/assets/img/avatar.png';
        }
        return url('uploads/profilePics/'.$this->image);
    }


    /**
    * send account activation email to user
    **/
    public static function sendAccountActivationMail($email,$email_token) {      
        $data['link'] =url('/verifyemail/'.$email_token);
        Mail::to($email)->send(new NewUserNotification($data));
    }

    /**
    * Send password reset email to user
    **/
    public static function sendResetEmail($email, $token){
        try{
            //Retrieve the user from the database
            $user = User::where('email', $email)->select('email')->first();
            //Generate, the password reset link. The token generated is embedded in the link
            $link = url('/') . '/password/reset/' . $token . '?email=' . urlencode($user->email);
            $data['user'] = $user;
            $data['link'] = $link;

            Mail::to($email)->send(new ForgotPasswordNotification($data));
        }
        catch(\Exception $e){
            
        }
    }

    public function profiles(){
        return $this->hasOne(UserProfile::class, 'user_id','_id');
    }



    public function addresses(){
        return $this->hasOne(UserAddress::class, 'user_id','_id');
    }

    public function reviews(){
        return $this->hasMany(Review::class, 'type_id','_id')->where('is_approved',1)->where('review_type',"7");
    }
    public function agentreviews(){
        return $this->hasMany(Review::class, 'type_id','_id')->where('is_approved',1)->where('review_type',"6");
    }
    public function professionals(){
        return $this->hasMany(UserProfessionalDetail::class, 'user_id','_id');
    }

    public function documents(){
        return $this->hasMany(UserDocument::class, 'user_id','_id');
    }

    public function qualifications(){
        return $this->hasOne(UserQualification::class, 'user_id','_id');
    }


    public function rentProperties(){
        return $this->hasMany(Property::class, 'user_id','_id')->where('listing_type','rent')->where('is_approved',1);
    }

    public function saleProperties(){
        return $this->hasMany(Property::class, 'user_id','_id')->where('listing_type','sale')->where('is_approved',1);
    }

    public function coworkspaceProperties(){
        return $this->hasMany(CoworkspaceProperty::class, 'user_id','_id')->where('is_approved',1);
    }

    public function swapProperties(){
        return $this->hasMany(SwapProperty::class, 'user_id','_id')->where('is_approved',1);
    }

    public function buddyProfessional(){
        return $this->hasOne(UserProfessionalDetail::class, 'user_id','_id')->where('role','BUDDY');
    }

    public function agentProfessional(){
        return $this->hasOne(UserProfessionalDetail::class, 'user_id','_id')->where('role','AGENT');
    }

    // return complete current address
    public function getFullAddressAttribute(){
        $address = UserAddress::where('user_id', $this->_id)->first();
        if(!$address){
            return 'N/A';
        }else{
            $full_address = '';
            $full_address.=$address->current_location;
            $full_address=$full_address.', '.$address->current_unit_number;
            if($address->current_address_1){
                $full_address=$full_address.' '.$address->current_address_1;
            }
            if($address->current_address_2){
                $full_address=$full_address.' '.$address->current_address_2;
            }
            if($address->current_landmark){
                $full_address=$full_address.' '.$address->current_landmark;
            }
            $full_address=$full_address.', '.$address->current_city;
            $full_address=$full_address.', '.$address->current_country;
            $full_address=$full_address.', '.$address->current_state;
            $full_address=$full_address.'-'.$address->current_zipcode;
            return $full_address;
        }
    }

    /**
    * boot method of eloquent will remove all related data of user.
    */
    public static function boot() {
        parent::boot();
        // before delete() method call this
        static::deleting(function($user) {
            $id = new \MongoDB\BSON\ObjectID($user->_id);
            UserProfile::where('user_id', $id)->delete();
            UserAddress::where('user_id', $id)->delete();
            UserProfessionalDetail::where('user_id', $id)->delete();
            UserDocument::where('user_id', $id)->delete();
            UserQualification::where('user_id', $id)->delete();
            Property::where('user_id', $id)->delete();
            CoworkspaceProperty::where('user_id', $id)->delete();
            SwapProperty::where('user_id', $id)->delete();
        });
    }
}