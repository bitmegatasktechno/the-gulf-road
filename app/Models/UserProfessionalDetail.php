<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class UserProfessionalDetail extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'user_id',
    	'role',
        'languages',
        'localities',
        'real_estate_agent',
        'deal_property_in_uae',
        'rera_number',
        'permit_number',
        'availability',
        'timing',
        'other_countries'
    ];

    // protected $casts = [
    //     'user_id' => 'string',
    // ];
}
