<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Location extends Eloquent
{
    public $timestamps = false;

    protected $collection = 'locations';
    protected $primaryKey = '_id';
    protected $guarded = [];

    public function cities(){
        return $this->hasMany(City::class, 'location_id','_id');
    }
}
