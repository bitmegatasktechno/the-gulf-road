<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class HouseType extends Model
{
    public $timestamps = false;

    protected $collection = 'house_types';
    protected $primaryKey = '_id';
    protected $guarded = [];
}
