<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;
use App\Models\Property;
use App\Models\SwapProperty;
use App\Models\CoworkspaceProperty;

class Report extends Model
{
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $collection = 'report_listing';
    protected $primaryKey = '_id';
    protected $fillable = [
        'property_id','type','status','user_id'
    ];

    public function user(){
      return $this->belongsTo(\App\Models\User::class,'user_id','_id');
    }
    public function property()
    {
        if($this->type == 'buy' || $this->type == 'rent')
        {
            $property = Property::where('_id',$this->property_id)->first();
            return $property->property_title;
        }elseif($this->type == 'swap')
        {
            $property = SwapProperty::where('_id',$this->property_id)->first();
            return $property->title;
        }else{
            $property = CoworkspaceProperty::where('_id',$this->property_id)->first();
            return $property->title;
        }
    }
}
