<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Language extends Eloquent
{
    public $timestamps = false;

    protected $collection = 'languages';
    protected $primaryKey = '_id';
    protected $guarded = [];
}
