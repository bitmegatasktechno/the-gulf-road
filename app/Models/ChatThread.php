<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class ChatThread extends Model
{
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $collection = 'chat_thread';
    protected $primaryKey = '_id';
    protected $fillable = [
        'sender_id', 'reciever_id','status','message'
    ];

    public function reciever(){
    	return $this->belongsTo(\App\Models\User::class,'reciever_id','_id');
    }

    public function sender(){
    	return $this->belongsTo(\App\Models\User::class,'sender_id','_id');
    }
}
