<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Testimonial extends Model
{
    public $timestamps = true;

    protected $collection = 'testimonials';
    protected $primaryKey = '_id';
    protected $guarded = [];

    // public function user(){
    // 	return $this->belongsTo(\App\Models\User::class,'user_id','_id');
    // }
}
