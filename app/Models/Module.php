<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Module extends Eloquent
{
    public $timestamps = true;

    protected $collection = 'modules';
    protected $primaryKey = '_id';
    protected $guarded = [];
}
