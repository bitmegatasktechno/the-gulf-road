<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class ChatMessage extends Model
{
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $collection = 'chat_message';
    protected $primaryKey = '_id';
    protected $fillable = [
        'sender_id', 'reciever_id','thraed_id','message','status'
    ];

    public function reciever(){
    	return $this->belongsTo(\App\Models\User::class,'reciever_id','_id');
    }

    public function sender(){
    	return $this->belongsTo(\App\Models\User::class,'sender_id','_id');
    }
}
