<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Subscription extends Model
{
    public $timestamps = true;

    protected $collection = 'subscription';
    protected $primaryKey = '_id';
    protected $guarded = [];

    // public function user(){
    // 	return $this->belongsTo(\App\Models\User::class,'user_id','_id');
    // }
    public function transactions(){
        return $this->hasMany(Transaction::class, 'plan_id','_id');
    }
}
