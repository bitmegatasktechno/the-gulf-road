<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Blog extends Model
{
    public $timestamps = false;
    public $table='blog';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pic','title', 'description','user_id'
    ];

    public function user(){
        return $this->belongsTo(\App\Models\User::class,'user_id','_id');
    }


}
