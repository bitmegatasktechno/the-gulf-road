<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class UserDocument extends Model
{
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'user_id',
    	'role',
        'documents',
    ];

    protected $casts = [
        'user_id' => 'string',
    ];
}
