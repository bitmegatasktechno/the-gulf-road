<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Video extends Model
{
    public $timestamps = false;

    protected $collection = 'videos';
    
    protected $primaryKey = '_id';
    protected $fillable = [
        'user_type','video','status'
    ];
    
}
