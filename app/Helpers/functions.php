<?php
if (!function_exists('pr')) {
	function pr($var) {
		echo '<pre>';
		print_r($var);
		echo '</pre>';
	}
}

/**
* get all contries list
*/

function getAllCountries(){
    return \App\Models\Country::all();
}

if(!function_exists('convert_your_currency'))
{
	function convert_your_currency($from_currency='USD',$to_currency='USD',$base_price=1)
	{
		$req_url = 'https://v6.exchangerate-api.com/v6/'.env("EXCHANGE_CURRENCY_API_KEY").'/latest/'.$from_currency;
		$response_json = file_get_contents($req_url);
		// Continuing if we got a result
		if(false !== $response_json) {

		    // Try/catch for json_decode operation
		    try {

				// Decoding
				$response = json_decode($response_json);
				// Check for success
				if('success' == $response->result) 
				{
					$responce_currency = round($response->conversion_rates->$to_currency,2);
					return $EUR_price = round(($base_price * $responce_currency), 2);

				}

		    }
		    catch(Exception $e) {
		          return $base_price;
		    }

		}
	}
}
/*
*get all city of country
*/
function getCountryCities()
{
	 
	$loc=Request::segment(3);
	if($loc == 'ksa' || $loc == 'uae' )
	{
	   $country = strtoupper($loc);
	}else 
	{
	    $country = ucfirst($loc);
	}
	$cities = \App\Models\City::whereHas('location',function($q) use($country){
            $q->where('name',$country);
        })->get();
	 return $cities;
}

/**
* get all country codes
*/

function getAllCodes(){
	$codes = file_get_contents(base_path('/data/codes.json'));
	$codes = json_decode($codes, true);
	$codes =array_filter($codes, function($value) { return $value['dial_code']!='null' && $value['dial_code'] !='' && $value['dial_code'] !=' '; });
	return $codes;
}

function getAllCodesWithName(){
	$codes = file_get_contents(base_path('/data/codes.json'));
	$codes = json_decode($codes, true);
	$codes =array_filter($codes, function($value) {
		if($value['dial_code']!=''){
			return ['dial_code'=>$value['dial_code'], 'name'=>$value['name']];
		}
	});
	return $codes;
}
function getAllCodeWithName(){
	// $codes = file_get_contents(base_path('/data/code.json'));
	// $codes = json_decode($codes, true);
	// $codes =array_filter($codes, function($value) {
	// 	if($value['dial_code']!=''){
	// 		return ['dial_code'=>$value['dial_code'], 'name'=>$value['name']];
	// 	}
	// });
	// return $codes;


	$locations = \App\Models\Location::where('status',1)->orderBy('_id','desc')->get();
	$data = [];
	foreach($locations as $key=>$location)
	{
		if($location->name == 'UAE')
		{
			$data[$key]['name'] = 'United Arab Emirates';
			$data[$key]['dial_code'] = '+971';
			$data[$key]['code'] = 'AE';
		}elseif($location->name == 'KSA'){
			$data[$key]['name'] = 'Saudi Arabia';
			$data[$key]['dial_code'] = '+966';
			$data[$key]['code'] = 'SA';
		}elseif($location->name == 'Qatar'){
			$data[$key]['name'] = 'Qatar';
			$data[$key]['dial_code'] = '+974';
			$data[$key]['code'] = 'QA';
		}elseif($location->name == 'Oman'){
			$data[$key]['name'] = 'Oman';
			$data[$key]['dial_code'] = '+968';
			$data[$key]['code'] = 'OM';
		}elseif($location->name == 'Kuwait'){
			$data[$key]['name'] = 'Kuwait';
			$data[$key]['dial_code'] = '+965';
			$data[$key]['code'] = 'KW';
		}elseif($location->name == 'KSA'){
			$data[$key]['name'] = 'Bahrain';
			$data[$key]['dial_code'] = '+973';
			$data[$key]['code'] = 'BH';
		}
	}
	return $data;
}

/*
* get all locales
*/

function getAllLocales(){
	$locales = config('app.supported_locales');
	return $locales;
}

function getAllLocations(){
    return \App\Models\Location::where('status',1)->get();
}

function getAllAmenities(){
    return \App\Models\Amenity::where('status',1)->get();
}

function getAllFlooring(){
    return \App\Models\Floor::where('status',1)->get();
}

function getTotalSaleProperties(){
	return \App\Models\Property::where('listing_type','sale')->count();
}
function getTotalRentProperties(){
	return \App\Models\Property::where('listing_type','rent')->count();
}

function getAllHouseTypes(){
	return \App\Models\HouseType::where('status', 1)->get();
}

function getAllCurrencies(){
	return \App\Models\Currency::where('status', 1)->get();
}

function getAllLanguages(){
	return \App\Models\Language::where('status', 1)->get();
}

function getAllLanguage(){
	$codes = file_get_contents(base_path('/data/languages.json'));
	$codes = json_decode($codes, true);
	$codes =array_filter($codes, function($value) { return $value['name']!='null' && $value['name'] !='' && $value['name'] !=' '; });
	return $codes;
}

function getFurnishingTypes(){
	return [
		'fully_furnished'=>'Fully Furnished',
		'semi_furnished'=>'Semi Furnished',
		'unfurnished'=>'Unfurnished',
	];
}

function getAllRentFrequency(){
	return [
		'daily'=>'Daily',
		'weekly'=>'Weekly',
		'monthly'=>'Monthly',
		'yearly'=>'Yearly',
		'quarterly'=>'Quarterly'
	];
}

function getAllSpecialAmenities(){
	return \App\Models\SpecialAmenity::where('status', 1)->get();
}

function getAllAmentityCategory()
{
	return \App\Models\AmenityCategory::where('status', 1)->whereHas('amentities', function($q){
		$q->where('status',1);
	})->with('amentities')->get();
}

function getSelectedCountryStates($country){
	$states = file_get_contents(base_path('/data/states.json'));
    $states = json_decode($states, true);
    $states = $states['countries'];
    $country = $country;
    $state_data = [];
    foreach ($states as $state) {
        if(strtolower($state['country'])==strtolower($country))
        {
            $state_data = $state['states'];
        }
    }
	return $state_data;
}
function getUserRoleStatus()
{
	if(Auth::user())
	{
		if(in_array("BUDDY",Auth::user()->role) || in_array("AGENT",Auth::user()->role))
		{
			return 'readonly';
		}
	}
	return "";
}
function checkSubscription()
{
	if(Auth::user())
	{
		$expired = \App\Models\Transaction::where('user_id',Auth::user()->_id)->whereIn('purchase_type',['swap','rent','cospace'])->where('expiry_date','<',date("Y-m-d"))->where('renew','!=',(int)1)->count();
		if($expired){
			return true;
		}
	}
	return false;
}