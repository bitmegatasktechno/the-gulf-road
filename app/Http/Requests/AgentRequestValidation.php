<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class AgentRequestValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $step = Request::segment(4);

        switch ($step) {
            case '1':
                return [
                    //'profile_pic' =>'bail|required|image|mimes:jpeg,jpg,JPG,JPEG|max:2048',
                    'gender'=>'bail|required|string|in:male,other,female',
                    'first_name'=>'bail|required|string|max:50',
                    'last_name'=>'bail|required|string|max:50',
                    'middle_name'=>'bail|nullable|string|max:50',
                    'father_name'=>'bail|required|string|max:50',
                    'phone' => 'bail|required|numeric|regex:/[1-9]{1}[0-9]{5,14}/|digits_between:6,15',
                    // 'country' => 'bail|required|string|max:50',
                    'country_code' => 'bail|required|string|max:50',
                    'marriage_status' => 'bail|required|string|max:50',
                    'date_of_birth' => 'bail|required|date_format:d/m/Y|before:today',
                    'preffered_language' => 'bail|required|string|max:250',
                    'preffered_currency' => 'bail|required|string|max:50',
                    'working_since' => 'bail|required|digits:4|numeric|min:1900|max:'.date("Y"),
                    'residence_id_number' => 'bail|max:100',
                    'passport_number' => 'bail|required|string|max:50',
                    'driving_licence' => 'bail|required|in:yes,no',
                    // 'licence_number' => 'bail|required_if:driving_licence,yes|max:50',
                    'licence_number' => 'bail|max:50',
                    'whatsapp_number_same' => 'bail|required|in:yes,no',
                    'whatsapp_number' => 'bail|required_if:whatsapp_number_same,no|nullable|numeric|regex:/[1-9]{1}[0-9]{5,14}/|digits_between:6,15',
                ];
                break;

            case '2':
                return [
                    'current_location'=>'bail|required|string|max:250',
                    'current_unit_number'=>'bail|required|string|max:250',
                    // 'current_address_1'=>'bail|required|string|max:250',
                    // 'current_address_2'=>'bail|required|string|max:250',
                    'current_landmark'=>'bail|max:250',
                    'current_city'=>'bail|required|string|max:250',
                    'current_country'=>'bail|required|string|max:250',
                    'current_state'=>'bail|required|string|max:250',
                    'current_zipcode'=>'bail|required|string|max:250',
                    // native
                    'native_location'=>'bail|required|string|max:250',
                    'native_unit_number'=>'bail|required|string|max:250',
                    // 'native_address_1'=>'bail|required|string|max:250',
                    // 'native_address_2'=>'bail|required|string|max:250',
                    'native_landmark'=>'bail|max:250',
                    'native_city'=>'bail|required|string|max:250',
                    'native_country'=>'bail|required|string|max:250',
                    'native_state'=>'bail|required|string|max:250',
                    'native_zipcode'=>'bail|required|string|max:250', 
                ];
                break;

            case '3':
                return [
                   	'languages'=>'bail|required',
                    'localities'=>'bail|required',
                    'real_estate_agent'=>'bail|required|in:yes,no',
                    'deal_property_in_uae'=>'bail|required_if:real_estate_agent,yes|in:inside,outside',
                    // 'rera_number'=>'bail|required_if:real_estate_agent,yes|max:50',
                    // 'permit_number'=>'bail|required_if:real_estate_agent,yes|max:50',
                    // time
                    'monday_start_time'=>'bail|nullable|required_if:monday,1|date_format:H:i',
                    'monday_end_time'=>'bail|nullable|required_if:monday,1|date_format:H:i|after:monday_start_time',
                    // tuesday
                    'tuesday_start_time'=>'bail|nullable|required_if:tuesday,1|date_format:H:i',
                    'tuesday_end_time'=>'bail|nullable|required_if:tuesday,1|date_format:H:i|after:tuesday_start_time',
                    // wednesday
                    'wednesday_start_time'=>'bail|nullable|required_if:wednesday,1|date_format:H:i',
                    'wednesday_end_time'=>'bail|nullable|required_if:wednesday,1|date_format:H:i|after:wednesday_start_time',
                    // thursday
                    'thursday_start_time'=>'bail|nullable|required_if:thursday,1|date_format:H:i',
                    'thursday_end_time'=>'bail|nullable|required_if:thursday,1|date_format:H:i|after:thursday_start_time',
                    // friday
                    'friday_start_time'=>'bail|nullable|required_if:friday,1|date_format:H:i',
                    'friday_end_time'=>'bail|nullable|required_if:friday,1|date_format:H:i|after:friday_start_time',
                    // saturday
                    'saturday_start_time'=>'bail|nullable|required_if:saturday,1|date_format:H:i',
                    'saturday_end_time'=>'bail|nullable|required_if:saturday,1|date_format:H:i|after:saturday_start_time',
                    // sunday
                    'sunday_start_time'=>'bail|nullable|required_if:sunday,1|date_format:H:i',
                    'sunday_end_time'=>'bail|nullable|required_if:sunday,1|date_format:H:i|after:sunday_start_time',
                ];
                break;

            case '4':
                return [
                    'documents'=>'required',
                    'documents.*' => 'mimes:doc,pdf,docx,zip,jpeg,jpg,JPG,JPEG,png|max:2048'
                ];
                break;

            case '5':
                return [
                    'course_name.*'=>'bail|required|string|max:100',
                    'university_name.*'=>'bail|required|string|max:100',
                    'hobby.*'=>'bail|required|string|max:100',
                ];
                break;
                                    
            default:
                # code...
                break;
        }
        return [
            //
        ];
    }

    public function messages(){
        return [
            'whatsapp_number.required_if'=>'The whatsapp number is required.',
            'licence_number.required_if'=>'The licence number is required.',
            // 'documents.*.mimes'=>'The Document format is invalid.',
            'course_name.*.required'=>'The Course name is required.',
            'university_name.*.required'=>'The University name is required.',
            'hobby.*.required'=>'The hobby name is required.',
            
            'monday_start_time.required_if'=>'The start time is required.',
            'monday_end_time.required_if'=>'The end time is required.',
            'monday_end_time.after'=>'The end time must be greater than start time.',

            'tuesday_start_time.required_if'=>'The start time is required.',
            'tuesday_end_time.required_if'=>'The end time is required.',
            'tuesday_end_time.after'=>'The end time must be greater than start time.',

            'wednesday_start_time.required_if'=>'The start time is required.',
            'wednesday_end_time.required_if'=>'The end time is required.',
            'wednesday_end_time.after'=>'The end time must be greater than start time.',

            'thursday_start_time.required_if'=>'The start time is required.',
            'thursday_end_time.required_if'=>'The end time is required.',
            'thursday_end_time.after'=>'The end time must be greater than start time.',

            'friday_start_time.required_if'=>'The start time is required.',
            'friday_end_time.required_if'=>'The end time is required.',
            'friday_end_time.after'=>'The end time must be greater than start time.',

            'saturday_start_time.required_if'=>'The start time is required.',
            'saturday_end_time.required_if'=>'The end time is required.',
            'saturday_end_time.after'=>'The end time must be greater than start time.',

            'sunday_start_time.required_if'=>'The start time is required.',
            'sunday_end_time.required_if'=>'The end time is required.',
            'sunday_end_time.after'=>'The end time must be greater than start time.',

            'monday_start_time.date_format'=>'The start time format is not correct.',
            'monday_end_time.date_format'=>'The end time format is not correct.',

            'tuesday_start_time.date_format'=>'The start time format is not correct.',
            'tuesday_end_time.date_format'=>'The start time format is not correct.',

            'wednesday_start_time.date_format'=>'The start time format is not correct.',
            'wednesday_end_time.date_format'=>'The start time format is not correct.',

            'thursday_start_time.date_format'=>'The start time format is not correct.',
            'thursday_end_time.date_format'=>'The start time format is not correct.',

            'friday_start_time.date_format'=>'The start time format is not correct.',
            'friday_end_time.date_format'=>'The start time format is not correct.',

            'saturday_start_time.date_format'=>'The start time format is not correct.',
            'saturday_end_time.date_format'=>'The start time format is not correct.',

            'sunday_start_time.date_format'=>'The start time format is not correct.',
            'sunday_end_time.date_format'=>'The start time format is not correct.',
        ];
    }
}
