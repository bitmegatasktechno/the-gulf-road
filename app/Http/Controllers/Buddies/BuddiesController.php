<?php

namespace App\Http\Controllers\Buddies;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\{User};
use Auth;
class BuddiesController extends Controller{
    /**
    * list all buddies
    */
    public function getBuddiesLists(Request $request){
        $latlongData = [];
         $loc=request()->segment(3);
        if($loc == 'ksa' || $loc == 'uae' )
        {
           $loc21 = strtoupper($loc);
        }else 
        {
            $loc21 = ucfirst($loc);
        }
        
        $defaultCountry = $loc21;
        $users = User::whereIn('role',['BUDDY'])->where('country',$defaultCountry);
        if($request->filled('country')){
        	$users->where('country', 'like', '%' . $request->input('country'));
        }
        if($request->filled('location')){
            $location = $request->input('location');
            // $users->whereHas('addresses', function($q) use($location){
            //     $q->where('current_location', 'like', '%' . $location . '%')
            //       ->orWhere('current_city', 'like', '%' . $location . '%')
            //       ->orWhere('current_state', 'like', '%' . $location . '%')
            //       ->orWhere('current_zipcode', 'like', '%' . $location . '%')
            //       ->orWhere('current_country', 'like', '%' . $location . '%');
            // });
            $latitude = (float)$request->latitude;
            $longitude = (float)$request->longitude;
            
           $users->where('location', 'near', [
                '$geometry' => [
                    'type' => 'Point',
                    'coordinates' => [
                        $longitude, // longitude
                        $latitude, // latitude
                    ],
                ],
                '$maxDistance' => 10000,
            ]); 
        }
        if(Auth::user())
        {
            $users = $users->where('_id','!=',Auth::user()->_id);
        }
        $data = $users->where('status', 1)->with(['addresses','reviews'])->orderBy('created_at', 'desc')->paginate(12);
        foreach ($data->items() as $key => $value) {
            if($value->latitude && $value->longitude){
                $latlongData[]= [$value->name, $value->latitude, $value->longitude];
            }
        }
        $map_data = $users->paginate(12);
        if($request->ajax()){
            return view('frontend.users-listing.buddies.list', compact('data','map_data','latlongData'));
        }
        return view('frontend.users-listing.buddies.index', compact('data','map_data','latlongData'));
    }
}