<?php

namespace App\Http\Controllers\Agent;
use App\Models\{User,Video,Property,Blog};
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use AmrShawky\LaravelCurrency\Facade\Currency;


class AgentController extends Controller{
    /**
    * list all agents
    */
    public function getLandingAgentsList(){
        $defaultCountry = request()->segment(3);
        $defaultLanguage = request()->segment(1);
        $defaultCountry = request()->segment(3);

        //////////////for delete unwanted properties///////////
        $users_check = User::pluck('_id')->toArray();
        $proerties_check = Property::all();
        foreach($proerties_check as $check)
        {
            if(!in_array($check->user_id,$users_check))
            {
                $delete = Property::where('_id',$check->_id)->delete();
            }
        }
        //////////////for delete unwanted properties///////////

        $video=Video::where('user_type',"1")->first();
        $allProperties = Property::where('listing_type', 'rent')->where('country',$defaultCountry)->orderBy('_id','DESC')->get();
        $rentProperties = Property::where('listing_type', 'rent')->where('country',$defaultCountry)->where('is_premium',1)->orderBy('_id','DESC')->limit(3)->get();
        $saleProperties = Property::where('listing_type', 'sale')->where('country',$defaultCountry)->where('is_premium',1)->orderBy('_id','DESC')->limit(2)->get();
    	if(\Auth::user()){
        	$buddies = User::whereIn('role',['BUDDY'])->where('_id','!=',\Auth::user()->id)->where('country',$defaultCountry)->where('status',1)->whereIn('top_list',['BUDDY'])->orderBy('_id','DESC')->get();
        }else{
            $buddies = User::whereIn('role',['BUDDY'])->where('country',$defaultCountry)->where('status',1)->whereIn('top_list',['BUDDY'])->orderBy('_id','DESC')->get();
        }
        if(\Auth::user()){
        	$results = User::with('agentreviews')->whereIn('role',['AGENT'])->where('_id','!=',\Auth::user()->id)->where('country',$defaultCountry)->where('status',1)->where('top_list', 'AGENT')->whereIn('top_list',['AGENT'])->take(10)->orderBy('_id','DESC')->get();
        	if($results->count() == 0){
	        	$results = User::with('agentreviews')->whereIn('role',['AGENT'])->where('_id','!=',\Auth::user()->id)->where('country',$defaultCountry)->where('status',1)->where('top_list', 'AGENT')->whereIn('top_list',['AGENT'])->take(10)->orderBy('_id','DESC')->get();
        	}
        }else{
            $results = User::with('agentreviews')->whereIn('role',['AGENT'])->where('country',$defaultCountry)->where('status',1)->whereIn('top_list',['AGENT'])->orderBy('_id','DESC')->whereIn('top_list',['AGENT'])->take(10)->get();
            if($results->count() == 0){
	            $results = User::with('agentreviews')->whereIn('role',['AGENT'])->where('country',$defaultCountry)->where('status',1)->whereIn('top_list',['AGENT'])->orderBy('_id','DESC')->take(10)->get();
            }
        }
        foreach ($results as  $agent) {
            if($agent->agentreviews->count() > 0){
                $totalrating= $agent->agentreviews->count() * 5;
                $agent->rating = number_format($agent->agentreviews->sum('average') / $totalrating *5);
            }else{
                $agent->rating = 0;
            }
        }
        $blogs = Blog::where('status',1)->orderBy('_id','DESC')->get();
        $agents = $results->sortByDesc('rating');
        return view('frontend.home.agents', compact('blogs','defaultCountry','allProperties','rentProperties','saleProperties','buddies','agents','video'));
    }

    public function getAgentsLists(Request $request){
    	$latlongData=[];
        //$defaultCountry = request()->segment(3);

        $loc=request()->segment(3);
        if($loc == 'ksa' || $loc == 'uae' )
        {
           $loc21 = strtoupper($loc);
        }else 
        {
            $loc21 = ucfirst($loc);
        }
        
        $defaultCountry = $loc21;

        $users = User::whereIn('role',['AGENT'])->where('country', $defaultCountry);
        if($request->filled('location')){
            $location = $request->input('location');
            /*$users->whereHas('addresses', function($q) use($location)
            {
                $q->where('current_location', 'like', '%' . $location . '%')
                  ->orWhere('current_city', 'like', '%' . $location . '%')
                  ->orWhere('current_state', 'like', '%' . $location . '%')
                  ->orWhere('current_zipcode', 'like', '%' . $location . '%')
                  ->orWhere('current_country', 'like', '%' . $location . '%');
            });*/

            if($request->filled('latitude') && $request->filled('longitude'))
            {
                $latitude = (float)$request->latitude;
                $longitude = (float)$request->longitude;
                
                $users->where('location', 'near', [
                    '$geometry' => [
                        'type' => 'Point',
                        'coordinates' => [
                            $longitude, // longitude
                            $latitude, // latitude
                        ],
                    ],
                    '$maxDistance' => 10000,
                ]); 
            }
          
        }
        if(Auth::user())
        {
            $users = $users->where('_id','!=',Auth::user()->_id);
        }
        $results=$users->where('status',1)->with('addresses')->orderBy('created_at', 'desc')->get();
        $data = $users->paginate(12);
        // dd($data);
        $map_data = $users->paginate(12);

         
	    foreach($results as $result){
            if($result->latitude && $result->longitude){
    	     $latlongData[] = [$result->name, $result->latitude, $result->longitude];
            }
	    }
        if($request->ajax()){
            return view('frontend.users-listing.agents.list', compact('data','map_data','latlongData'));
        }
        return view('frontend.users-listing.agents.index', compact('data','map_data','latlongData'));
    }

}
