<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use Illuminate\Validation\Rule;
use Intervention\Image\ImageManagerStatic as Image;

class BlogController extends Controller
{
    public function index(){
        return view('frontend.profile.blog.index');
    }

    public function listing(){
        $getBlogs=Blog::where('status',0)->where('user_id',\Auth::user()->id)->get();
        $getBlogsapprove=Blog::where('status',1)->where('user_id',\Auth::user()->id)->get();
        return view('frontend.profile.blog.list_index',compact('getBlogs','getBlogsapprove'));
    }

    public function blog_listing(){
        $getBlogs=Blog::where('status',1)->orderBy('_id','DESC')->get();
        return view('frontend.blog.index',compact('getBlogs'));
    }

    public function save_blog(Request $request){
        
            $rules = [
                'blog_pic'=>'image|mimes:webp,jpeg,jpg,JPG,JPEG,png',
                'title' => 'required'
            ];

            $customMessages = [
                'blog_pic.max' => 'The :attribute field is required and less than 10240 KB.'
            ];
            
            $this->validate($request, $rules, $customMessages);
            $title=$request->title;
            $desc=$request->desription;
            $logo = $request->file('blog_pic');

            if(!@$logo){
                session(['title'=>$title]);
                session(['desc'=>$desc]);
                session(['pic_not'=>1]);
                return back()->with(['error'=>'Pic is required']);
            }else{
                $exte = $logo->getClientOriginalExtension();
                if($exte != "png" && $exte != "jpg" && $exte != "jpeg" && $exte != "webp"){
                    session(['pic_not'=>1]);
                    return back()->with(['error'=>'Pic is required']);
                }else{
                    \Session::forget('pic_not');
                    \Session::forget('title');
                    \Session::forget('desc');
                }               
            }
            $loc=$request->segment(3);
            $pic_name = null;
            $logo = $request->file('blog_pic');
            $pic_name = md5($logo->getClientOriginalName().time()).".".$logo->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/blog');
            $logo->move($destinationPath, $pic_name);
            $blog=new Blog;
            $blog->title=$request->title;
            $blog->pic=$pic_name;
            $blog->status=(int)0;
            $blog->description=$request->desription;
            $blog->created_at=date('Y-m-d h:i:s');
            $blog->updated_at=date('Y-m-d h:i:s');
            $blog->user_id=\Auth::user()->id;
            $blog->save();
            return redirect(app()->getLocale().'/listing'.'/'.$loc);
    }

    public function delete_blog(Request $request){
        $delete = Blog::where('_id',$request->id)->delete();
        return response()->json(['status'=>1]);
    }
    public function blog_details(Request $request){
        $blogs = Blog::where(['status'=>1,'_id'=>$request->segment(4)])->first();
         
         if(empty($blogs))
         {
              return redirect(app()->getLocale());
         }
        return view('frontend.blog.detail',compact('blogs'));
    }

    public function edit_blog(Request $request){
        $blogs = Blog::where('_id',$request->segment(4))->first();
        return view('frontend.profile.blog.edit_blog',compact('blogs'));
    }

    public function update_blog(Request $request){
        $loc=$request->segment(3);
        $pic_name = null;
        $logo = @$request->file('blog_pic');
        if(@$logo){
        $pic_name = md5(@$logo->getClientOriginalName().time()).".".@$logo->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/blog');
        $logo->move($destinationPath, @$pic_name);
        }
        $blogs = Blog::where('_id',$request->blog_id)->first();
        $blogs->title=$request->title;
        if(@$logo){
        $blogs->pic=$pic_name;
        }
        $blogs->description=$request->desription;
        $blogs->updated_at=date('Y-m-d h:i:s');
        $blogs->save();
        return redirect(app()->getLocale().'/listing'.'/'.$loc);
    }
}