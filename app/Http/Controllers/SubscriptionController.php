<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\Property;
use App\Models\SwapProperty;
use App\Models\CoworkspaceProperty;
use Illuminate\Validation\Rule;
use Intervention\Image\ImageManagerStatic as Image;

class SubscriptionController extends Controller
{
    public function index(){
        $myTransactions=Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','!=','user')->where('renew','!=',(int)1)->latest()->get();
        $cospace = Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','!=','user')->where('renew','!=',(int)1)->where('purchase_type','cospace')->latest()->get();
        $swap = Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','!=','user')->where('renew','!=',(int)1)->where('purchase_type','swap')->latest()->get();
        $rent = Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','!=','user')->where('renew','!=',(int)1)->where('purchase_type','rent')->latest()->get();
        $buddyAgent = Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','!=','user')->where('renew','!=',(int)1)
        ->where( function($q){
            $q->where('purchase_type','buddy')
            ->orWhere('purchase_type','agent');
        })->latest()->get();
        return view('frontend.profile.subscription.index',compact('myTransactions','cospace','swap','rent','buddyAgent'));
    }

    public function mysubscription_property(Request $request)
    {
        $plan_id = request()->segment(4);
        $plan_type = request()->segment(5);
        if($plan_type=='rent'){
            $ty='rent';
            $myProperties=Property::where('plan_id',$plan_id)->where('user_id',\Auth::user()->id)->get();
        }elseif($plan_type=='swap'){
            $ty='swap';
            $myProperties=SwapProperty::where('plan_id',$plan_id)->where('user_id',\Auth::user()->id)->get();
        }elseif($plan_type=='cospace'){
            $ty='cospace';
            $myProperties=CoworkspaceProperty::where('plan_id',$plan_id)->where('user_id',\Auth::user()->id)->get();
        }
        return view('frontend.profile.subscription.my',compact('myProperties','ty'));
    }

    public function show_hide(){
        $property_id = request()->segment(4);
        $plan_type = request()->segment(5);
        if($plan_type=='rent'){
            $check = Property::where('_id',$property_id)->first();
            if($check->show==1){
                $update=\DB::table('properties')->where('_id',$property_id)->update(['show'=>0]);
            }else{
                $update=\DB::table('properties')->where('_id',$property_id)->update(['show' => 1]);
            }
        }elseif($plan_type=='swap'){
            $check=SwapProperty::where('_id',$property_id)->first();
            if($check->show==1){
                $update=\DB::table('swap_properties')
                ->where('_id',$property_id)
                ->update(['show'=>0]);
            }else{
                $update=\DB::table('swap_properties')
                ->where('_id',$property_id)
                ->update(['show'=>1]);
            }
        }elseif($plan_type=='cospace'){
            $check=CoworkspaceProperty::where('_id',$property_id)->first();
            if($check->show==1){
                $update=\DB::table('coworkspace_properties')
                ->where('_id',$property_id)
                ->update(['show'=>0]);
            }else{
                $update=\DB::table('coworkspace_properties')
                ->where('_id',$property_id)
                ->update(['show'=>1]);
            }
        }
        return redirect()->back();
    }
}