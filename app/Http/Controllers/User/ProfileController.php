<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\Common;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\{
    User, Country, UserProfile, UserAddress, UserProfessionalDetail, UserDocument, UserQualification,Property,UserEnquiry, Transaction, Subscription , CoworkspaceProperty, SwapProperty
};
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use App\Rules\IsUserCurrentPassword;


use App\Http\Requests\{
    BuddyRequestValidation, AgentRequestValidation
};
use AmrShawky\LaravelCurrency\Facade\Currency;

class ProfileController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){

    }
    public function skip(){
        // if(auth()->user()->logged_first_time){
            return view('frontend.profile.skip');
        // }
        // return redirect('/');
    }
    /**
    * return profile page to save some informations which can be for agent/user/buddy
    */
    public function getProfile(Request $request){

        $defaultLoginRole = isset(Auth::user()->defaultLoginRole) ? Auth::user()->defaultLoginRole : 'USER';
        // convert id to mongo based Object iD
        $authId = new \MongoDB\BSON\ObjectID(Auth::user()->_id);

        if($defaultLoginRole!='USER'){
            $user = User::where('_id', $authId)->with(['profiles','addresses','professionals','professionals'=>function($q)use($defaultLoginRole){
                $q->where('role', $defaultLoginRole);
            },'documents'=>function($q)use($defaultLoginRole){
                $q->where('role', $defaultLoginRole);
            },'qualifications'])->firstOrFail();
        }else{
            $user = User::where('_id', $authId)->firstOrFail();
        }

        $countries = Country::all();
        $locales = config('app.supported_locales');
        $localities = config('app.localities');
        $codes = getAllCodes();
        $currencies = config('app.currencies');

        return view('frontend.profile.details.index',compact('user','defaultLoginRole','countries','locales','localities','codes','currencies'));
    }

    /**
    * return password change screen
    */
    public function showChangePassword(Request $request){
        $user = Auth::user();
        $defaultLoginRole = isset(Auth::user()->defaultLoginRole) ? Auth::user()->defaultLoginRole : 'USER';

        return view('frontend.profile.change-password.index',compact('defaultLoginRole','user'));
    }

    /**
    * validate and update password
    */
    public function updateChangePassword(Request $request){
        $request->validate([
            'current_password'=>['bail','required','string','min:8', new IsUserCurrentPassword],
            'password'=>'bail|required|string|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
            'password_confirmation'=>'bail|required|string|min:8|same:password|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
        ],
        [
            'password.regex'=>'The password must be more than 8 characters long, should contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character.',
            'password_confirmation.regex'=>'The confirm password must be more than 8 characters long, should contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character.',
        ]);

        $authId = new \MongoDB\BSON\ObjectID(Auth::user()->_id);// convert id to mongo based Object iD
        $user = User::where('_id', $authId)->first();
        if(!$user){
            return response()->json(['status'=>false,'message'=>'User not found. Please try again later.'], 200);
        }else{
            $user->password = Hash::make($request->password);
            $user->save();
            Auth::logout();
            return response()->json(['status'=>true, 'message'=>'Password changed successfully. Please login with new credentials.'], 200);
        }
    }

    /**
    * update profile about me details
    */
    public function saveAboutUser(Request $request){
        $authId = new \MongoDB\BSON\ObjectID(Auth::user()->_id);// convert id to mongo based Object iD

        $request->validate([
            'about'=>'bail|required|string|max:255',
        ],
        [
            'about.required'=>'The about me field is required'
        ]);

        $user = User::where('_id', $authId)->first();
        if(!$user){
            return response()->json(['status'=>false,'message'=>'User not found. Please try again later'],200);
        }else{
            $user->about = $request->about;
            $user->save();
            return response()->json(['status'=>true,'message'=>'About User updated successfully.'],200);
        }
    }

    /**
    * update profile details
    */
    public function postProfile(Request $request){
       
        $authId = new \MongoDB\BSON\ObjectID(Auth::user()->_id);// convert id to mongo based Object iD

        $request->validate([
            'name'=>'bail|required|string|max:255',
            //'email' => ['bail','required','string','email','max:255',Rule::unique('users')->ignore($authId, '_id')],
            'country' => 'bail|required|string|max:25',
            'country_code' => 'bail|required|string|max:15',
            'phone_number' => 'bail|required|numeric|regex:/[1-9]{1}[0-9]{5,14}/|digits_between:6,15',
            'address' => 'bail|required|string|max:500',
            'gender' => 'bail|required|in:male,female,other',
            'father_name'=>'bail|required|string|max:50',
            'date_of_birth' => 'bail|required|date_format:d/m/Y|before:today',
        ]);

        $user = User::where('_id', $authId)->first();
        if(!$user){
            return response()->json(['status'=>false,'message'=>'User not found. Please try again later'],200);
        }else{
            $user->name = $request->name;
            $user->country = $request->country;
            $user->countryCode = $request->country_code;
            $user->phone = $request->phone_number;
            $user->gender = $request->gender;
            $user->address = $request->address;
            $user->father_name = $request->father_name;
            $user->date_of_birth = $request->date_of_birth;

            $user->save();

            return response()->json(['status'=>true,'message'=>'Personal details updated successfully.'],200);
        }
    }

    /*
    * Update address of user(agent/buddy)
    **/
    public function updateAddress(Request $request){
        $authId = new \MongoDB\BSON\ObjectID(Auth::user()->_id);

        $request->validate([
            'current_location'=>'bail|required|string|max:250',
            'current_unit_number'=>'bail|required|string|max:250',
            // 'current_address_1'=>'bail|required|string|max:250',
            // 'current_address_2'=>'bail|required|string|max:250',
            'current_landmark'=>'bail|string|max:250',
            'current_city'=>'bail|required|string|max:250',
            'current_country'=>'bail|required|string|max:250',
            'current_state'=>'bail|required|string|max:250',
            'current_zipcode'=>'bail|required|string|max:250',
            // native
            'native_location'=>'bail|required|string|max:250',
            'native_unit_number'=>'bail|required|string|max:250',
            // 'native_address_1'=>'bail|required|string|max:250',
            // 'native_address_2'=>'bail|required|string|max:250',
            'native_landmark'=>'bail|string|max:250',
            'native_city'=>'bail|required|string|max:250',
            'native_country'=>'bail|required|string|max:250',
            'native_state'=>'bail|required|string|max:250',
            'native_zipcode'=>'bail|required|string|max:250',
        ]);
        $user1 = User::where('_id', Auth::user()->_id)->first();
        if(@$request->latitude!=''){
        $user1->latitude = $request->latitude;
        $user1->longitude = $request->longitude;
        $user1->location = [(float) $request->longitude,(float)$request->latitude]; ;
        $user1->save();
        }
        $user = UserAddress::where('user_id', Auth::user()->_id)->first();
        if(!$user){
            return response()->json(['status'=>false,'message'=>'User not found. Please try again later'],200);
        }
        else{

            
            $user->current_location = $request->input('current_location');
            $user->current_unit_number = $request->input('current_unit_number');
            $user->current_address_1 = $request->input('current_address_1');
            $user->current_address_2 = $request->input('current_address_2');
            $user->current_landmark = $request->input('current_landmark')??'';
            $user->current_city = $request->input('current_city');
            $user->current_country = $request->input('current_country');
            $user->current_state = $request->input('current_state');
            $user->current_zipcode = $request->input('current_zipcode');
            $user->same_current_address = $request->input('same_current_address');
            $user->native_location = $request->input('native_location');
            $user->native_unit_number = $request->input('native_unit_number');
            $user->native_address_1 = $request->input('native_address_1');
            $user->native_address_2 = $request->input('native_address_2');
            $user->native_landmark = $request->input('native_landmark');
            $user->native_city = $request->input('native_city');
            $user->native_country = $request->input('native_country');
            $user->native_state = $request->input('native_state');
            $user->native_zipcode = $request->input('native_zipcode');
            $user->save();

            return response()->json(['status'=>true,'message'=>'Address updated successfully.'],200);
        }
    }

    /*
    * Update Qualification of user(agent/buddy)
    **/
    public function updateQualification(Request $request){
        $authId = new \MongoDB\BSON\ObjectID(Auth::user()->_id);// convert id to mongo based Object iD
        $request->validate([
            'course_name.*'=>'bail|required|string|max:100',
            'university_name.*'=>'bail|required|string|max:100',
            'hobby.*'=>'bail|required|string|max:100',
        ],
        [
            'course_name.*.required'=>'The Course name is required.',
            'university_name.*.required'=>'The University name is required.',
            'hobby.*.required'=>'The hobby name is required.',
        ]);

        $user = UserQualification::where('user_id', Auth::user()->_id)->first();
        if(!$user){
            return response()->json(['status'=>false,'message'=>'User not found. Please try again later'],200);
        }
        else
        {
            $course_name =[];
            $hobbies =[];
            $data =[];
            if($request->has('course_name') && $request->has('university_name')){
                foreach($request->get('course_name') as $value){
                    $course_name[] = $value;
                }

                foreach($request->get('university_name') as $key => $value){
                    $data[] = ['course_name'=>$course_name[$key],'university_name'=>$value];
                }
            }

            if($request->has('hobby')){
               foreach($request->get('hobby') as $value){
                    $hobbies[] = $value;
                }
            }
            $user->education =$data;
            $user->hobbies = $hobbies;
            $user->save();

            return response()->json(['status'=>true,'message'=>'Qualifications updated successfully.'],200);
        }
    }

    /*
    * Update updateAdditionalData of user(agent/buddy)
    **/
    public function updateAdditionalData(Request $request){
        $authId = new \MongoDB\BSON\ObjectID(Auth::user()->_id);// convert id to mongo based Object iD
        $request->validate([
            'marriage_status' => 'bail|required|string|max:50',
            'preffered_language' => 'bail|required|string|max:250',
            'preffered_currency' => 'bail|required|string|max:50',
            'working_since' => 'bail|required|digits:4|numeric|min:1900|max:'.date("Y"),
            'residence_id_number' => 'bail|required|string|max:100',
            'passport_number' => 'bail|required|string|max:50',
            'driving_licence' => 'bail|required|in:yes,no',
            'licence_number' => 'bail|required_if:driving_licence,yes|max:50',
            'whatsapp_number' => 'bail|required|numeric|digits_between:6,15',
        ],
        [
            'licence_number.required_if'=>'The licence number is required.',
        ]);

        $user = UserProfile::where('user_id', Auth::user()->_id)->first();
        if(!$user){
            return response()->json(['status'=>false,'message'=>'User not found. Please try again later'],200);
        }
        else
        {
            $user->marriage_status = $request->marriage_status;
            $user->preffered_language = $request->preffered_language;
            $user->preffered_currency = $request->preffered_currency;
            $user->working_since = $request->working_since;
            $user->residence_id_number = $request->residence_id_number;
            $user->passport_number = $request->passport_number;
            $user->driving_licence = $request->driving_licence;
            $user->licence_number = $request->licence_number;
            $user->whatsapp_number_same = $request->whatsapp_number_same;
            $user->whatsapp_number = $request->whatsapp_number;
            $user->save();

            return response()->json(['status'=>true,'message'=>'Additional informations updated successfully.'],200);
        }
    }

    /*
    * Update professional of user(agent/buddy)
    **/
    public function updateProfessionalData(Request $request){
        $authId = new \MongoDB\BSON\ObjectID(Auth::user()->_id);// convert id to mongo based Object iD
        $defaultLoginRole = $request->defaultLoginRole;
        $defaultLoginRole = strtoupper($defaultLoginRole);
        
        $user = UserProfessionalDetail::where('user_id', Auth::user()->_id)->where('role', $defaultLoginRole)->first();
        if(!$user){
            return response()->json(['status'=>false,'message'=>'User not found. Please try again later'],200);
        }

        $errors = [
                'languages'=>'bail|required',
                'localities'=>'bail|required',
                'real_estate_agent'=>'bail|required|in:yes,no',
                'deal_property_in_uae'=>'bail|required_if:real_estate_agent,yes|in:inside,outside',
                // 'permit_number'=>'bail|required_if:real_estate_agent,yes|max:50',
                // time
                'monday_start_time'=>'bail|nullable|required_if:monday,1|date_format:H:i',
                'monday_end_time'=>'bail|nullable|required_if:monday,1|date_format:H:i|after:monday_start_time',
                // tuesday
                'tuesday_start_time'=>'bail|nullable|required_if:tuesday,1|date_format:H:i',
                'tuesday_end_time'=>'bail|nullable|required_if:tuesday,1|date_format:H:i|after:tuesday_start_time',
                // wednesday
                'wednesday_start_time'=>'bail|nullable|required_if:wednesday,1|date_format:H:i',
                'wednesday_end_time'=>'bail|nullable|required_if:wednesday,1|date_format:H:i|after:wednesday_start_time',
                // thursday
                'thursday_start_time'=>'bail|nullable|required_if:thursday,1|date_format:H:i',
                'thursday_end_time'=>'bail|nullable|required_if:thursday,1|date_format:H:i|after:thursday_start_time',
                // friday
                'friday_start_time'=>'bail|nullable|required_if:friday,1|date_format:H:i',
                'friday_end_time'=>'bail|nullable|required_if:friday,1|date_format:H:i|after:friday_start_time',
                // saturday
                'saturday_start_time'=>'bail|nullable|required_if:saturday,1|date_format:H:i',
                'saturday_end_time'=>'bail|nullable|required_if:saturday,1|date_format:H:i|after:saturday_start_time',
                // sunday
                'sunday_start_time'=>'bail|nullable|required_if:sunday,1|date_format:H:i',
                'sunday_end_time'=>'bail|nullable|required_if:sunday,1|date_format:H:i|after:sunday_start_time',
            ];
        if($request->defaultLoginRole=='AGENT')
        {
            $errors['rera_number'] = ['bail','required_if:real_estate_agent,yes','max:50'];
        }else{
            $errors['permit_number'] = ['bail','required_if:real_estate_agent,yes','max:50'];
        }

        $messages = [
            'monday_start_time.required_if'=>'The start time is required.',
            'monday_end_time.required_if'=>'The end time is required.',
            'monday_end_time.after'=>'The end time must be greater than start time.',

            'tuesday_start_time.required_if'=>'The start time is required.',
            'tuesday_end_time.required_if'=>'The end time is required.',
            'tuesday_end_time.after'=>'The end time must be greater than start time.',

            'wednesday_start_time.required_if'=>'The start time is required.',
            'wednesday_end_time.required_if'=>'The end time is required.',
            'wednesday_end_time.after'=>'The end time must be greater than start time.',

            'thursday_start_time.required_if'=>'The start time is required.',
            'thursday_end_time.required_if'=>'The end time is required.',
            'thursday_end_time.after'=>'The end time must be greater than start time.',

            'friday_start_time.required_if'=>'The start time is required.',
            'friday_end_time.required_if'=>'The end time is required.',
            'friday_end_time.after'=>'The end time must be greater than start time.',

            'saturday_start_time.required_if'=>'The start time is required.',
            'saturday_end_time.required_if'=>'The end time is required.',
            'saturday_end_time.after'=>'The end time must be greater than start time.',

            'sunday_start_time.required_if'=>'The start time is required.',
            'sunday_end_time.required_if'=>'The end time is required.',
            'sunday_end_time.after'=>'The end time must be greater than start time.',

            'monday_start_time.date_format'=>'The start time format is not correct.',
            'monday_end_time.date_format'=>'The end time format is not correct.',

            'tuesday_start_time.date_format'=>'The start time format is not correct.',
            'tuesday_end_time.date_format'=>'The start time format is not correct.',

            'wednesday_start_time.date_format'=>'The start time format is not correct.',
            'wednesday_end_time.date_format'=>'The start time format is not correct.',

            'thursday_start_time.date_format'=>'The start time format is not correct.',
            'thursday_end_time.date_format'=>'The start time format is not correct.',

            'friday_start_time.date_format'=>'The start time format is not correct.',
            'friday_end_time.date_format'=>'The start time format is not correct.',

            'saturday_start_time.date_format'=>'The start time format is not correct.',
            'saturday_end_time.date_format'=>'The start time format is not correct.',

            'sunday_start_time.date_format'=>'The start time format is not correct.',
            'sunday_end_time.date_format'=>'The start time format is not correct.',
        ];
        $request->validate($errors, $messages);
        $user->languages = $request->input('languages');
        $user->localities = $request->input('localities');
        $user->real_estate_agent = $request->input('real_estate_agent');
        $user->deal_property_in_uae = ($request->input('real_estate_agent')=='yes') ? $request->input('deal_property_in_uae') : '';
        $user->rera_number = ($request->input('real_estate_agent')=='yes') ? $request->input('rera_number') : '';
        $user->permit_number = ($request->input('real_estate_agent')=='yes') ? $request->input('permit_number') : '';

        $user->availability = [
            'sunday'=>($request->input('sunday')==1) ? 1: 0,
            'monday'=>($request->input('monday')==1) ? 1: 0,
            'tuesday'=>($request->input('tuesday')==1) ? 1: 0,
            'wednesday'=>($request->input('wednesday')==1) ? 1: 0,
            'thursday'=>($request->input('thursday')==1) ? 1: 0,
            'friday'=>($request->input('friday')==1) ? 1: 0,
            'saturday'=>($request->input('saturday')==1) ? 1: 0,
        ];

        $user->timing =[
            'sunday_start_time'=>($request->input('sunday')==1) ? $request->input('sunday_start_time'): 0,
            'sunday_end_time'=>($request->input('sunday')==1) ? $request->input('sunday_end_time'): 0,

            'monday_start_time'=>($request->input('monday')==1) ? $request->input('monday_start_time'): 0,
            'monday_end_time'=>($request->input('monday')==1) ? $request->input('monday_end_time'): 0,

            'tuesday_start_time'=>($request->input('tuesday')==1) ? $request->input('tuesday_start_time'): 0,
            'tuesday_end_time'=>($request->input('tuesday')==1) ? $request->input('tuesday_end_time'): 0,

            'wednesday_start_time'=>($request->input('wednesday')==1) ? $request->input('wednesday_start_time'): 0,
            'wednesday_end_time'=>($request->input('wednesday')==1) ? $request->input('wednesday_end_time'): 0,

            'thursday_start_time'=>($request->input('thursday')==1) ? $request->input('thursday_start_time'): 0,
            'thursday_end_time'=>($request->input('thursday')==1) ? $request->input('thursday_end_time'): 0,

            'friday_start_time'=>($request->input('friday')==1) ? $request->input('friday_start_time'): 0,
            'friday_end_time'=>($request->input('friday')==1) ? $request->input('friday_end_time'): 0,

            'saturday_start_time'=>($request->input('saturday')==1) ? $request->input('saturday_start_time'): 0,
            'saturday_end_time'=>($request->input('saturday')==1) ? $request->input('saturday_end_time'): 0,
        ];
        $user->other_countries = isset($request->other_countries) ? $request->input('other_countries') : [];
        $user->save();

        return response()->json(['status'=>true,'message'=>'Additional informations updated successfully.'],200);
    }

    // update documents of agent/Buddy
    public function updateDocuments(Request $request){
        $request->validate(
        [
            'documents'=>'required',
            'documents.*' => 'mimes:doc,pdf,docx,zip,webp,jpeg,jpg,JPG,JPEG|max:2048'
        ]);
        $authId = new \MongoDB\BSON\ObjectID(Auth::user()->_id);// convert id to mongo based Object iD
        $defaultLoginRole = isset(Auth::user()->defaultLoginRole) ? Auth::user()->defaultLoginRole : '';

        $user = UserDocument::where('user_id', Auth::user()->_id)->where('role',$defaultLoginRole)->first();
        if(!$user){
            return response()->json(['status'=>false,'message'=>'User not found. Please try again later'],200);
        }

        $data =[];
        foreach($request->file('documents') as $file)
        {
            $name = md5($file->getClientOriginalName().time()).".".$file->getClientOriginalExtension();
            $file->move(public_path('/uploads/requests/docs/'.$authId), $name);
            $data[] = $name;
        }

        $already_data = $user->documents;
        if(is_array($already_data)){
            $user->documents = array_merge($already_data, $data);
        }else{
            $user->documents = $data;
        }

        $user->save();
        return response()->json(['status'=>true,'message'=>'Documents uploaded successfully.'],200);

    }

    /**
    * upload profile image
    */
    public function updateProfileImage(Request $request){
        $request->validate([
            'profile_pic' =>'bail|required|mimes:webp,jpeg,jpg,JPG,JPEG,png|max:2048',
        ]);
        $id = Auth::user()->_id;
        $authId = new \MongoDB\BSON\ObjectID($id);

        $user = User::where('_id', $authId)->first();
        if(!$user){
            return response()->json(['status'=>false,'message'=>'User not found. Please try again later'],200);
        }else{
            $name = null;
            $logo = $request->file('profile_pic');
            $name = md5($logo->getClientOriginalName().time()).".".$logo->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/profilePics');
            $logo->move($destinationPath, $name);

            if(file_exists(public_path('uploads/profilePics/'.$user->image)) && is_file(public_path('uploads/profilePics/'.$user->image))){
                unlink(public_path('uploads/profilePics/'.$user->image));
            }

            $user->image = $name;
            $user->save();
            return response()->json(['status'=>true,'message'=>'Profile picture updated successfully.'],200);
        }
    }


    private function redirectToSteps($steps, $userprofile,$loc){

        switch ($steps) {
            case '0':
                return redirect(app()->getLocale().'/'.$userprofile.'/step/1/'.$loc);
                break;

            case '1':
                return redirect(app()->getLocale().'/'.$userprofile.'/step/2/'.$loc);
                break;

            case '2':
                return redirect(app()->getLocale().'/'.$userprofile.'/step/3/'.$loc);
                break;

            case '3':
                return redirect(app()->getLocale().'/'.$userprofile.'/step/4/'.$loc);
                break;

            case '4':
                return redirect(app()->getLocale().'/'.$userprofile.'/step/5/'.$loc);
                break;

            default:
                return redirect(app()->getLocale().'/profile/'.$loc);
                break;
        }
    }

    // buddyRequest
    public function buddyRequest(Request $request){
        $id = Auth::user()->_id;
        $loc = $request->segment(3);
        $authId = new \MongoDB\BSON\ObjectID($id);

        $user = User::where('_id', $authId)->first();
        if(!$user){
             return redirect(app()->getLocale().'/profile/'.$loc);
        }else
        {
            $buddyrequest = $user->buddyProfile ? $user->buddyProfile: 0;
            if($buddyrequest==0){
                $buddySteps = $user->buddyProfileSteps ? $user->buddyProfileSteps: 0;
                return self::redirectToSteps($buddySteps,'buddy-profile', $loc);
            }else{
                return redirect(app()->getLocale().'/profile/'.$loc);
            }
        }
    }

    public function agentRequest(Request $request){
        $id = Auth::user()->_id;
        $loc = $request->segment(3);
        
        $authId = new \MongoDB\BSON\ObjectID($id);
        $user = User::where('_id', $authId)->first();
        if(!$user){
            
            return redirect(app()->getLocale().'/profile/'.$loc);
        }else
        {
            $agentRequest = $user->agentProfile ? $user->agentProfile: 0;
            if($agentRequest==0){
                $steps = $user->agentProfileSteps ? $user->agentProfileSteps: 0;

                return self::redirectToSteps($steps,'agent-profile',$loc );
            }else{
                
                return redirect(app()->getLocale().'/profile/'.$loc);
            }
        }
    }

    // show buddy profile request form based on steps
    public function buddyProfileRequestSteps(Request $request){
        $step = $request->segment(4);
        $loc = $request->segment(5);
        $id = Auth::user()->_id;
        $authId = new \MongoDB\BSON\ObjectID($id);

        $user = User::where('_id', $authId)->first();
        if(!$user){
            return redirect(app()->getLocale().'/profile/'.$loc);
        }else
        {
            $countries = Country::all();
            $locales = config('app.supported_locales');
            $localities = config('app.localities');
            $currencies = config('app.currencies');
            $codes = getAllCodes();

            $buddySteps = $user->buddyProfileSteps ? $user->buddyProfileSteps: 0;

            if(!in_array($step, [1,2,3,4,5])){
                return redirect(app()->getLocale().'/profile/'.$loc);
            }

            switch ($buddySteps) {
                case '0':
                    if($step!=1){
                        return self::redirectToSteps($buddySteps, 'buddy-profile', $loc);
                    }

                    $userData = UserProfile::where('user_id', Auth::user()->_id)->first();
                    return view('frontend.profile.requests.buddy.index',compact('step','countries','locales','user','userData','codes','currencies'));
                    break;

                case '1':
                    if($step!=2){
                        return self::redirectToSteps($buddySteps, 'buddy-profile', $loc);
                    }

                    $userAddress = UserAddress::where('user_id', Auth::user()->_id)->first();

                    return view('frontend.profile.requests.buddy.address', compact('step','countries','locales','localities','userAddress'));
                    break;
                case '2':
                    if($step!=3){
                        return self::redirectToSteps($buddySteps, 'buddy-profile', $loc);
                    }
                    return view('frontend.profile.requests.buddy.professional_details', compact('step','countries','locales','localities'));
                    break;

                case '3':
                    if($step!=4){
                        return self::redirectToSteps($buddySteps, 'buddy-profile', $loc);
                    }
                    return view('frontend.profile.requests.buddy.documents',compact('step','countries','locales'));
                    break;

                case '4':
                    if($step!=5){
                        return self::redirectToSteps($buddySteps, 'buddy-profile', $loc);
                    }
                    $qualifications = UserQualification::where('user_id', Auth::user()->_id)->first();
                    return view('frontend.profile.requests.buddy.qualifications',compact('step','countries','locales','qualifications'));
                    break;

                default:
                    return redirect(app()->getLocale().'/profile/'.$loc);
                    break;
            }
        }
    }

    // show agent profile request form based on steps
    public function agentProfileRequestSteps(Request $request){
        
        $step = $request->segment(4);
        $loc = $request->segment(5);
        $id = Auth::user()->_id;
        $authId = new \MongoDB\BSON\ObjectID($id);

        $user = User::where('_id', $authId)->first();
        if(!$user){
            return redirect(app()->getLocale().'/profile');
        }else
        {
            $countries = Country::all();
            $locales = config('app.supported_locales');
            $localities = config('app.localities');
            $currencies = config('app.currencies');
            $codes = getAllCodes();

            $agentSteps = $user->agentProfileSteps ? $user->agentProfileSteps: 0;

            if(!in_array($step, [1,2,3,4,5])){
                return redirect(app()->getLocale().'/profile');
            }

            switch ($agentSteps) {
                case '0':
                    if($step!=1){
                        return self::redirectToSteps($agentSteps, 'agent-profile',$loc );
                    }
                    $userData = UserProfile::where('user_id', Auth::user()->_id)->first();

                    return view('frontend.profile.requests.agent.index',compact('step','countries','locales','user','userData','codes','currencies'));
                    break;
                case '1':
                    if($step!=2){
                        return self::redirectToSteps($agentSteps, 'agent-profile',$loc );
                    }
                    $userAddress = UserAddress::where('user_id', Auth::user()->_id)->first();

                    return view('frontend.profile.requests.agent.address', compact('step','countries','locales','localities','userAddress'));
                    break;
                case '2':
                    if($step!=3){
                        return self::redirectToSteps($agentSteps, 'agent-profile',$loc );
                    }
                    return view('frontend.profile.requests.agent.professional_details', compact('step','countries','locales','localities'));
                    break;

                case '3':
                    if($step!=4){
                        return self::redirectToSteps($agentSteps, 'agent-profile',$loc );
                    }
                    return view('frontend.profile.requests.agent.documents',compact('step','countries','locales'));
                    break;

                case '4':
                    if($step!=5){
                        return self::redirectToSteps($agentSteps, 'agent-profile',$loc );
                    }
                    $qualifications = UserQualification::where('user_id',Auth::user()->_id)->first();
                    return view('frontend.profile.requests.agent.qualifications',compact('step','countries','locales','qualifications'));
                    break;

                default:
                    return redirect(app()->getLocale().'/profile/'.$loc );
                    break;
            }
        }
    }

    public function saveBuddyProfileSteps(BuddyRequestValidation $request){
        $step = $request->segment(4);
        $loc = $request->segment(5);

        $id = Auth::user()->_id;
        $authId = new \MongoDB\BSON\ObjectID($id);
        $user = User::where('_id', $authId)->first();

        if(!$user)
        {
            return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
        }
        else
        {

            switch ($step) {
                case '1':
                    $pic_name = null;
                    $res = self::saveStep1($authId, $request, $pic_name, $user);
                    if($res){
                        $user->buddyProfileSteps = 1;
                        $user->save();
                        return response()->json(['status'=>true,'message'=>'Personal details saved successfully.','url'=>url(app()->getLocale().'/buddy-request/'.$loc)],200);
                    }else{
                        return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                    }
                    break;
                case '2':
                    $res = self::saveStep2($authId, $request);
                    if($res){
                        // dd($request->all());
                        $location = Common::getLatLong($request->current_location);
                        $location = [(float) $location['lng'],(float)$location['lat']];
                        $user->latitude = $request->latitude;
                        $user->longitude = $request->longitude;
                        $user->location = $location;
                        $user->buddyProfileSteps = 2;
                        $user->save();
                        return response()->json(['status'=>true,'message'=>'Address saved successfully.','url'=>url(app()->getLocale().'/buddy-request/'.$loc)],200);
                    }else{
                        return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                    }
                    break;
                case '3':
                    $res = self::saveStep3($authId, $request, 'BUDDY');
                    $request->validate([
                        'charges' => 'bail|required',
                    ]);
                    if($res){
                        $user->buddyProfileSteps = 3;
                        $user->charges = (int)$request->charges;
                        $user->charge_currency = $request->charge_currency;
                        $user->charge_type = $request->charge_type;
                        $user->area_of_spec = $request->area_of_spec ?? '';
                        $user->save();
                        return response()->json(['status'=>true,'message'=>'Professional details saved successfully.','url'=>url(app()->getLocale().'/buddy-request/'.$loc)],200);
                    }else{
                        return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                    }
                    break;

                case '4':
                    $data =[];
                    foreach($request->file('documents') as $file)
                    {
                        $name=md5($file->getClientOriginalName().time()).".".$file->getClientOriginalExtension();
                        $file->move(public_path('/uploads/requests/docs/'.$authId), $name);
                        $data[] = $name;
                    }
                    // update or create entry of buddy Professional details
                    $res = UserDocument::updateOrCreate(
                            [
                                'user_id'=>Auth::user()->_id,
                                'role'=>'BUDDY',
                            ],
                            [
                                'documents'=>$data
                            ]
                        );

                    if($res){
                        $user->buddyProfileSteps = 4;
                        $user->save();
                        return response()->json(['status'=>true,'message'=>'Documents uploaded successfully.','url'=>url(app()->getLocale().'/buddy-request/'.$loc)],200);
                    }else{
                        return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                    }
                    break;

                case '5':
                    $res= self::saveStep5($authId, $request);

                    if($res){
                        $user->buddyProfileSteps = 5;
                        $user->buddyProfile = 1; // means user requested buddy profile. now admin will approve
                        $user->save();
                        return response()->json(['status'=>true,'message'=>'Thank you for providing details. Upon validation of the details provided, you will be listed.','url'=>url(app()->getLocale().'/buddy-request/'.strtolower(\Auth::user()->country))],200);
                    }else{
                        return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                    }
                    break;

                default:
                    # code...
                    break;
            }
        }
    }

    public function saveAgentProfileSteps(AgentRequestValidation $request){
        $step = $request->segment(4);
         $loc = $request->segment(5);
        $id = Auth::user()->_id;
        $authId = new \MongoDB\BSON\ObjectID($id);
        $user = User::where('_id', $authId)->first();

        if(!$user)
        {
            return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
        }
        else
        {
            switch ($step) {
                case '1':
                    $pic_name = null;
                    /*
                    $logo = $request->file('profile_pic');
                    $pic_name = md5($logo->getClientOriginalName().time()).".".$logo->getClientOriginalExtension();
                    $destinationPath = public_path('/uploads/profilePics');
                    $logo->move($destinationPath, $pic_name);
*/
                    // update or create entry of buddy profile details
                    $res = self::saveStep1($authId, $request, $pic_name, $user);

                    if($res){

                        $user->agentProfileSteps = 1;
                        $user->save();
                        return response()->json(['status'=>true,'message'=>'Personal details saved successfully.','url'=>url(app()->getLocale().'/agent-request/'.$loc)],200);
                    }else{
                        return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                    }
                    break;

                case '2':

                    // update or create entry of buddy profile details
                   $res = self::saveStep2($authId, $request);

                    if($res){
                        $location = Common::getLatLong($request->current_location);
                        $location = [(float) $location['lng'],(float)$location['lat']];
                        $user->location = $location;
                        $user->agentProfileSteps = 2;

                        $user->save();

                        return response()->json(['status'=>true,'message'=>'Address saved successfully.','url'=>url(app()->getLocale().'/agent-request/'.$loc)],200);
                    }else{
                        return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                    }
                    break;

                case '3':
                    // update or create entry of buddy Professional details
                    $res = self::saveStep3($authId, $request, 'AGENT');

                    if($res){
                        $user->agentProfileSteps = 3;
                        $user->save();
                        return response()->json(['status'=>true,'message'=>'Professional details saved successfully.','url'=>url(app()->getLocale().'/agent-request/'.$loc)],200);
                    }else{
                        return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                    }
                    break;

                case '4':
                    $data =[];

                    foreach($request->file('documents') as $file)
                    {
                        $name=md5($file->getClientOriginalName().time()).".".$file->getClientOriginalExtension();
                        $file->move(public_path('/uploads/requests/docs/'.$authId), $name);
                        $data[] = $name;
                    }
                    // update or create entry of buddy Professional details
                    $res = UserDocument::updateOrCreate(
                            [
                                'user_id'=>Auth::user()->_id,
                                'role'=>'AGENT',
                            ],
                            [
                                'documents'=>$data
                            ]
                        );

                    if($res){
                        $user->agentProfileSteps = 4;
                        $user->save();
                        return response()->json(['status'=>true,'message'=>'Documents uploaded successfully.','url'=>url(app()->getLocale().'/agent-request/'.$loc)],200);
                    }else{
                        return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                    }
                    break;

                case '5':
                    $res= self::saveStep5($authId, $request);

                    if($res){
                        $user->agentProfileSteps = 5;
                        $user->agentProfile = 1; // means user requested buddy profile. now admin will approve
                        $user->save();
                        return response()->json(['status'=>true,'message'=>'Thank you for providing details. Upon validation of the details provided, you will be listed.','url'=>url(app()->getLocale().'/agent-request/'.strtolower(Auth::user()->country))],200);
                    }else{
                        return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                    }
                    break;

                default:
                    # code...
                    break;
            }
        }
    }

    // save application step 1 data. personal data
    private function saveStep1($authId, $request, $pic_name, $user){
        $res = UserProfile::updateOrCreate(
                [
                    'user_id'=>Auth::user()->_id,
                ],
                [
                    'marriage_status' => $request->input('marriage_status'),
                    'preffered_language' => $request->input('preffered_language'),
                    'preffered_currency' => $request->input('preffered_currency'),
                    'working_since' => $request->input('working_since'),
                    'residence_id_number' => $request->input('residence_id_number'),
                    'passport_number' => $request->input('passport_number'),
                    'driving_licence' => $request->input('driving_licence'),
                    //'profile_pic' => $pic_name,
                    'whatsapp_number_same' => $request->input('whatsapp_number_same'),
                    'whatsapp_number' => ($request->input('whatsapp_number_same')=='yes') ? $request->input('phone') : $request->input('whatsapp_number'),
                    'licence_number' => ($request->input('driving_licence')=='yes') ? $request->input('licence_number') : '',
                ]
            );
        $full_name = '';
        $full_name = $request->input('first_name');
        if($request->input('middle_name')){
            $full_name = $full_name.' '.$request->input('middle_name');
        }
        $full_name = $full_name.' '.$request->input('last_name');

        $user->first_name = $request->input('first_name');
        $user->middle_name = $request->input('middle_name');
        $user->last_name = $request->input('last_name');
        $user->gender = $request->input('gender');
        $user->father_name = $request->input('father_name');
        $user->name = $full_name;
        $user->phone = $request->input('phone');
        $user->countryCode = $request->input('country_code');
        $user->nationality = $request->input('nationality');
        $user->date_of_birth = $request->input('date_of_birth');

        return $res;
    }

    // save application step 2 data. address
    private function saveStep2($authId, $request){
        $res = UserAddress::updateOrCreate(
            [
                'user_id'=>Auth::user()->_id,
            ],
            [
                'current_location' => $request->input('current_location'),
                'current_unit_number' => $request->input('current_unit_number'),
                'current_address_1' => $request->input('current_address_1'),
                'current_address_2' => $request->input('current_address_2'),
                'current_landmark' => $request->input('current_landmark'),
                'current_city' => $request->input('current_city'),
                'current_country' => $request->input('current_country'),
                'current_state' => $request->input('current_state'),
                'current_zipcode' => $request->input('current_zipcode'),

                'same_current_address' => $request->input('same_current_address'),

                'native_location' => $request->input('native_location'),
                'native_unit_number' => $request->input('native_unit_number'),
                'native_address_1' => $request->input('native_address_1'),
                'native_address_2' => $request->input('native_address_2'),
                'native_landmark' => $request->input('native_landmark'),
                'native_city' => $request->input('native_city'),
                'native_country' => $request->input('native_country'),
                'native_state' => $request->input('native_state'),
                'native_zipcode' => $request->input('native_zipcode'),
            ]
        );

        return $res;
    }

    // save step3 data Professional details
    private function saveStep3($authId, $request, $role){
        // update or create entry of buddy Professional details
        return UserProfessionalDetail::updateOrCreate(
                [
                    'user_id'=>Auth::user()->_id,
                    'role'=>$role,
                ],
                [
                    'languages' => $request->input('languages'),
                    'localities' => $request->input('localities'),
                    'real_estate_agent' => $request->input('real_estate_agent'),
                    'deal_property_in_uae' => ($request->input('real_estate_agent')=='yes') ? $request->input('deal_property_in_uae') : '',
                    'rera_number' => ($request->input('real_estate_agent')=='yes') ? $request->input('rera_number') : '',
                    'permit_number' => ($request->input('real_estate_agent')=='yes') ? $request->input('permit_number') : '',
                    'availability'=>[
                        'sunday'=>($request->input('sunday')==1) ? 1: 0,
                        'monday'=>($request->input('monday')==1) ? 1: 0,
                        'tuesday'=>($request->input('tuesday')==1) ? 1: 0,
                        'wednesday'=>($request->input('wednesday')==1) ? 1: 0,
                        'thursday'=>($request->input('thursday')==1) ? 1: 0,
                        'friday'=>($request->input('friday')==1) ? 1: 0,
                        'saturday'=>($request->input('saturday')==1) ? 1: 0,
                    ],
                    'timing'=>[
                        'sunday_start_time'=>($request->input('sunday')==1) ? $request->input('sunday_start_time'): 0,
                        'sunday_end_time'=>($request->input('sunday')==1) ? $request->input('sunday_end_time'): 0,

                        'monday_start_time'=>($request->input('monday')==1) ? $request->input('monday_start_time'): 0,
                        'monday_end_time'=>($request->input('monday')==1) ? $request->input('monday_end_time'): 0,

                        'tuesday_start_time'=>($request->input('tuesday')==1) ? $request->input('tuesday_start_time'): 0,
                        'tuesday_end_time'=>($request->input('tuesday')==1) ? $request->input('tuesday_end_time'): 0,

                        'wednesday_start_time'=>($request->input('wednesday')==1) ? $request->input('wednesday_start_time'): 0,
                        'wednesday_end_time'=>($request->input('wednesday')==1) ? $request->input('wednesday_end_time'): 0,

                        'thursday_start_time'=>($request->input('thursday')==1) ? $request->input('thursday_start_time'): 0,
                        'thursday_end_time'=>($request->input('thursday')==1) ? $request->input('thursday_end_time'): 0,

                        'friday_start_time'=>($request->input('friday')==1) ? $request->input('friday_start_time'): 0,
                        'friday_end_time'=>($request->input('friday')==1) ? $request->input('friday_end_time'): 0,

                        'saturday_start_time'=>($request->input('saturday')==1) ? $request->input('saturday_start_time'): 0,
                        'saturday_end_time'=>($request->input('saturday')==1) ? $request->input('saturday_end_time'): 0,
                    ],
                ]
            );
    }

    // save step5 qualification data
    private function saveStep5($authId, $request){
        $course_name =[];
        $hobbies =[];
        $data =[];

        if($request->has('course_name') && $request->has('university_name')){
            foreach($request->get('course_name') as $value)
            {
                $course_name[] = $value;
            }

            foreach($request->get('university_name') as $key => $value)
            {
                $data[] = ['course_name'=>$course_name[$key],'university_name'=>$value];
            }
        }

        if($request->has('hobby')){
            foreach($request->get('hobby') as $value)
            {
                $hobbies[] = $value;
            }
        }

        // update or create entry of buddy Professional details
        $res = UserQualification::updateOrCreate(
                [
                    'user_id'=>Auth::user()->_id,
                ],
                [
                    'education'=>$data,
                    'hobbies'=>$hobbies
                ]
            );
        return $res;
    }

    public function changeDefaultRole(Request $request){

        $id = $request->input('id');
        $status = $request->input('value');
        $status = strtoupper($status);

        // $authId = new \MongoDB\BSON\ObjectID($id);
        $authId=Auth::user()->id;
        $user = User::where('_id', $authId)->first();

        if(!$user){
            return response()->json(['status'=>false,'message'=>'User not found. Please try again later.'], 200);
        }
        else{
            switch ($status) {
                case 'USER':
                    $user->defaultLoginRole = 'USER';
                    $user->save();
                    return response()->json(['status'=>true, 'message'=>'Profile changed successfully.'], 200);
                    break;

                case 'BUDDY':
                    if($user->buddyProfile==2){
                        $user->defaultLoginRole = 'BUDDY';
                        $user->save();
                        return response()->json(['status'=>true, 'message'=>'Profile changed successfully.'], 200);
                    }else{
                        return response()->json(['status'=>false, 'message'=>'Buddy application is not approved. Please contact admin.'], 200);
                    }
                    break;

                case 'AGENT':
                    if($user->agentProfile==2){
                        $user->defaultLoginRole = 'AGENT';
                        $user->save();
                        return response()->json(['status'=>true, 'message'=>'Profile changed successfully.'], 200);
                    }else{
                        return response()->json(['status'=>false, 'message'=>'Agent application is not approved. Please contact admin.'], 200);
                    }
                    break;

                default:
                    return response()->json(['status'=>false, 'message'=>'Something went wrong. Please try again later.'], 200);
                    break;
            }
        }
    }

    public function getState(Request $request){
        $states = file_get_contents(base_path('/data/states.json'));
        $states = json_decode($states, true);
        $states = $states['countries'];
        $country = $request->segment(3);

        $data = [];
        foreach ($states as $state) {
            if(strtolower($state['country'])==strtolower($country))
            {
                $data= $state['states'];
            }
        }

        return response()->json(['status'=>true,'states'=>$data],200);
    }

    public function enquiryList(Request $request){
        $start_date = date("Y-m-d", strtotime($request->start_date));
        $start_date1 =  date("Y", strtotime($request->start_date));
        $start_date2 =  date("m", strtotime($request->start_date));
        $start_date3 =  date("d", strtotime($request->start_date));
        $end_date1 = date("Y", strtotime($request->end_date));
        $end_date2 = date("m", strtotime($request->end_date));
        $end_date3 = date("d", strtotime($request->end_date));
        $end_date = date("Y-m-d", strtotime($request->end_date));

      $getProperties=UserEnquiry::where('user_id',Auth::user()->_id);

      if($start_date!='1970-01-01' && $end_date!='1970-01-01')
      {

          // $transactions = $transactions->where('created_at','>=','2020-10-01 ')->get();
          $getProperties = $getProperties->whereBetween(
              'created_at', array(
                  Carbon::createFromDate($start_date1, $start_date2, $start_date3),
                  Carbon::createFromDate($end_date1, $end_date2, $end_date3)
              )
              );


      }
      else{
          $start_date = '1970-01-01';
          $end_date = date('Y-m-d');
          
      }



 

      $status = $request->filter4;


 
     if($status=='In-Progress')
      {
         $getProperties=$getProperties->where('is_action',1);
      }else if($status=='Pending')
      {
         $getProperties=$getProperties->where('is_action',0);
      }else if($status=='Closed')
      {
         $getProperties=$getProperties->where('is_action',2);
      } 





      $getEnquiry =     $getProperties->orderBy('_id','DESC')->get();

      
    
      return view('frontend.profile.enquiry.index',['getEnquiry'=>@$getEnquiry,'start_date'=>$start_date,'end_date'=>$end_date]);
    }

    public function subscription(Request $request){

         
        $type = ($request->segment(5)) ? ($request->segment(5)) : ('user');
         
        $current=date('Y-m-d');

        $users = Transaction::where('user_id',\Auth::user()->id)->where('purchase_type',$type)->where('expiry_date','>=',$current )->count();
        if($users >0)
        {
            return redirect()->back()->with('success','Thank You. You have already Purchase Plans ');     
        }
        
        return view('frontend.profile.requests.subscribe');
    }
    public function renewSubscription(){
        return view('frontend.profile.requests.renew');
    }
    public function renew(Request $request)
    {
        $user_id=\Auth::user()->id;
        $plan_id=(string)$request->id;
        $number=(string)$request->number;
        $amount=$request->amount;
        $update = Transaction::where('_id',$request->transaction_id)->update(['renew' => (int)1]);
        $getPlans=Subscription::where('_id',$plan_id)->first();




            $skuid              = $request->sku_id;
            $sku_base_decode    = base64_decode($skuid);
            $sku_json_decode    = json_decode($sku_base_decode);
            $amount             = $sku_json_decode->plan_amount;
            $plan_currency      = $sku_json_decode->currency;




        foreach($getPlans->sub_plan as $plans){
            if($plans['number']==$number){
                $validity=$plans['validity'];
                $current = date("Y-m-d");
                $expiry_date=date('Y-m-d', strtotime("+".$validity." days"));
                $transaction = new Transaction;
                $transaction->plan_id=$plan_id;
                $transaction->user_id=$user_id;
                $transaction->count=(int)$plans['count'];
                $transaction->number=$plans['number'];
                $transaction->expiry_date=$expiry_date;
                $transaction->amount=$amount;
                $transaction->currency=$plan_currency;
                $transaction->validity=$plans['validity'];
                $transaction->purchase=(int)1;
                $transaction->listed=(int)0;
                $transaction->renew=(int)0;
                $transaction->purchase_date=$current;
                $transaction->purchase_type=$request->purchase_type??$getPlans->type;
                $transaction->save();
            }
        }
        if($request->purchase_type == 'cospace')
        {
            $listed = CoworkspaceProperty::where('transaction_id',$request->transaction_id)->count();
            $property_listed = $transaction->count < $listed ? $transaction->count : $listed;
            $update_p = CoworkspaceProperty::where('transaction_id',$request->transaction_id)->update(['transaction_id'=>$transaction->_id]);
        }elseif($request->purchase_type == 'swap'){
            $listed = SwapProperty::where('transaction_id',$request->transaction_id)->count();
            $property_listed = $transaction->count < $listed ? $transaction->count : $listed;
            $update_p = SwapProperty::where('transaction_id',$request->transaction_id)->update(['transaction_id'=>$transaction->_id]);
        }elseif($request->purchase_type == 'rent'){
            $listed = Property::where('transaction_id',$request->transaction_id)->count();
            $property_listed = $transaction->count < $listed ? $transaction->count : $listed;
            $update_p = Property::where('transaction_id',$request->transaction_id)->update(['transaction_id'=>$transaction->_id]);
        }
        Transaction::where('transaction_id',$transaction->_id)->update(['listed',(int)$property_listed]);
        return response()->json(['status'=>1]);
    }
}
