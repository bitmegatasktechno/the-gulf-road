<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\{
    ChatMessage, ChatThread, User, UserAddress, UserProfessionalDetail, UserDocument, UserQualification,Property,UserEnquiry,Notification
};
use Illuminate\Validation\Rule;
use App\Rules\IsUserCurrentPassword;
use App\Http\Requests\{
    BuddyRequestValidation, AgentRequestValidation
};

class ChatController extends Controller
{
    public function user_chat(){
        $other = request()->segment(4);
        $loc = request()->segment(3);
        $locale = request()->segment(1);
        $sender=\Auth::user()->id;
        $checkThread=ChatThread::where(function ($query) use($sender){
            $query->where('sender_id', $sender)
            ->orWhere('reciever_id', $sender);
          })->where(function ($query) use($other){
            $query->where('sender_id', $other)
            ->orWhere('reciever_id', $other);
          })->first();
        if(!@$checkThread){
            $thread=new ChatThread;
            $thread->sender_id=$sender;
            $thread->reciever_id=$other;
            $thread->save();
        }
        $getOtherUsers= ChatThread::where(function ($query) use($sender){
                            $query->where('sender_id', $sender)->orWhere('reciever_id', $sender);
                        })->get();
        if(@$other==''){
            $checkFirst=ChatThread::where('sender_id',$sender)->orWhere('reciever_id',$sender)->first();
        if(@$checkFirst->sender_id==$sender){
            $other=$checkFirst->reciever_id;
        }else{
            $other=$checkFirst->sender_id;
        }
        }else{
            $other=$other;
        }
        $getAllMessage=ChatMessage::where(function ($query) use($sender){
            $query->where('sender_id', $sender)
            ->orWhere('reciever_id', $sender);
          })->where(function ($query) use($other){
            $query->where('sender_id', $other)
            ->orWhere('reciever_id', $other);
          })->get();
          if(@$getAllMessage=='[]'){
            $getAllMessage=[];
          }
        $username=User::where('_id',$other)->first();
        return view('frontend.chat.index',compact('getOtherUsers','getAllMessage','username','loc','locale'));
    }

    public function send_message(Request $request){
        
        $sender=\Auth::user()->id;
        $other=$request->reciever;

        if($request->file('media'))
        {
            $file = $request->file('media');
            $name = md5($file->getClientOriginalName().time()).".".$file->getClientOriginalExtension();
            $file->move(public_path('/uploads/chat/'), $name);
            $request->message .= " <br> <a style=\"color:white;\" href=\"/uploads/chat/".$name."\" target=\"_blank\"><i class=\"fa fa-file\"></i> ".$file->getClientOriginalName()."</a>";
        }

        
        $checkThread=ChatThread::where(function ($query) use($sender){
            $query->where('sender_id', $sender)
            ->orWhere('reciever_id', $sender);
          })->where(function ($query) use($other){
            $query->where('sender_id', $other)
            ->orWhere('reciever_id', $other);
          })->first();
        if(@$checkThread){
            $thread=$checkThread->_id;
            $update=\DB::table('chat_thread')
            ->where('_id',$thread)
            ->update(array('message'=>$request->message));
        }else{
            $thread=new ChatThread;
            $thread->sender_id=$sender;
            $thread->reciever_id=$other;
            $thread->message=$request->message;
            $thread->save();
            $thread=$thread->_id;
        }

        $message=new ChatMessage;
        $message->sender_id=$sender;
        $message->reciever_id=$request->reciever;
        $message->message=$request->message;
        $message->thread_id=$thread;
        $message->save();
        
        $newNot = new Notification;
        $newNot->receiver_id = $request->reciever;
        $newNot->sender_id = $sender;
        $newNot->message = 'You got new Message';
        $newNot->read = (int)0;
        $newNot->save();

        return redirect()->back();

    }

    public function all_message(){
        $loc = request()->segment(3);
        $other = request()->segment(4);
        $sender=\Auth::user()->id;
        $getAllMessage=ChatMessage::where(function ($query) use($sender){
            $query->where('sender_id', $sender)
            ->orWhere('reciever_id', $sender);
          })->where(function ($query) use($other){
            $query->where('sender_id', $other)
            ->orWhere('reciever_id', $other);
          })->get();
        $username=User::where('_id',$other)->first();
        return view('frontend.chat.index',compact('getAllMessage','username'));
    }
}