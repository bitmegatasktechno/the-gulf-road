<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Controllers\StripeController;
use Illuminate\Support\Facades\Mail;
use App\Traits\Common;
use Illuminate\Http\Request;
use Auth, Session;

 
 
use \Carbon\Carbon;
use Illuminate\Validation\Rule;
use App\Mail\PropertyRegistrationUser;
use App\Mail\PropertyRegistrationAdmin;
use \App\Models\{
    Property, CoworkspaceProperty, SwapProperty,Amenity,Review,Subscription,Transaction,User, City
};

use Stripe\Charge;
use Stripe\Stripe;
use AmrShawky\LaravelCurrency\Facade\Currency;

class PropertyController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        

    }

    /**
    * return main page of listing of property
    */
    public function index(Request $request){
        $property_listed_count = 0;
        $property_for_sale_rent = true;
        $plan_property_listed_count = 0;
        $transactions = Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','!=','user')->get();
        foreach ($transactions as $transaction) {
            if($transaction->purchase_type == 'rent'){
                $plan_property_listed_count = $transaction->count;
            }
        }
        $property_listed_count = Property::where('user_id', \Auth::user()->id)->orderBy('_id','desc')->get();
        if($plan_property_listed_count >= $property_for_sale_rent){
            $property_for_sale_rent = false;
        }
        return view('frontend.list-property.index', compact('transactions','property_for_sale_rent'));
    }

    /**
    * return view to create property
    */

    public function createRentSaleProperty(Request $request){


         

        return view('frontend.list-property.rentsale.index');
    }

    /**
    * return view to create swap property
    */
    public function createSwapProperty(Request $request){
        return view('frontend.list-property.swap.index');
    } 

    /**
    * return view to create cowrorkspace property
    */
    public function createCoworkspaceProperty(Request $request){
        return view('frontend.list-property.coworkspace.index');
    }

     /**
    * return all saved rent/sale properties listing by user
    */
    public function myListings(){
        $auth_id = Auth::user()->_id;
        $data = Property::where('user_id', $auth_id)->orderBy('_id','desc')->get();
        return view('frontend.profile.mylisting.index', compact('data'));
    }

    /**
    * return all saved coworkspaces listing by user
    */
    public function myCoworkspaces(){
        $auth_id = Auth::user()->_id;
        $data = CoworkspaceProperty::where('user_id', $auth_id)->orderBy('_id','desc')->get();
        return view('frontend.profile.mycoworkspaces.index', compact('data'));
    }

    /**
    * return all saved swap listing by user
    */
    public function mySwapProperties(){
        $auth_id = Auth::user()->_id;
        $data = SwapProperty::where('user_id', $auth_id)->orderBy('_id','desc')->get();
        return view('frontend.profile.myswap.index', compact('data'));
    }


    /**
    * return edit view of sale rent property
    */
    public function getRentSaleEditProperty(Request $request){
        $id = request()->segment(5);
        $data = Property::where('_id', $id)->where('user_id', Auth::user()->_id)->firstOrFail();
        return view('frontend.profile.edit-property.index', compact('data'));
    }

    /**
    * return edit view of coworkspace property
    */
    public function editCoworkspace(Request $request){
        $id = request()->segment(5);
        $data = CoworkspaceProperty::where('_id', $id)->where('user_id', Auth::user()->_id)->firstOrFail();
        $state_data = getSelectedCountryStates($data->country);
        return view('frontend.profile.edit-coworkspace.index', compact('data','state_data'));
    }

    /**
    * return edit view of swap property
    */
    public function editSwapProperty(Request $request){
        $id = request()->segment(5);
        $data = SwapProperty::where('_id', $id)->where('user_id', Auth::user()->_id)->firstOrFail();
        return view('frontend.profile.edit-swap.index', compact('data'));
    }

    /**
    * validate rent/sale property and add property
    */
    public function validateRentSaleProperty(Request $request, $step){
        $step = $request->segment(4) ? $request->segment(4): 0;
        $loc = $request->country;

        switch ($step) {
            case '1':
                $request->validate([
                    'listing_type'=>'bail|required|in:rent,sale,both',
                    'house_type'=>'bail|required',
                    'location_name'=>'bail|required|max:200',
                    /*'lat'=>'bail|required',
                    'longitude'=>'bail|required',*/
                    'house_number'=>'bail|required|max:50',
                    'landmark'=>'bail|nullable|max:200',
                    'country'=>'bail|required|max:200',
                    'city'=>'bail|required|max:200',
                ]);
                return response()->json(['status'=>true],200);
                break;

            case '2':
                $request->validate([
                    'build_up_area'=>'bail|required|numeric',
                    'build_up_area_unit'=>'bail|required',
                    'carpet_area_unit'=>'bail|nullable',
                    'carpet_area'=>'bail|nullable|numeric|lt:build_up_area',
                    'no_of_bedrooms'=>'bail|required|numeric|min:1|max:15',
                    'no_of_bathrooms'=>'bail|required|numeric|min:1|max:15',
                    'furnishing_type'=>'bail|required',
                    'flooring_type'=>'bail|required',
                    'parking_available'=>'bail|required|in:yes,no',
                    'no_of_covered_parking'=>'bail|required|numeric|min:0|max:15',
                    'no_of_open_parking'=>'bail|required|numeric|min:0|max:15',
                ],
                [
                    'carpet_area.lt'=>'The carpet area must be less than build up area.'
                ]);
                return response()->json(['status'=>true],200);
                break;

            case '3':
                // if($request->has('amenties'))
                // {
                    return response()->json(['status'=>true],200);
                // }else{
                //     return response()->json(['status'=>false,'message'=>'Please select some amenties.'],200);
                // }
                break;

            case '4':
                
                $request->validate([
                    'property_title'=>'bail|required|string|max:200',
                    'property_description'=>'bail|required|string|max:1000',
                    'smoking_allowed'=>'bail|required|in:yes,no',
                    'parties_allowed'=>'bail|required|in:yes,no',
                    'pets_allowed'=>'bail|required|in:yes,no',
                    'additional_rules'=>'bail|nullable|max:5000',
                ]);
                return response()->json(['status'=>true],200);
                break;

            case '5':

                $request->validate([
                    'files.*'=>'bail|required|mimes:webp,jpeg,jpg,png|max:5000',
                ],[
                    'files.*.required'=>'The file is required.',
                    'files.*.mimes'=>'The file must be type of jpeg,jpg,png.',
                ]);
                return response()->json(['status'=>true],200);
                break;

            case '6':
            	$errors =[];
                if($request->input('listing_type')=='rent'){
                    $errors['rent'] = 'bail|required|numeric|min:0';
                    $errors['maintenance_charges'] = 'bail|required|numeric|min:0';
                    $errors['security_amount'] = 'bail|required|numeric|min:0';
                    $errors['available_from'] = 'bail|required|date_format:d/m/Y';

                    $errors['rent_currency'] = 'bail|required';
                    $errors['maintenance_charges_currency'] = 'bail|required';
                    $errors['security_amount_currency'] = 'bail|required';
                    $errors['rent_frequency'] = 'bail|required';

                    if($request->filled('instant_book_available') && $request->input('instant_book_available')=='checked'){
                        $errors['night_price'] = 'bail|required|numeric|min:0';
                        $errors['night_price_currency'] = 'bail|required';
                    }
                }
                elseif ($request->input('listing_type')=='sale') {
                    $errors['price'] = 'bail|required|numeric|min:0';
                    $errors['price_currency'] = 'bail|required';
                    if($request->filled('discount_available') && $request->input('discount_available')=='checked'){
                        $errors['discount_price'] = 'bail|required|numeric|min:0';
                        $errors['discount_price_currency'] = 'bail|required';
                    }
                }
                else
                {
                    $errors['rent'] = 'bail|required|numeric|min:0';
                    $errors['maintenance_charges'] = 'bail|required|numeric|min:0';
                    $errors['security_amount'] = 'bail|required|numeric|min:0';
                    $errors['available_from'] = 'bail|required|date_format:d/m/Y';

                    $errors['rent_currency'] = 'bail|required';
                    $errors['rent_frequency'] = 'bail|required';
                    $errors['maintenance_charges_currency'] = 'bail|required';
                    $errors['security_amount_currency'] = 'bail|required';

                    if($request->filled('instant_book_available') && $request->input('instant_book_available')=='checked'){
                        $errors['night_price'] = 'bail|required|numeric|min:0';
                        $errors['night_price_currency'] = 'bail|required';
                    }

                    $errors['price'] = 'bail|required|numeric|min:0';
                    $errors['price_currency'] = 'bail|required';

                    if($request->filled('discount_available') && $request->input('discount_available')=='checked'){
                        $errors['discount_price'] = 'bail|required|numeric|min:0';
                        $errors['discount_price_currency'] = 'bail|required';
                    }
                }
            	
                $request->validate($errors);
                return response()->json(['status'=>true],200);
                break;

            case '7':
            	$errors =[];
            	
                $errors= [
                    // step1
            		'listing_type'=>'bail|required|in:rent,sale,both',
                    'house_type'=>'bail|required',
                    'location_name'=>'bail|required|max:200',
                    'house_number'=>'bail|required|max:50',
                    'landmark'=>'bail|nullable|max:200',
                    'country'=>'bail|required|max:200',

                    // step2
                    'build_up_area'=>'bail|required|numeric',
                    'build_up_area_unit'=>'bail|required',
                    'carpet_area_unit'=>'bail|nullable',
                    'carpet_area'=>'bail|nullable|numeric|lt:build_up_area',
                    'no_of_bedrooms'=>'bail|required|numeric|min:1|max:15',
                    'no_of_bathrooms'=>'bail|required|numeric|min:1|max:15',
                    'furnishing_type'=>'bail|required',
                    'flooring_type'=>'bail|required',
                    'parking_available'=>'bail|required|in:yes,no',
                    'no_of_covered_parking'=>'bail|required|numeric|min:0|max:15',
                    'no_of_open_parking'=>'bail|required|numeric|min:0|max:15',

                    // step3
                    'amenties.*'=>'bail|required',
                    
                    // step4
                    'property_title'=>'bail|required|string|max:200',
                    'property_description'=>'bail|required|string|max:1000',
                    'smoking_allowed'=>'bail|required|in:yes,no',
                    'parties_allowed'=>'bail|required|in:yes,no',
                    'pets_allowed'=>'bail|required|in:yes,no',
                    'additional_rules'=>'bail|nullable|max:5000',
            	];

                if($request->input('image_skip')==0){
                    // step5
                    $errors['files.*'] = 'bail|required|mimes:webp,jpeg,jpg,png|max:5000';
                }
                // step6
            	if($request->input('listing_type')=='rent'){
                    $errors['rent'] = 'bail|required|numeric|min:0';
                    $errors['maintenance_charges'] = 'bail|required|numeric|min:0';
                    $errors['security_amount'] = 'bail|required|numeric|min:0';
                    $errors['available_from'] = 'bail|required|date_format:d/m/Y';

                    $errors['rent_currency'] = 'bail|required';
                    $errors['rent_frequency'] = 'bail|required';
                    $errors['maintenance_charges_currency'] = 'bail|required';
                    $errors['security_amount_currency'] = 'bail|required';

                    if($request->filled('instant_book_available') && $request->input('instant_book_available')=='checked'){
                        $errors['night_price'] = 'bail|required|numeric|min:0';
                        $errors['night_price_currency'] = 'bail|required';
                    }
                }
                elseif ($request->input('listing_type')=='sale') {
                    $errors['price'] = 'bail|required|numeric|min:0';
                    $errors['price_currency'] = 'bail|required';
                    if($request->filled('discount_available') && $request->input('discount_available')=='checked'){
                        $errors['discount_price'] = 'bail|required|numeric|min:0';
                        $errors['discount_price_currency'] = 'bail|required';
                    }
                }
                else{
                    $errors['rent'] = 'bail|required|numeric|min:0';
                    $errors['maintenance_charges'] = 'bail|required|numeric|min:0';
                    $errors['security_amount'] = 'bail|required|numeric|min:0';
                    $errors['available_from'] = 'bail|required|date_format:d/m/Y';

                    $errors['rent_currency'] = 'bail|required';
                    $errors['rent_frequency'] = 'bail|required';
                    $errors['maintenance_charges_currency'] = 'bail|required';
                    $errors['security_amount_currency'] = 'bail|required';

                    if($request->filled('instant_book_available') && $request->input('instant_book_available')=='checked'){
                        $errors['night_price'] = 'bail|required|numeric|min:0';
                        $errors['night_price_currency'] = 'bail|required';
                    }

                    $errors['price'] = 'bail|required|numeric|min:0';
                    $errors['price_currency'] = 'bail|required';

                    if($request->filled('discount_available') && $request->input('discount_available')=='checked'){
                        $errors['discount_price'] = 'bail|required|numeric|min:0';
                        $errors['discount_price_currency'] = 'bail|required';
                    }
                }
                if($request->filled('premium_available') && $request->input('premium_available')=='yes'){
                    $errors['name'] = 'bail|required';
                    $errors['number'] = 'bail|required|numeric|min:0';
                    $errors['month'] = 'bail|required';
                    $errors['year'] = 'bail|required';
                    $errors['cvv'] = 'bail|required|numeric|min:0';
                }
                $request->validate($errors);
                
                $location = Common::getLatLong($request->location_name);
                $transaction_id = Session::get('transaction_id');

                $obj = new Property();


                $obj->created_at = Carbon::now();
                $obj->updated_at = Carbon::now();
                $obj->transaction_id = $transaction_id;

                $obj->user_id = Auth::user()->_id;
                 $location = [(float) $request->longitude,(float)$request->lat]; 
                /*$location['type'] = "Point";
                $location['coordinates'] = [(float) $request->longitude,(float)$request->lat];
                $location = json_encode($location);*/
               /* $location = (object) $location;*/
                
                // dd($location);

                // step1
                $obj->listing_type = $request->listing_type;
                $obj->house_type = $request->house_type;
                $obj->location_name = $request->location_name;
                $obj->house_number = $request->house_number;
                $obj->landmark = $request->landmark;
                $obj->country = $request->country;
                $obj->city = $request->city;
                $obj->latitude = $request->lat;
                $obj->longitude = $request->longitude;
                $obj->location = $location;
                
                // step2
                $obj->build_up_area = $request->build_up_area;
                $obj->build_up_area_unit = $request->build_up_area_unit;
                $obj->carpet_area = $request->carpet_area;
                $obj->carpet_area_unit = $request->carpet_area_unit;
                $obj->no_of_bedrooms = $request->no_of_bedrooms;
                $obj->no_of_bathrooms = $request->no_of_bathrooms;
                $obj->furnishing_type = $request->furnishing_type;
                $obj->flooring_type = $request->flooring_type;
                $obj->parking_available = $request->parking_available;


                if(session('is_premium') != ''){
                    $obj->is_premium = session('is_premium');
                    \Session::forget('is_premium');
                }

                $obj->created_at = Carbon::now();
                $obj->updated_at = Carbon::now();

                if($request->parking_available=='yes'){
                    $obj->no_of_covered_parking = $request->no_of_covered_parking;
                    $obj->no_of_open_parking = $request->no_of_open_parking;
                }else{
                    $obj->no_of_covered_parking = 0;
                    $obj->no_of_open_parking = 0;
                }

                // step3
                $amenties = [];
                foreach($request->get('amenties') as $key => $value) {
                    $amenties[] = $value;
                }
                $obj->amenties = $amenties;
                
                // step4
                $obj->property_title = $request->property_title;
                $obj->property_description = $request->property_description;
                $obj->smoking_allowed = $request->smoking_allowed;
                $obj->parties_allowed = $request->parties_allowed;
                $obj->pets_allowed = $request->pets_allowed;
                $obj->additional_rules = $request->additional_rules;


                // step5
                $files = [];
                if($request->hasfile('files'))
                {
                    foreach($request->file('files') as $file)
                    {
                        $name=Auth::user()->_id.'-'.md5($file->getClientOriginalName().time()).".".$file->getClientOriginalExtension();
                        $file->move(public_path('/uploads/properties'), $name);
                        $files[] = $name;
                    }
                }
                $obj->files = $files;

                // step6
                if($request->input('listing_type')=='rent'){
                    $obj->rent = (int)$request->rent;
                    $obj->security_amount = (int)$request->security_amount;
                    $obj->maintenance_charges = (int)$request->maintenance_charges;

                    $obj->available_from = $request->available_from;
                    $obj->rent_currency = $request->rent_currency;
                    $obj->rent_frequency = $request->rent_frequency;
                    $obj->maintenance_charges_currency = $request->maintenance_charges_currency;
                    $obj->security_amount_currency = $request->security_amount_currency;

                    $obj->instant_book_available = $request->instant_book_available;
                    if($request->filled('instant_book_available') && $request->input('instant_book_available')=='checked'){
                       
                        $obj->night_price = (int)$request->night_price;
                        $obj->night_price_currency = $request->night_price_currency;
                    }
                    if($request->filled('premium_available') && $request->input('premium_available')=='yes'){

                        if(session('is_premium') != ''){
                            $obj->is_premium = session('is_premium');
                            \Session::forget('is_premium');
                        }


                    }
                }
                elseif ($request->input('listing_type')=='sale') {
                    $obj->price = (int)$request->price;
                    $obj->price_currency = $request->price_currency;
                    $obj->discount_available = $request->discount_available;

                    if($request->filled('discount_available') && $request->input('discount_available')=='checked'){
                        $obj->discount_price = (int)$request->discount_price;
                        $obj->discount_price_currency = $request->discount_price_currency;
                    }
                }
                else{
                    $obj->rent = (int)$request->rent;
                    $obj->security_amount = (int)$request->security_amount;
                    $obj->maintenance_charges = (int)$request->maintenance_charges;

                    $obj->available_from = $request->available_from;
                    $obj->rent_currency = $request->rent_currency;
                    $obj->rent_frequency = $request->rent_frequency;
                    $obj->maintenance_charges_currency = $request->maintenance_charges_currency;
                    $obj->security_amount_currency = $request->security_amount_currency;

                    $obj->instant_book_available = $request->instant_book_available;
                    if($request->filled('instant_book_available') && $request->input('instant_book_available')=='checked'){
                       
                        $obj->night_price = (int)$request->night_price;
                        $obj->night_price_currency = $request->night_price_currency;
                    }
                    // sale
                    $obj->price = (int)$request->price;
                    $obj->price_currency = $request->price_currency;
                    $obj->discount_available = $request->discount_available;

                    if($request->filled('discount_available') && $request->input('discount_available')=='checked'){
                        $obj->discount_price = (int)$request->discount_price;
                        $obj->discount_price_currency = $request->discount_price_currency;
                    }
                }
                
                $current=date('Y-m-d');
                $checkPlan = Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','rent')->where('expiry_date','>=',$current)->latest()->first();
                if(@$checkPlan){
                    $obj->plan_id = @$checkPlan->plan_id;
                    if(@$checkPlan->listed < @$checkPlan->count){
                        $obj->show = (int)1;
                    }else{
                        $obj->show = (int)0;
                    }
                    $update=\DB::table('transaction')
                    ->where('_id',@$checkPlan->_id)
                    ->update(['listed'=>@$checkPlan->listed+1]);
                }
                $obj->is_approved = (int)0;

                $obj->created_at = Carbon::now();
                $obj->updated_at = Carbon::now();
                // dd($obj);

                if($obj->save()){
                    Mail::to('info@thegulfroad.com')->send(new PropertyRegistrationAdmin($obj));
                    Mail::to(Auth::user()->email)->send(new PropertyRegistrationUser($obj));
                    return response()->json(['status'=>true,'message'=>'Property listed successfully. Once Your property approved then property will be visible on listing page','url'=>url(app()->getLocale().'/mylistings'.'/'.strtolower($loc))],200);
                }
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);

            default:
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;
        }
    }

    /**
    * update rent/sale property
    **/
    public function updateRentSaleProperty(Request $request){
        $step = $request->segment(4) ? $request->segment(4): 0;
        $object = Property::where('_id', $request->property_id)->where('user_id', Auth::user()->_id)->first();
        if(!$object){
            return response()->json(['status'=>false,'message'=>'Property not found, Please try again later.'],200);
        }
        switch ($step) {
            case '1':
                $request->validate([
                    'listing_type'=>'bail|required|in:rent,sale,both',
                    'house_type'=>'bail|required',
                    'location_name'=>'bail|required|max:200',
                    'house_number'=>'bail|required|max:50',
                    'landmark'=>'bail|nullable|max:200',
                    'country'=>'bail|required|max:200',
                ]);

                /*$location['type'] = "Point";
                $location['coordinates'] = [(float) $request->longitude,(float)$request->latitude];
                $location = (object) $location;
                $location = json_encode($location);*/

                $location = [(float)$request->longitude,(float)$request->latitude];

                
                $object->house_type     = $request->house_type;
                $object->listing_type   = $request->listing_type;
                $object->location_name  = $request->location_name;
                $object->house_number   = $request->house_number;
                $object->landmark       = $request->landmark;
                $object->country        = $request->country;
                $object->latitude       = $request->latitude;
                $object->longitude      = $request->longitude;
                $object->location      = $location;
                $object->updated_at     = Carbon::now();

                if($object->save()){
                    return response()->json(['status'=>true,'message'=>'Property details updated successfully.'],200);
                }
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;

            case '2':
                $request->validate([
                    'build_up_area'=>'bail|required|numeric|between:100,250000',
                    'build_up_area_unit'=>'bail|required',
                    'carpet_area_unit'=>'bail|nullable',
                    'carpet_area'=>'bail|nullable|numeric|between:100,250000|lt:build_up_area',
                    'no_of_bedrooms'=>'bail|required|numeric|min:1|max:15',
                    'no_of_bathrooms'=>'bail|required|numeric|min:1|max:15',
                    'furnishing_type'=>'bail|required',
                    'flooring_type'=>'bail|required',
                    'parking_available'=>'bail|required|in:yes,no',
                    'no_of_covered_parking'=>'bail|required|numeric|min:0|max:15',
                    'no_of_open_parking'=>'bail|required|numeric|min:0|max:15',
                ],
                    [
                        'carpet_area.lt'=>'The carpet area must be less than build up area.'
                    ]
                );

                $object->build_up_area = $request->build_up_area;
                $object->build_up_area_unit = $request->build_up_area_unit;
                $object->carpet_area_unit = $request->carpet_area_unit;
                $object->carpet_area = $request->carpet_area;
                $object->no_of_bedrooms = $request->no_of_bedrooms;
                
                $object->no_of_bathrooms = $request->no_of_bathrooms;
                $object->furnishing_type = $request->furnishing_type;
                $object->flooring_type = $request->flooring_type;
                $object->parking_available = $request->parking_available;

                if($request->parking_available=='yes'){
                    $object->no_of_covered_parking = $request->no_of_covered_parking;
                    $object->no_of_open_parking = $request->no_of_open_parking;
                }else{
                    $object->no_of_covered_parking = 0;
                    $object->no_of_open_parking = 0;
                }
                
                $object->updated_at = Carbon::now();
                if($object->save()){
                    return response()->json(['status'=>true,'message'=>'Property details updated successfully.'],200);
                }
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;

            case '3':
                $request->validate([
                    'amenties.*'=>'bail|required',
                ]);
                $amenties = [];
                foreach($request->get('amenties') as $key => $value) {
                    $amenties[] = $value;
                }
                $object->amenties = $amenties;
                $object->updated_at = Carbon::now();

                if($object->save()){
                    return response()->json(['status'=>true,'message'=>'Property details updated successfully.'],200);
                }
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;

            case '4':
                $request->validate([
                    'property_title'=>'bail|required|string|max:200',
                    'property_description'=>'bail|required|string|max:1000',
                    'smoking_allowed'=>'bail|required|in:yes,no',
                    'parties_allowed'=>'bail|required|in:yes,no',
                    'pets_allowed'=>'bail|required|in:yes,no',
                    'additional_rules'=>'bail|nullable|max:5000',
                ]);

                $object->property_title = $request->property_title;
                $object->property_description = $request->property_description;
                $object->smoking_allowed = $request->smoking_allowed;
                $object->parties_allowed = $request->parties_allowed;
                $object->pets_allowed = $request->pets_allowed;
                $object->additional_rules = $request->additional_rules;
                $object->updated_at = Carbon::now();

                if($object->save()){
                    return response()->json(['status'=>true,'message'=>'Property details updated successfully.'],200);
                }
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;

            case '5':
                $request->validate([
                    'files.*'=>'bail|required|mimes:webp,jpeg,jpg,png|max:5000',
                ],[
                    'files.*.required'=>'The file is required.',
                    'files.*.mimes'=>'The file must be type of jpeg,jpg,png.',
                ]);
                
                $files = [];
                $exists_files = $object->files;

                if($request->exist_file){
                    $files  = $request->exist_file;
                }
                
                if($request->hasfile('files')){
                    foreach($request->file('files') as $file)
                    {
                        $name=Auth::user()->_id.'-'.md5($file->getClientOriginalName().time()).".".$file->getClientOriginalExtension();
                        $file->move(public_path('/uploads/properties'), $name);
                        $files[] = $name;
                    }
                }
                $object->files = $files;
                $object->updated_at = Carbon::now();
                if($object->save()){
                    return response()->json(['status'=>true,'message'=>'Property details updated successfully.'],200);
                }
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;

            case '6':
                $errors =[];
                if($object->listing_type=='rent'){
                    $errors['rent'] = 'bail|required|numeric|min:0';
                    $errors['maintenance_charges'] = 'bail|required|numeric|min:0';
                    $errors['security_amount'] = 'bail|required|numeric|min:0';
                    $errors['available_from'] = 'bail|required|date_format:d/m/Y';

                    $errors['rent_currency'] = 'bail|required';
                    $errors['maintenance_charges_currency'] = 'bail|required';
                    $errors['security_amount_currency'] = 'bail|required';
                    $errors['rent_frequency'] = 'bail|required';

                    if($request->filled('instant_book_available') && $request->input('instant_book_available')=='checked'){
                        $errors['night_price'] = 'bail|required|numeric|min:0';
                        $errors['night_price_currency'] = 'bail|required';
                    }
                }
                elseif ($object->listing_type=='sale') {
                    $errors['price'] = 'bail|required|numeric|min:0';
                    $errors['price_currency'] = 'bail|required';
                    if($request->filled('discount_available') && $request->input('discount_available')=='checked'){
                        $errors['discount_price'] = 'bail|required|numeric|min:0';
                        $errors['discount_price_currency'] = 'bail|required';
                    }
                }
                else
                {
                    $errors['rent'] = 'bail|required|numeric|min:0';
                    $errors['maintenance_charges'] = 'bail|required|numeric|min:0';
                    $errors['security_amount'] = 'bail|required|numeric|min:0';
                    $errors['available_from'] = 'bail|required|date_format:d/m/Y';

                    $errors['rent_currency'] = 'bail|required';
                    $errors['rent_frequency'] = 'bail|required';
                    $errors['maintenance_charges_currency'] = 'bail|required';
                    $errors['security_amount_currency'] = 'bail|required';

                    if($request->filled('instant_book_available') && $request->input('instant_book_available')=='checked'){
                        $errors['night_price'] = 'bail|required|numeric|min:0';
                        $errors['night_price_currency'] = 'bail|required';
                    }

                    $errors['price'] = 'bail|required|numeric|min:0';
                    $errors['price_currency'] = 'bail|required';

                    if($request->filled('discount_available') && $request->input('discount_available')=='checked'){
                        $errors['discount_price'] = 'bail|required|numeric|min:0';
                        $errors['discount_price_currency'] = 'bail|required';
                    }
                }
                
                $request->validate($errors);

                if($object->listing_type=='rent'){
                    $object->rent = (int)$request->rent;
                    $object->security_amount = (int)$request->security_amount;
                    $object->maintenance_charges = (int)$request->maintenance_charges;

                    $object->available_from = $request->available_from;
                    $object->rent_currency = $request->rent_currency;
                    $object->rent_frequency = $request->rent_frequency;
                    $object->maintenance_charges_currency = $request->maintenance_charges_currency;
                    $object->security_amount_currency = $request->security_amount_currency;

                    $object->instant_book_available = $request->instant_book_available;
                    if($request->filled('instant_book_available') && $request->input('instant_book_available')=='checked'){
                       
                        $object->night_price = (int)$request->night_price;
                        $object->night_price_currency = $request->night_price_currency;
                    }
                }
                elseif ($object->listing_type=='sale') {
                    $object->price = (int)$request->price;
                    $object->price_currency = $request->price_currency;
                    $object->discount_available = $request->discount_available;

                    if($request->filled('discount_available') && $request->input('discount_available')=='checked'){
                        $object->discount_price = (int)$request->discount_price;
                        $object->discount_price_currency = $request->discount_price_currency;
                    }
                }
                else{
                    $object->rent = (int)$request->rent;
                    $object->security_amount = (int)$request->security_amount;
                    $object->maintenance_charges = (int)$request->maintenance_charges;

                    $object->available_from = $request->available_from;
                    $object->rent_currency = $request->rent_currency;
                    $object->rent_frequency = $request->rent_frequency;
                    $object->maintenance_charges_currency = $request->maintenance_charges_currency;
                    $object->security_amount_currency = $request->security_amount_currency;

                    $object->instant_book_available = $request->instant_book_available;
                    if($request->filled('instant_book_available') && $request->input('instant_book_available')=='checked'){
                       
                        $object->night_price = (int)$request->night_price;
                        $object->night_price_currency = $request->night_price_currency;
                    }
                    // sale
                    $object->price = (int)$request->price;
                    $object->price_currency = $request->price_currency;
                    $object->discount_available = $request->discount_available;

                    if($request->filled('discount_available') && $request->input('discount_available')=='checked'){
                        $object->discount_price = (int)$request->discount_price;
                        $object->discount_price_currency = $request->discount_price_currency;
                    }
                }
                $object->updated_at = Carbon::now();
                
                if($object->save()){
                    return response()->json(['status'=>true,'message'=>'Property details updated successfully.'],200);
                }
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;

            default:
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;
        }
    }

    
    /**
    * delete rent/sale property
    **/
    public function deleteProperty(Request $request){
        $id = $request->id;
        $data = Property::where('user_id', Auth::user()->_id)->where('_id', $id)->first();
        if($data){
            if($data->delete()){
                return response()->json(['status'=>true,'message'=>'Property deleted successfully.'],200);
            }
            return response()->json(['status'=>false,'message'=>'Unable to delete property, Please try again later.'],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
        }
    }

    /**
    * validate coworkspce and add property
    */
    public function validateSaveCoworkspaceProperty(Request $request, $step){
        $step = $request->segment(4) ? $request->segment(4): 0;
        $loc = $request->country;
        switch ($step) {
            case '1':
                $request->validate([
                    'title'=>'bail|required|max:500',
                    'description'=>'bail|required|max:2000',
                ]);
                return response()->json(['status'=>true],200);
                break;

            case '2':
                $request->validate([
                    'address'=>'bail|required|max:500',
                    'location_name'=>'bail|required|max:100',
                    // 'unit_number'=>'bail|required|max:100',
                    // 'address_line_1'=>'bail|nullable|max:100',
                    // 'address_line_2'=>'bail|nullable|max:100',
                    // 'landmark'=>'bail|nullable|max:100',
                    'city'=>'bail|required|max:100',
                    'country'=>'bail|required|max:100',
                    // 'state'=>'bail|required|max:100',
                    // 'zip_code'=>'bail|required|max:50',
                ]);
                return response()->json(['status'=>true],200);
                break;

            case '3':
                // if(!$request->has('special_amenties'))
                // {
                // return response()->json(['status'=>false,'message'=>'Please select some special amenties.'],200);
                // }
                // if(!$request->has('food_amenties'))
                // {
                //     return response()->json(['status'=>false,'message'=>'Please select some food amenties.'],200);
                // }
                // elseif (!$request->has('facilities_amenties')) {
                //     return response()->json(['status'=>false,'message'=>'Please select some facilities amenties.'],200);
                // }
                // elseif (!$request->has('equipments_amenties')) {
                //     return response()->json(['status'=>false,'message'=>'Please select some equipments amenties.'],200);
                // }
                // elseif (!$request->has('seating_amenties')) {
                //     return response()->json(['status'=>false,'message'=>'Please select some seating amenties.'],200);
                // }
                // elseif (!$request->has('accessibility_amenties')) {
                //     return response()->json(['status'=>false,'message'=>'Please select some accessibility amenties.'],200);
                // }
                // else{
                    return response()->json(['status'=>true],200);
                // }
                
                break;

            case '4':
                $errors = [];
                $errors['monday'] = 'bail|required|in:closed,open';
                $errors['tuesday'] = 'bail|required|in:closed,open';
                $errors['wednesday'] = 'bail|required|in:closed,open';
                $errors['thursday'] = 'bail|required|in:closed,open';
                $errors['friday'] = 'bail|required|in:closed,open';
                $errors['saturday'] = 'bail|required|in:closed,open';
                $errors['sunday'] = 'bail|required|in:closed,open';
                // monday
                    
                if($request->input('monday')=='open'){
                    $errors['monday_start_time'] = 'bail|required|date_format:H:i';
                    $errors['monday_end_time'] = 'bail|required|date_format:H:i|after:monday_start_time';
                }

                if($request->input('tuesday')=='open'){
                    $errors['tuesday_start_time'] = 'bail|nullable|required_if:tuesday,open|date_format:H:i';
                    $errors['tuesday_end_time'] = 'bail|nullable|required_if:tuesday,open|date_format:H:i|after:tuesday_start_time';
                }
                if($request->input('wednesday')=='open'){
                    $errors['wednesday_start_time'] = 'bail|nullable|required_if:wednesday,open|date_format:H:i';
                    $errors['wednesday_end_time'] = 'bail|nullable|required_if:wednesday,open|date_format:H:i|after:wednesday_start_time';
                }
                if($request->input('thursday')=='open'){
                    $errors['thursday_start_time'] = 'bail|nullable|required_if:thursday,open|date_format:H:i';
                    $errors['thursday_end_time'] = 'bail|nullable|required_if:thursday,open|date_format:H:i|after:thursday_start_time';
                }
                if($request->input('friday')=='open'){
                    $errors['friday_start_time'] = 'bail|nullable|required_if:friday,open|date_format:H:i';
                    $errors['friday_end_time'] = 'bail|nullable|required_if:friday,open|date_format:H:i|after:friday_start_time';
                }
                if($request->input('saturday')=='open'){
                    $errors['saturday_start_time'] = 'bail|nullable|required_if:saturday,open|date_format:H:i';
                    $errors['saturday_end_time'] = 'bail|nullable|required_if:saturday,open|date_format:H:i|after:saturday_start_time';
                }
                if($request->input('sunday')=='open'){
                    $errors['sunday_start_time'] = 'bail|nullable|required_if:sunday,open|date_format:H:i';
                    $errors['sunday_end_time'] = 'bail|nullable|required_if:sunday,open|date_format:H:i|after:sunday_start_time';
                }
                $request->validate($errors,
                [                    
                    'monday_start_time.required_if'=>'The start time is required.',
                    
                    'monday_end_time.required_if'=>'The end time is required.',
                    'monday_end_time.after'=>'The end time must be greater than start time.',
                    

                    'tuesday_start_time.required_if'=>'The start time is required.',
                    
                    'tuesday_end_time.required_if'=>'The end time is required.',
                    'tuesday_end_time.after'=>'The end time must be greater than start time.',
                    

                    'wednesday_start_time.required_if'=>'The start time is required.',
                    'wednesday_end_time.required_if'=>'The end time is required.',
                    'wednesday_end_time.after'=>'The end time must be greater than start time.',

                    'thursday_start_time.required_if'=>'The start time is required.',
                    'thursday_end_time.required_if'=>'The end time is required.',
                    'thursday_end_time.after'=>'The end time must be greater than start time.',

                    'friday_start_time.required_if'=>'The start time is required.',
                    'friday_end_time.required_if'=>'The end time is required.',
                    'friday_end_time.after'=>'The end time must be greater than start time.',

                    'saturday_start_time.required_if'=>'The start time is required.',
                    'saturday_end_time.required_if'=>'The end time is required.',
                    'saturday_end_time.after'=>'The end time must be greater than start time.',

                    'sunday_start_time.required_if'=>'The start time is required.',
                    'sunday_end_time.required_if'=>'The end time is required.',
                    'sunday_end_time.after'=>'The end time must be greater than start time.',

                    'monday_start_time.date_format'=>'The start time format is not correct.',
                    'monday_end_time.date_format'=>'The end time format is not correct.',

                    'tuesday_start_time.date_format'=>'The start time format is not correct.',
                    'tuesday_end_time.date_format'=>'The start time format is not correct.',

                    'wednesday_start_time.date_format'=>'The start time format is not correct.',
                    'wednesday_end_time.date_format'=>'The start time format is not correct.',

                    'thursday_start_time.date_format'=>'The start time format is not correct.',
                    'thursday_end_time.date_format'=>'The start time format is not correct.',

                    'friday_start_time.date_format'=>'The start time format is not correct.',
                    'friday_end_time.date_format'=>'The start time format is not correct.',

                    'saturday_start_time.date_format'=>'The start time format is not correct.',
                    'saturday_end_time.date_format'=>'The start time format is not correct.',

                    'sunday_start_time.date_format'=>'The start time format is not correct.',
                    'sunday_end_time.date_format'=>'The start time format is not correct.',
                ]);
                return response()->json(['status'=>true],200);
                break;

            case '5':
                $request->validate([
                    'contact_number'=>'bail|required|numeric|regex:/[1-9]{1}[0-9]{5,14}/|digits_between:6,15',
                    'email'=>'bail|required|email|max:50',
                //    'website_url'=>'bail|required|max:200',
                    // 'facebook_url'=>'bail|required|max:200',
                    // 'twitter_url'=>'bail|required|max:200',
                    // 'instagram_url'=>'bail|required|max:200',
                ]);
                return response()->json(['status'=>true],200);
                break;

            case '6':
                // $request->validate([
                //     'files.*'=>'bail|mimes:jpeg,jpg,png|max:5000',
                // ],[
                //     // 'files.required'=>'The file is required.',
                //     'files.*.mimes'=>'The file must be type of jpeg,jpg,png',
                // ]);
                return response()->json(['status'=>true],200);
                break;

            case '7':
                $errors =[
                    'dedicated_desk_available'=>'bail|required',
                    'open_workspace_available' => 'bail|required',
                    'private_office_available' => 'bail|required',
                    'currency_type' => 'bail|required',
                ];
                if($request->input('open_workspace_available')==1 ){
                    $errors['open_space_daily_price'] = 'bail|required|numeric|between:1,250000';
                    $errors['open_space_weekly_price'] = 'bail|required|numeric|between:1,250000';
                    $errors['open_space_monthly_price'] = 'bail|required|numeric|between:1,250000';
                    $errors['open_space_daily_currency'] = 'bail|required|max:50';
                    $errors['open_space_weekly_currency'] = 'bail|required|max:50';
                    $errors['open_space_monthly_currency'] = 'bail|required|max:50';
                    $errors['open_space_seats'] = 'bail|required';
                }

                if($request->input('dedicated_desk_available')==1){
                    $errors['dedicated_space_daily_price'] = 'bail|required|numeric|between:1,250000';
                    $errors['dedicated_space_weekly_price'] = 'bail|required|numeric|between:1,250000';
                    $errors['dedicated_space_monthly_price'] = 'bail|required|numeric|between:1,250000';
                    $errors['dedicated_space_daily_currency'] = 'bail|required|max:50';
                    $errors['dedicated_space_weekly_currency'] = 'bail|required|max:50';
                    $errors['dedicated_space_monthly_currency'] = 'bail|required|max:50';
                    $errors['dedicated_space_seats'] = 'bail|required';
                }

                if($request->input('private_office_available')==1){
                    $errors['private_space_daily_price'] = 'bail|required|numeric|between:1,250000';
                    $errors['private_space_weekly_price'] = 'bail|required|numeric|between:1,250000';
                    $errors['private_space_monthly_price'] = 'bail|required|numeric|between:1,250000';
                    $errors['private_space_daily_currency'] = 'bail|required|max:50';
                    $errors['private_space_weekly_currency'] = 'bail|required|max:50';
                    $errors['private_space_monthly_currency'] = 'bail|required|max:50';
                    $errors['seating_capacity'] = 'bail|required';
                    $errors['white_board_available'] = 'bail|required';
                    $errors['no_of_private_office'] = 'bail|required';
                }

                $request->validate($errors);

                return response()->json(['status'=>true],200);
                break;

            case '8':
                $errors = [
                    // 'title'=>'bail|required|min:10|max:500',
                    // 'description'=>'bail|required|min:10|max:2000',
                    'address'=>'bail|required|max:500',
                    'location_name'=>'bail|required|max:100',
                    // 'unit_number'=>'bail|required|max:100',
                    // 'address_line_1'=>'bail|nullable|max:100',
                    // 'address_line_2'=>'bail|nullable|max:100',
                    // 'landmark'=>'bail|nullable|max:100',
                    'city'=>'bail|required|max:100',
                    'country'=>'bail|required|max:100',
                    // 'state'=>'bail|required|max:100',
                    // 'zip_code'=>'bail|required|max:50',
                    'monday'=>'bail|required|in:closed,open',
                    'tuesday'=>'bail|required|in:closed,open',
                    'wednesday'=>'bail|required|in:closed,open',
                    'thursday'=>'bail|required|in:closed,open',
                    'friday'=>'bail|required|in:closed,open',
                    'saturday'=>'bail|required|in:closed,open',
                    'sunday'=>'bail|required|in:closed,open',
                    'open_space_daily_price'=>'bail|required|numeric|between:1,250000',
                    'open_space_weekly_price'=>'bail|required|numeric|between:1,250000',
                    'open_space_monthly_price'=>'bail|required|numeric|between:1,250000',
                    'open_space_daily_currency'=>'bail|required|max:50',
                    'open_space_weekly_currency'=>'bail|required|max:50',
                    'open_space_monthly_currency'=>'bail|required|max:50',
                    'dedicated_desk_available'=>'bail|required',
                    'open_space_seats'=>'bail|required',
                    'email'=>'bail|required|email|max:50',
                    'contact_number'=>'bail|required|numeric|digits_between:5,15',
                    // 'website_url'=>'bail|required|max:200',
                    // 'facebook_url'=>'bail|required|max:200',
                    // 'twitter_url'=>'bail|required|max:200',
                    // 'instagram_url'=>'bail|required|max:200',
                    // 'food_amenties.*'=>'bail|required',
                    // 'seating_amenties.*'=>'bail|required',
                    // 'accessibility_amenties.*'=>'bail|required',
                    // 'equipments_amenties.*'=>'bail|required',
                    // 'facilities_amenties.*'=>'bail|required',
                ];
                
                
                // monday
                if($request->input('monday')=='open'){
                    $errors['monday_start_time'] = 'bail|required|date_format:H:i';
                    $errors['monday_end_time'] = 'bail|required|date_format:H:i|after:monday_start_time';
                }
                if($request->input('tuesday')=='open'){
                    $errors['tuesday_start_time'] = 'bail|nullable|required_if:tuesday,open|date_format:H:i';
                    $errors['tuesday_end_time'] = 'bail|nullable|required_if:tuesday,open|date_format:H:i|after:tuesday_start_time';
                }
                if($request->input('wednesday')=='open'){
                    $errors['wednesday_start_time'] = 'bail|nullable|required_if:wednesday,open|date_format:H:i';
                    $errors['wednesday_end_time'] = 'bail|nullable|required_if:wednesday,open|date_format:H:i|after:wednesday_start_time';
                }
                if($request->input('thursday')=='open'){
                    $errors['thursday_start_time'] = 'bail|nullable|required_if:thursday,open|date_format:H:i';
                    $errors['thursday_end_time'] = 'bail|nullable|required_if:thursday,open|date_format:H:i|after:thursday_start_time';
                }
                if($request->input('friday')=='open'){
                    $errors['friday_start_time'] = 'bail|nullable|required_if:friday,open|date_format:H:i';
                    $errors['friday_end_time'] = 'bail|nullable|required_if:friday,open|date_format:H:i|after:friday_start_time';
                }
                if($request->input('saturday')=='open'){
                    $errors['saturday_start_time'] = 'bail|nullable|required_if:saturday,open|date_format:H:i';
                    $errors['saturday_end_time'] = 'bail|nullable|required_if:saturday,open|date_format:H:i|after:saturday_start_time';
                }
                if($request->input('sunday')=='open'){
                    $errors['sunday_start_time'] = 'bail|nullable|required_if:sunday,open|date_format:H:i';
                    $errors['sunday_end_time'] = 'bail|nullable|required_if:sunday,open|date_format:H:i|after:sunday_start_time';
                }
                if($request->input('image_skip')==0){
                    // step5
                    // $errors['files.*'] = 'bail|required|mimes:jpeg,jpg,png|max:5000';
                }
                if($request->input('dedicated_desk_available')==1){
                    $errors['dedicated_space_daily_price'] = 'bail|required|numeric|between:1,250000';
                    $errors['dedicated_space_weekly_price'] = 'bail|required|numeric|between:1,250000';
                    $errors['dedicated_space_monthly_price'] = 'bail|required|numeric|between:1,250000';
                    $errors['dedicated_space_daily_currency'] = 'bail|required|max:50';
                    $errors['dedicated_space_weekly_currency'] = 'bail|required|max:50';
                    $errors['dedicated_space_monthly_currency'] = 'bail|required|max:50';
                    $errors['dedicated_space_seats'] = 'bail|required';
                }
                if($request->input('private_office_available')==1){
                    $errors['private_space_daily_price'] = 'bail|required|numeric|between:1,250000';
                    $errors['private_space_weekly_price'] = 'bail|required|numeric|between:1,250000';
                    $errors['private_space_monthly_price'] = 'bail|required|numeric|between:1,250000';
                    $errors['private_space_daily_currency'] = 'bail|required|max:50';
                    $errors['private_space_weekly_currency'] = 'bail|required|max:50';
                    $errors['private_space_monthly_currency'] = 'bail|required|max:50';
                    $errors['seating_capacity'] = 'bail|required';
                    $errors['white_board_available'] = 'bail|required';
                    $errors['no_of_private_office'] = 'bail|required';
                }

                $request->validate($errors,
                    [
                        // 'files.*.required'=>'The file is required.',
                        // 'files.*.mimes'=>'The file must be type of jpeg,jpg,png.',
                    ]);

                /*$location = Common::getLatLong($request->address);
                $location = [(float) $location['lng'],(float)$location['lat']];*/

                 $location = [(float) $request->longitude,(float) $request->latitude];
                $transaction_id = Session::get('transaction_id');
            
                $obj = new CoworkspaceProperty();


                $obj->created_at = Carbon::now();
                $obj->updated_at = Carbon::now();

                $obj->user_id = Auth::user()->_id;
                $obj->transaction_id = $transaction_id;

                // step1
                $obj->title = $request->title;
                $obj->description = $request->description;

                // step2
                $obj->address = $request->address;
                $obj->location_name = $request->location_name;
                $obj->unit_number = @$request->unit_number;
                $obj->address_line_1 = @$request->address_line_1;
                $obj->address_line_2 = @$request->address_line_2;
                $obj->city = @$request->city;
                $obj->landmark = @$request->landmark;
                $obj->country = $request->country;
                $obj->state = @$request->state;
                $obj->zip_code = @$request->zip_code;
                $obj->latitude = $request->latitude;
                $obj->longitude = $request->longitude;
                $obj->location = $location;


                if(session('is_premium') != ''){
                    $obj->is_premium = session('is_premium');
                    \Session::forget('is_premium');
                }

                // step3
                if($request->has('special_amenties')){
                $special_amenties = [];
                foreach($request->get('special_amenties') as $key => $value) {
                    $special_amenties[] = $value;
                }
                
                $obj->special_amenties = $special_amenties;
                }
                if($request->has('food_amenties')){
                $food_amenties = [];
                foreach($request->get('food_amenties') as $key => $value) {
                    $food_amenties[] = $value;
                }
                $obj->food_amenties = $food_amenties;
            }
            if($request->has('facilities_amenties')){
                $facilities_amenties = [];
                foreach($request->get('facilities_amenties') as $key => $value) {
                    $facilities_amenties[] = $value;
                }
                $obj->facilities_amenties = $facilities_amenties;
            }
            if($request->has('equipments_amenties')){
                $equipments_amenties = [];
                foreach($request->get('equipments_amenties') as $key => $value) {
                    $equipments_amenties[] = $value;
                }
                $obj->equipments_amenties = $equipments_amenties;
            }
            if($request->has('seating_amenties')){
                $seating_amenties = [];
                foreach($request->get('seating_amenties') as $key => $value) {
                    $seating_amenties[] = $value;
                }
                $obj->seating_amenties = $seating_amenties;
            }
            if($request->has('accessibility_amenties')){
                $accessibility_amenties = [];
                foreach($request->get('accessibility_amenties') as $key => $value) {
                    $accessibility_amenties[] = $value;
                }
                $obj->accessibility_amenties = $accessibility_amenties;
            }
                // step4
                $obj->availability = [
                    'sunday'=>$request->input('sunday'),
                    'monday'=>$request->input('monday'),
                    'tuesday'=>$request->input('tuesday'),
                    'wednesday'=>$request->input('wednesday'),
                    'thursday'=>$request->input('thursday'),
                    'friday'=>$request->input('friday'),
                    'saturday'=>$request->input('saturday'),
                ];
                $obj->timings =[
                    'sunday_start_time'=>($request->input('sunday')=='open') ? $request->input('sunday_start_time'): '',
                    'sunday_end_time'=>($request->input('sunday')=='open') ? $request->input('sunday_end_time'): '',

                    'monday_start_time'=>($request->input('monday')=='open') ? $request->input('monday_start_time'): '',
                    'monday_end_time'=>($request->input('monday')=='open') ? $request->input('monday_end_time'): '',

                    'tuesday_start_time'=>($request->input('tuesday')=='open') ? $request->input('tuesday_start_time'): '',
                    'tuesday_end_time'=>($request->input('tuesday')=='open') ? $request->input('tuesday_end_time'): '',

                    'wednesday_start_time'=>($request->input('wednesday')=='open') ? $request->input('wednesday_start_time'): '',
                    'wednesday_end_time'=>($request->input('wednesday')=='open') ? $request->input('wednesday_end_time'): '',

                    'thursday_start_time'=>($request->input('thursday')=='open') ? $request->input('thursday_start_time'): '',
                    'thursday_end_time'=>($request->input('thursday')=='open') ? $request->input('thursday_end_time'): '',

                    'friday_start_time'=>($request->input('friday')=='open') ? $request->input('friday_start_time'): '',
                    'friday_end_time'=>($request->input('friday')=='open') ? $request->input('friday_end_time'): '',

                    'saturday_start_time'=>($request->input('saturday')=='open') ? $request->input('saturday_start_time'): '',
                    'saturday_end_time'=>($request->input('saturday')=='open') ? $request->input('saturday_end_time'): '',
                ];

                // step5
                $obj->email = $request->email;
                $obj->contact_number = $request->phone_code.$request->contact_number;
                $obj->website_url = $request->website_url;
                $obj->facebook_url = 'https://facebook.com/'.$request->facebook_url;
                $obj->twitter_url = 'https://twitter.com/'.$request->twitter_url;
                $obj->instagram_url = 'https://instagram.com/'.$request->instagram_url;

                // step6
                $files = [];
                if($request->hasfile('files'))
                {
                    foreach($request->file('files') as $file)
                    {
                        $name=Auth::user()->_id.'-'.md5($file->getClientOriginalName().time()).".".$file->getClientOriginalExtension();
                        $file->move(public_path('/uploads/coworkspce'), $name);
                        $files[] = $name;
                    }
                }
                $obj->files = $files;

                // step7
                $obj->open_workspace_available = ($request->open_workspace_available==1) ? 1: 0;
                if($request->filled('open_workspace_available') && $request->input('open_workspace_available')==1){
                    $obj->open_space_daily_price = (int)$request->open_space_daily_price;
                    $obj->open_space_weekly_price = (int)$request->open_space_weekly_price;
                    $obj->open_space_monthly_price = (int)$request->open_space_monthly_price;
                    $obj->open_space_daily_currency = $request->currency_type;
                    $obj->open_space_weekly_currency = $request->currency_type;
                    $obj->open_space_monthly_currency = $request->currency_type;
                    $obj->open_space_seats = $request->open_space_seats;
                }
                

                $obj->dedicated_desk_available = ($request->dedicated_desk_available==1) ? 1: 0;
                if($request->filled('dedicated_desk_available') && $request->input('dedicated_desk_available')==1){
                    $obj->dedicated_space_daily_price=(int)$request->dedicated_space_daily_price;
                    $obj->dedicated_space_weekly_price=(int)$request->dedicated_space_weekly_price;
                    $obj->dedicated_space_monthly_price=(int)$request->dedicated_space_monthly_price;
                    $obj->dedicated_space_daily_currency=$request->currency_type;
                    $obj->dedicated_space_weekly_currency=$request->currency_type;
                    $obj->dedicated_space_monthly_currency=$request->currency_type;
                    $obj->dedicated_space_seats = $request->dedicated_space_seats;
                }

                $obj->private_office_available = ($request->private_office_available==1) ? 1: 0;
                if($request->filled('private_office_available') && $request->input('dedicated_desk_available')==1){
                    $obj->private_space_daily_price=(int)$request->private_space_daily_price;
                    $obj->private_space_weekly_price=(int)$request->private_space_weekly_price;
                    $obj->private_space_monthly_price=(int)$request->private_space_monthly_price;
                    $obj->private_space_daily_currency=$request->currency_type;
                    $obj->private_space_weekly_currency=$request->currency_type;
                    $obj->private_space_monthly_currency=$request->currency_type;
                    $obj->white_board_available=($request->white_board_available==1) ? 1 : 0;
                    $obj->seating_capacity=$request->seating_capacity;
                    $obj->no_of_private_office = $request->no_of_private_office;
                }
                
                $current=date('Y-m-d');
                $checkPlan = Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','cospace')->where('expiry_date','>=',$current)->latest()->first();
                if(@$checkPlan){
                    $obj->plan_id = @$checkPlan->plan_id;
                    if(@$checkPlan->listed < @$checkPlan->count){
                        $obj->show = (int)1;
                    }else{
                        $obj->show = (int)0;
                    }
                    $update=\DB::table('transaction')
                    ->where('_id',@$checkPlan->_id)
                    ->update(['listed'=>@$checkPlan->listed+1]);
                }

                $obj->created_at = Carbon::now();
                $obj->updated_at = Carbon::now();
                $obj->is_approved = (int)0;
                if($obj->save()){
                    Mail::to('info@thegulfroad.com')->send(new PropertyRegistrationAdmin($obj));
                    Mail::to(Auth::user()->email)->send(new PropertyRegistrationUser($obj));
                    return response()->json(['status'=>true,'message'=>'Property listed successfully. Once Your property approved then property will be visible on listing page','url'=>url(app()->getLocale().'/mycoworkspaces'.'/'.strtolower($loc))],200);
                }
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                return response()->json(['status'=>true],200);

                break;

            default:
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;
        }
    }

    /**
    * update coworkspace property
    **/
    public function updateCoworkspaceProperty(Request $request){
        $step = $request->segment(4) ? $request->segment(4): 0;

        $object = CoworkspaceProperty::where('_id', $request->property_id)->where('user_id', Auth::user()->_id)->first();
        if(!$object){
            return response()->json(['status'=>false,'message'=>'Property not found, Please try again later.'],200);
        }


        switch ($step) {
            case '1':
                $request->validate([
                    'title'=>'bail|required|min:10|max:500',
                    'description'=>'bail|required|min:10|max:2000',
                ]);

                $object->title = $request->title;
                $object->description = $request->description;
                $object->updated_at = Carbon::now();

                if($object->save()){
                    return response()->json(['status'=>true,'message'=>'Description details updated successfully.'],200);
                }
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;

            case '2':
                $request->validate([
                    'address'=>'bail|required|max:500',
                    'location_name'=>'bail|required|max:100',
                    'unit_number'=>'bail|required|max:100',
                    'address_line_1'=>'bail|nullable|max:100',
                    'address_line_2'=>'bail|nullable|max:100',
                    'landmark'=>'bail|nullable|max:100',
                    'city'=>'bail|required|max:100',
                    'country'=>'bail|required|max:100',
                    'state'=>'bail|required|max:100',
                    'zip_code'=>'bail|required|max:50',
                ]);


                 $location = [(float) $request->longitude,(float) $request->latitude];
                $object->address = $request->address;
                $object->location_name = $request->location_name;
                $object->unit_number = $request->unit_number;
                $object->address_line_1 = $request->address_line_1;
                $object->address_line_2 = $request->address_line_2;
                $object->landmark = $request->landmark;
                $object->city = $request->city;
                $object->country = $request->country;
                $object->state = $request->state;
                $object->zip_code = $request->zip_code;  
                $object->latitude = $request->latitude;
                $object->longitude = $request->longitude;
                $object->location = $location;              
                $object->updated_at = Carbon::now();

                
                if($object->save()){
                    return response()->json(['status'=>true,'message'=>'Location details updated successfully.'],200);
                }
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;

            case '3':
                
                if(!$request->has('food_amenties'))
                {
                    return response()->json(['status'=>false,'message'=>'Please select some food amenties.'],200);
                }
                elseif (!$request->has('facilities_amenties')) {
                    return response()->json(['status'=>false,'message'=>'Please select some facilities amenties.'],200);
                }
                elseif (!$request->has('equipments_amenties')) {
                    return response()->json(['status'=>false,'message'=>'Please select some equipments amenties.'],200);
                }
                elseif (!$request->has('seating_amenties')) {
                    return response()->json(['status'=>false,'message'=>'Please select some seating amenties.'],200);
                }
                elseif (!$request->has('accessibility_amenties')) {
                    return response()->json(['status'=>false,'message'=>'Please select some accessibility amenties.'],200);
                }
                else{
                    
                }

                $food_amenties = [];
                foreach($request->get('food_amenties') as $key => $value) {
                    $food_amenties[] = $value;
                }
                $object->food_amenties = $food_amenties;

                $facilities_amenties = [];
                foreach($request->get('facilities_amenties') as $key => $value) {
                    $facilities_amenties[] = $value;
                }
                $object->facilities_amenties = $facilities_amenties;

                $equipments_amenties = [];
                foreach($request->get('equipments_amenties') as $key => $value) {
                    $equipments_amenties[] = $value;
                }
                $object->equipments_amenties = $equipments_amenties;

                $seating_amenties = [];
                foreach($request->get('seating_amenties') as $key => $value) {
                    $seating_amenties[] = $value;
                }
                $object->seating_amenties = $seating_amenties;

                $accessibility_amenties = [];
                foreach($request->get('accessibility_amenties') as $key => $value) {
                    $accessibility_amenties[] = $value;
                }
                $object->accessibility_amenties = $accessibility_amenties;

                $object->updated_at = Carbon::now();

                if($object->save()){
                    return response()->json(['status'=>true,'message'=>'Amenities details updated successfully.'],200);
                }
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;

            case '4':
                $errors = [];
                $errors['monday'] = 'bail|required|in:closed,open';
                $errors['tuesday'] = 'bail|required|in:closed,open';
                $errors['wednesday'] = 'bail|required|in:closed,open';
                $errors['thursday'] = 'bail|required|in:closed,open';
                $errors['friday'] = 'bail|required|in:closed,open';
                $errors['saturday'] = 'bail|required|in:closed,open';
                $errors['sunday'] = 'bail|required|in:closed,open';
                // monday
                    
                if($request->input('monday')=='open'){
                    $errors['monday_start_time'] = 'bail|required|date_format:H:i';
                    $errors['monday_end_time'] = 'bail|required|date_format:H:i|after:monday_start_time';
                }

                if($request->input('tuesday')=='open'){
                    $errors['tuesday_start_time'] = 'bail|nullable|required_if:tuesday,open|date_format:H:i';
                    $errors['tuesday_end_time'] = 'bail|nullable|required_if:tuesday,open|date_format:H:i|after:tuesday_start_time';
                }
                if($request->input('wednesday')=='open'){
                    $errors['wednesday_start_time'] = 'bail|nullable|required_if:wednesday,open|date_format:H:i';
                    $errors['wednesday_end_time'] = 'bail|nullable|required_if:wednesday,open|date_format:H:i|after:wednesday_start_time';
                }
                if($request->input('thursday')=='open'){
                    $errors['thursday_start_time'] = 'bail|nullable|required_if:thursday,open|date_format:H:i';
                    $errors['thursday_end_time'] = 'bail|nullable|required_if:thursday,open|date_format:H:i|after:thursday_start_time';
                }
                if($request->input('friday')=='open'){
                    $errors['friday_start_time'] = 'bail|nullable|required_if:friday,open|date_format:H:i';
                    $errors['friday_end_time'] = 'bail|nullable|required_if:friday,open|date_format:H:i|after:friday_start_time';
                }
                if($request->input('saturday')=='open'){
                    $errors['saturday_start_time'] = 'bail|nullable|required_if:saturday,open|date_format:H:i';
                    $errors['saturday_end_time'] = 'bail|nullable|required_if:saturday,open|date_format:H:i|after:saturday_start_time';
                }
                if($request->input('sunday')=='open'){
                    $errors['sunday_start_time'] = 'bail|nullable|required_if:sunday,open|date_format:H:i';
                    $errors['sunday_end_time'] = 'bail|nullable|required_if:sunday,open|date_format:H:i|after:sunday_start_time';
                }
                $request->validate($errors,
                [                    
                    'monday_start_time.required_if'=>'The start time is required.',
                    
                    'monday_end_time.required_if'=>'The end time is required.',
                    'monday_end_time.after'=>'The end time must be greater than start time.',
                    

                    'tuesday_start_time.required_if'=>'The start time is required.',
                    
                    'tuesday_end_time.required_if'=>'The end time is required.',
                    'tuesday_end_time.after'=>'The end time must be greater than start time.',
                    

                    'wednesday_start_time.required_if'=>'The start time is required.',
                    'wednesday_end_time.required_if'=>'The end time is required.',
                    'wednesday_end_time.after'=>'The end time must be greater than start time.',

                    'thursday_start_time.required_if'=>'The start time is required.',
                    'thursday_end_time.required_if'=>'The end time is required.',
                    'thursday_end_time.after'=>'The end time must be greater than start time.',

                    'friday_start_time.required_if'=>'The start time is required.',
                    'friday_end_time.required_if'=>'The end time is required.',
                    'friday_end_time.after'=>'The end time must be greater than start time.',

                    'saturday_start_time.required_if'=>'The start time is required.',
                    'saturday_end_time.required_if'=>'The end time is required.',
                    'saturday_end_time.after'=>'The end time must be greater than start time.',

                    'sunday_start_time.required_if'=>'The start time is required.',
                    'sunday_end_time.required_if'=>'The end time is required.',
                    'sunday_end_time.after'=>'The end time must be greater than start time.',

                    'monday_start_time.date_format'=>'The start time format is not correct.',
                    'monday_end_time.date_format'=>'The end time format is not correct.',

                    'tuesday_start_time.date_format'=>'The start time format is not correct.',
                    'tuesday_end_time.date_format'=>'The start time format is not correct.',

                    'wednesday_start_time.date_format'=>'The start time format is not correct.',
                    'wednesday_end_time.date_format'=>'The start time format is not correct.',

                    'thursday_start_time.date_format'=>'The start time format is not correct.',
                    'thursday_end_time.date_format'=>'The start time format is not correct.',

                    'friday_start_time.date_format'=>'The start time format is not correct.',
                    'friday_end_time.date_format'=>'The start time format is not correct.',

                    'saturday_start_time.date_format'=>'The start time format is not correct.',
                    'saturday_end_time.date_format'=>'The start time format is not correct.',

                    'sunday_start_time.date_format'=>'The start time format is not correct.',
                    'sunday_end_time.date_format'=>'The start time format is not correct.',
                ]);

                $object->availability = [
                    'sunday'=>$request->input('sunday'),
                    'monday'=>$request->input('monday'),
                    'tuesday'=>$request->input('tuesday'),
                    'wednesday'=>$request->input('wednesday'),
                    'thursday'=>$request->input('thursday'),
                    'friday'=>$request->input('friday'),
                    'saturday'=>$request->input('saturday'),
                ];
                $object->timings =[
                    'sunday_start_time'=>($request->input('sunday')=='open') ? $request->input('sunday_start_time'): '',
                    'sunday_end_time'=>($request->input('sunday')=='open') ? $request->input('sunday_end_time'): '',

                    'monday_start_time'=>($request->input('monday')=='open') ? $request->input('monday_start_time'): '',
                    'monday_end_time'=>($request->input('monday')=='open') ? $request->input('monday_end_time'): '',

                    'tuesday_start_time'=>($request->input('tuesday')=='open') ? $request->input('tuesday_start_time'): '',
                    'tuesday_end_time'=>($request->input('tuesday')=='open') ? $request->input('tuesday_end_time'): '',

                    'wednesday_start_time'=>($request->input('wednesday')=='open') ? $request->input('wednesday_start_time'): '',
                    'wednesday_end_time'=>($request->input('wednesday')=='open') ? $request->input('wednesday_end_time'): '',

                    'thursday_start_time'=>($request->input('thursday')=='open') ? $request->input('thursday_start_time'): '',
                    'thursday_end_time'=>($request->input('thursday')=='open') ? $request->input('thursday_end_time'): '',

                    'friday_start_time'=>($request->input('friday')=='open') ? $request->input('friday_start_time'): '',
                    'friday_end_time'=>($request->input('friday')=='open') ? $request->input('friday_end_time'): '',

                    'saturday_start_time'=>($request->input('saturday')=='open') ? $request->input('saturday_start_time'): '',
                    'saturday_end_time'=>($request->input('saturday')=='open') ? $request->input('saturday_end_time'): '',
                ];
                $object->updated_at = Carbon::now();

                if($object->save()){
                    return response()->json(['status'=>true,'message'=>'Operating hours & days updated successfully.'],200);
                }
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;

            case '5':
                $request->validate([
                    'email'=>'bail|required|email|max:50',
                    'contact_number'=>'bail|required|numeric|digits_between:5,15',
                    'website_url'=>'bail|required|max:200',
                    'facebook_url'=>'bail|required|max:200',
                    'twitter_url'=>'bail|required|max:200',
                    'instagram_url'=>'bail|required|max:200',
                ]);
                
                $object->email = $request->email;
                $object->contact_number = $request->contact_number;
                $object->website_url = $request->website_url;
                $object->facebook_url = $request->facebook_url;
                $object->twitter_url = $request->twitter_url;
                $object->instagram_url = $request->instagram_url;
                $object->updated_at = Carbon::now();
                if($object->save()){
                    return response()->json(['status'=>true,'message'=>'Contact details updated successfully.'],200);
                }
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;

            case '6':
                $request->validate([
                    'files.*'=>'bail|required|mimes:webp,jpeg,jpg,png|max:5000',
                ],[
                    'files.*.required'=>'The file is required.',
                    'files.*.mimes'=>'The file must be type of jpeg,jpg,png',
                ]);
                
                $files = [];
                $exists_files = $object->files;

                if($request->exist_file){
                    $files  = $request->exist_file;
                }
                
                if($request->hasfile('files')){
                    foreach($request->file('files') as $file)
                    {
                        $name=Auth::user()->_id.'-'.md5($file->getClientOriginalName().time()).".".$file->getClientOriginalExtension();
                        $file->move(public_path('/uploads/coworkspce'), $name);
                        $files[] = $name;
                    }
                }
                $object->files = $files;
                $object->updated_at = Carbon::now();
                
                if($object->save()){
                    return response()->json(['status'=>true,'message'=>'Photos updated successfully.'],200);
                }
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;

            case '7':
                $errors =[
                    'open_space_daily_price'=>'bail|required|numeric|between:1,250000',
                    'open_space_weekly_price'=>'bail|required|numeric|between:1,250000',
                    'open_space_monthly_price'=>'bail|required|numeric|between:1,250000',
                    'open_space_daily_currency'=>'bail|required|max:50',
                    'open_space_weekly_currency'=>'bail|required|max:50',
                    'open_space_monthly_currency'=>'bail|required|max:50',
                    'dedicated_desk_available'=>'bail|required',
                    'open_space_seats'=>'bail|required',
                ];

                if($request->input('dedicated_desk_available')==1){
                    $errors['dedicated_space_daily_price'] = 'bail|required|numeric|between:1,250000';
                    $errors['dedicated_space_weekly_price'] = 'bail|required|numeric|between:1,250000';
                    $errors['dedicated_space_monthly_price'] = 'bail|required|numeric|between:1,250000';
                    $errors['dedicated_space_daily_currency'] = 'bail|required|max:50';
                    $errors['dedicated_space_weekly_currency'] = 'bail|required|max:50';
                    $errors['dedicated_space_monthly_currency'] = 'bail|required|max:50';
                    $errors['dedicated_space_seats'] = 'bail|required';
                }

                if($request->input('private_office_available')==1){
                    $errors['private_space_daily_price'] = 'bail|required|numeric|between:1,250000';
                    $errors['private_space_weekly_price'] = 'bail|required|numeric|between:1,250000';
                    $errors['private_space_monthly_price'] = 'bail|required|numeric|between:1,250000';
                    $errors['private_space_daily_currency'] = 'bail|required|max:50';
                    $errors['private_space_weekly_currency'] = 'bail|required|max:50';
                    $errors['private_space_monthly_currency'] = 'bail|required|max:50';
                    $errors['seating_capacity'] = 'bail|required';
                    $errors['white_board_available'] = 'bail|required';
                    $errors['no_of_private_office'] = 'bail|required';
                }

                $request->validate($errors);

                $object->open_space_daily_price = (int)$request->open_space_daily_price;
                $object->open_space_weekly_price = (int)$request->open_space_weekly_price;
                $object->open_space_monthly_price = (int)$request->open_space_monthly_price;
                $object->open_space_daily_currency = $request->open_space_daily_currency;
                $object->open_space_weekly_currency = $request->open_space_weekly_currency;
                $object->open_space_monthly_currency = $request->open_space_monthly_currency;
                $object->open_space_seats = (int)$request->open_space_seats;

                $object->dedicated_desk_available = ($request->dedicated_desk_available==1) ? 1 : 0;

                if($request->filled('dedicated_desk_available') && $request->input('dedicated_desk_available')==1){
                    $object->dedicated_space_daily_price=(int)$request->dedicated_space_daily_price;
                    $object->dedicated_space_weekly_price=(int)$request->dedicated_space_weekly_price;
                    $object->dedicated_space_monthly_price=(int)$request->dedicated_space_monthly_price;
                    $object->dedicated_space_daily_currency=$request->dedicated_space_daily_currency;
                    $object->dedicated_space_weekly_currency=$request->dedicated_space_weekly_currency;
                    $object->dedicated_space_monthly_currency=$request->dedicated_space_monthly_currency;
                    $object->dedicated_space_seats = $request->dedicated_space_seats;
                }

                $object->private_office_available = ($request->private_office_available==1) ? 1: 0;

                if($request->filled('private_office_available') && $request->input('dedicated_desk_available')==1){
                    $object->private_space_daily_price=(int)$request->private_space_daily_price;
                    $object->private_space_weekly_price=(int)$request->private_space_weekly_price;
                    $object->private_space_monthly_price=(int)$request->private_space_monthly_price;
                    $object->private_space_daily_currency=$request->private_space_daily_currency;
                    $object->private_space_weekly_currency=$request->private_space_weekly_currency;
                    $object->private_space_monthly_currency=$request->private_space_monthly_currency;
                    $object->white_board_available=($request->white_board_available==1) ? 1: 0;
                    $object->seating_capacity=$request->seating_capacity;
                    $object->no_of_private_office = $request->no_of_private_office;
                }
                $object->updated_at = Carbon::now();
                
                if($object->save()){
                    return response()->json(['status'=>true,'message'=>'Pricing details updated successfully.'],200);
                }
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;

            default:
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;
        }
    }

    /**
    * delete coworkspace property
    **/
    public function deleteCoworkspaceProperty(Request $request){
        $id = $request->id;
        $data = CoworkspaceProperty::where('user_id', Auth::user()->_id)->where('_id', $id)->first();
        if($data){
            if($data->delete()){
                return response()->json(['status'=>true,'message'=>'Property deleted successfully.'],200);
            }
            return response()->json(['status'=>false,'message'=>'Unable to delete property, Please try again later.'],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
        }
    }

    /**
    * validate swap and add property
    */
    public function validateSaveSwapProperty(Request $request, $step){
        $step = $request->segment(4) ? $request->segment(4): 0;
        $loc = $request->country;
        
        switch ($step) {
            case '1':
                $request->validate([
                    'title'=>'bail|required|max:500',
                    'country'=>'bail|required|max:100',
                    'city'=>'bail|required|max:100',
                    'location_name'=>'bail|required|max:200',
                    'house_type'=>'bail|required|max:200',
                    'location_accessible'=>'bail|required|max:200',
                ]);
                return response()->json(['status'=>true],200);
                break;

            case '2':
                $request->validate([
                    'surface_area'=>'bail|required|numeric|between:10,250000',
                    'surface_area_unit'=>'bail|required',
                    'no_of_bedrooms'=>'bail|required|numeric|min:0|max:15',
                    'no_of_bathrooms'=>'bail|required|numeric|min:0|max:15',
                    'no_of_children_beds'=>'bail|required|numeric|min:0|max:15',
                    'no_of_single_beds'=>'bail|required|numeric|min:0|max:15',
                    'no_of_double_beds'=>'bail|required|numeric|min:0|max:15',
                    'no_of_extra_beds'=>'bail|required|numeric|min:0|max:15',
                ]);
                return response()->json(['status'=>true],200);
                break;

            case '3':
                if($request->has('amenties'))
                {
                    return response()->json(['status'=>true],200);
                }else{
                    return response()->json(['status'=>false,'message'=>'Please select some amenties.'],200);
                }
                break;

            case '4':
                $request->validate([
                    'neighbourhood_description'=>'bail|required|string|max:5000',
                    'property_description'=>'bail|required|string|max:5000',
                    'availability'=>'bail|required|string',
                ],
                [
                    'availability.required'=>'Please select availability of your house.'
                ]);
                return response()->json(['status'=>true],200);
                break;

            case '5':
                $request->validate([
                    'files.*'=>'bail|required|mimes:webp,jpeg,jpg,png|max:5000',
                ],[
                    'files.*.required'=>'The file is required.',
                    'files.*.mimes'=>'The file must be type of webp,jpeg,jpg,png.',
                ]);
                return response()->json(['status'=>true],200);
                break;

            case '6':
                $errors = [
                    // 'title'=>'bail|required|min:10|max:500',
                    'country'=>'bail|required|max:100',
                    'location_name'=>'bail|required|max:200',
                    'house_type'=>'bail|required|max:200',
                    'location_accessible'=>'bail|required|max:200',
                    'surface_area'=>'bail|required|numeric|between:10,250000',
                    'surface_area_unit'=>'bail|required',
                    'no_of_bedrooms'=>'bail|required|numeric|min:0|max:15',
                    'no_of_bathrooms'=>'bail|required|numeric|min:0|max:15',
                    'total_accommodation'=>'bail|required|numeric|min:0|max:50',
                    'no_of_children_beds'=>'bail|required|numeric|min:0|max:15',
                    'no_of_single_beds'=>'bail|required|numeric|min:0|max:15',
                    'no_of_double_beds'=>'bail|required|numeric|min:0|max:15',
                    'no_of_extra_beds'=>'bail|required|numeric|min:0|max:15',
                    // 'neighbourhood_description'=>'bail|required|string|min:10|max:5000',
                    // 'property_description'=>'bail|required|string|min:10|max:5000',
                ];
                
                // step5
                if($request->has('files')){
                    $errors['files.*'] = 'bail|required|mimes:webp,jpeg,jpg,png|max:5000';
                }

                $request->validate($errors,
                    [
                        'files.*.required'=>'The file is required.',
                        'files.*.mimes'=>'The file must be type of webp,jpeg,jpg,png.',
                    ]);

                $location = Common::getLatLong($request->location_name);
                $location = [(float) $request->longitude,(float) $request->latitude];
                $transaction_id = Session::get('transaction_id');

                $obj = new SwapProperty();


                $obj->created_at = Carbon::now();
                $obj->updated_at = Carbon::now();
                
                $obj->user_id = Auth::user()->_id;
                $obj->transaction_id = $transaction_id;

                $obj->title = $request->title;
                $obj->country = $request->country;
                $obj->city = $request->city;
                $obj->latitude = $request->latitude;
                $obj->longitude = $request->longitude;
                $obj->location = $location;
                $obj->location_name = $request->location_name;
                $obj->house_type = $request->house_type;
                $obj->owner_locations = $request->owner_locations??'';
                $obj->location_accessible = $request->location_accessible;
                $obj->surface_area = (int)$request->surface_area;
                $obj->surface_area_unit = $request->surface_area_unit;
                $obj->no_of_bedrooms = $request->no_of_bedrooms;
                $obj->no_of_bathrooms = $request->no_of_bathrooms;
                $obj->no_of_children_beds = $request->no_of_children_beds;
                $obj->no_of_single_beds = $request->no_of_single_beds;
                $obj->no_of_double_beds = $request->no_of_double_beds;
                $obj->no_of_extra_beds = $request->no_of_extra_beds;
                $obj->neighbourhood_description = $request->neighbourhood_description;
                $obj->property_description = $request->property_description;
                $obj->total_accommodation = (int)$request->total_accommodation;



                if(session('is_premium') != ''){
                    $obj->is_premium = session('is_premium');
                    \Session::forget('is_premium');
                }

                $obj->created_at = Carbon::now();
                $obj->updated_at = Carbon::now();

                $amenties = [];
                if($request->has('amenties'))
                {
                    
                    foreach($request->get('amenties') as $key => $value) {
                        $amenity=Amenity::where('_id',$value)->first();
                        $amenties[$key]['id'] = $value;
                        $amenties[$key]['name'] = $amenity->name;
                        $amenties[$key]['logo'] = $amenity->logo;
                    } 
                }
                $obj->amenties = $amenties;

                $availability = [];
                if($request->has('availability'))
                {
                    $availability = explode(',', $request->input('availability'));
                }
                $obj->availability = $availability;

                // step6
                $files = [];
                if($request->hasfile('files'))
                {
                    foreach($request->file('files') as $file)
                    {
                        $name=Auth::user()->_id.'-'.md5($file->getClientOriginalName().time()).".".$file->getClientOriginalExtension();
                        $file->move(public_path('/uploads/swap'), $name);
                        $files[] = $name;
                    }
                }
                $obj->files = $files;

                $current=date('Y-m-d');
                
                $checkPlan = Transaction::where('user_id',\Auth::user()->id)->where('purchase_type','swap')->where('expiry_date','>=',$current)->latest()->first();
                if(@$checkPlan){
                    $obj->plan_id = @$checkPlan->plan_id;
                    if(@$checkPlan->listed < @$checkPlan->count){
                        $obj->show = (int)1;
                    }else{
                        $obj->show = (int)0;
                    }
                    $update=\DB::table('transaction')
                    ->where('_id',@$checkPlan->_id)
                    ->update(['listed'=>@$checkPlan->listed+1]);
                }


                $obj->created_at = Carbon::now();
                $obj->updated_at = Carbon::now();
                $obj->is_approved = (int)0;

                if($obj->save()){
                    Mail::to('info@thegulfroad.com')->send(new PropertyRegistrationAdmin($obj));
                    Mail::to(Auth::user()->email)->send(new PropertyRegistrationUser($obj));
                    return response()->json(['status'=>true,'message'=>'Property listed successfully. Once Your property approved then property will be visible on listing page','url'=>url(app()->getLocale().'/myswap'.'/'.strtolower($loc))],200);
                }
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;

            default:
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;
        }
    }

    /**
    * update swap property
    **/
    public function updateSwapProperty(Request $request){
        $step = $request->segment(4) ? $request->segment(4): 0;

        $object = SwapProperty::where('_id', $request->property_id)->where('user_id', Auth::user()->_id)->first();
        if(!$object){
            return response()->json(['status'=>false,'message'=>'Property not found, Please try again later.'],200);
        }
        switch ($step) {
            case '1':
                $request->validate([
                    'title'=>'bail|required|min:10|max:500',
                    'country'=>'bail|required|max:100',
                    'location_name'=>'bail|required|max:200',
                    'house_type'=>'bail|required|max:200',
                    'location_accessible'=>'bail|required|max:200',
                ]);

                $object->title = $request->title;
                $object->country = $request->country;
                $object->location_name = $request->location_name;
                $object->house_type = $request->house_type;
                $object->latitude = $request->latitude;
                $object->longitude = $request->longitude;
                $object->location_accessible = $request->location_accessible;
                $object->location =   [(float) $request->longitude,(float)$request->latitude];;
                $object->updated_at = Carbon::now();

                if($object->save()){
                    return response()->json(['status'=>true,'message'=>'Details updated successfully.'],200);
                }
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;

            case '2':
                $request->validate([
                    'surface_area'=>'bail|required|numeric|between:10,250000',
                    'surface_area_unit'=>'bail|required',
                    'no_of_bedrooms'=>'bail|required|numeric|min:0|max:15',
                    'no_of_bathrooms'=>'bail|required|numeric|min:0|max:15',
                    'no_of_children_beds'=>'bail|required|numeric|min:0|max:15',
                    'no_of_single_beds'=>'bail|required|numeric|min:0|max:15',
                    'no_of_double_beds'=>'bail|required|numeric|min:0|max:15',
                    'no_of_extra_beds'=>'bail|required|numeric|min:0|max:15',
                    'total_accommodation'=>'bail|required|numeric|min:0|max:100',
                ]);

                $object->surface_area = (int)$request->surface_area;
                $object->surface_area_unit = $request->surface_area_unit;
                $object->no_of_bedrooms = $request->no_of_bedrooms;
                $object->no_of_bathrooms = $request->no_of_bathrooms;
                $object->no_of_children_beds = $request->no_of_children_beds;
                $object->no_of_single_beds = $request->no_of_single_beds;
                $object->no_of_double_beds = $request->no_of_double_beds;
                $object->no_of_extra_beds = $request->no_of_extra_beds;
                
                $object->total_accommodation = (int)$request->total_accommodation;                
                $object->updated_at = Carbon::now();
                if($object->save()){
                    return response()->json(['status'=>true,'message'=>'Size & Space details updated successfully.'],200);
                }
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;

            case '3':
                
               /* $amenties = [];
                if($request->has('amenties'))
                {
                    foreach($request->get('amenties') as $key => $value) {
                        $amenties[] = $value;
                    } 
                }
                $object->amenties = $amenties;*/

                $amenties = [];
                if($request->has('amenties'))
                {
                    
                    foreach($request->get('amenties') as $key => $value) {
                        $amenity=Amenity::where('_id',$value)->first();
                        $amenties[$key]['id'] = $value;
                        $amenties[$key]['name'] = $amenity->name;
                        $amenties[$key]['logo'] = $amenity->logo;
                    } 
                }
                $object->amenties = $amenties;

                $object->updated_at = Carbon::now();

                if($object->save()){
                    return response()->json(['status'=>true,'message'=>'Amenities details updated successfully.'],200);
                }
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;

            case '4':
                $request->validate([
                    'neighbourhood_description'=>'bail|required|string|min:10|max:5000',
                    'property_description'=>'bail|required|string|min:10|max:5000',
                    'availability'=>'bail|required|string',
                ]);
                $object->neighbourhood_description = $request->neighbourhood_description;
                $object->property_description = $request->property_description;

                $availability = [];
                if($request->has('availability'))
                {
                    $availability = explode(',', $request->input('availability'));
                }
                $object->availability = $availability;
                $object->updated_at = Carbon::now();

                if($object->save()){
                    return response()->json(['status'=>true,'message'=>'Descriptions updated successfully.'],200);
                }
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;

            case '5':
                $request->validate([
                    'files.*'=>'bail|required|mimes:webp,jpeg,jpg,png|max:5000',
                ],[
                    'files.*.required'=>'The file is required.',
                    'files.*.mimes'=>'The file must be type of webp,jpeg,jpg,png.',
                ]);
                
                $files = [];
                $exists_files = $object->files;

                if($request->exist_file){
                    $files  = $request->exist_file;
                }
                
                if($request->hasfile('files')){
                    foreach($request->file('files') as $file)
                    {
                        $name=Auth::user()->_id.'-'.md5($file->getClientOriginalName().time()).".".$file->getClientOriginalExtension();
                        $file->move(public_path('/uploads/swap'), $name);
                        $files[] = $name;
                    }
                }
                $object->files = $files;

                $object->updated_at = Carbon::now();
                if($object->save()){
                    return response()->json(['status'=>true,'message'=>'Photos updated successfully.'],200);
                }
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;

            default:
                return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
                break;
        }
    }

    /**
    * delete swap property
    **/
    public function deleteSwapProperty(Request $request){
        $id = $request->id;
        
        $data = SwapProperty::where('user_id', Auth::user()->_id)->where('_id', $id)->first();
        if($data){
            if($data->delete()){
                return response()->json(['status'=>true,'message'=>'Property deleted successfully.'],200);
            }
            return response()->json(['status'=>false,'message'=>'Unable to delete property, Please try again later.'],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Something went wrong, Please try again later.'],200);
        }
    }

    public function compare(Request $request){
        $swap_id = request()->segment(6);
       $data = SwapProperty::where('_id', $swap_id)->first();
        $totaluser=Review::where('is_approved',1)->where('review_type',"3")->where('type_id',$swap_id)->count();
          $totalrating=$totaluser*5;
          if($totalrating == 0){
            $totalrating=1;
          }
          $ratinggiven6=Review::where('is_approved',1)->where('review_type',"3")->where('type_id',$swap_id)->sum('average');
          $avgtotal=number_format($ratinggiven6/$totalrating*5);
       $all_swaps = SwapProperty::where('_id','!=', $swap_id)->get();
       $compare = SwapProperty::where('_id',@$request->otherswap)->first();
       $totaluser1=Review::where('is_approved',1)->where('review_type',"3")->where('type_id',@$request->otherswap)->count();
          $totalrating1=$totaluser1*5;
          if($totalrating1 == 0){
            $totalrating1=1;
          }
          $ratinggiven61=Review::where('is_approved',1)->where('review_type',"3")->where('type_id',@$request->otherswap)->sum('average');
          $avgtotal1=number_format($ratinggiven61/$totalrating1*5);
        return view('frontend.property-details.swap.compare',compact('data','compare','all_swaps','avgtotal1','avgtotal'));
    }

    public function transaction(Request $request){
        $user_id=\Auth::user()->id;
        $plan_id=(string)$request->id;
        $number=(string)$request->number;
        $amount=$request->amount;
       


             
            $skuid              = $request->sku_id;
            $sku_base_decode    = base64_decode($skuid);
            $sku_json_decode    = json_decode($sku_base_decode);
            $amount             = $sku_json_decode->plan_amount;
            $plan_currency      = $sku_json_decode->currency;
            //$multiply_amount = $this->multiplyAmount($plan_currency);
            //$itemPriceCents = round($plan_amount*$multiply_amount); 


        $getPlans=Subscription::where('_id',$plan_id)->first();
        foreach($getPlans->sub_plan as $plans){

            if($plans['number']==$number){

                $purchasetype = $request->purchase_type??$getPlans->type;
                $validity=$plans['validity'];
                $current = date("Y-m-d");
                $expiry_date=date('Y-m-d', strtotime("+".$validity." days"));
                $transaction = new Transaction;
                $transaction->plan_id=$plan_id;
                $transaction->user_id=$user_id;
                $transaction->count=(int)$plans['count'];
                $transaction->number=$plans['number'];
                $transaction->expiry_date=$expiry_date;
                $transaction->amount=$amount;
                $transaction->currency=$plan_currency;
                $transaction->validity=$plans['validity'];
                $transaction->purchase=(int)1;
                $transaction->listed=(int)0;
                $transaction->purchase_date=$current;
                $transaction->purchase_type=$purchasetype;
                $transaction->save();
                Session::put('transaction_id',$transaction->_id);

                /*update top rated if buddy or agent default role */
                if($purchasetype=='user')
                {
                 $user = User::where('_id', $user_id)->first();
                 if($user)
                 {
                     $top_list = $user->top_list;
                     $defaultLoginRole = $user->defaultLoginRole;
                     $updatedList = array();
                     if(!empty($top_list))
                     {
                        foreach($top_list as $val){

                            $updatedList[] = $val;
                        }   
                     }
                     $updatedList[] = $defaultLoginRole;
                    User::where('_id', $user_id)->update(['top_list'=>$updatedList]);
                 } 
                }
                /*update top rated if buddy or agent default role */


            }  
        }
        return response()->json(['status'=>1]);
    }
    function getCities(Request $request)
    {
        $country = $request->country;
        $cities = City::where('status',1)->whereHas('location',function($q) use($country){
            $q->where('name',$country);
        })->get();
        return response()->json(['status'=>1,'success' => true, 'cities' => $cities]);
    }


    /*stripe payment init popup start*/
    public function stripePaymentInit(Request $request){
        return view('frontend.list-property.stripe-payment-property');
    } 
     /*stripe payment init popup start*/
    public function stripePaymentPremiumInit(Request $request){
        return view('frontend.list-property.stripe-payment-premium');
    }
    /*stripe payment init popup end*/

     /*stripe payment init popup start stripePaymentSubscribe , stripePaymentSubscribeSuccess*/
    public function stripePaymentSubscribe(Request $request){
        return view('frontend.profile.requests.stripe-payment-subscribe');
    }
    /*stripe payment init popup end*/
     /*stripe payment init popup start*/
    public function stripePaymentPremiumSuccess(Request $request){
        return view('frontend.list-property.stripe-premium-success');
    }
    /*stripe payment init popup end*/
    /*stripe payment init popup start*/
    public function stripePaymentSuccess(Request $request){
        return view('frontend.list-property.stripe-success');
    }
    /*stripe payment init popup end*/ 
    /*stripe payment init popup start*/
    public function stripePaymentSubscribeSuccess(Request $request){
        return view('frontend.profile.requests.stripe-subscribe-success');
    }
    /*stripe payment init popup end*/
    
    /*stripe payment recieve start */
public function multiplyAmount($plan_currency)
{
    switch ($plan_currency) {
        case 'KWD':
            return 1000;
            break;
        case 'USD':
            return 100;
            break;

        default:
             return 100;
            break;
        }
}


    public function stripeorder(Request $request)
{    


//create_payment_intent
//create_customer
              \Stripe\Stripe::setApiKey(env('STRIPE_SECRET')); 


        $jsonStr = file_get_contents('php://input'); 
        $jsonObj = json_decode($jsonStr); 
         
        if($jsonObj->request_type == 'create_payment_intent'){ 
             
            // Define item price and convert to cents 
           /* $itemPriceCents = round($jsonObj->plan_amount*100); */
             
 
 
                $skuid              = $jsonObj->sku_id;
                $sku_base_decode    = base64_decode($skuid);
                $sku_json_decode    = json_decode($sku_base_decode);

                $plan_amount        = $sku_json_decode->plan_amount;
                $plan_currency      = $sku_json_decode->currency;

                $current_exchange = @Currency::convert()
                ->from($plan_currency)
                ->to(env('STRIPE_CURRENCY'))
                ->amount($plan_amount)
                ->get();
                $current_exchange  = round($current_exchange);

                $to_be_plan_currency = env('STRIPE_CURRENCY');
                $multiply_amount = $this->multiplyAmount($to_be_plan_currency);
                $itemPriceCents = ($current_exchange*$multiply_amount); 

             // Set content type to JSON 
            header('Content-Type: application/json'); 
             
            try { 
                // Create PaymentIntent with amount and currency 
                $paymentIntent = \Stripe\PaymentIntent::create([ 
                    'amount' => $itemPriceCents, 
                    'currency' => $to_be_plan_currency, 
                    'description' => 'New Add Property', 
                    'payment_method_types' => [ 
                        'card' 
                    ] 
                ]); 
             
                $output = [ 
                    'id' => $paymentIntent->id, 
                    'clientSecret' => $paymentIntent->client_secret 
                ]; 
             
                echo json_encode($output); 
            } catch (Error $e) { 
                //http_response_code(500); 
                echo json_encode(['error' => $e->getMessage()]); 
            } 

        }elseif($jsonObj->request_type == 'create_customer'){ 
             $user =\Auth::user();
            $payment_intent_id = !empty($jsonObj->payment_intent_id)?$jsonObj->payment_intent_id:''; 
            $name = !empty($jsonObj->name)?$jsonObj->name:'Test'; 
            $email = !empty($jsonObj->email)?$jsonObj->email:'test@gmail.com'; 
            $plan_id = !empty($jsonObj->plan_id)?$jsonObj->plan_id:''; 
            $amount = !empty($jsonObj->amount)?$jsonObj->amount:''; 
            $number = !empty($jsonObj->number)?$jsonObj->number:''; 
            $purchase_type = !empty($jsonObj->purchase_type)?$jsonObj->purchase_type:''; 
             
            // Add customer to stripe 
            try {   
                $customer = \Stripe\Customer::create(array(  
                    'name' => $name,  
                    'email' => $email,
                      'address' => [
                        'line1' => $user->address,
                        'line2' =>  'test address',
                        'postal_code' => '',
                        'city' => '',
                        'state' => '',
                        'country' => $user->country
                    ],
                ));  
            }catch(Exception $e) {   
                $api_error = $e->getMessage();   
            } 
             
            if(empty($api_error) && $customer){ 
                try { 
                    // Update PaymentIntent with the customer ID 
                    $paymentIntent = \Stripe\PaymentIntent::update($payment_intent_id, [ 
                        'customer' => $customer->id 
                    ]); 
                } catch (Exception $e) {  
                    // log or do what you want 
                } 
                 
                $output = [ 
                    'id' => $payment_intent_id, 
                    'customer_id' => $customer->id,
                    'plan_id' => $plan_id,
                    'number' => $number,
                    'amount' => $amount,
                    'purchase_type' => $purchase_type,
                ]; 
                echo json_encode($output); 
            }else{ 
                http_response_code(500); 
                echo json_encode(['error' => $api_error]); 
            } 
        }

        elseif($jsonObj->request_type == 'payment_insert'){ 
                 
                $payment_intent = !empty($jsonObj->payment_intent)?$jsonObj->payment_intent:''; 
                $customer_id = !empty($jsonObj->customer_id)?$jsonObj->customer_id:''; 
                 
                // Retrieve customer info 
                try {   
                    $customer = \Stripe\Customer::retrieve($customer_id);  
                }catch(Exception $e) {   
                    $api_error = $e->getMessage();   
                } 
                 
                // Check whether the charge was successful 
                if(!empty($payment_intent) && $payment_intent->status == 'succeeded'){

                    $this->transaction($request);
                    echo json_encode(['success' => 1,'message'=>'Payment Recieved']);
                     /*$this->transaction($request); */
                    // Transaction details  
                    /*$transactionID = $payment_intent->id; 
                    $paidAmount = $payment_intent->amount; 
                    $paidAmount = ($paidAmount/100); 
                    $paidCurrency = $payment_intent->currency; 
                    $payment_status = $payment_intent->status; 
                     */
                   /* $name = $email = ''; 
                    if(!empty($customer)){ 
                        $name = !empty($customer->name)?$customer->name:''; 
                        $email = !empty($customer->email)?$customer->email:''; 
                    } */

                    //$this->transaction($request);
                     
                    //echo json_encode(['success' => 1,'message'=>'Payment Recieved']);
  
                }else{ 
                    http_response_code(500); 
                    echo json_encode(['error' => 'Transaction has been failed!']); 
                } 
            }


   

   // //return $request->stipe_payment_btn; //response()->json($request);
   //  if($request->stipe_payment_btn !=='')
   //  {

   //      $user =\Auth::user();
   //      //$user->address; $user->country
   //      $stripetoken = $request->input('stripeToken');
   //       $amount = $request->input('amount');
         
   //      Stripe::setApiKey(env('STRIPE_SECRET'));
   //      \Stripe\Charge::create([
   //          'amount' => 100 * $amount,
   //          'currency' => env('STRIPE_CURRENCY'),
   //          'description' => "Thank you for purchasing with The Gulf Road",
   //          'source' => $stripetoken,
   //          'shipping' => [
   //              'name' => $user->name,
   //              'phone' => $user->phone,
   //              'address' => [
   //                  'line1' => "Addrss 1",
   //                  'line2' =>  "Addrss 1",
   //                  'postal_code' => '110025',
   //                  'city' => 'Delhi',
   //                  'state' => 'Delhi',
   //                  'country' => "India"
   //              ],
   //          ],
   //      ]);
   //      return response()->json(['status'=>1]);
         
   //  } 
} 
 public function stripeorderpremium(Request $request)
{    

    //return $request->stipe_payment_btn; //
   // return $request->stipe_payment_btn;// response()->json($request);
    if(!empty($request->stipe_payment_btn))
    {


        $user =\Auth::user();

        $stripetoken = $request->stripeToken;
        $amount = $request->amount;
          
        Stripe::setApiKey(env('STRIPE_SECRET'));
        \Stripe\Charge::create([
            'amount' => 100 * $amount,
            'currency' => 'USD',//env('STRIPE_CURRENCY'),
            'description' => "Thank you for purchasing with The Gulf Road",
            'source' => $stripetoken,
            'shipping' => [
                'name' => $user->name,
                'phone' => $user->phone,
                'address' => [
                    'line1' => $user->address,
                    'line2' =>  '',
                    'postal_code' => '',
                    'city' => '',
                    'state' => '',
                    'country' => $user->country
                ],
            ],
        ]);
        return response()->json(['status'=>1]);
         
    }
}
    /*stripe payment recieve end*/

}
