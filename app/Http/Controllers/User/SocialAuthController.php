<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Socialite;
use App\Models\{
	Country, User, Media
};
use Auth;

class SocialAuthController extends Controller
{
	private $redirectTo;
	
	public function __construct()
	{
		$this->req = app('request');
		$this->lang = app()->getLocale(); // get current locale language
		$this->redirectTo = app()->getLocale() . '/'; // set redirect url after login
	}

	/**
	* redirect to social media login page for Login/SIgnup purpose
	*/
	public function redirect() {
		return Socialite::driver($this->req->segment(3))->redirect();
	}

	/**
	* Handle response return from social media
	* If user exists then login the user with details else register the user 
	*/
	public function callback() {
		$provider = $this->req->segment(3);
		$userData = Socialite::driver($provider)->stateless()->user();
		$user_name = explode(" ",$userData->getName());
		$users = User::where(['email' => $userData->getEmail()])->first();
		 $media = Media::first();
		if($users){

			if(@$users->status==0){
				$status=1;
				return view('frontend.landingPage.index',compact('status','media'));
			}
            Auth::login($users);
            return redirect($this->redirectTo);
        }
        else{
			$user = User::create([
				'name'        => $userData->getName(),
				'first_name'  => $user_name[0]??'',
				'middle_name' => count($user_name) > 2 ? $user_name[1] : '',
				'last_name'   => count($user_name) > 2 ? $user_name[2] : ($user_name[1]??''),
				'email'       => strtolower($userData->getEmail()),
				'image'       => '',//$userData->getAvatar(),
				'provider_id' => $userData->getId(),
				'provider'    => $provider,
				'password' => '',
	            'country' => '',
	            'countryCode' => '',
	            'phone' => '',
	            'role' => ['USER'],
	            'registerVia' => $provider,
	            'defaultLoginRole' => 'USER',
	            'status'=>1,
	            'email_verified'=>1,
	            'logged_first_time'=>false,
	        ]);
			Auth::login($user);
			return redirect($this->redirectTo);
     		return redirect(app()->getLocale() . '/skip');
        }
	}
}