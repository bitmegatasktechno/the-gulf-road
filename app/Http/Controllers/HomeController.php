<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Auth,Session,DB,Stripe;
use \Carbon\Carbon;
use Instagram\Api;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use App\Mail\Enquiry; 
use App\Mail\HomeLoanEnquiry; 
use App\Mail\SubscribeEnquiry; 
use AmrShawky\LaravelCurrency\Facade\Currency;

use App\Models\{
    Property, User, CoworkspaceProperty, SwapProperty,UserEnquiry,Review,Blog,Specialization,Video,Favourate,ContactOwner,Report,BookProperty,Consultation,Newsletter,Subscribe,Module,Testimonial,Homeloan,UserAward,Notification,ContactUs,CospaceStory,Transaction, Media ,Subscription
};


class HomeController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        
        $media = Media::first();
        return view('frontend.landingPage.index')->with('media',$media);
    }

    public function about()
    {
        return view('frontend.others.about_us');
    }

    public function tearms()
    {
        return view('frontend.others.tearms');
    }

    public function privacy()
    {
        return view('frontend.others.privacy');
    }

    public function contact()
    {
        return view('frontend.others.contact');
    }

    public function package()
    {
        return view('frontend.others.packagePage');
    }
    public function faq()
    {
        return view('frontend.others.faq');
    }

    /**
    * redirect to home page after user succesfully logged in
    */
    public function home(){
        $defaultCountry = request()->segment(3);
        if(@$defaultCountry){
            $defaultCountry = request()->segment(3);
        }else{
            $defaultCountry='all';
        }
        $defaultLanguage = request()->segment(1);
        $modules=Module::where('location',$defaultCountry)->get();
        if($modules->count() > 0){
        foreach($modules as $module){
        if($module->type=='buy'){
            return redirect()->route('user.buy',['locale' => $defaultLanguage,'slug'=>$defaultCountry]);
        }
    }
            // return view('frontend.home.index', compact('blogs','defaultCountry','allProperties','rentProperties','saleProperties','buddies','agents'));
        foreach($modules as $module){
        if($module->type=='rent'){
        return redirect()->route('user.rent',['locale' => $defaultLanguage,'slug'=>$defaultCountry]);
        }
       }   // return view('frontend.home.index', compact('blogs','defaultCountry','allProperties','rentProperties','saleProperties','buddies','agents'));
       foreach($modules as $module){
       if($module->type=='swap'){
            return redirect()->route('user.swap',['locale' => $defaultLanguage,'slug'=>$defaultCountry]);
        }
    }
            // return view('frontend.home.swap', compact('blogs','defaultCountry','allProperties','rentProperties','saleProperties','buddies','agents'));
        foreach($modules as $module){
        if($module->type=='cospace'){
            return redirect()->route('user.cospace',['locale' => $defaultLanguage,'slug'=>$defaultCountry]);
        }
    }
            // return view('frontend.home.co_space', compact('blogs','defaultCountry','allProperties','rentProperties','saleProperties','buddies','agents'));
            foreach($modules as $module){
            if($module->type=='agent'){
            return redirect()->route('user.agents',['locale' => $defaultLanguage,'slug'=>$defaultCountry]);
        }
    }
            // return view('frontend.home.agents', compact('blogs','defaultCountry','allProperties','rentProperties','saleProperties','buddies','agents'));
            foreach($modules as $module){
            if($module->type=='buddy'){
            return redirect()->route('user.buddies',['locale' => $defaultLanguage,'slug'=>$defaultCountry]);
        }
    }
            // return view('frontend.home.buddies', compact('blogs','defaultCountry','allProperties','rentProperties','saleProperties','buddies','agents'));
    }else{
        return redirect()->route('user.buy',['locale' => $defaultLanguage,'slug'=>$defaultCountry]);
    }
    }

    public function buy_landing(){
 

        $loc=request()->segment(3);
        if($loc == 'ksa' || $loc == 'uae' )
        {
           $loc21 = strtoupper($loc);
        }else 
        {
            $loc21 = ucfirst($loc);
        }
        
        $defaultCountry = $loc21;


        $defaultLanguage = request()->segment(1);
        $video=Video::where('user_type',"1")->first();
        $modules=Module::where('location',$defaultCountry)->get();
        $allProperties = Property::where('is_approved',1)->where(function($q){
            $q->where('listing_type','sale')->orWhere('listing_type','both');
        })->where('is_populated',1)->where('country',$defaultCountry)
        ->whereHas('transaction',function($qu){
            $qu->where('expiry_date','>=',date("Y-m-d"));
        })
        ->orderBy('_id','DESC')->get();
        $rentProperties = Property::where('is_approved',1)->where('listing_type', 'rent')->where('country',$defaultCountry)
        ->whereHas('transaction',function($qu){
            $qu->where('expiry_date','>=',date("Y-m-d"));
        })->orderBy('_id','DESC')->limit(8)->get();
        $saleProperties = Property::where('is_approved',1)->where('listing_type', 'sale')->where('country',$defaultCountry)
        ->whereHas('transaction',function($qu){
            $qu->where('expiry_date','>=',date("Y-m-d"));
        })
        ->orderBy('_id','DESC')->limit(8)->get();
        $buddies = User::with('reviews');
        $agents = User::with('reviews');
        if(\Auth::user()){
            $buddies = $buddies->where('_id','!=',\Auth::user()->id);
            $agents = $agents->where('_id','!=',\Auth::user()->id);
        }
        $elevated = Transaction::where('purchase_type','user')->where('expiry_date','>=',date("Y-m-d"))->pluck('user_id')->toArray();
        $buddy_ids = $buddies->where('country',$defaultCountry)->whereIn('top_list', ['BUDDY'])->pluck('_id')->toArray();
        $agent_ids = $agents->where('country',$defaultCountry)->whereIn('top_list', ['AGENT'])->pluck('_id')->toArray();
        $b_ids = array_merge($elevated,$buddy_ids);
        $a_ids = array_merge($elevated,$agent_ids);
        $buddies = User::with('reviews')->where('country',$defaultCountry)->where('status',1)->where('defaultLoginRole','BUDDY')->whereIn('_id',$b_ids)->orderBy('created_at','DESC')->take(20)->get();
        $agents = User::with('reviews')->where('country',$defaultCountry)->where('status',1)->where('defaultLoginRole','AGENT')->whereIn('_id',$a_ids)->orderBy('created_at','DESC')->take(20)->get();
       
        // if(\Auth::user()){
        //     $results = User::with('reviews')->where('_id','!=',\Auth::user()->id)->where('country',$defaultCountry)->where('top_list', 'BUDDY')->orderBy('_id','DESC')->take(20)->get();
        //     if($results->count() == 0){
        //         $results = User::with('reviews')->where('_id','!=',\Auth::user()->id)->where('country',$defaultCountry)->orderBy('_id','DESC')->take(20)->get();
        //     }
        // }else{
        //     $results = User::with('reviews')->whereIn('role',['BUDDY'])->where('country',$defaultCountry)->where('top_list', 'BUDDY')->orderBy('_id','DESC')->take(10)->get();
        //     if($results->count() == 0){
        //         $results = User::with('reviews')->has('reviews')->whereIn('role',['BUDDY'])->where('country',$defaultCountry)->where('top_list', 'BUDDY')->orderBy('_id','DESC')->take(10)->get();
        //     }
        // }
        // foreach ($results as  $buddy) {
        //     if($buddy->reviews->count() > 0){
        //         $totalrating= $buddy->reviews->count() * 5;
        //         $buddy->rating = number_format($buddy->reviews->sum('average') / $totalrating *5);
        //     }else{
        //         $buddy->rating = 0;
        //     }
        // }
        // $buddies = $results->sortByDesc('rating');
        // $agents = User::whereIn('role',['AGENT'])->where('country',$defaultCountry)->orderBy('_id','DESC')->get();

        //dd($buddy_ids);
        $blogs = Blog::where('status',1)->orderBy('_id','DESC')->get();
        $reviews = Testimonial::where('status',1)->get();
        return view('frontend.home.index', compact('blogs','defaultCountry','allProperties','rentProperties','saleProperties','buddies','agents','reviews','video'));
    }


    public function buddies_landing(){
        $loc=request()->segment(3);
        if($loc == 'ksa' || $loc == 'uae' )
        {
           $loc21 = strtoupper($loc);
        }else 
        {
            $loc21 = ucfirst($loc);
        }
        
        $defaultCountry = $loc21;

        $defaultLanguage = request()->segment(1);
        $video=Video::where('user_type',"2")->first();
         
        $allProperties = Property::where('is_approved','!=',0)->where('listing_type', 'rent')->where('country',$defaultCountry)
        ->whereHas('transaction',function($qu){
            $qu->where('expiry_date','>=',date("Y-m-d"));
        })->orderBy('_id','DESC')->get();
        $rentProperties = Property::where('is_approved','!=',0)->where('listing_type', 'rent')->where('country',$defaultCountry)
        // ->where('is_premium',1)
        ->whereHas('transaction',function($qu){
            $qu->where('expiry_date','>=',date("Y-m-d"));
        })->orderBy('_id','DESC')->limit(3)->get();
        $saleProperties = Property::where('is_approved','!=',0)->where('listing_type', 'sale')->where('country',$defaultCountry)
        // ->where('is_premium',1)
        ->whereHas('transaction',function($qu){
            $qu->where('expiry_date','>=',date("Y-m-d"));
        })->orderBy('_id','DESC')->limit(2)->get();
        $buddies = User::whereIn('role',['BUDDY'])->where('country',$defaultCountry)->where('status',1)->whereIn('top_list',['BUDDY'])->orderBy('_id','DESC')->where('status', 1)->get();
        $agents = User::whereIn('role',['AGENT'])->where('country',$defaultCountry)->where('status',1)->whereIn('top_list',['AGENT'])->orderBy('_id','DESC')->get();
            // dd($buddies);
        $blogs = Blog::where('status',1)->orderBy('_id','DESC')->get();
        return view('frontend.home.buddies', compact('blogs','defaultCountry','allProperties','rentProperties','saleProperties','buddies','agents','video'));
    }

    

    public function cospace_landing(){
        $video=Video::where('user_type',"1")->first();
        $loc=request()->segment(3);
        if($loc == 'ksa' || $loc == 'uae' )
        {
           $loc21 = strtoupper($loc);
        }else 
        {
            $loc21 = ucfirst($loc);
        }
        
        $defaultCountry = $loc21;

        $allProperties = Property::where('is_approved','!=',0)->where('listing_type', 'rent')->where('country',$defaultCountry)
        ->whereHas('transaction',function($qu){
            $qu->where('expiry_date','>=',date("Y-m-d"));
        })->orderBy('_id','DESC')->get();
        $rentProperties = Property::where('is_approved','!=',0)->where('listing_type', 'rent')->where('country',$defaultCountry)
        ->whereHas('transaction',function($qu){
            $qu->where('expiry_date','>=',date("Y-m-d"));
        })->orderBy('_id','DESC')->limit(3)->get();
        $saleProperties = Property::where('is_approved','!=',0)->where('listing_type', 'sale')->where('country',$defaultCountry)
        ->whereHas('transaction',function($qu){
            $qu->where('expiry_date','>=',date("Y-m-d"));
        })->orderBy('_id','DESC')->limit(2)->get();
        $buddies = User::where('defaultLoginRole', 'BUDDY')->orderBy('_id','DESC')->get();
        $agents = User::where('defaultLoginRole', 'AGENT')->orderBy('_id','DESC')->get();
        $blogs = Blog::where('status',1)->orderBy('_id','DESC')->get();
        $CospaceStory = CospaceStory::where('status',1)->orderBy('_id','desc')
                        // ->limit(3)
                        ->get();
         
        $data=CoworkspaceProperty::where('is_approved','!=',0)->where('country',$defaultCountry)
        ->whereHas('transaction',function($qu){
            $qu->where('expiry_date','>=',date("Y-m-d"));
        })->orderBy('_id','desc');
        $data = $data->limit(8)->paginate(8);
        $cachePool = new FilesystemAdapter('Instagram', 0, __DIR__ . '/../cache');
        // $api = new Api($cachePool);
        // $api->login('thegulfroad9', 'thegulfroad@123'); // mandatory
        // $instagram = $api->getProfile('thegulfroad9');
        // $instagram = $instagram->getMedias();
        $instagram = [];
        return view('frontend.home.co_space', compact('blogs','defaultCountry','allProperties','rentProperties','saleProperties','buddies','agents','video','data','instagram','CospaceStory'));
    }

    public function swap_landing(){
        $video=Video::where('user_type',"1")->first();
        $loc=request()->segment(3);
        if($loc == 'ksa' || $loc == 'uae' )
        {
           $loc21 = strtoupper($loc);
        }else 
        {
            $loc21 = ucfirst($loc);
        }
        
        $defaultCountry = $loc21;


        $allProperties = SwapProperty::where('is_approved','!=',0)->where('is_populated',1)
        // ->where('is_populated',1)
        ->whereHas('transaction',function($qu){
            $qu->where('expiry_date','>=',date("Y-m-d"));
        })->orderBy('_id','DESC')->get();
        $rentProperties = Property::where('listing_type', 'rent')->where('country',$defaultCountry)
        ->whereHas('transaction',function($qu){
            $qu->where('expiry_date','>=',date("Y-m-d"));
        })->orderBy('_id','DESC')->limit(3)->get();
        $saleProperties = Property::where('is_approved',1)->where('listing_type', 'sale')->where('country',$defaultCountry)
        ->whereHas('transaction',function($qu){
            $qu->where('expiry_date','>=',date("Y-m-d"));
        })->orderBy('_id','DESC')->limit(2)->get();
        $buddies = User::where('defaultLoginRole', 'BUDDY')->where('country',$defaultCountry)->where('status',1)->orderBy('_id','DESC')->get();
        $agents = User::where('defaultLoginRole', 'AGENT')->where('country',$defaultCountry)->where('status',1)->orderBy('_id','DESC')->get();
        $blogs = Blog::where('status',1)->orderBy('_id','DESC')->get();
        return view('frontend.home.swap', compact('blogs','defaultCountry','allProperties','rentProperties','saleProperties','buddies','agents','video'));
    }

    public function rent_landing(){
        $loc=request()->segment(3);
        if($loc == 'ksa' || $loc == 'uae' )
        {
           $loc21 = strtoupper($loc);
        }else 
        {
            $loc21 = ucfirst($loc);
        }
        
        $defaultCountry = $loc21;
         

         /* $update=\DB::table('transaction')
                    ->where('purchase_type','cospace')
                    ->update(['expiry_date'=>date('Y-m-d', strtotime(' +25 day'))]);*/

        $defaultLanguage = request()->segment(1);
        $video=Video::where('user_type',"1")->first();
         ;
        $allProperties = Property::where('is_approved',1)->where(function($q){
            $q->where('listing_type','rent')->orWhere('listing_type','both');
        })->where('is_populated',1)->where('country',$defaultCountry)
        ->whereHas('transaction',function($qu){
            $qu->where('expiry_date','>=',date("Y-m-d"));
        })->orderBy('_id','DESC')->get();


//dd($allProperties);

        $rentProperties = Property::where('is_approved',1)->where('listing_type', 'rent')->where('country',$defaultCountry)
        ->whereHas('transaction',function($qu){
            $qu->where('expiry_date','>=',date("Y-m-d"));
        })->orderBy('_id','DESC')
        /*->limit(8)*/
        ->get();
        $saleProperties = Property::where('is_approved',1)->where('listing_type', 'sale')->where('country',$defaultCountry)
        ->whereHas('transaction',function($qu){
            $qu->where('expiry_date','>=',date("Y-m-d"));
        })->orderBy('_id','DESC')
        // ->limit(8)
        ->get();

         $buddies = User::with('reviews');
        $agents = User::with('reviews');
        if(\Auth::user()){
            $buddies = $buddies->where('_id','!=',\Auth::user()->id);
            $agents = $agents->where('_id','!=',\Auth::user()->id);
        }
        $elevated = Transaction::where('purchase_type','user')->where('expiry_date','>=',date("Y-m-d"))->pluck('user_id')->toArray();
        $buddy_ids = $buddies->where('country',$defaultCountry)->whereIn('top_list', ['BUDDY'])->pluck('_id')->toArray();
        $agent_ids = $agents->where('country',$defaultCountry)->whereIn('top_list', ['AGENT'])->pluck('_id')->toArray();
        $b_ids = array_merge($elevated,$buddy_ids);
        $a_ids = array_merge($elevated,$agent_ids);
        $buddies = User::with('reviews')->where('country',$defaultCountry)->where('status',1)->where('defaultLoginRole','BUDDY')->whereIn('_id',$b_ids)->orderBy('created_at','DESC')->take(20)->get();
        $agents = User::with('reviews')->where('country',$defaultCountry)->where('status',1)->where('defaultLoginRole','AGENT')->whereIn('_id',$a_ids)->orderBy('created_at','DESC')->take(20)->get();

        
        /*$buddies = User::with('reviews');
        $agents = User::with('reviews');
        if(\Auth::user()){
            $buddies = $buddies->where('_id','!=',\Auth::user()->id);
            $agents = $agents->where('_id','!=',\Auth::user()->id);
        }
        $elevated = Transaction::where('purchase_type','user')->where('expiry_date','>=',date("Y-m-d"))->pluck('user_id')->toArray();
        $buddy_ids = $buddies->where('country',$defaultCountry)->where('top_list', 'BUDDY')->pluck('_id')->toArray();
        $agent_ids = $agents->where('country',$defaultCountry)->where('top_list', 'AGENT')->pluck('_id')->toArray();
        $b_ids = array_merge($elevated,$buddy_ids);
        $a_ids = array_merge($elevated,$agent_ids);
        $buddies = User::with('reviews')->where('country',$defaultCountry)->whereIn('_id',$b_ids)->orderBy('_id','DESC')->take(20)->get();
        $agents = User::with('reviews')->where('country',$defaultCountry)->whereIn('_id',$a_ids)->orderBy('_id','DESC')->take(20)->get();*/
        // $buddies = User::whereIn('role',['BUDDY'])->where('country',$defaultCountry)->orderBy('_id','DESC')->get();
        // $agents = User::whereIn('role',['AGENT'])->where('country',$defaultCountry)->orderBy('_id','DESC')->get();
        $reviews = Testimonial::where('status',1)->get();
        $blogs = Blog::where('status',1)->orderBy('_id','DESC')->get();

        return view('frontend.home.rent', compact('blogs','defaultCountry','allProperties','rentProperties','saleProperties','buddies','agents','video','reviews'));
    }

    public function gallery(){
        $data = Property::where('is_approved',1)->where('_id', request()->segment(3))->select('files')->firstOrFail();

        return view('frontend.property-details.gallery.index', compact('data'));
    }

    /*
    * List properties for buy means show all properties which are marked for sale or both
    **/
    public function buyPropertyListing(Request $request){
 



 
       
        $loc=request()->segment(3);
        if($loc == 'ksa' || $loc == 'uae' )
        {
           $loc21 = strtoupper($loc);
        }else 
        {
            $loc21 = ucfirst($loc);
        }
        
        $defaultCountry = $loc21;

        $property = Property::where('is_approved',1)->where(function($q){
            $q->where('listing_type','sale')->orWhere('listing_type','both');
        })
        ->where('country',$defaultCountry)->whereHas('transaction',function($qu){
            $qu->where('expiry_date','>=',(date("Y-m-d")));
        }) ;


  
      
        //country  location longitude amenties
        
        $inputs = $request->all();

        if($request->filled('house_type')){
            $property->where('house_type', 'like', '%' . $request->input('house_type').'%');
        }
        if($request->filled('is_premium'))
        {
            $property->where('is_premium','!=',NULL);
        }
        if($request->filled('country')){
            $property->where('country', 'like', '%' . $request->input('country').'%');
        }
         if($loc21){
            $property->where('country',$loc21);
        }
        if($request->filled('city')){
            $property->where('city', $request->input('city'));
        }
        if($request->filled('bedrooms')){
            $property->where('no_of_bedrooms', $request->input('bedrooms'));
        }
        if($request->filled('bathrooms')){
            $property->where('no_of_bathrooms', $request->input('bathrooms'));
        }
        if($request->filled('furnishing_type')){
            $property->where('furnishing_type', $request->input('furnishing_type'));
        }
        if($request->filled('amenties')){
            $property->whereIn('amenties', [$request->input('amenties')]);
        }
        if($request->filled('price')){
            $property->orderBy('price', $request->input('price'));
        }else{
            $property->orderBy('_id', 'desc');
        }

        if($request->filled('min_size')){
            $property->where('build_up_area','>=',$request->input('min_size') );
        }
        if($request->filled('max_size')){
            $property->where('build_up_area','<=',$request->input('max_size') );
        }

        if($request->filled('location'))
        {

            $location = strtolower($request->input('location')) ;
         /*   $property->orWhere('property_title', 'like', '%' . $location . '%');
            $property->orWhere('property_description', 'like', '%' . $location. '%');
            $property->orWhere('location_name', 'like', '%' . $location .  '%');*/ 
/*
             $property->where(function($q)use($location){
                        $q->where('property_title', 'like', '%' . $location. '%')
                            ->orWhere('country', 'like', '%' . $location. '%')
                            ->orWhere('location_name', 'like', '%' . $location. '%');
                    }); */
             
            $max = 10000;
            if($request->input('nearby'))
            {
                $max = 20000;
            }
         

                  if($request->latitude !==''  && $request->longitude !=='')
                {
                      $latitude = (float)$request->latitude;
                      $longitude = (float)$request->longitude;
                        
                        $property->where('location', 'near', [
                            '$geometry' => [    
                                'type' => 'Point',
                                'coordinates' => [
                                    $longitude, // longitude
                                    $latitude, // latitude
                                ],
                            ],
                            '$maxDistance' => $max,
                        ]); 


                }else
                {
                    $property->where(function($q)use($location){
                        $q->where('property_title', 'like', '%' . $location. '%')
                            ->orWhere('country', 'like', '%' . $location. '%')
                            ->orWhere('location_name', 'like', '%' . $location. '%');
                    });
                }
                
             
        }
		
		  
        if($request->filled('property_title')){ 

            $property_title = $request->input('property_title');
            
                    $property->where(function($q)use($property_title){
                        $q->where('property_title', 'like', '%' . $property_title. '%')
                            ->orWhere('property_description', 'like', '%' . $property_title. '%')
                            ->orWhere('country', 'like', '%' . $property_title. '%')
                            ->orWhere('location_name', 'like', '%' . $property_title. '%');
                    });


                    /*$property->orWhere('property_title', 'like', '%' . $request->input('property_title') . '%')
                    ->orWhere('property_description', 'like', '%' . $request->input('property_title') . '%')
                    ->orWhere('location_name', 'like', '%' . $request->input('property_title') . '%');*/
        }
  
        $data = $property->paginate(12);
        
        $map_data = $property->paginate(12);

 

        $latlongData=self::getAllLongLat($map_data); 
		 
        if($request->ajax()){
			 
            return view('frontend.property-listing.buy.index', compact('map_data','data','latlongData'));
        }
       
        return view('frontend.property-listing.buy.index', compact('map_data','data','latlongData'));
    }

    /*
    * List properties for rent means show all properties which are marked for rent
    **/
    public function rentPropertyListing(Request $request){
          


 


        $agent_id = request()->segment(5);
		$loc  =  request()->segment(3);
        if($loc == 'ksa' || $loc == 'uae' )
        {
           $loc21 = strtoupper($loc);
        }else 
        {
            $loc21 = ucfirst($loc);
        }
        
        $defaultCountry = $loc21;
  

      

		  // $properties = DB::table('properties')->get();
		   //dd($request); country listed_by
		


         $property = Property::where('is_approved',1)->where(function($q){
            $q->where('listing_type','rent')->orWhere('listing_type','both');
        })
            ->where('country','like', '%' .$defaultCountry.'%')
            ->whereHas('transaction',function($qu){
                $qu->where('expiry_date','>=',date("Y-m-d"));
            }); 
         

        

 
          if($request->filled('house_type')){
            $property->where('house_type', $request->input('house_type'));
             
             
        }
         
        if($request->filled('is_premium'))
        {
            $property->where('is_premium','!=',NULL);
        }
        if($request->filled('furnishing_type')){
            $property->where('furnishing_type', $request->input('furnishing_type'));
        }
        if($request->filled('country')){
            $property->where('country', 'like', '%' . $request->input('country').'%');
        }
        if($loc21){
            $property->where('country',$loc21);
        }
        if($request->filled('city')){
            $property->where('city', 'like', '%' . $request->input('city').'%');
        }
        if($request->filled('bedrooms')){
            $property->where('no_of_bedrooms', $request->input('bedrooms'));
        }
        if($request->filled('bathrooms')){
            $property->where('no_of_bathrooms', $request->input('bathrooms'));
        }
        
        
         
        if($request->filled('amenties')){
            $property->whereIn('amenties', [$request->input('amenties')]);
        }
        if($request->filled('min_size')){
            $property->where('build_up_area','>=',$request->input('min_size') );
        }
        if($request->filled('max_size')){
            $property->where('build_up_area','<=',$request->input('max_size') );
        }
        if($request->filled('price')){
            $property->orderBy('rent', $request->input('price'));
        }else{
            $property->orderBy('_id', 'desc');
        }
        if($request->filled('location')){
           
            

             

            /*$property->orWhere('property_title', 'like', '%' . $location . '%');
            $property->orWhere('location_name', 'like', '%' . $location .  '%');*/


           $max = 10000;
            if($request->input('nearby'))
            {
                $max = 20000;
            }
         

                  if($request->latitude !==''  && $request->longitude !=='')
                {
                      $latitude = (float)$request->latitude;
                      $longitude = (float)$request->longitude;
                        
                        $property->where('location', 'near', [
                            '$geometry' => [    
                                'type' => 'Point',
                                'coordinates' => [
                                    $longitude, // longitude
                                    $latitude, // latitude
                                ],
                            ],
                            '$maxDistance' => $max,
                        ]); 


                }else
                {
                    /*$location = strtolower($request->input('location')) ;
                    $property->where(function($q)use($location){
                        $q->where('property_title', 'like', '%' . $location. '%')
                            ->orWhere('city', 'like', '%' . $location. '%')
                            ->orWhere('country', 'like', '%' . $location. '%')
                            ->orWhere('location_name', 'like', '%' . $location. '%');
                    });*/
                }
         }
        if($request->filled('property_title')){
                    $property_title = $request->input('property_title');
                    $property->where(function($q)use($property_title){
                        $q->where('property_title', 'like', '%' . $property_title. '%')
                            ->orWhere('property_description', 'like', '%' . $property_title. '%')
                            ->orWhere('country', 'like', '%' . $property_title. '%')
                            ->orWhere('city', 'like', '%' . $property_title. '%')
                            ->orWhere('location_name', 'like', '%' . $property_title. '%');
                    }); 
        }  

        if($request->filled('rent_frequenecy'))
        {

            $property->where('rent_frequency', $request->input('rent_frequenecy'));
        } 

         
  
		
        $data = $property->paginate(12);


           
        $map_data = $property->paginate(12);

        $latlongData=self::getAllLongLat($map_data);
		
		   
        
		 
        if($request->ajax()){
            //return view('frontend.property-listing.rent.index', compact('map_data','data','latlongData'));
            return view('frontend.property-listing.rent.index', compact('map_data','data','latlongData'));
        }

        return view('frontend.property-listing.rent.index', compact('map_data','data','latlongData'));
    }

    public function tabPropertyListing(Request $request){
        $id = $request->id;
        $property = Property::where('is_approved',1)->where('user_id',$id);
        if($request->type==1){
            $property->where('listing_type',  'both');
            //$property->where('listing_type', 'like', '%' . 'both');
        }
        if($request->type==2){
            $property->where('listing_type', 'rent');   
            //$property->where('listing_type', 'like', '%' . 'rent');   
        }
        if($request->type==3){
            $property->where('listing_type', 'sale');   
           // $property->where('listing_type', 'like', '%' . 'sale');   
        }
        $myProperty = $property->paginate(12);
        if($request->ajax()){
            return view('frontend.users-details.agents.list', compact('myProperty'));
        }

        return view('frontend.users-details.agents.index', compact('myProperty'));
    }

    public function rateListing(Request $request){
        $id=$request->id;
        $ratingsData=Review::where('is_approved',1)->where('review_type',"6")->where('type_id',$id)->orderBy('created_at','DESC');
        if($request->type==6){
            $ratingsData->where('average','>',0);   
        }
        if($request->type==5){
            $ratingsData->where('average', 5);   
        }
        if($request->type==4){
            $ratingsData->where('average', 4);   
        }
        if($request->type==3){
            $ratingsData->where('average', 3);   
        }
        if($request->type==2){
            $ratingsData->where('average', 2);   
        }
        if($request->type==1){
            $ratingsData->where('average', 1);   
        }
        $ratings = $ratingsData->paginate(12);
      
        if($request->ajax()){
            // return redirect()->route('filterList')->with( ['data' => $data] );
            return view('frontend.users-details.agents.ratings', compact('ratings'));
        }

        return view('frontend.users-details.agents.index', compact('ratings'));
    }

    public function rateListingBuddy(Request $request){
        $id=$request->id;
        $ratingsData=Review::where('is_approved',1)->where('review_type',"7")->where('type_id',$id)->orderBy('created_at','DESC');
        if($request->type==6){
            $ratingsData->where('average','>',0);   
        }
        if($request->type==5){
            $ratingsData->where('average', 5);   
        }
        if($request->type==4){
            $ratingsData->where('average', 4);   
        }
        if($request->type==3){
            $ratingsData->where('average', 3);   
        }
        if($request->type==2){
            $ratingsData->where('average', 2);   
        }
        if($request->type==1){
            $ratingsData->where('average', 1);   
        }
        $ratings = $ratingsData->paginate(12);
      
        if($request->ajax()){
            // return redirect()->route('filterList')->with( ['data' => $data] );
            return view('frontend.users-details.buddies.ratings', compact('ratings'));
        }

        return view('frontend.users-details.buddies.index', compact('ratings'));
    }


    public function filterPropertyListing(Request $request){
        $id = $request->user_id;
        if($type=$request->segment_3!='coworkspace' || $type=$request->segment_3!='swap'){
        $property = Property::where('is_approved',1)->where('_id','!=','');
        }else if($type=$request->segment_3=='coworkspace'){
            $property = CoworkspaceProperty::where('is_approved',1)->where('_id','!=','');
        }else if($type=$request->segment_3=='swap'){
            $property = SwapProperty::where('is_approved',1)->where('_id','!=','');
            }
        
        $inputs = $request->all();

        if($request->filled('house_type')){
            $property->where('house_type', 'like', '%' . $request->input('house_type'));
            
        }
        if($request->filled('furnishing_type')){
            $property->where('furnishing_type', $request->input('furnishing_type'));
        }
        if($request->filled('country')){
            $property->where('country', 'like', '%' . $request->input('country'));
        }
        if($request->filled('bedrooms')){
            $property->where('no_of_bedrooms', $request->input('bedrooms'));
        }
        if($request->filled('bathrooms')){
            $property->where('no_of_bathrooms', $request->input('bathrooms'));
        }
        if($request->filled('rent_frequenecy')){
            $property->where('rent_frequenecy', $request->input('rent_frequenecy'));
        }
        if($request->filled('amenties')){
            $property->whereIn('amenties', [$request->input('amenties')]);
        }
        if($request->filled('min_size')){
            $property->where('build_up_area','>=',$request->input('min_size') );
        }
        if($request->filled('max_size')){
            $property->where('build_up_area','<=',$request->input('max_size') );
        }
        if($request->filled('price')){
            $property->orderBy('rent', $request->input('price'));
        }else{
            $property->orderBy('_id', 'desc');
        }

        if($request->filled('location')){
            $location = $request->input('location');
            $property->where(function($q) use($location){
                $q->where('location_name', 'like', '%' . $location . '%')
                  ->orWhere('country', 'like', '%' . $location . '%');
            });
        }

        if($request->filled('property_title')){
            $property->where('property_title', 'like', '%' . $request->input('property_title') . '%')
            ->orWhere('property_description', 'like', '%' . $request->input('property_title') . '%');
        }

        $myProperty = $property->paginate(12);
        
        if($request->ajax()){
            return response()->json(['success'=>1]);
            // return redirect()->route('filterList')->with( ['data' => $data] );
            // return view('frontend.property-details.common-data.list', compact('myProperty'));
        }

        return view('frontend.users-details.agents.index', compact('myProperty'));
    }

    /**
    * list all getCoworkspaceLists
    */
    public function getCoworkspaceLists(Request $request, $filter_by = null){
		
		 

         //dd($request);
        $filter_by = request()->filter_by;

        $loc=request()->segment(3);
        if($loc == 'ksa' || $loc == 'uae' )
        {
           $loc21 = strtoupper($loc);
        }else 
        {
            $loc21 = ucfirst($loc);
        }
        
        $defaultCountry = $loc21;


        //$defaultCountry = ucfirst(request()->segment(3)); type_filter
		 $all_data = CoworkspaceProperty::where('is_approved',1)->get();
		 
		  
        $data=CoworkspaceProperty::where('is_approved',1)->where('country',$defaultCountry)->whereHas('transaction',function($qu){
            $qu->where('expiry_date','>=',date("Y-m-d"));
        });
        if($request->filled('country')){
            $data->where('country', 'like', '%' . ucfirst($request->input('country')).'%');
        }
        if($request->filled('location')){
            
			/* // $data->where('location_name', 'like', '%' . ($location).'%'); 
             $data->where(function($q) use($location){
                   $q->where('location_name', 'like', '%' . $location . '%')
                   ->orWhere('address', 'like', '%' . $location . '%');
              });
*/
           $max = 10000;

            if($request->input('nearby'))
            {
                $max = 20000;
            }
         

                  if($request->latitude !==''  && $request->longitude !=='')
                {
                      $latitude = (float)$request->latitude;
                      $longitude = (float)$request->longitude;
                        
                        $data->where('location', 'near', [
                            '$geometry' => [    
                                'type' => 'Point',
                                'coordinates' => [
                                    $longitude, // longitude
                                    $latitude, // latitude
                                ],
                            ],
                            '$maxDistance' => $max,
                        ]); 


                }else
                {

                    $location = $request->input('location');

                    $data->where(function($q)use($location){
                        $q->where('title', 'like', '%' . $location. '%')
                            ->orWhere('landmark', 'like', '%' . $location. '%')
                            ->orWhere('city', 'like', '%' . $location. '%')
                            ->orWhere('country', 'like', '%' . $location. '%')
                            ->orWhere('address_line_1', 'like', '%' . $location. '%')
                            ->orWhere('address_line_2', 'like', '%' . $location. '%')
                            ->orWhere('location_name', 'like', '%' . $location. '%');
                    });
                }
           
        }
        if($filter_by){
            switch ($filter_by) {
                case 'dedicated_desk_available':
                    $data->where(function($q){
                        $q->where('dedicated_desk_available', 1);
                    });
                    break;
                case 'private_office_available':
                    $data->where(function($q){
                        $q->where('private_office_available',1);
                    });
                    break;
                default:
                break;
            }
        }
        if($request->filled('type')){
            switch ($request->type) {
                case 'open':
                    $data->where(function($q){
                        $q->where('open_workspace_available', 1);
                    });
                    break;
                case 'dedicated':
                    $data->where(function($q){
                        $q->where('dedicated_desk_available', 1);
                    });
                    break;
                case 'private':
                    $data->where(function($q){
                        $q->where('private_office_available',1);
                    });
                    break;
                default:
                break;
            }
        }
        if($request->filled('people')){
            $data->where(function($q) use($request){
                $q->where('open_space_seats', 'like', '%' . $request->input('people'))
                ->orWhere('dedicated_space_seats','like', '%' . $request->input('people'))
                ->orWhere('seating_capacity','like', '%' . $request->input('people'));
            });
        }


        $data = $data->paginate(12);
         //dd($data);
        $map_data = $data;

        $latlongData=self::getCospaceLongLat($map_data);
 		
 
                   
        if($request->ajax()){
			 
            $result_count = $data->count();
            /*if(isset($request->view_type) && $request->view_type == 'map')
            {
                // dd($latlongData);
                return view('frontend.property-listing.coworkspace.map_list', compact('data','map_data', 'result_count','latlongData'));
            }*/
            //return view('frontend.property-listing.coworkspace.list', compact('data','map_data', 'result_count'));
            return view('frontend.property-listing.coworkspace.index', compact('data','map_data','result_count','latlongData'));
        }
        return view('frontend.property-listing.coworkspace.index', compact('data','map_data','latlongData'));
    }

    /**
    * list all swap property listing
    */
    public function getSwapLists(Request $request){
       // $defaultCountry = ucfirst(request()->segment(3));

         
         $loc=request()->segment(3);
        if($loc == 'ksa' || $loc == 'uae' )
        {
           $loc21 = strtoupper($loc);
        }else 
        {
            $loc21 = ucfirst($loc);
        }
        
        $defaultCountry = $loc21;


        $data = SwapProperty::where('is_approved',1)
                           ->orderBy('_id','desc') 
                            ->where('country',$defaultCountry)
                            ->whereHas('transaction',function($qu){
                                $qu->where('expiry_date','>=',date("Y-m-d"));
                            });
        
        if($request->filled('location')){

            /*
            $data->orWhere('title', 'like', '%' . $location . '%');
             $data->orWhere('location_name', 'like', '%' . $location .  '%');/
            
             $data->where(function($q) use($location){
                $q->where('location_name', 'like', '%' . $location . '%')
                    ->orWhere('country', 'like', '%' . $location . '%');
            }); 
            */
            $max = 10000;

            if($request->input('nearby'))
            {
                $max = 20000;
            }
         

                  if($request->latitude !==''  && $request->longitude !=='')
                {
                      $latitude = (float)$request->latitude;
                      $longitude = (float)$request->longitude;
                        
                        $data->where('location', 'near', [
                            '$geometry' => [    
                                'type' => 'Point',
                                'coordinates' => [
                                    $longitude, // longitude
                                    $latitude, // latitude
                                ],
                            ],
                            '$maxDistance' => $max,
                        ]); 


                }else
                {

                    $location = strtolower($request->input('location')) ;
                    
                    $data->where(function($q)use($location){
                        $q->where('title', 'like', '%' . $location. '%')
                            ->orWhere('owner_locations', 'like', '%' . $location. '%')
                            ->orWhere('country', 'like', '%' . $location. '%')
                            ->orWhere('city', 'like', '%' . $location. '%')
                            ->orWhere('location_name', 'like', '%' . $location. '%');
                    });
                }
        }

        if($request->filled('house_type')){
            $data->where('house_type', 'like', '%' . $request->input('house_type'));
        }

        if($request->filled('capacity')){
            if($request->input('capacity') == "10+")
            {
                $data->where('total_accommodation','>=', 10);
            }else{
                $data->where('total_accommodation','>=', (int)$request->input('capacity'));
            }
            
        }

        if($request->filled('start_date') && $request->filled('end_date'))
        {
            $data->whereBetween('availability', [$request->input('start_date'), $request->input('end_date')]);
        }else
        {
            $start_date = \Carbon\Carbon::now()->format('Y-m-d');
            $end_date   = \Carbon\Carbon::now()->addDays(30)->format('Y-m-d');
            $data->whereBetween('availability', [$start_date, $end_date]);
             

        } 


        $data = $data->paginate(12);
          
        $map_data = $data;
        $latlongData=self::getCospaceLongLat($map_data);
        if($request->ajax()){
            return view('frontend.property-listing.swap.list', compact('data','map_data','latlongData'));
        }
        return view('frontend.property-listing.swap.index', compact('data','map_data','latlongData'));
    }

    


    /**
    * get property details based on buy or rent
    */
    public function propertyDetails(Request $request){
        $id         = request()->segment(5);
        $loc        = request()->segment(3);
        $type       = request()->segment(4);

        $loc=request()->segment(3);
        if($loc == 'ksa' || $loc == 'uae' )
        {
           $loc21 = strtoupper($loc);
        }else 
        {
            $loc21 = ucfirst($loc);
        }
        $loc = $loc21;

        if($type=='buy'){
            /*if(\Auth::user()){
                $myProperty=Property::where('is_approved',1)->where('_id','!=',$id)->where('listing_type','sale')->where('user_id','!=',\Auth::user()->id)->where('country',$loc)->limit(4)->get();
                }else{
                    $myProperty=Property::where('is_approved',1)->where('_id','!=',$id)->where('listing_type','sale')->where('country',$loc)->limit(4)->get();
                }*/




          $ratings=Review::where('is_approved',1)->where('review_type',"1")->where('type_id',$id)->get();
          $totaluser=Review::where('is_approved',1)->where('review_type',"1")->where('type_id',$id)->count();

          $totalrating=$totaluser*5;
          if($totalrating == 0){
            $totalrating=1;
          }
          $ratinggiven1=Review::where('is_approved',1)->where('review_type',"1")->where('type_id',$id)->sum('hygine');
          $avghy=number_format($ratinggiven1/$totalrating*5);
          $ratinggiven2=Review::where('is_approved',1)->where('review_type',"1")->where('type_id',$id)->sum('service');
          $avgser=number_format($ratinggiven2/$totalrating*5);
          $ratinggiven3=Review::where('is_approved',1)->where('review_type',"1")->where('type_id',$id)->sum('location');
          $avgloc=number_format($ratinggiven3/$totalrating*5);
          $ratinggiven4=Review::where('is_approved',1)->where('review_type',"1")->where('type_id',$id)->sum('cleanliness');
          $avgcle=number_format($ratinggiven4/$totalrating*5);
          $ratinggiven5=Review::where('is_approved',1)->where('review_type',"1")->where('type_id',$id)->sum('atmosphere');
          $avgatm=number_format($ratinggiven5/$totalrating*5);
          $ratinggiven6=Review::where('is_approved',1)->where('review_type',"1")->where('type_id',$id)->sum('average');
          $avgtotal=number_format($ratinggiven6/$totalrating*5);
            $data = Property::where('_id', $id)->firstOrFail();




             /*if(\Auth::user()){
                $myProperty=Property::where('is_approved',1)->where('_id','!=',$id)->where('listing_type','sale')->where('user_id','!=',\Auth::user()->id)->where('country',$loc)->limit(4)->get();
                }else{
                    $myProperty=Property::where('is_approved',1)->where('_id','!=',$id)->where('listing_type','sale')->where('country',$loc)->limit(4)->get();
                }*/

             if(\Auth::user()){
            $myPropertyQuery=Property::where('is_approved',1)->where('_id','!=',$id)->where('listing_type','sale')->where('user_id','!=',\Auth::user()->id)->where('country',$loc);

              $max = 10000;
               if($data->latitude && $data->longitude)
                {

                    /*$myPropertyQuery->whereBetween('age', [$ageFrom, $ageTo]);*/
                     $latitude = (float)$data->latitude;
                    $longitude = (float)$data->longitude;
                    $myPropertyQuery->where('location', 'near', [
                        '$geometry' => [
                            'type' => 'Point',
                            'coordinates' => [
                                $longitude, // longitude
                                $latitude, // latitude
                            ],
                        ],
                        '$maxDistance' => $max,
                    ]);
                }

             }else{
                $myPropertyQuery=Property::where('is_approved',1)->where('_id','!=',$id)->where('listing_type','sale')->where('country',$loc);
                $max = 10000;
               if($data->latitude && $data->longitude)
                {
                     $latitude = (float)$data->latitude;
                    $longitude = (float)$data->longitude;
                    $myPropertyQuery->where('location', 'near', [
                        '$geometry' => [
                            'type' => 'Point',
                            'coordinates' => [
                                $longitude, // longitude
                                $latitude, // latitude
                            ],
                        ],
                        '$maxDistance' => $max,
                    ]);
                }

 
            } 
            $listingg_type = $type;
             $myProperty= $myPropertyQuery->limit(4)->get();

            return view('frontend.property-details.buy.index', compact('data','ratings','avghy','avgtotal','avgatm','avgcle','avgloc','avgser','myProperty','listingg_type'));
        }elseif ($type=='rent') {

            $data = Property::where('_id', $id)->firstOrFail();

            if(\Auth::user()){
            $myPropertyQuery=Property::where('is_approved',1)->where('_id','!=',$id)->where('listing_type','rent')->where('user_id','!=',\Auth::user()->id)->where('country',$loc);

              $max = 10000;
               if($data->latitude && $data->longitude)
                {

                    /*$myPropertyQuery->whereBetween('age', [$ageFrom, $ageTo]);*/
                     $latitude = (float)$data->latitude;
                    $longitude = (float)$data->longitude;
                    $myPropertyQuery->where('location', 'near', [
                        '$geometry' => [
                            'type' => 'Point',
                            'coordinates' => [
                                $longitude, // longitude
                                $latitude, // latitude
                            ],
                        ],
                        '$maxDistance' => $max,
                    ]);
                }

             }else{
                $myPropertyQuery=Property::where('is_approved',1)->where('_id','!=',$id)->where('listing_type','rent')->where('country',$loc);
                $max = 10000;
               if($data->latitude && $data->longitude)
                {
                     $latitude = (float)$data->latitude;
                    $longitude = (float)$data->longitude;
                    $myPropertyQuery->where('location', 'near', [
                        '$geometry' => [
                            'type' => 'Point',
                            'coordinates' => [
                                $longitude, // longitude
                                $latitude, // latitude
                            ],
                        ],
                        '$maxDistance' => $max,
                    ]);
                }

 
            } 
             $myProperty= $myPropertyQuery->limit(4)->get();


           
            
            $ratings=Review::where('is_approved',1)->where('review_type',"2")->where('type_id',$id)->get();
            $totaluser=Review::where('is_approved',1)->where('review_type',"2")->where('type_id',$id)->count();

            $totalrating=$totaluser*5;
            if($totalrating == 0){
              $totalrating=1;
            }
            
            $ratinggiven1=Review::where('is_approved',1)->where('review_type',"2")->where('type_id',$id)->sum('hygine');
            $avghy=number_format($ratinggiven1/$totalrating*5);
            $ratinggiven2=Review::where('is_approved',1)->where('review_type',"2")->where('type_id',$id)->sum('service');
            $avgser=number_format($ratinggiven2/$totalrating*5);
            $ratinggiven3=Review::where('is_approved',1)->where('review_type',"2")->where('type_id',$id)->sum('location');
            $avgloc=number_format($ratinggiven3/$totalrating*5);
            $ratinggiven4=Review::where('is_approved',1)->where('review_type',"2")->where('type_id',$id)->sum('cleanliness');
            $avgcle=number_format($ratinggiven4/$totalrating*5);
            $ratinggiven5=Review::where('is_approved',1)->where('review_type',"2")->where('type_id',$id)->sum('atmosphere');
            $avgatm=number_format($ratinggiven5/$totalrating*5);
            $ratinggiven6=Review::where('is_approved',1)->where('review_type',"2")->where('type_id',$id)->sum('average');
            $avgtotal=number_format($ratinggiven6/$totalrating*5);
            $listingg_type = $type;
            return view('frontend.property-details.rent.index', compact('data','ratings','avghy','avgtotal','avgatm','avgcle','avgloc','avgser','myProperty','listingg_type'));
        }elseif ($type=='detail') {
            if(\Auth::user()){


            $myProperty=Property::where('is_approved',1)->where('_id','!=',$id)->where('user_id','!=',\Auth::user()->id)->where('country',$loc)->limit(4)->get();



            }else{

                $myProperty=Property::where('is_approved',1)->where('_id','!=',$id)->where('country',$loc)->limit(4)->get();

            }
          $ratings=Review::where('is_approved',1)->where('review_type',"5")->where('type_id',$id)->get();
          $totaluser=Review::where('is_approved',1)->where('review_type',"5")->where('type_id',$id)->count();

          $totalrating=$totaluser*5;
          if($totalrating == 0){
            $totalrating=1;
          }
          $ratinggiven1=Review::where('is_approved',1)->where('review_type',"5")->where('type_id',$id)->sum('hygine');
          $avghy=number_format($ratinggiven1/$totalrating*5);
          $ratinggiven2=Review::where('is_approved',1)->where('review_type',"5")->where('type_id',$id)->sum('service');
          $avgser=number_format($ratinggiven2/$totalrating*5);
          $ratinggiven3=Review::where('is_approved',1)->where('review_type',"5")->where('type_id',$id)->sum('location');
          $avgloc=number_format($ratinggiven3/$totalrating*5);
          $ratinggiven4=Review::where('is_approved',1)->where('review_type',"5")->where('type_id',$id)->sum('cleanliness');
          $avgcle=number_format($ratinggiven4/$totalrating*5);
          $ratinggiven5=Review::where('is_approved',1)->where('review_type',"5")->where('type_id',$id)->sum('atmosphere');
          $avgatm=number_format($ratinggiven5/$totalrating*5);
          $ratinggiven6=Review::where('is_approved',1)->where('review_type',"5")->where('type_id',$id)->sum('average');
          $avgtotal=number_format($ratinggiven6/$totalrating*5);
            $data = Property::where('_id', $id)->firstOrFail();
            $listingg_type = $type;
            return view('frontend.property-details.static.index', compact('data','ratings','avghy','avgtotal','avgatm','avgcle','avgloc','avgser','myProperty','listingg_type'));
        }elseif ($type=='coworkspace') {

          $ratings=Review::where('is_approved',1)->where('review_type',"4")->where('type_id',$id)->get();
          $totaluser=Review::where('is_approved',1)->where('review_type',"4")->where('type_id',$id)->count();

          $totalrating=$totaluser*5;
          if($totalrating == 0){
            $totalrating=1;
          }
          $ratinggiven1=Review::where('is_approved',1)->where('review_type',"4")->where('type_id',$id)->sum('hygine');
          $avghy=number_format($ratinggiven1/$totalrating*5);
          $ratinggiven2=Review::where('is_approved',1)->where('review_type',"4")->where('type_id',$id)->sum('service');
          $avgser=number_format($ratinggiven2/$totalrating*5);
          $ratinggiven3=Review::where('is_approved',1)->where('review_type',"4")->where('type_id',$id)->sum('location');
          $avgloc=number_format($ratinggiven3/$totalrating*5);
          $ratinggiven4=Review::where('is_approved',1)->where('review_type',"4")->where('type_id',$id)->sum('cleanliness');
          $avgcle=number_format($ratinggiven4/$totalrating*5);
          $ratinggiven5=Review::where('is_approved',1)->where('review_type',"4")->where('type_id',$id)->sum('atmosphere');
          $avgatm=number_format($ratinggiven5/$totalrating*5);
          $ratinggiven6=Review::where('is_approved',1)->where('review_type',"4")->where('type_id',$id)->sum('average');
          $avgtotal=number_format($ratinggiven6/$totalrating*5);

         $data = CoworkspaceProperty::where('is_approved',1)->where('_id', $id)->firstOrFail();
          

         if(\Auth::user()){
                $myProperty=CoworkspaceProperty::where('is_approved',1)->where('_id','!=',$id)->where('country',$loc)->where('user_id','!=',\Auth::user()->id);


                }else{
                    $myProperty=CoworkspaceProperty::where('is_approved',1)->where('_id','!=',$id)->where('country',$loc);
                }
                 $max = 10000;
                 if(@$data->latitude && @$data->longitude)
                {
                     $latitude = (float)$data->latitude;
                    $longitude = (float)$data->longitude;
                    $myProperty->where('location', 'near', [
                        '$geometry' => [
                            'type' => 'Point',
                            'coordinates' => [
                                $longitude, // longitude
                                $latitude, // latitude
                            ],
                        ],
                        '$maxDistance' => $max,
                    ]);
                }
                $listingg_type = $type;
                $myProperty= $myProperty->limit(4)->get();

                return view('frontend.property-details.coworkspace.index', compact('data','ratings','avghy','avgtotal','avgatm','avgcle','avgloc','avgser','myProperty','listingg_type'));
                
 
        }elseif ($type=='swap') {
           /* $myProperty=SwapProperty::where('is_approved',1)->where('_id','!=',$id)->where('country',$loc)->limit(4)->get();*/
            $ratings=Review::where('is_approved',1)->where('review_type',"3")->where('type_id',$id)->get();
            $totaluser=Review::where('is_approved',1)->where('review_type',"3")->where('type_id',$id)->count();

          $totalrating=$totaluser*5;
          if($totalrating == 0){
            $totalrating=1;
          }
          $ratinggiven1=Review::where('is_approved',1)->where('review_type',"3")->where('type_id',$id)->sum('hygine');
          $avghy=number_format($ratinggiven1/$totalrating*5);
          $ratinggiven2=Review::where('is_approved',1)->where('review_type',"3")->where('type_id',$id)->sum('service');
          $avgser=number_format($ratinggiven2/$totalrating*5);
          $ratinggiven3=Review::where('is_approved',1)->where('review_type',"3")->where('type_id',$id)->sum('location');
          $avgloc=number_format($ratinggiven3/$totalrating*5);
          $ratinggiven4=Review::where('is_approved',1)->where('review_type',"3")->where('type_id',$id)->sum('cleanliness');
          $avgcle=number_format($ratinggiven4/$totalrating*5);
          $ratinggiven5=Review::where('is_approved',1)->where('review_type',"3")->where('type_id',$id)->sum('atmosphere');
          $avgatm=number_format($ratinggiven5/$totalrating*5);
          $ratinggiven6=Review::where('is_approved',1)->where('review_type',"3")->where('type_id',$id)->sum('average');
          $avgtotal=number_format($ratinggiven6/$totalrating*5);

            $data = SwapProperty::where('is_approved',1)->where('_id', $id)->firstOrFail();

            /*$myProperty=SwapProperty::where('is_approved',1)->where('_id','!=',$id)->where('country',$loc)->limit(4)->get();*/
            if(\Auth::user()){
                $myProperty=SwapProperty::where('is_approved',1)->where('_id','!=',$id)->where('country',$loc)->where('user_id','!=',\Auth::user()->id);


                }else{
                    $myProperty=SwapProperty::where('is_approved',1)->where('_id','!=',$id)->where('country',$loc);
                }
                 $max = 10000;
                 if(@$data->latitude && @$data->longitude)
                {
                     $latitude = (float)$data->latitude;
                    $longitude = (float)$data->longitude;
                    $myProperty->where('location', 'near', [
                        '$geometry' => [
                            'type' => 'Point',
                            'coordinates' => [
                                $longitude, // longitude
                                $latitude, // latitude
                            ],
                        ],
                        '$maxDistance' => $max,
                    ]);
                }

                $myProperty = $myProperty->limit(4)->get();


            $availability = [];
            if(!is_array($data->availability)){
                $availability = [];
            }else{
                $availability = $data->availability;
            }

            $listingg_type = $type;
            return view('frontend.property-details.swap.index', compact('data','availability','ratings','avghy','avgtotal','avgatm','avgcle','avgloc','avgser','myProperty','listingg_type'));
        }else{
         return redirect(app()->getLocale().'/404');
        }
    }

    /**
    * get buddy user details
    **/
    public function getBuddyDetails(Request $request){


      $currentURL = url()->current();
      Session::put('previous_url',$currentURL);
      $id = request()->segment(4);
      $ratings=Review::where('is_approved',1)->where('review_type',"7")->where('type_id',$id)->orderBy('created_at','DESC')->get();
      $totaluser=Review::where('is_approved',1)->where('review_type',"7")->where('type_id',$id)->count();
        $specials=Specialization::where('for','2')->limit(3)->get();
      $totalrating=$totaluser*5;
      if($totalrating == 0){
        $totalrating=1;
      }
      $awards = UserAward::where('user_id',$id)->get();
      $ratinggiven1=Review::where('is_approved',1)->where('review_type',"7")->sum('hygine');
      $avghy=number_format($ratinggiven1/$totalrating*5);
      $ratinggiven2=Review::where('is_approved',1)->where('review_type',"7")->sum('service');
      $avgser=number_format($ratinggiven2/$totalrating*5);
      $ratinggiven3=Review::where('is_approved',1)->where('review_type',"7")->sum('location');
      $avgloc=number_format($ratinggiven3/$totalrating*5);
      $ratinggiven4=Review::where('is_approved',1)->where('review_type',"7")->sum('cleanliness');
      $avgcle=number_format($ratinggiven4/$totalrating*5);
      $ratinggiven5=Review::where('is_approved',1)->where('review_type',"7")->sum('atmosphere');
      $avgatm=number_format($ratinggiven5/$totalrating*5);
      $ratinggiven6=Review::where('is_approved',1)->where('review_type',"7")->where('type_id',$id)->sum('average');
      $avgtotal=number_format($ratinggiven6/$totalrating*5);
      $id = request()->segment(4);
      $data = User::where('_id', $id)->with(['qualifications','addresses','profiles','professionals'=>function($q){
         $q->where('role', 'BUDDY');
      }])->whereIn('role',['BUDDY'])->first(); 

 

      $connections = UserEnquiry::where('user_id',$id)->where('type','buddy')->groupBy('sender_id')->get();
      return view('frontend.users-details.buddies.index', compact('awards','data','ratings','avghy','avgtotal','avgatm','avgcle','avgloc','avgser','id','specials','totaluser','connections'));
    }

    /**
    * get agent user details
    **/
    public function getAgentDetails(Request $request){

        $currentURL = url()->current();
        Session::put('previous_url',$currentURL);
        $id = request()->segment(4);
      $ratings=Review::where('is_approved',1)->where('review_type',"6")->where('type_id',$id)->orderBy('created_at','DESC')->get();
      $totaluser=Review::where('is_approved',1)->where('review_type',"6")->where('type_id',$id)->count();
      $specials=Specialization::where('for','1')->limit(3)->get();
        $myProperty=Property::where('is_approved',1)->where('user_id',$id)->where('listing_type', 'like', '%' . 'both')->get();
      $totalrating=$totaluser*5;
      if($totalrating == 0){
        $totalrating=1;
      }


       
      $awards = UserAward::where('user_id',$id)->get();
      $ratinggiven1=Review::where('is_approved',1)->where('review_type',"6")->sum('hygine');
      $avghy=number_format($ratinggiven1/$totalrating*5);
      $ratinggiven2=Review::where('is_approved',1)->where('review_type',"6")->sum('service');
      $avgser=number_format($ratinggiven2/$totalrating*5);
      $ratinggiven3=Review::where('is_approved',1)->where('review_type',"6")->sum('location');
      $avgloc=number_format($ratinggiven3/$totalrating*5);
      $ratinggiven4=Review::where('is_approved',1)->where('review_type',"6")->sum('cleanliness');
      $avgcle=number_format($ratinggiven4/$totalrating*5);
      $ratinggiven5=Review::where('is_approved',1)->where('review_type',"6")->sum('atmosphere');
      $avgatm=number_format($ratinggiven5/$totalrating*5);
      $ratinggiven6=Review::where('is_approved',1)->where('review_type',"6")->where('type_id',$id)->sum('average');
      $avgtotal=number_format($ratinggiven6/$totalrating*5);
      $id = request()->segment(4);
        
        $data = User::where('_id', $id)->with(['qualifications','profiles','professionals'=>function($q){
        $q->where('role', 'AGENT');

      }])->whereIn('role',['AGENT'])->first();
 
        
      return view('frontend.users-details.agents.index', compact('awards','data','ratings','avghy','avgtotal','avgatm','avgcle','avgloc','avgser','myProperty','id','specials','totaluser'));
    }

    public function sendEnquiry(Request $request){
      $today=date('Y-m-d');
      $enquiry=new UserEnquiry;
      $enquiry->name=@$request->name;
      $enquiry->email=@$request->email;
      $enquiry->phone_code=@$request->code;
      $enquiry->phone=@$request->phone;
      $enquiry->message=@$request->message;
      $enquiry->property_id=@$request->property;
      $enquiry->create=$today;
      $enquiry->is_action=0;
      $enquiry->type=@$request->type;
      $enquiry->start_date=@$request->startDate;
      $enquiry->start_time=@$request->endDate;
      $enquiry->user_id=@$request->touser;
      $enquiry->sender_id=\Auth::user()->id;
      if(@$request->type == 'swap'){
          $enquiry->name= \Auth::user()->name;
          $enquiry->email= \Auth::user()->email;
          $enquiry->phone_code = \Auth::user()->countryCode;
          $enquiry->phone= \Auth::user()->phone;
      }
      $enquiry->save();
      $newNot = new Notification;
      $newNot->receiver_id = @$request->touser;
      $newNot->sender_id = \Auth::user()->id;
      $newNot->message = 'You got new Enquiry';
      $newNot->read = (int)0;
      $newNot->save();
      $message = 'Your inspection request is submitted!!';
      Mail::to($enquiry->reciever->email)->cc('info@thegulfroad.com')->send(new Enquiry($enquiry));
      return response()->json(['success'=>true, 'message' => $message]);
    }

    public function sendReview(Request $request){
        
      $today=date('Y-m-d');
      $total=$request->hygine+$request->service+$request->location+$request->cleanliness+$request->atmosphere;
      $avg=number_format($total/25*5);
      $review=new Review;
      $review->user_id=\Auth::user()->id;
      $review->hygine=(int)$request->hygine;
      $review->service=(int)$request->service;
      $review->location=(int)$request->location;
      $review->cleanliness=(int)$request->cleanliness;
      $review->atmosphere=(int)$request->atmosphere;
      $review->review=$request->review;
      $review->review_type=$request->review_type;
      $review->type_id=$request->type_id;
      $review->is_approved=0;
      $review->average=(int)$avg;
      $review->create=$today;
      $review->save();
      return response()->json(['success'=>1,'msg'=>'Your reviews/ratings are successfully posted. After Your review moderation review will publish']);
    }

    public function agentReview(Request $request){
        $today=date('Y-m-d');
        $review=new Review;
        $review->user_id=Auth::user()->id;
        $review->average=(int)$request->rating;
        $review->review=$request->review;
        $review->review_type=$request->review_type;
        $review->type_id=$request->type_id;
        $review->is_approved=0;
        $review->create=$today;
        $review->save();
        $getAll=Review::where('type_id',$request->type_id)->sum('average');
        
        $totalRate=Review::where('type_id',$request->type_id)->count();
        $totalratting=$totalRate*5;
        
        $avg=($getAll/($totalratting))*$totalRate;
        // dd('fvf'.$getAll.'fdvfdvdfvfd'.$totalratting.'dfvdfv'.$avg);
        $update=\DB::table('users')
        ->where('_id',$request->type_id)
        ->update(array(
            'rating'=>$avg
        ));

        return response()->json(['success'=>1,'msg'=>'Review added successfully. After Your review moderation review will publish']);
      }

    public function enquiry_action(){
      $update=\DB::table('user_enquiries')
      ->where('_id',request()->segment(3))
      ->update(array('is_action'=>1));
      return back();
    }

    public function enquiry_action_one(){
      $update=\DB::table('user_enquiries')
      ->where('_id',request()->segment(3))
      ->update(array('is_action'=>2));
      return back();
    }

    public function agentBook(Request $request){
        $getEmail=User::where('_id',$request->type_id)->first();
        
        // Mail::send([], [], function ($message) use($request,$getEmail) {

        //     $message->to($getEmail->email)
        //       ->subject($request->content)

        //       ->setBody('Hi, user('.$request->name.') wants to be in touch with you');

        //   });
          return response()->json(['success'=>1,'msg'=>'successfully','status'=>1]);
    }

    public function getLongLat(Request $request)
    {
        $lon1   = $request->longitudeFrom; 
        $lon2  = $request->longitudeTo; 
        $lat1  = $request->latitudeFrom; 
        $lat2  = $request->latitudeTo; 

        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            $km = 0;
          }
          else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            
            
              $km=$miles*1.609344;
            
          }
        return response()->json(['distance'=>$km]); 
} 




private static function getAllLongLat($data){ 
    $location_data=[]; 
    foreach($data as $key=>$value){
     $location_data[]= [$value->property_title,$value->getLatLong()['lat'], $value->getLatLong()['lng'],$value->rent];
    }
    return $location_data;
 }


 private static function getCospaceLongLat($data){ 
    $location_data=[]; 
    foreach($data as $key=>$value){
     $location_data[]= [$value->title,$value->getLatLong()['lat'], $value->getLatLong()['lng'],$value->rent];
    }
    return $location_data;
 }

 public function favourate(Request $request){
    $check=Favourate::where('propert_id',$request->id)->where('user_id',\Auth::user()->id)->first();
    if(@$check){
        $message = 'Property removed from wishlist!!';
        $del=Favourate::where('propert_id',$request->id)->where('user_id',\Auth::user()->id)->delete();
    }else{
        $message = 'Property added to wishlist!!';
        $fav=new Favourate;
        $fav->user_id=\Auth::user()->id;
        $fav->propert_id=$request->id;
        $fav->save();
    }
    return response()->json(['success'=>1, 'message' => $message]);
 }

 public function myreview(){
    $ratings=Review::where('is_approved',1)->whereIn('review_type',["1","2","3","4","5"])->where('user_id',\Auth::user()->id)->get();
    return view('frontend.profile.review.index', compact('ratings'));
 }

 public function sendenquiry1(){
    return redirect()->back()->with('message', 'success');
 }

 public function contact_owner(Request $request){
    $ContactOwner=new ContactOwner;
    $ContactOwner->startdate=$request->startDate;
    $ContactOwner->enddate=$request->endDate;
    $ContactOwner->message=$request->message;
    $ContactOwner->type=$request->type;
    $ContactOwner->property_id=$request->property;
    $ContactOwner->user_id=\Auth::user()->id;
    $ContactOwner->save();
    return response()->json(['status'=>true]);
 }

 

 public function report_user(Request $request){
    $property = request()->segment(5);
    $type = request()->segment(4);
    $report_listing=new Report;
    $report_listing->property_id=$property;
    $report_listing->type=$type;
    $report_listing->reason=$request->reason??'';
    $report_listing->user_type=$request->user_type??'';
    $report_listing->message=$request->message??'';
    $report_listing->user_id=\Auth::user()->id;
    $report_listing->save();
    $message = "Your report has been submitted.";
    return response()->json(['success'=>true, 'message' => $message]);
 }

 public function newslatter(Request $request){

        

    $user_check = Newsletter::where('email', '=', $request->email)->first();
     if(empty($user_check))
     {
            $letter=new Newsletter;
            $letter->name=$request->name;
            $letter->email=$request->email;
            $letter->save();

            return redirect()->back()->with('success','You Have successfully Subscribe For News Letter !');

    
     }else
     {
            return redirect()->back()->with('error','You have Already Subscribe For News Letter!'); 
     }

    
     
 }

 public function subscribe(Request $request){
    $letter=new Subscribe;

   


    if(@$request->email)
    {
        $letter->email=strtolower($request->email);
    
    }else{
    
        $letter->email=Auth::user()->email;
    
    }
    $user_check = Subscribe::where('email', '=', $letter->email)->first();
     if(empty($user_check))
     {
            $letter->save();
            $message = 'Thank You. You have successfully subscribed !';
            @Mail::to($letter->email)->cc('info@thegulfroad.com')->send(new SubscribeEnquiry($letter));
            return redirect()->back()->with('success',$message); 

    
     }else
     {
            return redirect()->back()->with('success','Thank You. You have already subscribed !'); 
     }
    
 }

 public function homeloan(Request $request){
    $loan=new Homeloan;
    $loan->name=ucwords(strtolower($request->name));
    $loan->email=strtolower($request->email);
    $loan->phone=$request->phone_code.' '.$request->phone;
    $loan->content=$request->content1;
    $loan->save();
    $message = 'Thank you to submit your query. One of our representative will contact you.';
     @Mail::to($loan->email)->cc('info@thegulfroad.com')->send(new HomeLoanEnquiry($loan));
   // return response()->json(['success'=>true, 'message' => 'Thank you to submit your query. One of our representative will contact you.']);
    return response()->json(['success'=>true,'message' => $message]);
    //return redirect()->back()->with('message', 'success'); 
 }

 public function book_consult(Request $request){
    $today=date('Y-m-d');
    $Consultation=new Consultation;
    $Consultation->name=$request->name;
    $Consultation->email=$request->email;
    $Consultation->number=$request->number;
    $Consultation->startdate=$request->startDate;
    $Consultation->enddate=$request->endDate;
    $Consultation->type=$request->type;
    $Consultation->type_id=$request->type_id;
    $Consultation->content=$request->content;
    $Consultation->code=$request->code;
    $Consultation->user_id=\Auth::user()->id;
    $Consultation->save();
    return response()->json(['success'=>true]);
  }

  public function share(){
    $lang = request()->segment(1);
    $type = request()->segment(3);
    $type_id = request()->segment(4);
    return redirect()->to("https://api.whatsapp.com/send?text=Property Share "."https://dev.thegulfroad.com/".$lang."/property/".$type_id."/".$type);
  }

  public function postContactSend(Request $request){
      $enquiry=new ContactUs;
      $enquiry->name=$request->name;
      $enquiry->phone=$request->phone;
      $enquiry->email=$request->email;
      $enquiry->message=$request->message;
      $enquiry->phone_code=$request->phone_code;
      $enquiry->created_at=date('Y-m-d H:i:s');
      $enquiry->save();
      return response()->json(['success'=>true, 'message' => 'Thank you to submit your query. One of our representative will contact you.']);
  }

  public function myfav(){
      $user=Auth::user()->id;
      $favs=Favourate::where('user_id',$user)->get();
      return view('frontend.profile.favourate.index',compact('favs'));
  }

  public function Listpremium(Request $request){
         $user_id=\Auth::user()->id;
        $plan_id = $request->is_premium;
        if($plan_id)
        {
             $skuid              = $request->sku_id;
            $sku_base_decode    = base64_decode($skuid);
            $sku_json_decode    = json_decode($sku_base_decode);
            $amount             = $sku_json_decode->plan_amount;
            $plan_currency      = $sku_json_decode->currency;
            $number             = $sku_json_decode->number;


            $getPlans=Subscription::where('_id',$plan_id)->first();
            foreach($getPlans->sub_plan as $plans){

                if($plans['number']==$number){

                     
                    $validity=$plans['validity'];
                    $current = date("Y-m-d");
                    $expiry_date=date('Y-m-d', strtotime("+".$validity." days"));
                    $transaction = new Transaction;
                    $transaction->plan_id=$plan_id;
                    $transaction->user_id=$user_id;
                    $transaction->count=(int)$plans['count'];
                    $transaction->number=$plans['number'];
                    $transaction->expiry_date=$expiry_date;
                    $transaction->amount=$amount;
                    $transaction->currency=$plan_currency;
                    $transaction->validity=$plans['validity'];
                    $transaction->purchase=(int)1;
                    $transaction->listed=(int)0;
                    $transaction->purchase_date=$current;
                    $transaction->purchase_type='premium';
                    $transaction->save();
                     


                }  
            }
        }
        



      session(['is_premium'=>$request->is_premium]);
      return response()->json(['status'=>1]);
  }

  public function cost_calculate1(){
    $loc = request()->segment(3);
    $type = request()->segment(4);
    $id = request()->segment(5);
    if($type=='rent'){
        $result= \App\Models\Property::where('_id',$id)->first();
    }
    elseif($type=='buy'){
        $result= \App\Models\Property::where('_id',$id)->first();
    }
    elseif($type=='coworkspace'){
        $result= \App\Models\CoworkspaceProperty::where('_id',$id)->first();
    }
    elseif($type=='swap'){
        $result= \App\Models\SwapProperty::where('_id',$id)->first();
    }
    return view('frontend.property-details.common-data.calculate1',compact('result'));
  }

  public function cost_calculate2(){
    $loc = request()->segment(3);
    $type = request()->segment(4);
    $id = request()->segment(5);
    return view('frontend.property-details.common-data.calculate2');
  }
  public function cost_calculate3(Request $request){
    $data['loc'] = request()->segment(3);
    $data['type'] = request()->segment(4);
    $data['id'] = request()->segment(5);
    return view('frontend.property-details.common-data.calculate3',compact('data'));
  }

  public function enquirytouser(Request $request){
    $today=date('Y-m-d');
      $enquiry=new UserEnquiry;
      $enquiry->name=$request->name;
      $enquiry->email=$request->email;
      $enquiry->phone_code=$request->phone_code;
      $enquiry->phone=$request->number;
      $enquiry->message=$request->content;
      $enquiry->start_date=$request->startdate;
      $enquiry->start_time=$request->starttime;
      $enquiry->is_action=0;
      $enquiry->user_id=@$request->touser;
      $enquiry->property_id=@$request->property;
      $enquiry->sender_id=\Auth::user()->id;
      $enquiry->save();

        $newNot = new Notification;
        $newNot->receiver_id = @$request->touser;
        $newNot->sender_id = \Auth::user()->id;
        $newNot->message = 'You got new Enquiry';
        $newNot->read = (int)0;
        $newNot->save();

      return back();
  } 

  public function notifications(){
      $getAll= Notification::where('receiver_id',\Auth::user()->id)->orderby('_id','DESC')->get();
      $update=\DB::table('notifications')
      ->where('receiver_id',\Auth::user()->id)
      ->update(array(
          'read'=>(int)1
      ));
      return view('frontend.profile.notification.index',compact('getAll'));
  }

  public function compare(Request $request){
    $swap_id = request()->segment(6);
   $data = SwapProperty::where('_id', $swap_id)->first();
    $totaluser=Review::where('is_approved',1)->where('review_type',"3")->where('type_id',$swap_id)->count();
      $totalrating=$totaluser*5;
      if($totalrating == 0){
        $totalrating=1;
      }
      $ratinggiven6=Review::where('is_approved',1)->where('review_type',"3")->where('type_id',$swap_id)->sum('average');
      $avgtotal=number_format($ratinggiven6/$totalrating*5);
   $all_swaps = SwapProperty::where('is_approved',1)->where('_id','!=', $swap_id)->get();
   $compare = SwapProperty::where('_id',@$request->otherswap)->first();
   $totaluser1=Review::where('is_approved',1)->where('review_type',"3")->where('type_id',@$request->otherswap)->count();
      $totalrating1=$totaluser1*5;
      if($totalrating1 == 0){
        $totalrating1=1;
      }
      $ratinggiven61=Review::where('is_approved',1)->where('review_type',"3")->where('type_id',@$request->otherswap)->sum('average');
      $avgtotal1=number_format($ratinggiven61/$totalrating1*5);
    return view('frontend.property-details.swap.compare',compact('data','compare','all_swaps','avgtotal1','avgtotal'));
}

public function booking(Request $request){
    //$getAll= BookProperty::where('is_approved',1)->where('user_id',\Auth::user()->id)->get();
    $getAll= BookProperty::where('user_id',\Auth::user()->id)->get();

    return view('frontend.profile.booking.index',compact('getAll'));
}

public function booking2(Request $request){
    /*$getAll= BookProperty::where('is_approved',1)->where('touser',\Auth::user()->_id)->get();*/
        $getAll= BookProperty::where('touser',\Auth::user()->_id)->get();

    return view('frontend.profile.booking.booking',compact('getAll'));
}

public function clearnot(Request $request){
    $getAll= Notification::where('receiver_id',\Auth::user()->id)->delete();
    return back();
}

public function cospaceStory(Request $req,$lang,$id){

    $CospaceStory = CospaceStory::where('_id',$id)->first();

    return view('frontend.CospaceStory.cospaceStory',compact('CospaceStory'));
    
}

    public function stripeHandleGet()
    {
         return view('frontend.stripe.index');
    }
    
     public function stripeHandlePost(Request $request)
    {
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Charge::create ([
                "amount" => 100 * 150,
                "currency" => "inr",
                "source" => $request->stripeToken,
                "description" => "Making test payment." 
        ]);
  
        Session::flash('success', 'Payment has been successfully processed.');
          
        return back();
    }

}
