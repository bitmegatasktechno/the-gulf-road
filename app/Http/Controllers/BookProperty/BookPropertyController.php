<?php

namespace App\Http\Controllers\BookProperty;
use App\Models\BookProperty;
use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class BookPropertyController extends Controller{

	public function __construct(){
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
            return $next($request);
        });
    }

    public function postBookProperty(Request $request){
	    $bookProperty = new BookProperty;
	    $bookProperty->startdate=$request->startDate;
	    $bookProperty->enddate=$request->endDate;
	    $bookProperty->guest=@$request->capacity;
	    $bookProperty->type=$request->type;
	    $bookProperty->property_id=$request->property;
	    $bookProperty->touser=$request->touser;
	    $bookProperty->user_id=\Auth::user()->id;
	    $bookProperty->save();

	    $newNot = new Notification;
	    $newNot->receiver_id = @$request->touser;
	    $newNot->sender_id = \Auth::user()->id;
	    $newNot->message = 'You got new Booking';
	    $newNot->read = (int)0;
	    $newNot->save();
	    return response()->json(['status'=> 'success', 'message' => 'Thank You. We have received your booking.']);
 	}
}
