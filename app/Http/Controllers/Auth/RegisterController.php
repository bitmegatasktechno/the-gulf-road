<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\{
    Country, User, Role
};
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function redirectTo(){
        return app()->getLocale() . '/';
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $countries = Country::all();

        return view('frontend.auth.register', compact('countries'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        Validator::extend('special_password', function($attribute, $value, $parameters){
            return preg_match('/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/', $value);
        });

        return Validator::make($data, [

            'name' => 'bail|required|string|max:255',
            'email' => 'bail|required|email|max:255|unique:users|regex:/(.+)@(.+)\.(.+)/i',
            'password' => 'bail|required|special_password|min:8',
            'password_confirmation' => 'bail|required|special_password|same:password',
            'country' => 'bail|required|string',
            'phone_code' => 'bail|required|string',
            'phone' => 'bail|required|numeric|regex:/[1-9]{1}[0-9]{5,14}/|digits_between:6,15',
        ],
        [

        	'password.special_password'=>'The password must be atleast 8 characters long, should contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character.',
        	'password_confirmation.special_password'=>'The confirm password must be atleast 8 characters long, should contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character.',
            'phone.digits_between' => 'Phone Number must be in between 6 to 15 digits'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => strtolower($data['email']),
            'password' => Hash::make($data['password']),
            'country' => $data['country'],
            'countryCode' => $data['phone_code'],
            'phone' => $data['phone'],
            'role' => ['USER'],
            'image' => '',
            'provider_id' => '',
            'provider' => '',
            'registerVia' => 'web',
            'defaultLoginRole' => 'USER',
            'status' => 1,
            'email_token' => base64_encode($data['email']),
            'email_verified'=>0,
            'top_list' => []
        ]);

        User::sendAccountActivationMail($user->email,$user->email_token);

        return $user;
        //return redirect(app()->getLocale() . '/skip');
        //return response()->json(['status'=>true,'message'=>'Your Account has been created successfully.'],200);
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        Auth::logout();
        
        return $request->json()
            ? response()->json(['status'=>true,'message'=>'We just send a confirmation link to your email. Follow the link to verify your account.'],200)
            : redirect($this->redirectPath());
    }
}
