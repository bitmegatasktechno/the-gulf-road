<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth,Session;
use Illuminate\Http\Request;
use App\Rules\{
    EmailVerifiedUser, ActiveUserOnly, IsUserPasswordAgainstEmail
};
use App\Models\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function validateLogin(Request $request)
    {
        $request->validate([
            'email' => ['bail','required','string','email','exists:users', new EmailVerifiedUser, new ActiveUserOnly],
            'password' => 'bail|required|string|min:8',
        ],
        [
            'email.exists'=>'The selected email does not exists.'
        ]);
    }

    public function validateLogin2(Request $request)
    {   
        $email = strtolower($request->email);
        
        $errors =[
            'password' => [new IsUserPasswordAgainstEmail($email)],
        ];
        $request->validate($errors);
    }

    public function showLoginForm()
    {
        return redirect('/');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);
        $this->validateLogin2($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $data = $this->attemptLogin($request);
        if(checkSubscription())
        {
            Session::put('check_login',true);
        }

        if(is_array($data)) {
            return response()->json($data, 400);
        }
        else {
            if ($data) {
                return $this->sendLoginResponse($request);
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
    *Function to validate user based on user's type and and user's email verified status.
    */
    protected function credentials(Request $request){
        return ['email' => strtolower($request->email), 'password' => $request->password];
    }

    protected function attemptLogin(Request $request)
    {
        $valid = Auth::validate($this->credentials($request));

        if($valid) {
            $user = User::where('email', $request->input('email'))->first();
            
            if($user->email_verified==null || $user->email_verified == '') {
                return ['message' => 'User email is not verified yet. Please verify your email address to login.'];
            }
            if($user->status == 0) {
                return ['message' => 'User is not activated. Please try to contact admin.'];
            }
        }

        return $this->guard()->attempt(
            $this->credentials($request), $request->has('remember')
        );
    }


    /**
    * Function to save user's selected courses after login.
    */
    
    protected function authenticated(Request $request){

        if ($request->ajax()) {
            if(auth()->user()->logged_first_time){
                // $url = url(app()->getLocale() . '/skip');
                return redirect()->intended($this->redirectPath());
                auth()->user()->logged_first_time = false;
                auth()->user()->save();
            }
            else{
                $url = url()->previous();
            }
            return response()->json(['redirect_to' => $url, 'message' => 'Logged in successfully.','status'=>true], 200);
        } 
        else {
            return redirect()->intended($this->redirectPath());
        }        
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }
}
