<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\Rules\{
    IsFrontEndUser, EmailVerifiedUser, ActiveUserOnly
};
use Illuminate\Support\Facades\Password;
use Carbon\Carbon;
use App\Models\User;
use DB;
use Illuminate\Support\Str;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    //use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
    }

    public function sendResetLinkEmail(Request $request)
    {
        $request->validate([
            'email' => ['bail','required','email','exists:users', new IsFrontEndUser, new EmailVerifiedUser, new ActiveUserOnly]
        ],
        [
            'email.exists'=>'The selected email does not exists.'
        ]);

        $token = Str::random(60);
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);

        //Get the token just created above
        User::sendResetEmail($request->email, $token);

        return response()->json(['status'=>true,'message'=>'We just send a confirmation link to your email. Follow the link to create your new password.'],200);
    }
}
