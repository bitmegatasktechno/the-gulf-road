<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\VerifiesEmails;
use App\Models\User;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        // $this->middleware('signed')->only('verify');
        // $this->middleware('throttle:6,1')->only('verify', 'resend');

    }

    /**
    * show all products based on filter data and master search
    */
    public function verifyEmail($token)
    {
        $user = User::where('email_token', $token)->first();
        if (!$user) {
            return view('email.email-error');
        }
        if ($user->email_verified == '' || $user->email_verified==null) {
            
            $user->email_verified = 1;
            $user->status = 1;
            $user->email_token = null;
            if ($user->save()) {
                return view('email.emailconfirm');
            }
        }else{
            return view('email.email-error');
        }
    }
}
