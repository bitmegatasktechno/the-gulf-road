<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\PropertyApproval;
use App\Models\{
    Property, CoworkspaceProperty, SwapProperty
};

class PropertyController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function index(Request $request, $slug){
       	$active = 'properties';
        $data = Property::orderBy('_id','desc')->with(['user'=>function($q){
                $q->select('_id','email');
            }]);
        // filter code
        $inputs = $request->all();
        if(isset($inputs['name'])){
            $data->where('property_title', 'like', '%' . $inputs['name'] . '%');
        }
        if(isset($inputs['filterHouseType'])){
            $data->where('house_type', 'like', '%' . $inputs['filterHouseType']);
        }
        if(isset($inputs['filterFurnishedType'])){
            $data->where('furnishing_type', 'like', '%' . $inputs['filterFurnishedType']);
        }
        if(isset($inputs['filterCountry'])){
            $data->where('country', 'like', '%' . $inputs['filterCountry']);
        }
        if(isset($inputs['approved'])){
            $data->where('is_approved', (int)$inputs['approved']);
        }
        if(isset($inputs['populated'])){
            $data->where('is_populated', (int)$inputs['populated']);
        }
        if($slug=='rent'){
            $subactive = 'rent';
            $data = $data->where('listing_type', $slug)->paginate(10);
            if($request->ajax()){
                return view('admin.properties.rent.listing', compact('data'));
            }
            return view('admin.properties.rent.index', compact('active','data','subactive'));
        }
        elseif ($slug=='sale') {
            $subactive = 'sale';
            $data = $data->where('listing_type', $slug)->paginate(10);
            if($request->ajax()){
                return view('admin.properties.sale.listing', compact('data'));
            }
            return view('admin.properties.sale.index', compact('active','data','subactive'));
        }
        elseif ($slug=='rentsale') {
            $subactive = 'rentsale';
            $data = $data->where('listing_type','both')->paginate(10);

            if($request->ajax()){
                return view('admin.properties.rentsale.listing', compact('data'));
            }
            return view('admin.properties.rentsale.index', compact('active','data','subactive'));
        }
        else{
            return redirect(404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Floor  $floor
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $slug, $id)
    {
        $active = 'properties';
        
        $data = Property::where('_id', $id)->firstOrFail();

        if($slug=='rent')
        {
            $subactive = 'rent';
            return view('admin.properties.rent.details.index', compact('active','data','subactive'));
        }

        elseif ($slug=='sale') 
        {
            $subactive = 'sale';
            return view('admin.properties.rent.details.index', compact('active','data','subactive'));
        }
        elseif ($slug=='rentsale') 
        {
            $subactive = 'rentsale';
            return view('admin.properties.rentsale.details.index', compact('active','data','subactive'));
        }
        else{
            return redirect(404);
        }
    }

    public function destroy(Request $request){
        $id  =  $request->id;
        $id = new \MongoDB\BSON\ObjectID($id);
        $data = Property::where('_id', $id)->first();
        if($data->delete()){
            return response()->json(['status'=>true,'message'=>'Property deleted successfully.'],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Property record not found. Please try again later.'],200);
        }   
    }

    public function premium(Request $request, $slug, $id){
        $active = 'properties';
        $check=Property::where('_id', $id)->first();
        if($check->is_premium==1){
        $updatedata = \DB::table('properties')
        ->where('_id', $id)
        ->update(array('is_premium'=>2));
        }else{
            $updatedata = \DB::table('properties')
        ->where('_id', $id)
        ->update(array('is_premium'=>1));
        }
        return back();
    }


    /**
    * coworkspace listing start here
    ***/
    public function listCoworkspaces(Request $request){
        $active = 'properties';
        $subactive = 'coworkspace';
        $data = CoworkspaceProperty::orderBy('_id','desc')->with(['user'=>function($q){
                $q->select('_id','email');
            }]);

        // filter code
        $inputs = $request->all();

        if(isset($inputs['name'])){
            $data->where('title', 'like', '%' . $inputs['name'] . '%');
        }
        if(isset($inputs['filterCountry'])){
            $data->where('country', 'like', '%' . $inputs['filterCountry']);
        }
        if(isset($inputs['approved'])){
            $data->where('is_approved', (int)$inputs['approved']);
        }
        if(isset($inputs['populated'])){
            $data->where('is_populated', (int)$inputs['populated']);
        }
        $data = $data->paginate(10);
       
        if($request->ajax()){
            return view('admin.properties.coworkspace.listing', compact('data'));
        }
        return view('admin.properties.coworkspace.index', compact('active','data','subactive'));
    }

    /**
    * return detail page of coworkspace
    */
    public function detailCoworkspace(Request $request, $id)
    {
        $active = 'properties';
        $data = CoworkspaceProperty::where('_id', $id)->firstOrFail();
        $subactive = 'coworkspace';
        if(is_null($data->accessibility_amenties))
        {
            $data->accessibility_amenties = [];
        }
        if(is_null($data->facilities_amenties))
        {
            $data->facilities_amenties = [];
        }
        if(is_null($data->equipments_amenties))
        {
            $data->equipments_amenties = [];
        }
        if(is_null($data->food_amenties))
        {
            $data->food_amenties = [];
        }
        // dd($data);
        return view('admin.properties.coworkspace.details.index', compact('active','data','subactive'));
    }

    public function destroyCoworkspace(Request $request){
        $id  =  $request->id;
        $id = new \MongoDB\BSON\ObjectID($id);
        $data = CoworkspaceProperty::where('_id', $id)->first();
        if($data->delete()){
            return response()->json(['status'=>true,'message'=>'Property deleted successfully.'],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Property record not found. Please try again later.'],200);
        }   
    }

    /**
    * swap listing start here
    ***/
    public function listSwapProperties(Request $request){
        $active = 'properties';
        $subactive = 'swap';
        $data = SwapProperty::orderBy('_id','desc')->with(['user'=>function($q){
                $q->select('_id','email');
            }]);

        // filter code
        $inputs = $request->all();

        if(isset($inputs['name'])){
            $data->where('title', 'like', '%' . $inputs['name'] . '%');
        }
        if(isset($inputs['filterCountry'])){
            $data->where('country', 'like', '%' . $inputs['filterCountry']);
        }
        if(isset($inputs['filterHouseType'])){
            $data->where('house_type', 'like', '%' . $inputs['filterHouseType']);
        }
        if(isset($inputs['capacity'])){
            if($inputs['capacity'] == "10+")
            {
                $data->where('total_accommodation','>', 10);
            }else{
                $data->where('total_accommodation', (int)$inputs['capacity']);
            }
        }
        if(isset($inputs['approved'])){
            $data->where('is_approved', (int)$inputs['approved']);
        }
        if(isset($inputs['populated'])){
            $data->where('is_populated', (int)$inputs['populated']);
        }
        $data = $data->paginate(10);
       
        if($request->ajax()){
            return view('admin.properties.swap.listing', compact('data'));
        }
        return view('admin.properties.swap.index', compact('active','data','subactive'));
    }

    /**
    * return detail page of swap
    */
    public function detailSwapProperties(Request $request, $id)
    {
        $active = 'properties';
        $data = SwapProperty::where('_id', $id)->firstOrFail();
        $subactive = 'swap';
        return view('admin.properties.swap.details.index', compact('active','data','subactive'));
    }

    public function destroySwap(Request $request){
        $id  =  $request->id;
        $id = new \MongoDB\BSON\ObjectID($id);
        $data = SwapProperty::where('_id', $id)->first();
        if($data->delete()){
            return response()->json(['status'=>true,'message'=>'Property deleted successfully.'],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Property record not found. Please try again later.'],200);
        }   
    }
    public function populate(Request $request, $slug, $id)
    {
        if($slug=='rent' || $slug=='sale' || $slug=='rentsale'){
            $update = Property::where('_id',$id)->update([
                'is_populated' => (int)$request->status
            ]);
        }elseif($slug=='swap')
        {
            $update = SwapProperty::where('_id',$id)->update([
                'is_populated' => (int)$request->status
            ]);
        }elseif($slug=='cospace')
        {
            $update=CoworkspaceProperty::where('_id',$id)->update([
                'is_populated' => (int)$request->status
            ]);
        }
        return response()->json(['success'=>1,'msg'=>'Property updated successfully']);
    }
    public function approve(Request $request, $slug, $id)
    {
        if($slug=='rent' || $slug=='sale' || $slug=='rentsale'){
            $update = Property::where('_id',$id)->update([
                'is_approved' => (int)$request->status
            ]);
            $property = Property::where('_id',$id)->first();
        }elseif($slug=='swap')
        {
            $update = SwapProperty::where('_id',$id)->update([
                'is_approved' => (int)$request->status
            ]);
            $property = SwapProperty::where('_id',$id)->first();
        }elseif($slug=='cospace')
        {
            $update=CoworkspaceProperty::where('_id',$id)->update([
                'is_approved' => (int)$request->status
            ]);
            $property = CoworkspaceProperty::where('_id',$id)->first();
        }
        if((int)$request->status == 1)
        {
            Mail::to($property->user->email)->send(new PropertyApproval($property));
        }
        return response()->json(['success'=>1,'msg'=>'Property updated successfully']);
    }
}
