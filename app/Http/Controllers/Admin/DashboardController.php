<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Rules\{
    IsCurrentPassword,AdminUserEmail
};
use Illuminate\Support\Facades\Hash;
use App\Models\{
    User, Admin,Property,CoworkspaceProperty,SwapProperty,BookProperty};

class DashboardController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }


    /**
    * Validate admin login email and password and redirect to dashboard else return back to login screen.
    */
    public function login(Request $request, $guard=null){
        $validatedData = $request->validate([
            'email' => ['bail','required','max:255','email','exists:admins',new AdminUserEmail],
            'password' => ['bail','required','min:8', new IsCurrentPassword],
        ]);
        
        // attempt to do the login
        $auth = auth('admin')->attempt(
            [
                'email' => strtolower($request->get('email')),
                'password' => $request->get('password')
            ]
        );

        if ($auth) {
            return redirect('admin/dashboard');
        }
        else {
            flash('Details are not validated, Please try again.')->error();
            return redirect()->back();
        }
    }


    /**
    * return dashboard view of admin side
    */
    public function index(){
        $active = 'dashboard';
        $subactive = 'dashboard';
        $data['users'] = User::whereIn('role',['USER'])->count();
        $data['agents'] = User::whereIn('role',['AGENT'])->count();
        $data['buddy'] = User::whereIn('role',['BUDDY'])->count();
        $data['buddy-requests'] = User::where('buddyProfile', 1)->count();
        $data['agent-requests'] = User::where('agentProfile', 1)->count();

        $allusers = User::orderBy('id','Desc')->limit(1000)->get();

        $lineGraphpData = array();
        foreach ($allusers as $key => $value) {
            foreach ($value->role as $role) {

                if($role == 'USER'){
                    $lineGraphpData[date('Y-m-d',strtotime($value->created_at))]['users'][] = $value;
                }

                if($role == 'AGENT'){
                    $lineGraphpData[date('Y-m-d',strtotime($value->created_at))]['agents'][] = $value;
                    
                }
                if($role == 'BUDDY'){
                    $lineGraphpData[date('Y-m-d',strtotime($value->created_at))]['buddies'][] = $value;
                }
            }
        }

        $rentCount = Property::where('listing_type','rent')->count();
        $buyCount = Property::where('listing_type','sale')->count();
        $rentBuyCount = Property::where('listing_type','both')->count();
        $swapCount = SwapProperty::count();
        $coworkCount = CoworkspaceProperty::count();

        $bookings = BookProperty::orderBy('id','Desc')->limit(1000)->get();
        $bookingDatewise = array();
        foreach ($bookings  as $key => $value) {
            $bookingDatewise[date('Y-m-d',strtotime($value->created_at))][] = $value;
        }

        return view('admin.dashboard.index',compact('coworkCount','swapCount','buyCount','rentCount','active','subactive','data','allusers','lineGraphpData','bookingDatewise','rentBuyCount'));
    }

    /**
    * logout admin user if logged in 
    */
    public function logout(){
        Auth::guard('admin')->logout();
        return redirect('admin/login');
    }

    /**
    * return profile of admin to change password
    */
    public function getProfile(){
        $active = 'dashboard';
        $subactive = 'dashboard';
        return view('admin.auth.profile',compact('active','subactive'));
    }

    /**
    * update admin password after validating current password
    */
    public function postProfile(Request $request){
        $validatedData = $request->validate([
            'old_password' => ['bail','required','min:8',new IsCurrentPassword],
            'password' => 'bail|required|min:8|same:password',
            'password_confirmation' => 'bail|required|min:8|same:password',     
        ]);

        $authId = isset(Auth::guard('admin')->user()->_id) ? Auth::guard('admin')->user()->_id: '';
       
        $authId = new \MongoDB\BSON\ObjectID($authId);// convert id to mongo based Object iD
        Admin::where('_id', $authId)->update(['password'=>Hash::make($request->password)]);

        flash('Password updated successfully. Please login with new credentials.')->success()->important();
        Auth::guard('admin')->logout();
        return redirect('admin/login');
    }

    public function cospaceStories(){
        $active = 'cospaceStories';
        $subactive = 'cospaceStories';
        return view('admin.cospaceStories.index',compact('active','subactive'));
    }

}
