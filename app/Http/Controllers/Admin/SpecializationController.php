<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Specialization;
use Illuminate\Validation\Rule;

class SpecializationController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function index(){
        $active = 'propertycms';
        $subactive = 'specialization';
        $data = Specialization::orderBy('_id','desc')->get();

        return view('admin.specialization.index', compact('active','data','subactive'));
    }

    public function create(){
        $active = 'propertycms';
        $subactive = 'specialization';

        return view('admin.specialization.create', compact('active','subactive'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'title'=>'bail|required|unique:specialization|max:50',
                'content'=>'bail|required',
                'for'=>'bail|required',
                'project'=>'bail|required',
                'logo' =>'required|image|mimes:webp,jpeg,png,jpg,gif,svg',
            ]
            
        );

        $name = null;
        $logo = $request->file('logo');
        $name = md5($logo->getClientOriginalName() . time()) . "." . $logo->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/speacialization');
        $logo->move($destinationPath, $name);

        $res = Specialization::create([
            'title' => request()->get('title'),
            'content' => request()->get('content'),
            'for' => request()->get('for'),
            'project' => request()->get('project'),
            'logo' => $name,
            'status' => (int)1
        ]);
        if($res){
            return response()->json(['status'=>true,'message'=>'Record created successfully.','url'=>url('admin/specialization')],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Something went wrong. Please try again'],200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HouseType  $housetype
     * @return \Illuminate\Http\Response
     */
    public function edit(Specialization $specialization)
    {
        $active = 'propertycms';
        $subactive = 'specialization';
        $data = $specialization;
        
        return view('admin.specialization.edit', compact('active','subactive','data'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $request->validate(
            [
                'title'=>['bail','required','max:50', Rule::unique('specialization')->ignore($id, '_id')],
                'logo' =>'bail|nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:max_width=55,max_height=55',
            ],
            [
                'logo.dimensions'=>'The logo height and width should be less than equal to 55 pixels.'
            ]
        );
        $data = Specialization::where('_id', $id)->first();
        if(!$data){
            return response()->json(['status'=>false,'message'=>'Record not found. Please try again later'],200);
        }
        $data->name = request()->get('name');

        if($request->file('logo')){
            $name = null;
            $logo = $request->file('logo');
            $name = md5($logo->getClientOriginalName() . time()) . "." . $logo->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/speacialization');
            $logo->move($destinationPath, $name);

            $old_path = public_path('upload/speacialization/'.$data->logo);
            
            if(\File::exists($old_path)){
                \File::delete($old_path);
            }

            $data->logo = $name;
            $data->title = request()->get('title');
            $data->content = request()->get('content');
            $data->project = request()->get('project');
            $data->for = request()->get('for');

        }

        $res = $data->save();
        if($res){
            return response()->json(['status'=>true,'message'=>'Record updated successfully.','url'=>url('admin/specialization')],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Something went wrong. Please try again'],200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HouseType  $housetype
     * @return \Illuminate\Http\Response
     */
    public function destroy(Specialization $specialization)
    {
        if($specialization->delete()){
            return response()->json(['status'=>true, 'message'=>'Record deleted successfully.'],200);
        }
         return response()->json(['status'=>false, 'message'=>'Record not deleted, Please try again later.'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function status(Request $request)
    {
        $obj = Specialization::where('_id', $request->id)->first();
        if(!$obj){
            return response()->json(['status'=>false, 'message'=>'Record not found, Please try again later.'],200);
        }
        $status = ($request->status==0) ? 0 : 1;
        $obj->status = $status;

        if($obj->save()){
            return response()->json(['status'=>true, 'message'=>'Record updated successfully.'],200);
        }
        return response()->json(['status'=>false, 'message'=>'Record not deleted, Please try again later.'],200);
    }
}
