<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use Illuminate\Validation\Rule;
use Intervention\Image\ImageManagerStatic as Image;

class BannerController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function index(){
        $banner = Banner::get();
        $active = "banner";
        $subactive = "banner";
        return view('admin.banner.index', compact('banner','active','subactive'));
    }

    public function addBanner(Request $request)
    {
        $request->validate(
            [
                'banner_file' =>'required|image|mimes:webp,jpeg,png,jpg',
            ]
        );

        $name = null;
        $banner_file = $request->file('banner_file');
        $name = md5($banner_file->getClientOriginalName() . time()) . "." . $banner_file->getClientOriginalExtension();

        $image_resize = Image::make($banner_file->getRealPath());
        $image_resize->save(public_path('/uploads/banner/' .$name));

        Banner::where('banner_type',$request->banner_type)->delete();

        $res = Banner::create([
            'banner_file' => $name,
            'banner_type' => $request->banner_type,
        ]);

        return redirect()->route('change_banner');
    }

}
