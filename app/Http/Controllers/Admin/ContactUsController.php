<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ContactUs;
use Illuminate\Validation\Rule;
use Intervention\Image\ImageManagerStatic as Image;

class ContactUsController extends Controller
{

    public function index(){
        $data = ContactUs::orderBy('_id','Desc')->get();
        $active = "contact";
        $subactive = "contact";
        return view('admin.contact.index', compact('data','active','subactive'));
    }

}
