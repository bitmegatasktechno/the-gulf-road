<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CompanyIcon;
use Illuminate\Validation\Rule;

class CompanyiconController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function index(){
        $active = 'companiesIcons';
        $subactive = 'companiesIcons';
        $data = CompanyIcon::orderBy('_id','desc')->get();

        return view('admin.companiesIcons.index', compact('active','data','subactive'));
    }

    public function create(){
        $active = 'companiesIcons';
        $subactive = 'companiesIcons';

        return view('admin.companiesIcons.create', compact('active','subactive'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'user'=>'required',
                'content'=>'required|max:50',
                'logo' =>'required|image|mimes:webp,jpeg,png,jpg,gif,svg',
            ]
            
        );

        $name = null;
        $logo = $request->file('logo');
        $name = md5($logo->getClientOriginalName() . time()) . "." . $logo->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/companiesIcons');
        $logo->move($destinationPath, $name);

        $res = CompanyIcon::create([
            'name' => request()->get('user'),
            'content' => request()->get('content'),
            'logo' => $name,
            'status' => (int)1
        ]);
        if($res){
            return response()->json(['status'=>true,'message'=>'Record created successfully.','url'=>url('admin/companiesIcons')],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Something went wrong. Please try again'],200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HouseType  $housetype
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyIcon $testimonial)
    {
        $active = 'companiesIcons';
        $subactive = 'companiesIcons';
        $data = $testimonial;
        
        return view('admin.companiesIcons.edit', compact('active','subactive','data'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $request->validate(
            [
                'logo' =>'bail|nullable|image|mimes:webp,jpeg,png,jpg,gif,svg|max:2048|dimensions:max_width=55,max_height=55',
            ],
            [
                'logo.dimensions'=>'The logo height and width should be less than equal to 55 pixels.'
            ]
        );
        $data = CompanyIcon::where('_id', $id)->first();
        if(!$data){
            return response()->json(['status'=>false,'message'=>'Record not found. Please try again later'],200);
        }
        $data->name = request()->get('name');

        if($request->file('logo')){
            $name = null;
            $logo = $request->file('logo');
            $name = md5($logo->getClientOriginalName() . time()) . "." . $logo->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/companiesIcons');
            $logo->move($destinationPath, $name);

            $old_path = public_path('upload/companiesIcons/'.$data->logo);
            
            if(\File::exists($old_path)){
                \File::delete($old_path);
            }

            $data->logo = $name;
            $data->name = request()->get('user');
            $data->content = request()->get('content');
           

        }

        $res = $data->save();
        if($res){
            return response()->json(['status'=>true,'message'=>'Record updated successfully.','url'=>url('admin/companiesIcons')],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Something went wrong. Please try again'],200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HouseType  $housetype
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyIcon $testimonial,$id)
    {
        if(CompanyIcon::where('_id', $id)->delete()){
            return response()->json(['status'=>true, 'message'=>'Record deleted successfully.'],200);
        }
         return response()->json(['status'=>false, 'message'=>'Record not deleted, Please try again later.'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function status(Request $request)
    {
        $obj = CompanyIcon::where('_id', $request->id)->first();
        if(!$obj){
            return response()->json(['status'=>false, 'message'=>'Record not found, Please try again later.'],200);
        }
        $status = ($request->status==0) ? 0 : 1;
        $obj->status = $status;

        if($obj->save()){
            return response()->json(['status'=>true, 'message'=>'Record updated successfully.'],200);
        }
        return response()->json(['status'=>false, 'message'=>'Record not deleted, Please try again later.'],200);
    }
}
