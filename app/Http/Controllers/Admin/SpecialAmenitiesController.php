<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{
	SpecialAmenity, AmenityCategory
};
use Illuminate\Validation\Rule;
use Image;

class SpecialAmenitiesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function index(){
        $active = 'propertycms';
        $subactive = 'specialamenities'; 
        $data = SpecialAmenity::orderBy('category_id','desc')->with(['category'])->get();

        return view('admin.specialamenities.index', compact('active','data','subactive'));
    }

    public function create(){
        $active = 'propertycms';
        $subactive = 'specialamenities';
        $categories = AmenityCategory::where('status', 1)->get();

        return view('admin.specialamenities.create', compact('active','subactive','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'name'=>'bail|required|unique:special_amenities|max:50',
                'category'=>'bail|required',
                'description'=>'bail|required|max:500',
                'logo' =>'required|image|mimes:webp,jpeg,png,jpg',
            ],
            [
            	// 'logo.dimensions'=>'The logo height and width should be more than equal to 300*130 pixels.'
            ]
        );

        $name = null;
        $logo = $request->file('logo');
        $name = md5($logo->getClientOriginalName() . time()) . "." . $logo->getClientOriginalExtension();

        $image_resize = Image::make($logo->getRealPath());
        $image_resize->resize(64, 64);
        $image_resize->save(public_path('/uploads/specialamenities/' .$name));

        $res = SpecialAmenity::create([
            'name' => strtolower(request()->get('name')),
            'category_id' => request()->get('category'),
            'description' => request()->get('description'),
            'logo' => $name,
            'status' => 1
        ]);
        if($res){
            return response()->json(['status'=>true,'message'=>'Record created successfully.','url'=>url('admin/specialamenities')],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Something went wrong. Please try again'],200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SpecialAmenity  $amenity
     * @return \Illuminate\Http\Response
     */
    public function show(SpecialAmenity $amenity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SpecialAmenity  $specialamenity
     * @return \Illuminate\Http\Response
     */
    public function edit(SpecialAmenity $specialamenity)
    {
        $active = 'propertycms';
        $subactive = 'specialamenities';
        $data = $specialamenity;
        $categories = AmenityCategory::where('status', 1)->get();

        return view('admin.specialamenities.edit', compact('active','subactive','data','categories'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $request->validate(
            [
                'name'=>['bail','required','max:50', Rule::unique('special_amenities')->ignore($id, '_id')],
                'category'=>'bail|required',
                'description'=>'bail|required|max:500',
                'logo' =>'bail|nullable|image|mimes:webp,jpeg,png,jpg',
            ],
            [
                // 'logo.dimensions'=>'The logo height and width should be more than equal to 300*130 pixels.'
            ]
        );
        $data = SpecialAmenity::where('_id', $id)->first();
        if(!$data){
            return response()->json(['status'=>false,'message'=>'Record not found. Please try again later'],200);
        }
        $data->name = strtolower(request()->get('name'));
        $data->category_id = request()->get('category');
        $data->description = request()->get('description');

        if($request->file('logo')){
            $name = null;
            $logo = $request->file('logo');
            $name = md5($logo->getClientOriginalName() . time()) . "." . $logo->getClientOriginalExtension();
            
            $image_resize = Image::make($logo->getRealPath());
            $image_resize->resize(64, 64);
            $image_resize->save(public_path('/uploads/specialamenities/' .$name));

            $old_path = public_path('upload/specialamenities/'.$data->logo);
            
            if(\File::exists($old_path)){
                \File::delete($old_path);
            }

            $data->logo = $name;
        }

        $res = $data->save();
        if($res){
            return response()->json(['status'=>true,'message'=>'Record updated successfully.','url'=>url('admin/specialamenities')],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Something went wrong. Please try again'],200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SpecialAmenity  $specialamenity
     * @return \Illuminate\Http\Response
     */
    public function destroy(SpecialAmenity $specialamenity)
    {
        if($specialamenity->delete()){
            return response()->json(['status'=>true, 'message'=>'Record deleted successfully.'],200);
        }
         return response()->json(['status'=>false, 'message'=>'Record not deleted, Please try again later.'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function status(Request $request)
    {
        $obj = SpecialAmenity::where('_id', $request->id)->first();
        if(!$obj){
            return response()->json(['status'=>false, 'message'=>'Record not found, Please try again later.'],200);
        }
        $status = ($request->status==0) ? 0 : 1;
        $obj->status = $status;

        if($obj->save()){
            return response()->json(['status'=>true, 'message'=>'Record updated successfully.'],200);
        }
        return response()->json(['status'=>false, 'message'=>'Record not deleted, Please try again later.'],200);
    }
}

