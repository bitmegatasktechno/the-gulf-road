<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CospaceStory;
use Illuminate\Validation\Rule;

class CospacestoryController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function index(){
        $active = 'propertycms';
        $subactive = 'cospaceStories';
        $data = CospaceStory::orderBy('_id','desc')->get();

        return view('admin.cospaceStories.index', compact('active','data','subactive'));
    }

    public function create(){
        $active = 'propertycms';
        $subactive = 'cospaceStories';

        return view('admin.cospaceStories.create', compact('active','subactive'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'name'=>'bail|required|unique:house_types|max:50',
                'mainBody' => 'bail|required',
                'logo' =>'required|image|mimes:jpeg,png,jpg,gif,svg',
            ],
            [
                'name.required' => 'The title field is required.',
                'logo.dimensions'=>'The logo height and width should be less than equal to 55 pixels.',
                'mainBody.required' => 'The description field is required.'
            ]
        );

        $name = null;
        $logo = $request->file('logo');
        $name = md5($logo->getClientOriginalName() . time()) . "." . $logo->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/cospaceStories');
        $logo->move($destinationPath, $name);

        $res = CospaceStory::create([
            'name' => request()->get('name'),
            'logo' => $name,
            'description' => request()->get('mainBody'),
            'status' => 1
        ]);
        if($res){
            return response()->json(['status'=>true,'message'=>'Record created successfully.','url'=>url('admin/cospaceStories')],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Something went wrong. Please try again'],200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CospaceStory  $housetype
     * @return \Illuminate\Http\Response
     */
    public function edit(CospaceStory $housetype, $id)
    {
        $active = 'propertycms';
        $subactive = 'cospaceStories';
        $data = CospaceStory::where('_id',$id)->first();
        return view('admin.cospaceStories.edit', compact('active','subactive','data'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $request->validate(
            [
                'name'=>['bail','required','max:50', Rule::unique('house_types')->ignore($id, '_id')],
            ]
        );
        $data = CospaceStory::where('_id', $id)->first();
        if(!$data){
            return response()->json(['status'=>false,'message'=>'Record not found. Please try again later'],200);
        }
        $data->name = request()->get('name');
        $data->description = request()->get('mainBody');

        if($request->file('logo')){
            $name = null;
            $logo = $request->file('logo');
            $name = md5($logo->getClientOriginalName() . time()) . "." . $logo->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/cospaceStories');
            $logo->move($destinationPath, $name);

            $old_path = public_path('upload/cospaceStories/'.$data->logo);
            
            if(\File::exists($old_path)){
                \File::delete($old_path);
            }

            $data->logo = $name;
            $data->description = request()->get('mainBody');
        }

        $res = $data->save();
        if($res){
            return response()->json(['status'=>true,'message'=>'Record updated successfully.','url'=>url('admin/cospaceStories')],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Something went wrong. Please try again'],200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HouseType  $housetype
     * @return \Illuminate\Http\Response
     */
    public function destroy(CospaceStory $housetype, $id)
    {
        if(CospaceStory::where('_id',$id)->delete()){
            return response()->json(['status'=>true, 'message'=>'Record deleted successfully.'],200);
        }
         return response()->json(['status'=>false, 'message'=>'Record not deleted, Please try again later.'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function status(Request $request)
    {
        $obj = CospaceStory::where('_id', $request->id)->first();
        if(!$obj){
            return response()->json(['status'=>false, 'message'=>'Record not found, Please try again later.'],200);
        }
        $status = ($request->status==0) ? 0 : 1;
        $obj->status = $status;

        if($obj->save()){
            return response()->json(['status'=>true, 'message'=>'Record updated successfully.'],200);
        }
        return response()->json(['status'=>false, 'message'=>'Record not deleted, Please try again later.'],200);
    }
}
