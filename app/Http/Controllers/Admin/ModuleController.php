<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Location;
use App\Models\Module;
use Illuminate\Validation\Rule;

class ModuleController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function listModules(){
        $active = 'modules';
        $subactive = 'modules';
        $data = Location::orderBy('_id','desc')->get();
        $modules = Module::all();
        return view('admin.modules.index', compact('active','data','subactive','modules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function checkboxResult(Request $request)
    {
        $check = Module::where('location',$request->loc)->where('type',$request->type)->first();
        if(@$check){
            $del = Module::where('location',$request->loc)->where('type',$request->type)->delete();
            return response()->json(['status'=>true, 'message'=>'Record removed successfully.'],200);
        }else{
        $module=new Module;
        $module->location=$request->loc;
        $module->type=$request->type;
        if($module->save()){
            return response()->json(['status'=>true, 'message'=>'Record saved successfully.'],200);
        }
    }
         return response()->json(['status'=>false, 'message'=>'Record not saved, Please try again later.'],200);
    }

   
}
