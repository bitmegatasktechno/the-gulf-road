<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Video;
use Illuminate\Validation\Rule;

class VideoController extends Controller
{
    public function index(){
        $active = 'video';
        $subactive = 'video';
        $data = Video::orderBy('_id','desc')->get();

        return view('admin.video.index', compact('active','data','subactive'));
    }

    public function save_video(Request $request){
        $check=Video::where('user_type',$request->type)->first();
        if(@$check){
            $update=\DB::table('videos')
            ->where('_id',$check->_id)
            ->update(array('video'=>$request->link));
            return response()->json(['status'=>true, 'message'=>'Record saved successfully.'],200);
        }else{
        $video=new Video;
        $video->user_type=$request->type;
        $video->video=$request->link;
        $video->status=1;
        if($video->save()){
            return response()->json(['status'=>true, 'message'=>'Record saved successfully.'],200);
        }
    }
         return response()->json(['status'=>false, 'message'=>'Record not saved, Please try again later.'],200);
    }
}