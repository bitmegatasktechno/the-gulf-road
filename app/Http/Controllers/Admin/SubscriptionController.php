<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subscription;
use App\Models\Transaction;
use Illuminate\Validation\Rule;

class SubscriptionController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function index(){
        $type = request()->segment(3);
        if($type == 'rent'){
            $active = 'subscription';
            $subactive = 'rent';
            $data = Subscription::where('type','rent')->orderBy('order','ASC')->get();
        }
        elseif($type == 'buy'){
            $active = 'subscription';
            $subactive = 'buy';
            $data = Subscription::where('type','buy')->orderBy('order','ASC')->get();
        }
        
        elseif($type == 'swap'){
            $active = 'subscription';
            $subactive = 'swap';
            $data = Subscription::where('type','swap')->orderBy('order','ASC')->get();
        }
        elseif($type == 'cospace'){
            $active = 'subscription';
            $subactive = 'cospace';
            $data = Subscription::where('type','cospace')->orderBy('order','ASC')->get();
        }
        elseif($type == 'user'){
            $active = 'subscription';
            $subactive = 'user';
            $data = Subscription::where('type','user')->orderBy('order','ASC')->get();
        }

        elseif($type == 'premium'){
            $active = 'subscription';
            $subactive = 'premium';
            $data = Subscription::where('type','premium')->orderBy('order','ASC')->get();
        }
        else{
            $active = 'subscription';
            $subactive = 'user';
        }

        return view('admin.subscription.index', compact('active','data','subactive','type'));
    }

    public function create(){
        $type = request()->segment(4);
        if($type == 'rent'){
            $active = 'rent';
            $subactive = 'rent';
        }
        if($type == 'buy'){
            $active = 'buy';
            $subactive = 'buy';
        }
        if($type == 'swap'){
            $active = 'swap';
            $subactive = 'swap';
        }
        if($type == 'cospace'){
            $active = 'cospace';
            $subactive = 'cospace';
        }
        if($type == 'user'){
            $active = 'user';
            $subactive = 'user';
        }
        if($type == 'premium'){
            $active = 'subscription';
            $subactive = 'premium';
        }
        $getTrans=Subscription::where('type',$type)->count();
        return view('admin.subscription.create', compact('active','subactive','type','getTrans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $request->validate(
            [
                'logo' =>'required|image|mimes:jpeg,png,jpg,gif,svg',
            ]
            
        );

        $check=Subscription::where('type',request()->get('type'))->where('name',request()->get('plan'))->count();
        if(@$check == 0){
        $name = null;
        $logo = $request->file('logo');
        $name = md5($logo->getClientOriginalName() . time()) . "." . $logo->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/subscription');
        $logo->move($destinationPath, $name);
        $data1=[];
        if(@$request->validity1){
        $data1[]=[
            'validity'=>request()->get('validity1'),
            'amount' => request()->get('amount1'),
            'count' => request()->get('count1'),
            'number' => request()->get('plan_number1'),
        ];
        }
        if(@$request->validity2){
            $data1[]=[
                'validity'=>request()->get('validity2'),
                'amount' => request()->get('amount2'),
                'count' => request()->get('count2'),
                'number' => request()->get('plan_number2'),
            ];
            }
            if(@$request->validity3){
                $data1[]=[
                    'validity'=>request()->get('validity3'),
                    'amount' => request()->get('amount3'),
                    'count' => request()->get('count3'),
                    'number' => request()->get('plan_number3'),
                ];
                }
        $res = Subscription::create([
            'name' => request()->get('plan'),
            'sub_plan'=>$data1,
            'content' => request()->get('desription'),
            'type' => request()->get('type'),
            'logo' => $name,
            'order' => (int)request()->get('order'),
            'status' => (int)1
        ]);
        }else{
            return \Redirect::back()->withErrors(['Name Already Exists']);
        }
        if($res){
            return redirect('admin/subscription/'.request()->get('type'));
            // return response()->json(['status'=>true,'message'=>'Record created successfully.','type'=>request()->get('type')],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Something went wrong. Please try again'],200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HouseType  $housetype
     * @return \Illuminate\Http\Response
     */
    public function edit(Subscription $subscription)
    {
        $type = request()->segment(5);
        $id = request()->segment(3);
        if($type == 'rent'){
            $active = 'rent';
            $subactive = 'rent';
        }
        if($type == 'buy'){
            $active = 'buy';
            $subactive = 'buy';
        }
        if($type == 'swap'){
            $active = 'swap';
            $subactive = 'swap';
        }
        if($type == 'cospace'){
            $active = 'cospace';
            $subactive = 'cospace';
        }
        if($type == 'user'){
            $active = 'user';
            $subactive = 'user';
        }
        if($type == 'premium'){
            $active = 'subscription';
            $subactive = 'premium';
        }
        $sub = Subscription::where('_id',$id)->first();
        return view('admin.subscription.edit', compact('active','subactive','type','sub'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request){
        $request->validate(
            [
                // 'logo' =>'bail|nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048|dimensions:max_width=55,max_height=55',
            ],
            [
                // 'logo.dimensions'=>'The logo height and width should be less than equal to 55 pixels.'
            ]
        );
        $data = Subscription::where('_id', $request->id)->first();
        if(!$data){
            return response()->json(['status'=>false,'message'=>'Record not found. Please try again later'],200);
        }

        if($request->file('logo')){
            $name = null;
            $logo = $request->file('logo');
            $name = md5($logo->getClientOriginalName() . time()) . "." . $logo->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/subscription');
            $logo->move($destinationPath, $name);

            $old_path = public_path('upload/subscription/'.$data->logo);
            
            if(\File::exists($old_path)){
                \File::delete($old_path);
            }

            $data->logo = $name;
        }
        $data1=[];
        if(@$request->validity1){
        $data1[]=[
            'validity'=>request()->get('validity1'),
            'amount' => request()->get('amount1'),
            'count' => request()->get('count1'),
            'number' => request()->get('plan_number1'),
        ];
        }
        if(@$request->validity2){
            $data1[]=[
                'validity'=>request()->get('validity2'),
                'amount' => request()->get('amount2'),
                'count' => request()->get('count2'),
                'number' => request()->get('plan_number2'),
            ];
            }
            if(@$request->validity3){
                $data1[]=[
                    'validity'=>request()->get('validity3'),
                    'amount' => request()->get('amount3'),
                    'count' => request()->get('count3'),
                    'number' => request()->get('plan_number3'),
                ];
                }

            $data->name = request()->get('plan');
            $data->content = request()->get('desription');
            $data->sub_plan = $data1;
        $res = $data->save();
        if($res){
            return redirect('admin/subscription/'.$data->type);
            // return response()->json(['status'=>true,'message'=>'Record updated successfully.'],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Something went wrong. Please try again'],200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HouseType  $housetype
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Subscription $subscription)
    {
        $subscription = Subscription::where('_id', $request->id)->first();
        if($subscription->delete()){
            return response()->json(['status'=>true, 'message'=>'Record deleted successfully.'],200);
        }
         return response()->json(['status'=>false, 'message'=>'Record not deleted, Please try again later.'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function status(Request $request)
    {
        $obj = Subscription::where('_id', $request->id)->first();
        if(!$obj){
            return response()->json(['status'=>false, 'message'=>'Record not found, Please try again later.'],200);
        }
        $status = ($request->status==0) ? 0 : 1;
        $obj->status = $status;

        if($obj->save()){
            return response()->json(['status'=>true, 'message'=>'Record updated successfully.'],200);
        }
        return response()->json(['status'=>false, 'message'=>'Record not deleted, Please try again later.'],200);
    }

    public function transact(Request $request){
        $type = request()->segment(4);
        $id = request()->segment(5);
        $active = 'transaction';
        $subactive = 'transaction';
        $data = Transaction::where('purchase_type',$type)->with('subscription')->where('plan_id',$id)->orderBy('purchase_date', 'DESC')->get();
         
        return view('admin.subscription.transaction', compact('active','data','subactive'));
    }
}
