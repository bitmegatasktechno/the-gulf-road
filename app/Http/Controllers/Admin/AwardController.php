<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Award;
use Illuminate\Validation\Rule;

class AwardController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function index(){
        $active = 'awards';
        $subactive = 'awards';
        $data = Award::orderBy('_id','desc')->get();

        return view('admin.awards.index', compact('active','data','subactive'));
    }

    public function create(){
        $active = 'awards';
        $subactive = 'awards';

        return view('admin.awards.create', compact('active','subactive'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'award'=>'required',
                'logo' =>'required|image|mimes:webp,jpeg,png,jpg,gif,svg',
            ],[
                'award.required'=> 'The name field is required'
            ]
            
        );

        $name = null;
        $logo = $request->file('logo');
        $name = md5($logo->getClientOriginalName() . time()) . "." . $logo->getClientOriginalExtension();
        $destinationPath = public_path('/uploads/awards');
        $logo->move($destinationPath, $name);

        $res = Award::create([
            'award' => request()->get('award'),
            'logo' => $name,
            'status' => (int)1
        ]);
        if($res){
            return response()->json(['status'=>true,'message'=>'Record created successfully.','url'=>url('admin/awards')],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Something went wrong. Please try again'],200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HouseType  $housetype
     * @return \Illuminate\Http\Response
     */
    public function edit(Award $awards)
    {
        $awards = request()->segment(3);
        $awards = Award::where('_id', $awards)->first();
        $active = 'awards';
        $subactive = 'awards';
        $data = $awards;
        
        return view('admin.awards.edit', compact('active','subactive','data'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $request->validate(
            [
                'logo' =>'bail|nullable|image|mimes:webp,jpeg,png,jpg,gif,svg|max:2048|dimensions:max_width=55,max_height=55',
            ],
            [
                'logo.dimensions'=>'The logo height and width should be less than equal to 55 pixels.'
            ]
        );
        $data = Award::where('_id', $id)->first();
        if(!$data){
            return response()->json(['status'=>false,'message'=>'Record not found. Please try again later'],200);
        }
        $data->award = request()->get('award');

        if($request->file('logo')){
            $name = null;
            $logo = $request->file('logo');
            $name = md5($logo->getClientOriginalName() . time()) . "." . $logo->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/awards');
            $logo->move($destinationPath, $name);

            $old_path = public_path('upload/awards/'.$data->logo);
            
            if(\File::exists($old_path)){
                \File::delete($old_path);
            }

            $data->logo = $name;
            $data->award = request()->get('award');
           

        }

        $res = $data->save();
        if($res){
            return response()->json(['status'=>true,'message'=>'Record updated successfully.','url'=>url('admin/awards')],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Something went wrong. Please try again'],200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HouseType  $housetype
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Award $awards)
    {   $awards = Award::where('_id', $request->id)->first();
        if($awards->delete()){
            return response()->json(['status'=>true, 'message'=>'Record deleted successfully.'],200);
        }
         return response()->json(['status'=>false, 'message'=>'Record not deleted, Please try again later.'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function status(Request $request)
    {
        $obj = Award::where('_id', $request->id)->first();
        if(!$obj){
            return response()->json(['status'=>false, 'message'=>'Record not found, Please try again later.'],200);
        }
        $status = ($request->status==0) ? 0 : 1;
        $obj->status = $status;

        if($obj->save()){
            return response()->json(['status'=>true, 'message'=>'Record updated successfully.'],200);
        }
        return response()->json(['status'=>false, 'message'=>'Record not deleted, Please try again later.'],200);
    }
}
