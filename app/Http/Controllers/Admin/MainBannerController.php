<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MainBanner;
use App\Models\Media;
use Intervention\Image\ImageManagerStatic as Image;
use DB,Redirect;

class MainBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $active = 'main_banner';
        $subactive = 'main_banner';
        $banners = MainBanner::orderBy('id','DESC')->get();
        $media = Media::first();
        return view('admin.banners.index',compact('active','subactive','banners','media'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $active = 'main_banner';
        $subactive = 'main_banner';
        return view('admin.banners.create',compact('active','subactive'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'title'=>'bail|required|max:100',
                'banner' =>'bail|required',
            ]
        );
        try{
            $name = null;
            $banner_file = $request->file('banner');
            $name = md5($banner_file->getClientOriginalName() . time()) . "." . $banner_file->getClientOriginalExtension();

            $image_resize = Image::make($banner_file->getRealPath());
            $image_resize->save(public_path('/uploads/banner/' .$name));
            $res = MainBanner::create([
                'title' => request()->get('title'),
                'image' => $name, 
                'status' => 1
            ]);
            if($res){
                return Redirect::route('main_banner.index')->with(['status'=>true,'message'=>'Banner Image added successfully.'],200);
            }else{
                return Redirect::back()->with(['status'=>false,'message'=>'Something went wrong. Please try again'],200);
            }
        }catch(\Exception $e){
            return response()->json(['status'=>false,'message'=>$e->getMessage()],400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = MainBanner::where('id',$id)->delete();
        if($delete){
            return response()->json(['status'=>true, 'message'=>'Banner deleted successfully.'],200);
        }
        return response()->json(['status'=>false, 'message'=>'Banner not deleted, Please try again later.'],200);
    }
    public function changeHomepageBanner(Request $request)
    {
        try{
            $request->validate([
                // 'banner' =>'bail|required|image|mimes:jpeg,png,jpg|dimensions:min_width=1600,min_height=800',
                'banner' => 'bail|required',
            ]);
            $name = null;
            $banner_file = $request->file('banner');
            $name = md5($banner_file->getClientOriginalName() . time()) . "." . $banner_file->getClientOriginalExtension();

            $image_resize = Image::make($banner_file->getRealPath());
            $image_resize->save(public_path('/uploads/banner/' .$name));
            $check = Media::count();
            if($check)
            {
                $media = Media::first();
                $update = Media::where('_id',$media->_id)->update([
                    'media_type' => 'image',
                    'image'      => $name
                ]);
            }else{
                $add = Media::create([
                    'media_type' => 'image',
                    'image'      => $name
                ]);

            }
            return Redirect::route('main_banner.index')->with(['status'=>true,'message'=>'Banner Image added successfully.'],200);
            
        }catch(\Exception $e){
            dd($e);
            return Redirect::back()->with(['status'=>false,'message'=>'Something went wrong. Please try again'],400);
        }
    }
}
