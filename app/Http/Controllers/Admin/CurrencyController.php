<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Currency;
use Illuminate\Validation\Rule;

class CurrencyController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function index(){
        $active = 'currencies';
        $subactive = 'currencies';
        $data = Currency::orderBy('_id','desc')->get();

        return view('admin.currencies.index', compact('active','data','subactive'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'bail|required|string|max:50|unique:currencies',
            'code'=>'bail|required|string|max:50|unique:currencies',
        ]);

        $obj = new Currency;
        $obj->name = request()->get('name');
        $obj->code = $request->code;
        $obj->status = 1;
        if($obj->save()){
            return response()->json(['status'=>true, 'message'=>'Record saved successfully.'],200);
        }
         return response()->json(['status'=>false, 'message'=>'Record not saved, Please try again later.'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Currency  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Currency $location)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Currency  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(Currency $currency)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'name'=>['bail','required','max:50', Rule::unique('currencies')->ignore($request->id, '_id')],
            'code'=>['bail','required','max:50', Rule::unique('currencies')->ignore($request->id, '_id')],
        ]);

        $obj = Currency::where('_id', $request->id)->first();
        if(!$obj){
            return response()->json(['status'=>false, 'message'=>'Record not found, Please try again later.'],200);
        }
        
        $obj->name = request()->get('name');
        if($obj->save()){
            return response()->json(['status'=>true, 'message'=>'Record saved successfully.'],200);
        }
        return response()->json(['status'=>false, 'message'=>'Record not saved, Please try again later.'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function destroy(Currency $currency)
    {
        if($currency->delete()){
            return response()->json(['status'=>true, 'message'=>'Record deleted successfully.'],200);
        }
         return response()->json(['status'=>false, 'message'=>'Record not deleted, Please try again later.'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function status(Request $request)
    {
        $obj = Currency::where('_id', $request->id)->first();
        if(!$obj){
            return response()->json(['status'=>false, 'message'=>'Record not found, Please try again later.'],200);
        }
        $status = ($request->status==0) ? 0 : 1;
        $obj->status = $status;

        if($obj->save()){
            return response()->json(['status'=>true, 'message'=>'Record updated successfully.'],200);
        }
        return response()->json(['status'=>false, 'message'=>'Record not deleted, Please try again later.'],200);
    }
}
