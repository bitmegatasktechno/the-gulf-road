<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Amenity;
use Illuminate\Validation\Rule;
use Intervention\Image\ImageManagerStatic as Image;

class AmenitiesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function index(){
        $active = 'propertycms';
        $subactive = 'amenities';
        $data = Amenity::orderBy('_id','desc')->get();

        return view('admin.amenities.index', compact('active','data','subactive'));
    }

    public function create(){
        $active = 'propertycms';
        $subactive = 'amenities';

        return view('admin.amenities.create', compact('active','subactive'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'name'=>'bail|required|unique:amenities|max:50',
                'logo' =>'required|image|mimes:webp,jpeg,png,jpg|max:2048|dimensions:min_width=35,min_height=35',
            ],
            [
            	'logo.dimensions'=>'The logo height and width should be more than equal to 35 pixels.'
            ]
        );

        $name = null;
        $logo = $request->file('logo');
        $name = md5($logo->getClientOriginalName() . time()) . "." . $logo->getClientOriginalExtension();

        $image_resize = Image::make($logo->getRealPath());
        $image_resize->resize(50, 50);
        $image_resize->save(public_path('/uploads/amenities/' .$name));

        $res = Amenity::create([
            'name' => strtolower(request()->get('name')),
            'logo' => $name,
            'status' => 1
        ]);
        if($res){
            return response()->json(['status'=>true,'message'=>'Record created successfully.','url'=>url('admin/amenities')],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Something went wrong. Please try again'],200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Amenity  $amenity
     * @return \Illuminate\Http\Response
     */
    public function show(Amenity $amenity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Amenity  $amenity
     * @return \Illuminate\Http\Response
     */
    public function edit(Amenity $amenity)
    {
        $active = 'propertycms';
        $subactive = 'amenities';
        $data = $amenity;

        return view('admin.amenities.edit', compact('active','subactive','data'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $request->validate(
            [
                'name'=>['bail','required','max:50', Rule::unique('amenities')->ignore($id, '_id')],
                'logo' =>'bail|nullable|image|mimes:jpeg,png,jpg|max:2048|dimensions:min_width=55,min_height=55',
            ],
            [
            	'logo.dimensions'=>'The logo height and width should be more than equal to 55 pixels.'
            ]
        );
        $data = Amenity::where('_id', $id)->first();
        if(!$data){
            return response()->json(['status'=>false,'message'=>'Record not found. Please try again later'],200);
        }
        $data->name = strtolower(request()->get('name'));

        if($request->file('logo')){
            $name = null;
            $logo = $request->file('logo');
            $name = md5($logo->getClientOriginalName() . time()) . "." . $logo->getClientOriginalExtension();

            $image_resize = Image::make($logo->getRealPath());
            $image_resize->resize(50, 50);
            $image_resize->save(public_path('/uploads/amenities/' .$name));

            $old_path = public_path('upload/amenities/'.$data->logo);
            
            if(\File::exists($old_path)){
                \File::delete($old_path);
            }

            $data->logo = $name;
        }

        $res = $data->save();
        if($res){
            return response()->json(['status'=>true,'message'=>'Record updated successfully.','url'=>url('admin/amenities')],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Something went wrong. Please try again'],200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Amenity  $amenity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Amenity $amenity)
    {
        if($amenity->delete()){
            return response()->json(['status'=>true, 'message'=>'Record deleted successfully.'],200);
        }
         return response()->json(['status'=>false, 'message'=>'Record not deleted, Please try again later.'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function status(Request $request)
    {
        $obj = Amenity::where('_id', $request->id)->first();
        if(!$obj){
            return response()->json(['status'=>false, 'message'=>'Record not found, Please try again later.'],200);
        }
        $status = ($request->status==0) ? 0 : 1;
        $obj->status = $status;

        if($obj->save()){
            return response()->json(['status'=>true, 'message'=>'Record updated successfully.'],200);
        }
        return response()->json(['status'=>false, 'message'=>'Record not deleted, Please try again later.'],200);
    }
}
