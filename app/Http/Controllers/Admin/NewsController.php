<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subscribe;
use App\Models\Newsletter;
use App\Models\Homeloan;
use App\Models\BookProperty;

use App\Models\UserEnquiry;
use Illuminate\Validation\Rule;
use App\Mail\SendMailToSubscribers; 
use Illuminate\Support\Facades\Mail;

class NewsController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){

    }

    public function news(){
        $active = 'newsletter';
        $subactive = 'newsletter';
        $data = Newsletter::orderBy('_id','desc')->get();

        return view('admin.newsletter.newsletter', compact('active','data','subactive'));
    }

    public function subscribe(){
        $active = 'subscribe';
        $subactive = 'subscribe';
        $data = Subscribe::orderBy('_id','desc')->get();

        return view('admin.newsletter.subscription', compact('active','data','subactive'));
    }


    //New FUnctions

     public function subscribeMailSend(){
        $active = 'subscribe';
        $subactive = 'subscribe';
        $data = Subscribe::orderBy('_id','desc')->paginate(30);

        return view('admin.newsletter.subscribemailsend', compact('active','data','subactive'));
    }


    public function newsletterEmailSend(Request $request)
    {   


        $countsend = 0;
        
          $get_all_email = ($request->emails);
        

        
                if (!empty($get_all_email)) {
            $get_exploded_email = explode(',',$request->emails);

            foreach($get_exploded_email as $k) {

                $k = strtolower($k);
                $k = ltrim($k);
                $k = rtrim($k);
                $k = str_replace(' ', '', $k);
                $k = preg_replace('/\s+/', '', $k);

                $issend = Mail::to($k)->cc('info@thegulfroad.com')->send(new SendMailToSubscribers($k));
                 
                    $countsend++;
                 

            }
        }

       
       return response()->json(['success'=>true,'message'=>' Total Mail '.$countsend.' Send email successfully.']);
    }


    //New FUnctions    

    public function homeloan(){
        $active = 'homeloan';
        $subactive = 'homeloan';
        $data = Homeloan::orderBy('_id','desc')->get();

        return view('admin.newsletter.homeloan', compact('active','data','subactive'));
    }

    public function deleteHomeloan($id)
    {
        $delete = Homeloan::where('_id',$id)->delete();
        return response()->json(['status'=>1, 'message' => 'Homeloan deleted successfully']);
    }

    public function enquery(){
        $active = 'enquery';
        $subactive = 'enquery';
        $data = UserEnquiry::orderBy('_id','desc')->get();

        return view('admin.newsletter.enquery', compact('active','data','subactive'));
    }

    public function booking(){
        $active = 'booking';
        $subactive = 'booking';
        $data= BookProperty::orderBy('_id','desc')->get();

        return view('admin.newsletter.booking', compact('active','data','subactive'));
    }

}