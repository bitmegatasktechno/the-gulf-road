<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\Subscription;
use App\Models\Property;
use App\Models\CoworkspaceProperty;
use App\Models\SwapProperty;

use Illuminate\Validation\Rule;
use Carbon\Carbon;
class RevenueController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    public function index(Request $request){

        $transactions = new Transaction;
        $start_date = date("Y-m-d", strtotime($request->start_date));
        $start_date1 =  date("Y", strtotime($request->start_date));
        $start_date2 =  date("m", strtotime($request->start_date));
        $start_date3 =  date("d", strtotime($request->start_date));
        $end_date1 = date("Y", strtotime($request->end_date));
        $end_date2 = date("m", strtotime($request->end_date));
        $end_date3 = date("d", strtotime($request->end_date));
        $end_date = date("Y-m-d", strtotime($request->end_date));
        // $start_date=$start_date.'T11:40:15.101+00:00';
        // $end_date=$end_date.'T11:40:15.101+00:00';
        // $start_date =  new \MongoDB\BSON\UTCDateTime($request->start_date);
        


        if($start_date!='1970-01-01' && $end_date!='1970-01-01')
        {

            // $transactions = $transactions->where('created_at','>=','2020-10-01 ')->get();
            $transactions = $transactions->whereBetween(
                'created_at', array(
                    Carbon::createFromDate($start_date1, $start_date2, $start_date3),
                    Carbon::createFromDate($end_date1, $end_date2, $end_date3)
                )
                );


        }
        else{
            $start_date = '';
            $end_date = '';
            
        }
        $active = 'subscription';
        $subactive = 'revenue';
        $data =     $transactions->orderBy('_id','DESC')->get();
        return view('admin.revenue.index', compact('active','data','subactive','end_date','start_date'));
    }

    public function select_plan(Request $request){
        if($request->type == 'Rent & Buy'){
            $type='rent';
        }elseif($request->type == 'Swap'){
            $type='swap';
        }elseif($request->type == 'Co Space'){
            $type='cospace';
        }elseif($request->type == 'Users'){
            $type='user';
        }
        $getPlans=Subscription::where('type',$type)->get();
        foreach($getPlans as $getPlan){
            $plan_name = $getPlan->name;
            $users_arr[] = array("name" => $plan_name);
        }
        return response()->json(['users_arr'=>$users_arr]);
    }

    public function select_val(Request $request){
        if($request->type == 'Rent & Buy'){
            $type='rent';
        }elseif($request->type == 'Swap'){
            $type='swap';
        }elseif($request->type == 'Co Space'){
            $type='cospace';
        }elseif($request->type == 'Users'){
            $type='user';
        }
        $getPlans=Subscription::where('type',$type)->where('name',$request->name)->first();
        foreach($getPlans->sub_plan as $getPlan){
            $plan_val = $getPlan['validity'];
            $users_arr1[] = array("name" => $plan_val);
        }
        return response()->json(['users_arr1'=>$users_arr1]);
    }

    public function property_listing(Request $request){
        $active = 'properties';
        $subactive = 'rentsale';
        $data = array();
        if($request->type=='Rent & Buy'){
        $data = Property::where('plan_id',$request->plan_id)->orderBy('_id','desc')->with(['user'=>function($q){
            $q->select('_id','email');
        }])->get();
        }elseif($request->type=='Co Space'){
            $data = CoworkspaceProperty::where('plan_id',$request->plan_id)->orderBy('_id','desc')->with(['user'=>function($q){
                $q->select('_id','email');
            }])->get();
        }elseif($request->type=='Swap'){
            $data = SwapProperty::where('plan_id',$request->plan_id)->orderBy('_id','desc')->with(['user'=>function($q){
                $q->select('_id','email');
            }])->get();
        }
        return view('admin.revenue.property_listing', compact('active','data','subactive'));
    }

}
