<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Review;
use Illuminate\Validation\Rule;

class ReviewController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){

    }

    public function index(){
        $active = 'raview';
        $subactive = 'review';
        $data = Review::orderBy('_id','desc')->get();

        return view('admin.review.index', compact('active','data','subactive'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'bail|required|string|max:50|unique:languages',
        ]);

        $obj = new Language;
        $obj->name = strtolower(request()->get('name'));
        $obj->status = 1;
        if($obj->save()){
            return response()->json(['status'=>true, 'message'=>'Record saved successfully.'],200);
        }
         return response()->json(['status'=>false, 'message'=>'Record not saved, Please try again later.'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function show(Language $language)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function edit(Language $language)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'name'=>['bail','required','max:50', Rule::unique('languages')->ignore($request->id, '_id')],
        ]);

        $obj = Language::where('_id', $request->id)->first();
        if(!$obj){
            return response()->json(['status'=>false, 'message'=>'Record not found, Please try again later.'],200);
        }

        $obj->name = strtolower(request()->get('name'));
        if($obj->save()){
            return response()->json(['status'=>true, 'message'=>'Record saved successfully.'],200);
        }
        return response()->json(['status'=>false, 'message'=>'Record not saved, Please try again later.'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function destroy(Language $language)
    {
        if($language->delete()){
            return response()->json(['status'=>true, 'message'=>'Record deleted successfully.'],200);
        }
         return response()->json(['status'=>false, 'message'=>'Record not deleted, Please try again later.'],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function status(Request $request)
    {
        $obj = Review::where('_id', $request->id)->first();
        if(!$obj){
            return response()->json(['status'=>false, 'message'=>'Record not found, Please try again later.'],200);
        }
        $status = ($request->status==0) ? 0 : 1;
        $obj->is_approved = $status;

        if($obj->save()){
            return response()->json(['status'=>true, 'message'=>'Record updated successfully.'],200);
        }
        return response()->json(['status'=>false, 'message'=>'Record not deleted, Please try again later.'],200);
    }
}
