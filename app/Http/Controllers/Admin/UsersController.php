<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\User;
use App\Models\Award;
use App\Models\UserAward;
class UsersController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        
    }

    /**
    * return all users listing
    */
    public function users(Request $request){
        $active = 'users';
        $subactive = 'users';
        $inputs = $request->all();
        $users = User::whereIn('role',['USER']);

        // filter data based on filter requested 
        if(isset($inputs['name'])){
            $users->where('name', 'like', '%' . $inputs['name'] . '%');
        }
        if(isset($inputs['email'])){
            $users->where('email', 'like', '%' . $inputs['email'] . '%');
        }
        if(isset($inputs['phone'])){
            $users->where('phone', 'like', '%' . $inputs['phone'] . '%');
        }

        if(isset($inputs['status'])){
            $users->where('status', (int)$inputs['status']);
        }

        $users = $users->latest()->paginate(10);

        if($request->ajax()){
            return view('admin.users.listing',compact('active','users','subactive'));
        }
        return view('admin.users.index',compact('active','users','subactive'));
    }

    /**
    * get user details of particular user
    */
    public function userDetail(Request $request, $id){
        $active = 'users';
        $subactive = 'users';
        $id = new \MongoDB\BSON\ObjectID($id);
        $user = User::where('_id', $id)->firstOrFail();

        return view('admin.users.details.index', compact('active','user','subactive'));
    }

    /**
    * update user status
    */
    public function userStatusUpdate(Request $request){
        $status = ($request->status==1) ? 1 : 0;
        
        $id  =  $request->id;
        $id = new \MongoDB\BSON\ObjectID($id);
        $user = User::where('_id', $id)->first();
        if($user){
            User::where('_id', $id)->update(['status'=>$status]);
            return response()->json(['status'=>true,'message'=>'User status changed successfully.'],200);
        }else{
            return response()->json(['status'=>false,'message'=>'User record not found. Please try again later.'],200);
        }        
    }


    public function userTopUpdate(Request $request){
        $id  =  $request->id;
        $id = new \MongoDB\BSON\ObjectID($id);
        $user = User::where('_id', $id)->first();
        if($user){
            if($request->type == 'remove'){
                $top_list = $user->top_list;
                $updatedList = array();
                foreach($top_list as $val){
                    if($val == $request->top){
                        continue;
                    }
                    $updatedList[] = $val;
                }
                User::where('_id', $id)->update(['top_list'=>$updatedList]);
            }elseif($request->type == 'add'){
                $top_list = $user->role;
                $updatedList = $user->top_list;
                foreach($top_list as $val){
                    if($val == $request->top){
                        $updatedList[] = $val;
                    }
                }
                User::where('_id', $id)->update(['top_list'=>$updatedList]);
            }
            return response()->json(['status'=>true,'message'=>'User status changed successfully.'],200);

        }else{
            return response()->json(['status'=>false,'message'=>'User record not found. Please try again later.'],200);
        }        
    }


    /**
    * return all agents listing
    */
    public function agents(Request $request){
        $active = 'users';
        $subactive = 'agents';
        $inputs = $request->all();
        $users = User::whereIn('role',['AGENT']);

        // filter data based on filter requested 
        if(isset($inputs['name'])){
            $users->where('name', 'like', '%' . $inputs['name'] . '%');
        }
        if(isset($inputs['email'])){
            $users->where('email', 'like', '%' . $inputs['email'] . '%');
        }
        if(isset($inputs['phone'])){
            $users->where('phone', 'like', '%' . $inputs['phone'] . '%');
        }

        if(isset($inputs['status'])){
            $users->where('status', (int)$inputs['status']);
        }

        $users = $users->latest()->paginate(10);
        if($request->ajax()){
            return view('admin.agents.listing',compact('active','users','subactive'));
        }

        return view('admin.agents.index',compact('active','users','subactive'));
    }

     /**
    * get user details of particular user
    */
    public function agentDetail(Request $request, $id){
        $active = 'users';
        $subactive = 'agents';
        $id = new \MongoDB\BSON\ObjectID($id);
        $user = User::where('_id', $id)->with(['profiles','addresses','agentProfessional','qualifications'])->firstOrFail();
        return view('admin.agents.details.index', compact('active','user','subactive'));
    }

    /**
    * return all buddies listing
    */
    public function buddies(Request $request){
        $active = 'users';
        $subactive = 'buddies';
        $inputs = $request->all();
        $users = User::whereIn('role',['BUDDY']);
        // filter data based on filter requested 
        if(isset($inputs['name'])){
            $users->where('name', 'like', '%' . $inputs['name'] . '%');
        }
        if(isset($inputs['email'])){
            $users->where('email', 'like', '%' . $inputs['email'] . '%');
        }
        if(isset($inputs['phone'])){
            $users->where('phone', 'like', '%' . $inputs['phone'] . '%');
        }
        if(isset($inputs['status'])){
            $users->where('status', (int)$inputs['status']);
        }
        $users = $users->latest()->paginate(10);
        if($request->ajax()){
            return view('admin.buddies.listing',compact('active','users','subactive'));
        }
        return view('admin.buddies.index',compact('active','users','subactive'));
    }


    /**
    * get user details of particular user
    */
    public function buddyDetail(Request $request, $id){
        $active = 'users';
        $subactive = 'buddies';
        $id = new \MongoDB\BSON\ObjectID($id);
        $user = User::where('_id', $id)->firstOrFail();
        return view('admin.buddies.details.index', compact('active','user','subactive'));
    }

    public function showUpgradeRequests(Request $request, $user){
        $active = 'user_requests';
        $inputs = $request->all();
        $page = '';

        if($user =='buddy'){
            $subactive = 'buddy_request';
            $page = 'buddy-requests';
            $users = User::where('buddyProfile', 1);
        }
        else if($user =='agents'){
            $subactive = 'agent_request';
            $page = 'agent-requests';
            $users = User::where('agentProfile', 1);
        }else{
            return redirect('admin/dashboard');
        }

        // filter data based on filter requested 
        if(isset($inputs['name'])){
            $users->where('name', 'like', '%' . $inputs['name'] . '%');
        }
        if(isset($inputs['email'])){
            $users->where('email', 'like', '%' . $inputs['email'] . '%');
        }
        if(isset($inputs['phone'])){
            $users->where('phone', 'like', '%' . $inputs['phone'] . '%');
        }

        if(isset($inputs['status'])){
            $users->where('status', (int)$inputs['status']);
        }

        $users = $users->latest()->paginate(10);

        if($request->ajax()){
            return view('admin.'.$page.'.listing',compact('active','users','subactive'));
        }
        return view('admin.'.$page.'.index',compact('active','users','subactive'));
    }

    public function buddyApplicationDetails(Request $request, $id){
        $active = 'user_requests';
        $subactive = 'buddy_request';
        $id = new \MongoDB\BSON\ObjectID($id);
        $user = User::where('_id', $id)->firstOrFail();
        return view('admin.buddy-requests.details.index', compact('active','user','subactive'));
    }

    public function agentApplicationDetails(Request $request, $id){
        $active = 'user_requests';
        $subactive = 'agent_request';
        $id = new \MongoDB\BSON\ObjectID($id);
        $user = User::where('_id', $id)->firstOrFail();
        return view('admin.agent-requests.details.index', compact('active','user','subactive'));
    }

    /**
    * update user status
    */
    public function buddyApplicationStatus(Request $request){
        $status = ($request->status==1) ? 2 : 0;
        
        $id  =  $request->id;
        $id = new \MongoDB\BSON\ObjectID($id);
        $user = User::where('_id', $id)->first();
        if($user){
            $roles = $user->role;
            if(!in_array('BUDDY', $roles, true)){
                array_push($roles, 'BUDDY');
            }

            $user->update(['buddyProfile'=>$status,'role'=>$roles]);
            return response()->json(['status'=>true,'message'=>'Profile approved successfully.'],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Something went wrong. Please try again later.'],200);
        }        
    }

    /**
    * update user status
    */
    public function agentApplicationStatus(Request $request){
        $status = ($request->status==1) ? 2 : 0;
        
        $id  =  $request->id;
        $id = new \MongoDB\BSON\ObjectID($id);
        $user = User::where('_id', $id)->first();
        if($user){
            $roles = $user->role;
            if(!in_array('AGENT', $roles, true)){
                array_push($roles, 'AGENT');
            }
            
            $user->update(['agentProfile'=>$status,'role'=>$roles]);

            return response()->json(['status'=>true,'message'=>'Profile approved successfully.'],200);
        }else{
            return response()->json(['status'=>false,'message'=>'Something went wrong. Please try again later.'],200);
        }        
    }

    public function deleteUser(Request $request){        
        $id  =  $request->id;
        $id = new \MongoDB\BSON\ObjectID($id);
        $user = User::where('_id', $id)->first();
        if($user->delete()){
            return response()->json(['status'=>true,'message'=>'User deleted successfully.'],200);
        }else{
            return response()->json(['status'=>false,'message'=>'User record not found. Please try again later.'],200);
        }   
    }

    public function add_awards(Request $request){
        $active = 'users';
        $subactive = 'users';
        $id  =  $request->id;
        $allAwards=Award::all();
        return view('admin.users.add_award',compact('id','allAwards','active','subactive'));
    }

    public function save_awards(Request $request){
        $id  =  $request->id;
        $awards=$request->awards;

        $userAwards=new UserAward;
        $userAwards->user_id=$id;
        $userAwards->award_id=$awards;
        $userAwards->save();

        return response()->json(['status'=>true,'message'=>'successfully added.'],200);
    }
}
