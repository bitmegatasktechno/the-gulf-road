<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'admin')
    {
        if (Auth::guard('admin')->check()) 
        {    
            if(is_array(Auth::guard('admin')->user()->role) && in_array('ADMIN', Auth::guard('admin')->user()->role)){
                return $next($request);
            }else
            {
                //Auth::logout();
                //flash('Sorry! You are not the authorised user.')->error();
                return redirect('admin/login');
            }
        }
        else
        {
            //Auth::logout();
            //flash('Sorry! You are not the authorised user.')->error();
            return redirect('admin/login');
        }
    }
}
