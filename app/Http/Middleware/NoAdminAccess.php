<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class NoAdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if(is_array(Auth::user()->role) && in_array('ADMIN', Auth::user()->role)){
                Auth::logout();
                if($request->expectsJson())
                {
                    return response()->json(['status'=>false,'message'=>'Admin is not alloweded to access User section.'], 400);
                }
                return redirect('/');
            }else{
                return $next($request);
            }
        }else{
            return $next($request);
        }
    }
}
